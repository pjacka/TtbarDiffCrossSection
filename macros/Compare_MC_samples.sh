macroPath=$PWD"/TtbarDiffCrossSection/macros/"
cd $macroPath

if [ $USER == pberta ] ; then
dir=${TtbarDiffCrossSection_output_path}
configName="constants.env"
fi
if [ $USER == pjacka ] ; then
dir=${TtbarDiffCrossSection_output_path}
configName="constants.env"
fi
if [ $USER == jacka ] ; then
dir=${TtbarDiffCrossSection_output_path}
configName="constants.env"
fi
if [ $USER == jpalicka ] ; then
dir=${TtbarDiffCrossSection_output_path}
configName="constants.env"
fi

root -l -q -b "${macroPath}Compare_MC_samples.C+g(\"${dir}\",\"${configName}\")"

cd -

