#!/bin/bash

#mc_productions=( "MC16a" "MC16d" "MC16e" "All")
#mc_productions=( "MC16a")
mc_productions=( "All")

for MC_PROD in ${mc_productions[@]}
do

 inPath=${TtbarDiffCrossSection_output_path}/root.${MC_PROD}/systematics_histos/
outPath=${TtbarDiffCrossSection_output_path}/pdf.${MC_PROD}/MigrationMatrices
outPathPNG=${TtbarDiffCrossSection_output_path}/png.${MC_PROD}/MigrationMatrices

mkdir -p ${outPath}/Parton
mkdir -p ${outPath}/Particle
mkdir -p ${outPathPNG}/Parton
mkdir -p ${outPathPNG}/Particle


cd ${TTBAR_PATH}/TtbarDiffCrossSection/macros/
root -l -b -q "Migration.C(\"$inPath\",\"$outPath\")"
cd ${TTBAR_PATH}
done
