#ifndef DrawMMefficiencies_h
#define DrawMMefficiencies_h



#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <map>

#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TDirectory.h"
#include "TKey.h"
#include "TVectorD.h"
#include "TTree.h"
#include "TMath.h"
#include "TROOT.h"
#include "Math/Random.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"

#include "functions.h"

using namespace std;

void DrawCutflow(TString);
void plotCutflowHistogram(TH1D* h);
void printTexTables(ofstream& textfile,TH1D* h);
double g_lumi;
double g_ttbar_SF;

#endif
