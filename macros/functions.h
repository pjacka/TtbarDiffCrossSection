//  Peter Berta, 16.10.2012
#ifndef functions_macros_h
#define functions_macros_h

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>

#include "TString.h"
#include "TStyle.h"
#include "TGraph.h"
#include "TMath.h"
#include "TTree.h"
#include "TKey.h"
#include "TDirectory.h"
#include "TCollection.h"
#include "TClass.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TF1.h"
#include "TEfficiency.h"
#include "TRandom.h"
#include "TROOT.h"
#include "Math/Random.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"


using namespace std;

//this functions returns a vector of TStrings from a string containing smaller strings separated by ;
vector<TString> MakeVector(string text);
vector<float> MakeVectorFloat(string text);
vector<double> MakeVectorDouble(string text);

//returns a vector of subdirectories in the current directory
vector<TString> GetSubdirectories(int debug);
vector<TString> GetTTrees(int debug=0);
map<TString, TH1F*> GetTH1Fs(int debug);
map<TString, TH1D*> GetTH1Ds(int debug);
map<TString, TH2F*> GetTH2Fs(int debug);
map<TString, TProfile*> GetTProfiles(int debug);

//returns inverted value, if the input is too small (zero) than the returned value is 1e8
//Double_t invert(Double_t *x, Double_t *par);

double getLumi(TEnv* configLumi,TString mcProduction);

string convertNumberToString(double number, int nDigits=-1);
void PrintCanvas(TCanvas *can, TString outputDir, TString filename,Bool_t logy=false);
void PrintCanvas(TCanvas *can, TString outputDir, TString mc_production, TString filename,Bool_t logy=false);
void WriteGeneralInfo(TString cut_label, TString lumi="", float size = 0.039, float x = 0.22, float y = 0.955,float shift=0.58);
void WriteInfo(TString info, float size=0.039, float x=0.13, float y=0.76, int color=1);
void Draw2Histograms(TH1* hist1, TH1* hist2, TString name1, TString name2, Float_t xmin=0.58, Float_t ymax=0.9, Float_t xmax=0.89, Float_t ymin=0.72);
void Draw3Histograms(TH1* hist1, TH1* hist2, TH1* hist3, TString name1, TString name2, TString name3, float yaxis_min=1., float yaxis_max=0., Float_t xmin=0.66, Float_t ymax=0.85, Float_t xmax=0.92, Float_t ymin=0.64);
void NormalizeColumns(TH2D *hist, bool use_overlow_underflow=true);
void NormalizeRows(TH2 *hist);
void Normalize(TH1 *hist);
TString FindRange(TString range);
void DrawMCvsData(TH1* mc, TH1* data, Float_t xmin=0.7, Float_t ymax=1, Float_t xmax=1, Float_t ymin=0.82);
vector<TObject*> DrawMCvsData_correction(TPad *pad1, TPad *pad2, TPad *pad3, TPad *pad4, TH1* mc_reco, TH1* mc_corr, TH1* mc_truth, TH1* data_reco, TH1* data_corr, TString option="h");
vector<TObject*> DrawMCvsData_shapeTProfile(TPad *pad1, TPad *pad2, TPad *pad3, TH1* mc_reco, TH1* mc_corr, TH1* mc_truth, TH1* data_reco, TH1* data_corr, TString option="h", float xaxis_min=1., float xaxis_max=0., float yaxis_min=1., float yaxis_max=0.);
void DeletePointers(vector<TObject*> vec);
TH1F* GetNPVHistogram(TH2F* hist2D, int iNPV, int cummulativeFactor, TString name="histFrom_");
void ChangeYRange(TH1* hist, float &yaxis_min, float &yaxis_max);
void Draw4Histograms(TH1* mc_reco, TH1* mc_corr, TH1* data_reco, TH1* data_corr, TString mc_reco_string, TString mc_corr_string, TString data_reco_string, TString data_corr_string, Float_t xmin=0.7, Float_t ymax=0.95, Float_t xmax=0.9, Float_t ymin=0.82);
void DrawTH1s(map<TString, TH1F*> mymap, Float_t xmin=0, Float_t xmax=-1, Float_t ymin=0, Float_t ymax=-1, Float_t xmin_legend=0.56, Float_t ymax_legend=0.83, Float_t xmax_legend=0.85, Float_t ymin_legend=0.5);
TH1F* GetMedian(TH2* hist);
float round(float d, int n=1);
void ATLASLabel(Double_t x,Double_t y,const char* text, float tsize=-1, Color_t color=1);
void DrawTwoPadsForHistos(TPad *pad1, TPad* pad2, map<TString, TH1D*> mymap, Bool_t logy=false, TString yaxisTitle_pad2="", Float_t xmin=0, Float_t xmax=-1, Float_t ymin=0, Float_t ymax=-1, Float_t xmin_legend=0.7, Float_t ymax_legend=0.9, Float_t xmax_legend=0.9, Float_t ymin_legend=0.7);
void DrawTwoPadsForProfiles(TPad *pad1, TPad* pad2, map<TString, TProfile*> mymap, Bool_t logy=false, TString yaxisTitle_pad2="", Float_t xmin=0, Float_t xmax=-1, Float_t ymin=0, Float_t ymax=-1, Float_t xmin_legend=0.55, Float_t ymax_legend=0.85, Float_t xmax_legend=0.95, Float_t ymin_legend=0.55);
void  PrepareHistCONF(TH1* hist, int rebin, int color, int style, float scale, TString units);
void WriteGeneralInfoCONF(TString shape, TString name, bool lumi=false, bool info_for_all_events=false, TString sample="dijets (Herwig++)", float size = 0.04, float x = 0.2, float y = 0.87, TString type="Internal");
TH1F* GetEfficiencyVSNPV(TH2* hist, bool larger, float cutValue, float& cutValue_real);
TH1F* Get1DHistogramFrom2D(TH2F* hist2D, int low_int, int up_int, TString name);
TString CreateLegendName(int low, int high, TString variable);
TH1F* GetWidthHistogram(TH2F* hist2D, float frac, TString name);
void DivideByBinWidth(TH1* hist);
void MultiplyByBinWidth(TH1* hist);

void MigrateUnderOverFlowBins(TH1* hist);
void MigrateUnderOverFlowBins(TH2* hist);
TH1D* GetEfficiency(TH1* passed, TH1* total);
TH2D* ProduceHistogramWithSwitchedAxis(TH2* hist);
void ConvertToCrossSection(TH1* hist,float lumi, TString label="", TString unit="");
void SetLabelForNormalized(TH1* hist, TString label="", TString unit="");
void SetOverflowBinsToZero(TH2D* hist);
void RemoveNegatives(TH1* hist);
void RemoveNegatives(TH2* hist);
TH1D* Rebin1D(TH1* hist, vector<float> x, TString name="");
TH2D* Rebin2D(TH2* hist, vector<float> x, vector<float> y, TString name="");
Double_t* GetArrayOfBins(vector<Float_t> bins);
void DrawHistos(TPad* pad1, TPad* pad2, vector<TH1D*> graphs, vector<TString> names, Float_t xmin=0, Float_t xmax=-1, Float_t ymin=0, Float_t ymax=-1, Float_t xmin_legend=0.49, Float_t ymax_legend=0.89, Float_t xmax_legend=0.95, Float_t ymin_legend=0.55);

double chi2_ABCD(vector<vector<double> >& table,double&);
double chi2_ABCD_weighted(vector<vector<TH1D*> >& hist_table,double& chi2,vector<double>&);
double chi2_ABCD_weighted_binbybin(vector<vector<TH1D*> >& hist_table,double& chi2,vector<double>& chisquares,vector<vector<double> >& eff_numbers);
void get_Correlation_of_Table(vector<vector<TH1D*> >& hist_table,double& cor,double& cov,double& Cb);
double get_t_statistics(vector<vector<TH1D*> >& hist_table,double& w);
double chi2_ABCD_weighted(vector<vector<double> >& table,vector<vector<double> >& table2,vector<double>& sumrows ,vector<double>& sumcolumns,double ntotal1,int i0,int j0);
double chi2_ABCD_weighted(vector<vector<double> >& table,vector<vector<double> >& table2,vector<double>& sumrows ,vector<double>& sumcolumns,double ntotal1,double N,int i0,int j0);
double chi2_ABCD_weighted(vector<vector<double> > table,vector<vector<vector<vector<double> > > >& cov4D,int i0,int j0);
void Estimate_Covariance(vector<vector<double> >& table,vector<vector<double> >& table2,vector<vector<vector<vector<double> > > >& cov4d,unsigned long niter);


void smear_histogram(TH1D*,TH1D*);// Function for smearing histogram
void plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TH1D* hd, TString y2name, double y2min, double y2max,TString lumi="",bool use_logscale=false);
void plotGraphs(vector<TGraph*>& graph,TLegend* leg,TString dirname,TString name,TString lumi="");
void plotTH2histogram(TH2* h,TString dirname,TString name,TString lumi,TString option);
template<typename T> 
void SetXaxisTitle(T* hist, TString xname);
double chi2TestNormalized(TMatrixT<double>& x1,TMatrixT<double>& x2, TMatrixT<double>& cov,double &chi2,int &ndf,TString diffXsec);
TMatrixT<double> covarianceFromPrediction(TH1D* h,TString diffXsec);

void removeNegatives(TH1* hist);
void SetErrorsToZero(TH1* hist);
map<TString, TH2D*> GetTH2Ds(int debug=0);
TH1D *GetRandomTopPtFrom2DSpectrum(TH1D *h);
TH1D *GetLeadOrSubleadTopPtFrom2DSpectrum(TH1D *h, int LeadOrSublead);
TH1D *GetInclusiveTopPtFrom2DSpectrum(TH1D *h);


#endif
