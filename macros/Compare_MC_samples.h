// Peter Berta, 23.10.2013


#ifndef Compare_MC_samples_h
#define Compare_MC_samples_h



#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"
#include "TROOT.h"
#include "Math/Random.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"

#include "functions.cxx"

using namespace std;

void Compare_MC_samples(TString mainDir,TString configName);
vector<TString> FilterNames(vector<TString>& names,TString level,TString s); 
vector<TString> CreateXTitles(vector<TString>& names,TString);
TH1D* loadHistogram(TString histname,TFile* file);
void create_region_names(vector<TString>& names);

vector<TFile*> g_files;
vector<TString> g_names;
vector<TString> g_region_names;
double g_lumi;

#endif

