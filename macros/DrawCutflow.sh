macroPath=$PWD"/TtbarDiffCrossSection/macros/"
cd $macroPath

if [ $USER == pberta ] ; then
dir="/afs/cern.ch/user/p/pberta/Workspace/xAnalysis/"
fi
if [ $USER == pjacka ] ; then
dir="/afs/cern.ch/work/p/pjacka/Workspace/xAnalysis/"
fi
if [ $USER == jacka ] ; then
dir=${TtbarDiffCrossSection_output_path}
fi
if [ $USER == jpalicka ] ; then
dir=/afs/cern.ch/work/j/jpalicka/xAnalysis/
fi

if [ $USER == hejbal ] ; then
dir=${TtbarDiffCrossSection_output_path}
fi
root -l -q -b "${macroPath}DrawCutflow.C+g(\"${dir}\")"

cd -
