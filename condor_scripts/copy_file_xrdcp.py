import subprocess
import sys

def copy_file_xrdcp(file_path, destination, force=False):
    """
    Copy a file using xrdcp to the specified destination.

    Args:
        file_path (str): The file path to copy.
        destination (str): The directory or full file path to copy the file to.
        force (bool): If True, force the copy even if the destination file exists.
    """
    try:
        cmd = ["xrdcp"]
        if force:
            cmd.append("--force")
        file_path = file_path.strip()
        if file_path:
            cmd.extend([file_path, destination])
            subprocess.run(cmd, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as e:
        error_message = e.stderr.decode() if e.stderr is not None else "No error message available"
        print(f"Error copying file {file_path}: {error_message}")
        sys.exit(1)
