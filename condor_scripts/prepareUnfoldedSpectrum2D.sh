#!/bin/bash

variable=$3
level=$4

export TtbarDiffCrossSection_output_path=$1
export TTBAR_PATH=$2

### Hardcoded parameters

signal="410471"
mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/data/unfolding_histos2D_optimizedBinning.env
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
dimension="2D"
outputDir=systematics_histos2D
doSysPDF=1
writePseudoexperiments=0
useBootstrapHistos=0
nPseudoexperiments=100000
signalModelingWithMultijetShifted=1
seed=42
debugLevel=0

systematicHistosPath="systematics_histos2D"
generatorSystematicsDir="generatorSystematics2D"
generatorSysDir="${generatorSystematicsDir}/genSystematics_using_signal_${signal}"
pseudoexperimentsDir="pseudoexperiments2D"
bootstrapDir="bootstrap2D"
covariancesDir="covariances2D"

save_bootstrapHistos=0
save_stressTestHistos=0
doRebin=0

shiftNominal=0

AccEffDir="/AccEffPlots2D"
recoPlotsSubdir="RecoPlots2D"
logScales=(0 1)

normalizeSignal=1


cmd="PrepareSystematicHistos2D --mainDir $mainDir --config $config_lumi --configFiles $config_files --configBinning $config_binning --variable $variable --level $level --outputDir $systematicHistosPath --saveBootstrapHistos $save_bootstrapHistos --saveStressTestHistos $save_stressTestHistos --doRebin $doRebin --debugLevel $debugLevel"
echo "running $cmd" 
$cmd
rc=$?
echo "PrepareSystematicHistos2D ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

cmd="runGeneratorSystematics --mainDir ${mainDir}/ --config $config_lumi --signal $signal --variable $variable --level $level --inputDir $systematicHistosPath --outputDir ${generatorSystematicsDir} --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"
echo "running $cmd" 
$cmd
rc=$?
echo "runGeneratorSystematics ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

cmd="ProducePseudoexperiments --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --inputDir $systematicHistosPath --generatorSysDir ${generatorSysDir} --pseudoexperimentsDir ${pseudoexperimentsDir} --bootstrapDir ${bootstrapDir} --outputDir ${covariancesDir} --writePseudoexperiments $writePseudoexperiments --useBootstrapHistos $useBootstrapHistos --nPseudoexperiments $nPseudoexperiments --signalModelingWithMultijetShifted ${signalModelingWithMultijetShifted} --seed $seed --debugLevel $debugLevel"
echo "running $cmd"  
$cmd 
rc=$?
echo "ProducePseudoexperiments ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

plotsDir="sys_errors_summary_plots2D"
systematicUncertaintiesDir="SystematicUncertainties2D"

cmd="CalculateAsymmetricErrors --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --plotsDir ${plotsDir} --systematicUncertaintiesDir ${systematicUncertaintiesDir} --covariancesDir ${covariancesDir} --systematicHistosDir ${systematicHistosPath} --variable $variable --level $level --dimension $dimension --debugLevel $debugLevel"
echo "running $cmd"  
$cmd 
rc=$?
echo "CalculateAsymmetricErrors ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

if [[ $level == "ParticleLevel" ]]
then
  for logScale in ${logScales[@]}
  do
    cmd="drawReco2D --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${recoPlotsSubdir} --config ${config_lumi} --useLogScale ${logScale} --variable ${variable} --normalizeSignal ${normalizeSignal} --debugLevel ${debugLevel}"
    echo "running $cmd"
    $cmd
    rc=$?
    echo "drawReco2D ended with rc=$rc"
    if [[ $rc != 0 ]]; then exit $rc; fi
  done
fi

AccEffDir="/AccEffPlots2D"


cmd="CompareAccEff ${config_lumi} ${systematicHistosPath} ${AccEffDir} ${variable} ${level} ${dimension}"
echo "running $cmd"
$cmd
rc=$?
echo "CompareAccEff ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

if [[ ${save_stressTestHistos} != 0 ]]
then
  cmd="DoStressTest --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --stressTestHistosDir ${systematicHistosPath}"
  echo "running $cmd"
  $cmd
  rc=$?
  echo "DoStressTest ended with rc=$rc"
  if [[ $rc != 0 ]]; then exit $rc; fi
fi


exit $rc

