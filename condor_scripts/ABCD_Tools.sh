#!/bin/bash
cd ${TTBAR_PATH}

#defaults

config_file_name=NtuplesAnalysisTools/ABCDTools/data/Config.env
lumi_file_name=TtbarDiffCrossSection/data/config.json
doTests=1
doSys=1

batchDir=${TtbarDiffCrossSection_output_path}/batch/ABCD/
memory=2000
email=$TTBAR_EMAIL

debugLevel=0

samplesProd=( "All")
#samplesProd=( "MC16a")
#samplesProd=( "MC16a" "MC16d")
#samplesProd=("MC16a" "MC16d" "MC16e" "All")
#samplesProd=("MC16a" "MC16d" "MC16e")

buildDir=${TTBAR_PATH}/../build
jobDir=${TTBAR_PATH}
athena="${TTBAR_ANALYSIS_BASE}"
release='21.2'
version="${TTBAR_ANALYSIS_BASE_VERSION}"


simpleJobCommand="./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh -b ${buildDir} -j ${jobDir} -a ${athena} -r ${release} -v ${version} "



for MC in ${samplesProd[@]}
do
  if [ $MC == MC16a ] ; then
    data=data1516
  elif [ $MC == MC16d ] ; then
    data=data17
  elif [ $MC == MC16e ] ; then
    data=data18
  else
    data=AllYears
  fi
  
  binning_file_name=TtbarDiffCrossSection/data/unfolding_histos_optimizedBinning.env
  filelist_name=NtuplesAnalysisTools/ABCDTools/data/filelist.${data}.${MC}.txt
  outputDir=ABCD.${MC}
  echo "Your output directory is: "${outputDir}
  #./TtbarDiffCrossSection/condor_scripts/simpleJob.py ${batchDir} ${memory} ${email} ABCD.${data}.${MC} produce_ABCD_estimates --mainDir ${TtbarDiffCrossSection_output_path} --outputDir ${outputDir} --config ${config_file_name} --configLumi ${lumi_file_name} --fileList ${filelist_name} --doTests ${doTests} --doSys ${doSys} --debugLevel ${debugLevel}
  ./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJob.py ${batchDir} ${memory} ${email} ABCD.${data}.${MC} ${simpleJobCommand} -c produce_ABCD_estimates --mainDir ${TtbarDiffCrossSection_output_path} --outputDir ${outputDir} --config ${config_file_name} --configLumi ${lumi_file_name} --fileList ${filelist_name} --doTests ${doTests} --doSys ${doSys} --debugLevel ${debugLevel}
  #produce_ABCD_estimates --mainDir ${TtbarDiffCrossSection_output_path} --outputDir ${outputDir} --config ${config_file_name} --configLumi ${lumi_file_name} --fileList ${filelist_name} --doTests ${doTests} --doSys ${doSys} --debugLevel ${debugLevel}
  
done


