#!/bin/env python                                                                                                                                            

import os
import sys
import commands
import math
import getopt

sys.path.insert(0,os.getenv("TTBAR_PATH") + '/NtuplesAnalysisTools') 

from CondorJobsSubmissionHelpers import CondorSubFileMaker  
from CondorJobsSubmissionHelpers import CondorDagmanFileMaker    

def submitJob(force,suppressNotification):
    
    
    subFileMaker = CondorSubFileMaker()
    dagFileMaker = CondorDagmanFileMaker()
    
    memory='4000'
    runtime='20*60' # 20 min
    useProxy='False'
    maxRetries='0'
    maxJobs='20'
    

    baseDir   = os.getenv("TTBAR_PATH")
    outputDir = os.getenv("TtbarDiffCrossSection_output_path")
    email = os.getenv("TTBAR_EMAIL")
    analysisBaseVersion = os.getenv("TTBAR_ANALYSIS_BASE_VERSION")


    subFileMaker.request_memory=memory
    subFileMaker.MaxRunTime=runtime
    subFileMaker.use_x509userproxy=useProxy
    subFileMaker.notify_user=email
    subFileMaker.executable = baseDir + '/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh '

    ### Simple job configuration
    
    buildDir=baseDir+'/../build'
    athena= os.getenv("TTBAR_ANALYSIS_BASE")  
    release='21.2'        
    version=analysisBaseVersion         
    ldpath=baseDir+'/RooUnfold'
    
    simpleJobConfiguration = '-b ' + buildDir + ' -a ' + athena + ' -r ' +release + ' -v ' + version + ' -l ' + ldpath
    ### ------------------------------------------------


    #here will go the logs from submitted jobs
    batchDir=os.path.join(outputDir,"batch/drawAll")
    logsDir = os.path.join(batchDir,"logs")
    rc,out = commands.getstatusoutput("mkdir -p " + logsDir)
    

    dagFileName=batchDir+'/run_drawAll.dag'
    dagFile = open(dagFileName,'w')
    
    # qcds = ["ABCD16_BBB_S9_tight", "ABCD16_BBB_S1_tight", "ABCD16_BBB_ljet1_tau32_tight", "ABCD16_BBB_ljet2_tau32_tight", "ABCD16_BBB_bjets_tight", "ABCD16_BBB_bjet1_tight", "ABCD16_BBB_regionK", "ABCD16_BBB_regionL", "ABCD16_BBB_regionM", "ABCD16_BBB_regionN", "ABCD16_BBB_regionKS", "ABCD16_BBB_regionLS", "ABCD16_BBB_regionMS", "ABCD16_BBB_regionNS", "ABCD16_BBB_regionLS_ljet1_substructure", "ABCD16_BBB_regionNS_ljet2_substructure"]
    # mcProductions = ["MC16a", "MC16d", "MC16e", "All"]
    # signalSamples = ["nominal", "PS", "ME", "FSR_up", "FSR_down","ALPHAS_FSR_up", "ALPHAS_FSR_down"]
    # normalizations = ["0","1"]
    
    qcds = ["ABCD16_BBB_S9_tight"]
    mcProductions = ["All"]
    signalSamples = ["nominal"]
    normalizations = ["1"]
    
    doSys="1"
    use_lumi="0"
    doRebin="0"
    use_ttbar_SF="0"
    debugLevel="0"
    configsPath=baseDir+"/TtbarDiffCrossSection/data"
    
    
    for mc_prod in mcProductions:
      for signal in signalSamples:
	for qcd in qcds:
	  for doNorm in normalizations:
	  	    
	    doSys2=doSys
	    if signal != "nominal":
	      doSys2="0"
	    command = "drawAll"
	    params = "--mainDir %s --configsPath %s --qcd %s --signal %s --mcProd %s --doSys %s --doNorm %s --useLumi %s --doRebin %s --useTTBarSF %s --debugLevel %s" %(outputDir,configsPath,qcd,signal,mc_prod,doSys2,doNorm,use_lumi,doRebin,use_ttbar_SF,debugLevel)
	    
	    jobName="drawAll."+qcd
	    output = logsDir + "/" + jobName
	    
	    subFileMaker.logsDir=output
	    subFileMaker.arguments=simpleJobConfiguration + ' -c ' + command + ' ' + params 
	    
	    submitFileName=batchDir+"/run_drawAll_"+mc_prod+"_"+signal+"_"+qcd+"_DoNorm"+doNorm+".sub"
	    
	    subFileMaker.makeSubFile(submitFileName)
	    dagFileMaker.addJob('DRAWALL_'+mc_prod+"_"+signal+"_"+qcd+"_DoNorm"+doNorm,submitFileName,maxRetries)
	    
	    
    dagFileMaker.makeDagmanFile(dagFileName)

    additionalParameters=""
    if force==1:
      additionalParameters+=" -force"
    if suppressNotification==0:
      additionalParameters+=" -dont_suppress_notification"

    submCmd="condor_submit_dag -maxjobs " + maxJobs + additionalParameters + " -notification Complete -append \'notify_user = "  + email + "\' -outfile_dir " + logsDir + " %s" % (dagFileName) 
    print submCmd
    
    nJobs= len(mcProductions)*len(signalSamples)*len(qcds)*len(normalizations)
    
    rc=0
    rc,out = commands.getstatusoutput(submCmd)
    print "task with ", nJobs, " jobs submitted, rc= ",rc
    
    print "submission output: ",out

    

if __name__=="__main__":

    arguments = sys.argv[1:]
    
    # Default values
    force=0
    suppressNotification=0
    
    # Reading parameters
    try:
      opts, args = getopt.getopt(arguments,"hfs",["force,suppress_notification"])
    except getopt.GetoptError:
      print 'Error: Invalid parameters'
      print 'Usage of DrawAllCondor.py:'
      print '-f (--force)','optional parameter to tell dagman to overwrite results'
      print '-s (--suppress_notification)','optional parameter to tell to control notifications from subjobs, notifications are not suppressed by default'  
      sys.exit(2)
    
    for opt, arg in opts:
      if opt == '-h':
	print 'Usage of DrawAllCondor.py:'
	print '-f (--force)','optional parameter to tell dagman to overwrite results'
	print '-s (--suppress_notification)','optional parameter to tell to control notifications from subjobs, notifications are not suppressed by default'
      if opt in ('-f','--force'):
	print 'Using force to overwrite previous results (if exists)'
	force=1
      if opt in ('-s','--suppress_notification'):
	print 'Notifications from individual jobs will be suppressed'
	suppressNotification=1
    
    submitJob(force,suppressNotification)
