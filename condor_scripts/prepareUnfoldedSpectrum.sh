#!/bin/bash

mainDir=$1
ttbarPath=$2
variable=$3
level=$4

### Hardcoded parameters

export TtbarDiffCrossSection_output_path=$mainDir
export TTBAR_PATH=$ttbarPath

signal="410471"
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/data/unfolding_histos_optimizedBinning.env
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
dimension="1D"
doSysPDF=1
writePseudoexperiments=0
useBootstrapHistos=0
nPseudoexperiments=100000
debugLevel=0

seed=42

systematicHistosPath="systematics_histos"
generatorSystematicsDir="generatorSystematics"
generatorSysDir="${generatorSystematicsDir}/genSystematics_using_signal_${signal}"
pseudoexperimentsDir="pseudoexperiments"
bootstrapDir="bootstrap"
covariancesDir="covariances"

save_bootstrapHistos=0
save_stressTestHistos=1
doRebin=1

shiftNominal=0

normalizeSignal=1

recoPlotsSubdir=RecoPlots
AccEffDir="/AccEffPlots"

echo $mainDir

cmd="PrepareSystematicHistos --mainDir $mainDir --config $config_lumi --configFiles $config_files --configBinning $config_binning --variable $variable --level $level --outputDir $systematicHistosPath --saveBootstrapHistos $save_bootstrapHistos --saveStressTestHistos $save_stressTestHistos --doRebin $doRebin"
echo "running $cmd" 
$cmd
rc=$?
echo "PrepareSystematicHistos ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

cmd="runGeneratorSystematics --mainDir ${mainDir}/ --config $config_lumi --signal $signal --variable $variable --level $level --inputDir $systematicHistosPath --outputDir ${generatorSystematicsDir} --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"

echo "running $cmd" 
$cmd
rc=$?
echo "runGeneratorSystematics ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

cmd="ProducePseudoexperiments --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --inputDir $systematicHistosPath --generatorSysDir ${generatorSysDir} --pseudoexperimentsDir ${pseudoexperimentsDir} --bootstrapDir ${bootstrapDir} --outputDir ${covariancesDir} --writePseudoexperiments $writePseudoexperiments --useBootstrapHistos $useBootstrapHistos --nPseudoexperiments $nPseudoexperiments --seed $seed --debugLevel $debugLevel"
echo "running $cmd"  
$cmd 
rc=$?
echo "ProducePseudoexperiments ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi


plotsDir="sys_errors_summary_plots"
systematicUncertaintiesDir="SystematicUncertainties"

cmd="CalculateAsymmetricErrors --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --plotsDir ${plotsDir} --systematicUncertaintiesDir ${systematicUncertaintiesDir} --covariancesDir ${covariancesDir} --systematicHistosDir ${systematicHistosPath} --variable $variable --level $level --dimension $dimension --debugLevel $debugLevel"

echo "running $cmd"  
$cmd 
rc=$?
echo "CalculateAsymmetricErrors ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

if [[ $level == "ParticleLevel" ]]
then
  cmd="drawReco --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${recoPlotsSubdir} --config ${config_lumi} --variable $variable --normalizeSignal ${normalizeSignal} --debugLevel ${debugLevel}"
  echo "running $cmd"
  $cmd
  rc=$?
  echo "drawReco ended with rc=$rc"
  if [[ $rc != 0 ]]; then exit $rc; fi
fi



cmd="CompareAccEff ${config_lumi} ${systematicHistosPath} ${AccEffDir} ${variable} ${level} ${dimension}"
echo "running $cmd"
$cmd
rc=$?
echo "CompareAccEff ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

migrationMatricesOutputSubdir=migration

cmd="drawMigrationMatrix --mainDir ${mainDir} --config ${config_lumi} --outputSubDir ${migrationMatricesOutputSubdir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawMigrationMatrix ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi


if [[ ${save_stressTestHistos} != 0 ]]
then
  cmd="DoStressTest --variable ${variable} --level ${level} --config ${config_lumi}"
  echo "running $cmd"
  $cmd
  rc=$?
  echo "DoStressTest ended with rc=$rc"
  if [[ $rc != 0 ]]; then exit $rc; fi
fi


cmd="runClosureTest --variable ${variable} --level ${level} --dimension ${dimension} --closureTestHistosDir ${systematicHistosPath} --config ${config_lumi}"
echo "running $cmd"
$cmd
rc=$?
echo "runClosureTest ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi


outputSubdir=TheoryVsDataComparison
covariancesDir="covariances"

covStatName="covDataBasedOnlyDataStat"

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
covAllUncName="covDataBasedStatSysAndSigMod"
outputSubDir="TheoryVsDataComparison_newModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi}  --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
covAllUncName="covDataBasedAllUnc"
outputSubDir="TheoryVsDatacomparison_oldModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi}  --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

## This is for alternative method to propagate systematic ucertainties through unfolding. Unfolding corrections and MC background are shifted
covAllUncName="covDataBasedAllUncAlternative"
outputSubDir="TheoryVsDataComparison_alternative_oldModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi}  --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"

echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

covAllUncName="covDataBasedStatSysAndSigModAlternative"
outputSubDir="TheoryVsDataComparison_alternative_newModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi} --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi






exit $rc

