#!/bin/bash

###### To print all availabe options:
## ./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -h

########## Recommended to split into 2 runs as indicated. Once the first reun is finished submit the second. The second run is faster. Third run is for reweighted samples. They are required only for special studies. 
overwrite=0
doSys=1
doSysPDF=1
doSysFSR=1
doSysISR=1
bootstrap=0
doReweighting=0

## 1. Signal nominal samples ###############################################
./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s signal -p ${doSysPDF} -y ${doSys} -a ${doSysFSR} -f ${doSysISR} -r ${doReweighting} -b ${bootstrap} -o ${overwrite}
## 2. Data, signal modeling samples and MC background ################################
./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s data -b ${bootstrap}  -y ${doSys} -o ${overwrite}
#./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s generatorSysSignal -b ${bootstrap} -o ${overwrite}
#./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s ISRSysSignal -f ${doSysISR} -b ${bootstrap} -o ${overwrite}
./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s nonallhad -y ${doSys} -b ${bootstrap} -o ${overwrite}
./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s ttHWZ -y ${doSys} -b ${bootstrap} -o ${overwrite}
./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s Wt -y ${doSys} -b ${bootstrap} -o ${overwrite}
#./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s tChannel -y ${doSys} -b ${bootstrap} -o ${overwrite}
###################################################################

# run qcd samples (only for special studies)
#./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s QCD_LO -y 0 -b ${bootstrap}

# EFT samples
#ttbarConfig=${TTBAR_PATH}/TtbarDiffCrossSection/data/config_truth.env
#./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh -s EFT -y 0 -p 1 -b ${bootstrap} -o ${overwrite} -c ${ttbarConfig}
