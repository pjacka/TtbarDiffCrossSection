#!/bin/bash

mainDir=$1
ttbarPath=$2
variable=$3
level=$4

### Hardcoded parameters

export TtbarDiffCrossSection_output_path=$mainDir
export TTBAR_PATH=$ttbarPath

signal="410471"
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
configBinning=${TTBAR_PATH}/TtbarDiffCrossSection/data/optBinningND/${variable}.env
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
dimension="ND"
doSysPDF=1
writePseudoexperiments=0
useBootstrapHistos=0
nPseudoexperiments=10000
debugLevel=0

seed=42

systematicHistosPath="systematics_histosND"
stressTestHistosDir="systematics_histosND"
generatorSystematicsDir="generatorSystematicsND"
generatorSysDir="${generatorSystematicsDir}/genSystematics_using_signal_${signal}"
pseudoexperimentsDir="pseudoexperimentsND"
bootstrapDir="bootstrapND"
covariancesDir="covariancesND"

save_bootstrapHistos=0
save_stressTestHistos=1
doRebin=1

shiftNominal=0

normalizeSignal=1

recoPlotsSubdir=RecoPlotsND
AccEffDir="/AccEffPlotsND"

echo $mainDir

cmd="PrepareSystematicHistosND --mainDir $mainDir --config $config_lumi --configFiles $config_files --configBinning $configBinning --variable $variable --level $level --outputDir $systematicHistosPath --saveBootstrapHistos $save_bootstrapHistos --saveStressTestHistos $save_stressTestHistos --doRebin $doRebin"
echo "running $cmd" 
$cmd
rc=$?
echo "PrepareSystematicHistos ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

cmd="runGeneratorSystematics --mainDir ${mainDir}/ --config $config_lumi --signal $signal --variable $variable --level $level --inputDir $systematicHistosPath --outputDir ${generatorSystematicsDir} --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"

echo "running $cmd" 
$cmd
rc=$?
echo "runGeneratorSystematics ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi


cmd="ProducePseudoexperiments --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --inputDir $systematicHistosPath --generatorSysDir ${generatorSysDir} --pseudoexperimentsDir ${pseudoexperimentsDir} --bootstrapDir ${bootstrapDir} --outputDir ${covariancesDir} --writePseudoexperiments $writePseudoexperiments --useBootstrapHistos $useBootstrapHistos --nPseudoexperiments $nPseudoexperiments --seed $seed --debugLevel $debugLevel"
echo "running $cmd"  
$cmd 
rc=$?
echo "ProducePseudoexperiments ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi


plotsDir="sys_errors_summary_plotsND"
systematicUncertaintiesDir="SystematicUncertaintiesND"

cmd="CalculateAsymmetricErrors --mainDir ${mainDir} --config ${config_lumi}  --configBinning ${configBinning} --configSysNames $config_sysnames --plotsDir ${plotsDir} --systematicUncertaintiesDir ${systematicUncertaintiesDir} --covariancesDir ${covariancesDir} --systematicHistosDir ${systematicHistosPath} --variable $variable --level $level --dimension $dimension --debugLevel $debugLevel"

echo "running $cmd"  
$cmd 
rc=$?
echo "CalculateAsymmetricErrors ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

if [[ $level == "ParticleLevel" ]]
then
  drawRatio=1
  useLogScale=1
  cmd="drawRecoND --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${recoPlotsSubdir} --config ${config_lumi} --configBinning $configBinning --variable $variable --normalizeSignal ${normalizeSignal} --drawRatio ${drawRatio} --useLogScale ${useLogScale} --debugLevel ${debugLevel}"
  echo "running $cmd"
  $cmd
  rc=$?
  echo "drawReco ended with rc=$rc"
  if [[ $rc != 0 ]]; then exit $rc; fi
fi



cmd="CompareAccEff ${config_lumi} ${systematicHistosPath} ${AccEffDir} ${variable} ${level} ${dimension}"
echo "running $cmd"
$cmd
rc=$?
echo "CompareAccEff ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi


migrationMatricesOutputSubdir=migrationND

cmd="drawMigrationMatrix --mainDir ${mainDir} --config ${config_lumi} --configBinning ${configBinning} --outputSubDir ${migrationMatricesOutputSubdir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawMigrationMatrix ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi


if [[ ${save_stressTestHistos} != 0 ]]
then
  cmd="DoStressTest --variable ${variable} --level ${level} --config ${config_lumi} --dimension ${dimension} --configBinning ${configBinning} --systematicsHistosDir ${systematicHistosPath} --stressTestHistosDir ${stressTestHistosDir}"
  echo "running $cmd"
  $cmd
  rc=$?
  echo "DoStressTest ended with rc=$rc"
  if [[ $rc != 0 ]]; then exit $rc; fi
fi

cmd="runClosureTest --variable ${variable} --level ${level} --config ${config_lumi} --configBinning ${configBinning} --dimension ${dimension} --closureTestHistosDir ${systematicHistosPath}"
echo "running $cmd"
$cmd
rc=$?
echo "runClosureTest ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi


outputSubdir=TheoryVsDataComparisonND
covariancesDir="covariancesND"

covStatName="covDataBasedOnlyDataStat"

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
covAllUncName="covDataBasedStatSysAndSigMod"
outputSubDir="TheoryVsDataComparisonND_newModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi} --configBinning ${configBinning} --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
covAllUncName="covDataBasedAllUnc"
outputSubDir="TheoryVsDataComparisonND_oldModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi} --configBinning ${configBinning} --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

## This is for alternative method to propagate systematic ucertainties through unfolding. Unfolding corrections and MC background are shifted
covAllUncName="covDataBasedAllUncAlternative"
outputSubDir="TheoryVsDataComparisonND_alternative_oldModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi} --configBinning ${configBinning} --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"

echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi

covAllUncName="covDataBasedStatSysAndSigModAlternative"
outputSubDir="TheoryVsDataComparisonND_alternative_newModeling"

cmd="drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} --config ${config_lumi} --configBinning ${configBinning} --outputSubDir ${outputSubDir} --variable ${variable} --level ${level} --systematicsHistosDir ${systematicHistosPath} --dimension ${dimension} --covariancesDir ${covariancesDir} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}"
echo "running $cmd"
$cmd
rc=$?
echo "drawTheoryVsDataComparison ended with rc=$rc"
if [[ ${rc} != 0 ]]; then exit ${rc}; fi







exit $rc










exit $rc

