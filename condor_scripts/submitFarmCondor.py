#!/bin/env python

import os
import sys
import subprocess

sys.path.insert(0,os.getenv("TTBAR_PATH") + '/NtuplesAnalysisTools') 

from CondorJobsSubmissionHelpers import CondorSubFileMaker  
from CondorJobsSubmissionHelpers import CondorDagmanFileMaker  

def submitJob(filelistDir, sampleDir, email, configName, doSys, doSysPDF, doSysAlphaSFSR, doSysIFSR, doReweighting, doBootstrap, nEvents,overwrite):
  
    baseDir   = os.getenv("TTBAR_PATH")
    outputDir = os.getenv("TtbarDiffCrossSection_output_path")
    x509_proxy = os.getenv("X509_USER_PROXY")

    debug="1"
    buildDir=baseDir+'/../build'
  
    simpleJobConfiguration = '-b ' + buildDir + ' -e ' + "TTBAR_PATH=" + baseDir
    # Dagman will not run more than maxjobs jobs at the same time.
    # Set to low number if many running jobs cause problems.  
    maxjobs="10000" 
  
    postMergeScript=baseDir + '/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/postMerge.sh'
    checkFilesScript=baseDir + '/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/checkOutputFiles.sh'
  
  
    subFileMaker = CondorSubFileMaker()
    dagFileMaker = CondorDagmanFileMaker()
  
    #if doBootstrap > 0:
    #  Memory='16000'
    #else:
    #  Memory='1000'
    Memory='4000'
    Runtime='36*60*60' # 36 hours
    UseProxy='True'
    MaxRetries='1'
    Disk='20GB'
    DiskMerge='50GB'
    notification='Error'
    # notification="Complete"
  
    subFileMaker.request_memory=Memory
    subFileMaker.request_disk=Disk
    subFileMaker.MaxRunTime=Runtime
    subFileMaker.use_x509userproxy=UseProxy
    subFileMaker.notify_user=email
    subFileMaker.notification=notification
    subFileMaker.executable = baseDir + "/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh" # executable for producing histograms from ntuples
  
  
    if filelistDir.find("SPLITTED"): isSplitted=1
    else:                            isSplitted=0

    #here will go the logs from submitted jobs
    logsDir = os.path.join(outputDir,"batch/produce_hists/logs/"+sampleDir+'/')
    rc,out = subprocess.getstatusoutput("mkdir -p " + logsDir)
    rc,out = subprocess.getstatusoutput("mkdir -p " + outputDir+"/SPLITTED/"+sampleDir)
  
    rc,files = subprocess.getstatusoutput("ls " + baseDir+"/"+filelistDir+"/"+sampleDir)
    if files == '':
      print ("Error: Folder is empty")
      return 

    dagFileName=outputDir +'/batch/produce_hists/run_%s.dag'%(sampleDir)


    # Processing ntuples
    rootfilesFull = []
    rootfiles = []
    jobIDs = []

    pathSPLITTED = outputDir + '/SPLITTED/' + sampleDir + '/'

    i=0    
    for inputfile in files.split():
      
      # params = "%s %s %s %s %s %s %s %s %s %s %s %s %s" %(baseDir,outputDir,sampleDir,inputfile,configName,doSys,doSysPDF,doSysAlphaSFSR,doSysIFSR,doReweighting,doBootstrap,isSplitted,nEvents)
      
      rootfile=inputfile.replace('.txt_','.root_')
      params = ' '.join([
        simpleJobConfiguration,
        ' -c', os.path.join(baseDir, "TtbarDiffCrossSection/condor_scripts/produce_hists_farm.py"),
        "--filelist", os.path.join(baseDir,filelistDir,sampleDir,inputfile),
        "--outputFileName", rootfile,
        "--outputDestination", pathSPLITTED,
        "--config", configName,
        "--doDetSys", doSys,
        "--doSysPDF", doSysPDF,
        "--doSysAlphaSFSR", doSysAlphaSFSR,
        "--doSysIFSR", doSysIFSR,
        "--doReweighting", doReweighting,
        "--nBootstrap", doBootstrap,
        "--nEvents", nEvents,
        "--debug", debug
      ])

      output = logsDir + "/produce_hists"
      
      rootfileFull = pathSPLITTED + '/' + rootfile
      rootfilesFull.append(rootfileFull)
      rootfiles.append(rootfile)
      
      submitFileName = outputDir+'/batch/produce_hists/run_%s_filelist_%i.sub'%(sampleDir,i) 
      
      subFileMaker.arguments=params
      subFileMaker.logsDir=logsDir
    
      subFileMaker.makeSubFile(submitFileName)
        
      jobID = 'A' + str(i)
      jobIDs.append(jobID)
    
      dagFileMaker.addJob(jobID,submitFileName,MaxRetries)
      dagFileMaker.addPostScript(jobID,checkFilesScript + ' $RETURN ' + rootfileFull)
    
      i=i+1
    
    
    # Defining merge jobs parameters
    outputFile="allhad.boosted.output.root"
    outputPath=outputDir+"/"+sampleDir + "/"
    rc,out = subprocess.getstatusoutput("mkdir -p " + outputPath)
    command = 'mergeHistosFromRun'
  
    Runtime='7*24*60*60' # 7 days
    UseProxy='False'
    MaxRetries='1'
  
    # Definining number of files per merge job.
    if ("601237" in sampleDir):
      nFilesPerMerge = 8 # Smaller number is used for signal samples as files are large
    else:
      nFilesPerMerge = 20 # Large number is used for other files
    
    subFileMaker.MaxRunTime=Runtime
    subFileMaker.use_x509userproxy=UseProxy
    subFileMaker.disableTransferFiles()
    subFileMaker.executable=baseDir+"/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh" # Executable is the same for merge jobs.
  
  
  
    # Adding merge jobs into dagman files
    filesToMerge = rootfiles # List of input files
    parentsMerge = jobIDs # Job IDs to create input files
  
    iMerge = 1
    while len(filesToMerge) > 0:
    
      nFilesPerMerge = min(nFilesPerMerge,len(filesToMerge))
    
      inFiles =  filesToMerge[:nFilesPerMerge]
      parents = parentsMerge[:nFilesPerMerge]
    
      inFilesFull = [pathSPLITTED + '/' + f for f in inFiles]
    
      filesToMerge = filesToMerge[nFilesPerMerge:]
      parentsMerge = parentsMerge[nFilesPerMerge:]
    
      mergeID = 'MERGE_%i' %(iMerge)
      outPath = pathSPLITTED
      
    
      tmpFile = 'tmp_%i.root' %(iMerge)
      if len(filesToMerge) == 0:
        outPath = outputPath
        tmpFile = outputFile
     
      tmpFileFull = os.path.join(outPath,tmpFile)     
     
      if nFilesPerMerge > 1:
        command = os.path.join(baseDir, "TtbarDiffCrossSection/condor_scripts/merge_hists_farm.py")
        params =  tmpFileFull + ' ' + ' '.join(inFilesFull)
      else:
        command = 'cp'
        params = inFilesFull[0] + ' ' + tmpFileFull
       
      # subFileMaker.setInputFiles(inFilesFull)
      # subFileMaker.setOutputFilesRemaps([tmpFile],outPath)
      subFileMaker.request_disk=DiskMerge
       
      submitFileNameMerge = outputDir+'/batch/produce_hists/run_merge_%i_%s.sub'%(iMerge,sampleDir)
      jobName = 'mergeHistos_%i' %(iMerge)
      output = logsDir + "/" + jobName
      subFileMaker.arguments=simpleJobConfiguration + ' -c ' + command + ' ' + params
      subFileMaker.logsDir=output
      subFileMaker.makeSubFile(submitFileNameMerge)
       
      dagFileMaker.addJob(mergeID,submitFileNameMerge,MaxRetries,parents)
      dagFileMaker.addPostScript(mergeID,checkFilesScript + ' $RETURN ' + outPath + '/' + tmpFile)
      dagFileMaker.addPostScript(mergeID,postMergeScript + ' $RETURN ' + ' '.join(inFilesFull))
       
      filesToMerge.append(tmpFile)
      parentsMerge.append(mergeID)
       
      if len(filesToMerge) == 1:
        break
         
      iMerge = iMerge + 1
         
    dagFileMaker.makeDagmanFile(dagFileName) # Finalizing dagman file. File is written into dagFileName
  
  
    # Get number of input files --> number of jobs, merge jobs are not counted
    fullDir=os.path.join(filelistDir,sampleDir)
    rc,out=subprocess.getstatusoutput("ls %s" %(fullDir))
    nJobs=len(out.split())

    submCmd="condor_submit_dag"
    if overwrite=="1":
      submCmd+=" -f"
    submCmd+=" -insert_env X509_USER_PROXY=" + x509_proxy + " -dont_suppress_notification -maxjobs " + maxjobs + " -notification Complete -append \'notify_user = "  + email + "\' -outfile_dir " + logsDir + " %s" % (dagFileName)
    
    # Submitting condor dagman job
    print (submCmd)
    rc,out = subprocess.getstatusoutput(submCmd)
    print ("task with ", nJobs, " jobs submitted, rc= ",rc)
    print ("submission output: ",out)
  

if __name__=="__main__":


    filelistDir    = sys.argv[1]
    sampleDir      = sys.argv[2]
    email          = sys.argv[3]
    configName     = sys.argv[4]
    doSys          = sys.argv[5]
    doSysPDF       = sys.argv[6]
    doSysAlphaSFSR = sys.argv[7]
    doSysIFSR      = sys.argv[8]
    doReweighting  = sys.argv[9]
    doBootstrap    = sys.argv[10]
    nEvents        = sys.argv[11]
    overwrite      = sys.argv[12]

    submitJob(filelistDir, sampleDir, email, configName, doSys, doSysPDF, doSysAlphaSFSR, doSysIFSR, doReweighting, doBootstrap, nEvents, overwrite)



