batchDir=${TtbarDiffCrossSection_output_path}/batch/filterEfficiencyCorrections/
memory=2000
email=$TTBAR_EMAIL
debugLevel=1

buildDir=${TTBAR_PATH}/../build
jobDir=${TTBAR_PATH}
athena="${TTBAR_ANALYSIS_BASE}"
release='21.2'
version="${TTBAR_ANALYSIS_BASE_VERSION}"

simpleJobCommand="./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh -b ${buildDir} -j ${jobDir} -a ${athena} -r ${release} -v ${version} -c makeFilterEfficiencyCorrections "



mainDir=${TtbarDiffCrossSection_output_path}

outputDir=${mainDir}/root.All/filterEfficiencyCorrections

mkdir -p ${outputDir}






fileListDir=${TTBAR_PATH}/TtbarDiffCrossSection/filelists
config=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
debugLevel=2

options="--mainDir ${mainDir} --config ${config} --fileListDir ${fileListDir} --outputFile ${outputDir}/filterEfficiencies.root --debugLevel ${debugLevel}"

cmd="${simpleJobCommand} ${options}"
echo "Running command: $cmd"

./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJobWithProxy.py $batchDir $memory $email filterEff ${cmd} 

