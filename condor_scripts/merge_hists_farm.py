#!/bin/env python
"""The script merges files with histograms on farm
It is a wrapper around mergeHistosFromRun.

Usage:  
  ./merge_hists_farm.py targetfile source1 source2 [source3 ...]
"""

import os
import sys
import subprocess
import shutil
sys.path.append(os.path.join(os.getenv("TTBAR_PATH"), 'TtbarDiffCrossSection/python'))
import glob
from copy_file_xrdcp import copy_file_xrdcp

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Usage: merge_hists_farm.py <target_file> <input_file1> <input_file2> ...")
        sys.exit(1)

    target_file = sys.argv[1]
    input_files = sys.argv[2:]

    print(f"Target file: {target_file}")
    print(f"Input files: {input_files}")

    # Create tmp directory if it doesn't exist
    tmp_dir = "tmp"
    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)


    # Copy input files to tmp directory
    for i, input_file in enumerate(input_files):
        unique_name = f"file_{i}.root"
        destination = os.path.join(tmp_dir, unique_name)
        copy_file_xrdcp(input_file, destination)


    tmp_output_file = os.path.join(tmp_dir, "output.root")

    subprocess.run(["mergeHistosFromRun", tmp_output_file] + glob.glob(os.path.join(tmp_dir, "*.root")), check=True)

    copy_file_xrdcp(tmp_output_file, target_file, force=True)

    # Clean up tmp directory
    shutil.rmtree(tmp_dir)


