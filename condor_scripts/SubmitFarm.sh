#!/bin/bash

#=========================================
#
# examples of running:
#   ./TtbarDiffCrossSection/scripts/SubmitFarm.sh             #runs over all samples
#   ./TtbarDiffCrossSection/scripts/SubmitFarm.sh -s default  #runs over default samples
#   ./TtbarDiffCrossSection/scripts/SubmitFarm.sh -i 410234   #runs only over sample ID 410234
#
#=========================================

#constants
signal_reference="601237"
data_reference="data22"
filelistDir=TtbarDiffCrossSection/filelists/SPLITTED

#defaults:
samples="_"  #options: all nonallhad Wt QCD QCD_LO signal data1516 reweighted1 reweighted2
sampleIDs=""
email=${TTBAR_EMAIL}
configName=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
doDetSys=0
doSysPDF=0
doSysAlphaSFSR=0
doSysIFSR=0
doReweighting=0
doBootstrap=0 # Set to number of pseudo-experiments if you do Bootstrapping.
overwrite=0
nEvents=-1
	
MCsamples=( "MC23d" "MC23a")
# MCsamples=( "MC23a" )


USAGE="
Usage: SubmitFarm.sh [Options]

Options:
  -s <samples>            Run jobs for specific set of samples. [example: -s signal will submit jobs for all signal samples]
  
  -i <samplesID>          To select an exact list of DSIDs. -s option is ignored if -i is setup.
  
  -e <email>              Email for job notifications. [default: \${TTBAR_EMAIL}].        
     
  -y <doDetSys>           Run jobs with detector systematics included. [default: 0].
  
  -p <doPDFSys>           Run jobs with PDF systematics included. [default: 0].
  
  -a <doAlphaSFSRSys>     Run jobs with AlphaS FSR systematics included. [default: 0].
  
  -f <doISRSys>           Run jobs with ISR systematics included. [default: 0].
  
  -r <doReweighting>      Run jobs with reweighting for stress test included. [default: 0].
         
  -b <doBootstrap>        Bootstrap histograms will be created with <doBootstrap> replicas. [default: 0]. 
  
  -n <nEvents>            Set number of events for job. [default: -1].
  
  -o <overwrite>          Overwrite results from previous run. [default: 0]
  
  -c <configPath>         Path to ttbarConfig file. [default: \${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env]
    
  -h                      Print this message.
"


while getopts :s:i:e:y:p:a:f:r:b:n:o:c:h OPT; do
    case $OPT in
        s)    samples=$OPTARG               ;;
        i)    sampleIDs=$OPTARG; samples=""   ;;
        e)    email=$OPTARG               ;;            
        y)    doDetSys=$OPTARG               ;;
        p)    doSysPDF=$OPTARG               ;;
        a)    doSysAlphaSFSR=$OPTARG               ;;
        f)    doSysIFSR=$OPTARG               ;;
        r)    doReweighting=$OPTARG               ;;
        b)    doBootstrap=$OPTARG           ;;
        n)    nEvents=$OPTARG               ;;
        o)    overwrite=$OPTARG   ;;
        c)    configName=$OPTARG   ;;
        h)  echo "${USAGE}"; exit 0 ;;
        *)  echo "SubmitFarm.sh: unknown option -$OPTARG, bailing out.${USAGE}";
            exit -1;
            ;;
    esac
done


if [ ".$samples" != "."     ]; then echo "running on samples: $samples"
else                                echo "running on dataset IDs: $sampleIDs"; fi


if [ "$samples" == "default" ]; then sampleIDs="${signal_reference} ${data_reference} 601229 601230 601352 601355 601627 601631 601352 601355 601627 601631 522036 522040 602637 602638 700995 700996 700997 data22 data23"; fi
if [ "$samples" == "signal"  ]; then sampleIDs="601237"; fi
if [ "$samples" == "nonallhad"  ]; then sampleIDs="601229 601230"; fi
if [ "$samples" == "Wt"      ]; then sampleIDs="601352 601355 601627 601631"; fi 
if [ "$samples" == "tChan"      ]; then sampleIDs=""; fi 
if [ "$samples" == "ttHWZ"      ]; then sampleIDs="602637 602638 700995 700996 700997 522036 522040"; fi
if [ "$samples" == "data"    ]; then sampleIDs="data22 data23"; fi

echo "this corresponds to dataset IDs: ${sampleIDs}"

echo "running with options: email=$email doDetSys=$doDetSys doSysPDF=$doSysPDF doSysAlphaSFSR=$doSysAlphaSFSR doSysIFSR=$doSysIFSR doReweighting=$doReweighting nEvents=$nEvents overwrite=$overwrite" 

dirs=""

for MC in ${MCsamples[@]}; do

  #if [ "$samples" == "signal" ] && [ "$MC" == "MC16a" ]; then sampleIDs="601237"; fi
  #if [ "$samples" == "signal" ] && [ "$MC" != "MC16a" ]; then sampleIDs="601237"; fi

#                                                   #sample direcotory
  #Signal
  if [[ $sampleIDs == *"601237"* ]]; then dirs+=" ${MC}.601237.PhPy8EG "; fi
  
  #EFT

  #Signal non-allhad
  if [[ $sampleIDs == *"601229"* ]]; then dirs+=" ${MC}.601229.PhPy8EG  "; fi
  if [[ $sampleIDs == *"601230"* ]]; then dirs+=" ${MC}.601230.PhPy8EG  "; fi
  
  #Wt
  if [[ $sampleIDs == *"601352"* ]]; then dirs+=" ${MC}.601352.PhPy8EG  "; fi
  if [[ $sampleIDs == *"601355"* ]]; then dirs+=" ${MC}.601355.PhPy8EG  "; fi
  if [[ $sampleIDs == *"601627"* ]]; then dirs+=" ${MC}.601627.PhPy8EG  "; fi
  if [[ $sampleIDs == *"601631"* ]]; then dirs+=" ${MC}.601631.PhPy8EG  "; fi
  
  # Single-top t-channel - no samples at the moment  
  
  #ttHWZ
  if [[ $sampleIDs == *"522036"* ]]; then dirs+=" ${MC}.522036.aMCPy8EG  "; fi  # ttZqq
  if [[ $sampleIDs == *"522040"* ]]; then dirs+=" ${MC}.522040.aMCPy8EG  "; fi  # ttZnunu
  if [[ $sampleIDs == *"602637"* ]]; then dirs+=" ${MC}.602637.PhPy8EG  ";  fi
  if [[ $sampleIDs == *"602638"* ]]; then dirs+=" ${MC}.602638.PhPy8EG  ";  fi
  if [[ $sampleIDs == *"700995"* ]]; then dirs+=" ${MC}.700995.Sh_2214_ttW_0Lfilter  ";  fi
  if [[ $sampleIDs == *"700996"* ]]; then dirs+=" ${MC}.700996.Sh_2214_ttW_1Lfilter  ";  fi
  if [[ $sampleIDs == *"700997"* ]]; then dirs+=" ${MC}.700997.Sh_2214_ttW_2Lfilter  ";  fi
  #WZ+jets
  #QCD
  #QCD_LO
 
done

#DATA
if [[ $sampleIDs == *"data22"*    ]]; then dirs+=" data2022_AllYear.physics_Main    ";  fi
if [[ $sampleIDs == *"data23"*    ]]; then dirs+=" data2023_AllYear.physics_Main    ";  fi


nDirs=`echo $dirs|wc -w`
for iDir in `seq $nDirs`; do
       dir=`echo $dirs         | cut -f $iDir -d" "`
    echo "running with parameters: $dir $email $doDetSys $doSysPDF $doSysAlphaSFSR $doSysIFSR $doBootstrap $nEvents $overwrite"
    
    python ${TTBAR_PATH}/TtbarDiffCrossSection/condor_scripts/submitFarmCondor.py $filelistDir $dir $email $configName $doDetSys $doSysPDF $doSysAlphaSFSR $doSysIFSR $doReweighting $doBootstrap $nEvents $overwrite
done

