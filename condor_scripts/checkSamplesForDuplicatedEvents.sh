#!/bin/bash

batchDir=${TtbarDiffCrossSection_output_path}/batch/checkForDuplicates/
memory=16000
email=$TTBAR_EMAIL
debugLevel=1
mainDir=${TtbarDiffCrossSection_output_path}
fileListsDir=${TTBAR_PATH}/TtbarDiffCrossSection/filelists


buildDir=${TTBAR_PATH}/../build
jobDir=${TTBAR_PATH}
athena="${TTBAR_ANALYSIS_BASE}"
release='21.2'
version="${TTBAR_ANALYSIS_BASE_VERSION}"

simpleJobCommand="./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh -b ${buildDir} -j ${jobDir} -a ${athena} -r ${release} -v ${version} -c DuplicatedEventsChecker "


mc_productions=(MC16a MC16d MC16e)
signalSamples=( 
  410471.PhPy8EG
  410481.PhPy8EG
  410559.PowhegHerwig7EvtGen
  410466.aMcAtNloPy8EvtGen
  410429.PhPy8EG
  410431.PhPy8EG
  410433.PowhegHerwig7EvtGen
  410435.aMcAtNloPy8EvtGen
  410444.PhPy8EG
  410445.PhPy8EG
  410446.PowhegHerwig7EvtGen
  410447.aMcAtNloPythia8EvtGen
)

backgroundSamples=(
  346343.PhPy8EG
  346344.PhPy8EG
  346345.PhPy8EG
  410155.aMcAtNloPythia8EvtGen
  410156.aMcAtNloPythia8EvtGen
  410157.aMcAtNloPythia8EvtGen
  410470.PhPy8EG
  410646.PowhegPythia8EvtGen
  410647.PowhegPythia8EvtGen
)

data=(
  data2015_AllYear.physics_Main
  data2016_AllYear.physics_Main
  data2017_AllYear.physics_Main
  data2018_AllYear.physics_Main
)

#for MC in ${mc_productions[@]}
#do
  #for sample in ${signalSamples[@]}
  #do
    #./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJobWithProxy.py $batchDir $memory $email checkForDuplicates.${MC}.${sample} ${simpleJobCommand} --treeName truth --physicsVariables "filter_HT;MC_t_afterFSR_pt;MC_t_afterFSR_eta" --sampleName ${MC}.${sample} --mainDir ${mainDir} --fileListsDir ${fileListsDir} --debugLevel ${debugLevel}
    ##./TtbarDiffCrossSection/condor_scripts/simpleJob.py $batchDir $memory $email checkForDuplicates.${MC}.${sample} DuplicatedEventsChecker --treeName truth --physicsVariables "filter_HT;MC_t_afterFSR_pt;MC_t_afterFSR_eta" --sampleName ${MC}.${sample} --debugLevel ${debugLevel}
    ##echo DuplicatedEventsChecker --treeName truth --physicsVariables "filter_HT;MC_t_afterFSR_pt;MC_t_afterFSR_eta" --sampleName ${MC}.${sample}
  #done
#done


#for MC in ${mc_productions[@]}
#do
  #for sample in ${backgroundSamples[@]}
  #do
    #./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJobWithProxy.py $batchDir $memory $email checkForDuplicates.${MC}.${sample} ${simpleJobCommand} --treeName nominal --physicsVariables "filter_HT;reco_t1_pt;reco_t1_y" --sampleName ${MC}.${sample} --mainDir ${mainDir} --fileListsDir ${fileListsDir} --debugLevel ${debugLevel}
    ##./TtbarDiffCrossSection/condor_scripts/simpleJob.py $batchDir $memory $email checkForDuplicates.${MC}.${sample} DuplicatedEventsChecker --treeName nominal --physicsVariables "filter_HT;reco_t1_pt;reco_t1_y" --sampleName ${MC}.${sample} --debugLevel ${debugLevel}
    ##echo DuplicatedEventsChecker --treeName truth --physicsVariables "filter_HT;MC_t_afterFSR_pt;MC_t_afterFSR_eta" --sampleName ${MC}.${sample}
  #done

#done

for dat in ${data[@]}
do
  ./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJobWithProxy.py $batchDir $memory $email checkForDuplicates.${dat} ${simpleJobCommand} --treeName nominal --physicsVariables "filter_HT;reco_t1_pt;reco_t1_y" --sampleName ${dat} --mainDir ${mainDir} --fileListsDir ${fileListsDir} --debugLevel ${debugLevel}
  #./TtbarDiffCrossSection/condor_scripts/simpleJob.py $batchDir $memory $email checkForDuplicates.${dat} DuplicatedEventsChecker --treeName nominal --physicsVariables "filter_HT;reco_t1_pt;reco_t1_y" --sampleName ${dat} --debugLevel ${debugLevel}
  #echo DuplicatedEventsChecker --treeName truth --physicsVariables "filter_HT;MC_t_afterFSR_pt;MC_t_afterFSR_eta" --sampleName ${MC}.${sample}
done
