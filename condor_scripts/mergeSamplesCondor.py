#!/bin/env python                                                                                                                                            

import os
import sys
import subprocess
def executeCommand(cmd):
  """
    Execute a shell command and capture its output.

    Args:
        cmd (str): The shell command to execute.

    Returns:
        tuple: A tuple containing the return code, standard output, and standard error.
  """
  result = subprocess.run(cmd, shell=True, capture_output=True, text=True)
  return result.returncode, result.stdout, result.stderr

outputPath=os.getenv('TtbarDiffCrossSection_output_path')
email=os.getenv('TTBAR_EMAIL')
ttbarPath=os.getenv('TTBAR_PATH')

print ("output path: " + outputPath)

sys.path.insert(0,os.getenv("TTBAR_PATH") + '/NtuplesAnalysisTools') 
filename='allhad.boosted.output.root'


from CondorJobsSubmissionHelpers import CondorSubFileMaker  
from CondorJobsSubmissionHelpers import CondorDagmanFileMaker  

subFileMaker = CondorSubFileMaker()
dagFileMaker = CondorDagmanFileMaker()

checkFilesScript=ttbarPath + '/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/checkOutputFiles.sh'
release='21.2'
batchDir=outputPath + '/batch/merge/'
memory='2000'
buildDir=ttbarPath+'/../build'
jobDir=ttbarPath
cmd=os.path.join(ttbarPath, "TtbarDiffCrossSection/condor_scripts/merge_hists_farm.py")
runTime='3*60*60' # 3 hours
maxRetries='2'
disk='10GB'
notification='Error'

subFilesDir=os.path.join(outputPath,"batch/merge")
logsDir = subFilesDir+"/logs/"
rc,out,err = executeCommand("mkdir -p " + logsDir)
subFileMaker.logsDir=logsDir
simpleJobConfiguration = '-b ' + buildDir + ' -e ' + "TTBAR_PATH=" + ttbarPath
subFileMaker.request_memory=memory
subFileMaker.request_disk=disk
subFileMaker.MaxRunTime=runTime
subFileMaker.notify_user=email
subFileMaker.notification=notification
subFileMaker.executable = ttbarPath + '/NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh' # executable for producing histograms from ntuples

mc_productions=("MC23a","MC23d")

outputDirs = (
  'Wt_DR_merged',
  'Wt_DS_merged',
  # 'tchan_merged',
  'singleTop_merged',
  'HWZ_merged',
  'ttbar_allhad_nominal_merged',
  # 'ttbar_allhad_PhPy8EG_hdamp_merged',
  # 'ttbar_allhad_PhPy8EG_MECoff_merged',
  # 'ttbar_allhad_PowhegHerwig704EvtGen_merged',
  # 'ttbar_allhad_PowhegHerwig713EvtGen_merged',
  # 'ttbar_allhad_aMcAtNloPy8EvtGen_merged',
  'ttbar_nonallhad_merged',
  # 'eft_merged'
)

inputDirs={}
inputDirs['Wt_DR_merged'] = (
  '601352.PhPy8EG',
  '601355.PhPy8EG'
)

inputDirs['Wt_DS_merged'] = (
  '601627.PhPy8EG',
  '601631.PhPy8EG'
)
# inputDirs['tchan_merged'] = (
  # '600027.PhPy8EG',
  # '600028.PhPy8EG'
# )
inputDirs['singleTop_merged'] = (
  '601352.PhPy8EG',
  '601355.PhPy8EG'
)
inputDirs['HWZ_merged'] = (
  '522036.aMCPy8EG',
  '522040.aMCPy8EG',
  '602637.PhPy8EG',
  '602638.PhPy8EG',
  '700995.Sh_2214_ttW_0Lfilter',
  '700996.Sh_2214_ttW_1Lfilter',
  '700997.Sh_2214_ttW_2Lfilter'
)
inputDirs['ttbar_allhad_nominal_merged'] = (
  '601237.PhPy8EG',
)

inputDirs['ttbar_nonallhad_merged'] = (
  '601229.PhPy8EG',
  '601230.PhPy8EG'
)



def getFiles(inputs, prod):
    """
    Get a list of files from the specified directories.

    Args:
        inputs (list): List of input directories.
        prod (str): Production prefix to add to the directory names.

    Returns:
        list: List of file paths.
    """
    files = []
    prefix = f"{prod}." if prod else ""

    for input_dir in inputs:
        fileDir = os.path.join(outputPath, f"{prefix}{input_dir}")
        rc, infiles, err = executeCommand(f'ls {fileDir}')
        files.extend(os.path.join(fileDir, infile) for infile in infiles.split())

    return files

def initSubFile(inputFiles,outputFiles,jobName):
    """
    Initialize the submission file with the given parameters.

    Args:
        inputFiles (list): List of input file paths.
        outputFiles (list): List of output file paths.
        jobName (str): Name of the job.

    Returns:
        str: The path to the created submission file.
    """
    params = ' '.join(outputFiles + inputFiles)
    subFileMaker.arguments=simpleJobConfiguration + ' -c ' + cmd + ' ' + params
    subFileName=subFilesDir + '/' + jobName + '.sub'
    subFileMaker.makeSubFile(subFileName)
    return subFileName

# Making output directories
for dir in outputDirs:
  for prod in mc_productions:
    fullDir=outputPath + '/' + prod + '.' + dir
    executeCommand("mkdir -p " + fullDir)
  
  if len(mc_productions)>1:
    fullDir=outputPath + '/' +  'All.' + dir
    executeCommand("mkdir -p " + fullDir)



inputFiles={dir:{prod:getFiles(inputDirs[dir],prod) for prod in mc_productions} for dir in outputDirs}
  
#print(inputFiles)

for dir in outputDirs:
  outputFilesFull = []
  parents = []
  for prod in mc_productions:
    fullDir=outputPath + '/' + prod + '.' + dir
    outputFilesFull.append(fullDir+'/'+filename)
    
    if(len(inputFiles[dir][prod])>1):
      jobName=prod+'.'+dir
      subFileName=initSubFile(inputFiles[dir][prod],[fullDir+'/'+filename],jobName)
      parents.append(jobName)
      dagFileMaker.addJob(jobName,subFileName,maxRetries)
      dagFileMaker.addPostScript(jobName,checkFilesScript + ' $RETURN ' + fullDir+'/'+filename)

    if(len(inputFiles[dir][prod])==1):
      print(fullDir+'/'+filename)
      print(inputFiles[dir][prod][0])
      if(fullDir+'/'+filename != inputFiles[dir][prod][0]):
        executeCommand('ln -s ' + inputFiles[dir][prod][0] + " " + fullDir+'/'+filename)

      
  # print(outputFilesFull)
  if len(mc_productions)>1:
    fullDir=outputPath + '/' + 'All.' + dir
    jobName='All.'+dir
    print(outputFilesFull, [fullDir+'/'+filename], jobName)
    subFileName=initSubFile(outputFilesFull,[fullDir+'/'+filename],jobName)
    dagFileMaker.addJob(jobName,subFileName,maxRetries,parents)
    dagFileMaker.addPostScript(jobName,checkFilesScript + ' $RETURN ' + fullDir+'/'+filename)



dirs = ('data2022_AllYear.physics_Main','data2023_AllYear.physics_Main')
outputDir = 'data_AllYears'
fullDir=outputPath + '/' + outputDir
executeCommand("mkdir -p " + fullDir)
inputFilesFull=getFiles(dirs,'')
jobName=outputDir
subFileName=initSubFile(inputFilesFull,[fullDir+'/'+filename],jobName)
dagFileMaker.addJob(jobName,subFileName,maxRetries)
dagFileMaker.addPostScript(jobName,checkFilesScript + ' $RETURN ' + fullDir+'/'+filename)


dagFileName=subFilesDir + '/merge.dag'
dagFileMaker.makeDagmanFile(dagFileName) # Finalizing dagman file. File is written into dagFileName


overwrite='0'
submCmd="condor_submit_dag"
if overwrite=="1":
  submCmd+=" -f"
submCmd+=" -dont_suppress_notification" + " -notification Complete -append \'notify_user = "  + email + "\' -outfile_dir " + logsDir + " %s" % (dagFileName)

print (submCmd)
rc,out,err = executeCommand(submCmd)
print ("jobs submitted, rc= ",rc)
print ("submission stdout: ",out)
print ("submission stderr: ",err)

