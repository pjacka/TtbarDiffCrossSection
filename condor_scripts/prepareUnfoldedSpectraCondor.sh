#!/bin/bash

#defaults
#outDir=${TTBAR_PATH}/run/
outDir=${TtbarDiffCrossSection_output_path}
email=${TTBAR_EMAIL}
memory=2000
batchDir=${TtbarDiffCrossSection_output_path}/batch/prepareUnfoldedSpectra/
mkdir -p $batchDir


buildDir=${TTBAR_PATH}/../build
jobDir=${TTBAR_PATH}
athena="${TTBAR_ANALYSIS_BASE}"
release='21.2'
version="${TTBAR_ANALYSIS_BASE_VERSION}"
ldpath=${TTBAR_PATH}/RooUnfold


simpleJobCommand="./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/simpleJob.sh -b ${buildDir} -j ${jobDir} -a ${athena} -r ${release} -v ${version} -l ${ldpath} "



levels=(
"ParticleLevel"
"PartonLevel"
)

variables=(
"total_cross_section"
"randomTop_pt"
"randomTop_y"
"t1_pt"
"t1_y"
"t2_pt"
"t2_y"
"ttbar_mass"
"ttbar_pt"
"ttbar_y"
"chi_ttbar"
"y_boost"
"pout"
"H_tt_pt"
"ttbar_deltaphi"
"cos_theta_star"
"delta_pt"
"W1_pt"
"W1_mass"
"W1_y"
"W2_pt"
"W2_mass"
"W2_y"
"b1_pt"
"b1_mass"
"b1_y"
"b2_pt"
"b2_mass"
"b2_y"
##"z_tt_pt"
##"y_star"
##"t1_mass"
##"t2_mass"
##"t1_m_over_pt"
##"t2_m_over_pt"
)

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    ./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJob.py ${batchDir} ${memory} ${email} prepareUnfoldedSpectra.${level}.${variable} ${simpleJobCommand} -c ./TtbarDiffCrossSection/condor_scripts/prepareUnfoldedSpectrum.sh ${TtbarDiffCrossSection_output_path} ${TTBAR_PATH} $variable $level
  done
done

variablesND=(
"t1_pt_vs_t2_pt"
"t1_pt_vs_delta_pt"
"t1_pt_vs_ttbar_pt"
"t1_pt_vs_ttbar_mass"
"ttbar_y_vs_t1_pt"
"t1_y_vs_t1_pt"
"ttbar_y_vs_ttbar_mass"
"ttbar_y_vs_ttbar_pt"
"t2_pt_vs_ttbar_mass"
"ttbar_y_vs_t2_pt"
"t2_y_vs_t2_pt"
"top_pt_vs_ttbar_mass"
"ttbar_y_vs_top_pt"
"ttbar_y_vs_ttbar_mass_vs_t1_pt"
"ttbar_y_vs_ttbar_pt_vs_ttbar_mass"
"top_y_vs_top_pt"
"ttbar_y_vs_top_y"
"ttbar_y_vs_t1_y"
"ttbar_y_vs_t2_y"
"t1_y_vs_ttbar_mass"
"t2_y_vs_ttbar_mass"
"top_y_vs_ttbar_mass"
"ttbar_pt_vs_ttbar_mass"
"delta_eta_vs_ttbar_mass"
"delta_y_vs_ttbar_mass"
"delta_phi_vs_ttbar_mass"
"t1_y_vs_t2_y"
"delta_y_vs_t1_y"
"t1_phi_vs_t1_eta"
"t2_phi_vs_t2_eta"
)

for level in ${levels[@]}
do
  for variable in ${variablesND[@]}
  do
    ./NtuplesAnalysisTools/CondorJobsSubmissionHelpers/submitSimpleJob.py ${batchDir} ${memory} ${email} prepareUnfoldedSpectraND.${level}.${variable} ${simpleJobCommand} -c  ./TtbarDiffCrossSection/condor_scripts/prepareUnfoldedSpectrumND.sh ${TtbarDiffCrossSection_output_path} ${TTBAR_PATH} $variable $level
  done
done


