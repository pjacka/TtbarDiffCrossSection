#!/bin/env python
"""Script to process ntuples in filelist and store output histograms in destination.  

Usage:  
  ./produce_hists_farm.py [options]
  
Options:
    --help -h                     Show help screen.
    --filelist -f <string>        Filelist name. [default: TtbarDiffCrossSection/filelists/SPLITTED/MC23a.601237.PhPy8EG/allhad.boosted.output.txt_000]
    --outputFileName <string>     Output file name. [default: allhad.boosted.output.root_000]
    --outputDestination <string>  Output file is moved to the destination after succesful run. [default: SPLITTED/MC23a.601237.PhPy8EG/]
    --config -c <string>          Config name. [default: TtbarDiffCrossSection/data/config.json]
    --doDetSys -d <string>        Do detector systematics. [default: 0]
    --doSysPDF -p <string>        Do PDF systematics. [default: 0]
    --doSysAlphaSFSR -a <string>  Do alphaSFSR systematics. [default: 0]
    --doSysIFSR -i <string>       Do IFSR systematics. [default: 0]
    --doReweighting -r <string>   Do reweighting. [default: 0]
    --nBootstrap -b <string>      Number of bootstrap replicas. [default: 0]
    --nEvents -n <string>         Number of events. [default: -1]
    --debug -g <string>           Debug level. [default: 0]
"""

import os
import sys
import subprocess
import shutil
sys.path.append(os.path.join(os.getenv("TTBAR_PATH"), 'TtbarDiffCrossSection/python'))
import docopt
import glob
from copy_file_xrdcp import copy_file_xrdcp

def download_files(filelist, destination):
    """
    Download files listed in the filelist to the specified destination directory.

    Args:
        filelist (str): Path to the file containing the list of files to download.
        destination (str): The directory to download the files to.
    """
    if not os.path.exists(destination):
        os.makedirs(destination)
    with open(filelist, 'r') as f:
        for line in f:
            copy_file_xrdcp(line, destination)


def generate_filelist(destination, output_filelist):
    """
    Create a filelist of all files in the specified destination directory.

    Args:
        destination (str): The directory to list files from.
        output_filelist (str): The file to write the list of files to.
    """
    files = glob.glob(destination + "/*")
    with open(output_filelist, 'w') as f_out:
        f_out.writelines(file + "\n" for file in files)


if __name__ == "__main__":

    arguments = docopt.docopt(__doc__)
    filelist = arguments["--filelist"]
    outputFileName = arguments["--outputFileName"]
    outputDestination = arguments["--outputDestination"]
    config = arguments["--config"]
    doDetSys = arguments["--doDetSys"]
    doSysPDF = arguments["--doSysPDF"]
    doSysAlphaSFSR = arguments["--doSysAlphaSFSR"]
    doSysIFSR = arguments["--doSysIFSR"]
    doReweighting = arguments["--doReweighting"]
    nBootstrap = arguments["--nBootstrap"]
    nEvents = arguments["--nEvents"]
    debug = arguments["--debug"]

    # Check if the filelist exists
    if not os.path.exists(filelist):
        print("Filelist does not exist: %s" % filelist)
        sys.exit(1)

    # Check if the config exists
    if not os.path.exists(config):
        print("Config does not exist: %s" % config)
        sys.exit(1)

    # Check if the output directory exists
    if not os.path.exists("tmp"):
        os.makedirs("tmp")


    downloaded_files_list = "tmp/filelist.txt"
    destination = "tmp/files"
    print("Downloading files via xrdcp")
    download_files(filelist, destination)
    generate_filelist(destination, downloaded_files_list)

    outputFile = os.path.join(outputFileName)

    cmd = " ".join([
        "produceHistos",
        "--output " + outputFile,
        "--filelist " + downloaded_files_list,
        "--config " + config,
        "--doDetSys " + doDetSys,
        "--doSysPDF " + doSysPDF,
        "--doSysAlphaSFSR " + doSysAlphaSFSR,
        "--doSysIFSR " + doSysIFSR,
        "--doReweighting " + doReweighting,
        "--nBootstrapReplicas " + nBootstrap,
        "--nEvents " + nEvents,
        "--debugLevel " + debug
    ])

    print(cmd)
    # Execute the command
    exit_status = subprocess.call(cmd, shell=True)

    if exit_status == 0:
        print("Command produceHistos executed successfully")
        if not os.path.exists(outputDestination):
            os.makedirs(outputDestination, exist_ok=True)
        
        destinationFile = os.path.join(outputDestination, outputFileName)
        copy_file_xrdcp(outputFile, outputDestination, force=True)
        os.remove(outputFile)
        print("File ", outputFile, " moved to ", outputDestination)
        shutil.rmtree("tmp")
    else:
        print("Error: Command produceHistos exited with status:", exit_status)

    sys.exit(exit_status)

