#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include <memory>
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"

using namespace std;

class Sample {
  public:
  ~Sample() {
    if(file) delete file;
    if(hist) delete hist;
  }
  TFile* file{nullptr};
  TH1D* hist{nullptr};
  TString filename{""};
  TString histname{""};
  TString legend{""};
  int color{1};
  int lineStyle{1};
  int lineWidth{2};
};



int main(int nArg, char **arg) {
  if(nArg!=6){
    cout << "Error: Wrong number of input parameters!" << endl;
    return -1;
  }
  TH1::AddDirectory(kFALSE);
  TString mainDir=arg[1];
  TString outputSubdir=arg[2];
  TString path_config_files=arg[3];
  TString path_config_lumi=arg[4];
  TString variable = arg[5];

  SetAtlasStyle();
  TString ttbar_path = gSystem->Getenv("TTBAR_PATH");
  
  unique_ptr<NtuplesAnalysisToolsConfig> ttbarConfig= make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  unique_ptr<TEnv> configFiles = make_unique<TEnv>(path_config_files);
  
  TString mc_samples_production = ttbarConfig->mc_samples_production();
  double lumi = configFunctions::getLumi(*ttbarConfig);
  
  TString lumi_string = configFunctions::getLumiString(*ttbarConfig);
  
  cout << "Using lumi: " << lumi_string << endl;
  
  
  vector<TString> suffixes{"_signal_reco","_signal_truth"};
  
  for (const TString& histnameSuffix : suffixes) {
  
  
    std::vector<Sample> samples;
    
    Sample s;
    s.filename=mainDir + "/root.All/systematics_histos/"+variable+"_ParticleLevel.root";
    s.lineStyle=2;
    
    
    
    //s.histname= "PhH704/"+variable+histnameSuffix;
    //s.legend="Powheg+Herwig-7.0.4";
    //s.color=2;
    //s.lineStyle=2;
    //samples.push_back(s);
    
    //s.histname= "PhH713/"+variable+histnameSuffix;
    //s.legend="Powheg+Herwig-7.1.3";
    //s.color=3;
    //samples.push_back(s);
    
    
    s.histname= "ME/"+variable+histnameSuffix;
    s.legend="aMCatNLO+Pythia8";
    s.color=6;
    samples.push_back(s);
    
    s.histname = "nominal/"+variable+histnameSuffix;
    s.legend="Powheg+Pythia8";
    s.color=1;
    s.lineStyle=1;
    samples.push_back(s);
    
    s.histname= "PhPy8MECoff/"+variable+histnameSuffix;
    s.legend="Powheg+Pythia8 MEC off";
    s.color=4;
    samples.push_back(s);
  
    
  
    auto makeLeg = [](double x1,double y1,double x2,double y2,double textSize) {
      auto leg = make_unique<TLegend>(x1,y1,x2,y2);
      leg->SetNColumns(1)   ;
      leg->SetFillColor(0)  ;
      leg->SetFillStyle(0)  ;
      leg->SetBorderSize(0) ;
      leg->SetTextFont(42)  ;
      leg->SetTextSize(textSize);
      return leg;
    };
  
    auto leg = makeLeg(0.6,0.4,0.9,0.8,0.06);
    auto legRatios = makeLeg(0.55,0.7,0.9,0.92,0.04);
  
    vector<TH1D*> histos;
    vector<TH1D*> histosRatios;
  
    int i=0;
    for(Sample& s : samples) {
      s.file=TFile::Open(s.filename,"");
      s.hist=(TH1D*)s.file->Get(s.histname);
      s.hist->SetLineStyle(s.lineStyle);
      s.hist->SetLineColor(s.color);
      s.hist->SetMarkerColor(s.color);
      s.hist->SetLineWidth(s.lineWidth);
      
      s.hist->Scale(1./lumi);
      functions::DivideByBinWidth(s.hist);
      
      leg->AddEntry(s.hist,s.legend);
      histos.push_back(s.hist);
      
      s.hist->GetXaxis()->SetTitle(NamesAndTitles::getXtitle(variable));
      s.hist->GetYaxis()->SetTitle("#frac{d#sigma}{"+NamesAndTitles::getDifferentialVariableLatex(variable) +"} ["+NamesAndTitles::getInverseUnit(variable)+"]");
      
      if(i!=0) {
	histosRatios.push_back((TH1D*)s.hist->Clone());
	histosRatios.back()->Divide(histos[0]);
	histosRatios.back()->GetYaxis()->SetTitle("Prediction / Nominal");
	legRatios->AddEntry(s.hist,s.legend);
      }
      
      i++;
    }
  
    TString outputDirPDF = mainDir + "/pdf." + mc_samples_production + "/" + outputSubdir + "/"; 
    TString outputDirPNG = mainDir + "/png." + mc_samples_production + "/" + outputSubdir + "/"; 
    gSystem->mkdir(outputDirPDF,true);
    gSystem->mkdir(outputDirPNG,true);
       
  
       
  
    //TString y2name="Prediction / Nominal ";
    TString y2name="Prediction / aMC@NLO+Pythia8";
    std::pair<double,double> range = functions::getYRange(histosRatios,false,0.05,0.1);
    const TString ending="_ME";
    
    functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+histnameSuffix+ending+".pdf",histos[0], y2name, range.first, range.second, "",false);
    functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+histnameSuffix+ending+"_log.pdf",histos[0], y2name, range.first, range.second, "",true);
    functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+histnameSuffix+ending+".png",histos[0], y2name, range.first, range.second, "",false);
    functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+histnameSuffix+ending+"_log.png",histos[0], y2name, range.first, range.second, "",true);
    
    for(TH1D* h : histosRatios) h->GetYaxis()->SetTitle(y2name);
    
    range = functions::getYRange(histosRatios,false,0.05,0.5);
    
    functions::plotHistogramsWithFixedYRange(histosRatios,legRatios.get(),outputDirPDF,variable+histnameSuffix+"_ratios"+ending+".pdf","",range.first, range.second,false);
    functions::plotHistogramsWithFixedYRange(histosRatios,legRatios.get(),outputDirPNG,variable+histnameSuffix+"_ratios"+ending+".png","",range.first, range.second,false);
    
  }


   cout << "drawSamplesComparison: Done." << endl;
   return 0;
}

