// Peter Berta, 15.10.2015

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"

using namespace std;

class Sample {
  public:
  ~Sample() {
    if(file) delete file;
    if(hist) delete hist;
  }
  TFile* file{nullptr};
  TH1D* hist{nullptr};
  TString filename{""};
  TString mainDir;
  vector<TString> regions;
  TString subDir;
  TString legend{""};
  int color{1};
  int lineStyle{1};
  int lineWidth{2};
};


int main(int nArg, char **arg) {
  
  if(nArg!=6){
    cout << "Error: Wrong number of input parameters!" << endl;
    return -1;
  }
  TH1::AddDirectory(kFALSE);
  TString mainDir=arg[1];
  TString outputSubdir=arg[2];
  TString path_config_files=arg[3];
  TString path_config_lumi=arg[4];
  TString variable = arg[5];

  SetAtlasStyle();
  TString ttbar_path = gSystem->Getenv("TTBAR_PATH");
  
  unique_ptr<NtuplesAnalysisToolsConfig> ttbarConfig= make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  unique_ptr<TEnv> configFiles = make_unique<TEnv>(path_config_files);
  
  const TString& mc_samples_production = ttbarConfig->mc_samples_production();
  double lumi = configFunctions::getLumi(*ttbarConfig);
  
  TString lumi_string = configFunctions::getLumiString(*ttbarConfig);
  
  cout << "Using lumi: " << lumi_string << endl;
  
  
  vector<TString> regions;
  regions.push_back("Leading_1t_1b_Recoil_1t_1b_tight"); 
  regions.push_back("Leading_0t_1b_Recoil_1t_1b_tight"); 
  regions.push_back("Leading_1t_0b_Recoil_1t_1b_tight"); 
  regions.push_back("Leading_1t_1b_Recoil_0t_1b_tight"); 
  regions.push_back("Leading_1t_1b_Recoil_1t_0b_tight"); 
  regions.push_back("Leading_0t_0b_Recoil_1t_1b_tight"); 
  regions.push_back("Leading_0t_1b_Recoil_0t_1b_tight"); 
  regions.push_back("Leading_0t_1b_Recoil_1t_0b_tight"); 
  regions.push_back("Leading_1t_0b_Recoil_0t_1b_tight"); 
  regions.push_back("Leading_1t_0b_Recoil_1t_0b_tight"); 
  regions.push_back("Leading_1t_1b_Recoil_0t_0b_tight"); 
  regions.push_back("Leading_0t_0b_Recoil_0t_1b_tight"); 
  regions.push_back("Leading_0t_0b_Recoil_1t_0b_tight"); 
  regions.push_back("Leading_0t_1b_Recoil_0t_0b_tight"); 
  regions.push_back("Leading_1t_0b_Recoil_0t_0b_tight"); 
  regions.push_back("Leading_0t_0b_Recoil_0t_0b_tight"); 
  
  TString subdir="RecoLevel";
  if(variable.EndsWith("RecoLevel")) subdir="unfolding";
  
  
  
  std::vector<Sample> samples;
  
  Sample s;
  s.regions=regions;
  s.mainDir="nominal";
  s.subDir=subdir;
  
  s.filename=mainDir + "/" + mc_samples_production + "." + configFiles->GetValue("ttbar_signal_nominal_filename","");
  s.legend="Powheg+Pythia8";
  s.color=1;
  s.lineStyle=1;
  samples.push_back(s);
  
  s.filename=mainDir + "/" + mc_samples_production + "." + configFiles->GetValue("ttbar_signal_PhH704_filename","");
  s.legend="Powheg+Herwig-7.0.4";
  s.color=2;
  s.lineStyle=2;
  samples.push_back(s);
  
  s.filename=mainDir + "/" + mc_samples_production + "." + configFiles->GetValue("ttbar_signal_PhH713_filename","");
  s.legend="Powheg+Herwig-7.1.3";
  s.color=3;
  samples.push_back(s);
  
  s.filename=mainDir + "/" + mc_samples_production + "." + configFiles->GetValue("ttbar_signal_PhPy8MECoff_filename","");
  s.legend="Powheg+Pythia8 MEC off";
  s.color=4;
  samples.push_back(s);
  
  s.filename=mainDir + "/" + mc_samples_production + "." + configFiles->GetValue("ttbar_signal_ME_filename","");
  s.legend="aMCatNLO+Pythia8";
  s.color=6;
  samples.push_back(s);

  auto makeLeg = [](double x1,double y1,double x2,double y2,double textSize) {
    auto leg = make_unique<TLegend>(x1,y1,x2,y2);
    leg->SetNColumns(1)   ;
    leg->SetFillColor(0)  ;
    leg->SetFillStyle(0)  ;
    leg->SetBorderSize(0) ;
    leg->SetTextFont(42)  ;
    leg->SetTextSize(textSize);
    return leg;
  };

  auto leg = makeLeg(0.6,0.4,0.9,0.8,0.06);
  auto legRatios = makeLeg(0.55,0.7,0.9,0.92,0.04);

  vector<TH1D*> histos;
  vector<TH1D*> histosRatios;

  int i=0;
  for(Sample& s : samples) {
    s.file=TFile::Open(s.filename,"");
    
    TString histname=s.mainDir+"/"+s.regions[0]+"/"+s.subDir+"/"+variable;
    
    s.hist=(TH1D*)s.file->Get(histname);
    for(size_t idir=1;idir<s.regions.size();idir++) {
      TString histname=s.mainDir+"/"+s.regions[idir]+"/"+s.subDir+"/"+variable;
      auto h = std::unique_ptr<TH1D>{(TH1D*)s.file->Get(histname)};
      s.hist->Add(h.get());
    }
    
    s.hist->SetLineStyle(s.lineStyle);
    s.hist->SetLineColor(s.color);
    s.hist->SetMarkerColor(s.color);
    s.hist->SetLineWidth(s.lineWidth);
    
    s.hist->Scale(1./lumi);
    functions::DivideByBinWidth(s.hist);
    
    leg->AddEntry(s.hist,s.legend);
    histos.push_back(s.hist);
    
    //s.hist->GetXaxis()->SetTitle(NamesAndTitles::getXtitle(variable));
    //s.hist->GetYaxis()->SetTitle("#frac{d#sigma}{"+NamesAndTitles::getDifferentialVariableLatex(variable) +"} ["+NamesAndTitles::getInverseUnit(variable)+"]");
    
    if(i!=0) {
      histosRatios.push_back((TH1D*)s.hist->Clone());
      histosRatios.back()->Divide(histos[0]);
      histosRatios.back()->GetYaxis()->SetTitle("Prediction / Nominal");
      legRatios->AddEntry(s.hist,s.legend);
    }
    
    i++;
  }

  TString outputDirPDF = mainDir + "/pdf." + mc_samples_production + "/" + outputSubdir + "/"; 
  TString outputDirPNG = mainDir + "/png." + mc_samples_production + "/" + outputSubdir + "/"; 
  gSystem->mkdir(outputDirPDF,true);
  gSystem->mkdir(outputDirPNG,true);
     

     

  TString y2name="Prediction / Nominal ";
  double y2min=0.81;
  double y2max=1.09;

  bool use_logscale=false;

     
  functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_16regions"+".pdf",histos[0], y2name, y2min, y2max, "",use_logscale);
  functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_16regions"+"_log.pdf",histos[0], y2name, y2min, y2max, "",true);
  functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_16regions"+".png",histos[0], y2name, y2min, y2max, "",use_logscale);
  functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_16regions"+"_log.png",histos[0], y2name, y2min, y2max, "",true);
  
  functions::plotHistogramsWithFixedYRange(histosRatios,legRatios.get(),outputDirPDF,variable+"_16regions"+"_ratios.pdf","",y2min, y2max,false);
  functions::plotHistogramsWithFixedYRange(histosRatios,legRatios.get(),outputDirPNG,variable+"_16regions"+"_ratios.png","",y2min, y2max,false);
  

  cout << "drawSamplesComparison: Done." << endl;
  return 0;
}
