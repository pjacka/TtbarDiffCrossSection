////////////////////////////////////////////////////////////
//                  Ota Zaplatilek                        //
//   Compare Acceptancy and Efficiency of different MC    //
//                    14.11.2017                          //
////////////////////////////////////////////////////////////

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "CovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "HelperFunctions/AtlasStyle.h"
#include "TtbarDiffCrossSection/CompareAccEff_functions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include <algorithm>
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"



int main(int nArg, char **arg) {
  try {
    
    if(nArg!=7){
	  cout << "Error: Wrong number of input parameters!" << endl;
	  cout << nArg << " were used" << endl;
	  throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    }     
  
    bool useNewISR=true;
  
    SetAtlasStyle();
  
    //set input arguments 
    
    TString configLumiPath = arg[1];
    TString systematicHistosFolder = arg[2];
    TString str_AccEffDirName = (TString)arg[3] + "/";	
    TString str_variableName = arg[4];
    TString level = arg[5]; 
    TString dimension = arg[6];
    
    TString pathConfigBinningND = (TString)"TtbarDiffCrossSection/data/optBinningND/" + str_variableName + ".env";
    TString pathConfigPlotSettingsND = (TString)"TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env";
    
    TString levelShort=level;
    levelShort.ReplaceAll("Level","");
    
    std::unique_ptr<HistogramNDto1DConverter> histogramConverterND_reco, histogramConverterND_truth;
    std::unique_ptr<MultiDimensionalPlotsSettings> plotSettingsND;
    
    if(dimension=="ND") {
      histogramConverterND_reco = std::make_unique<HistogramNDto1DConverter>(pathConfigBinningND.Data());
      histogramConverterND_reco->initializeOptBinning((str_variableName + "_"+levelShort+"_reco").Data());
      histogramConverterND_reco->initialize1DBinning();
      
      histogramConverterND_truth = std::make_unique<HistogramNDto1DConverter>(pathConfigBinningND.Data());
      histogramConverterND_truth->initializeOptBinning((str_variableName + "_"+levelShort+"_truth").Data());
      histogramConverterND_truth->initialize1DBinning();
      
      plotSettingsND = std::make_unique<MultiDimensionalPlotsSettings>(pathConfigPlotSettingsND.Data());
    }
    
    
    
    auto config = std::make_shared<const NtuplesAnalysisToolsConfig>(configLumiPath);
    
    const TString mc_samples_production = config->mc_samples_production();
    const TString nominalTreeName = config->nominalFolderName().c_str();
    const TString rootDir="root." + mc_samples_production;
    
    
    TString str_TtbarDiffCrossSection_output_path = gSystem->Getenv("TtbarDiffCrossSection_output_path");     // output path ${TtbarDiffCrossSection_output_path} e.q. /raid7_atlas/zaplatilek/xAnalysis/xAnalysis_output/pJacka_run_output/
    TString str_rootFileNamePointRoot = str_variableName +"_"+level+".root";                 // name of rootFile e.q. cos_theta_star_fine_ParticleLevel.root
    TString str_rootFile = str_TtbarDiffCrossSection_output_path + "/" + rootDir + "/" + systematicHistosFolder + "/" + str_rootFileNamePointRoot;						// eq. /AccEffPlots
																
																// now set the subdirectories for 5 types of pdf: Aff; Eff; AffEffTogether; AffRatio and at last EffRatio
    TString str_AccRatioDirName = "AccRatio";						// eq. /AccRatio
    TString str_EffRatioDirName = "EffRatio"; 						// eq. /EffRatio
    TString str_AccFinalDirName = "Acc";						// eq. /Acc
    TString str_EffFinalDirName = "Eff"; 						// eq. /Eff
    TString str_AccEffTogetherDirName = "AccEff";   				// eq. /AccEff
        
    TString str_subDir="";
      
    cout << str_rootFileNamePointRoot << endl;
      
    cout << "Working on var: " << str_variableName << " at " << level << endl; 
      
    // output path for final pdf plots       
    TString outPutPath_AccEff = str_TtbarDiffCrossSection_output_path +"/pdf."+ mc_samples_production + "/" + str_subDir + str_AccEffDirName;
    TString outPutPath_AccRatio = outPutPath_AccEff + str_AccRatioDirName;
    TString outPutPath_EffRatio = outPutPath_AccEff + str_EffRatioDirName;
    TString outPutPath_AccFinal = outPutPath_AccEff + str_AccFinalDirName;
    TString outPutPath_EffFinal = outPutPath_AccEff + str_EffFinalDirName;
    TString outPutPath_AccEffTogether = outPutPath_AccEff + str_AccEffTogetherDirName;
    
    gSystem->mkdir(outPutPath_AccRatio,true);
    gSystem->mkdir(outPutPath_EffRatio,true);
    gSystem->mkdir(outPutPath_AccFinal,true);
    gSystem->mkdir(outPutPath_EffFinal,true);
    gSystem->mkdir(outPutPath_AccEffTogether,true);

    //setup colors
    
    
    vector<TString> vec_SubDir;
    vector<TString> vec_SamplesNames;
    vector<int> vec_colorUnc;
    vector<int> vec_colorMarkers;
    
    auto pushFunc = [&](const TString& directory, const TString& legendName,int colorUnc,int colorMarker) {
      vec_SubDir.push_back(directory);
      vec_SamplesNames.push_back(legendName);
      vec_colorUnc.push_back(colorUnc);
      vec_colorMarkers.push_back(colorMarker);
    };
    // names of considered TDirectories in root file
    // NOMINAL sample have to be at the first!
    
    pushFunc(nominalTreeName,"Powheg + Pythia8 (nominal)",632,632-10);
    
    bool plotAlternativeSamples = false;
    if (plotAlternativeSamples) {
      pushFunc(config->hardScatteringDirectory(),"aMcAtNlo + Pythia8",600,600-9);
      pushFunc(config->PhH704Directory(),"Powheg + Herwig7.0.4",432,432-10);
      pushFunc(config->PhPy8MECoffDirectory(),"Powheg + Pythia8 (MECoff)",416+3,416-10);
      pushFunc(config->partonShoweringDirectory(),"Powheg + Herwig7.1.3",616 + 2,616 - 8);
    }
    bool plotISRFSR=false;
    if(plotISRFSR) {
      if(useNewISR) {
        pushFunc(config->ISRmuRupDirectory(),"Powheg + Pythia8 (ISR #muR up)",432,432-10);
        pushFunc(config->ISRmuRdownDirectory(),"Powheg + Pythia8 (ISR #muR down)",416+3,416-10);
        pushFunc(config->ISRmuFupDirectory(),"Powheg + Pythia8 (ISR #muF up)",14,14);
        pushFunc(config->ISRmuFdownDirectory(),"Powheg + Pythia8 (ISR #muF down)",15,15);
        pushFunc(config->ISRVar3cUpDirectory(),"Powheg + Pythia8 (ISR var3c up)",16,16);
        pushFunc(config->ISRVar3cDownDirectory(),"Powheg + Pythia8 (ISR var3c down)",17,17);
      }
      else {
        pushFunc(config->ISRupDirectory(),"Powheg + Pythia8 (ISR up)",432,432-10);
        pushFunc(config->ISRdownDirectory(),"Powheg + Pythia8 (ISR down)",416+3,416-10);
      }
      pushFunc(config->FSRupDirectory(),"Powheg + Pythia8 (FSR up)",800,800-9);
      pushFunc(config->FSRdownDirectory(),"Powheg + Pythia8 (FSR down)",400+1,400-10);
    }
    
    TFile* rootFile= new TFile(str_rootFile);
    
    // there are two type of plots: 1.) acceptancy 2.) efficieny basicaly
    // those histograms will be located in following vector
    vector<TH1D*> vec_EffToPlot;
    vector<TH1D*> vec_AccToPlot;
    
    vector<TH1D*> vec_Eff_ratio;
    vector<TH1D*> vec_Acc_ratio;
        
    
    TString str_hist = str_variableName;  
      
    TString str_hist_signal = 	str_hist + "_signal_truth";
    TString str_hist_reco = 	str_hist + "_EffOfRecoLevelCutsNominator";
    
    TString str_Acc_truth = str_hist+"_EffOfTruthLevelCutsNominator";
    TString str_Acc_reco = str_hist+"_signal_reco";    
    
    TH1D* h_eff_nominal = (TH1D*)rootFile->Get(nominalTreeName + "/" + str_hist_reco);
    TH1D* h_acc_nominal = (TH1D*)rootFile->Get(nominalTreeName + "/"+str_Acc_truth);

    unsigned int nSubDir = vec_SubDir.size();   
    // loop over considered TDirectories in rooFile    
    for(unsigned int iSubDir = 0; iSubDir < nSubDir; iSubDir++){
      TString str_subDir = vec_SubDir.at(iSubDir);
      cout << "enter to subDir: " << str_subDir << endl;
      
      									// Acceptancy - denominator
      
      std::cout << "reco hist: " << str_subDir+"/"+str_hist_reco << std::endl;
	  
      /////////////////////////////////////////////////////////////////////////////
      // Efficiency
      //
      std::cout << "reco hist: " << str_subDir+"/"+str_hist_reco << std::endl;
      TH1D* h_eff = (TH1D*) rootFile->Get( str_subDir+"/"+str_hist_reco); 		// Eff - numerator
      std::cout << "truth hist: " << str_subDir+"/"+str_hist_signal << std::endl;
      TH1D* h_truth = (TH1D*) rootFile->Get( str_subDir+"/"+str_hist_signal);		// Eff - denominator
      
      
      
      h_eff->Divide(h_eff, h_truth, 1, 1, "B");	 								// compute efficiency as division
      vec_EffToPlot.push_back(h_eff);		
      
      // get clone NOMINAL MC sample for ratio
      // NOMINAL sample HAVE TO be at first in 
      if (str_subDir == nominalTreeName){
        h_eff_nominal -> Divide(h_eff_nominal,h_truth,1,1,"B" );
        cout << "eff_integral - nominal: " << h_eff_nominal->Integral() << endl;
      }
      //prepare Ratia of efficiency with respect the NOMINAL MC sample: h_eff/h_eff_nominal
      else{
        TH1D* h_eff_ratio = (TH1D*)h_eff->Clone();
        h_eff_ratio->Divide(h_eff_ratio, h_eff_nominal, 1, 1, "B");
        cout << "eff_integral - ratio: " << h_eff_ratio->Integral() << endl;
        vec_Eff_ratio.push_back(h_eff_ratio);			
      }
      
      //////////////////////////////////
      // Acceptancy
      
      TH1D* h_Acc_truth 	= (TH1D*) rootFile->Get( str_subDir+"/"+str_Acc_truth); // Acc - numerator       
      TH1D* h_Acc_reco 	= (TH1D*) rootFile->Get( str_subDir+"/"+str_Acc_reco); 	// Acc - denominator
            
      
      h_Acc_truth->Divide(h_Acc_truth, h_Acc_reco, 1, 1, "B");					//compute ecceptance as division
      vec_AccToPlot.push_back(h_Acc_truth);
      
      // get clone NOMINAL MC sample for ratio
      // NOMINAL sample HAVE TO be at first in 	
      if (str_subDir == nominalTreeName) {
        h_acc_nominal -> Divide(h_acc_nominal, h_Acc_reco, 1,1,"B");
        cout << "Acc_integral-nominal: " << h_acc_nominal->Integral() << endl;
      }
      //prepare Ratia of acceptynce with respect the NOMINAL MC sample: h_acc/h_acc_nominal
      else{
        TH1D* h_acc_ratio = (TH1D*)h_Acc_truth->Clone();
        h_acc_ratio->Divide(h_acc_ratio, h_acc_nominal, 1, 1, "B");
        cout << "Acc_integral-ratio : " << h_acc_ratio->Integral() << endl;
        vec_Acc_ratio.push_back(h_acc_ratio);			
      }
    }
      
    TString label = "label";
    bool bool_ratio = true;
    
    //////////////////////////////////////////////////////////
    //Now call function to make pdf of vectors of histograms
    //

    // Do pdf plots 1 pad plots - only ratio
    PlotVectorInOnePlot(vec_Eff_ratio, vec_SamplesNames, vec_colorMarkers, vec_colorUnc, str_variableName, outPutPath_EffRatio, level, label, "EffOnly", bool_ratio);
    PlotVectorInOnePlot(vec_Acc_ratio, vec_SamplesNames, vec_colorMarkers, vec_colorUnc, str_variableName, outPutPath_AccRatio, level, label, "AccOnly", bool_ratio);			
    
    // Do pdf plot with 3 pads - 1. acceptancy, 2. efficiency, 3. between pad 
    PlotAllAccInOnePlot(vec_EffToPlot, vec_AccToPlot, vec_SamplesNames, vec_colorMarkers, vec_colorUnc, str_variableName, outPutPath_AccEffTogether, level, label);
    
    // Do pdf plots in 2 pads with ratio plot
    PlotVectorIn2Pads(vec_AccToPlot, vec_SamplesNames, vec_colorMarkers, vec_colorUnc, str_variableName, outPutPath_AccFinal, level, label, "Acc_final");	
    PlotVectorIn2Pads(vec_EffToPlot, vec_SamplesNames, vec_colorMarkers, vec_colorUnc, str_variableName, outPutPath_EffFinal, level, label, "Eff_final");	
    
    
    vector<double> binning_x,binning_x_truth;
    vector<vector<double> > binning_y,binning_y_truth;
    
    
    TString subdir = str_AccEffDirName + "/" + "AccEffNominal/";
    gSystem->mkdir(str_TtbarDiffCrossSection_output_path+"/pdf."+mc_samples_production +"/" +subdir,true);
    gSystem->mkdir(str_TtbarDiffCrossSection_output_path+"/png."+mc_samples_production +"/" +subdir,true);
    
    
    TH1D* h_acc_nominal_concatenated;
    TH1D* h_eff_nominal_concatenated;
    if(dimension=="2D"){
      functions::load2DBinning(rootFile,str_variableName,binning_x,binning_y,binning_x_truth,binning_y_truth);
      functions::makeConcatenatedHistogram(h_acc_nominal,h_acc_nominal_concatenated,binning_x_truth,binning_y_truth);
      functions::makeConcatenatedHistogram(h_eff_nominal,h_eff_nominal_concatenated,binning_x,binning_y);
      
      NamesAndTitles::setXtitle(h_acc_nominal_concatenated,str_variableName);
      NamesAndTitles::setXtitle(h_eff_nominal_concatenated,str_variableName);
      h_acc_nominal_concatenated->GetYaxis()->SetTitle("Correction");
      PlotAccEffInOnePlot(h_acc_nominal_concatenated, h_eff_nominal_concatenated,str_TtbarDiffCrossSection_output_path,mc_samples_production,subdir,level,str_variableName,"Simulation Internal",dimension,binning_x_truth,binning_y_truth);

    }
    else if(dimension=="ND") {
      TString dirname=str_TtbarDiffCrossSection_output_path + "/pdf." +mc_samples_production+"/"+subdir;
      plotAccEffInOnePlotND(h_acc_nominal, h_eff_nominal, histogramConverterND_reco.get(), plotSettingsND.get(),dirname,level,str_variableName); 
    }
    else{
      NamesAndTitles::setXtitle(h_acc_nominal,str_variableName);
      NamesAndTitles::setXtitle(h_eff_nominal,str_variableName);
      h_acc_nominal->GetYaxis()->SetTitle("Correction");
      h_eff_nominal->GetYaxis()->SetTitle("Correction");
      
      PlotAccEffInOnePlot(h_acc_nominal, h_eff_nominal, str_TtbarDiffCrossSection_output_path,mc_samples_production,subdir,level,str_variableName,"Simulation Internal",dimension,binning_x,binning_y);

    }
    
   
    
    
   
	
  }
  catch(const TtbarDiffCrossSection_exception& l){
    l.printMessage();
    cout << "Exiting now!" << endl;
  return -1;
  }
  
  cout << "Everything ok!" << endl;
  return 0;
}
