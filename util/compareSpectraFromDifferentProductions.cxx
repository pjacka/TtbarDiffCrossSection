// Peter Berta, 15.10.2015

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/AtlasStyle.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <tuple>
 
#include "TString.h" 
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TROOT.h"
#include "TStyle.h"

#include "docopt.h"

using namespace std;

int compareSpectraFromDifferentProductions(TString mainDir1,TString mainDir2, TString variable, TString level,TString dimension, TString outputDir, int debugLevel);

void makeRatioPlot(TH1D* h1,TH1D* h2,const TString& outputDir,const TString& name,const TString& info,bool normalize=false);
TH1D* loadHistogramFromFile(TFile& f, const TString& dir,const TString& variable,const TString& name);
void loadPredictions(std::map<TString,TH1D*>& predictions, TFile& f, const TString& histDir, const TString& variable);


TString mcProduction1;
TString mcProduction2;

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to compare distributions from different productions. 

Usage:  
  compareSpectraFromDifferentProductions [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir1 <mainDir1>                   Directory where the first production is stored. [default: $TtbarDiffCrossSection_output_path]
  --mainDir2 <mainDir2>                   Directory where the second production is stored. [default: $TtbarDiffCrossSection_output_path]
  --mcProduction1 <mcProduction1>         MC campaign used for the first production. Available options MC16a, MC16d, MC16e, All. [default: All]
  --mcProduction2 <mcProduction2>         MC campaign used for the second production. Available options MC16a, MC16d, MC16e, All. [default: All]
  --variable <string>                 	  Variable name. [default: total_cross_section]
  --level <string>                        ParticleLevel or PartonLevel. [default: ParticleLevel]
  --dimension <string>                    Dimension of distribution. 1D or 2D. [default: 1D]
  --outputDir <string>                    Path to output directory. [default: comparisonOfSpectraFromDifferentProductions]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {

  cout << endl << endl << " ***** running  " << arg[0] << " ***** " << endl << endl;

  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true);  // Using docopt package to read parameters

  cout << "Reading parameters." << endl;  
  
  TString mainDir1("");
  TString mainDir2("");
  TString variable("");
  TString level("");
  TString dimension("");
  TString outputDir("");
  int  debugLevel(0);
  
  try{
  
    try{ mainDir1=args["--mainDir1"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir1 option is expected to be a string. Check input parameters."); }
      
    try{ mainDir2=args["--mainDir2"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir2 option is expected to be a string. Check input parameters."); }
      
    try{ mcProduction1=args["--mcProduction1"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mcProduction1 option is expected to be a string. Check input parameters."); }
      
    try{ mcProduction2=args["--mcProduction2"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mcProduction2 option is expected to be a string. Check input parameters."); }
      
    try{ variable=args["--variable"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
      
    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
      
    try{ dimension=args["--dimension"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--dimension option is expected to be a string. Check input parameters."); }
      
    try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
    try{ debugLevel = args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel is not an integer. Check input parameters."); }
  
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }  

  if(mainDir1=="$TtbarDiffCrossSection_output_path") mainDir1=gSystem->Getenv("TtbarDiffCrossSection_output_path");
  if(mainDir2=="$TtbarDiffCrossSection_output_path") mainDir2=gSystem->Getenv("TtbarDiffCrossSection_output_path");
  

  return compareSpectraFromDifferentProductions(mainDir1,mainDir2, variable, level, dimension, outputDir, debugLevel);
}

int compareSpectraFromDifferentProductions(TString mainDir1,TString mainDir2, TString variable, TString level, TString dimension, TString outputDir, int debugLevel){
 
  if(debugLevel>0){
    std::cout << "Printing input parameters: " << std::endl;
    std::cout << "mainDir1: " + mainDir1 << std::endl;
    std::cout << "mainDir2: " + mainDir2 << std::endl;
    std::cout << "mcProduction1: " + mcProduction1 << std::endl;
    std::cout << "mcProduction2: " + mcProduction2 << std::endl;
    std::cout << "variable: " + variable << std::endl;
    std::cout << "level: " + level << std::endl;
    std::cout << "dimension: " + dimension << std::endl;
    std::cout << "outputDir: " + outputDir << std::endl;
    std::cout << "debugLevel: " << debugLevel << std::endl;
  }
  TH1::AddDirectory(false);
  gROOT->SetStyle("ATLAS");
  gStyle->SetErrorX();
 
  TString rootDir1="root."+mcProduction1;
  TString rootDir2="root."+mcProduction2;
  TString systematicsHistosDir = "systematics_histos";
  TString theoryVsDataComparisonDir="Theory_vs_data_comparison";
  if(dimension=="2D"){
    systematicsHistosDir     +="2D";
    theoryVsDataComparisonDir+="2D";
  }
  TString filename=variable+"_"+level+".root";
  
  auto fileSystematicsHistos1 = make_shared<TFile>(mainDir1 + "/" + rootDir1 + "/" + systematicsHistosDir+"/"+filename);
  auto fileSystematicsHistos2 = make_shared<TFile>(mainDir2 + "/" + rootDir2 + "/" + systematicsHistosDir+"/"+filename);

  TString histDir="nominal"; // Directory inside TFile
  
  std::map<TString,TH1D*> predictions1, predictions2;
  
  
  loadPredictions(predictions1, *fileSystematicsHistos1,histDir,variable);
  loadPredictions(predictions2, *fileSystematicsHistos2,histDir,variable);
  
  TString outputDirPNG=outputDir;
  outputDirPNG.ReplaceAll("pdf.","png.");
  
  gSystem->mkdir(outputDir,true);
  gSystem->mkdir(outputDirPNG,true);
  
  vector<TString> normalize{"truth","reco","ttbar_nonallhad","multijet","unfoldedDistribution","data"};
  
  
  for(auto& it : predictions1) {
    const TString& key = it.first;
    TH1D* h1 = it.second;
    TH1D* h2 = predictions2.at(key);
    
    TString info = key;
    if(key=="unfoldedDistribution") info = "Unfolded results";
    info+=" ratio";
    
    makeRatioPlot(h2,h1,outputDirPNG,variable+"_"+level+"_" + key + ".png",info);
    makeRatioPlot(h2,h1,outputDir,variable+"_"+level+"_" + key + ".pdf",info);
    if(std::find(std::begin(normalize),std::end(normalize),key)!=std::end(normalize)) {
      makeRatioPlot(h2,h1,outputDirPNG,variable+"_"+level+"_" + key + "_normalized.png",info,/*normalize=*/true);
      makeRatioPlot(h2,h1,outputDir,variable+"_"+level+"_" + key + "_normalized.pdf",info,/*normalize=*/true);
    }
    
    
    
  }
 
  return 0;
}

//----------------------------------------------------------------------

void loadPredictions(std::map<TString,TH1D*>& predictions, TFile& f, const TString& histDir, const TString& variable) {
  
  predictions["data"] = loadHistogramFromFile(f,histDir,variable,"data");
  predictions["efficiencyNominator"] = loadHistogramFromFile(f,histDir,variable,"EffOfRecoLevelCutsNominator");
  predictions["truth"] = loadHistogramFromFile(f,histDir,variable,"signal_truth");
  predictions["reco"] = loadHistogramFromFile(f,histDir,variable,"signal_reco");
  predictions["acceptanceNominator"] = loadHistogramFromFile(f,histDir,variable,"EffOfTruthLevelCutsNominator");
  predictions["unfoldedDistribution"] = loadHistogramFromFile(f,histDir,variable,"unfolded_data_minus_bkg");
  
  predictions["multijet"] = loadHistogramFromFile(f,histDir,variable,"bkg_Multijet");
  predictions["ttbar_nonallhad"] = loadHistogramFromFile(f,histDir+"/MCBackgrounds",variable,"ttbar_nonallhad");
  
  predictions["efficiency"] = (TH1D*)predictions["efficiencyNominator"]->Clone();
  predictions["efficiency"]->Divide(predictions["efficiencyNominator"],predictions["truth"],1,1,"B");
  
  predictions["acceptance"] = (TH1D*)predictions["acceptanceNominator"]->Clone();
  predictions["acceptance"]->Divide(predictions["acceptanceNominator"],predictions["reco"],1,1,"B");

}

//----------------------------------------------------------------------

void makeRatioPlot(TH1D* nominator,TH1D* denumenator,const TString& outputDir,const TString& name,const TString& info,bool normalize){
  
  auto ratio=std::unique_ptr<TH1D>{(TH1D*)nominator->Clone()};
  const double normFactor=denumenator->Integral()/nominator->Integral();
  ratio->Divide(denumenator);
  if(normalize) ratio->Scale(normFactor);
  
  ratio->SetStats(kFALSE);
  
  auto can=std::make_unique<TCanvas>("can","canvas",0,0,1000.,600.);
  
  double maximum = functions::getMaximum(*ratio);
  double minimum = functions::getMinimum(*ratio);
  
  ratio->GetYaxis()->SetRangeUser(minimum*(1.-0.5*(maximum-minimum)),maximum*(1.+1.2*(maximum-minimum)));
  
  std::map<TString,TString> mapping = {{"MC16a","2015+2016"}, {"MC16d","2017"},{"MC16e","2018"}};
  
  TString ytitle = mapping[mcProduction2] + " / " + mapping[mcProduction1];

  
  
  ratio->GetYaxis()->SetTitle(ytitle);
  ratio->Draw();
  functions::WriteGeneralInfo("","139 fb^{-1}",0.05,0.5,0.85,0.06);
  functions::WriteInfo(info,0.05,0.5,0.74);
  
  can->SaveAs(outputDir+"/"+name);
}

//----------------------------------------------------------------------

TH1D* loadHistogramFromFile(TFile& f, const TString& dir, const TString& variable,const TString& name){
  TH1D* h=(TH1D*)f.Get(dir +"/" + variable+"_"+name);
  NamesAndTitles::setXtitle(h,variable);
  h->SetTitle("");
  
  return h;
}
