
#include "TtbarDiffCrossSection/FileStore.h"
#include "TtbarDiffCrossSection/HistStore.h"
#include "CovarianceCalculator/Pseudoexperiment.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

int runPrepareSystematicHistosND( TString mainDir,
				TString path_config_files,
				TString path_config_lumi,
				TString path_config_binning,
				TString variable_name,
				TString level,
				TString systematicHistosPath,
				bool saveBootstrapHistos,
				bool saveStressTestHistos,
				bool /*doRebin*/,
				int debugLevel)
{
				  
  delete gRandom;
  gRandom = new TRandom3(0);
				  
  
  cout << "saveBootstrapHistos: " <<saveBootstrapHistos << endl;
  cout << "saveStressTestHistos: " <<saveStressTestHistos << endl;
  
  auto config = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  
  bool prepareModelingHistos = config->doSignalModeling();
  TString mc_samples_production = config->mc_samples_production();
  
  cout << "prepareModelingHistos: " <<prepareModelingHistos << endl;
  
  FileStore* fileStore = new FileStore();
  
  fileStore->initialize(mainDir,debugLevel, path_config_files,  path_config_lumi, path_config_binning);
  TString path_to_output_file=mainDir +"/root." + mc_samples_production +"/" + systematicHistosPath+"/";
  gSystem->mkdir(path_to_output_file,true);
  TFile* fOutput = new TFile(path_to_output_file+variable_name + "_" + level + ".root","RECREATE");
  
  HistStore* histStore = new HistStore();
  histStore->initialize(fileStore,debugLevel,path_config_lumi,path_config_binning);
  fOutput->cd();
  histStore->load_nominal_histosND(variable_name,level);
  
  histStore->WriteNominalHistosND(fOutput);
  histStore->WriteAllSysHistos(fOutput,"ND");
  if(prepareModelingHistos){
    fileStore->load_special_tfiles();
    
    vector<vector<TString>> modNames = { 
      {"ME","ME","nominal"},
      {"PhPy8MECoff","PhPy8MECoff","nominal"},
      {"PhH704","PhH704","nominal"},
      {"PhH713","PhH713","nominal"},
      {"hdamp","hdamp","nominal"},
      {"ISR_muR05_muF05_Var3cUp","hdamp","ISR_muR05_muF05_Var3cUp"},
      {"ISR_muR20_muF20_Var3cDown","nominal","ISR_muR20_muF20_Var3cDown"},
      {"ISR_muR20_muF10","nominal","ISR_muR20_muF10"},
      {"ISR_muR05_muF10","nominal","ISR_muR05_muF10"},
      {"ISR_muR10_muF20","nominal","ISR_muR10_muF20"},
      {"ISR_muR10_muF05","nominal","ISR_muR10_muF05"},
      //{"ISR_muR05_muF05","nominal","ISR_muR05_muF05"},
      //{"ISR_muR20_muF20","nominal","ISR_muR20_muF20"},
      //{"ISR_muR05_muF20","nominal","ISR_muR05_muF20"},
      //{"ISR_muR20_muF05","nominal","ISR_muR20_muF05"},
      {"Var3cUp","nominal","Var3cUp"},
      {"Var3cDown","nominal","Var3cDown"},
      {"FSR_muRfac_05","nominal","FSR_muRfac_05"},
      //{"FSR_muRfac_0625","nominal","FSR_muRfac_0625"},
      //{"FSR_muRfac_075","nominal","FSR_muRfac_075"},
      //{"FSR_muRfac_0875","nominal","FSR_muRfac_0875"},
      //{"FSR_muRfac_125","nominal","FSR_muRfac_125"},
      //{"FSR_muRfac_15","nominal","FSR_muRfac_15"},
      //{"FSR_muRfac_175","nominal","FSR_muRfac_175"},
      {"FSR_muRfac_20","nominal","FSR_muRfac_20"},
      {"CT14","nominal","CT14_000"},
      {"CT14_001","nominal","CT14_001"},
      {"CT14_002","nominal","CT14_002"},
      {"CT14_003","nominal","CT14_003"},
      {"CT14_004","nominal","CT14_004"},
      {"CT14_005","nominal","CT14_005"},
      {"CT14_006","nominal","CT14_006"},
      {"MMHT2014","nominal","MMHT2014_000"},
      {"MMHT2014_001","nominal","MMHT2014_001"},
      {"MMHT2014_002","nominal","MMHT2014_002"},
      {"MMHT2014_003","nominal","MMHT2014_003"},
      {"MMHT2014_004","nominal","MMHT2014_004"},
      {"MMHT2014_005","nominal","MMHT2014_005"},
      {"MMHT2014_006","nominal","MMHT2014_006"},
    };
    
    for(const std::vector<TString>& vec : modNames) histStore->WriteSignalModelingHistosND(fOutput,variable_name,vec[0],vec[1],vec[2]);
    //histStore->WriteSignalModelingHistosND(fOutput,variable_name,"Sherpa","Sherpa","nominal"); // Not available at the moment
    
  }

  //// Writing bootstrap histos
  //if(saveBootstrapHistos){
    //histStore->load_bootstrapped_histos();
    //path_to_output_file=mainDir+"/root." + mc_samples_production +"/" + "bootstrapND/";
    //gSystem->mkdir(path_to_output_file,true);
    //TFile* fBootstrap = new TFile(path_to_output_file+variable_name + "_" + level + ".root","RECREATE");
    //histStore->WriteBootstrapHistos(fBootstrap);
    //fBootstrap->Close();
    //delete fBootstrap;
  //}
  //histStore->delete_nominal_histos();
  fOutput->Close();
  delete fOutput;
  
  cout << "Everything ok!" << endl;
  return 0;
				  
}

static const char * USAGE =
    R"(Program to prepare rootfiles with histograms for all systematic branches. These rootfiles are inputs to unfolding chain.  

Usage:  
  PrepareSystematicHistosND [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.json]
  --configFiles <configfile>              Config file with file names. [default: TtbarDiffCrossSection/data/datasetnames.env]
  --configBinning <configfile>            Config file with binning definitions. [default: TtbarDiffCrossSection/data/unfolding_histosND_optimizedBinning.env]
  --variable <string>                  	  Variable name. [default: ttbar_y_vs_ttbar_mass_vs_t1_pt]
  --level <string>                        ParticleLevel or PartonLevel. [default: ParticleLevel]
  --outputDir <string>                    Relative path to output directory taken from mainDir/root.<mc_samples_production>/ [default: systematics_histosND]
  --saveBootstrapHistos <int>             Option to create rootfiles with Bootstrapped histograms. [default: 0] 
  --saveStressTestHistos <int>            Option to create rootfiles with stress-test histograms. [default: 0]
  --doRebin <int>                         Activates rebinning. [default: 0]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  TH1::AddDirectory(kFALSE);
  
  try {
    
    TString mainDir("");
    TString path_config_files("");
    TString path_config_lumi("");
    TString path_config_binning("");
    TString path_config_sys_names("");
    TString signal("");
    TString variable_name("");
    TString level("");
    TString systematicHistosPath("");
    bool saveBootstrapHistos(0);
    bool saveStressTestHistos(0);
    bool doRebin(0);
    int debugLevel(0);
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ path_config_files=args["--configFiles"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configFiles option is expected to be a string. Check input parameters."); }
	
      try{ path_config_binning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
	
      try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
      
      try{ systematicHistosPath=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
      try{ saveBootstrapHistos=args["--saveBootstrapHistos"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--saveBootstrapHistos option is expected to be an integer. Check input parameters."); }
	
      try{ saveStressTestHistos=args["--saveStressTestHistos"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--saveStressTestHistos option is expected to be an integer. Check input parameters."); }
	
      try{ doRebin=args["--doRebin"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--doRebin option is expected to be an integer. Check input parameters."); }
	
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    
    cout << endl << "Running PrepareSystematicHistosND for variable " << variable_name << " at " << level << "." << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
    
    return runPrepareSystematicHistosND( mainDir,
				       path_config_files,
				       path_config_lumi,
				       path_config_binning,
				       variable_name,
				       level,
				       systematicHistosPath,
				       saveBootstrapHistos,
				       saveStressTestHistos,
				       doRebin,
				       debugLevel);
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  return 0;
}
