
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TSystem.h"


using std::cout;
using std::endl;
using std::cerr;
void loadHistVector(TFile* file,vector<TH1D*>& histos,TString name);

int main(int nArg, char **arg) {
  cout << endl << "Running ProducePseudoexperiments for variable " << arg[5] << endl << endl;
  TH1::AddDirectory(kFALSE);
  try {
    if(nArg!=7){
      cout << "Error: Wrong number of input parameters!" << endl;
      throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    }
    
    TString mainDir=arg[1];
    TString path_config_files=arg[2];
    TString path_config_lumi=arg[3];
    TString signal=arg[4];
    TString variable_name=arg[5];
    TString level=arg[6];
    
    
    TFile* file_systematics= new TFile(mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root");
    if(file_systematics->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root"); 
      return -1;
    }
    
    
    TString shortName=variable_name;
    shortName.ReplaceAll("_fine","");
    
    const int nhistos=3;
    vector<TH1D*> histos(nhistos);
    
    loadHistVector(file_systematics,histos,shortName + "_signal_reco");
    
    
    //cout << nominal+"/" + shortName + "_signal_reco" << endl;
    for(int i=0;i<nhistos;i++)cout<< histos[i]->Integral() << endl;
    
    
    vector<TH1D*> pseudodata(nhistos);
    vector<TH1D*> multijetBackgroundFractions(nhistos);
    vector<TH1D*> otherBackgroundFractions(nhistos);
    
    loadHistVector(file_systematics,pseudodata,shortName + "_pseudodata");
    loadHistVector(file_systematics,multijetBackgroundFractions,shortName + "_bkg_Multijet");
    loadHistVector(file_systematics,otherBackgroundFractions,shortName + "_bkg_MC");
    for(int i=0;i<nhistos;i++)cout<< pseudodata[i]->Integral() << endl;
    for(int i=0;i<nhistos;i++)cout<< multijetBackgroundFractions[i]->Integral() << endl;
    for(int i=0;i<nhistos;i++)cout<< otherBackgroundFractions[i]->Integral() << endl;
    
    for(int i=0;i<nhistos;i++){
      multijetBackgroundFractions[i]->Divide(pseudodata[i]);
      otherBackgroundFractions[i]->Divide(pseudodata[i]);
      cout << multijetBackgroundFractions[i]->Integral()/multijetBackgroundFractions[i]->GetNbinsX() << endl;
      cout << otherBackgroundFractions[i]->Integral()/otherBackgroundFractions[i]->GetNbinsX() << endl;
      
      histos[i]->SetLineColor(i+1);
      histos[i]->SetMarkerColor(i+1);
      functions::DivideByBinWidth(histos[i]);
      NamesAndTitles::setXtitle(histos[i],shortName);
      histos[i]->SetYTitle("Differential cross-section");
      
      multijetBackgroundFractions[i]->SetLineColor(i+1);
      multijetBackgroundFractions[i]->SetMarkerColor(i+1);
      NamesAndTitles::setXtitle(multijetBackgroundFractions[i],shortName);
      multijetBackgroundFractions[i]->SetYTitle("Bkg fraction");
      
      otherBackgroundFractions[i]->SetLineColor(i+1);
      otherBackgroundFractions[i]->SetMarkerColor(i+1);
      NamesAndTitles::setXtitle(otherBackgroundFractions[i],shortName);
      otherBackgroundFractions[i]->SetYTitle("Bkg fraction");
      
      
    }
    
    
    TLegend *leg = new TLegend(0.58,0.58,0.95,0.9);
    leg->SetNColumns(1);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.1);
    
    leg->AddEntry(histos[0],"Nominal");
    leg->AddEntry(histos[1],"JMR down");
    leg->AddEntry(histos[2],"JMR up");
    
    TString dirname=mainDir+"/pdf/PlotsWithJMR/";
    gSystem->mkdir(dirname,true);
    TString lumi_string="";
    bool use_logscale=false;
    double y2min=0.951;
    double y2max=1.049;
    TString y2name="Shifted / Nominal";
    
    
    functions::plotHistograms(histos,leg,dirname,level+ "_" + shortName + ".pdf" ,histos[0],y2name,y2min,y2max,lumi_string,use_logscale);
    functions::plotHistograms(multijetBackgroundFractions,leg,dirname,level+ "_" + shortName + "_multijetFraction.pdf" ,multijetBackgroundFractions[0],y2name,0.901,1.099,lumi_string,use_logscale);
    functions::plotHistograms(otherBackgroundFractions,leg,dirname,level+ "_" + shortName + "_otherBkgFraction.pdf" ,otherBackgroundFractions[0],y2name,0.801,1.199,lumi_string,use_logscale);
    //functions::plotHistograms(vector<TH1D*>& hist,TLegend* leg,TString dirname,TString name,TH1D* hd, TString y2name, double y2min, double y2max, TString lumi,bool use_logscale)
    
    //functions::plotHistograms(relativeUncertaintiesRelative,leg,dirname,level+ "_" + name + "_rel.png" ,lumi_string,use_logscale);
	  
  }catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  cout << "Everything ok!" << endl;
  return 0;
}

void loadHistVector(TFile* file,vector<TH1D*>& histos,TString name){
  
  const TString JMR_Branche="LARGERJET_Weak_JET_Top_massRes_mass__1down";
  const TString nominal="nominal";
  histos.resize(3);
  histos[0] = (TH1D*)file->Get(nominal+"/" + name);
  histos[1] = (TH1D*)file->Get(JMR_Branche+"/" + name);
  
  histos[2]=(TH1D*)histos[0]->Clone();
  histos[2]->Add(histos[0]);
  histos[2]->Add(histos[1],-1);
  
  
  for(int i=0;i<3;i++)NamesAndTitles::setXtitle(histos[i],name);
  
}
