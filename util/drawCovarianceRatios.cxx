#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;


// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to make reco level plots from histograms stored in systematics_histos rootfiles.  

Usage:  
  drawReco [options]
  
Options:
  -h --help                                      Show help screen.
  --mainDir <mainDir>             	         Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	         Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --covariancesDir <covariancesDir>              Directory with rootfiles containing covariances. [default: covariances]
  --outputDir <outputDir>                        Directory to store plots. Will be created in maindir/pdf.<MC> . [default: covariancesPlots]
  --variable <string>                  	         Variable name. [default: leadingTop_pt]
  --level <string>                               ParticleLevel or PartonLevel. Option is used just to identify input rootfile. [default: ParticleLevel]
  --covarianceNameNumerator <covarianceNameNumerator>              Name of covariance matrix stored in rootfile. [default: covDataBasedStatSysAndSigMod]
  --covarianceNameDenominator <covarianceNameDenominator>              Name of covariance matrix stored in rootfile. [default: covDataBasedStatSysAndSigMod]
  --debugLevel <int>                             Option to control debug messages. Higher value means more messages. [default: 0]
)";

int drawCovarianceRatios(TString mainDir,TString covariancesDir,TString covarianceNameNumerator,TString covarianceNameDenominator,TString outputSubdir,TString path_config_lumi,TString variable_name,TString level,int debugLevel);

int main(int nArg, char **arg) {
  
  
  TString mainDir="";
  TString covariancesDir="";
  TString covarianceNameNumerator="";
  TString covarianceNameDenominator="";
  TString outputSubdir="";
  TString path_config_lumi="";
  TString variable_name="";
  TString level="";
  
  int debugLevel=0;
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
  
  try{
  
    try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
      
    try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      
    try{ covariancesDir=args["--covariancesDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covariancesDir option is expected to be a string. Check input parameters."); }
      
    try{ outputSubdir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
    try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
      
    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
    
    try{ covarianceNameNumerator=args["--covarianceNameNumerator"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covarianceNameNumerator option is expected to be a string. Check input parameters."); }
    
    try{ covarianceNameDenominator=args["--covarianceNameDenominator"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covarianceNameDenominator option is expected to be a string. Check input parameters."); }
    
    try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
      
  } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
  
  if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
  
  if(debugLevel>0){
    cout << endl << "Running drawReco for variable " << variable_name << "." << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
  }
  
  return drawCovarianceRatios(mainDir,covariancesDir,covarianceNameNumerator,covarianceNameDenominator,outputSubdir,path_config_lumi,variable_name,level,debugLevel);
}

int drawCovarianceRatios(TString mainDir,TString covariancesDir,TString covarianceNameNumerator,TString covarianceNameDenominator,TString outputSubdir,TString path_config_lumi,TString variable_name,TString level,int debugLevel){  
  try {
    
    SetAtlasStyle();
    TH1::AddDirectory(kFALSE);
    
    auto configlumi = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
    const TString mc_samples_production = configlumi->mc_samples_production();
    const TString lumi_string = configFunctions::getLumiString(*configlumi);
    const TString rootDir=(TString)"root."+mc_samples_production;
    const TString pdfDir=(TString)"pdf."+mc_samples_production;
    const TString pngDir=(TString)"png."+mc_samples_production;
    
    
    TString filename=mainDir + "/" + rootDir + "/" + covariancesDir + "/" + variable_name + "_" + level + ".root";
    if(debugLevel>0) std::cout << "Opening input file: " << filename << std::endl;
    TFile* inputFile= new TFile(filename);
    if(inputFile->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with covariance from " +filename); 
      return -1;
    }
    
    const TString fullCovarianceNameNumerator = variable_name + "_" + covarianceNameNumerator;
    const TString fullCovarianceNameDenominator = variable_name + "_" + covarianceNameDenominator;
    
    if(debugLevel>0) std::cout << "Loading covariance: " << fullCovarianceNameNumerator << std::endl;
    
    auto covariance = std::shared_ptr<TMatrixT<double> > {(TMatrixT<double>*)inputFile->Get(fullCovarianceNameNumerator)};
    if(covariance.get()==0){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading covariance " + fullCovarianceNameNumerator + " from " + filename); 
      return -1;
    }
    auto covarianceDenominator = std::shared_ptr<TMatrixT<double> > {(TMatrixT<double>*)inputFile->Get(fullCovarianceNameDenominator)};
    if(covarianceDenominator.get()==0){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading covariance " + fullCovarianceNameDenominator + " from " + filename); 
      return -1;
    }
    
    
    const int nbinsx = covariance->GetNcols();
    const int nbinsy = covariance->GetNrows();
    
    auto correlation = std::shared_ptr<TMatrixT<double>> {functions::makeCorrelationMatrix(*covariance)};
    auto correlationDenominator = std::shared_ptr<TMatrixT<double>> {functions::makeCorrelationMatrix(*covarianceDenominator)};
    
    
    auto histogramCorrelation = std::make_shared<TH2D>("histogram_correlation","",nbinsx,0,nbinsx,nbinsy,0,nbinsy);
    auto histogramCovariance = std::make_shared<TH2D>("histogram_covariance","",nbinsx,0,nbinsx,nbinsy,0,nbinsy);
    for(int i=0;i<nbinsx;i++) for(int j=0;j<nbinsy;j++){
      
      const double cov = (*covarianceDenominator)[i][j] != 0. ? (*covariance)[i][j]/(*covarianceDenominator)[i][j] : 0;
      const double cor = (*correlationDenominator)[i][j] != 0. ? (*correlation)[i][j]/(*correlationDenominator)[i][j] : 0;
      
      histogramCovariance->SetBinContent(i+1,j+1,cov);
      histogramCorrelation->SetBinContent(i+1,j+1,cor);
    }
    
    
    
    const TString outputDirPDF = mainDir + "/" +pdfDir + "/" + outputSubdir + "/" + level + "/"+ variable_name; 
    const TString outputDirPNG = mainDir + "/" +pngDir + "/" + outputSubdir + "/" + level + "/"+ variable_name;  
    
    gSystem->mkdir(outputDirPDF,true);
    gSystem->mkdir(outputDirPNG,true);
    
    gStyle->SetPaintTextFormat("4.2f");
   
    TCanvas *can=new TCanvas("can","canvas",0,0,600,600);
    can->cd();
    
    //Int_t colors[] = {0, 1, 2, 3, 4, 5, 6}; // #colors >= #levels - 1
    //gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
    //vector<double> levels = {-1.79e308, 1.17e-38,0.5,0.55,0.6,0.65,.7,.75,.8,.85, .90, 0.95, 1.00, 1.05, 1.10,1.15,1.2,1.25,1.3,1.35,1.4,1.45,1.5,1.6,1.7,1.8,1.9,2., 1.79e308};
    //histogramCovariance->SetContour(levels.size(), &levels[0]);
    //histogramCorrelation->SetContour(levels.size(), &levels[0]);
    
    
    
    histogramCovariance->GetZaxis()->SetRangeUser(0.5,2.);
    histogramCovariance->DrawClone("colz");
    histogramCovariance->GetZaxis()->SetRangeUser(-100,100);
    histogramCovariance->Draw("text same");
    
    can->SaveAs(outputDirPDF+"/Covariance_"+ covarianceNameNumerator + "_over_" + covarianceNameDenominator + ".pdf");
    can->SaveAs(outputDirPNG+"/Covariance_"+ covarianceNameNumerator + "_over_" + covarianceNameDenominator + ".png");
    
    histogramCorrelation->GetZaxis()->SetRangeUser(0.5,2.);
    histogramCorrelation->DrawClone("colz");
    histogramCorrelation->GetZaxis()->SetRangeUser(-100.,100.);
    histogramCorrelation->Draw("text same");
    
    can->SaveAs(outputDirPDF+"/Correlation_"+ covarianceNameNumerator + "_over_" + covarianceNameDenominator + ".pdf");
    can->SaveAs(outputDirPNG+"/Correlation_"+ covarianceNameNumerator + "_over_" + covarianceNameDenominator + ".png");
    
    
    
    
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  cout << "drawCovariance: Done." << endl;
  return 0;
}
