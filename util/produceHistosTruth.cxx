// RL, 20.4.2017

#include "TtbarDiffCrossSection/RunOverMiniTreeTruth.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 

using std::cout;
using std::endl;
using std::cerr;

int main(int nArg, char **arg) {

  cout << endl << endl << " ***** running  " << arg[0] << " ***** " << endl;
  if (nArg != 4){
    cout << "Wrong number of arguments!!!" << endl;
    return EXIT_FAILURE;
  }

  TString filelist = arg[1];
  TString output   = arg[2];
  int     nEvents  = atoi(arg[3]);


  cout << "filelist: " << filelist << endl;
  cout << "output: "   << output   << endl;
  cout << "nEvents: "  << nEvents  << endl;
  cout << endl;


  RunOverMiniTreeTruth* analysis = new RunOverMiniTreeTruth(filelist, output, nEvents);

  analysis->Execute();
  
  analysis->Finalise();

  return 0;
}
