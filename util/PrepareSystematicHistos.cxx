
#include "TtbarDiffCrossSection/FileStore.h"
#include "TtbarDiffCrossSection/HistStore.h"
#include "CovarianceCalculator/Pseudoexperiment.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TEnv.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;


int runPrepareSystematicHistos( TString mainDir,
				TString path_config_files,
				TString path_config_lumi,
				TString path_config_binning,
				TString variable_name,
				TString level,
				TString systematicHistosPath,
				bool saveBootstrapHistos,
				bool saveStressTestHistos,
				bool saveEFTHistos,
				bool doRebin,
				int debugLevel)
{
  delete gRandom;
  gRandom = new TRandom3(0);   
      
  cout << "saveBootstrapHistos: " <<saveBootstrapHistos << endl;
  cout << "saveStressTestHistos: " <<saveStressTestHistos << endl;
  cout << "saveEFTHistos: " <<saveEFTHistos << endl;
  
  auto config = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  
  bool prepareModelingHistos = config->doSignalModeling();
  const TString mc_samples_production = config->mc_samples_production();
  const TString nominalTreeName = config->nominalFolderName().c_str();
  
  FileStore* fileStore = new FileStore();
  
  fileStore->initialize(mainDir,debugLevel, path_config_files,  path_config_lumi, path_config_binning);
  TString path_to_output_file=mainDir +"/root." + mc_samples_production +"/" + systematicHistosPath+"/";
  gSystem->mkdir(path_to_output_file,true);
  TFile* fOutput = new TFile(path_to_output_file+variable_name + "_" + level + ".root","RECREATE");
  
  HistStore* histStore = new HistStore();
  histStore->initialize(fileStore,debugLevel,path_config_lumi,path_config_binning);
  histStore->doRebin(doRebin); // Activates rebinning
  histStore->useResolutionHistos(true);
  fOutput->cd();
  histStore->load_nominal_histos(variable_name,level);
  histStore->WriteNominalHistos(fOutput);
  histStore->WriteAllSysHistos(fOutput);
  if(prepareModelingHistos) {
    fileStore->load_special_tfiles();
    vector<vector<TString>> modNames = { 
      {"ME","ME",nominalTreeName},
      {"PhPy8MECoff","PhPy8MECoff",nominalTreeName},
      {"PhH704","PhH704",nominalTreeName},
      {"PhH713","PhH713",nominalTreeName},
      {"hdamp","hdamp",nominalTreeName},
      {"ISR_muR20_muF10",nominalTreeName,"ISR_muR20_muF10"},
      {"ISR_muR05_muF10",nominalTreeName,"ISR_muR05_muF10"},
      {"ISR_muR10_muF20",nominalTreeName,"ISR_muR10_muF20"},
      {"ISR_muR10_muF05",nominalTreeName,"ISR_muR10_muF05"},
      {"ISR_muR05_muF05",nominalTreeName,"ISR_muR05_muF05"},
      {"ISR_muR20_muF20",nominalTreeName,"ISR_muR20_muF20"},
      {"ISR_muR05_muF20",nominalTreeName,"ISR_muR05_muF20"},
      {"ISR_muR20_muF05",nominalTreeName,"ISR_muR20_muF05"},
      {"Var3cUp",nominalTreeName,"Var3cUp"},
      {"Var3cDown",nominalTreeName,"Var3cDown"}
    };

    
    for(const vector<TString>& vec : modNames)
      histStore->WriteSignalModelingHistos(fOutput,variable_name,vec[0],vec[1],vec[2]);
    //histStore->WriteSignalModelingHistos(fOutput,variable_name,"Sherpa","Sherpa","nominal"); // Not available at the moment
  }

  // Writing bootstrap histos
  if(saveBootstrapHistos){
    histStore->load_bootstrapped_histos();
    path_to_output_file=mainDir+"/root."+mc_samples_production+"/bootstrap/";
    gSystem->mkdir(path_to_output_file,true);
    TFile* fBootstrap = new TFile(path_to_output_file+variable_name + "_" + level + ".root","RECREATE");
    histStore->WriteBootstrapHistos(fBootstrap);
    fBootstrap->Close();
    delete fBootstrap;
  }
  
  if(saveEFTHistos && level=="PartonLevel") {
    path_to_output_file=mainDir+"/root."+mc_samples_production+"/EFT/";
    gSystem->mkdir(path_to_output_file,true);
    auto fEFT = std::make_unique<TFile>(path_to_output_file+variable_name + "_" + level + ".root","RECREATE");
    histStore->WriteEFTHistos(fEFT.get());
    fEFT->Close();
  }
  
  
  
  histStore->delete_nominal_histos();
  fOutput->Close();
  delete fOutput;
  
  cout << "Everything ok!" << endl;
  return 0;
      
}



// This is not just a print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to prepare rootfiles with histograms for all systematic branches. These rootfiles are inputs to unfolding chain.  

Usage:  
  PrepareSystematicHistos [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.json]
  --configFiles <configfile>              Config file with file names. [default: TtbarDiffCrossSection/data/datasetnames.env]
  --configBinning <configfile>            Config file with binning definitions. [default: TtbarDiffCrossSection/data/unfolding_histos.env]
  --variable <string>                  	  Variable name. [default: total_cross_section]
  --level <string>                        ParticleLevel or PartonLevel. [default: ParticleLevel]
  --outputDir <string>                    Relative path to output directory from mainDir/root.<mc_samples_production>/ [default: systematics_histos]
  --saveBootstrapHistos <int>             Option to create rootfiles with Bootstrapped histograms. [default: 0] 
  --saveStressTestHistos <int>            Option to create rootfiles with stress-test histograms. [default: 0]
  --saveEFTHistos <int>                   Option to create rootfiles with EFT histograms. [default: 0]
  --doRebin <int>                         Activates rebinning. [default: 0]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";

int main(int nArg, char **arg) {
  
  TH1::AddDirectory(kFALSE);  
  try {
    
    TString mainDir("");
    TString path_config_files("");
    TString path_config_lumi("");
    TString path_config_binning("");
    TString variable_name("");
    TString level("");
    TString systematicHistosPath("");
    bool saveBootstrapHistos=0;
    bool saveStressTestHistos=0;
    bool saveEFTHistos=0;
    bool doRebin=0;
    int debugLevel=0;
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ path_config_files=args["--configFiles"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configFiles option is expected to be a string. Check input parameters."); }
	
      try{ path_config_binning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
	
      try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
	
      try{ systematicHistosPath=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
	
      try{ saveBootstrapHistos=args["--saveBootstrapHistos"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--saveBootstrapHistos option is expected to be an integer. Check input parameters."); }
	
      try{ saveStressTestHistos=args["--saveStressTestHistos"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--saveStressTestHistos option is expected to be an integer. Check input parameters."); }
	
      try{ saveEFTHistos=args["--saveEFTHistos"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--saveStressTestHistos option is expected to be an integer. Check input parameters."); }
	
      try{ doRebin=args["--doRebin"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--doRebin option is expected to be an integer. Check input parameters."); }
	
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
     
      if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
      
      cout << endl << "Running PrepareSystematicHistos for variable " << variable_name << " at " << level << "." << endl;
      cout << "Main directory is " << mainDir << "." << endl << endl;
      
    return runPrepareSystematicHistos( mainDir,
				       path_config_files,
				       path_config_lumi,
				       path_config_binning,
				       variable_name,
				       level,
				       systematicHistosPath,
				       saveBootstrapHistos,
				       saveStressTestHistos,
				       saveEFTHistos,
				       doRebin,
				       debugLevel);
				       
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
}
