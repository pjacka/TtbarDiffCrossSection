
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "CovarianceCalculator/SystematicHistos.h"
#include "CovarianceCalculator/PseudoexperimentsCalculator.h"
#include "CovarianceCalculator/CovarianceCalculator.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"


using std::cout;
using std::endl;
using std::cerr;

int main(int nArg, char **arg) {
  cout << endl << "Running ProducePseudoexperiments for variable " << arg[6] << endl << endl;
  TH1::AddDirectory(kFALSE);
  SetAtlasStyle();
  try {
    if(nArg!=9) throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    delete gRandom;
    gRandom = new TRandom3(0);
    TString mainDir=arg[1];
    TString path_config_files=arg[2];
    TString path_config_lumi=arg[3];
    TString path_config_binning=arg[4];
    TString path_config_sysnames=arg[5];
    TString signal=arg[6];
    TString level=arg[7];
    const bool useBootstrapHistos=atoi(arg[8]);
    
    cout << "useBootstrapHistos: " << useBootstrapHistos << endl;
    
    //if(level.Contains("Parton")){
      //spectrums.push_back("topCandidate_pt");pretty_names.push_back("p_{T}^{t}");
      //spectrums.push_back("randomTop_y");pretty_names.push_back("|y^{t}|");
    //}
    //spectrums.push_back("leadingTop_pt");pretty_names.push_back("p_{T}^{t,1}");
    //spectrums.push_back("leadingTop_y");pretty_names.push_back("|y^{t,1}|");
    //spectrums.push_back("subleadingTop_pt");pretty_names.push_back("p_{T}^{t,2}");
    //spectrums.push_back("subleadingTop_y");  pretty_names.push_back("|y^{t,2}|");
    //spectrums.push_back("ttbar_mass");pretty_names.push_back("m^{t#bar{t}}");
    //spectrums.push_back("ttbar_pt");pretty_names.push_back("p_{T}^{t#bar{t}}");
    //spectrums.push_back("ttbar_y");pretty_names.push_back("|y^{t#bar{t}}|");
    //spectrums.push_back("chi_ttbar"); pretty_names.push_back("#chi^{t#bar{t}}"); 
    //spectrums.push_back("ttbar_deltaphi"); pretty_names.push_back("#Delta #phi^{t#bar{t}}"); 
    //spectrums.push_back("pout");pretty_names.push_back("|p_{out}^{t#bar{t}}|");
    //spectrums.push_back("y_boost");pretty_names.push_back("y_{B}^{t#bar{t}}");
    //spectrums.push_back("cos_theta_star");pretty_names.push_back("cos#theta^{*}"); 
    //spectrums.push_back("H_tt_pt");pretty_names.push_back("H_{T}^{t#bar{t}}");
    
    
    vector<TString> variables;
    //"total_cross_section_fine"
    if(level.Contains("Parton")){
      variables.push_back("randomTop_pt");
      variables.push_back("randomTop_y");
    }
    variables.push_back("leadingTop_pt");
    variables.push_back("leadingTop_y");
    variables.push_back("subleadingTop_pt");
    variables.push_back("subleadingTop_y");
    variables.push_back("ttbar_mass");
    variables.push_back("ttbar_pt");
    variables.push_back("ttbar_y");
    variables.push_back("chi_ttbar");
    variables.push_back("ttbar_deltaphi");
    variables.push_back("pout");
    variables.push_back("y_boost");
    variables.push_back("cos_theta_star");
    variables.push_back("H_tt_pt");
    //variables.push_back("z_tt_pt");
    //variables.push_back("y_star");
    //variables.push_back("inclusive_top_pt");
    //variables.push_back("inclusive_top_y");

    
    const int nVariables=variables.size();
    
    const TString nominal="nominal/";
    auto ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
		
    double lumi= configFunctions::getLumi(*ttbarConfig);
    double lumi_unit=1.;
    lumi/=lumi_unit;
    
    vector<SystematicHistos*> sysHistos(nVariables);
    
    cout << "Loading histograms for single variables: " << endl;
    for(int iVariable=0;iVariable<nVariables;iVariable++){
      TString variable_name=variables[iVariable];
      TString variable=variable_name;
      cout << variable << endl;
      TFile* file_systematics= new TFile(mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root");
      if(file_systematics->IsZombie()){
	throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root"); 
	return -1;
      }
      variable.ReplaceAll("_fine","");
      //TString chain_name="nominal";
      
      sysHistos[iVariable]= new SystematicHistos(file_systematics,variable,*ttbarConfig);
      
      
      
      //if(useBootstrapHistos){
	//TFile* fBootstrapHistos = new TFile(mainDir+"/root/bootstrap/" + variable_name + "_" + level + ".root");
	//if(fBootstrapHistos->IsZombie()){
	  //throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with bootstrapped histograms from " + mainDir+"/root/bootstrap/" + variable_name + "_" + level + ".root"); 
	  //return -1;
	//}
      //}
      
      delete file_systematics;
    }
    cout << "Loading done." << endl;
    cout << "Creating systematic histos with concatenated histograms." << endl;
    SystematicHistos* concatenatedSysHistos = new SystematicHistos(sysHistos);
    TFile* f=new TFile(mainDir+"/root/systematics_histos/" + "concatenated_" + level + ".root","RECREATE");
    concatenatedSysHistos->write(f);
    cout << "Concatenated systematic histos created." << endl;
    
    cout << "Loading bootstrapped histos" << endl;
    vector<TH1DBootstrap*> data_boots(nVariables);
    vector<TH1DBootstrap*> signal_boots(nVariables);
    vector<TH1DBootstrap*> bkg_MC_boots(nVariables);
    vector<TH1DBootstrap*> bkg_Multijet_boots(nVariables);
    vector<TH1DBootstrap*> bkg_all_boots(nVariables);
    vector<TH1DBootstrap*> pseudodata_boots(nVariables);
    vector<vector<TH1DBootstrap*> > sidebandRegions_MC_boots(nVariables);
    vector<vector<TH1DBootstrap*> > sidebandRegions_data_boots(nVariables);
    for(int iVariable=0;iVariable<nVariables;iVariable++){
      TString variable_name=variables[iVariable];
      TString variable=variable_name;
      variable.ReplaceAll("_fine","");
      
      cout << variable << endl;
      TFile* file_bootstrap= new TFile(mainDir+"/root/bootstrap/" + variable_name + "_" + level + ".root");
      if(file_bootstrap->IsZombie()){
	throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root"); 
	return -1;
      }
      data_boots[iVariable] = (TH1DBootstrap*)file_bootstrap->Get(nominal + variable +"_boots_" + "data");
      signal_boots[iVariable] = (TH1DBootstrap*)file_bootstrap->Get(nominal + variable +"_boots_" + "signal");
      bkg_MC_boots[iVariable] = (TH1DBootstrap*)file_bootstrap->Get(nominal + variable +"_boots_" + "bkg_MC");
      bkg_Multijet_boots[iVariable] = (TH1DBootstrap*)file_bootstrap->Get(nominal + variable +"_boots_" + "bkg_Multijet");
      bkg_all_boots[iVariable] = (TH1DBootstrap*)file_bootstrap->Get(nominal + variable +"_boots_" + "bkg_all");
      pseudodata_boots[iVariable] = (TH1DBootstrap*)file_bootstrap->Get(nominal + variable +"_boots_" + "pseudodata");
      //cout << signal_boots[iVariable]->GetNominal()->Integral() << endl;
      for(char x='A';x<='O';x++){
	//cout << nominal + "SidebandRegions/" + variable +"_boots_" + "MC_" + x << endl;
	sidebandRegions_MC_boots[iVariable].push_back((TH1DBootstrap*)file_bootstrap->Get(nominal + "SidebandRegions/" + variable +"_MC_" + x));
	sidebandRegions_data_boots[iVariable].push_back((TH1DBootstrap*)file_bootstrap->Get(nominal + "SidebandRegions/" + variable +"_data_" + x));
      
      }
      
      delete file_bootstrap;
    }
    cout << "Bootstrapped histos loaded" << endl;
    cout << "Creating concatenated bootstrap histograms" << endl;
    TH1DBootstrap* concatenated_data_boots = functions::createEmptyConcatenatedHistogram(data_boots);
    TH1DBootstrap* concatenated_signal_boots = new TH1DBootstrap(*concatenated_data_boots);
    TH1DBootstrap* concatenated_bkg_MC_boots = new TH1DBootstrap(*concatenated_data_boots);
    TH1DBootstrap* concatenated_bkg_Multijet_boots = new TH1DBootstrap(*concatenated_data_boots);
    TH1DBootstrap* concatenated_bkg_all_boots = new TH1DBootstrap(*concatenated_data_boots);
    TH1DBootstrap* concatenated_pseudodata_boots = new TH1DBootstrap(*concatenated_data_boots);
    
    vector<TH1DBootstrap*> concatenated_sidebandRegions_MC_boots(15);
    vector<TH1DBootstrap*> concatenated_sidebandRegions_data_boots(15);
    
    for(int i=0;i<15;i++){
      concatenated_sidebandRegions_MC_boots[i] = new TH1DBootstrap(*concatenated_data_boots);
      concatenated_sidebandRegions_data_boots[i] = new TH1DBootstrap(*concatenated_data_boots);
    }
    
    cout << "Filling concatenated bootstrap histograms" << endl;
    
    
    functions::FillConcatenatedHistogram(concatenated_data_boots,data_boots);
    functions::FillConcatenatedHistogram(concatenated_signal_boots,signal_boots);
    functions::FillConcatenatedHistogram(concatenated_bkg_MC_boots,bkg_MC_boots);
    functions::FillConcatenatedHistogram(concatenated_bkg_Multijet_boots,bkg_Multijet_boots);
    functions::FillConcatenatedHistogram(concatenated_bkg_all_boots,bkg_all_boots);
    functions::FillConcatenatedHistogram(concatenated_pseudodata_boots,pseudodata_boots);
    
    vector<TH1DBootstrap*> helpvec(nVariables);
    //for(int iVariable=0;iVariable<nVariables;iVariable++)cout << iVariable << " " << sidebandRegions_MC_boots[iVariable].size() << " " << sidebandRegions_data_boots[iVariable].size() << endl; 
    for(int i=0;i<15;i++){
      for(int iVariable=0;iVariable<nVariables;iVariable++)helpvec[iVariable]=sidebandRegions_MC_boots[iVariable][i];
      functions::FillConcatenatedHistogram(concatenated_sidebandRegions_MC_boots[i],helpvec);
      for(int iVariable=0;iVariable<nVariables;iVariable++)helpvec[iVariable]=sidebandRegions_data_boots[iVariable][i];
      functions::FillConcatenatedHistogram(concatenated_sidebandRegions_data_boots[i],helpvec);
    }
      
    cout << "Concatenated bootstrap histos created." << endl;
    cout << "Writing concatenated boostrap histograms" << endl;
    TFile* file_bootstrap= new TFile(mainDir+"/root/bootstrap/" + "concatenated_fine" + "_" + level + ".root","RECREATE");
    file_bootstrap->mkdir(nominal);
    file_bootstrap->cd(nominal);
    concatenated_data_boots->Write("concatenated_boots_data");
    concatenated_signal_boots->Write("concatenated_boots_signal");
    concatenated_bkg_MC_boots->Write("concatenated_boots_bkg_MC");
    concatenated_bkg_Multijet_boots->Write("concatenated_boots_bkg_Multijet");
    concatenated_bkg_all_boots->Write("concatenated_boots_bkg_all");
    concatenated_pseudodata_boots->Write("concatenated_boots_pseudodata");
    
    
    file_bootstrap->mkdir(nominal+"SidebandRegions");
    file_bootstrap->cd(nominal+"SidebandRegions");
    char x='A';
    for(int i=0;i<15;i++){
      concatenated_sidebandRegions_MC_boots[i]->Write((TString)"concatenated_MC_" + x);
      concatenated_sidebandRegions_data_boots[i]->Write((TString)"concatenated_data_" + x);
      x++;
    }
    file_bootstrap->Close();
    cout << "Concatenated bootstrap histograms had been written." << endl;
    
    
    
	  
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  cout << "Everything ok!" << endl;
  return 0;
}

