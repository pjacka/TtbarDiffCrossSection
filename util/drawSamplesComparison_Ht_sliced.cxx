// Peter Berta, 15.10.2015

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"

using namespace std;

int main(int nArg, char **arg) {
  if(nArg!=6){
    cout << "Error: Wrong number of input parameters!" << endl;
    return -1;
  }
  TH1::AddDirectory(kFALSE);
  TString mainDir=arg[1];
  TString outputSubdir=arg[2];
  TString path_config_files=arg[3];
  TString path_config_lumi=arg[4];
  TString variable = arg[5];
  
  try {
    SetAtlasStyle();
    TString ttbar_path = gSystem->Getenv("TTBAR_PATH");
    
    std::unique_ptr<NtuplesAnalysisToolsConfig> ttbarConfig= std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
    std::unique_ptr<TEnv> configFiles = std::make_unique<TEnv>(path_config_files);
    
    TString mc_samples_production = ttbarConfig->mc_samples_production();
    double lumi = configFunctions::getLumi(*ttbarConfig);
   
    TString lumi_string = configFunctions::getLumiString(*ttbarConfig).c_str();
    
    cout << "Using lumi: " << lumi_string << endl;
    
    
    shared_ptr<TFile> fSignalSliced( TFile::Open(mainDir + "/" + mc_samples_production + "." +     configFiles->GetValue("ttbar_signal_nominal_filename","")) );
    
    //shared_ptr<TFile> fSignalSliced( TFile::Open(mainDir + "/" + mc_samples_production + "." +     configFiles->GetValue("3HT_combined_filename_OLD","")) );
    //shared_ptr<TFile> fSignalSliced( TFile::Open(mainDir + "/" + mc_samples_production + "." +     configFiles->GetValue("3HT_combined_filename_NEW","")) );
    //shared_ptr<TFile> fSignalSliced( TFile::Open(mainDir + "/" + mc_samples_production + "." +     configFiles->GetValue("low_incl_2HT_filename_NEW","")) );

    
    shared_ptr<TFile> fSignalFlat( TFile::Open(mainDir + "/" + mc_samples_production + "." +       configFiles->GetValue("signal_410471_filename","")) );
    //shared_ptr<TFile> fSignalFlat( TFile::Open(mainDir + "/" + mc_samples_production + "." +       configFiles->GetValue("signal_410471_filename_NEW","")) );
    //shared_ptr<TFile> fSignalFlat( TFile::Open(mainDir + "/" + mc_samples_production + "." +       configFiles->GetValue("signal_410471_filename_OLD","")) );
    //shared_ptr<TFile> fSignalFlat( TFile::Open(mainDir + "/" + mc_samples_production + "." +       configFiles->GetValue("low_incl_filename_NEW","")) );
    //shared_ptr<TFile> fSignalFlat( TFile::Open(mainDir + "/" + mc_samples_production + "." +       configFiles->GetValue("low_incl_2HT_filename_NEW","")) ); 

    shared_ptr<TFile> fSignal_500_1000( TFile::Open(mainDir + "/" + mc_samples_production + "." +  configFiles->GetValue("signal_410428_filename","")) );
    shared_ptr<TFile> fSignal_1000_1500( TFile::Open(mainDir + "/" + mc_samples_production + "." + configFiles->GetValue("signal_410429_filename","")) );
    shared_ptr<TFile> fSignal_1500( TFile::Open(mainDir + "/" + mc_samples_production + "." +      configFiles->GetValue("signal_410444_filename","")) );
   
   
    if(fSignalFlat->IsZombie())throw TtbarDiffCrossSection_exception("Error: Signal flat sample does not exist");
    if(fSignalSliced->IsZombie())throw TtbarDiffCrossSection_exception("Error: Signal sliced sample does not exist");
    
    TString directory = "nominal/Leading_1t_1b_Recoil_1t_1b_tight/RecoLevel/";
    if(variable.EndsWith("RecoLevel"))directory="nominal/Leading_1t_1b_Recoil_1t_1b_tight/unfolding/";
    
    TH1D *h_flat( (TH1D*)fSignalFlat->Get(directory+variable) ); 
    cout << "Integral: " << h_flat->Integral() << endl;
    
    TH1D *h_sliced( (TH1D*)fSignalSliced->Get(directory+variable) ); 
    cout << "Integral: " << h_sliced->Integral() << endl;
 
    TH1D *h_500_1000( (TH1D*)fSignal_500_1000->Get(directory+variable) ); 
    cout << "Integral: " << h_500_1000->Integral() << endl;
 
    TH1D *h_1000_1500( (TH1D*)fSignal_1000_1500->Get(directory+variable) ); 
    cout << "Integral: " << h_1000_1500->Integral() << endl;

    TH1D *h_1500( (TH1D*)fSignal_1500->Get(directory+variable) ); 
    cout << "Integral: " << h_1500->Integral() << endl;

    vector<TString> directories;
    //directories.push_back("nominal/Leading_0t_1b_Recoil_1t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_0b_Recoil_1t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_1b_Recoil_0t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_1b_Recoil_1t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_0b_Recoil_1t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_1b_Recoil_0t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_1b_Recoil_1t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_0b_Recoil_0t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_0b_Recoil_1t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_1b_Recoil_0t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_0b_Recoil_0t_1b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_0b_Recoil_1t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_1b_Recoil_0t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_1t_0b_Recoil_0t_0b_tight/RecoLevel/"); 
    //directories.push_back("nominal/Leading_0t_0b_Recoil_0t_0b_tight/RecoLevel/"); 



    for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i
       TH1D *h_flat_region( (TH1D*)fSignalFlat->Get(*i+variable) );
       h_flat->Add(h_flat_region);
       cout << "h_flat->Integral: " << h_flat_region->Integral() << "  h_flat_all_regions->Integral(): "  << h_flat->Integral() << endl;
    }

    for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i
       TH1D *h_sliced_region( (TH1D*)fSignalSliced->Get(*i+variable) );
       h_sliced->Add(h_sliced_region);
       cout << "h_sliced->Integral: " << h_sliced_region->Integral() << "  h_sliced_all_regions->Integral(): "  << h_sliced->Integral() << endl;
    }

      for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i
       TH1D *h_500_1000_region( (TH1D*)fSignal_500_1000->Get(*i+variable) );
       h_500_1000->Add(h_500_1000_region);
       cout << "h_500_1000->Integral: " << h_500_1000_region->Integral() << "  h_500_1000_all_regions->Integral(): "  << h_500_1000->Integral() << endl;
    }

      for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i
       TH1D *h_1000_1500_region( (TH1D*)fSignal_1000_1500->Get(*i+variable) );
       h_1000_1500->Add(h_1000_1500_region);
       cout << "h_1000_1500->Integral: " << h_1000_1500_region->Integral() << "  h_1000_1500_all_regions->Integral(): "  << h_1000_1500->Integral() << endl;
    }

      for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i
       TH1D *h_1500_region( (TH1D*)fSignal_1500->Get(*i+variable) );
       h_1500->Add(h_1500_region);
       cout << "h_1500->Integral: " << h_1500_region->Integral() << "  h_1500_all_regions->Integral(): "  << h_1500->Integral() << endl;
    }


    //shared_ptr<TH1D> h_flat( (TH1D*)fSignalFlat->Get(directory+variable) );
    //shared_ptr<TH1D> h_sliced    ( (TH1D*)fSignalSliced->Get(directory+variable) );
    //shared_ptr<TH1D> h_500_1000  ( (TH1D*)fSignal_500_1000->Get(directory+variable) );
    //shared_ptr<TH1D> h_1000_1500 ( (TH1D*)fSignal_1000_1500->Get(directory+variable) );
    //shared_ptr<TH1D> h_1500      ( (TH1D*)fSignal_1500->Get(directory+variable) );
   
    //vector<TH1D*> histos = {h_sliced.get(),h_flat.get(),h_500_1000.get(),h_1000_1500.get(),h_1500.get() };
    //vector<TH1D*> histos = {h_sliced.get(),h_flat.get()}; 
    vector<TH1D*> histos = {h_sliced,h_flat,h_500_1000,h_1000_1500,h_1500 };
    //vector<TH1D*> histos = {h_sliced,h_flat};
    const int nhistos=histos.size();
    for(int i=0;i<nhistos;i++){
      functions::DivideByBinWidth(histos[i]);
      histos[i]->Scale(1./lumi);
      int color=i+1;
      if(i>3)color+=1; 
      
      histos[i]->SetLineColor(color);
      histos[i]->SetLineWidth(2);
      histos[i]->SetMarkerColor(color);
      histos[i]->SetLineStyle(2);


      histos[i]->SetMaximum(1e4);
      histos[i]->GetXaxis()->SetLimits(0.4,3.);

      histos[i]->GetYaxis()->SetTitle("#frac{d#sigma}{dx} [unit^{-1}]");
    }
  
    TString outputDirPDF = mainDir + "/pdf." + mc_samples_production + "/" + outputSubdir + "/"; 
    TString outputDirPNG = mainDir + "/png." + mc_samples_production + "/" + outputSubdir + "/"; 
    gSystem->mkdir(outputDirPDF,true);
    gSystem->mkdir(outputDirPNG,true);
    
    //auto c = make_shared<TCanvas> ("c","");
    
  
    //h_flat->Draw();
    
    //h_sliced->Draw("Same");
    
    
    
    
    
    //c->SaveAs(outputDirPDF + variable + ".pdf");
    //c->SaveAs(outputDirPNG + variable + ".png");
    
    
    //auto leg = make_shared<TLegend>(0.6,0.5,0.9,0.8);
    auto leg = make_shared<TLegend>(0.62,0.53,0.92,0.83);
    leg->SetNColumns(1)   ;
    leg->SetFillColor(0)  ;
    leg->SetFillStyle(0)  ;
    leg->SetBorderSize(0) ;
    leg->SetTextFont(42)  ;
    leg->SetTextSize(0.06);
    
    leg->AddEntry(h_sliced,"Ht-sliced");
    leg->AddEntry(h_flat,"Nominal H_{T}<1 TeV");
    //leg->AddEntry(histos[2],"0.5 <Ht < 1.0 [TeV]");
    leg->AddEntry(histos[3],"1.0 < Ht < 1.5 [TeV]");
    leg->AddEntry(histos[4],"1.5 < Ht [TeV]");
    
    TString y2name="410471 / Ht-sliced";
    //double y2min=0.701;
    //double y2max=1.499;
    double y2min=0.;
    double y2max=1.2;
   
    bool use_logscale=false;
    
    functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+".pdf",histos[0], y2name, y2min, y2max, "",use_logscale);
    functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_log.pdf",histos[0], y2name, y2min, y2max, "",true);
    functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+".png",histos[0], y2name, y2min, y2max, "",use_logscale);
    functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_log.png",histos[0], y2name, y2min, y2max, "",true);
    
    
        
    
  }catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  cout << "drawSamplesComparison: Done." << endl;
  return 0;
}
