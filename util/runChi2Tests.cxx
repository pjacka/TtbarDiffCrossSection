#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision                                                                                                            

#include "TString.h"
#include "TH1D.h"
#include "TFile.h"
#include "TSystem.h"
#include "TMatrixT.h"
#include "TMath.h"
#include "TRandom3.h"

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
//#include "HelperFunctions/plottingFunctions.h"
//#include "HelperFunctions/stringFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
//#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
//#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
//#include "HelperFunctions/HistogramNDto1DConverter.h"
#include "CovarianceCalculator/Chi2TestMaker.h"

#include "docopt.h"

using namespace std;
int debug;

struct PredictionConfig{
  TString folderName{"nominal"};
  TString histName{"signal_truth"};
  TString latexName{"SetMe"};
  TString PDFSet{"SetMe"};
};

void printTableHeading(ofstream& table, vector<PredictionConfig>& predictionConfigs);
void printChi2Result(ofstream& table, const TString& variable, const vector<double>& chi2, const vector<int>& ndfs, const vector<double>& pValues);
void printChi2ResultPlot(const TString& variable, const vector<double>& chi2, const vector<int>& ndfs, const vector<double>& pValues);
void finalizeTable(ofstream& table);

std::pair<TH1D*,TMatrixT<double>> getMatrixPrediction(TFile* f,const TString& variableName, bool isNormalized,bool is2D);


int runChi2Tests(const TString& path, const TString& pathTTbarConfig, const TString& outputSubDir,  const TString& level,
				    const TString& systematicHistosPath, const TString& covariancesDir,
				    const TString& covUncName);

static const char * USAGE =
    R"(Program to run theory vs data comparison using chi2 tests. 

Usage:  
  runChi2Tests [options]
  
Options:
  -h --help                         Show help screen.
  --mainDir <mainDir>               Path to main output directory. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	         Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --outputSubDir <outputSubDir>	    Output subdirectory to save plots. [default: TheoryVsDataComparison]
  --level <string>                  ParticleLevel or PartonLevel. [default: ParticleLevel]
  --systematicsHistosDir <systematicsHistosDir>   Directory with systematic histos rootfiles. [default: systematics_histos]
  --covariancesDir <covariancesDir>              Directory with rootfiles containing covariances. [default: covariances]
  --covUncName <covUncName>   Full covariance matrix name. [default: covDataBasedStatSysAndSigMod]
  --debugLevel <int>                Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  
  TString path      = "";
  TString pathTTbarConfig="";
  TString outputSubDir = "";
  TString level = "";
  TString systematicHistosPath="";
  TString covariancesDir="";
  TString covUncName = "";
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true);  // Using docopt package to read parameters
  try{
    try{ path=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
    
    try{ pathTTbarConfig=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
    
    try{ outputSubDir=args["--outputSubDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputSubDir option is expected to be a string. Check input parameters."); }

    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }

    try{ systematicHistosPath=args["--systematicsHistosDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--systematicsHistosDir option is expected to be a string. Check input parameters."); }
    
    try{ covariancesDir=args["--covariancesDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covariancesDir option is expected to be a string. Check input parameters."); }
    
    try{ covUncName=args["--covUncName"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covUncName option is expected to be a string. Check input parameters."); }
     
    try{ debug = args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is not an integer. Check input parameters."); }
  
  
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }  

  path.ReplaceAll("$TtbarDiffCrossSection_output_path",gSystem->Getenv("TtbarDiffCrossSection_output_path"));

  return runChi2Tests(path, pathTTbarConfig, outputSubDir, level, systematicHistosPath, covariancesDir,covUncName);
}


int runChi2Tests(const TString& path, const TString& pathTTbarConfig, const TString& outputSubDir,
				    const TString& level, const TString& systematicHistosPath, const TString& covariancesDir,
				    const TString& covUncName) {

  TH1::AddDirectory(false);
  
  delete gRandom;
  gRandom = new TRandom3(42);
  
  auto ttbarConfig= std::make_unique<NtuplesAnalysisToolsConfig>(pathTTbarConfig);
  
  TString levelShort = level;
  levelShort.ReplaceAll("Level","");
  
  vector<TString> variables { 
    "total_cross_section",
    "randomTop_pt",
    "randomTop_y",
    "t1_pt",
    "t1_y",
    "t2_pt",
    "t2_y",
    "ttbar_mass",
    "ttbar_pt",
    "ttbar_y",
    "chi_ttbar",
    "y_boost",
    "pout",
    "H_tt_pt",
    "ttbar_deltaphi",
    "cos_theta_star",
    "t1_pt_vs_t2_pt",
    "t1_y_vs_t2_y",
    "t1_y_vs_t1_pt",
    "t2_y_vs_t2_pt",
    "t1_pt_vs_ttbar_pt",
    "t1_pt_vs_ttbar_mass",
    "ttbar_y_vs_t1_pt",
    "ttbar_y_vs_t1_y",
    "t1_y_vs_ttbar_mass",
    "ttbar_y_vs_ttbar_mass",
    "ttbar_pt_vs_ttbar_mass",
    "ttbar_y_vs_ttbar_pt",
    "ttbar_y_vs_ttbar_mass_vs_t1_pt"
  };
			      

  const int nvariables=variables.size();
  

  const TString mcSamplesProduction = ttbarConfig->mc_samples_production();
  
  double lumi = configFunctions::getLumi(*ttbarConfig);
  if(lumi < 0.) {
    cout << "Unknown mc samples production: " << mcSamplesProduction << endl;
    exit(-1);
  }
  const TString lumiString = configFunctions::getLumiString(*ttbarConfig);
  
  const TString rootDir = (TString)"root."+mcSamplesProduction;
  const TString texDir = (TString)"tex."+mcSamplesProduction;
  
  const TString outputPath = path + "/" + texDir +"/" + outputSubDir;
  gSystem->mkdir(outputPath,true);
  if(debug>0) std::cout << "Output latex tables will be saved in: " << outputPath << std::endl;
  
  
  
  TString predictionsPath=path+ "/"+rootDir;
  TString matrixPath = (TString)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/data/MATRIX/NNLO_HT2_NNPDF31/hists_MATRIX_NNLO_HT2_NNPDF31.root"; 
  
  auto matrixFile = std::unique_ptr<TFile>{TFile::Open(matrixPath)};
  vector<std::unique_ptr<TFile>> predictionFiles(nvariables);
  vector<std::unique_ptr<TFile>> covarianceFiles(nvariables);
  for(int i=0;i<nvariables;i++) {
    TString predictionFilePath=predictionsPath+"/"+systematicHistosPath;
    if(variables[i].Contains("_vs_")) predictionFilePath+="ND";
    predictionFilePath+="/"+variables[i]+"_"+level+".root";
    
    predictionFiles[i] = std::unique_ptr<TFile>{TFile::Open(predictionFilePath)};
    if(predictionFiles[i]->IsZombie()) {
      throw TtbarDiffCrossSection_exception("Error in loading file with prediction: " + predictionFilePath);
    }
    
    TString covarianceFilePath = predictionsPath + "/" + covariancesDir;
    if(variables[i].Contains("_vs_")) covarianceFilePath += "ND";
    covarianceFilePath += "/" + variables[i]+"_"+level+".root";
    
    covarianceFiles[i] = std::unique_ptr<TFile>{TFile::Open(covarianceFilePath)};
    if(covarianceFiles[i]->IsZombie()) {
      throw TtbarDiffCrossSection_exception("Error in loading file with covariances and central values: " + covarianceFilePath);
    }
  }
  
  
  
  
  vector<PredictionConfig> predictionConfigs;
  
  PredictionConfig predConfig;
  predConfig.folderName="nominal";
  predConfig.latexName="PWG+Py8";
  predConfig.PDFSet="NNPDF30 A14";
  predictionConfigs.push_back(predConfig);

  predConfig.folderName=ttbarConfig->hardScatteringDirectory();
  predConfig.latexName="MG5_aMC@NLO+Py8";
  predConfig.PDFSet="NNPDF30 UE-EE-5";
  predictionConfigs.push_back(predConfig);
  
  predConfig.folderName=ttbarConfig->partonShoweringDirectory();
  predConfig.latexName="PWG+H7.1.3";
  predConfig.PDFSet="NNPDF30 A14";
  predictionConfigs.push_back(predConfig);
  
  predConfig.folderName=ttbarConfig->ISRupDirectory();
  predConfig.latexName="PWG+Py8 (more IFSR)";
  predConfig.PDFSet="NNPDF30 A14";
  predictionConfigs.push_back(predConfig);
  
  predConfig.folderName=ttbarConfig->ISRdownDirectory();
  predConfig.latexName="PWG+Py8 (less IFSR)";
  predConfig.PDFSet="NNPDF30 A14";
  predictionConfigs.push_back(predConfig);

  //if(level=="PartonLevel") {
  //  predConfig.folderName="";
  //  predConfig.latexName="MATRIX NNLO";
  //  predConfig.PDFSet="NNPDF31";
  //  predConfig.histName="stat";
  //  predictionConfigs.push_back(predConfig);
  //}
  
  
  const int npredictions=predictionConfigs.size(); 
  
  vector<std::unique_ptr<TH1D>> predictionsAbs(npredictions); // Absolute spectra
  vector<std::unique_ptr<TH1D>> predictionsRel(npredictions); // Relative spectra
  
  
  // Tex files initialization
  const TString tablePathAbs=outputPath+"/"+level+"_pvalues.tex";
  const TString tablePathRel=outputPath+"/"+level+"_normalized_pvalues.tex";
  ofstream tableAbs(tablePathAbs.Data(), std::ofstream::out);
  ofstream tableRel(tablePathRel.Data(), std::ofstream::out);
  
  if(debug>0) {
    std::cout << "Latex tables with p-values initialized:\nAbsolute: " << tablePathAbs << "\nRelative: " << tablePathRel << std::endl;
  }
  
  // Here we will create latex table headings
  printTableHeading(tableAbs, predictionConfigs);
  printTableHeading(tableRel, predictionConfigs);
  
  if(debug > 1) {
    std::cout << "chi2 results: ";
    for(int ipred=0;ipred<npredictions;ipred++) std::cout << predictionConfigs[ipred].latexName << " ";
    std::cout << std::endl;
  }
  
  // Loop over variables  
  for(int ivariable=0;ivariable<nvariables;ivariable++) {
    const TString& variable = variables[ivariable];
    bool doChi2Normalized = (variable!="total_cross_section");
    
    vector<std::unique_ptr<TMatrixT<double>>> covariancePredAbs(npredictions);
    vector<std::unique_ptr<TMatrixT<double>>> covariancePredRel(npredictions);
    
    bool hasNNLO=false;
    
    // Loop over predictions
    for(int ipred=0;ipred<npredictions;ipred++) {
      // Loading prediction histograms
      TString name{""};
      if(predictionConfigs[ipred].folderName!="") name = predictionConfigs[ipred].folderName+"/"+variable+"_"+predictionConfigs[ipred].histName;
      else name = variable+"_"+predictionConfigs[ipred].histName;
      
      bool isND = variable.Contains("_vs_");
      
      bool isMatrix = predictionConfigs[ipred].latexName.Contains("MATRIX");
      
      if(isMatrix) {
	auto p = getMatrixPrediction(matrixFile.get(),variable, false/*isNormalized*/,isND);
	predictionsAbs[ipred] = std::unique_ptr<TH1D>{p.first};
	if(predictionsAbs[ipred]) {
	  hasNNLO = true;
	  covariancePredAbs[ipred] = std::make_unique<TMatrixT<double>>(p.second);
	  covariancePredRel[ipred] = std::make_unique<TMatrixT<double>>(functions::covarianceForNormalizedPrediction(*predictionsAbs[ipred],*covariancePredAbs[ipred],/*niter=*/10000,/*divideByBinWidth=*/true));
	}
      }
      else {
	std::cout << "Loading prediction histogram: " << name << std::endl;
	predictionsAbs[ipred] = std::unique_ptr<TH1D>{(TH1D*)predictionFiles[ivariable]->Get(name)};
      }
      
      if(!predictionsAbs[ipred]) continue;
      
      std::cout << predictionsAbs[ipred].get() << std::endl;
      std::cout << predictionsAbs[ipred]->GetName() << std::endl;
      
      if(!predictionsAbs[ipred]) throw TtbarDiffCrossSection_exception((TString)"Error in loading prediction from:\n" + predictionFiles[ivariable]->GetName() + "\npath: " + name);
      if(doChi2Normalized) predictionsRel[ipred] = std::unique_ptr<TH1D>{(TH1D*)predictionsAbs[ipred]->Clone()};
      
      //dividing by bin width + normalization
      predictionsAbs[ipred]->Scale(1./lumi);
      functions::DivideByBinWidth(predictionsAbs[ipred].get());
      if(doChi2Normalized) {
	predictionsRel[ipred]->Scale(1./predictionsRel[ipred]->Integral());
	functions::DivideByBinWidth(predictionsRel[ipred].get());
      }
      // Creating MC stat covariances for predictions
      if(debug > 2) std::cout << "Creating covariances for predictions" << std::endl;
      
      if(!isMatrix) {
	covariancePredAbs[ipred] = std::make_unique<TMatrixT<double>>(functions::covarianceFromPrediction(predictionsAbs[ipred].get(), "absolute"));
	// Absolute spectrum is also used to calculate MC statistical covariance matrix for relative spectrum using pseudo-experiments 
	if(doChi2Normalized) covariancePredRel[ipred] = std::make_unique<TMatrixT<double>>(functions::covarianceFromPrediction(predictionsAbs[ipred].get(), "relative", 100000)); 
	if(debug > 2) std::cout << "Covariances for predictions created." << std::endl;
      }
      else {
	for(int i=0;i<covariancePredAbs[ipred]->GetNcols();i++) for(int j=0;j<covariancePredAbs[ipred]->GetNcols();j++) {
	  (*covariancePredAbs[ipred])[i][j]=(*covariancePredAbs[ipred])[i][j]/(pow(lumi,2)*predictionsAbs[ipred]->GetBinWidth(i+1)*predictionsAbs[ipred]->GetBinWidth(j+1));
	  
	}
	
      }
      
    }
   
    // Loading central values and covariances
    auto dataAbs = std::unique_ptr<TH1D>{(TH1D*) covarianceFiles[ivariable]->Get(variable+"_centralValues")};
    auto dataRel = std::unique_ptr<TH1D>{(TH1D*) covarianceFiles[ivariable]->Get(variable+"_centralValuesNormalized")};
    
    auto covAbs = std::unique_ptr<TMatrixT<double>>{(TMatrixT<double>*)covarianceFiles[ivariable]->Get(variable+"_"+covUncName)};
    auto covRel = std::unique_ptr<TMatrixT<double>>{(TMatrixT<double>*)covarianceFiles[ivariable]->Get(variable+"_"+covUncName+"Normalized")};
    
    // Here we will do chi2 tests
    
    
    
    if(debug > 1) {
      std::cout << variable << " " << std::endl;
    }
    
    int n=npredictions;
    //if(!hasNNLO) n=n-1;
    
    vector<double> chi2Abs(n);
    vector<double> pValuesAbs(n);
    vector<int> ndfsAbs(n);
    vector<double> chi2Rel(n);
    vector<double> pValuesRel(n);
    vector<int> ndfsRel(n);
    
    
    for(int ipred=0;ipred<n;ipred++) {
    
      Chi2TestMaker chi2TestMakerAbs;
      Chi2TestMaker chi2TestMakerRel;
      
      
      if(debug > 2) if(ipred==n-1 && n==npredictions) std::cout << hasNNLO << " " << ipred << " " << npredictions << " Data: " << dataAbs->GetBinContent(1) << " prediction: " << predictionsAbs[ipred]->GetBinContent(1) << std::endl;
      
      chi2TestMakerAbs.setDistr1(dataAbs.get());
      chi2TestMakerAbs.setCov1(*covAbs);
      chi2TestMakerAbs.setDistr2(predictionsAbs[ipred].get());
      chi2TestMakerAbs.setCov2(*covariancePredAbs[ipred]);
      //if(debug > 2) {
	//chi2TestMakerAbs.getDistr1()->Print();
	//chi2TestMakerAbs.getDistr2()->Print();
	//covAbs->Print();
	//covariancePredAbs[ipred]->Print();
      //}
      if(doChi2Normalized) {
	chi2TestMakerRel.setDistr1(dataRel.get());
	chi2TestMakerRel.setCov1(*covRel);
	chi2TestMakerRel.setDistr2(predictionsRel[ipred].get());
	chi2TestMakerRel.setCov2(*covariancePredRel[ipred]);
	chi2TestMakerRel.removeBin(predictionsRel[ipred]->GetNbinsX()); // Removing last column and row from covariances
      }
      
      if(debug > 2) std::cout << "Running chi2 tests for absolute spectra: " << variable << std::endl;
      chi2TestMakerAbs.chi2Test();
      if(debug > 2) std::cout << "Running chi2 tests for normalized spectra" << std::endl;
      if(doChi2Normalized) chi2TestMakerRel.chi2Test();
      if(debug > 2) std::cout << "Chi2 tests done" << std::endl;
      
      if(debug > 2) {
	chi2TestMakerAbs.getDistr1()->Print();
	chi2TestMakerAbs.getDistr2()->Print();
	covAbs->Print();
	covariancePredAbs[ipred]->Print();
      }
      
      
      chi2Abs[ipred] =  chi2TestMakerAbs.getChi2();
      ndfsAbs[ipred] =  chi2TestMakerAbs.getNDF();
      pValuesAbs[ipred] =  chi2TestMakerAbs.getPValue();
      
      if(doChi2Normalized) {
	chi2Rel[ipred] =  chi2TestMakerRel.getChi2();
	ndfsRel[ipred] =  chi2TestMakerRel.getNDF();
	pValuesRel[ipred] =  chi2TestMakerRel.getPValue();
      }
      if(debug > 1) {
	std::cout << chi2Abs[ipred] << "/" << ndfsAbs[ipred] << " " << pValuesAbs[ipred] << " & ";
      }
      
    } // End chi2 tests
    
    if(debug > 1) std::cout << std::endl;
    
    // Here we will write chi2 test results for this variable in the table
    printChi2Result(tableAbs,variable,chi2Abs,ndfsAbs,pValuesAbs);
    if(doChi2Normalized) printChi2Result(tableRel,variable,chi2Rel,ndfsRel,pValuesRel);
    
   
  } // End loop over variables
  
  finalizeTable(tableAbs);
  finalizeTable(tableRel);
  
  
  
  
  return true;
}

//----------------------------------------------------------------------

void printTableHeading(ofstream& table, vector<PredictionConfig>& predictionConfigs) {
  const int npredictions = predictionConfigs.size();
  
  // First line
  table << fixed << "\\begin{tabular}{|c|"; 
  for(int ipred=0;ipred<npredictions;ipred++) table << "cc|"; 
  table << "} \\hline" << endl;
  
  // Second line
  table << "Observable"; 
  for(int ipred=0;ipred<npredictions;ipred++) {
    TString latexname=predictionConfigs[ipred].latexName;
    latexname.ReplaceAll("_","\\_");
    table << " & \\multicolumn{2}{|c|}{" << latexname << "}";
  } 
  table << " \\\\" << endl;
  
  // Third line
  for(int ipred=0;ipred<npredictions;ipred++) table << " & \\multicolumn{2}{|c|}{" << predictionConfigs[ipred].PDFSet << "}";
  table << " \\\\" << endl;
  
  // Fourth line
  for(int ipred=0;ipred<npredictions;ipred++) table << " & \\multicolumn{1}{|c|}{$\\chi^{2}$/NDF} & \\multicolumn{1}{|c|}{$p$-value}";
  table << " \\\\ \\hline \\hline" << endl;
  
}

//----------------------------------------------------------------------

void printChi2Result(ofstream& table, const TString& variable, const vector<double>& chi2, const vector<int>& ndfs, const vector<double>& pValues) {
  const int npredictions = chi2.size();
  TString label = NamesAndTitles::getVariableLatex(variable);
  label.ReplaceAll("#","\\");
  
  table << "$" << label << "$ ";
  for (int ipred=0;ipred<npredictions;++ipred) {
  cout << "***************: " << ipred << endl;
    table << " & " << setprecision(1) << chi2[ipred] << "/" << ndfs[ipred] << setprecision(2) << " & "  ;
    table << setprecision(2)  ;
    if(pValues[ipred] >=0.01) table << " " << pValues[ipred] << " "  ;
    else table << " $<\\ $0.01 ";

    //cout << "**** " << "\"" << pValues[ipred] << "\"" << endl;
  }
  table << " \\\\" << endl; // End line
}

//----------------------------------------------------------------------

void printChi2ResultPlot(const TString& variable, const vector<double>& chi2, const vector<int>& /*ndfs*/, const vector<double>& pValues, vector<PredictionConfig>& /*predictionConfigs*/) {
  const int npredictions = chi2.size();
  TString label = NamesAndTitles::getVariableLatex(variable);
  label.ReplaceAll("#","\\");
  
  TH1F* pvalues=new TH1F("h", "P-values;;p-value of #chi^2 test(without theoretical unc.)", 10, 0, 1);

  for (int ipred=0;ipred<npredictions;++ipred) {
    pvalues->SetBinContent(ipred,pValues[ipred]);

  }



  // bin names
  //  TString latexname=predictionConfigs[ipred].latexName;
  //  latexname.ReplaceAll("_","\\_");
  //  table << " & \\multicolumn{2}{|c|}{" << latexname << "}";


  //for (int ipred=0;ipred<npredictions;++ipred) {
  //  cout << " & " << setprecision(1) << chi2[ipred] << "/" << ndfs[ipred] << setprecision(2) << " & "  ;
  //  if(pValues[ipred] >=0.01) cout << pValues[ipred];
  //  else cout << " $<0.01$";
  //}
  

}

//----------------------------------------------------------------------

void finalizeTable(ofstream& table) {
  table << "\\hline" << endl << "\\end{tabular}" << endl;
  table.close();
}

//----------------------------------------------------------------------

std::pair<TH1D*,TMatrixT<double>> getMatrixPrediction(TFile* f,const TString& variableName, bool isNormalized,bool is2D) {
  TH1D* h=nullptr;
  TMatrixT<double> cov;
  
  vector<TString> matrixPredictions {
    "total_cross_section"   ,
    "t1_pt"                 ,
    "t1_y"                  ,
    "t2_pt"                 ,
    "t2_y"                  ,
    "ttbar_mass"            ,
    "ttbar_pt"              ,
    "ttbar_y"               ,
    "H_tt_pt"               ,
    "ttbar_deltaphi"        ,
    //"t1_pt_vs_t2_pt"        ,
    //"t1_pt_vs_ttbar_pt"     ,
    //"t1_pt_vs_ttbar_mass"   ,
    //"t2_pt_vs_ttbar_pt"     ,
    //"t2_pt_vs_ttbar_mass"   ,
    //"ttbar_y_vs_t1_pt"      ,
    //"ttbar_y_vs_t2_pt"      ,
    //"ttbar_y_vs_ttbar_mass" ,
    //"t1_y_vs_t2_y"          ,
    //"t1_y_vs_t1_pt"         ,
    //"t2_y_vs_t2_pt"         ,
    //"t1_y_vs_ttbar_mass"    ,
    //"ttbar_y_vs_t1_y"       ,
    //"ttbar_y_vs_ttbar_pt"   ,
    //"ttbar_pt_vs_ttbar_mass",
  };

  if(std::find(std::begin(matrixPredictions),std::end(matrixPredictions),variableName)==std::end(matrixPredictions)) return {nullptr,cov};
  
  
  TString var = variableName;
  var.ReplaceAll("t1_","leadingTop_");
  var.ReplaceAll("t2_","subleadingTop_");
  
  std::vector<TString> vecChangeUnits = {
    "ttbar_mass",
    "ttbar_pt",
    "t1_pt",
    "t2_pt",
    "H_tt_pt"
  };
  
  if(!is2D){
  
    TString name{""};
    if(isNormalized) name = "norm_"+var+"_stat";
    else name = var+"_stat";
    std::cout <<f->GetName() << " " << name << std::endl;
    
    TString nameSys = name;
    nameSys.ReplaceAll("_stat","_syst");
    
    auto gr = std::unique_ptr<TGraphAsymmErrors>{(TGraphAsymmErrors*)f->Get(name)};
    auto grSys = std::unique_ptr<TGraphAsymmErrors>{(TGraphAsymmErrors*)f->Get(nameSys)};
    if(std::find(std::begin(vecChangeUnits),std::end(vecChangeUnits),variableName)!=std::end(vecChangeUnits)) {
      functions::changeUnits(gr.get(),0.001);
      functions::changeUnits(grSys.get(),0.001);
    }
    
    const int nbins = gr->GetN();
    
    std::vector<double> binning(nbins+1);
    binning[0] = gr->GetPointX(0)-gr->GetErrorXlow(0);
    for(int i=1;i<=nbins;i++) {
      binning[i]= gr->GetPointX(i-1)+gr->GetErrorXhigh(i-1);
      
    }
    
    h=new TH1D(variableName+"_Matrix","",nbins,&binning[0]);
    auto hSys=std::make_unique<TH1D>(variableName+"_Matrix_SYS","",gr->GetN(),&binning[0]);
    for(int i=0;i<gr->GetN();i++) {
      h->SetBinContent(i+1,gr->GetPointY(i));
      h->SetBinError(i+1,gr->GetErrorYhigh(i));
      
      hSys->SetBinContent(i+1,gr->GetPointY(i));
      hSys->SetBinError(i+1,0.5*(grSys->GetErrorYhigh(i) + grSys->GetErrorYlow(i)));

    }
    const double lumi = 138.96516;
    const double iBR=2.1882987;
    h->Scale(lumi/iBR);
    hSys->Scale(lumi/iBR);
    
    h->SetDirectory(0);
    hSys->SetDirectory(0);
    functions::MultiplyByBinWidth(h);
    functions::MultiplyByBinWidth(hSys.get());
    
    
    
    TMatrixT<double> covSys(nbins,nbins);
    
    TString fileDir = gSystem->Getenv("TtbarDiffCrossSection_output_path");
    fileDir+="/root.All/MATRIXCorrelations/MATRIXCorrelations.root";
    auto fileCorMatrix = std::make_unique<TFile>(fileDir);
    
    auto corSys = std::unique_ptr<TMatrixT<double>>{(TMatrixT<double>*)fileCorMatrix->Get(variableName+"_correl")};
    
    
    for(int i=0;i<nbins;i++) for(int j=0;j<nbins;j++) {
      covSys[i][j] = hSys->GetBinError(i+1)*hSys->GetBinError(j+1)*(*corSys)[i][j];
      //covSys[i][j] = hSys->GetBinError(i+1)*hSys->GetBinError(j+1);
      //if((i==0 && j>i) || (j==0 && i>j)) covSys[i][j] = -hSys->GetBinError(i+1)*hSys->GetBinError(j+1);
      //if(i!=j) covSys[i][j] = 0.;
      //else covSys[i][j]=hSys->GetBinError(i+1)*hSys->GetBinError(j+1);
      //covSys[i][j] = 0.;
    }
    
    cov.ResizeTo(nbins,nbins);
    for(int i=0;i<nbins;i++) for(int j=0;j<nbins;j++) {
      double statError = (i==j ? h->GetBinError(i+1) : 0.);
      cov[i][j] = statError*statError + covSys[i][j];
    } 
        
    
    cov.Print();
    
    //std::cout << h->GetName() << " " << h->GetNbinsX() << " " << gr->GetN() << std::endl;
    //std::cout << "Binning: ";
    //for(double bin : binning) std::cout << bin << " ";
    //std::cout << std::endl;

    //std::cout << "YieldsGr: ";
    //for(int i=0;i<gr->GetN();i++) std::cout << gr->GetPointY(i)*lumi/iBR*(binning[i+1]-binning[i]) << "+-" << gr->GetErrorYhigh(i)*lumi/iBR*(binning[i+1]-binning[i]) << " ";
    //std::cout << std::endl;
    //std::cout << "YieldsHist: ";
    //for(int i=0;i<gr->GetN();i++) std::cout << h->GetBinContent(i+1) << "+-" << h->GetBinError(i+1) << " ";
    //std::cout << std::endl;
  }
  else {
    TString name = var;
    name.ReplaceAll("_vs_","-");
    
    
  }
  
  return {h,cov};
}
