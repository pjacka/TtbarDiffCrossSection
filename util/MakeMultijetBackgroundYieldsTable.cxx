
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "HelperFunctions/AtlasStyle.h"

#include <iostream>
#include "TString.h" 

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

int makeMultijetBacgroundYieldsTable( const TString& mainDir,
                               const TString& pathTTbarConfig,
                               const TString& outputDir,
                               const TString& systematicHistosDir,
                               int debugLevel) {
				 
  SetAtlasStyle();
    
  auto ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(pathTTbarConfig);
  
  const TString mc_samples_production = ttbarConfig->mc_samples_production();
  const TString rootDir="root."+mc_samples_production;
  
  const TString level="ParticleLevel";
  TString texDir=mainDir + "/tex."+ mc_samples_production +"/"+outputDir;
  
  gSystem->mkdir(texDir,true);
  ofstream filetable((texDir+"/" + "TableMultijet.tex").Data());
  
  
  
  std::vector<TString> variables = {
    "total_cross_section",
    "randomTop_pt",
    "randomTop_y",
    "leadingTop_pt",
    "leadingTop_y",
    "subleadingTop_pt",
    "subleadingTop_y",
    "ttbar_mass",
    "ttbar_pt",
    "ttbar_y",
    "chi_ttbar",
    "y_boost",
    "pout",
    "H_tt_pt",
    "ttbar_deltaphi",
    "cos_theta_star",
    "t1_pt_vs_t2_pt",
    "t1_pt_vs_delta_pt",
    "t1_pt_vs_ttbar_pt",
    "t1_pt_vs_ttbar_mass",
    "ttbar_y_vs_t1_pt",
    "ttbar_y_vs_ttbar_mass",
    "ttbar_y_vs_ttbar_mass_vs_t1_pt"
  };
  
  
  filetable << "\\begin{tabular}{|c|c|c|c|c|} \\hline" << endl; 
  filetable << "Variable & Multijet & Data & Signal & Multijet/Data \\\\ \\hline" << endl; 

  
  
  for(const TString& variable : variables) {
    
    const TString sysDir = variable.Contains("_vs_") ? systematicHistosDir + "ND" : systematicHistosDir;
    const TString fileName=mainDir+"/" + rootDir + "/" +sysDir + "/" + variable + "_" + level + ".root";
    
    
    if(debugLevel>0) {
      cout << "Processing variable: " << variable << endl; 
      cout << "Loading histograms from file: " << fileName << endl;
    }
    
    auto file = std::make_unique<TFile>(fileName);
    if(file->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + fileName); 
      return -1;
    }
    
    auto histos = std::make_unique<OneSysHistos>("nominal",variable,file.get());
    
    const TString data = Form("%4.0f",histos->data->Integral());
    const TString multijet = Form("%4.0f",histos->bkg_Multijet->Integral());
    const TString signal = Form("%4.2f",histos->signal_reco->Integral());
    const TString multijetOverData = Form("%4.3f",histos->bkg_Multijet->Integral()/histos->data->Integral());
    
    
    TString variableLatex = "$"+NamesAndTitles::getVariableLatex(variable)+"$";
    variableLatex.ReplaceAll("#","\\");
    
    filetable << variableLatex << " & " << multijet << " & " << data << " & " << signal << " & " <<  multijetOverData << " \\\\ \\hline" << std::endl;   
    
  }
  
  filetable << "\\end{tabular}" << endl;
  filetable.close();
  
  cout << "Everything ok!" << endl;
  return 0;
}

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to calculate asymmetric errors. It creates plots and tables with uncertainties.  

Usage:  
  CalculateAsymmetricErrors [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --outputDir <string>                     Directory to store plots. [default: MultijetYieldsTable]
  --systematicHistosDir <string>          Relative path to output directory from mainDir/root.<mc_samples_production>/ [default: systematics_histos]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";



int main(int nArg, char **arg) {
  TH1::AddDirectory(kFALSE);
  try {
    
    TString mainDir("");
    TString pathTTbarConfig("");
    TString outputDir("");
    TString systematicHistosDir("");
    int debugLevel=0;
    
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try {
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
        { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ pathTTbarConfig=args["--config"].asString();} catch(const std::invalid_argument& e)
        { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
        { throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
      try{ systematicHistosDir=args["--systematicHistosDir"].asString();} catch(const std::invalid_argument& e)
        { throw std::invalid_argument("--systematicHistosDir option is expected to be a string. Check input parameters."); }
      
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
        { throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    cout << "Main directory is " << mainDir << "." << endl << endl;
    
    return makeMultijetBacgroundYieldsTable(mainDir,pathTTbarConfig,outputDir,systematicHistosDir,debugLevel);
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  
  return 0;
}
