
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/plottingFunctions.h"
#include "CovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"

#include "docopt.h"
#include <nlohmann/json.hpp> // nlohmann::json classes to parse json files

using namespace std;

TH1D* doStressTest(OneSysHistos* reweighted,OneSysHistos* nominal,int niter=4);
void doComparison(TH1D* reweighted,TH1D* nominal,const TString& dirname,const TString& figureName,const double y2min,const double y2max);
void plotStressTestResult(vector<TH1D*>& ratios,TLegend* leg,const TString& dirname,const TString& name);

void doComparionND(TH1D* reweighted,
		   TH1D* nominal,
		   const HistogramNDto1DConverter* histogramConverterND,
		   MultiDimensionalPlotsSettings* plotSettingsND,
		   const TString& variable,
		   const TString& level,
		   const TString& dirname,
		   const TString& figureName,
		   const double ymin,
		   const double ymax);
		   
void plotStressTestResultND(vector<TH1D*>& ratios,
			    const HistogramNDto1DConverter* histogramConverterND,
			    MultiDimensionalPlotsSettings* plotSettingsND,
			    const TString& variable,
			    const TString& level,
			    TLegend* leg,
			    const TString& dirname,
			    const TString& figureName);


int doStressTest(const TString& mainDir,
		 const TString& path_config_lumi,
		 const TString& pathMultiDimensionalPlotsConfig,
		 const TString& path_config_sysnames,
		 const TString& outputDir,
		 const TString& systematicHistosDir,
		 const TString& stressTestHistosDir,
		 const TString& variable_name,
		 const TString& level,
		 const TString& dimension,
		 const int debug);

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to run stress tests of unfolding.  

Usage:  
  drawAll [options]
  
Options:
  -h --help                                      Show help screen.
  --mainDir <mainDir>                            Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <config>                              Path to ttbar config file. [default: $TTBAR_PATH/TtbarDiffCrossSection/data/config.json]
  --configBinning <configBinning>                Path to file with binning. [default: TtbarDiffCrossSection/data/optBinningND/ttbar_y_vs_ttbar_mass_vs_t1_pt.env]
  --multiDimensionalPlotsConfig <string>         Path to config file with plots settings. [default: TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env]
  --variable <variable>                          Variable name. [default: total_cross_section]
  --level <level>                                ParticleLevel or PartonLevel. [default: ParticleLevel]
  --dimension <dimension>                        1D or ND. [default: 1D]
  --systematicsHistosDir <systematicsHistosDir>  Directory with systematic histos rootfiles. [default: systematics_histos]
  --stressTestHistosDir <stressTestHistosDir>    Directory with reweighted histos rootfiles. [default: systematics_histos]
  --outputDir <outputDir>                        Directory to store output. Will be created in maindir/pdf.<MC> . [default: StressTestResults]
  --debugLevel <int>                             Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  SetAtlasStyle();
  TH1::AddDirectory(kFALSE);
  int result=0;
  try {
    
    TString mainDir="";
    TString path_config_lumi="";
    TString path_config_binning("");
    TString pathMultiDimensionalPlotsConfig="";
    TString variable_name="";
    TString level="";
    TString dimension="";
    TString systematicHistosDir="";
    TString stressTestHistosDir="";
    TString outputDir="";
    int debug=0;
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
  
    try{
    
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      cout << "Loading configBinning" << endl;
      try{ path_config_binning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
	      cout << "Loading multiDimensionalPlotsConfig" << endl;

      try{ pathMultiDimensionalPlotsConfig=args["--multiDimensionalPlotsConfig"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--multiDimensionalPlotsConfig option is expected to be a string. Check input parameters."); }

      try{ systematicHistosDir=args["--systematicsHistosDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--systematicsHistosDir option is expected to be a string. Check input parameters."); }
	
      try{ stressTestHistosDir=args["--stressTestHistosDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--stressTestHistosDir option is expected to be a string. Check input parameters."); }
	
      try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
	
      try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
	cout << "Loading dimension" << endl;
      try{ dimension=args["--dimension"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--dimension option is expected to be a string. Check input parameters."); }
      
      try{ debug=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
	cout << e.what() << endl << endl;
	cout << USAGE << endl;
	return -1;
      }
    cout << "Loading parameters done." << endl;
    if (mainDir.Contains("$TtbarDiffCrossSection_output_path")) mainDir.ReplaceAll("$TtbarDiffCrossSection_output_path",gSystem->Getenv("TtbarDiffCrossSection_output_path")); // If not set the default value is $TtbarDiffCrossSection_output_path
    if (path_config_lumi.Contains("$TTBAR_PATH")) path_config_lumi.ReplaceAll("$TTBAR_PATH",gSystem->Getenv("TTBAR_PATH"));
    
    
    
    result = doStressTest(mainDir,path_config_lumi,path_config_binning, pathMultiDimensionalPlotsConfig,outputDir,systematicHistosDir,stressTestHistosDir,variable_name,level,dimension,debug);
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  return result;
  
}
    
int doStressTest(const TString& mainDir,
		 const TString& path_config_lumi,
		 const TString& path_config_binning,
		 const TString& pathMultiDimensionalPlotsConfig,
		 const TString& outputDir,
		 const TString& systematicHistosDir,
		 const TString& stressTestHistosDir,
		 const TString& variable_name,
		 const TString& level,
		 const TString& dimension,
		 const int /*debug*/){
    
  auto ttbarConfig=make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  TString mc_samples_production = ttbarConfig->mc_samples_production();
  
  const nlohmann::json reweightingJSON = nlohmann::json::parse(
      std::ifstream(ttbarConfig->stressTestReweightingConfig().Data())
    );
    
  std::vector<std::string> reweightingIDs;
  for (auto &item : reweightingJSON.items()) {
    reweightingIDs.push_back(item.key().c_str());
  }
  
  const int nStressTestBranches=reweightingIDs.size(); //2;
  
  std::unique_ptr<HistogramNDto1DConverter> histogramConverterND_reco, histogramConverterND_truth;
  std::unique_ptr<MultiDimensionalPlotsSettings> plotSettingsND;
  TString levelShort=level;
  levelShort.ReplaceAll("Level","");
  
  if(dimension=="ND") {
    histogramConverterND_reco = std::make_unique<HistogramNDto1DConverter>(path_config_binning.Data());
    histogramConverterND_reco->initializeOptBinning((variable_name + "_"+levelShort+"_reco").Data());
    histogramConverterND_reco->initialize1DBinning();
    
    histogramConverterND_truth = std::make_unique<HistogramNDto1DConverter>(path_config_binning.Data());
    histogramConverterND_truth->initializeOptBinning((variable_name + "_"+levelShort+"_truth").Data());
    histogramConverterND_truth->initialize1DBinning();
    
    plotSettingsND = std::make_unique<MultiDimensionalPlotsSettings>(pathMultiDimensionalPlotsConfig.Data());
  }
  
  
  
  TString rootDir=(TString)"root."+mc_samples_production;
  
  const TString dirname_png = mainDir + "/" + "png." + mc_samples_production + "/" + outputDir + "/" + variable_name + "_" + level + "/";
  const TString dirname_pdf = mainDir + "/" + "pdf." + mc_samples_production + "/" + outputDir + "/" + variable_name + "_" + level + "/";
  gSystem->mkdir(dirname_png,true);
  gSystem->mkdir(dirname_pdf,true);
  
  TFile* file_systematics= new TFile(mainDir+"/"+rootDir+"/" + systematicHistosDir + "/" + variable_name + "_" + level + ".root");
  if(file_systematics->IsZombie()){
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/"+rootDir+"/" + systematicHistosDir + "/" + variable_name + "_" + level + ".root"); 
    return -1;
  }
  TFile* file_stress_test= new TFile(mainDir+"/"+rootDir+"/"+stressTestHistosDir+"/" + variable_name + "_" + level + ".root");
  if(file_stress_test->IsZombie()){
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/"+rootDir+"/"+stressTestHistosDir+"/" + variable_name + "_" + level + ".root"); 
    return -1;
  }
  
  TString shortName=variable_name;
  shortName.ReplaceAll("_fine","");
  
  
  vector<OneSysHistos*> histvec(nStressTestBranches+1);
  histvec[0]= new OneSysHistos("nominal",shortName,file_systematics);
  
  std::shared_ptr<TH1D> hEfficiency{ (TH1D*)histvec[0]->effOfRecoLevelCutsNominator->Clone()};
  hEfficiency->Divide(histvec[0]->signal_truth);
  std::shared_ptr<TH1D> hAcceptance{ (TH1D*)histvec[0]->effOfTruthLevelCutsNominator->Clone()};
  hAcceptance->Divide(histvec[0]->signal_reco);
  
  vector<vector<TH1D*>  >ratios(nStressTestBranches+1);
  
  //for(int i=1;i<=nStressTestBranches;i++) {
  int i=1;
  for(const std::string& reweightingID : reweightingIDs) {
    const TString chain_name=reweightingID.c_str();
    const TString prefix = "stress_test_" + chain_name;
    const TString sufix = level + "_" + shortName;
    
    histvec[i] = new OneSysHistos(chain_name,shortName,file_stress_test);
    std::shared_ptr<TH1D> hEfficiency_reweighted{ (TH1D*)histvec[i]->effOfRecoLevelCutsNominator->Clone()};
    hEfficiency_reweighted->Divide(histvec[i]->signal_truth);
    std::shared_ptr<TH1D> hAcceptance_reweighted{ (TH1D*)histvec[i]->effOfTruthLevelCutsNominator->Clone()};
    hAcceptance_reweighted->Divide(histvec[i]->signal_reco);
    
    if (dimension == "1D") {
      doComparison(histvec[i]->signal_reco,histvec[0]->signal_reco,dirname_png,prefix + "_recoComparison_" + sufix + ".png",0.9,1.1);
      doComparison(histvec[i]->signal_reco,histvec[0]->signal_reco,dirname_pdf,prefix + "_recoComparison_" + sufix + ".pdf",0.9,1.1);
      doComparison(histvec[i]->signal_truth,histvec[0]->signal_truth,dirname_png,prefix + "_truthComparison_" + sufix + ".png",0.9,1.1);
      doComparison(histvec[i]->signal_truth,histvec[0]->signal_truth,dirname_pdf,prefix + "_truthComparison_" + sufix + ".pdf",0.9,1.1);
      
      doComparison(hEfficiency_reweighted.get(),hEfficiency.get(),dirname_png,prefix + "_efficiency_" + sufix + ".png",0.9,1.1);
      doComparison(hEfficiency_reweighted.get(),hEfficiency.get(),dirname_pdf,prefix + "_efficiency_" + sufix + ".pdf",0.9,1.1);
      doComparison(hAcceptance_reweighted.get(),hAcceptance.get(),dirname_png,prefix + "_acceptance_" + sufix + ".png",0.9,1.1);
      doComparison(hAcceptance_reweighted.get(),hAcceptance.get(),dirname_pdf,prefix + "_acceptance_" + sufix + ".pdf",0.9,1.1);
    }
    if (dimension == "ND") {
      doComparionND(histvec[i]->signal_reco,histvec[0]->signal_reco,histogramConverterND_reco.get(),plotSettingsND.get(),variable_name,level,dirname_pdf,prefix + "_recoComparison_" + sufix,0.9,1.1);
      doComparionND(histvec[i]->signal_truth,histvec[0]->signal_truth,histogramConverterND_truth.get(),plotSettingsND.get(),variable_name,level,dirname_pdf,prefix + "_truthComparison_" + sufix,0.9,1.1);
      
      doComparionND(hEfficiency_reweighted.get(),hEfficiency.get(),histogramConverterND_truth.get(),plotSettingsND.get(),variable_name,level,dirname_pdf,prefix + "_efficiency_" + sufix,0.9,1.1);
      doComparionND(hAcceptance_reweighted.get(),hAcceptance.get(),histogramConverterND_reco.get(),plotSettingsND.get(),variable_name,level,dirname_pdf,prefix + "_acceptance_" + sufix,0.9,1.1);
    }
    
    ratios[i].resize(6);
    ratios[i][0] = (TH1D*)histvec[i]->signal_truth->Clone();
    ratios[i][0]->Divide(histvec[0]->signal_truth);
    NamesAndTitles::setXtitle(ratios[i][0],variable_name);
    ratios[i][0]->GetYaxis()->SetTitle("Unfolded / Reweighted");
    ratios[i][1] = doStressTest(histvec[i],histvec[0],2);
    ratios[i][2] = doStressTest(histvec[i],histvec[0],3);
    ratios[i][3] = doStressTest(histvec[i],histvec[0],4);
    ratios[i][4] = doStressTest(histvec[i],histvec[0],6);
    ratios[i][5] = doStressTest(histvec[i],histvec[0],10);
    //ratios[i][5] = doStressTest(histvec[i],histvec[0],20);
    
    for(unsigned int k=0;k<ratios[i].size();k++){
      ratios[i][k]->SetLineColor(k+1);
      ratios[i][k]->SetMarkerColor(k+1);
    }
    
    const double xmin_legend=0.55;
    const double ymin_legend=0.75;
    const double ymax_legend=0.89;
    const double xmax_legend=0.90; 
    
    auto leg = std::make_shared<TLegend>(xmin_legend,ymin_legend,xmax_legend,ymax_legend);
    leg->AddEntry(ratios[i][0],"Truth Nominal / Reweighted");
    leg->AddEntry(ratios[i][1],"N = 2");
    leg->AddEntry(ratios[i][2],"N = 3");
    leg->AddEntry(ratios[i][3],"N = 4");
    leg->AddEntry(ratios[i][4],"N = 6");
    leg->AddEntry(ratios[i][5],"N = 10");
    //leg->AddEntry(ratios[i][5],"N = 20");
    leg->SetTextFont(42);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetTextSize(0.035);
    
    const TString name = prefix + "_result_" + sufix;
    
    
    if (dimension == "1D") {
      plotStressTestResult(ratios[i],leg.get(),dirname_png+'/',name+".png");
      plotStressTestResult(ratios[i],leg.get(),dirname_pdf+'/',name+".pdf");
    }
    
    if (dimension == "ND") {
      plotStressTestResultND(ratios[i],histogramConverterND_truth.get(),plotSettingsND.get(),variable_name,level,leg.get(),dirname_pdf,name);
    }
    
    i++;
  }
  return 0;
}


TH1D* doStressTest(OneSysHistos* reweighted,OneSysHistos* nominal,int niter){
  static RooUnfoldResponse* response = new RooUnfoldResponse(0,0,nominal->migration);
  auto h = std::unique_ptr<TH1D>{(TH1D*)nominal->signal_reco->Clone()};
  h->Add(reweighted->pseudodata);
  h->Add(nominal->pseudodata,-1);
  
  TH1D* ratio = functions::UnfoldBayes(h.get(),nominal->signal_reco,nominal->effOfTruthLevelCutsNominator,nominal->signal_truth,nominal->effOfRecoLevelCutsNominator,response,niter,0);
  ratio->Divide(reweighted->signal_truth);
  functions::SetErrorsToZero(ratio);
  return ratio;
}


void doComparison(TH1D* reweighted,TH1D* nominal,const TString& dirname,const TString& figureName,const double y2min,const double y2max){
  
  vector<TH1D*> histos;
  
  histos.push_back(nominal);
  histos.push_back(reweighted);
  
  histos[1]->SetLineColor(2);
  histos[1]->SetMarkerColor(2);
  histos[1]->SetLineStyle(2);
  
  const double xmin_legend=0.55;
  const double ymin_legend=0.65;
  const double ymax_legend=0.89;
  const double xmax_legend=0.90; 
  
  auto leg = std::make_shared<TLegend>(xmin_legend,ymin_legend,xmax_legend,ymax_legend);
  
  leg->AddEntry(histos[0],"Nominal");
  leg->AddEntry(histos[1],"Reweighted");
  
  TString lumi="";
 
  functions::plotHistograms(histos,leg.get(),dirname+'/', figureName,histos[0], "Reweighted / Nominal",y2min,y2max, lumi);
  
}

void doComparionND(TH1D* reweighted,
		   TH1D* nominal,
		   const HistogramNDto1DConverter* histogramConverterND,
		   MultiDimensionalPlotsSettings* plotSettingsND,
		   const TString& variable,
		   const TString& level,
		   const TString& dirname,
		   const TString& figureName,
		   const double ymin,
		   const double ymax)
{
  auto ratio = std::unique_ptr<TH1D>{(TH1D*)reweighted->Clone()};
  ratio->Divide(nominal);
  
  vector<TH1D*> ratioProj = histogramConverterND->makeProjections(ratio.get(),"ratio_proj");
  const int nprojections = ratioProj.size();
  
  vector<TString> generalInfo;
  generalInfo.push_back("#bf{#it{ATLAS}} Simulation " +NamesAndTitles::getResultsStatus()+", #sqrt{s}=13 TeV");
  if(level.Contains("Parton")) generalInfo.push_back("Parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV");
  else generalInfo.push_back("Fiducial phase-space");
  
  const int nlinesInfo=generalInfo.size();
  
  int nPerLine(0),nlines(0);
  functions::getGridSize(nprojections,nlines,nPerLine);
  
  const double canvas_xsize = (1000./3) * nPerLine;
  const double canvas_ysize = 175.*(nlines+1);
  
  plotSettingsND->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, nlinesInfo);
  
  auto can = std::unique_ptr<TCanvas>(plotSettingsND->makeCanvas("can"));
  can->cd();
  vector<vector<shared_ptr<TPad>>> pads = plotSettingsND->makeArrayOfPads(nprojections);
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable);
  
  int iproj=0;
  for(int iline=0;iline<nlines;iline++) {
    int nInLine = pads[iline].size();
    
    for(int iInLine=0;iInLine<nInLine;iInLine++) {
      TPad* currentPad = pads[iline][iInLine].get();
      
      plotSettingsND->setHistLabelStyle(ratioProj[iproj]);
            
      if(iInLine ==0 && iline ==0) { 
	ratioProj[iproj]->GetYaxis()->SetTitle("Reweighted / Nominal");
      }
      else {
	ratioProj[iproj]->GetYaxis()->SetTitle("");
      }
      if(iline != nlines-1) { 
	ratioProj[iproj]->GetXaxis()->SetTitle("");
      }
      else {
	ratioProj[iproj]->GetXaxis()->SetTitle(xtitles.back());
      }
      
      currentPad->cd();
      
      std::vector<TString> externalBinInfo = histogramConverterND->getExternalBinsInfo(iproj);
      TPaveText* paveText = functions::makePaveText(externalBinInfo, plotSettingsND->textSizePad(), 0.35, 0.75, 0.93); // xmin,xmax,ymax
      paveText->SetFillStyle(0);
      
      
      ratioProj[iproj]->GetYaxis()->SetRangeUser(ymin,ymax);
      
      ratioProj[iproj]->Draw("p");
      
      paveText->Draw();
      
      currentPad->RedrawAxis("g"); 
      currentPad->RedrawAxis();
      
      iproj++;
    }
  }
  
  can->cd();
  
  TPaveText* paveTextGeneralInfo = plotSettingsND->makePaveText(generalInfo);
  paveTextGeneralInfo->Draw();
  
  auto leg = unique_ptr<TLegend>(plotSettingsND->makeLegend(0.6,0.98));
  leg->SetFillStyle(0);
  leg->SetNColumns(1);
  leg->AddEntry( ratio.get(), "Reweighted / Nominal", "p" );
  leg->Draw();
  
  TString dirname_png=dirname;
  dirname_png.ReplaceAll("pdf.","png.");
  
  can->SaveAs(dirname+"/"+figureName+".pdf");
  can->SaveAs(dirname_png+"/"+figureName+".png");
  
  for (TH1D* h : ratioProj) delete h;
  
}

void plotStressTestResult(vector<TH1D*>& ratios,TLegend* leg,const TString& dirname,const TString& name) {
  
  const double ymin=0.95;
  const double ymax=1.08;
  
  functions::plotHistogramsWithFixedYRange(ratios,leg,dirname,name,"",ymin,ymax);

}

void plotStressTestResultND(vector<TH1D*>& ratios,
			    const HistogramNDto1DConverter* histogramConverterND,
			    MultiDimensionalPlotsSettings* plotSettingsND,
			    const TString& variable,
			    const TString& level,
			    TLegend* leg,
			    const TString& dirname,
			    const TString& figureName)
{
  
  const double ymin=0.95;
  const double ymax=1.08;
  
  const int nratios = ratios.size();
  vector<vector<TH1D*>> ratiosProj(nratios);
  for(int i=0;i<nratios;i++){
    ratiosProj[i] = histogramConverterND->makeProjections(ratios[i],Form("ratio_%i",i));

  }
  
  const int nprojections = ratiosProj.at(0).size();
  
  vector<TString> generalInfo;
  generalInfo.push_back("#bf{#it{ATLAS}} Simulation " +NamesAndTitles::getResultsStatus()+", #sqrt{s}=13 TeV");
  if(level.Contains("Parton")) generalInfo.push_back("Parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV");
  else generalInfo.push_back("Fiducial phase-space");

  const int nlinesInfo=generalInfo.size();
  
  int nPerLine(0),nlines(0);
  functions::getGridSize(nprojections,nlines,nPerLine);
  
  const double canvas_xsize = (1000./3) * nPerLine;
  const double canvas_ysize = 175.*(nlines+1);
  
  plotSettingsND->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, nlinesInfo);
  
  auto can = std::unique_ptr<TCanvas>(plotSettingsND->makeCanvas("can"));
  can->cd();
  vector<vector<shared_ptr<TPad>>> pads = plotSettingsND->makeArrayOfPads(nprojections);
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable);
  
  int iproj=0;
  for(int iline=0;iline<nlines;iline++) {
    int nInLine = pads[iline].size();
    
    for(int iInLine=0;iInLine<nInLine;iInLine++) {
      TPad* currentPad = pads[iline][iInLine].get();
      
      vector<TH1D*> histos(nratios);
      for(int i=0;i<nratios;i++) {
	
	histos[i] = ratiosProj[i][iproj];
	plotSettingsND->setHistLabelStyle(histos[i]);
	
	if(iInLine ==0 && iline ==0) { 
	  histos[i]->GetYaxis()->SetTitle("Reweighted / Nominal");
	}
	else {
	  histos[i]->GetYaxis()->SetTitle("");
	}
	if(iline != nlines-1) { 
	  histos[i]->GetXaxis()->SetTitle("");
	}
	else {
	  histos[i]->GetXaxis()->SetTitle(xtitles.back());
	}
	
	histos[i]->GetYaxis()->SetRangeUser(ymin,ymax);
	
      }
            
      
      
      currentPad->cd();
      
      std::vector<TString> externalBinInfo = histogramConverterND->getExternalBinsInfo(iproj);
      TPaveText* paveText = functions::makePaveText(externalBinInfo, plotSettingsND->textSizePad(), 0.35, 0.75, 0.93); // xmin,xmax,ymax
      paveText->SetFillStyle(0);
      
      
      histos[0]->Draw("p");
      for(int i=1;i<nratios;i++) {
	histos[i]->Draw("same");
      }
      
      paveText->Draw();
      
      currentPad->RedrawAxis("g"); 
      currentPad->RedrawAxis();
      
      iproj++;
    }
  }
  
  can->cd();
  
  TPaveText* paveTextGeneralInfo = plotSettingsND->makePaveText(generalInfo);
  paveTextGeneralInfo->Draw();
  
  leg->Draw();
  
  can->Update();
  plotSettingsND->setLegendAttributes(leg,0.45,0.98);
  leg->SetFillStyle(0);
  leg->SetNColumns(3);
    
  TString dirname_png=dirname;
  dirname_png.ReplaceAll("pdf.","png.");
  
  can->SaveAs(dirname+"/"+figureName+".pdf");
  can->SaveAs(dirname_png+"/"+figureName+".png");
  
  for(int i=0;i<nratios;i++) for(int j=0;j<nprojections;j++) delete ratiosProj[i][j];
 
}




