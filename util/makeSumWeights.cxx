#include "HelperFunctions/functions.h"
#include "HelperFunctions/stringFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib> 
#include <vector>


#include "TString.h" 
#include "TSystem.h"
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TStopwatch.h"
#include "TH1D.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

std::vector<TString> readFileInLines(const TString& filename);
std::vector<std::unique_ptr<TFile>> makeListOfFiles(const std::vector<TString>& fileNames);
TChain* makeChain(const std::vector<std::unique_ptr<TFile>>& files, const TString& treeName);

class Minitree {
  
  private:
    TChain* m_chain;
    
    TBranch        *b_totalEvents;   //!
    TBranch        *b_totalEventsWeighted;   //!
    TBranch        *b_totalEventsWeighted_mc_generator_weights;   //!
  
  public:
  
    std::vector<float>* totalEventsWeighted_mc_generator_weights=0;
    ULong64_t totalEvents;
    float totalEventsWeighted;
        

    Minitree(TChain* chain) : m_chain(chain) {init();}
    void init() {
      
      totalEventsWeighted_mc_generator_weights=0;
      
      m_chain->SetBranchAddress("totalEvents", &totalEvents, &b_totalEvents);
      m_chain->SetBranchAddress("totalEventsWeighted", &totalEventsWeighted, &b_totalEventsWeighted);
      
    }
    
    void initGenWeights() {
      m_chain->SetBranchAddress("totalEventsWeighted_mc_generator_weights", &totalEventsWeighted_mc_generator_weights, &b_totalEventsWeighted_mc_generator_weights);
    }
    
    int getEntry(Long64_t entry) {
      if (!m_chain) return 0;
      return m_chain->GetEntry(entry);
    }
    
    
};

int makeSumWeights(const TString& mainDir, const TString& pathConfig, const TString& fileListDir, const TString& outputFileName, int debugLevel)
{
  
  TH1::AddDirectory(false);
  
  if (debugLevel>0) {
    std::cout << "mainDir: " << mainDir << std::endl;
    std::cout << "config: " << pathConfig << std::endl;
    std::cout << "fileListDir: " << fileListDir << std::endl;
    
  }
  
  auto fOutput = std::unique_ptr<TFile>{TFile::Open(mainDir+"/"+outputFileName,"RECREATE")};
  
  std::cout << "Creating output file with name: " << fOutput->GetName() <<std::endl;
  
  
  auto ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(pathConfig);
  const std::vector<int>& signalDSIDs = ttbarConfig->signalDSIDs();
  
  
  
  const TString treeName = "sumWeights";
  const TString textfileName="allhad.boosted.output.txt";
  
  
  std::vector<TString> samples;
  
  void *dirp = gSystem->OpenDirectory(fileListDir);
  char* dir;
  while ((dir = const_cast<Char_t *>(gSystem->GetDirEntry(dirp)))) {
   
   TString dirname = dir;
   
   if(!dirname.BeginsWith("MC16")) continue;
   std::cout << fileListDir + "/" + dirname << std::endl;
  
   samples.push_back(dir);
  
  }
  
  
  for(const TString& sample : samples) {
    
    std::cout << "Processing sample with name: " << sample << std::endl; 
    
    const TString fullName = fileListDir + "/" + sample + "/" + textfileName;
    
    std::vector<TString> vec = functions::splitTString(sample,".");
    TString prod = vec.at(0);
    int dsid = std::atoi(vec.at(1));
    std::cout << prod << " " << dsid << std::endl;
    
    bool isSignal = (std::find(std::begin(signalDSIDs),std::end(signalDSIDs),dsid)!=std::end(signalDSIDs));
    
    
    std::vector<TString> fileNames = readFileInLines(fullName);
    std::vector<std::unique_ptr<TFile>> files = makeListOfFiles(fileNames);
    auto chain = std::unique_ptr<TChain>{makeChain(files,treeName)};
    auto minitree = std::make_unique<Minitree>(chain.get());
    if(isSignal) minitree->initGenWeights();
    
    long nentries = chain->GetEntries();
    
    std::cout << "Nentries: " << nentries << std::endl;
    
    minitree->getEntry(0);
    
    int nGenWeights(0);
    
    fOutput->mkdir(prod+"."+dsid);
    fOutput->cd(prod+"."+dsid);
    
    auto h_nEventsTotal = std::make_unique<TH1D>("nEventsTotal","nEventsTotal",1,0.5,1.5);
    auto h_nEventsTotalWeighted = std::make_unique<TH1D>("nEventsTotalWeighted","nEventsTotal",1,0.5,1.5);
    
    std::cout << "totalEvents: " << minitree->totalEvents << std::endl;
    std::cout << "totalEventsWeighted: " << minitree->totalEventsWeighted << std::endl;
    
    
    std::unique_ptr<TH1D> h_sumGenWeights;
    if(isSignal) {
      nGenWeights=minitree->totalEventsWeighted_mc_generator_weights->size();
      h_sumGenWeights=std::make_unique<TH1D>("totalEventsWeighted_mc_generator_weights","nEventsTotal",nGenWeights,0.5,nGenWeights-0.5);
    }
    
    for(long ientry =0; ientry<nentries;ientry++) {
      minitree->getEntry(ientry);
     
      h_nEventsTotal->Fill(1,minitree->totalEvents);
      h_nEventsTotalWeighted->Fill(1,minitree->totalEventsWeighted);
      
      if(isSignal) {
	for(int i=0;i<nGenWeights;i++) {
	  double weight = minitree->totalEventsWeighted_mc_generator_weights->at(i);
	  if(!std::isfinite(weight)) std::cout << "Error: not a number in mc_generator_weights std::vector: " << i << std::endl;
	  else h_sumGenWeights->Fill(i,weight);
	}
      }
    }
    
    h_nEventsTotal->Write();
    h_nEventsTotalWeighted->Write();
    if(isSignal) h_sumGenWeights->Write();
  }
  
  
  
  
  std::cout << "All done!" << std::endl;
  return 0;
}

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to calculate normalization of MC samples.  

Usage:  
  makeSumWeights [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.json]
  --fileListDir <fileList>                Path to the filelist dir. [default: TtbarDiffCrossSection/filelists]
  --outputFile <outputFile>                Path to the filelist dir. [default: sumWeights.root]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  
  TString mainDir("");
  TString pathConfig("");
  TString fileListDir("");
  TString outputFile("");
  int debugLevel(0);
  
  try {
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ pathConfig=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ fileListDir=args["--fileListDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--fileListDir option is expected to be a string. Check input parameters."); }
	
      try{ outputFile=args["--outputFile"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputFile option is expected to be a string. Check input parameters."); }
	
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      std::cout << e.what() << std::endl << std::endl;
      std::cout << USAGE << std::endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    
    return makeSumWeights( mainDir, pathConfig, fileListDir, outputFile, debugLevel);
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  
  return 0;
}

std::vector<TString> readFileInLines(const TString& filename) {
  
  std::vector<TString> res;
  
  std::ifstream file(filename.Data());
  
  std::string line;
  while (std::getline(file, line)) {
    res.push_back(line);
  }
  
  return res;
}

std::vector<std::unique_ptr<TFile>> makeListOfFiles(const std::vector<TString>& fileNames) {
  const int size = fileNames.size();
  //const int size = 27;
  std::vector<std::unique_ptr<TFile>> res;
  const int nprint=1;
  for(int i=0;i<size;i++) {
    if( (i+1)%nprint == 0 ) std::cout << "Opening files: " << i+1 << "/" << size << " filename: " << fileNames[i] << std::endl;
    
    TFile* f = TFile::Open(fileNames[i]);
    if (!f || f->IsZombie()) {
      std::cout << "Error: Failed to open " << fileNames[i] << " File is skipped." << std::endl;
      if(f) delete f;
      continue;
    } 
    res.push_back(std::unique_ptr<TFile>(f));
  }
  return res;
}

TChain* makeChain(const std::vector<std::unique_ptr<TFile>>& files, const TString& treeName) {
 
  TChain* chain = new TChain(); 
  for (auto& file : files){
    chain->AddFile(file->GetName(), TChain::kBigNumber, treeName.Data());
  }
  return chain;
}
