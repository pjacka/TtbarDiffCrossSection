// Peter Berta, 15.10.2015

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"

using namespace std;

int main(int nArg, char **arg) {
  if(nArg!=6){
    cout << "Error: Wrong number of input parameters!" << endl;
    return -1;
  }
  TH1::AddDirectory(kFALSE);
  TString mainDir=arg[1];
  TString outputSubdir=arg[2];
  TString path_config_files=arg[3];
  TString path_config_lumi=arg[4];
  TString variable = arg[5];
  
  try {
    SetAtlasStyle();
    TString ttbar_path = gSystem->Getenv("TTBAR_PATH");
    
    unique_ptr<NtuplesAnalysisToolsConfig> ttbarConfig= make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
    unique_ptr<TEnv> configFiles = make_unique<TEnv>(path_config_files);
    
    TString mc_samples_production = ttbarConfig->mc_samples_production();
    TString lumi_string = configFunctions::getLumiString(*ttbarConfig).c_str();
    
    cout << "Using lumi: " << lumi_string << endl;
    
    //shared_ptr<TFile> fSignalNominal( TFile::Open(mainDir + "/MC16a." + configFiles->GetValue("signal_410471_filename","")) );
    //shared_ptr<TFile> fSignalPS( TFile::Open(mainDir + "/MC16a." + configFiles->GetValue("ttbar_signal_PS_filename","")) );
    //shared_ptr<TFile> fSignalME( TFile::Open(mainDir + "/MC16a." + configFiles->GetValue("ttbar_signal_ME_filename","")) );
      
    shared_ptr<TFile> fSignalNominal( TFile::Open(mainDir + "/All.ttbar_allhad_nominal_merged/allhad.boosted.output.root",""));
    shared_ptr<TFile> fSignalAlphaSUp(  TFile::Open(mainDir + "/All.ttbar_allhad_nominal_merged/allhad.boosted.output.root",""));
    shared_ptr<TFile> fSignalAlphaSDown(  TFile::Open(mainDir + "/All.ttbar_allhad_nominal_merged/allhad.boosted.output.root",""));   

    //shared_ptr<TFile> fSignalNominal( TFile::Open(mainDir + "/All.410471.PhPy8EG/allhad.boosted.output.root",""));
    //shared_ptr<TFile> fSignalAlphaSUp(  TFile::Open(mainDir + "/All.410471.PhPy8EG/allhad.boosted.output.root",""));
    //shared_ptr<TFile> fSignalAlphaSDown(  TFile::Open(mainDir + "/All.410471.PhPy8EG/allhad.boosted.output.root",""));  
      
    if(fSignalNominal->IsZombie())throw TtbarDiffCrossSection_exception("Error: Signal nominal sample does not exist");
    if(fSignalAlphaSUp->IsZombie())throw TtbarDiffCrossSection_exception("Error: Signal alphaS up sample does not exist");
    if(fSignalAlphaSDown->IsZombie())throw TtbarDiffCrossSection_exception("Error: Signal alphaS down sample does not exist");
    
    TString directoryNom = "nominal/Leading_1t_1b_Recoil_1t_1b_tight/RecoLevel/";

    TString directoryUp = "ALPHAS_FSR__1up/Leading_1t_1b_Recoil_1t_1b_tight/RecoLevel/";
    TString directoryDown = "ALPHAS_FSR__1down/Leading_1t_1b_Recoil_1t_1b_tight/RecoLevel/";
    //TString directoryUp = "FSR_muRfac_20/Leading_1t_1b_Recoil_1t_1b_tight/RecoLevel/";
    //TString directoryDown = "FSR_muRfac_05/Leading_1t_1b_Recoil_1t_1b_tight/RecoLevel/";







    //if(variable.EndsWith("RecoLevel"))directory="nominal/Leading_1t_1b_Recoil_1t_1b_tight/unfolding/";
   

    TH1D *h_nominal( (TH1D*)fSignalNominal->Get(directoryNom+variable) ); 
    cout << "Integral: " << h_nominal->Integral() << endl;
    
    TH1D *h_alphaS_up( (TH1D*)fSignalAlphaSUp->Get(directoryUp+variable) ); 
    cout << "Integral: " << h_alphaS_up->Integral() << endl;
 
    TH1D *h_alphaS_down( (TH1D*)fSignalAlphaSDown->Get(directoryDown+variable) ); 
    cout << "Integral: " << h_alphaS_down->Integral() << endl;

    vector<TString> directories;
    directories.push_back("Leading_0t_1b_Recoil_1t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_0b_Recoil_1t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_1b_Recoil_0t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_1b_Recoil_1t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_0b_Recoil_1t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_1b_Recoil_0t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_1b_Recoil_1t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_0b_Recoil_0t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_0b_Recoil_1t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_1b_Recoil_0t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_0b_Recoil_0t_1b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_0b_Recoil_1t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_1b_Recoil_0t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_1t_0b_Recoil_0t_0b_tight/RecoLevel/"); 
    directories.push_back("Leading_0t_0b_Recoil_0t_0b_tight/RecoLevel/"); 
    

    for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i
       //TH1D *h_nominal_region( (TH1D*)fSignalNominal->Get("ALPHAS_FSR__0875/"+*i+variable) );
       TH1D *h_nominal_region( (TH1D*)fSignalNominal->Get("nominal/"+*i+variable) );
       h_nominal->Add(h_nominal_region);
       cout << "h_nominal->Integral: " << h_nominal_region->Integral() << "  h_nominal_all_regions->Integral(): "  << h_nominal->Integral() << endl;
    }

    for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i

       TH1D *h_alphaS_up_region( (TH1D*)fSignalAlphaSUp->Get("ALPHAS_FSR__1up/"+*i+variable) );
       //TH1D *h_alphaS_up_region( (TH1D*)fSignalAlphaSup->Get("FSR_muRfac_05/"+*i+variable) );
       


       h_alphaS_up->Add(h_alphaS_up_region);
       cout << "h_alphaS_up->Integral: " << h_alphaS_up_region->Integral() << "  h_alphaS_up_all_regions->Integral(): "  << h_alphaS_up->Integral() << endl;
    }

      for(vector<TString>::const_iterator i = directories.begin(); i != directories.end(); ++i) {
       // process i

       TH1D *h_alphaS_down_region( (TH1D*)fSignalAlphaSDown->Get("ALPHAS_FSR__1down/"+*i+variable) );
       //TH1D *h_alphaS_down_region( (TH1D*)fSignalAlphaSDown->Get("FSR_muRfac_05/"+*i+variable) );
  



       h_alphaS_down->Add(h_alphaS_down_region);
       cout << "h_alphaS_down->Integral: " << h_alphaS_down_region->Integral() << "  h_alphaS_down_all_regions->Integral(): "  << h_alphaS_down->Integral() << endl;
    }


    //shared_ptr<TH1D> h_nominal( (TH1D*)fSignalNominal->Get(directory+variable) );
    //shared_ptr<TH1D> h_PS ( (TH1D*)fSignalPS->Get(directory+variable) );
    //shared_ptr<TH1D> h_ME ( (TH1D*)fSignalME->Get(directory+variable) );
  
    //functions::DivideByBinWidth(h_nominal.get());
    //functions::DivideByBinWidth(h_PS.get());
    //functions::DivideByBinWidth(h_ME.get());
    //h_nominal->Scale(1./lumi);
    //h_PS->Scale(1./lumi);
    //h_ME->Scale(1./lumi);

      //h_nominal->Scale(1./h_nominal->Integral());
      //h_PS->Scale(1./h_PS->Integral());
      //h_ME->Scale(1./h_ME->Integral());

    functions::DivideByBinWidth(h_nominal);
    functions::DivideByBinWidth(h_alphaS_up);
    functions::DivideByBinWidth(h_alphaS_down);
    //h_nominal->GetYaxis()->SetTitle("#frac{d#sigma}{dx} [unit^{-1}]");
  
  
    h_alphaS_up->SetLineColor(2);
    h_alphaS_up->SetLineWidth(2);
    h_alphaS_up->SetMarkerColor(2);
    h_alphaS_up->SetLineStyle(2);
 
    h_alphaS_down->SetLineColor(3);
    h_alphaS_down->SetLineWidth(3);
    h_alphaS_down->SetMarkerColor(3);
    h_alphaS_down->SetLineStyle(3);

    TString outputDirPDF = mainDir + "/pdf.All/" + outputSubdir + "/"; 
    TString outputDirPNG = mainDir + "/png.All/" + outputSubdir + "/"; 
    gSystem->mkdir(outputDirPDF,true);
    gSystem->mkdir(outputDirPNG,true);
    
    //auto c = make_shared<TCanvas> ("c","");
    
  
    //h_flat->Draw();
    
    //h_sliced->Draw("Same");
    
    
    
    
    
    //c->SaveAs(outputDirPDF + variable + ".pdf");
    //c->SaveAs(outputDirPNG + variable + ".png");
    
    
    auto leg = make_shared<TLegend>(0.55,0.6,0.9,0.8);
    leg->SetNColumns(1)   ;
    leg->SetFillColor(0)  ;
    leg->SetFillStyle(0)  ;
    leg->SetBorderSize(0) ;
    leg->SetTextFont(42)  ;
    leg->SetTextSize(0.06);
    
    leg->AddEntry(h_nominal,"Nominal");
    //leg->AddEntry(h_nominal,"#alphaS FSR 0.875");
    leg->AddEntry(h_alphaS_up,"#alpha_{S} FSR Up");
    leg->AddEntry(h_alphaS_down,"#alpha_{S} FSR Down");
    vector<TH1D*> histos = {h_nominal,h_alphaS_up,h_alphaS_down};
    
    TString y2name="Other/Nominal";
    //double y2min=0.84;
    //double y2max=1.16;

    double y2min=0.59;
    double y2max=1.41;   



    bool use_logscale=false;
    
    functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_all_regions_with_nominal.pdf",histos[0], y2name, y2min, y2max, "",use_logscale);
    functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_log_all_regions_with_nominal.pdf",histos[0], y2name, y2min, y2max, "",true);
    functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_all_regions_with_nominal.png",histos[0], y2name, y2min, y2max, "",use_logscale);
    functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_log_all_regions_with_nominal.png",histos[0], y2name, y2min, y2max, "",true);
 
    //functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_with_nominal.pdf",histos[0], y2name, y2min, y2max, "",use_logscale);
    //functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_log_with_nominal.pdf",histos[0], y2name, y2min, y2max, "",true);
    //functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_with_nominal.png",histos[0], y2name, y2min, y2max, "",use_logscale);
    //functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_log_with_nominal.png",histos[0], y2name, y2min, y2max, "",true);   

    //functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_all_regions.pdf",histos[0], y2name, y2min, y2max, "",use_logscale);
    //functions::plotHistograms(histos,leg.get(),outputDirPDF,variable+"_log_all_regions.pdf",histos[0], y2name, y2min, y2max, "",true);
    //functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_all_regions.png",histos[0], y2name, y2min, y2max, "",use_logscale);
    //functions::plotHistograms(histos,leg.get(),outputDirPNG,variable+"_log_all_regions.png",histos[0], y2name, y2min, y2max, "",true);   
        
    
  }catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  cout << "drawSamplesComparison: Done." << endl;
  return 0;
}
