// Peter Berta, 15.10.2015

#include "TtbarDiffCrossSection/RunOverMinitree.h"

#include "AsgMessaging/StatusCode.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "THnBase.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to run over ntuples and fill histograms for individual systematic branches. 

Usage:  
  produceHistos [options]
  
Options:
  -h --help                               Show help screen.
  --filelist <path>, -i <path>            Path to file with filelist.
  --output <outputfile>, -o <outputfile>  Path to output file.
  --config <configfile>                   Path to config file.
  --doDetSys <int>                        Activates detector systematics. [default: 0]
  --doSysPDF <int>                        Activates PDF systematics branches. [default: 0]
  --doSysAlphaSFSR <int>                  Activates AlphaS FSR systematics. [default: 0]
  --doSysIFSR <int>                       Activates IFSR systematics. [default: 0]
  --doReweighting <int>                   Activates reweighting for stress test. [default: 0] 
  --nBootstrapReplicas <int>              Number of replicas used in bootstrap method. Method is activated if nBootstrapReplicas > 0. [default: 0]
  --nEvents <int>                         Set number of events to run over with. Running over all events if nEvents < 0. [default: -1]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";

bool checkParameters(std::map<std::string, docopt::value>& args){
  
  bool ok=true;
  
  if( !args["--filelist"].isString() ){
    cout << "Error: --filelist option is mandatory and it is expected to be a string." << endl;
    ok=false;
  }
  
  if( !args["--output"].isString() ){
    cout << "Error: --output option is mandatory. Option expected to be a string." << endl;
    ok=false;
  }
  if( !args["--config"].isString() ){
    cout << "Error: --config option is mandatory. Option expected to be a string." << endl;
    ok=false;
  }
  
  if(!ok)cout << USAGE << endl;
  
  return ok;
}

int main(int nArg, char **arg) {

  cout << endl << endl << " ***** running  " << arg[0] << " ***** " << endl << endl;

  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true);  // Using docopt package to read parameters

  cout << "Reading parameters." << endl;  
  
  if (!checkParameters(args) ) return -3;
  
  TString filelist("");
  TString output("");
  TString configName("");
  bool doSys (0);
  bool doSysPDF (0);
  bool doSysAlphaSFSR(0);
  bool doSysIFSR(0);
  int  doReweighting(0);
  int  doBootstrap=(0);
  int  nEvents(-1);
  int  debugLevel(0);
  
  try{
  
    try{ filelist=args["--filelist"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--filelist option is mandatory and it is expected to be a string. Check input parameters."); }
      
    try{ output=args["--output"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--output option is mandatory. Option expected to be a string. Check input parameters."); }
      
    try{ configName=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is mandatory. Option expected to be a string. Check your input parameters."); }
      
    try{ doSys = args["--doDetSys"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doDetSys is not an integer. Check input parameters."); }
      
    try{ doSysPDF = args["--doSysPDF"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doSysPDF is not an integer. Check input parameters."); }
      
    try{ doSysAlphaSFSR = args["--doSysAlphaSFSR"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doSysAlphaSFSR is not a long variable. Check input parameters."); }
      
    try{ doSysIFSR = args["--doSysIFSR"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doSysIFSR is not an integer. Check input parameters."); }

    try{ doReweighting = args["--doReweighting"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doReweighting is not an integer. Check input parameters."); }
      
    try{ doBootstrap = args["--nBootstrapReplicas"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--nBootstrapReplicas is not an integer. Check input parameters."); }
      
    try{ nEvents = args["--nEvents"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--nEvents is not an integer. Check input parameters."); }
  
    try{ debugLevel = args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel is not an integer. Check input parameters."); }
  
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }  

  if(debugLevel>0){

    cout << "RunOverMinitree will be initialized with the following parameters:" << endl;
    
    cout << "filelist: " << filelist << endl;
    cout << "output: " << output << endl;
    cout << "Config file: " << configName << endl;
    cout << "doSys: " << doSys << endl;
    cout << "doSysPDF: " << doSysPDF << endl;
    cout << "doSysIFSR: " << doSysIFSR << endl;
    cout << "doReweighting: " << doReweighting << endl;
    cout << "nEvents: " << nEvents << endl;
    cout << "debugLevel: " << debugLevel << endl;
    cout << endl;

  }

  StatusCode::enableFailure();

  auto analysis = std::make_unique<RunOverMinitree>(
    filelist,
    output,
    configName,
    doSys,
    doSysPDF,
    doSysAlphaSFSR,
    doSysIFSR,
    doReweighting,
    doBootstrap,
    nEvents,
    debugLevel
  );
  
  analysis->initialize();
  analysis->execute();
  analysis->finalize();

  return 0;
}
