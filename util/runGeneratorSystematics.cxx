// Peter Berta, 15.10.2015

#include "CovarianceCalculator/GeneratorSystematics.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "CovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/plottingFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TEnv.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

int runGeneratorSystematics(TString mainDir,
                            TString path_config_lumi,
                            TString signal,
                            TString variable,
                            TString level,
                            TString inputDir,
                            TString outputDir,
                            bool doSysPDF,
                            bool shiftNominal,
			    int debugLevel)
{
  if (debugLevel>0) cout << "doSysPDF = "<<doSysPDF << endl;
  
  // initialization part
  functions::setStyle();
  
  TH1::AddDirectory(kFALSE);
  
  auto config = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  TString mc_samples_production = config->mc_samples_production();
  
  TString sysHistosDir= "root." + mc_samples_production + "/" + inputDir;
  
  GeneratorSystematics* genSys = new GeneratorSystematics;
  genSys->init(mainDir,sysHistosDir,outputDir,path_config_lumi,signal,doSysPDF,debugLevel);
  genSys->LoadHistos(variable,level);
  genSys->LoadDataHistos(variable,level);
  
  genSys->prepareReplicas(10000,shiftNominal); // For evaluation of stat uncert. of modeling uncertainties
  genSys->runPseudoexperiments(); // Evaluation of stat uncert. of modeling uncertainties
  
  genSys->prepareHistos(); // dividing by bin width truth level distributions
  
  genSys->CalculateSysErrors(); // Calculate covariances and relative uncertainties
  genSys->writeCovariances();  // Write covariances into rootfiles
  
  
  // Now follows drawing of testing plots
  genSys->drawErrorsComparison();
  //genSys->drawTruthHistComparison();
  //genSys->drawRecoCorrectionsComparison();
  //genSys->drawTruthCorrectionsComparison();
  //genSys->drawMigrationMatrices();
  //if(level=="PartonLevel"){
    //genSys->drawRecoLevelHistComparison();
    //genSys->drawNormalizedRecoLevelHistComparison();
  //}
  //genSys->drawUnfoldedHistComparison();
  
  genSys->drawNormalizedErrorsComparison();
  //genSys->drawNormalizedTruthHistComparison();
  //genSys->drawNormalizedUnfoldedHistComparison();
  
  delete genSys;
  
  cout << "All done!" << endl;
  return 0;  
}


// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to calculate signal modeling systematic uncertainties.  

Usage:  
  runGeneratorSystematics [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --signal <string>                       Signal name. Should be consistent with runGeneratorSystematics configuration. [default: 410471]
  --variable <string>                 	  Variable name. [default: total_cross_section]
  --level <string>                        ParticleLevel or PartonLevel. [default: ParticleLevel]
  --inputDir <string>                     Directory with input histograms for uncertainties calculation. [default: systematics_histos}
  --outputDir <string>                    Output directory. [default: generatorSystematics}
  --doSysPDF <int>                        Switch to activate PDF systematics. [default: 0]
  --shiftNominal <int>                    Switch to shift nominal sample when evaluating statistical fluctuations in signal modeling uncertainties. [default: 0]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";

int main(int nArg, char **arg) {
  
  TString mainDir("");
  TString path_config_lumi("");
  TString signal("");
  TString variable("");
  TString level("");
  TString inputDir("");
  TString outputDir("");
  bool doSysPDF(0);
  bool shiftNominal(0);
  int debugLevel(0);
  
  try {
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ signal=args["--signal"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--signal option is expected to be a string. Check input parameters."); }
	
      try{ variable=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
      
      try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
      try{ inputDir=args["--inputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--inputDir option is expected to be a string. Check input parameters."); }
      
      try{ doSysPDF=args["--doSysPDF"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--doSysPDF option is expected to be an integer. Check input parameters."); }
	
      try{ shiftNominal=args["--shiftNominal"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--shiftNominal option is expected to be an integer. Check input parameters."); }
	
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    cout << endl << "Running ProducePseudoexperiments for variable " << variable << " at " << level  << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
    
    
    return runGeneratorSystematics( mainDir, path_config_lumi, signal, variable, level, inputDir, outputDir, doSysPDF, shiftNominal, debugLevel);
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  
  return 0;
}
