#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "CovarianceCalculator/CovarianceCalculatorFunctions.h"

#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TEnv.h"
#include "TPrincipal.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"


using namespace std;


int main(int nArg, char **arg) {
		
  try {
  
    cout << "Running prepareUnfoldedDataStatBootstrappedPseudoexperiments with nReplicas " << arg[4] << endl;
	  
    if (nArg !=5){
      cout << nArg << endl;
      throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    }
    
    TString mainDir = arg[1];
    TString configLumiString=arg[2];
    TString level=arg[3];
    int nReplicas = atoi(arg[4]);
    cout << "mainDir: " << mainDir << endl;
    cout << "level: " << level << endl;
    cout << "nReplicas: " << nReplicas << endl;
    if( nReplicas <= 0 ) throw TtbarDiffCrossSection_exception("Error: Number of replicas is not positive number!"); 
    
    TEnv* configLumi = new TEnv(configLumiString);
    double lumi=configLumi->GetValue("lumi",0.);
    
    TH1::AddDirectory(kFALSE);
    vector<TString> spectrums,pretty_names;
    
    if(level.Contains("Parton")){
      spectrums.push_back("topCandidate_pt");pretty_names.push_back("p_{T}^{t}");
      spectrums.push_back("randomTop_y");pretty_names.push_back("|y^{t}|");
    }
    spectrums.push_back("leadingTop_pt");pretty_names.push_back("p_{T}^{t,1}");
    spectrums.push_back("leadingTop_y");pretty_names.push_back("|y^{t,1}|");
    spectrums.push_back("subleadingTop_pt");pretty_names.push_back("p_{T}^{t,2}");
    spectrums.push_back("subleadingTop_y");  pretty_names.push_back("|y^{t,2}|");
    spectrums.push_back("ttbar_mass");pretty_names.push_back("m^{t#bar{t}}");
    spectrums.push_back("ttbar_pt");pretty_names.push_back("p_{T}^{t#bar{t}}");
    spectrums.push_back("ttbar_y");pretty_names.push_back("|y^{t#bar{t}}|");
    spectrums.push_back("chi_ttbar"); pretty_names.push_back("#chi^{t#bar{t}}"); 
    spectrums.push_back("ttbar_deltaphi"); pretty_names.push_back("#Delta #phi^{t#bar{t}}"); 
    spectrums.push_back("pout");pretty_names.push_back("|p_{out}^{t#bar{t}}|");
    spectrums.push_back("y_boost");pretty_names.push_back("y_{B}^{t#bar{t}}");
    spectrums.push_back("cos_theta_star");pretty_names.push_back("cos#theta^{*}"); 
    spectrums.push_back("H_tt_pt");pretty_names.push_back("H_{T}^{t#bar{t}}");
    
    const int nSpectrums=spectrums.size();
    
    //spectrums.push_back("z_tt_pt"); 
    //spectrums.push_back("y_star");
    //spectrums.push_back("inclusive_top_pt");
    //spectrums.push_back("inclusive_top_y"); 
    vector<TH1DBootstrap*> data(nSpectrums);
    vector<TH1D*> signal_reco(nSpectrums), effOfRecoLevelCutsNominator(nSpectrums), effOfTruthLevelCutsNominator(nSpectrums), signal_truth(nSpectrums);
    vector<TH2D*> migration(nSpectrums);
    vector<TH1D*> bkg_all(nSpectrums);
    vector<RooUnfoldResponse*> response(nSpectrums);
    double* nbins_vector = new double[nSpectrums];
    int nbins_all(0);
    
    cout << "Info: Loading nominal migration matrices, efficiencies, acceptances and background for all spectra." << endl;
    for(int iSpectrum=0;iSpectrum<nSpectrums;iSpectrum++){
      TString variable_name=spectrums[iSpectrum]+"_fine";
      TFile* file_systematics= new TFile(mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root");
      if(file_systematics->IsZombie()){
	throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/root/systematics_histos/" + variable_name + "_" + level + ".root"); 
	return -1;
      }
      
      signal_reco[iSpectrum]=(TH1D*)file_systematics->Get((TString)"nominal/" + spectrums[iSpectrum] + "_signal_reco");
      effOfRecoLevelCutsNominator[iSpectrum]=(TH1D*)file_systematics->Get((TString)"nominal/" + spectrums[iSpectrum] + "_EffOfRecoLevelCutsNominator");
      effOfTruthLevelCutsNominator[iSpectrum]=(TH1D*)file_systematics->Get((TString)"nominal/" + spectrums[iSpectrum] + "_EffOfTruthLevelCutsNominator");
      signal_truth[iSpectrum]=(TH1D*)file_systematics->Get((TString)"nominal/" + spectrums[iSpectrum] + "_signal_truth");
      migration[iSpectrum]=(TH2D*)file_systematics->Get((TString)"nominal/" + spectrums[iSpectrum] + "_migration");
      
      
      
      bkg_all[iSpectrum]=(TH1D*)file_systematics->Get((TString)"nominal/" + spectrums[iSpectrum] + "_bkg_all");
      
      
      
      response[iSpectrum] = new RooUnfoldResponse(0,0,migration[iSpectrum]);
		//response->UseOverflow(true);
      
      nbins_vector[iSpectrum] = signal_reco[iSpectrum]->GetNbinsX();
      nbins_all+=nbins_vector[iSpectrum];
      
      //cout << effOfRecoLevelCuts[iSpectrum]->GetBinContent(1) << " " << effOfTruthLevelCuts[iSpectrum]->GetBinContent(1) << " " << migration[iSpectrum]->GetBinContent(1,1) << " " << bkg_all[iSpectrum]->Integral() << endl;
      delete file_systematics;
    }
    cout << "Info: Nominal migration matrices, efficiencies, acceptances and background loaded." << endl;
    
    cout << "Info: Loading bootstrapped data for all spectra." << endl;
    for(int iSpectrum=0;iSpectrum<nSpectrums;iSpectrum++){
      TString variable_name=spectrums[iSpectrum]+"_fine";
      TFile* fBootstrapHistos = new TFile(mainDir+"/root/bootstrap/" + variable_name + "_" + level + ".root");
      if(fBootstrapHistos->IsZombie()){
	throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with bootstrapped histograms from " + mainDir+"/root/bootstrap/" + variable_name + "_" + level + ".root"); 
	return -1;
      }
      
      data[iSpectrum]=(TH1DBootstrap*)fBootstrapHistos->Get((TString)"nominal/"+spectrums[iSpectrum] + "_boots_data");
      //cout << data[iSpectrum]->GetNominal()->Integral() << endl;
      
      delete fBootstrapHistos;
    }
    cout << "Info: Bootstrapped data loaded." << endl;
    
    TString pathToOutput=mainDir+"/root/bootstrap_dataStat/";
    gSystem->mkdir(pathToOutput,true);
    TFile* fileOutput= new TFile(pathToOutput + level + ".root","RECREATE");
    fileOutput->mkdir("Absolute");
    fileOutput->mkdir("Relative");
    stringstream ss;
    
    
    TPrincipal* principal_absolute = new TPrincipal( nbins_all, "ND" );
    TPrincipal* principal_relative = new TPrincipal( nbins_all, "ND" );
    double* bincontents_all_absolute = new double[nbins_all];
    double* bincontents_all_relative = new double[nbins_all];
    
    
    
    for( int iReplica=0; iReplica<nReplicas ; iReplica++){
      //cout << "Info: Running replica no. " << iReplica + 1 << endl;
      int currentLowIndex=0;
      for(int iSpectrum=0;iSpectrum<nSpectrums;iSpectrum++){
	cout << spectrums[iSpectrum] << endl;
	TString variable_name=spectrums[iSpectrum];
	variable_name.ReplaceAll("_fine","");
	  
	TH1D* reco=(TH1D*)data[iSpectrum]->GetReplica(iReplica);
	if(1){
	//if(spectrums[iSpectrum].Contains("ttbar_y")){
	  const int nbins=reco->GetNbinsX();
	  cout << nbins << " " << bkg_all[iSpectrum]->GetNbinsX() << " " << reco->GetXaxis()->GetXmax() << " " << bkg_all[iSpectrum]->GetXaxis()->GetXmax() << " " << reco->GetXaxis()->GetXmin() << " " << bkg_all[iSpectrum]->GetXaxis()->GetXmin() << " " << TMath::AreEqualRel(reco->GetXaxis()->GetXmin(), bkg_all[iSpectrum]->GetXaxis()->GetXmin(),1.E-12) << " " << TMath::AreEqualRel(reco->GetXaxis()->GetXmax(), bkg_all[iSpectrum]->GetXaxis()->GetXmax(),1.E-12) << endl;
	  for(int i=1;i<=nbins;i++){
	    cout << i << " bootstrap [" << reco->GetXaxis()->GetBinLowEdge(i) << "," << reco->GetXaxis()->GetBinUpEdge(i) << "] signal [" << bkg_all[iSpectrum]->GetXaxis()->GetBinLowEdge(i) << "," << bkg_all[iSpectrum]->GetXaxis()->GetBinUpEdge(i) << "]" 
	    << " " << TMath::AreEqualRel( reco->GetXaxis()->GetBinLowEdge(i), bkg_all[iSpectrum]->GetXaxis()->GetBinLowEdge(i), 1E-10 )  << " " << (reco->GetXaxis()->GetBinLowEdge(i) - bkg_all[iSpectrum]->GetXaxis()->GetBinLowEdge(i))/reco->GetXaxis()->GetBinLowEdge(i) << endl;;
	    
	  }
	  
	}
	
	reco->Add(bkg_all[iSpectrum],-1);
	TH1D* unfoldedReplica = functions::UnfoldBayes(reco,signal_reco[iSpectrum],effOfTruthLevelCutsNominator[iSpectrum],signal_truth[iSpectrum],effOfRecoLevelCutsNominator[iSpectrum],response[iSpectrum],4,0);
	TH1D* unfoldedReplicaNormalized=(TH1D*)unfoldedReplica->Clone();
	unfoldedReplica->Scale(1./lumi);
	functions::DivideByBinWidth(unfoldedReplica);
	unfoldedReplicaNormalized->Scale(1./unfoldedReplicaNormalized->Integral());
	functions::DivideByBinWidth(unfoldedReplicaNormalized);
	
	const int nbins=reco->GetNbinsX();
	for(int i=0;i<nbins;i++){
	  bincontents_all_absolute[currentLowIndex+i]=unfoldedReplica->GetBinContent(i+1);
	  bincontents_all_relative[currentLowIndex+i]=unfoldedReplicaNormalized->GetBinContent(i+1);
	}
	currentLowIndex+=nbins;
	//ss.str("");
	//ss << variable_name + "_" << iReplica+1;
	//fileOutput->cd("Absolute");
	//unfoldedReplica->Write(ss.str().c_str());
	//fileOutput->cd("Relative");
	//unfoldedReplicaNormalized->Write(ss.str().c_str());
	
      }
      principal_absolute->AddRow( bincontents_all_absolute );
      principal_relative->AddRow( bincontents_all_relative );
    }
    principal_absolute->MakePrincipals();
    principal_relative->MakePrincipals();
    TMatrixT<double> cov_abs = *(TMatrixT<double>*) principal_absolute->GetCovarianceMatrix ();
    //cov_abs.Print();
    TMatrixT<double> cov_rel = *(TMatrixT<double>*) principal_relative->GetCovarianceMatrix ();
    //cov_rel.Print();
    
    TH2D* h2_cov_abs = new TH2D( "covariance absolute", "Covariance matrix (Absolute cross-sections)", nbins_all, 0.5, nbins_all+0.5, nbins_all, 0.5, nbins_all+0.5 );
    for(int i=0;i<nbins_all;i++)for(int j=0;j<nbins_all;j++)h2_cov_abs->SetBinContent( i+1, j+1, cov_abs[i][j] );
    TH2D* h2_corr_abs = new TH2D( "correlation absolute", "Correlation matrix (Absolute cross-sections)", nbins_all, 0.5, nbins_all+0.5, nbins_all, 0.5, nbins_all+0.5 );
    for(int i=0;i<nbins_all;i++)for(int j=0;j<nbins_all;j++)h2_corr_abs->SetBinContent( i+1, j+1, cov_abs[i][j]/sqrt(cov_abs[i][i]*cov_abs[j][j]) );
    h2_corr_abs->SetMaximum(  1.0 );
    h2_corr_abs->SetMinimum( -1.0 );
    
    TH2D* h2_cov_rel = new TH2D( "covariance relative", "Covariance matrix (Relative cross-sections)", nbins_all, 0.5, nbins_all+0.5, nbins_all, 0.5, nbins_all+0.5 );
    for(int i=0;i<nbins_all;i++)for(int j=0;j<nbins_all;j++)h2_cov_rel->SetBinContent( i+1, j+1, cov_rel[i][j] );
    TH2D* h2_corr_rel = new TH2D( "correlation_relative", "Correlation matrix (Relative cross-sections)", nbins_all, 0.5, nbins_all+0.5, nbins_all, 0.5, nbins_all+0.5 );
    for(int i=0;i<nbins_all;i++)for(int j=0;j<nbins_all;j++)h2_corr_rel->SetBinContent( i+1, j+1, cov_rel[i][j]/sqrt(cov_rel[i][i]*cov_rel[j][j]) );
    h2_corr_rel->SetMaximum(  1.0 );
    h2_corr_rel->SetMinimum( -1.0 );
    
    
    int k = 0;
    for (int iSpectrum=0;iSpectrum<nSpectrums;iSpectrum++){
      for(int i=0;i<nbins_vector[iSpectrum];i++){
	ss.str("");
	ss << pretty_names[iSpectrum] << " - " << i+1;
	TString label=ss.str().c_str();
	h2_cov_abs->GetXaxis()->SetBinLabel( k+1, label );
	h2_cov_abs->GetYaxis()->SetBinLabel( k+1, label );
	h2_corr_abs->GetXaxis()->SetBinLabel( k+1, label );
	h2_corr_abs->GetYaxis()->SetBinLabel( k+1, label );
	h2_cov_rel->GetXaxis()->SetBinLabel( k+1, label );
	h2_cov_rel->GetYaxis()->SetBinLabel( k+1, label );
	h2_corr_rel->GetXaxis()->SetBinLabel( k+1, label );
	h2_corr_rel->GetYaxis()->SetBinLabel( k+1, label );
	k++;
      }
    }
    
    
    
    
    
    
    fileOutput->cd("Absolute");
    h2_cov_abs->Write("Covariance");
    h2_corr_abs->Write("Correlation");
    fileOutput->cd("Relative");
    h2_cov_rel->Write("Covariance");
    h2_corr_rel->Write("Correlation");
    
    delete h2_cov_abs;
    delete h2_cov_rel;
    delete h2_corr_abs;
    delete h2_corr_rel;
    fileOutput->Close();
  } 
  catch (const TtbarDiffCrossSection_exception& l) {
    l.printMessage();
    cout << "Usage: " << arg[0] <<  " mainDir configLumi level nReplicas" << endl;
    cout << "Exiting now!" << endl;
    return -1;
  }
	
  cout << "Everything ok!" << endl;
  return 0;
}





