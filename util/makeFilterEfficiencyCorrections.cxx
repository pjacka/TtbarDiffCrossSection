#include "HelperFunctions/functions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib> 
#include <vector>
#include "TString.h" 
#include "TSystem.h"
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TStopwatch.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

std::vector<TString> readFileInLines(const TString& filename);
std::vector<std::unique_ptr<TFile>> makeListOfFiles(const std::vector<TString>& fileNames);
TChain* makeChain(const std::vector<std::unique_ptr<TFile>>& files, const TString& treeName);

class Minitree {
  public:
  
    Float_t filter_HT;
    Float_t weight_mc;
    std::vector<float>  *mc_generator_weights;
  
    Minitree(TChain* chain) : m_chain(chain) {init();}
    void init() {
      mc_generator_weights=0;
      
      m_chain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
      m_chain->SetBranchAddress("filter_HT_custom", &filter_HT, &b_filter_HT);
      m_chain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
    }
    
    int getEntry(Long64_t entry) {
      if (!m_chain) return 0;
      return m_chain->GetEntry(entry);
    }
    
    void getEntryFast(Long64_t entry) {
      if (!m_chain) return;
      int localEntry = m_chain->LoadTree(entry);
      b_filter_HT->GetEntry(localEntry);
      b_weight_mc->GetEntry(localEntry);
      b_mc_generator_weights->GetEntry(localEntry);
      
    }
    
    
  private:
    TChain* m_chain;
    
    TBranch        *b_filter_HT;   //!
    TBranch        *b_weight_mc;   //!
    TBranch        *b_mc_generator_weights;   //!
};

int makeFilterEfficiencyCorrections(const TString& mainDir, const TString& pathConfig, const TString& fileListDir, const TString& outputFileName, int debugLevel)
{
  
  TH1::AddDirectory(false);
  
  if (debugLevel>0) {
    cout << "mainDir: " << mainDir << endl;
    cout << "config: " << pathConfig << endl;
    cout << "fileListDir: " << fileListDir << endl;
    
  }
  
  auto config = std::make_unique<TEnv>(pathConfig);
  
  auto output = std::make_unique<TFile>(outputFileName,"RECREATE");
  const TString fileListName = "allhad.boosted.output.txt";
  const TString treeName = "truth";
  
  std::vector<TString> inclusiveSamples{"410471.PhPy8EG","410481.PhPy8EG"};
  std::vector<TString> midSlices{"410429","410431"};
  std::vector<TString> highSlices{"410444","410445"};
  
  for(size_t isample=0;isample<inclusiveSamples.size();isample++) {
  
    const TString sample = inclusiveSamples[isample];
   
  
   
    
    std::vector<TString> fileNames{};
    
    for (const TString prod : {"MC16a","MC16d","MC16e"}) {
      
      const TString fileList = fileListDir + "/" + prod + "." + sample + "/" + fileListName;
      if(debugLevel>5) cout << "Filelist: " << fileList << std::endl;
      std::vector<TString> v = readFileInLines(fileList);
      fileNames.insert(std::end(fileNames),std::begin(v),std::end(v));
    }
      
    std::vector<std::unique_ptr<TFile>> files = makeListOfFiles(fileNames);
    if(debugLevel>5) {
      for(auto& file : files) {
	cout << "file: " << file->GetName() << endl;
      }
    }
    
    auto chain = std::unique_ptr<TChain>(makeChain(files,treeName));
    auto minitree = std::make_unique<Minitree>(chain.get());
    
    
    long nentries = chain->GetEntries();
    if(debugLevel>1) {
      cout << "Number of entries in chain: " << nentries << endl;
    }
    
    long nprint = 100000;
    
    TStopwatch stopwatch;
    stopwatch.Start();
    
    const double filter_HT_cut_low = 1e6;
    const double filter_HT_cut_up = 1.5e6;
    
    std::vector<double> numeratorMiddle;
    std::vector<double> numeratorUp;
    std::vector<double> sumWeights;
    double numeratorISR_muR05_muF05_Var3cUp_Middle = 0.;
    double numeratorISR_muR05_muF05_Var3cUp_Up = 0.;
    double sumWeightsISR_muR05_muF05_Var3cUp = 0.;
    double numeratorISR_muR20_muF20_Var3cDown_Middle = 0.;
    double numeratorISR_muR20_muF20_Var3cDown_Up = 0.;
    double sumWeightsISR_muR20_muF20_Var3cDown = 0.;
    
    //ISR_muR05_muF05_Var3cUp:5:193;ISR_muR20_muF20_Var3cDown:6:194
    
    int size=0;
    
    for(long ientry=0;ientry<nentries;ientry++) {
      if(debugLevel > 0 && (ientry+1)%nprint == 0) cout << "Processing entry: " << ientry+1 << "/" << nentries << " [" << (ientry+1.)/nentries*100 << "%]" << endl;
      minitree->getEntryFast(ientry);
      //minitree->getEntry(ientry);
      
      if(ientry==0) {
	size=minitree->mc_generator_weights->size();
	numeratorMiddle.resize(size,0.);
	numeratorUp.resize(size,0.);
	sumWeights.resize(size,0.);
      }
      
      float filterHT = minitree->filter_HT;
      bool passedMiddle = (filterHT > filter_HT_cut_low && filterHT < filter_HT_cut_up);
      bool passedUp = filterHT > filter_HT_cut_up;
      
      for(int i=0;i<size;i++) {
	float weight = minitree->mc_generator_weights->at(i);
	
	sumWeights[i] += weight;
	if(passedMiddle) {
	  numeratorMiddle[i] += weight;
	}
	if(passedUp) {
	  numeratorUp[i] += weight;
	}
      }
      
      float weightISR_muR05_muF05_Var3cUp = minitree->mc_generator_weights->at(5)*minitree->mc_generator_weights->at(193);
      float weightISR_muR20_muF20_Var3cDown = minitree->mc_generator_weights->at(6)*minitree->mc_generator_weights->at(194);
      
      
      
      if(passedMiddle) numeratorISR_muR05_muF05_Var3cUp_Middle += weightISR_muR05_muF05_Var3cUp;
      if(passedUp) numeratorISR_muR05_muF05_Var3cUp_Up += weightISR_muR05_muF05_Var3cUp;
      sumWeightsISR_muR05_muF05_Var3cUp += weightISR_muR05_muF05_Var3cUp;
      if(passedMiddle) numeratorISR_muR20_muF20_Var3cDown_Middle += weightISR_muR20_muF20_Var3cDown;
      if(passedUp) numeratorISR_muR20_muF20_Var3cDown_Up += weightISR_muR20_muF20_Var3cDown;
      sumWeightsISR_muR20_muF20_Var3cDown += weightISR_muR20_muF20_Var3cDown;
    }
    cout <<"Real time in loop: " << stopwatch.RealTime() << endl;
    
    std::vector<double> effMiddle(size);
    std::vector<double> effUp(size);
    std::vector<double> correctionsMiddle(size);
    std::vector<double> correctionsUp(size);
    
    
    
    for(int i=0;i<size;i++) {
      effMiddle[i] = sumWeights[i] > 0 ? numeratorMiddle[i]/sumWeights[i] : 0.;
      effUp[i] = sumWeights[i] > 0 ? numeratorUp[i]/sumWeights[i] : 0.;
      
      correctionsMiddle[i] = effMiddle[i]/effMiddle[0];
      correctionsUp[i] = effUp[i]/effUp[0];
      
      //cout << "Filter efficiency for middle Ht slice: " << effMiddle[i] << " index: " << i << endl;
      //cout << "Filter efficiency for high Ht slice: " << effUp[i] << " index: " << i << endl;
      
    }
    
    //FSRbranches: FSR_muRfac_20:198;FSR_muRfac_05:199
    //ISRbranches: ISR_muR05_muF05_Var3cUp:5:193;ISR_muR20_muF20_Var3cDown:6:194
    
    double effISR_muR05_muF05_Var3cUp_Middle = numeratorISR_muR05_muF05_Var3cUp_Middle/sumWeightsISR_muR05_muF05_Var3cUp;
    double effISR_muR05_muF05_Var3cUp_Up = numeratorISR_muR05_muF05_Var3cUp_Up/sumWeightsISR_muR05_muF05_Var3cUp;
    
    double correctionsISR_muR05_muF05_Var3cUp_Middle = effISR_muR05_muF05_Var3cUp_Middle/effMiddle[0];
    double correctionsISR_muR05_muF05_Var3cUp_Up = effISR_muR05_muF05_Var3cUp_Up/effUp[0];
    
    double effISR_muR20_muF20_Var3cDown_Middle = numeratorISR_muR20_muF20_Var3cDown_Middle/sumWeightsISR_muR20_muF20_Var3cDown;
    double effISR_muR20_muF20_Var3cDown_Up = numeratorISR_muR20_muF20_Var3cDown_Up/sumWeightsISR_muR20_muF20_Var3cDown;
    
    double correctionsISR_muR20_muF20_Var3cDown_Middle = effISR_muR20_muF20_Var3cDown_Middle/effMiddle[0];
    double correctionsISR_muR20_muF20_Var3cDown_Up = effISR_muR20_muF20_Var3cDown_Up/effUp[0];
    
    int index = 0;
    
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] <<  " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    index = 198;
    
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] <<  " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    
    
    index = 199;
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] << " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    index = 5;
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] << " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    index = 6;
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] << " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    index = 193;
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] << " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    index = 194;
    cout << "index: " << index << " Filter efficiency for middle Ht slice: " << effMiddle[index] << " correction: " << correctionsMiddle[index] << endl;
    cout << "index: " << index << " Filter efficiency for high Ht slice: " << effUp[index] << " correction: " << correctionsUp[index] << endl;
    
    cout << "muR05_muF05_Var3cUp: " << " Filter efficiency for middle Ht slice: " << effISR_muR05_muF05_Var3cUp_Middle << " correction: " << correctionsISR_muR05_muF05_Var3cUp_Middle << endl;
    cout << "muR05_muF05_Var3cUp: " << " Filter efficiency for high Ht slice: " << effISR_muR05_muF05_Var3cUp_Up << " correction: " << correctionsISR_muR05_muF05_Var3cUp_Up << endl;
    
    cout << "muR20_muF20_Var3cDown: " << " Filter efficiency for middle Ht slice: " << effISR_muR20_muF20_Var3cDown_Middle << " correction: " << correctionsISR_muR20_muF20_Var3cDown_Middle << endl;
    cout << "muR20_muF20_Var3cDown: " << " Filter efficiency for high Ht slice: " << effISR_muR20_muF20_Var3cDown_Up << " correction: " << correctionsISR_muR20_muF20_Var3cDown_Up << endl;
    
    
    
    output->cd();
    
    auto hCorrectionsMiddle = std::make_unique<TH1D>("filterEfficiencyCorrections."+midSlices[isample],"",size+1,1,size+2);
    auto hCorrectionsUp = std::make_unique<TH1D>("filterEfficiencyCorrections."+highSlices[isample],"",size+1,1,size+2);
    
    for(int i=1;i<size;i++) {
      hCorrectionsMiddle->SetBinContent(i,correctionsMiddle[i]);
      hCorrectionsUp->SetBinContent(i,correctionsUp[i]);
      
    }
    hCorrectionsMiddle->SetBinContent(size,correctionsISR_muR05_muF05_Var3cUp_Middle);
    hCorrectionsUp->SetBinContent(size,correctionsISR_muR05_muF05_Var3cUp_Up);
    
    hCorrectionsMiddle->SetBinContent(size+1,correctionsISR_muR20_muF20_Var3cDown_Middle);
    hCorrectionsUp->SetBinContent(size+1,correctionsISR_muR20_muF20_Var3cDown_Up);
    
    
    
    hCorrectionsMiddle->Write();
    hCorrectionsUp->Write();

  }


  
  output->Close();
  
  cout << "All done!" << endl;
  return 0;
}

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to calculate signal modeling systematic uncertainties.  

Usage:  
  makeFilterEfficiencyCorrections [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --fileListDir <fileList>                Path to the filelist dir. [default: TtbarDiffCrossSection/filelists]
  --outputFile <outputFile>                Path to the filelist dir. [default: filterEfficiencies.root]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  
  TString mainDir("");
  TString pathConfig("");
  TString fileListDir("");
  TString outputFile("");
  int debugLevel(0);
  
  try {
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ pathConfig=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ fileListDir=args["--fileListDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--fileListDir option is expected to be a string. Check input parameters."); }
	
      try{ outputFile=args["--outputFile"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputFile option is expected to be a string. Check input parameters."); }
	
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    
    return makeFilterEfficiencyCorrections( mainDir, pathConfig, fileListDir, outputFile, debugLevel);
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  
  return 0;
}

std::vector<TString> readFileInLines(const TString& filename) {
  
  std::vector<TString> res;
  
  std::ifstream file(filename.Data());
  
  std::string line;
  while (std::getline(file, line)) {
    res.push_back(line);
  }
  
  return res;
}

std::vector<std::unique_ptr<TFile>> makeListOfFiles(const std::vector<TString>& fileNames) {
  const int size = fileNames.size();
  //const int size = 27;
  std::vector<std::unique_ptr<TFile>> res;
  const int nprint=1;
  for(int i=0;i<size;i++) {
    if( (i+1)%nprint == 0 ) cout << "Opening files: " << i+1 << "/" << size << " filename: " << fileNames[i] << endl;
    
    TFile* f = TFile::Open(fileNames[i]);
    if (!f || f->IsZombie()) {
      cout << "Error: Failed to open " << fileNames[i] << " File is skipped." << endl;
      if(f) delete f;
      continue;
    } 
    res.push_back(std::unique_ptr<TFile>(f));
  }
  return res;
}

TChain* makeChain(const std::vector<std::unique_ptr<TFile>>& files, const TString& treeName) {
 
  TChain* chain = new TChain(); 
  for (auto& file : files){
    chain->AddFile(file->GetName(), TChain::kBigNumber, treeName.Data());
  }
  return chain;
}
