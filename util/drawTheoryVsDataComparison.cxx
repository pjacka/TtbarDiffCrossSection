#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision                                                                                                            

#include "TString.h"
#include "TEnv.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TMatrixT.h"
#include "TGraphAsymmErrors.h"
#include "TPaveText.h"
#include "TROOT.h"

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/latexTablesFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"
#include "HelperFunctions/Predictions.h"

#include "docopt.h"

using namespace std;

struct PredictionStyle {
  TString folderName{"nominal"};
  TString histName{"signal_truth"};
  TString fileName{""};
  int color{1};
  int markerStyle{20};
  double markerSize{1.2};
  double lineWidth{3};
  int lineStyle{1};
};



int getExponentFactor(int nprojections,const TString& variable);
double getLinearFactor(const TString& variable);

void write1DObjects(const TH1D* data, const vector<TH1D*>& predictions, const TGraphAsymmErrors* graphStatUnc,
		    const TGraphAsymmErrors* graphStatSysUnc, bool isNormalized);

void draw1DComparisonPlots(const TString& figureName, const TString& variable, const TString& level,
			   Predictions& predictions, Predictions& ratios, bool isNormalized, bool useLogScale);

	
void writeNDObjects(const TH1D* data, const vector<TH1D*>& predictions, const TGraphAsymmErrors* graphStatUnc,
		    const TGraphAsymmErrors* graphStatSysUnc, bool isNormalized);
		    
void drawNDComparisonPlots(const TString& figureName, const TString& variable, const TString& level,
			   vector<Predictions>& predictions, vector<Predictions>& ratios, bool isNormalized, bool useLogScale=true);

void drawSinglePadNDComparison(const TString& figureName, const TString& variable, const TString& level,
			       TH1D* data, TH1D* prediction, bool isNormalized, bool useLogScale=true);

std::vector<TGraphAsymmErrors*> loadNDNNLOPrediction(TFile* f,const TString& variable);

std::vector<TString> makeGeneralInfo(const TString& level,bool singlePad=false);
TString makeDiffVariable(const TString& variable,const bool isNormalized);


void createGrafWithSysErrors(TGraphAsymmErrors* graph_unc,TMatrixT<double>& cov);
void createHistWithSysErrors(TH1D* hist_unc,TMatrixT<double>& cov);
TGraphAsymmErrors* makeGraphWithErrors(TFile* f, TH1D* h, const TString& covarianceName);

void setupHistStyle(TH1* hist,const PredictionStyle& predStyle);
int saveObj(TString outDir, TString level, TString observable, TString sampleName,
	     TObject* obj, TString objName, bool recreateFile);

double g_lumi;
TString g_lumi_string;
TString g_pathToFigures,g_dirname,g_rootDir,g_mc_samples_production;

TPad* makePad(const TString& name,const double xmin,const double ymin, const double xmax,const double ymax,const double leftMargin,const double topMargin, const double rightMargin, const double bottomMargin);
TLegend* makeLegend(const double x1, const double y1, const double x2, const double y2);


int debug = 0;
const float  fb = 1000;

vector<PredictionStyle> g_predictionStyles;
PredictionStyle g_dataStyle;
PredictionStyle g_statStyle;
PredictionStyle g_statSysStyle;
std::unique_ptr<MultiDimensionalPlotsSettings> g_plotSettings;
std::unique_ptr<HistogramNDto1DConverter> g_histogramConverterND;


int drawTheoryVsDataComparison(const TString& path, const TString& pathTTbarConfig, const TString& pathConfigBinning, const TString& pathMultiDimensionalPlotsConfig,
				    const TString& outputSubDir, const TString& variable, const TString& level,
				    const TString& systematicHistosPath, const TString& dimension, const TString& covariancesDir,
				    const TString& covAllUncName, const TString& covStatName,
				    bool drawNNLOPredictions, const TString& NNLOPredictionsDir, const vector<TString>& NNLOPredictionsScales);

static const char * USAGE =
    R"(Program to draw theory vs data comparison plots and to make chi2 tests. 

Usage:  
  drawTheoryVsDataComparison [options]
  
Options:
  -h --help                         Show help screen.
  --mainDir <mainDir>               Path to main output directory. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	         Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --configBinning <configBinning>                Path to config file with binning. [default: TtbarDiffCrossSection/data/optBinningND/ttbar_y_vs_ttbar_mass_vs_t1_pt.env]
  --multiDimensionalPlotsConfig <string>         Path to config file with plots settings. [default: TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env]
  --outputSubDir <outputSubDir>	    Output subdirectory to save plots. [default: Theory_vs_data_comparison]
  --variable <string>               Variable name. [default: total_cross_section]
  --level <string>                  ParticleLevel or PartonLevel. [default: ParticleLevel]
  --systematicsHistosDir <systematicsHistosDir>   Directory with systematic histos rootfiles. [default: systematics_histos]
  --dimension <dimension>           Dimension of variables of interest. [default: 1D]"
  --covariancesDir <covariancesDir>              Directory with rootfiles containing covariances. [default: covariances]
  --covAllUncName <covAllUncName>   Full covariance matrix name. [default: covDataBasedAllUnc]
  --covStatName <covStatName>       Data stat. covariance matrix name. [default: covDataBasedOnlyDataStat]
  --debugLevel <int>                Option to control debug messages. Higher value means more messages. [default: 0]
  --drawNNLOPredictions <int>       NNLO predictions will be drawn. [default: 0]
  --NNLOPredictionsDir <Path>       Directory containing NNLO predictions. [default: TtbarDiffCrossSection/data/MATRIX]
  --NNLOPredictionsScales <Path>    Directory containing NNLO predictions. [default: NLO_HT2,NNLO_HT2,NNLO_HT4]
)";


int main(int nArg, char **arg) {
  
  TString path      = "";
  TString pathTTbarConfig="";
  TString pathConfigBinning="";
  TString pathMultiDimensionalPlotsConfig="";
  TString outputSubDir = "";
  TString variable = "";
  TString level = "";
  TString systematicHistosPath="";
  TString dimension = "";
  TString covariancesDir="";
  TString covAllUncName = "";
  TString covStatName = "";
  bool drawNNLOPredictions = "";
  TString NNLOPredictionsDir = "";
  std::vector<TString> NNLOPredictionsScales;
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true);  // Using docopt package to read parameters
  try{
    try{ path=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
    
    try{ pathTTbarConfig=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      
    try{ pathConfigBinning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
      
    try{ pathMultiDimensionalPlotsConfig=args["--multiDimensionalPlotsConfig"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--multiDimensionalPlotsConfig option is expected to be a string. Check input parameters."); }
      
    try{ outputSubDir=args["--outputSubDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputSubDir option is expected to be a string. Check input parameters."); }

    try{ variable=args["--variable"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }

    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }

    try{ systematicHistosPath=args["--systematicsHistosDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--systematicsHistosDir option is expected to be a string. Check input parameters."); }
    
    try{ dimension=args["--dimension"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--dimension option is expected to be a string. Check input parameters."); }

    try{ covariancesDir=args["--covariancesDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covariancesDir option is expected to be a string. Check input parameters."); }
    
    try{ covAllUncName=args["--covAllUncName"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covAllUncName option is expected to be a string. Check input parameters."); }
     
    try{ covStatName=args["--covStatName"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--covStatName option is expected to be a string. Check input parameters."); }
     
    try{ debug = args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is not an integer. Check input parameters."); }
  
    try{ drawNNLOPredictions = args["--drawNNLOPredictions"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--drawNNLOPredictions option is not an integer. Check input parameters."); }
  
    try{ NNLOPredictionsDir = args["--NNLOPredictionsDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--NNLOPredictionsDir option is not a string. Check input parameters."); }
  
    try{ 
      std::string s = args["--NNLOPredictionsScales"].asString();
      std::vector<std::string> vec = functions::splitString(s,",");
      for(std::string& scale : vec) {
	functions::trim(scale);
	NNLOPredictionsScales.push_back(scale.c_str());
      }
      
    } catch(const std::invalid_argument& e) { 
      throw std::invalid_argument("--NNLOPredictionsDir option is not a string. Check input parameters.");
    }
  
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }  

  path.ReplaceAll("$TtbarDiffCrossSection_output_path",gSystem->Getenv("TtbarDiffCrossSection_output_path"));

  return drawTheoryVsDataComparison(path, pathTTbarConfig, pathConfigBinning, pathMultiDimensionalPlotsConfig, outputSubDir,
				    variable, level, systematicHistosPath, dimension, covariancesDir,covAllUncName,covStatName,
				    drawNNLOPredictions,NNLOPredictionsDir,NNLOPredictionsScales);
}


int drawTheoryVsDataComparison(const TString& path, const TString& pathTTbarConfig, const TString& pathConfigBinning, const TString& pathMultiDimensionalPlotsConfig,
				    const TString& outputSubDir, const TString& variable, const TString& level,
				    const TString& systematicHistosPath, const TString& dimension, const TString& covariancesDir,
				    const TString& covAllUncName, const TString& covStatName,
				    bool drawNNLOPredictions, const TString& NNLOPredictionsDir, const vector<TString>& NNLOPredictionsScales){

  gROOT->SetStyle("ATLAS");
  gStyle->SetErrorX();
  TH1::AddDirectory(false);
  
  auto ttbarConfig= std::make_unique<NtuplesAnalysisToolsConfig>(pathTTbarConfig);
  
  TString levelShort = level;
  levelShort.ReplaceAll("Level","");
  
  if(dimension=="ND") {
    g_plotSettings = std::make_unique<MultiDimensionalPlotsSettings>(pathMultiDimensionalPlotsConfig.Data());
    
    g_histogramConverterND = std::make_unique<HistogramNDto1DConverter>(pathConfigBinning.Data());
    g_histogramConverterND->initializeOptBinning((variable+"_"+levelShort+"_reco").Data());
    g_histogramConverterND->initialize1DBinning();
    
  }

  g_mc_samples_production = ttbarConfig->mc_samples_production();
  
  g_lumi = configFunctions::getLumi(*ttbarConfig); // lumi in fb^-1
  if(g_lumi < 0.) {
    cout << "Unknown mc samples production: " << g_mc_samples_production << endl;
    exit(-1);
  }
  g_lumi_string = configFunctions::getLumiString(*ttbarConfig);
  
  bool useSignalModeling=ttbarConfig->doSignalModeling();
    
  g_rootDir="root."+g_mc_samples_production;

  g_pathToFigures = path;
  g_dirname = outputSubDir;
  const TString texDirFull = g_pathToFigures + "/tex."+g_mc_samples_production+"/" + g_dirname;
  const TString rootDirFull = g_pathToFigures + "/"+ g_rootDir + "/" + g_dirname;

  gSystem->mkdir(g_pathToFigures + "/pdf."+g_mc_samples_production+"/" + g_dirname,true);
  gSystem->mkdir(g_pathToFigures + "/png."+g_mc_samples_production+"/" + g_dirname,true);
  gSystem->mkdir(texDirFull,true);
  gSystem->mkdir(g_pathToFigures + "/"+g_rootDir+"/" + g_dirname,true);
  
  const TString fileNameBase=rootDirFull+"/"+levelShort+"."+variable+".";
  auto getFileName = [&](const TString& shortName) {
    TString fileName = fileNameBase + shortName + ".root";
    fileName.ReplaceAll("\\","");
    fileName.ReplaceAll("{" ,"");
    fileName.ReplaceAll("}" ,"");
    fileName.ReplaceAll(" " ,"_");
    return fileName;
  };
  
  std::map<TString,std::vector<TString>> legendNames;
  
  legendNames["data"].push_back("Data");
  g_dataStyle.fileName=getFileName(legendNames["data"].back());
  g_dataStyle.color=1;
  g_dataStyle.markerStyle=20;
  g_dataStyle.markerSize=1.2;
  g_dataStyle.lineWidth=0;
  g_dataStyle.lineStyle=1;
  
  
  legendNames["unc"].push_back("Stat. #oplus Sys. Unc.");
  g_statStyle.fileName=getFileName(legendNames["unc"].back());
  
  legendNames["unc"].push_back("Stat. Unc.");
  g_statSysStyle.fileName=getFileName(legendNames["unc"].back());
  
  legendNames["histos"].push_back("PWG+Py8");
  PredictionStyle predStyle;
  predStyle.folderName="nominal";
  predStyle.fileName=getFileName(legendNames["histos"].back());
  predStyle.color=2;
  predStyle.markerStyle=0;
  predStyle.markerSize=0;
  predStyle.lineWidth=3;
  predStyle.lineStyle=1;
  
  g_predictionStyles.push_back(predStyle);
  
  if(useSignalModeling){
    legendNames["histos"].push_back("MG5_aMC@NLO+Py8");
    predStyle.folderName=ttbarConfig->hardScatteringDirectory();
    predStyle.fileName=getFileName(legendNames["histos"].back());
    predStyle.color = 600;
    predStyle.lineStyle = 2;
    g_predictionStyles.push_back(predStyle);

    
    legendNames["histos"].push_back("PWG+H7.1.3");
    predStyle.folderName=ttbarConfig->partonShoweringDirectory();
    predStyle.fileName=getFileName(legendNames["histos"].back());
    predStyle.color = 418;
    predStyle.lineStyle = 5;
    g_predictionStyles.push_back(predStyle);
    
    legendNames["histos"].push_back("PWG+Py8 (more IFSR)");
    predStyle.folderName=ttbarConfig->ISRupDirectory();
    predStyle.fileName=getFileName(legendNames["histos"].back());
    predStyle.color = kPink + 7;
    predStyle.lineStyle = 7;
    g_predictionStyles.push_back(predStyle);
    
    legendNames["histos"].push_back("PWG+Py8 (less IFSR)");
    predStyle.folderName=ttbarConfig->ISRdownDirectory();
    predStyle.fileName=getFileName(legendNames["histos"].back());
    predStyle.color = kPink - 7;
    predStyle.lineStyle = 4;
    g_predictionStyles.push_back(predStyle);
  }
  
  std::map<TString,std::vector<TString>> legendNamesRatios = legendNames;
  legendNamesRatios.erase("data");
  
  
  const int npredictions=g_predictionStyles.size();  
  
  double inverseBR=1.0;
  if(levelShort=="Parton") inverseBR = ttbarConfig->factor_to_ttbar_cross_section(); // Scale factor from all-hadronic to total cross-section
  
  if(debug>0){
    cout << "using SFToTtbarCrossSection: " << inverseBR << endl;
    cout << "using luminosity: " << g_lumi_string << endl;
  }
    
  TString label="";
  TString unit="";

    
  TString predictionsPath=path+ "/"+g_rootDir+"/"+systematicHistosPath;
  TFile* fPredictions= new TFile(predictionsPath +"/" + variable + "_" + level + ".root" );
  if(fPredictions->IsZombie()){
    cout << "Error: file with systematics histos is a zombie!" << endl;
    cout << "Attempting to load file: " << predictionsPath +"/" + variable + "_" + level + ".root" << endl;
    exit(-1);
  }
  
  TString rootfilename=variable + "_" + level + ".root";
  
  TString name=path + "/"+g_rootDir+"/"+covariancesDir+"/";
  
  TFile *fCovariances = new TFile(name + rootfilename);
  if(fCovariances->IsZombie()){
    cout << "Error: File with covariances is zombie" << endl;
    exit(-1);
  }
  
  TH1D* data_hist=(TH1D*)fCovariances->Get(variable+"_centralValues");
  TH1D* data_hist_normalized=(TH1D*)fCovariances->Get(variable+"_centralValuesNormalized");
  data_hist_normalized->SetName((TString)data_hist->GetName() + "_normalized");
  
  data_hist->Scale(inverseBR);
  
  
  functions::SetErrorsToZero(data_hist);
  functions::SetErrorsToZero(data_hist_normalized);
  
  setupHistStyle(data_hist,g_dataStyle);
  setupHistStyle(data_hist_normalized,g_dataStyle);
  
  std::unique_ptr<TGraphAsymmErrors> graph_unc {makeGraphWithErrors(fCovariances, data_hist, variable+"_"+covAllUncName)};
  std::unique_ptr<TGraphAsymmErrors> graph_unc_stat {makeGraphWithErrors(fCovariances, data_hist, variable+"_"+covStatName)};
  std::unique_ptr<TGraphAsymmErrors> graph_unc_normalized {makeGraphWithErrors(fCovariances, data_hist_normalized, variable+"_"+covAllUncName+"Normalized")};
  std::unique_ptr<TGraphAsymmErrors> graph_unc_stat_normalized {makeGraphWithErrors(fCovariances, data_hist_normalized, variable+"_"+covStatName+"Normalized")};
  
  functions::scaleGraphErrors(graph_unc.get(),inverseBR);
  functions::scaleGraphErrors(graph_unc_stat.get(),inverseBR);
  
  if(debug>0)cout << "Graphs with uncertainties obtained" << endl;
  
  graph_unc->SetFillStyle(1001);
  graph_unc->SetFillColor(kGray+1);
  graph_unc->SetLineColor(kGray+1);
  
  graph_unc_stat->SetFillStyle(1001);
  graph_unc_stat->SetFillColor(kGray);
  graph_unc_stat->SetLineColor(kGray);
  
  graph_unc_normalized->SetFillStyle(1001);
  graph_unc_normalized->SetFillColor(kGray+1);
  graph_unc_normalized->SetLineColor(kGray+1);
  
  graph_unc_stat_normalized->SetFillStyle(1001);
  graph_unc_stat_normalized->SetFillColor(kGray);
  graph_unc_stat_normalized->SetLineColor(kGray);
  
  std::unique_ptr<TGraphAsymmErrors> unit_graph_unc{functions::getUnitGraph(graph_unc.get())};
  std::unique_ptr<TGraphAsymmErrors> unit_graph_unc_stat{functions::getUnitGraph(graph_unc_stat.get())};
  std::unique_ptr<TGraphAsymmErrors> unit_graph_unc_normalized{functions::getUnitGraph(graph_unc_normalized.get())};
  std::unique_ptr<TGraphAsymmErrors> unit_graph_unc_stat_normalized{functions::getUnitGraph(graph_unc_stat_normalized.get())};

  
  
  vector<TH1D*> hist_predictions,hist_predictions_normalized;
  for (int ipred=0;ipred<npredictions;++ipred) {

    if(debug>0) cout << "getting prediction: " << g_predictionStyles[ipred].fileName << endl;
    if(debug>0) cout << g_predictionStyles[ipred].folderName+"/" + variable + "_signal_truth" << endl;
    
    TH1D* prediction= (TH1D*)fPredictions->Get(g_predictionStyles[ipred].folderName+"/" + variable + "_" + g_predictionStyles[ipred].histName);
    TH1D* prediction_normalized=(TH1D*)prediction->Clone();
    prediction_normalized->Scale(1./prediction_normalized->Integral());
    functions::DivideByBinWidth(prediction_normalized);
    
    prediction->Scale(inverseBR);   // scaling from all-hadronic channel to all-decays channel

    functions::ConvertToCrossSection(prediction,g_lumi,label,unit);
    NamesAndTitles::setLabelForNormalized(prediction_normalized, label, unit);
    
    NamesAndTitles::setYtitle(prediction,variable,unit,"absolute");
    NamesAndTitles::setYtitle(prediction_normalized,variable,unit,"relative");
    
    if(debug>0) cout << ipred + 1 << " " << npredictions << endl;
    hist_predictions.push_back(prediction);
    hist_predictions_normalized.push_back(prediction_normalized);
			    
    setupHistStyle(hist_predictions[ipred],g_predictionStyles[ipred]);
    setupHistStyle(hist_predictions_normalized[ipred],g_predictionStyles[ipred]);
    
  }
  
  
  
  
  Predictions predictionsAbsolute;
  Predictions predictionsNormalized;
  
  predictionsAbsolute.setData(data_hist);
  for(TH1D* h : hist_predictions) {
    predictionsAbsolute.addHistogram(h);
  }
  predictionsAbsolute.addUnc(graph_unc.get());
  predictionsAbsolute.addUnc(graph_unc_stat.get());
  
  Predictions ratiosAbsolute = predictionsAbsolute.makeRatios(data_hist);
  
  
  predictionsNormalized.setData(data_hist_normalized);
  for(TH1D* h : hist_predictions_normalized) {
    predictionsNormalized.addHistogram(h);
  }
  predictionsNormalized.addUnc(graph_unc_normalized.get());
  predictionsNormalized.addUnc(graph_unc_stat_normalized.get());
  Predictions ratiosNormalized = predictionsNormalized.makeRatios(data_hist_normalized);
  
  predictionsAbsolute.setLegendNames(legendNames);
  ratiosAbsolute.setLegendNames(legendNamesRatios);
  
  predictionsNormalized.setLegendNames(legendNames);
  ratiosNormalized.setLegendNames(legendNamesRatios);
  
  
  
  TString figureName=level+"_"+variable;
  
  // Making latex table strings
  const TString tableNameAbsolute = texDirFull + "/" + level + "_" + variable + "_absolute" + ".tex";
  const TString tableNameRelative = texDirFull + "/" + level + "_" + variable + "_relative" + ".tex";

  if(dimension=="1D") {
    
    
    
    bool isNormalized = false;
    write1DObjects(data_hist, hist_predictions, graph_unc_stat.get(), graph_unc.get(),isNormalized);
    draw1DComparisonPlots(figureName+"_log", variable, levelShort, predictionsAbsolute, ratiosAbsolute, isNormalized, true);
    draw1DComparisonPlots(figureName, variable, levelShort, predictionsAbsolute, ratiosAbsolute, isNormalized, false);
        
    if(variable!="total_cross_section") {
      isNormalized = true;
      write1DObjects(data_hist_normalized, hist_predictions_normalized, graph_unc_stat_normalized.get(), graph_unc_normalized.get(),isNormalized);
      draw1DComparisonPlots(figureName+"_normalized_log", variable, levelShort, predictionsNormalized, ratiosNormalized, isNormalized, true);
      draw1DComparisonPlots(figureName+"_normalized", variable, levelShort, predictionsNormalized, ratiosNormalized, isNormalized, false);
    }
    
    // Printing latex tables
    latexTablesFunctions::printTableWithDiffCrossSection( tableNameAbsolute , variable, data_hist, graph_unc_stat.get(), graph_unc.get(), "absolute");
    if(variable!="total_cross_section") {
      latexTablesFunctions::printTableWithDiffCrossSection( tableNameRelative , variable, data_hist_normalized, graph_unc_stat_normalized.get(), graph_unc_normalized.get(), "relative");
    }
    
    
  }
  
  if(dimension=="ND") {
    
    const TString fileNameData = getFileName(g_dataStyle.fileName);
    
    std::vector<Predictions> predictionsAbsoluteProjections = predictionsAbsolute.makeProjections(g_histogramConverterND.get());
    std::vector<Predictions> ratiosAbsoluteProjections = ratiosAbsolute.makeProjections(g_histogramConverterND.get());
    
    std::vector<Predictions> predictionsNormalizedProjections = predictionsNormalized.makeProjections(g_histogramConverterND.get());
    std::vector<Predictions> ratiosNormalizedProjections = ratiosNormalized.makeProjections(g_histogramConverterND.get());
    
    predictionsAbsoluteProjections[0].setLegendNames(legendNames);
    ratiosAbsoluteProjections[0].setLegendNames(legendNamesRatios);
    predictionsNormalizedProjections[0].setLegendNames(legendNames);
    ratiosNormalizedProjections[0].setLegendNames(legendNamesRatios);
    
    
    
    bool isNormalized = false;
    bool useLogScale = true;
    
    std::vector<TString> linearScaleVariables {
      "t1_y_vs_t2_y",
      "ttbar_y_vs_top_y",
      "ttbar_y_vs_t1_y",
      "ttbar_y_vs_t2_y",
      "delta_y_vs_t1_y"
    };
    
    if(std::find(std::begin(linearScaleVariables),std::end(linearScaleVariables),variable)!=std::end(linearScaleVariables)) useLogScale = false;
    writeNDObjects(data_hist, hist_predictions, graph_unc_stat.get(), graph_unc.get(),isNormalized);
    
    drawNDComparisonPlots(figureName, variable, levelShort, predictionsAbsoluteProjections,ratiosAbsoluteProjections, isNormalized, useLogScale);
    drawSinglePadNDComparison(figureName+"_singlePad", variable, levelShort, data_hist, hist_predictions[0],isNormalized,useLogScale);
    isNormalized = true;
    writeNDObjects(data_hist_normalized, hist_predictions_normalized, graph_unc_stat_normalized.get(), graph_unc_normalized.get(),isNormalized);
    drawNDComparisonPlots(figureName+"_normalized", variable, levelShort, predictionsNormalizedProjections,ratiosNormalizedProjections, isNormalized, useLogScale);
    drawSinglePadNDComparison(figureName+"_normalized"+"_singlePad", variable, levelShort, data_hist_normalized, hist_predictions_normalized[0],isNormalized,useLogScale);
    
    
    // Printing latex tables
    latexTablesFunctions::printTableWithDiffCrossSectionND( tableNameAbsolute , variable, data_hist, graph_unc_stat.get(), graph_unc.get(), g_histogramConverterND.get(), "absolute");
    
    if(variable!="total_cross_section") {
      latexTablesFunctions::printTableWithDiffCrossSectionND( tableNameRelative , variable, data_hist_normalized, graph_unc_stat_normalized.get(), graph_unc_normalized.get(), g_histogramConverterND.get(), "relative");
    }
    
    
  }
    
  if(drawNNLOPredictions) {
    
    
    
    const TString filenamePrefix="hists_MATRIX_";
    const size_t nNNLOPredictions = NNLOPredictionsScales.size();
    
    std::vector<std::unique_ptr<TFile>> fNNLOPredictions;
    for(const TString& scale : NNLOPredictionsScales) {
      const TString filename = NNLOPredictionsDir + "/" + scale + "/" + filenamePrefix + scale + ".root";
      fNNLOPredictions.push_back(std::make_unique<TFile>(filename));
      if(fNNLOPredictions.back()->IsZombie()) {
	std::cout << "Failed to open file with NNLO predictions: " << filename << std::endl;
      }
    }
    
    vector<TString> legendNamesNNLO;
    for(TString scale : NNLOPredictionsScales) {
      scale.ReplaceAll("HT","#mu=H_{T}/");
      scale.ReplaceAll("MT","#mu=m_{T}/");
      functions::replaceFirst(scale,"_",", ");
      scale.ReplaceAll("_NNPDF31","");
      legendNamesNNLO.push_back("MATRIX " + scale);
    }
    
    legendNames["histos"] = {legendNames["histos"][0]};
    legendNames["graphs"] = legendNamesNNLO;
    
    legendNamesRatios=legendNames;
    legendNamesRatios.erase("data");
    
    Predictions NNLOPredictions;
    NNLOPredictions.setData(data_hist);
    NNLOPredictions.addHistogram(hist_predictions[0]);
    const double kFactorPWGPy8 = 1.1398;
    NNLOPredictions.getHistos()[0]->Scale(1./kFactorPWGPy8);
    NNLOPredictions.addUnc(graph_unc.get());
    NNLOPredictions.addUnc(graph_unc_stat.get());
    
    Predictions NNLOPredictionsNormalized;
    NNLOPredictionsNormalized.setData(data_hist_normalized);
    NNLOPredictionsNormalized.addHistogram(hist_predictions_normalized[0]);
    NNLOPredictionsNormalized.addUnc(graph_unc_normalized.get());
    NNLOPredictionsNormalized.addUnc(graph_unc_stat_normalized.get());
    
    gStyle->SetEndErrorSize(2.);

    if(dimension=="1D") {
      std::vector<int> colors{kGreen+2, kBlue, kOrange};
      std::vector<double> alphas{0.4,0.4,0.4};
      std::vector<int> fillStyles{3154,3145,3144};
      
      TString var = variable;
      var.ReplaceAll("t1_","leadingTop_");
      var.ReplaceAll("t2_","subleadingTop_");
      
      
      TString NNLOPredictionName=var+"_total";
      TString NNLOPredictionNameNormalized= "norm_"+NNLOPredictionName;
      
      std::cout << "NNLOPredictionName: " << NNLOPredictionName << std::endl;
      std::cout << "NNLOPredictionName normalized: " << NNLOPredictionNameNormalized << std::endl;
      
      std::vector<TString> vecChangeUnits = {
	"ttbar_mass",
	"ttbar_pt",
	"t1_pt",
	"t2_pt",
	"H_tt_pt"
      };
      
      for(size_t iPred=0;iPred<fNNLOPredictions.size();iPred++) {
	TGraphAsymmErrors* NNLOPrediction = (TGraphAsymmErrors*)fNNLOPredictions[iPred]->Get(NNLOPredictionName);
	TGraphAsymmErrors* NNLOPredictionNormalized = (TGraphAsymmErrors*)fNNLOPredictions[iPred]->Get(NNLOPredictionNameNormalized);
  
	std::cout << NNLOPredictionNormalized << std::endl;
  
	if(std::find(vecChangeUnits.begin(),vecChangeUnits.end(),variable) != vecChangeUnits.end()) {
	  functions::changeUnits(NNLOPrediction,0.001); // From GeV to TeV
	  functions::changeUnits(NNLOPredictionNormalized,0.001); // From GeV to TeV
	}
  
	const int color = iPred < colors.size() ? colors[iPred] : iPred + 3;
	const double alpha = iPred < alphas.size() ? alphas[iPred] : 0.5;
	NNLOPrediction->SetLineColor(color);
	NNLOPrediction->SetLineWidth(1);
	NNLOPrediction->SetFillColorAlpha(color,alpha);
	NNLOPrediction->SetFillStyle(fillStyles[iPred]);
	NNLOPredictions.addGraph(NNLOPrediction,false);
	
	NNLOPredictionNormalized->SetLineColor(color);
	NNLOPredictionNormalized->SetLineWidth(1);
	NNLOPredictionNormalized->SetFillColorAlpha(color,alpha);
	NNLOPredictionNormalized->SetFillStyle(fillStyles[iPred]);
	NNLOPredictionsNormalized.addGraph(NNLOPredictionNormalized,false);
      }
      
      Predictions NNLOPredictionsRatios = NNLOPredictions.makeRatios(data_hist);
      Predictions NNLOPredictionsNormalizedRatios = NNLOPredictionsNormalized.makeRatios(data_hist_normalized);
      
      NNLOPredictions.setLegendNames(legendNames);
      NNLOPredictionsRatios.setLegendNames(legendNamesRatios);
      NNLOPredictionsNormalized.setLegendNames(legendNames);
      NNLOPredictionsNormalizedRatios.setLegendNames(legendNamesRatios);
      
      TString figureName = level+"_"+variable + "_NNLO";
      bool isNormalized=false;
      draw1DComparisonPlots(figureName+"_log", variable, levelShort, NNLOPredictions, NNLOPredictionsRatios, isNormalized, true);
      draw1DComparisonPlots(figureName, variable, levelShort, NNLOPredictions, NNLOPredictionsRatios, isNormalized, false);
      
      figureName = level+"_"+variable + "_normalized_NNLO";
      isNormalized=true;
      draw1DComparisonPlots(figureName+"_log", variable, levelShort, NNLOPredictionsNormalized, NNLOPredictionsNormalizedRatios, isNormalized, true);
      draw1DComparisonPlots(figureName, variable, levelShort, NNLOPredictionsNormalized, NNLOPredictionsNormalizedRatios, isNormalized, false);
    }
    
    if(dimension=="ND") {
      std::vector<int> colors{kGreen+2,kBlue};
      std::vector<double> alphas{0.35,0.35};
      std::vector<int> fillStyles{3345,3344};
      
      std::vector<std::vector<TGraphAsymmErrors*>> NNLOPred;
      TString var = variable;
      var.ReplaceAll("_vs_","-");
      var.ReplaceAll("t1_","leadingTop_");
      var.ReplaceAll("t2_","subleadingTop_");
      var="split2D-" + var;
      
      for(const std::unique_ptr<TFile>& f : fNNLOPredictions) {
	NNLOPred.push_back(loadNDNNLOPrediction(f.get(),var));
      }
      
      
      
      std::vector<TString> vecChangeInternalUnits = {
	"t1_pt_vs_t2_pt",
	"t1_pt_vs_delta_pt",
	"t1_pt_vs_ttbar_pt",
	"t1_pt_vs_ttbar_mass",
	"t2_pt_vs_ttbar_mass",
	"t1_y_vs_t1_pt",
	"t2_y_vs_t2_pt",
	"ttbar_y_vs_t1_pt",
	"ttbar_y_vs_t2_pt",
	"ttbar_y_vs_ttbar_mass",
	"ttbar_y_vs_ttbar_mass_vs_t1_pt",
	"t1_y_vs_ttbar_mass",
	"ttbar_y_vs_ttbar_pt",
	"ttbar_pt_vs_ttbar_mass"
      };
      
      std::map<TString,double> mapChangeExternalUnits = {
	{"t1_pt_vs_t2_pt",1e3},
	{"t1_pt_vs_delta_pt",1e3},
	{"t1_pt_vs_ttbar_pt",1e3},
	{"t1_pt_vs_ttbar_mass",1e3},
	{"t2_pt_vs_ttbar_mass",1e3},
	{"ttbar_pt_vs_ttbar_mass",1e3},
      };
      auto it = mapChangeExternalUnits.find(variable);
      
      
      for(size_t iPred=0;iPred<nNNLOPredictions;iPred++) {
	
	for(TGraphAsymmErrors* NNLOPrediction : NNLOPred[iPred]) {
	  
	  if(std::find(vecChangeInternalUnits.begin(),vecChangeInternalUnits.end(),variable) != vecChangeInternalUnits.end()) {
	    functions::changeUnits(NNLOPrediction,0.001); // From GeV to TeV
	  }
	  if(it != mapChangeExternalUnits.end()) {
	    functions::scaleGraphY(NNLOPrediction,it->second); // From GeV to TeV
	  }
    
	  const int color = iPred < colors.size() ? colors[iPred] : iPred + 3;
	  const double alpha = iPred < alphas.size() ? alphas[iPred] : 0.5;
	  NNLOPrediction->SetLineColor(color);
	  NNLOPrediction->SetLineWidth(1);
	  NNLOPrediction->SetFillStyle(fillStyles[iPred]);
	  NNLOPrediction->SetFillColorAlpha(color,alpha);
	}
      }
      
      std::vector<Predictions> NNLOPredictionsProjections = NNLOPredictions.makeProjections(g_histogramConverterND.get());
      for(auto& pred : NNLOPred) {
	if(NNLOPredictionsProjections.size()!=pred.size()) {
	  throw std::runtime_error("Error: Different number of projections in NNLO predictions and measured distributions! Check binning!");
	}
	
	for(size_t iProj=0;iProj<pred.size();iProj++) {
	  NNLOPredictionsProjections[iProj].addGraph(pred[iProj]);
	}
      }
      
      std::vector<Predictions> NNLORatiosProjections;
      for(auto& proj : NNLOPredictionsProjections) NNLORatiosProjections.push_back(proj.makeRatios(proj.getData()));
      
      NNLOPredictionsProjections.at(0).setLegendNames(legendNames);
      NNLORatiosProjections.at(0).setLegendNames(legendNamesRatios);
      
      
      TString figureName = level+"_"+variable + "_NNLO";
      bool isNormalized=false;
      drawNDComparisonPlots(figureName, variable, levelShort, NNLOPredictionsProjections,NNLORatiosProjections, isNormalized, true);
    }
    
  }
  
  
  
    
  return 0;
}

//----------------------------------------------------------------------

TPad* makePad(const TString& name,const double xmin,const double ymin, const double xmax,const double ymax,const double leftMargin,const double topMargin, const double rightMargin, const double bottomMargin) {
  TPad* pad = new TPad(name,name,xmin,ymin,xmax,ymax,0,0,0);
  pad->SetBottomMargin(bottomMargin);
  pad->SetLeftMargin(leftMargin);
  pad->SetTopMargin(topMargin);
  pad->SetRightMargin(rightMargin);
  pad->SetTicks();
  return pad;
}

//----------------------------------------------------------------------

TLegend* makeLegend(const double x1, const double y1, const double x2, const double y2) {
  TLegend* leg = new TLegend(x1,y1,x2,y2);
  leg->SetNColumns(1);
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->SetTextFont(42);
  leg->SetTextSize(0.055);
  return leg;
}


//----------------------------------------------------------------------

void write1DObjects(const TH1D* data, const vector<TH1D*>& predictions, const TGraphAsymmErrors* graphStatUnc,const TGraphAsymmErrors* graphStatSysUnc, bool isNormalized) {
  
  const TString diffXsec = isNormalized ? "RelDiffXsec" : "AbsDiffXsec";
  const TString fileOption = isNormalized ? "UPDATE" : "RECREATE";
  
  functions::writeObject(data, diffXsec, g_dataStyle.fileName, fileOption);   
  for (int ipred=0;ipred<(int)predictions.size();++ipred) {
    functions::writeObject(predictions[ipred], diffXsec, g_predictionStyles[ipred].fileName, fileOption);
  }
  functions::writeObject(graphStatUnc, diffXsec, g_statStyle.fileName, fileOption);
  functions::writeObject(graphStatSysUnc, diffXsec, g_statSysStyle.fileName, fileOption);
  
}


//----------------------------------------------------------------------

void writeNDObjects(const TH1D* data, const vector<TH1D*>& predictions, const TGraphAsymmErrors* graphStatUnc,const TGraphAsymmErrors* graphStatSysUnc, bool isNormalized) {
  
  TString diffXsec = isNormalized ? "RelDiffXsec" : "AbsDiffXsec";
  
  
  vector<TH1D*> dataProjections = g_histogramConverterND->makeProjections(data,"data"); // Make projections from concatenated histogram
  const int nprojections = dataProjections.size();
  
  const int npredictions = predictions.size();
  vector<vector<TH1D*>> predictionsProjections(npredictions);
  for (int i=0;i<npredictions;i++) {
    predictionsProjections[i] = g_histogramConverterND->makeProjections(predictions[i],((std::string)"Pred_"+functions::intToString(i+1)).c_str());
  }
  
  vector<TGraphAsymmErrors*> graphStatUncProjections = g_histogramConverterND->makeProjections(graphStatUnc,"Stat");
  vector<TGraphAsymmErrors*> graphStatSysUncProjections = g_histogramConverterND->makeProjections(graphStatSysUnc,"StatSys");
  
  for(int iproj=0;iproj<nprojections;iproj++) {
    TString fileOption("UPDATE"); 
    if(!isNormalized && iproj==0) fileOption = "RECREATE";
    
    TString diffXsecProj= diffXsec;
    std::vector<TString> externalBinInfo = g_histogramConverterND->getExternalBinsInfo(iproj,"binning");
    for(int i=0;i<(int)externalBinInfo.size();i++) {
      TString binInfo=externalBinInfo[i];
      binInfo.ReplaceAll(",","_");
      binInfo.ReplaceAll("[","_");
      binInfo.ReplaceAll("]","");
      diffXsecProj+=Form("_axis%i",i);
      diffXsecProj+=binInfo;
    }
    
    
    functions::writeObject(dataProjections[iproj], diffXsecProj, g_dataStyle.fileName, fileOption);
    for (int ipred=0;ipred<(int)predictions.size();++ipred) {
      functions::writeObject(predictionsProjections[ipred][iproj], diffXsecProj, g_predictionStyles[ipred].fileName, fileOption);
    }
    functions::writeObject(graphStatUncProjections[iproj], diffXsecProj, g_statStyle.fileName, fileOption);
    functions::writeObject(graphStatSysUncProjections[iproj], diffXsecProj, g_statSysStyle.fileName, fileOption);
    
  }


  for(auto h : dataProjections) delete h;
  for(const auto& vec : predictionsProjections) for(auto h : vec) delete h;
  for(auto g : graphStatUncProjections) delete g;
  for(auto g : graphStatSysUncProjections) delete g;
}

//----------------------------------------------------------------------


void draw1DComparisonPlots(const TString& figureName, const TString& variable, const TString& level, 
			   Predictions& predictions, Predictions& ratios, bool isNormalized, bool useLogScale) {
  
  TH1D* data = predictions.getData();

  if (!isNormalized){
  cout << "*****: variable: " << variable << endl;
  cout << "*****: bins    : " << data->GetNbinsX() << endl;
  int nbins = data->GetNbinsX(); 
  
  float hist_integral=0.; 
  
     
     for (int i=1; i<=nbins; i++){
  cout << "*****: bins " << i << "  : " << data->GetBinContent(i) << endl;
  hist_integral += data->GetBinContent(i) * data->GetXaxis()->GetBinWidth(i);

  }

     cout << "+++++++++: Integral: " << hist_integral  << endl;
  }

  const TString diffVariable = makeDiffVariable(variable,isNormalized);
  
   cout <<  "**** VARIABLE ****   " << variable << endl; 
   //cout <<  "**** UNIT ****       " << unit << endl; 
  const TString xtitle = NamesAndTitles::getXtitle(variable); 


   cout <<  "**** XTITLE ****   " << xtitle << endl; 
  
  predictions.setXtitle(xtitle);
  predictions.setYtitle(diffVariable);
  ratios.setXtitle(xtitle);
  ratios.setYtitle("#frac{Prediction}{Data}");
  
  // Inititializing canvas
  const double height_tot=800.*(1.-0.25) + 1.*800.*0.25;
  auto can= std::make_unique<TCanvas>("can2","canvas 2 pads",0,0,1000.,height_tot);
  
  can->cd();
 
  auto pad1 = std::unique_ptr<TPad>{makePad("pad1",0. /*xmin*/, 0.4 /*ymin*/, 1. /*xmax*/,1. /*ymax*/,0.19 /*leftMargin*/,0.05/*topMargin*/,0.05/*rightMargin*/,0. /*bottomMargin*/)};
  auto pad2 = std::unique_ptr<TPad>{makePad("pad2",0. /*xmin*/, 0. /*ymin*/, 1. /*xmax*/,0.4 /*ymax*/,0.19 /*leftMargin*/,0./*topMargin*/,0.05/*rightMargin*/,0.5 /*bottomMargin*/)};
  pad2->SetGridy(1);
  
  // Initializing legend
  
  pad1->Draw();
  pad2->Draw();
  
  pad1->cd();
  
  double leg_x1 = 0.58;
  double leg_y1 = 0.49;
  double leg_x2 = 0.91;
  double leg_y2 = 0.91;
  auto leg = std::unique_ptr<TLegend>{makeLegend(leg_x1,leg_y1,leg_x2,leg_y2)};

  predictions.fillLegend(leg.get());
  
  
  // -------------------------------------
  // Setting y-axes range
  
  double coeffMax = level.Contains("Particle") ? 0.55 : 0.75;
  predictions.setRangeUser(useLogScale,0.05,coeffMax);
  double ymin = ratios.getMinimum(0.);
  double ymax = ratios.getMaximum();
  ratios.setRangeUser(ymin - (ymax-ymin)*0.08,ymax + (ymax-ymin)*0.08);
  if (variable=="randomTop_pt" && level.Contains("Parton")) ratios.setRangeUser(ymin - (ymax-ymin)*0.09,ymax + (ymax-ymin)*0.09);
  if (variable=="randomTop_y" && level.Contains("Parton")) ratios.setRangeUser(ymin - (ymax-ymin)*0.09,ymax + (ymax-ymin)*0.09);
  if (variable=="randomTop_pt" && level.Contains("Particle")) ratios.setRangeUser(ymin - (ymax-ymin)*0.11,ymax + (ymax-ymin)*0.11);
  if (variable=="ttbar_deltaphi" && level.Contains("Parton")) ratios.setRangeUser(ymin - (ymax-ymin)*0.12,ymax + (ymax-ymin)*0.12); 
  if (variable=="chi_ttbar" && level.Contains("Parton")) ratios.setRangeUser(ymin - (ymax-ymin)*0.12,ymax + (ymax-ymin)*0.12); 
  if (variable=="chi_ttbar" && level.Contains("Particle")) ratios.setRangeUser(ymin - (ymax-ymin)*0.12,ymax + (ymax-ymin)*0.12); 
  if (variable=="ttbar_mass" && level.Contains("Particle")) ratios.setRangeUser(ymin - (ymax-ymin)*0.10,ymax + (ymax-ymin)*0.10); 
  if (variable=="pout" && level.Contains("Particle")) ratios.setRangeUser(ymin - (ymax-ymin)*0.10,ymax + (ymax-ymin)*0.10); 
  if (variable=="t2_pt" && level.Contains("Particle")) ratios.setRangeUser(ymin - (ymax-ymin)*0.12,ymax + (ymax-ymin)*0.12);
  if (variable=="ttbar_pt" && level.Contains("Particle"))  ratios.setRangeUser(ymin - (ymax-ymin)*0.12,ymax + (ymax-ymin)*0.12);
  
  if (!isNormalized){  
     if (variable=="t2_pt"          && level.Contains("Parton"))  ratios.setRangeUser(ymin - (ymax-ymin)*0.15,ymax + (ymax-ymin)*0.15);
     if (variable=="t2_y"          && level.Contains("Parton"))  ratios.setRangeUser(ymin - (ymax-ymin)*0.15,ymax + (ymax-ymin)*0.15);
  }

  //ratios.setRangeUser(0.5,1.41);

  const double ytitleSizeSF=0.85;
	

  data->GetYaxis()->SetTitleSize(0.07*ytitleSizeSF);
  data->GetYaxis()->SetLabelSize(0.06);
  data->GetYaxis()->SetTitleOffset(0.8/ytitleSizeSF);	
  if (variable=="ttbar_deltaphi" && level.Contains("Parton"))   data->SetMinimum(0.011);	
  if (variable=="pout"           && level.Contains("Parton"))   data->SetMinimum(0.011);	
  if (variable=="ttbar_deltaphi" && level.Contains("Parton"))   data->SetMinimum(0.011);	
  if (variable=="cos_theta_star"                            )   data->SetMinimum(0.01);	
  if (variable=="t2_pt"          && level.Contains("Particle")) data->SetMinimum(0.005);	
  
  if (!isNormalized){  
     if (variable=="t2_pt"          && level.Contains("Particle")) data->SetMaximum(1000000.);
     if (variable=="t1_pt"          && level.Contains("Parton"))   data->SetMinimum(5.);	
  }

  data->GetXaxis()->SetTitleSize(0);
  data->GetXaxis()->SetLabelSize(0);
  data->GetXaxis()->SetNdivisions(508);

  
  
  //pad1->cd();

  size_t i=0;
  for(auto& h : predictions.getHistos()) {

    h->SetMarkerStyle(0);
    h->SetMarkerSize(0);
    i++;
  }

  data->Draw("p X0");
  
  gPad->SetLogy(useLogScale);
  if(variable=="chi_ttbar") {
    //data->GetXaxis()->SetRangeUser(data->GetXaxis()->GetBinLowEdge(1),data->GetXaxis()->GetBinUpEdge(data->GetXaxis()->GetNbins()));
    gPad->SetLogx(true);
  }
  
  for(auto& gr : predictions.getUnc()) {

    gr->Draw("same2");
  }
  
  for(auto& h : predictions.getHistos()) {

    h->Draw("same");
  }
  
  if(predictions.getHistos().size()>1) {

    predictions.getHistos().at(0)->Draw("HISTsame");
  }
  
  for(auto& gr : predictions.getGraphs()) {

    gr->Draw("same2");
    gr->DrawClone("sameP");
  }
  
  data->Draw("p X0 same");


  gPad->SetLogy(useLogScale);
  if (variable=="chi_ttbar")  gPad->SetLogx(1);
  leg->Draw();
  
  pad2->cd();
  
  TH1D* nomRatio = ratios.getHistos()[0].get();
  nomRatio->GetXaxis()->SetTitleSize(0.12);
  nomRatio->GetXaxis()->SetTitleOffset(1.2*1.);
  nomRatio->GetXaxis()->SetLabelSize(0.1);
  if (variable=="chi_ttbar") nomRatio->GetXaxis()->SetLabelSize(0.09); 
  nomRatio->GetXaxis()->SetNdivisions(508);
  
  nomRatio->GetYaxis()->SetTitleSize(0.1*ytitleSizeSF);
  nomRatio->GetYaxis()->SetTitleOffset(0.6/ytitleSizeSF);
  nomRatio->GetYaxis()->SetNdivisions(508);
  if (variable=="t1_y"           && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);
  //if (variable=="randomTop_pt"   && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="randomTop_y"   && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="randomTop_y"   && level.Contains("Parton")) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="t2_y"                                        ) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="ttbar_deltaphi" && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="pout"           && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="cos_theta_star" && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="cos_theta_star" && level.Contains("Parton"))   nomRatio->GetYaxis()->SetNdivisions(504);
  if (variable=="chi_ttbar"      && level.Contains("Particle")) nomRatio->GetYaxis()->SetNdivisions(504);

  nomRatio->GetYaxis()->SetLabelSize(0.085);
  nomRatio->SetStats(kFALSE);
    
     nomRatio->GetXaxis()->Print();

  if (variable=="chi_ttbar") nomRatio->GetXaxis()->SetMoreLogLabels();
  nomRatio->Draw();
  if(variable=="chi_ttbar") {
    //nomRatio->GetXaxis()->SetRangeUser(data->GetXaxis()->GetBinLowEdge(1),data->GetXaxis()->GetBinUpEdge(data->GetXaxis()->GetNbins()));
    gPad->SetLogx(true);
  }
  
  for(auto& gr : ratios.getUnc()) {
    gr->Draw("same2");
  }
  
  for(auto& h : ratios.getHistos()) h->Draw("same");
  
  nomRatio->Draw("same");
  
  for(auto& gr : ratios.getGraphs()) {
    gr->Draw("same2");
    gr->DrawClone("sameP");
  }
  
  
  if (variable=="chi_ttbar") pad2->SetLogx(1);
  pad2->RedrawAxis("g");
  pad2->RedrawAxis();

  pad1->cd();
    
  double x=0.23; //Position of the ATLAS label
  double y=0.92;

  double size=0.06;
  double shift=size*1.06;
  
  
  std::vector<TString> generalInfo = makeGeneralInfo(level);
  int nshifts = generalInfo.size();
  
  TPaveText paveText (x,y-nshifts*shift,0.3,y,"NDC"); 
  paveText.SetFillColor(0);
  paveText.SetBorderSize(0);
  paveText.SetTextSize(size);
  paveText.SetTextAlign(12);
  paveText.SetTextFont( 42 );
  
  for(const TString& info : generalInfo) paveText.AddText(info);

  paveText.Draw();
  
  pad1->RedrawAxis();
  pad1->Update();
  
  can->SaveAs(g_pathToFigures + "/pdf." + g_mc_samples_production + "/" + g_dirname + "/" + figureName + ".pdf");
  can->SaveAs(g_pathToFigures + "/png." + g_mc_samples_production + "/" + g_dirname + "/" + figureName + ".png");
  
  // Inititializing canvas
  auto canRatio= std::make_unique<TCanvas>("canRatio","canvas ratios",0,0,1000.,height_tot);
  canRatio->cd();
  
  nomRatio->GetXaxis()->SetTitleSize(0.07);
  nomRatio->GetXaxis()->SetTitleOffset(0.9);
  nomRatio->GetXaxis()->SetLabelSize(0.06);
  nomRatio->GetXaxis()->SetNdivisions(508);
  
  nomRatio->GetYaxis()->SetTitleSize(0.07*ytitleSizeSF);
  nomRatio->GetYaxis()->SetTitleOffset(0.9/ytitleSizeSF);
  nomRatio->GetYaxis()->SetLabelSize(0.06);  
  
  
  ratios.setRangeUser();
  
  //nomRatio->GetYaxis()->SetRangeUser(0.71,1.89);
  
  nomRatio->Draw();
  for(auto& gr : ratios.getUnc()) {
    gr->Draw("same2");
  }
  
  for(auto& h : ratios.getHistos()) h->Draw("same");
  
  nomRatio->Draw("same");
  
  for(auto& gr : ratios.getGraphs()) {
    gr->Draw("same2");
    gr->DrawClone("sameP");
  }
  size=0.04;
  shift=size*1.06;
  
  paveText.SetY1(y-nshifts*shift);
  paveText.SetX1(0.18);
  paveText.SetTextSize(size);
  paveText.Draw();
  
  leg_y1 = 0.7;
  leg_x1 = 0.57;
  leg_x2 = 0.9;
  leg_y2 = 0.92;
  leg = std::unique_ptr<TLegend>{makeLegend(leg_x1,leg_y1,leg_x2,leg_y2)};
  leg->SetTextSize(0.037);
  ratios.fillLegend(leg.get());
  leg->Draw();
 



  gPad->RedrawAxis("g");
  gPad->RedrawAxis();
  
  
  
  
  
  TString figureNameRatio=figureName;
  figureNameRatio.ReplaceAll("_log","");
  figureNameRatio+="_ratio";
  
  canRatio->SaveAs(g_pathToFigures + "/pdf." + g_mc_samples_production + "/" + g_dirname + "/" + figureNameRatio + ".pdf");
  canRatio->SaveAs(g_pathToFigures + "/png." + g_mc_samples_production + "/" + g_dirname + "/" + figureNameRatio + ".png");
  
  
  
}

//----------------------------------------------------------------------

void drawNDComparisonPlots(const TString& figureName, const TString& variable, const TString& level,
			   vector<Predictions>& predictionsProjections, vector<Predictions>& ratiosProjections,
			   bool isNormalized, bool useLogScale) {
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable);
  const TString diffVariable = makeDiffVariable(variable,isNormalized);
  
  const TString xtitle = xtitles.back();
  
  const int nprojections = predictionsProjections.size();
  
  int nPerLine(0),nlines(0);
  functions::getGridSize(nprojections,nlines,nPerLine);
  
  const double canvas_xsize = (1000./3) * nPerLine;
  //const double canvas_ysize = 150.*(nlines+1);
  const double canvas_ysize = 175.*(nlines+1);
  
  std::vector<TString> generalInfo = makeGeneralInfo(level);
  
  MultiDimensionalPlotsSettings* plotSettings = g_plotSettings.get();
  
  plotSettings->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, generalInfo.size());
  plotSettings->print();
  
  auto can = std::unique_ptr<TCanvas>(plotSettings->makeCanvas("can"));
  auto canRatio = std::unique_ptr<TCanvas>(plotSettings->makeCanvas("canRatio"));
  
  can->cd();
  vector<vector<shared_ptr<TPad>>> pads = plotSettings->makeArrayOfPads(nprojections);
  canRatio->cd();
  vector<vector<shared_ptr<TPad>>> padsRatios = plotSettings->makeArrayOfPads(nprojections);
  
  int iproj=0;
  for(int iline=0;iline<nlines;iline++) {
    int nInLine = pads[iline].size();
    
    for(int iInLine=0;iInLine<nInLine;iInLine++) {
      
      Predictions& predictions = predictionsProjections[iproj];
      Predictions& ratios = ratiosProjections[iproj];
      
      
      TPad* currentPad = pads[iline][iInLine].get();
      TPad* currentPadRatio = padsRatios[iline][iInLine].get();
      currentPad->cd();
      
      TH1D* h_data = predictions.getData();
      
      plotSettings->setHistLabelStyle(h_data);
      
      // Y-axis labels
      if(iInLine ==0) {
	h_data->GetYaxis()->SetTitle(diffVariable);
	for(auto& h : ratios.getHistos()) {
	  h->GetYaxis()->SetTitle("Pred. / Data");
	}
      }
      else {
	h_data->GetYaxis()->SetTitle("");
	for(auto& h : ratios.getHistos()) {
	  h->GetYaxis()->SetTitle("");
	}
      }
      // x-axis labels
      if(false) { 
      //if(iline != nlines-1) { 
	h_data->GetXaxis()->SetTitle("");
	for(auto& h : ratios.getHistos()) {
	  h->GetXaxis()->SetTitle("");
	}
      }
      else {
	h_data->GetXaxis()->SetTitle(xtitle);
	for(auto& h : ratios.getHistos()) {
	  h->GetXaxis()->SetTitle(xtitle);
	}
      }
      
      
      std::vector<TString> externalBinInfo = g_histogramConverterND->getExternalBinsInfo(iproj);
      TPaveText* paveText = functions::makePaveText(externalBinInfo, plotSettings->textSizePad(), 0.3, 0.75, 0.93); // size,xmin,xmax,ymax
      paveText->SetFillStyle(0);
      
      const double coeffMax=0.37*(1.+(externalBinInfo.size()-1)*1.5);
      predictions.setRangeUser(useLogScale,0.05,coeffMax);
      ratios.setRangeUser(false,0.05,coeffMax);
      
      h_data->Draw("p X0");
      
      for(auto& unc : predictions.getUnc())
	unc->Draw("same2");
      
      for(auto& h : predictions.getHistos())
	h->Draw("same");
	
      if(!predictions.getHistos().empty()) 
	predictions.getHistos()[0]->Draw("HISTsame");
	
      for(auto& h : predictions.getGraphs()){
	h->Draw("same2");
	h->DrawClone("sameP");
      }
      
      h_data->Draw("p X0 same");
      
      paveText->Draw();
      
      currentPad->SetLogy(useLogScale);
      
      currentPad->RedrawAxis("g"); 
      currentPad->RedrawAxis(); 
      
      currentPadRatio->cd();
      
      
      plotSettings->setHistLabelStyle(ratios.getHistos()[0].get());
      ratios.getHistos()[0]->Draw();
      for(auto& unc : ratios.getUnc())
	unc->Draw("same2");
      
      for(auto& h : ratios.getHistos())
	h->Draw("same");
      ratios.getHistos()[0]->Draw("same");
      
      for(auto& h : ratios.getGraphs()){
	h->Draw("same2");
	h->DrawClone("sameP");
      }
      

      //TPaveText* paveTextRatio = functions::makePaveText(externalBinInfo, plotSettings->textSizePad(), 0.35, 0.75, 0.33+externalBinInfo.size()*plotSettings->textSizePad()*1.14 ); // size,xmin,xmax,ymax
      TPaveText* paveTextRatio = functions::makePaveText(externalBinInfo, plotSettings->textSizePad(), 0.3, 0.75, 0.93); // size,xmin,xmax,ymax
      paveTextRatio->SetFillStyle(0);
      paveTextRatio->Draw();
      
      currentPadRatio->RedrawAxis("g"); 
      currentPadRatio->RedrawAxis(); 
      
      iproj++;
    }
  }
  
  double xminGeneralInfo=0.133;
  double xmaxGeneralInfo=0.283;
  if(variable=="ttbar_y_vs_ttbar_mass_vs_t1_pt" || variable=="ttbar_y_vs_ttbar_pt_vs_ttbar_mass") {
    xminGeneralInfo=0.09;
    xmaxGeneralInfo=0.24;
  }
  
  TPaveText* paveTextGeneralInfo = plotSettings->makePaveText(generalInfo,xminGeneralInfo,xmaxGeneralInfo); 
  
  auto leg = std::unique_ptr<TLegend>(plotSettings->makeLegend(0.4,0.98));
  leg->SetNColumns(2);
  
  auto legRatio = std::unique_ptr<TLegend>((TLegend*)leg->Clone());
  
  predictionsProjections[0].fillLegend(leg.get());
  ratiosProjections[0].fillLegend(legRatio.get());
  
  can->cd();
  
  leg->Draw();
  paveTextGeneralInfo->Draw();
  
  can->SaveAs(g_pathToFigures+ "/pdf."+g_mc_samples_production+"/" + g_dirname+"/"+figureName + "_log.pdf");
  can->SaveAs(g_pathToFigures+ "/png."+g_mc_samples_production+"/" + g_dirname+"/"+figureName + "_log.png");
  
  canRatio->cd();
  legRatio->Draw();
  paveTextGeneralInfo->Draw();
  canRatio->SaveAs(g_pathToFigures+ "/pdf."+g_mc_samples_production+"/" + g_dirname+"/"+figureName + "_ratio.pdf");
  canRatio->SaveAs(g_pathToFigures+ "/png."+g_mc_samples_production+"/" + g_dirname+"/"+figureName + "_ratio.png");
  
  
}



//----------------------------------------------------------------------


void drawSinglePadNDComparison(const TString& figureName, const TString& variable, const TString& level, TH1D* data, TH1D* prediction, bool isNormalized, bool useLogScale) {
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable);  
  const TString diffVariable = makeDiffVariable(variable,isNormalized);
  
  vector<TH1D*> data_proj = g_histogramConverterND->makeProjections(data,"data"); // Make projections from concatenated histogram
  const int nprojections = data_proj.size();
  
  vector<TH1D*> prediction_proj = g_histogramConverterND->makeProjections(prediction,"Prediction");
  
  double canvas_x = 1800.;
  double canvas_y = 1000.;
  if(nprojections > 6) canvas_x = 800;  
  
  
  auto can = std::make_unique<TCanvas>("c","",0,0,canvas_x,canvas_y);
  
  const double markerSize = 2.;
  std::vector<int> markerStyles = {20,24,21,25,22,26,23,32,33,27,34,28,29,30,31,2,5};
  //std::vector<int> lineColors = {2,3,4,kPink-8,6,7,8,9,kOrange+10,11,12,13,14,15,16,17,18};
  
  int ncolumns(0);
  double textSize(0.);
  double textSizeInfo(0.);
  double xminInfo(0.);
  double xmaxInfo(0.);
  double ymaxInfo(0.);
  
  std::vector<TString> generalInfo = makeGeneralInfo(level, /*singlePad=*/true);
  std::unique_ptr<TLegend> leg;
  
 
  if (nprojections>6) {
    ncolumns = 1;
    textSize = 0.025;
    textSizeInfo=0.035;
    xminInfo=0.26;
    xmaxInfo=0.4;
    ymaxInfo=0.935;
    
    gPad->SetLeftMargin(0.23);
    gPad->SetBottomMargin(0.13);
    
    double ymaxLegend = 0.815;
    
    leg = std::make_unique<TLegend>(0.25,ymaxLegend - 1.01*textSize*std::ceil((double)nprojections+1/ncolumns),0.85,ymaxLegend);
    leg->SetTextSize(textSize);
  }
  else {
    ncolumns = 1;
    textSize = 0.035;
    textSizeInfo = 0.04;
    xminInfo=0.165;
    xmaxInfo=0.5;
    ymaxInfo=0.92;
    
    gPad->SetLeftMargin(0.15);
    gPad->SetBottomMargin(0.13);
    
    leg = std::make_unique<TLegend>(0.5,0.925 - 1.5*textSize*(nprojections+1),0.9,0.925);
    leg->SetTextSize(textSize);
    
  }
    
  TPaveText* paveText = functions::makePaveText(generalInfo, textSizeInfo, xminInfo, xmaxInfo, ymaxInfo);
  paveText->SetFillStyle(0);  
  
  leg->SetNColumns(ncolumns);
  leg->SetTextFont(42);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  
  
  leg->SetFillStyle(0);
  
  
  int exponentFactor = getExponentFactor(nprojections,variable);
  
  int exponent = (nprojections-1)*std::abs(exponentFactor);
  
  double linearFactor = getLinearFactor(variable);;
  double lf = (nprojections-1)*linearFactor;
  
  
  for(int i=0;i<nprojections;i++) {
    const double factor = (useLogScale ? pow(10.,exponent) : lf);
    
    std::vector<TString> externalBinInfo = g_histogramConverterND->getExternalBinsInfo(i);
    TString externalBinInfoInLine = externalBinInfo[0];
    for(int j=1;j<(int)externalBinInfo.size();j++) {
      externalBinInfoInLine+=", "+externalBinInfo[j];
    }

    TString entry = externalBinInfoInLine;
    if(exponentFactor!=0) entry = Form("(x10^{%i}), ",exponent) + entry;
    if(linearFactor!=0.) entry = Form("(+%1.1f), ",std::max(factor,0.)) + entry;
    
    if(exponentFactor==0 && linearFactor==0.) entry = "Data, " + entry;
    else entry = "Data " + entry;
    
    
    data_proj[i]->SetMarkerSize(markerSize);
    if(i<(int)markerStyles.size()) data_proj[i]->SetMarkerStyle(markerStyles[i]);
    //if(i<(int)lineColors.size()) {
    data_proj[i]->SetLineColor(2);
    data_proj[i]->SetLineWidth(2);
    //int lineStyle = i%2 ? 1 : 7;
    int lineStyle = 1;
    data_proj[i]->SetLineStyle(lineStyle);
    prediction_proj[i]->SetLineColor(2);
    prediction_proj[i]->SetLineStyle(lineStyle);
    //}
    
    
    leg->AddEntry(data_proj[i],entry,"p");
    if(i+1==nprojections) {
      leg->AddEntry(data_proj[i],"PWG+Py8","l");
    }
    
    if(useLogScale) {
      data_proj[i]->Scale(factor);
      prediction_proj[i]->Scale(factor);
    } else {
      for(int ibin=1;ibin<=data_proj[i]->GetNbinsX();ibin++) {
	data_proj[i]->SetBinContent(ibin,data_proj[i]->GetBinContent(ibin)+factor);
	prediction_proj[i]->SetBinContent(ibin,prediction_proj[i]->GetBinContent(ibin)+factor);
      }
      
    }
    exponent+=exponentFactor;
    lf-=linearFactor;
  }
  
  vector<TH1D*> joinedVectors;
  functions::joinVectors(data_proj,prediction_proj,joinedVectors);
  
  std::pair<double,double> xRange = functions::getXRange(joinedVectors);
  std::pair<double,double> yRange; 
  if(nprojections>6 && level.Contains("Particle")) yRange = functions::getYRange(joinedVectors,useLogScale,0.1,0.12*nprojections); 
  else if(nprojections>6 && level.Contains("Parton")) yRange = functions::getYRange(joinedVectors,useLogScale,0.1,0.12*nprojections); 
  else yRange = functions::getYRange(joinedVectors,useLogScale,0.1,std::max(0.03*nprojections,0.11*generalInfo.size()));
  
  std::cout << "Plot range: " << "xmin: " << xRange.first << " xmax: " << xRange.second << " ymin: " << yRange.first << " ymax: " << yRange.second << std::endl;
  
  
  auto h_dummy = std::make_unique<TH2D>("h_dummy","",2,xRange.first,xRange.second,2,yRange.first,yRange.second);
  h_dummy->GetXaxis()->SetTitle(xtitles.back());
  h_dummy->GetYaxis()->SetTitle(diffVariable);
  h_dummy->GetXaxis()->SetTitleOffset(1.1);
  h_dummy->GetYaxis()->SetTitleOffset(1.3);
  if(nprojections>6) {
    h_dummy->GetYaxis()->SetTitleOffset(2.);
  }
  h_dummy->Draw();
  
  TString drawOptPrediction="";
  TString drawOptData="HIST P";
  
  for(int i=nprojections-1;i>=0;i--) {
    prediction_proj[i]->Draw(drawOptPrediction + " SAME");
  }
  
  for(int i=nprojections-1;i>=0;i--) {
    data_proj[i]->Draw(drawOptData + "SAME");
  }
  
  leg->Draw();
  paveText->Draw();
  
  can->SetLogy(useLogScale);
  
  can->SaveAs(g_pathToFigures+ "/pdf."+g_mc_samples_production+"/" + g_dirname+"/"+figureName + "_log.pdf");
  can->SaveAs(g_pathToFigures+ "/png."+g_mc_samples_production+"/" + g_dirname+"/"+figureName + "_log.png");
  
  for(TH1D* proj : data_proj) delete proj;
  for(TH1D* proj : prediction_proj) delete proj;
  
}

//----------------------------------------------------------------------

int getExponentFactor(int nprojections,const TString& variable) {
  int exponentFactor = -1;
  if(nprojections>6) exponentFactor = -2;
  if( variable=="t1_pt_vs_t2_pt" ||
      variable=="t1_y_vs_t2_y" ||
      variable=="ttbar_y_vs_t1_y" ||
      variable=="ttbar_y_vs_t2_y" ||
      variable=="t1_pt_vs_delta_pt"
    ) exponentFactor=0;
  
  return exponentFactor;
}

//----------------------------------------------------------------------

double getLinearFactor(const TString& variable) {
  double factor = 0.;
  if( variable=="t1_y_vs_t2_y" ||
      variable=="delta_y_vs_t1_y"
    ) factor=0.2;
    
  if( variable=="ttbar_y_vs_t1_y" ||
      variable=="ttbar_y_vs_t2_y" ||
      variable=="ttbar_y_vs_top_y"
    ) factor=0.3;
  
  return factor;
}

//----------------------------------------------------------------------

int saveObj(TString outDir, TString level, TString observable, TString sampleName,
	    TObject* obj, TString objName, bool recreateFile)
{
  sampleName.ReplaceAll("\\","");
  sampleName.ReplaceAll("{" ,"");
  sampleName.ReplaceAll("}" ,"");
  sampleName.ReplaceAll(" " ,"_");

  TString fileMode = recreateFile ? "RECREATE" : "UPDATE";
  TFile *f = new TFile( outDir + "/" + level + "." + observable + "." + sampleName + ".root", fileMode.Data());

  obj->Write(objName);

  f->Close();
  delete f;
  
  return 0;
}

//----------------------------------------------------------------------

void createGrafWithSysErrors(TGraphAsymmErrors* graph_unc,TMatrixT<double>& cov){
	
  for (int ibin=0;ibin<graph_unc->GetN();++ibin){
    graph_unc->SetPointEYlow(ibin,sqrt(std::abs(cov[ibin][ibin])));
    graph_unc->SetPointEYhigh(ibin,sqrt(std::abs(cov[ibin][ibin])));
  }
}

//----------------------------------------------------------------------

void createHistWithSysErrors(TH1D* hist_unc,TMatrixT<double>& cov){
	
  for (int ibin=0;ibin<hist_unc->GetNbinsX();++ibin){
    hist_unc->SetBinError(ibin+1,sqrt(fabs(cov[ibin][ibin])));
  }
}

//----------------------------------------------------------------------

TGraphAsymmErrors* makeGraphWithErrors(TFile* f, TH1D* h, const TString& covarianceName) {
  TGraphAsymmErrors* graph = functions::graphFromHist(h);
  auto cov=std::unique_ptr<TMatrixT<double>>{(TMatrixT<double>*)f->Get(covarianceName)};
  if(!cov) {
    std::cout << "Error in makeGraphWithErrors: Failed to load covariance " << covarianceName << " from " << f->GetName() << std::endl;
    return nullptr;
  }
  createGrafWithSysErrors(graph,*cov);
  return graph;
}

//----------------------------------------------------------------------

void setupHistStyle(TH1* hist, const PredictionStyle& predStyle) {
  hist->SetLineColor(predStyle.color);
  hist->SetMarkerColor(predStyle.color);
  hist->SetMarkerSize(predStyle.markerSize);
  hist->SetMarkerStyle(predStyle.markerStyle);
  hist->SetLineWidth(predStyle.lineWidth);
  hist->SetLineStyle(predStyle.lineStyle);
}

//----------------------------------------------------------------------

std::vector<TGraphAsymmErrors*> loadNDNNLOPrediction(TFile* f,const TString& variable) {
  std::vector<TGraphAsymmErrors*> output;
  std::vector<TString> keys = functions::getVectorOfKeys(f, variable);
  std::vector<TString> selected;
  for(const TString& key : keys) {
    if(key.Contains("_slice_") && key.EndsWith("_total")) {
      selected.push_back(key);
    }
  }
  functions::sortVectorOfTStrings(selected);
  
  for(const TString& key : selected) {
    std::cout << "Key: " << key << std::endl;
    output.push_back((TGraphAsymmErrors*)f->Get(key));
  }
  
  return output;
}
  
  
//----------------------------------------------------------------------

TString makeDiffVariable(const TString& variable,const bool isNormalized) {

   TString diffVariable = NamesAndTitles::getDifferentialVariableLatex(variable);
   TString inverseUnit = NamesAndTitles::getInverseUnit(variable);
   if (inverseUnit != ""){
      if(diffVariable!="" && !isNormalized) diffVariable =  (TString)"#frac{d #sigma}{" +diffVariable+"}"+ " [fb " + inverseUnit +"]";
      else if (diffVariable!="" && isNormalized) diffVariable = (TString)"#frac{1}{#sigma} " +"#frac{d #sigma}{" +diffVariable+"}"+ " ["+ inverseUnit +"]";
   }
   else{
      if (diffVariable!="" && isNormalized) diffVariable = (TString)"#frac{1}{#sigma} " +"#frac{d #sigma}{" +diffVariable+"}";
   }
   return diffVariable;
}

//----------------------------------------------------------------------

std::vector<TString> makeGeneralInfo(const TString& level, bool singlePad) {
  
  std::vector<TString> generalInfo;
  
  if(singlePad) {
    generalInfo.push_back(NamesAndTitles::ATLASString(g_lumi_string) + ", " + NamesAndTitles::energyLumiString(g_lumi_string));
    generalInfo.push_back(NamesAndTitles::analysisLabel());
  }
  else {
    generalInfo.push_back(NamesAndTitles::ATLASString(g_lumi_string));
    generalInfo.push_back(NamesAndTitles::energyLumiString(g_lumi_string));
    generalInfo.push_back(NamesAndTitles::analysisLabel());
    
    
  }
  
  std::vector<TString> levelInfo = NamesAndTitles::getLevelInfo(level);
  for(const TString& info : levelInfo)  generalInfo.push_back(info);

  return generalInfo;
  
}

