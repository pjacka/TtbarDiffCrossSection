
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "CovarianceCalculator/SystematicHistos.h"
#include "CovarianceCalculator/PseudoexperimentsCalculator.h"
#include "CovarianceCalculator/CovarianceCalculator.h"
#include "HelperFunctions/AtlasStyle.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"


using std::cout;
using std::endl;
using std::cerr;

int main(int nArg, char **arg) {
  cout << endl << "Running ProducePseudoexperiments for variable " << arg[6] << endl << endl;
  TH1::AddDirectory(kFALSE);
  SetAtlasStyle();
  try {
    if(nArg!=9) throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    delete gRandom;
    gRandom = new TRandom3(1);
    TString mainDir=arg[1];
    TString path_config_files=arg[2];
    TString path_config_lumi=arg[3];
    TString path_config_binning=arg[4];
    TString path_config_sysnames=arg[5];
    TString signal=arg[6];
    TString variable_name=arg[7];
    TString level=arg[8];
    
    
    TString variable =variable_name;
    variable.ReplaceAll("_fine","");
    
    TString pathToCovariances=mainDir+"/root/covariances/";
    gSystem->mkdir(pathToCovariances,true);
    TFile *f =new TFile(pathToCovariances + variable_name + "_" + level + ".root");
    if(f->IsZombie())throw  TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with covariances from " + pathToCovariances + variable_name + "_" + level + ".root"); 
    
    const TString dataBased="_covDataBased";
    //const TString method="StatAndSysNormalized";
    const TString method="StatMCStatRecoAndSysNormalized";
    //const TString method="StatMCStatRecoAndSys";
    //const TString method="StatAndSys";
    //const TString method="OnlyStat";
    //const TString method="OnlyDataStat";
    //const TString method="OnlyStatNormalized";
    
    TMatrixT<double>* cov = (TMatrixT<double>*)f->Get(variable + dataBased + method);
    
    //cov->Print();
    TVectorT< double > eigenValues;
    TMatrixT<double> eigenVectors =  cov->EigenVectors (eigenValues);
   
    const int size = eigenValues.GetNrows();
    for(int i=0;i<size;i++) {
      cout << size - i << " " <<  eigenValues[i];
      if(i<size-1) cout << " " << eigenValues[i+1]/eigenValues[i];
      cout <<  endl;
    
    }
	  
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  cout << "Everything ok!" << endl;
  return 0;
}

