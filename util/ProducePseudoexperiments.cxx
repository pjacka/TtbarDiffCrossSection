
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "CovarianceCalculator/SystematicHistos.h"
#include "CovarianceCalculator/PseudoexperimentsCalculator.h"
#include "CovarianceCalculator/CovarianceCalculator.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

int producePseudoExperiments( const TString& mainDir,
                              const TString& path_config_lumi,
                              const TString& path_config_sysnames,
                              const TString& variable_name,
                              const TString& level,
			      const TString& inputDir,
			      const TString& generatorSysDir,
			      const TString& pseudoexperimentsDir,
			      const TString& bootstrapDir,
			      const TString& outputDir,
                              const bool writePseudoexperiments,
                              const bool useBootstrapHistos,
			      const int& seed,
                              int nPseudoexperiments,
			      const int& debugLevel)
			      
{
  SetAtlasStyle();
  delete gRandom;
  gRandom = new TRandom3(seed);
  
  if(debugLevel > 0){
    cout << "writePseudoexperiments: " << writePseudoexperiments << endl;
    cout << "useBootstrapHistos: " << useBootstrapHistos << endl;
  }
  
  auto ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);

  TString mc_samples_production = ttbarConfig->mc_samples_production();
  
  
  
  double lumi = configFunctions::getLumi(*ttbarConfig) ;
  if(lumi<0.)throw TtbarDiffCrossSection_exception("Error: Unknown samples production!");
  
  double lumi_unit=1.;
  lumi/=lumi_unit;
  
  
  bool useSignalModeling=ttbarConfig->doSignalModeling();
  
  const TString rootDir="root."+mc_samples_production;
  const TString texDir="tex."+mc_samples_production;
  
  
  TFile* file_systematics= new TFile(mainDir+"/" + rootDir + "/" + inputDir + "/" + variable_name + "_" + level + ".root");
  if(file_systematics->IsZombie()){
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/" + rootDir + "/" + inputDir + "/" + variable_name + "_" + level + ".root"); 
    return -1;
  }
  
  TString variable=variable_name;
  variable.ReplaceAll("_fine","");
  //TString chain_name="nominal";
  
  SystematicHistos* sysHistos= new SystematicHistos(file_systematics,variable,*ttbarConfig); // Contains nominal and all shifted histograms
  sysHistos->prepareRelativeShiftsSignalModeling(); // Prepare relative post-unfolding shifts for signal modeling covariances calculation
  
  PseudoexperimentsCalculator* pseudoexperimentsCalculator= new PseudoexperimentsCalculator(sysHistos,path_config_lumi,path_config_sysnames,mainDir,variable_name,level,debugLevel); // Take care about calculating random shifts at reco level
  Pseudoexperiment* pseudoexperiment = new Pseudoexperiment(pseudoexperimentsCalculator); // Contains shifted pseudoexperiments at reco level
  
  
  Pseudoexperiment* nominalPseudoexperiment = new Pseudoexperiment(pseudoexperimentsCalculator); // Nominal histos at reco level
  pseudoexperimentsCalculator->prepareNominalPseudoexperiment(nominalPseudoexperiment);
  CovarianceCalculator* covarianceCalculator = new CovarianceCalculator(nominalPseudoexperiment,level,debugLevel); // Used to unfold reco level pseudoexperiments and calculate covariance matrix
  covarianceCalculator->setLumi(lumi);
  covarianceCalculator->setVariableName(variable);
  covarianceCalculator->setNominalResponse(nominalPseudoexperiment);
  covarianceCalculator->setLevel(level);
  
  if(variable.Contains("concatenated")){ // for big covariance with all spectra in one big histogram
    vector<int> nbins;
    if(level.Contains("Parton"))nbins={7,8,8,8,7,8,10,8,8,7,4,7,7,6,10}; // number of bins of single variables
    if(level.Contains("Particle"))nbins={8,8,7,8,10,8,8,7,4,7,7,6,10};
    covarianceCalculator->setSubHistNbins(nbins);
    covarianceCalculator->setConcatenated(true); // Letting know that we are using concatenated histogram
    if(debugLevel > 0) cout << "Number of subhistograms for concatenated histogram: " << nbins.size() << endl;
  }
  
  // Get Central values
  covarianceCalculator->calculateCentralValues(nominalPseudoexperiment); // Calculate and prepare central values
  
  
  
  
  
  
  
  
  
  pseudoexperimentsCalculator->allocateMemoryForPseudoExperiments(); // Creates empty histograms and vectors
  
  if(useBootstrapHistos){
    TFile* fBootstrapHistos = new TFile(mainDir+"/" + rootDir +"/" + bootstrapDir + "/" + variable_name + "_" + level + ".root");
    if(fBootstrapHistos->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with bootstrapped histograms from " + mainDir+"/root/bootstrap/" + variable_name + "_" + level + ".root"); 
      return -1;
    }
    pseudoexperimentsCalculator->useBootstrap(useBootstrapHistos); // Activate bootstrapping for calculation of stat and MC stat fluctuations
    pseudoexperimentsCalculator->loadBootstrapHistos(fBootstrapHistos,variable); // Loading bootstrap histos
    const int nBootstrapPseudoexperiments = pseudoexperimentsCalculator->getNBootstrapPseudoexperiments();

    if(nBootstrapPseudoexperiments<nPseudoexperiments){ // if nPseudoexperiments is higher than number of bootstrap replicas the number is set to nReplicas
      cout << "Warning: Number of pseudoexperiments is higher than number of bootstrap replica" << endl;
      cout << "         Number of pseudoexperiments: " << nPseudoexperiments << endl;
      cout << "         Number of bootstrap replica: " << nBootstrapPseudoexperiments << endl;
      cout << "         Using only " << nBootstrapPseudoexperiments << " pseudoexperiments" << endl;
      nPseudoexperiments=nBootstrapPseudoexperiments;
    }
    
  }
  TFile* fPseudoexperiments=0;
  TString path_to_pseudoexperiments_file=mainDir+"/"+rootDir+"/" +pseudoexperimentsDir + "/" + variable_name + "_" + level +".root";
  if(writePseudoexperiments){
    gSystem->mkdir(path_to_pseudoexperiments_file,true);
    fPseudoexperiments = new TFile(path_to_pseudoexperiments_file,"RECREATE");
  }
  
  stringstream ss;
  const string prefix="pseudoexperiment_";
  if(debugLevel > 0) cout << "Producing pseudo-experiments" << endl;
  TStopwatch stopwatch1;
  stopwatch1.Start();
  
  const int nprint = variable.Contains("concatenated") ? 100 : 10000; 
  
  for(int i=0;i<nPseudoexperiments;i++){
    if( (debugLevel>-1) && ( (i+1)%nprint==0) )	{
      double time=stopwatch1.RealTime();
      stopwatch1.Continue();
      time=time/(i+1);
      time= (nPseudoexperiments - i - 1)*time;
      int hours=time/3600;
      int minutes= (time - hours*3600)/60;
      int seconds = time - hours*3600 - minutes*60;
      cout << i+1 << "/" << nPseudoexperiments << " Remaining time: ";
      if( hours > 0) cout << hours << "h";
      if(minutes >0) cout << minutes << "m";
      cout << seconds << "s" << endl;
    }
    ss.str("");
    ss << prefix << i+1;
    pseudoexperimentsCalculator->prepareShiftsForOnePseudoExperiment(); // Relative random shifts for pseudoexperiment are med
    pseudoexperimentsCalculator->preparePseudoexperiment(pseudoexperiment); // Pseudoexperiment at reco level is prepared 
    if(writePseudoexperiments){
      pseudoexperiment->writePseudoexperiment(fPseudoexperiments,ss.str().c_str()); // Pseudoexperiment at reco level is saved if requested
      if((i+1)%1000==0){
	delete fPseudoexperiments;
	fPseudoexperiments = new TFile(path_to_pseudoexperiments_file,"UPDATE");
      }
    }
    covarianceCalculator->addPseudoexperiment(pseudoexperiment,nominalPseudoexperiment); // Pseudoexperiment is propagated to truth level using unfolding procedure
  
  }
  
   
  
  if(useSignalModeling){
    TString pathToSignaModelingFiles=mainDir + "/"+rootDir+"/" + generatorSysDir;
    auto fSignalModeling = std::make_unique<TFile>(pathToSignaModelingFiles + "/" + variable + "_" + level + ".root");
    if(fSignalModeling->IsZombie()) throw TtbarDiffCrossSection_exception("Error: File with signal modeling covariances is a zombie!!!");
    covarianceCalculator->loadSignalModelingCovariances(fSignalModeling.get()); // Signal modelling covariances are loaded
  }
  
  covarianceCalculator->finalize(); // Covariances are finalized - division by npseudoexperiment, subtracting mean values
  covarianceCalculator->printMeanValues(); // Printing relults
  //covarianceCalculator->printCovariances(); // Printing relults
  
  
  
  TString path=mainDir+"/" + texDir + "/final_results";
  covarianceCalculator->printLatexTablesWithFinalResults(path);
  
  TString pathToOutput=mainDir+"/"+rootDir+"/" + outputDir;
  gSystem->mkdir(pathToOutput,true);
  TFile *f =new TFile(pathToOutput + "/" + variable_name + "_" + level + ".root","RECREATE");
  covarianceCalculator->writeCovariances(f); // Covariances are written to output file
  covarianceCalculator->writeCentralValues(f); // Central values are written to output file

  delete f;
  if(debugLevel > -1) cout << "Averaged time required to run 1000 pseudoexperiments: " << stopwatch1.RealTime()/nPseudoexperiments*1000 << " s" << endl;
  if(debugLevel > -1) cout << "All done!" << endl;
  
  return 0;
}
			  

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to propagate uncertainties through unfolding using pseudoexperiments.  

Usage:  
  ProducePseudoexperiments [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --configSysNames <configfile>           Config file with sysnames. [default: TtbarDiffCrossSection/data/sys_names.env]
  --variable <string>                 	  Variable name. [default: total_cross_section]
  --level <string>                        ParticleLevel or PartonLevel. [default: ParticleLevel]
  --inputDir <string>                     Directory with input histograms. [default: systematics_histos]
  --generatorSysDir <string>              Directory with signal modeling covariances. [default: generatorSystematics/genSystematics_using_signal_410471]
  --pseudoexperimentsDir <string>         Directory used to store pseudo-expermients. [default: pseudoexperiments]
  --bootstrapDir <string>                 Directory with signal modeling covariances. [default: bootstrap]
  --outputDir <string>                    Output directory. [default: covariances]
  --writePseudoexperiments <int>          If activated, pseudoexperiments will be saved to file. [default: 0]
  --useBootstrapHistos <int>              Switch to activate boostrap histograms. [default: 0]
  --seed <int>                            Random generator seed. [default: 42]
  --nPseudoexperiments <int>              Number of pseudoexperiments (toys) to run over with. [default: 10000]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {

  TH1::AddDirectory(kFALSE);
  
  try {
    
    TString mainDir("");
    TString path_config_lumi("");
    TString path_config_sysnames("");
    TString variable_name("");
    TString level("");
    TString inputDir("");
    TString generatorSysDir("");
    TString pseudoexperimentsDir("");
    TString bootstrapDir("");
    TString outputDir("");
    bool writePseudoexperiments(0);
    bool useBootstrapHistos(0);
    int seed(0);
    int nPseudoexperiments(0);
    int debugLevel=0;
    
    
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ path_config_sysnames=args["--configSysNames"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configSysNames option is expected to be a string. Check input parameters."); }
		
      try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
      
      try{ inputDir=args["--inputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--inputDir option is expected to be a string. Check input parameters."); }
      
      try{ generatorSysDir=args["--generatorSysDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--generatorSysDir option is expected to be a string. Check input parameters."); }
      
      try{ pseudoexperimentsDir=args["--pseudoexperimentsDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--pseudoexperimentsDir option is expected to be a string. Check input parameters."); }
      
      try{ bootstrapDir=args["--bootstrapDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--bootstrapDir option is expected to be a string. Check input parameters."); }
      
      try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
      try{ writePseudoexperiments=args["--writePseudoexperiments"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--writePseudoexperiments option is expected to be an integer. Check input parameters."); }
      
      try{ useBootstrapHistos=args["--useBootstrapHistos"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--useBootstrapHistos option is expected to be an integer. Check input parameters."); }
      
      try{ seed=args["--seed"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--seed option is expected to be an integer. Check input parameters."); }
      
      try{ nPseudoexperiments=args["--nPseudoexperiments"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--nPseudoexperiments option is expected to be an integer. Check input parameters."); }
      
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    cout << endl << "Running ProducePseudoexperiments for variable " << variable_name << " at " << level  << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
    
    
    return producePseudoExperiments(mainDir, path_config_lumi, path_config_sysnames, variable_name, level, 
                                    inputDir, generatorSysDir, pseudoexperimentsDir, bootstrapDir, outputDir, writePseudoexperiments,
				    useBootstrapHistos, seed, nPseudoexperiments,debugLevel);
    
    
    
    
	  
  } catch(const TtbarDiffCrossSection_exception& l) {l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  return 0;
}

