// Peter Berta, 15.10.2015

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/stringFunctions.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <memory>
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"
#include "TPaveText.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to make reco level plots from 2D histograms stored in systematics_histos rootfiles.  

Usage:  
  drawRecoND [options]
  
Options:
  -h --help                                      Show help screen.
  --mainDir <mainDir>             	         Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	         Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --configBinning <configBinning>                Path to config file with binning. [default: TtbarDiffCrossSection/data/optBinningND/t1_y_vs_t1_pt.env]
  --multiDimensionalPlotsConfig <string>         Path to config file with plots settings. [default: TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env]
  --systematicsHistosDir <systematicsHistosDir>  Directory with systematic histos rootfiles. [default: systematics_histosND]
  --outputDir <outputDir>                        Directory to store plots. Will be created in maindir/pdf.<MC> . [default: RecoPlotsND]
  --variable <string>                  	         Variable name. [default: t1_y_vs_t1_pt]
  --level <string>                               ParticleLevel or PartonLevel. Option is used just to identify input rootfile. [default: ParticleLevel]
  --normalizeSignal <normalizeSignal>            Option to normalize Signal to (Data - Background). [default: 1]
  --drawRatio <drawRatio>                        Option to draw ratio plot in second pad. [default: 1]
  --useLogScale <useLogScale>                    Option to use log scale on y-axis. [default: 1]
  --debugLevel <int>                             Option to control debug messages. Higher value means more messages. [default: 0]
)";

int drawRecoND(TString mainDir,TString systematicHistosPath,TString outputSubdir,TString path_config_lumi,TString confing_binning,TString pathMultiDimensionalPlotsConfig,TString variable_name,TString level,bool normalizeSignal,bool drawRatio, bool useLogScale,int debugLevel);

int main(int nArg, char **arg) {

  TH1::AddDirectory(0);
  
  TString mainDir="";
  TString systematicHistosPath="";
  TString outputSubdir="";
  TString path_config_lumi="";
  TString path_config_binning="";
  TString pathMultiDimensionalPlotsConfig="";
  TString variable_name="";
  TString level="";
  bool useLogScale=0;
  bool drawRatio=0;
  bool normalizeSignal=0;
  int debugLevel=0;
  
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
  try{
    
    try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
      
    try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      
    try{ path_config_binning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
      
    try{ pathMultiDimensionalPlotsConfig=args["--multiDimensionalPlotsConfig"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--multiDimensionalPlotsConfig option is expected to be a string. Check input parameters."); }
      
    try{ systematicHistosPath=args["--systematicsHistosDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--systematicsHistosDir option is expected to be a string. Check input parameters."); }
      
    try{ outputSubdir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
    try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
      
    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
      
    try{ normalizeSignal=args["--normalizeSignal"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--normalizeSignal option is expected to be an integer. Check input parameters."); }
      
    try{ useLogScale=args["--useLogScale"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--useLogScale option is expected to be an integer. Check input parameters."); }
      
    try{ drawRatio=args["--drawRatio"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--drawRatio option is expected to be an integer. Check input parameters."); }
     
    try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
      
  } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
  
  if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
  
  if(debugLevel>0){
    cout << endl << "Running drawRecoND for variable " << variable_name << "." << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
  }
  
  return drawRecoND(mainDir,systematicHistosPath,outputSubdir,path_config_lumi,path_config_binning,pathMultiDimensionalPlotsConfig,variable_name,level,normalizeSignal,drawRatio, useLogScale,debugLevel);
  
}

int drawRecoND(TString mainDir,TString systematicHistosPath,TString outputSubdir,TString path_config_lumi,TString config_binning,TString pathMultiDimensionalPlotsConfig,TString variable_name,TString level,bool normalizeSignal,bool drawRatio,bool useLogScale,int debugLevel){
  try {
    
    SetAtlasStyle();
    TH1::AddDirectory(kFALSE);
    
    auto configlumi = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
    TString mc_samples_production = configlumi->mc_samples_production();
    TString lumi_string = configFunctions::getLumiString(*configlumi);
    
    TString filename=mainDir + "/root." + mc_samples_production + "/" + systematicHistosPath + "/" + variable_name + "_" + level + ".root";
    TFile* file_systematics= new TFile(filename);
    if(file_systematics->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " +filename); 
      return -1;
    }
    
    TString levelShort = level;
    levelShort.ReplaceAll("Level","");
    auto histogramConverterND = std::make_unique<HistogramNDto1DConverter>(config_binning.Data(),debugLevel);
    histogramConverterND->initializeOptBinning((variable_name+"_"+levelShort+"_reco").Data());
    histogramConverterND->initialize1DBinning();
    
    
    // Loading histograms 
    
    TH1D* h_data=(TH1D*)file_systematics->Get("nominal/" + variable_name + "_data");
    vector<TH1D*> histos;
    vector<TString> names;
    vector<int> colors;
    histos.push_back((TH1D*)file_systematics->Get("nominal/" + variable_name + "_bkg_Multijet"));names.push_back("Multijet");colors.push_back(619);
    histos.push_back((TH1D*)file_systematics->Get("nominal/MCBackgrounds/" + variable_name + "_ttbar_nonallhad"));names.push_back("t#bar{t} Non All Hadr");colors.push_back(432);
    histos.push_back((TH1D*)file_systematics->Get("nominal/MCBackgrounds/" + variable_name + "_ttHWZ"));names.push_back("t#bar{t}+W/Z/H");colors.push_back(800);
    
    {// Adding single-top: Wt channel + t-channel
      auto helpvec = std::unique_ptr<TH1D>{(TH1D*)file_systematics->Get("nominal/MCBackgrounds/" + variable_name + "Wt_singletop")};
      TH1D* h = (TH1D*)file_systematics->Get("nominal/MCBackgrounds/" + variable_name + "tChannel_singletop");
      h->Add(helpvec.get());
      histos.push_back(h);names.push_back("Single top");colors.push_back(600);
    }
    //histos.push_back((TH1D*)file_systematics->Get("nominal/MCBackgrounds/" + variable_name + "Wt_singletop"));names.push_back("Wt single top");colors.push_back(600);
    //histos.push_back((TH1D*)file_systematics->Get("nominal/MCBackgrounds/" + variable_name + "tChannel_singletop"));names.push_back("single top t-channel");colors.push_back(kGreen+3);
    histos.push_back((TH1D*)file_systematics->Get("nominal/" + variable_name + "_signal_reco"));names.push_back("t#bar{t} All Hadr");colors.push_back(0); // Signal must be last
    
    TH1D* h_pseudodata=(TH1D*)file_systematics->Get("nominal/" + variable_name + "_pseudodata")->Clone();
    h_pseudodata->Reset();
    
    const int nhistos=histos.size();
    
    std::unique_ptr<TLegend> leg;
    
    if (drawRatio) {
      leg = std::make_unique<TLegend>(0.52,0.66,0.92,0.93);
      leg->SetTextSize(0.039);
    }
    else {
      leg = std::make_unique<TLegend>(0.45,0.66,0.92,0.93);
      leg->SetTextSize(0.035);
    }
    leg->SetTextFont(42);
    leg->SetFillColor(0);
    leg->SetBorderSize(0);
    leg->SetNColumns(2);
    
    //double nEvents_dataD=h_data->Integral();
    leg->AddEntry(h_data,"Data");
    
    
    functions::DivideByBinWidth(h_data);
    
    double sumEventsBkg=0.;
    double factor=1.0;
    TString factorStr{""};
    for(int i=0;i<nhistos;i++) {
      double nEventsD=histos[i]->Integral();
      if(i<nhistos-1)sumEventsBkg+=nEventsD;
      
      if(normalizeSignal && (i==nhistos-1) ){
	//double factor=(nEvents_dataD - sumEventsBkg)/ nEventsD;
	factor=0.83;
	factorStr=Form("%.2f",factor);
	histos[i]->Scale(factor);
      }
      
      functions::DivideByBinWidth(histos[i]);
      histos[i]->SetFillColor(colors[i]);
      
      if(i!=nhistos-1)histos[i]->SetLineColor(colors[i]);
      h_pseudodata->Add(histos[i]);
    }
    
    for(int i=nhistos-1;i>=0;i--) {
      
      if(normalizeSignal && (i==nhistos-1) ){
	//double factor=(nEvents_dataD - sumEventsBkg)/ nEventsD;
	factor=0.83;
	factorStr=Form("%.2f",factor);
      }
      if(normalizeSignal && (i==nhistos-1)) leg->AddEntry(histos[i],names[i] +" #color[4]{#times"+factorStr+"}","F");
      else leg->AddEntry(histos[i],names[i],"F");
      
    }
   
   
    TGraphAsymmErrors* graph_unc=new TGraphAsymmErrors(h_pseudodata);
    TGraphAsymmErrors* graph_stat_unc=new TGraphAsymmErrors(h_pseudodata);
    
    
    functions::calculateSystematicUncertainties(graph_unc,file_systematics,variable_name); // Get relative detector systematic errors
    const int nbins=h_pseudodata->GetNbinsX();
    for(int i=0;i<nbins;i++){
      int ibin=i+1;
      double bincontent = h_pseudodata->GetBinContent(ibin);
      graph_unc->SetPointEYhigh( i, sqrt( pow(graph_unc->GetErrorYhigh(i)*bincontent,2) + pow(graph_stat_unc->GetErrorYhigh(i),2) ) );
      graph_unc->SetPointEYlow(  i, sqrt( pow(graph_unc->GetErrorYlow(i)*bincontent, 2) + pow(graph_stat_unc->GetErrorYlow(i), 2) ) );
      
      double x,y;
      graph_unc->GetPoint(i,x,y);
      
      if (debugLevel>1) cout << "bin: " << ibin << " bincontent: " << bincontent << " center point: " << y << " rel. stat. unc: " << graph_stat_unc->GetErrorYhigh(i)/bincontent << " rel. combine unc.:  " << graph_unc->GetErrorYhigh(i)/bincontent << endl;
      
    }
    
    graph_unc->SetFillStyle(1001);
    graph_unc->SetFillColor(kGray+1);
    graph_unc->SetLineColor(kGray+1);
    graph_stat_unc->SetFillStyle(1001);
    graph_stat_unc->SetFillColor(kGray);
    graph_stat_unc->SetLineColor(kGray);
    
    std::vector<TGraphAsymmErrors*> graph_unc_proj = histogramConverterND->makeProjections(graph_unc,"unc");
    std::vector<TGraphAsymmErrors*> graph_stat_unc_proj = histogramConverterND->makeProjections(graph_stat_unc,"unc_stat");
    
    leg->AddEntry(graph_stat_unc,"MC Stat. Unc.","F");
    leg->AddEntry(graph_unc,"MC Stat.#oplus Det. Syst. Unc.","F");
    
    
    vector<vector<TH1D*>> projections(nhistos);
    
    vector<TH1D*> data_projections = histogramConverterND->makeProjections(h_data,"data"); // Make projections from concatenated histogram
    vector<TH1D*> pseudodata_projections = histogramConverterND->makeProjections(h_pseudodata,"pseudodata"); // Make projections from concatenated histogram
    for (int i=0;i<nhistos;i++) {
      projections[i] = histogramConverterND->makeProjections(histos[i],((std::string)"MC_"+functions::intToString(i+1)).c_str());
    }
    
   
    const int nprojections = data_projections.size();
    std::vector<TH1D*> ratios_projections(nprojections);
    std::vector<TGraphAsymmErrors*> unit_graph_proj(nprojections);
    std::vector<TGraphAsymmErrors*> unit_graph_stat_proj(nprojections);

    for (int iproj=0;iproj<nprojections;iproj++) {
      ratios_projections[iproj] = (TH1D*)data_projections[iproj]->Clone();
      ratios_projections[iproj]->Divide(pseudodata_projections[iproj]);
      unit_graph_proj[iproj] = functions::getUnitGraph(graph_unc_proj[iproj]);
      unit_graph_stat_proj[iproj]= functions::getUnitGraph(graph_stat_unc_proj[iproj]);
    }
   
    
   
    TString pdfdir=mainDir + "/pdf."+ mc_samples_production + "/" + outputSubdir + "/" + variable_name;
    TString pngdir=mainDir + "/png."+ mc_samples_production + "/" + outputSubdir + "/" + variable_name;
    
    gSystem->mkdir(pdfdir+ "/projections",true);
    gSystem->mkdir(pngdir+ "/projections",true);
    
    std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable_name);
    TString diffVariable = NamesAndTitles::getDifferentialVariableLatex(variable_name);
    TString inverseUnit = NamesAndTitles::getInverseUnit(variable_name);
    if(inverseUnit!="") diffVariable = (TString)"#frac{dEvents}{" +diffVariable+"}"+ " [" + inverseUnit +"]";
    else diffVariable = (TString)"#frac{dEvents}{" +diffVariable+"}";
    
    for (int iproj=0;iproj<nprojections;iproj++) {
      TH1D* data = data_projections[iproj];
      if(xtitles.size()>0) data->GetXaxis()->SetTitle(xtitles.back());
      data->GetYaxis()->SetTitle(diffVariable);
      
      vector<TH1D*> mc_histos(nhistos);
      for(int iMC=0;iMC<nhistos;iMC++) {
	mc_histos[iMC] = projections[iMC][iproj];
	//std::cout << "iMC " << iMC << " color: " << colors[iMC] << std::endl;
	mc_histos[iMC]->SetFillColor(colors[iMC]);
	if(iMC!=nhistos-1) mc_histos[iMC]->SetLineColor(colors[iMC]);
	else mc_histos[iMC]->SetLineColor(1);
      }
    
      std::vector<TString> info1 = { NamesAndTitles::ATLASString(lumi_string),
				     NamesAndTitles::energyLumiString(lumi_string),
				     NamesAndTitles::analysisLabel(),
				     "Signal region"};
      
      std::vector<TString> info2 = histogramConverterND->getExternalBinsInfo(iproj);
      
      std::vector<TString> info;
      functions::joinVectors(info1,info2,info);
      
      std::string projectionName = histogramConverterND->getProjectionName(iproj);

      if(useLogScale){
	functions::drawStackHistos(pdfdir + "/projections" + "/" + projectionName + "_log.pdf", data, mc_histos, graph_stat_unc_proj[iproj], graph_unc_proj[iproj], info, leg.get(), drawRatio, useLogScale);
	functions::drawStackHistos(pngdir + "/projections" + "/" + projectionName + "_log.png", data, mc_histos, graph_stat_unc_proj[iproj], graph_unc_proj[iproj], info, leg.get(), drawRatio, useLogScale);
      }
      else {
	functions::drawStackHistos(pdfdir + "/projections" + "/" + projectionName + ".pdf", data, mc_histos, graph_stat_unc_proj[iproj], graph_unc_proj[iproj], info, leg.get(), drawRatio, useLogScale);
	functions::drawStackHistos(pngdir + "/projections" + "/" + projectionName + ".png", data, mc_histos, graph_stat_unc_proj[iproj], graph_unc_proj[iproj], info, leg.get(), drawRatio, useLogScale);
      }
      
    }
    
    auto plotSettings = std::make_unique<MultiDimensionalPlotsSettings>(pathMultiDimensionalPlotsConfig.Data());
    
    int nPerLine(0),nlines(0);
    functions::getGridSize(nprojections,nlines,nPerLine);
  
    const double canvas_xsize = (1000./3) * nPerLine;
    const double canvas_ysize = 175.*(nlines+1);
    std::vector<TString> generalInfo = { NamesAndTitles::ATLASString(lumi_string),
				     NamesAndTitles::energyLumiString(lumi_string),
				     NamesAndTitles::analysisLabel(),
				     "Signal region"};
    
    
    plotSettings->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, generalInfo.size());
    plotSettings->print();

    auto can = std::unique_ptr<TCanvas>(plotSettings->makeCanvas("can"));
    auto canRatio = std::unique_ptr<TCanvas>(plotSettings->makeCanvas("canRatio"));
    can->cd();
    vector<vector<shared_ptr<TPad>>> pads = plotSettings->makeArrayOfPads(nprojections);
    canRatio->cd();
    vector<vector<shared_ptr<TPad>>> padsRatios = plotSettings->makeArrayOfPads(nprojections);
    vector<unique_ptr<THStack> > stacks(nprojections);
    
    
    int iproj=0;
    for(int iline=0;iline<nlines;iline++) {
      int nInLine = pads[iline].size();
      
      for(int iInLine=0;iInLine<nInLine;iInLine++) {

	TPad* currentPad = pads[iline][iInLine].get();
	TPad* currentPadRatios = padsRatios[iline][iInLine].get();
	
	TH1D* data = data_projections[iproj];
	TH1D* pseudodata = pseudodata_projections[iproj];
	TH1D* ratio = ratios_projections[iproj];
	
	//const int ndivisions=6;
	
	plotSettings->setHistLabelStyle(data);
	plotSettings->setHistLabelStyle(ratio);
	
	// Selecting where to put y-axis label
	if(iInLine ==0) { 
	  data->GetYaxis()->SetTitle(diffVariable);
	}
	else {
	  data->GetYaxis()->SetTitle("");
	}
	
	// Selecting where to put x-axis label
	//if(iline != nlines-1) { 
	if(false) { 
	  data->GetXaxis()->SetTitle("");
	}
	
	vector<TH1D*> mc_histos(nhistos);
	for(int iMC=0;iMC<nhistos;iMC++) {
	  mc_histos[iMC] = projections[iMC][iproj];
	}
	
	stacks[iproj] = std::unique_ptr<THStack>{functions::makeTHStack(Form("stack_%i",iproj),mc_histos)};
	currentPad->SetLogy(useLogScale);
	
	std::vector<TString> externalBinInfo = histogramConverterND->getExternalBinsInfo(iproj);
	
	double max = std::max(functions::getMaximum(data),functions::getMaximum(pseudodata));
	double min = functions::getMinimum(mc_histos[0]);
	const double upSpace=0.37*(1.+(externalBinInfo.size()-1)*1.5);
	const double bottomSpace=0.05;
	
	if(useLogScale){
	  max = max*(1+pow(max/min,upSpace));
	  min = min/(1+pow(max/min,bottomSpace));
	  //ymin = ymin/2;
	}
	else{
	  max= max + (max-min)*upSpace;
	  min= min - (max-min)*bottomSpace;
	}
	
	TPaveText* paveText = functions::makePaveText(externalBinInfo, plotSettings->textSizePad(), 0.35, 0.75, 0.93); // xmin,xmax,ymax
	paveText->SetFillStyle(0);
	
	data->GetYaxis()->SetRangeUser(min,max);
	
	functions::drawStackHistos(currentPad, data, pseudodata, stacks[iproj].get(), {graph_unc_proj[iproj],graph_stat_unc_proj[iproj]});
	paveText->Draw();
	currentPad->RedrawAxis("g"); 
	currentPad->RedrawAxis(); 
	
	
	if(externalBinInfo.size() > 1) ratio->GetYaxis()->SetRangeUser(0.61,1.69);
	else ratio->GetYaxis()->SetRangeUser(0.77,1.29);
	ratio->GetYaxis()->SetNdivisions(6);
	
	// Selecting where to put y-axis label
	if(iInLine == 0) {
	  ratios_projections[iproj]->GetYaxis()->SetTitle("Data / Pred.");
	}
	// Selecting where to put x-axis label
	//if(iline == nlines-1) {
	if(true) {
	  ratios_projections[iproj]->GetXaxis()->SetTitle(xtitles.back());
	}
	
	functions::plotHistogram(currentPadRatios, ratios_projections[iproj], {unit_graph_proj[iproj],unit_graph_stat_proj[iproj]}, "");
	paveText->Draw();
	currentPadRatios->RedrawAxis("g"); 
	currentPadRatios->RedrawAxis();
	
	iproj++;
      }
    }
    
    TString plotName = variable_name;
    if(useLogScale) plotName+="_log";
    
    can->cd();
    
    TPaveText* paveTextGeneralInfo = plotSettings->makePaveText(generalInfo,0.14,0.265);    
    
    plotSettings->setLegendAttributes(leg.get(),/*xmin=*/0.37);
    leg->SetNColumns(3);
    leg->Draw();
    paveTextGeneralInfo->Draw();
    
    can->SaveAs(pdfdir+"/"+plotName+".pdf");
    can->SaveAs(pngdir+"/"+plotName+".png");
   
    canRatio->cd();
    auto legRatio = std::unique_ptr<TLegend>(plotSettings->makeLegend(0.65,0.98));
    legRatio->SetNColumns(1);
    legRatio->AddEntry(h_data,"data");
    legRatio->AddEntry(graph_stat_unc,"MC Stat. Unc.","F");
    legRatio->AddEntry(graph_unc,"MC Stat. #oplus Det. Syst. Unc.","F");

    legRatio->Draw();
    paveTextGeneralInfo = plotSettings->makePaveText(generalInfo);

    paveTextGeneralInfo->Draw();
    canRatio->SaveAs(pdfdir+"/" +variable_name + "_ratio.pdf");
    canRatio->SaveAs(pngdir+"/" +variable_name + "_ratio.png");
    
    file_systematics->Close();
    
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  cout << "drawRecoND: Done." << endl;
  return 0;
}


