
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "CovarianceCalculator/SystematicHistos.h"
#include "CovarianceCalculator/AsymmetricErrorsCalculator.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TEnv.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;

int calculateAsymmetricErrors( const TString& mainDir,
                               const TString& path_config_lumi,
			       const TString& path_config_binning,
			       const TString& pathMultiDimensionalPlotsConfig,
                               const TString& path_config_sysnames,
                               const TString& variable_name,
                               const TString& level,
			       const TString& plotsDir,
			       const TString& systematicUncertaintiesDir,
			       const TString& covariancesDir,
                               const TString& systematicHistosDir,
			       const TString& dimension,
			       int debugLevel) {
				 
  SetAtlasStyle();
				 
  bool is1D=true;
  if(dimension!="1D") is1D=false;
    
  auto ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
  
  TString mc_samples_production = ttbarConfig->mc_samples_production();
  
  TString rootDir="root."+mc_samples_production;
  
  TFile* file_systematics= new TFile(mainDir+"/root."+mc_samples_production+"/"+systematicHistosDir+"/" + variable_name + "_" + level + ".root");
  if(file_systematics->IsZombie()) {
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/root."+mc_samples_production+"/"+systematicHistosDir+"/" + variable_name + "_" + level + ".root"); 
    return -1;
  }
  
  TString texDir=mainDir + "/tex."+ mc_samples_production +"/"+systematicUncertaintiesDir;
  
  gSystem->mkdir(texDir,true);
  ofstream textfileSysFullAbsolute((texDir+"/Table_" + level + "_" + variable_name + "_Absolute.tex" ).Data());
  ofstream textfileSysFullNormalized((texDir+"/Table_" + level + "_" + variable_name + "_Normalized.tex" ).Data());
  ofstream textfileSysSummaryAbsolute((texDir+"/SummaryTable_" + level + "_" + variable_name + "_Absolute.tex" ).Data());
  ofstream textfileSysSummaryNormalized((texDir+"/SummaryTable_" + level + "_" + variable_name + "_Normalized.tex" ).Data());
  
  SystematicHistos* sysHistos= new SystematicHistos(file_systematics,variable_name,*ttbarConfig);
  sysHistos->prepareRelativeShifts();
  AsymmetricErrorsCalculator* asymErrCalculator = new AsymmetricErrorsCalculator(sysHistos,path_config_lumi,path_config_sysnames,mainDir,variable_name,level,debugLevel,is1D);
  
  TFile* fCovariances = new TFile(mainDir + "/root."+mc_samples_production+"/"+covariancesDir+"/" + variable_name + "_" + level + ".root" );
  if(fCovariances->IsZombie())throw TtbarDiffCrossSection_exception("Error: file with covariances and central values is not loaded");
  asymErrCalculator->loadStatisticalUncertainties(fCovariances);
  asymErrCalculator->prepareSysUncertainties();
  
  if(!is1D){
    asymErrCalculator->loadNDbinning(path_config_binning);
    asymErrCalculator->loadMultiDPlotsSettings(pathMultiDimensionalPlotsConfig);
  }
  
  asymErrCalculator->printDetailedTablesOfUncertainties(textfileSysFullAbsolute,textfileSysFullNormalized);
  asymErrCalculator->printSummaryTables(textfileSysSummaryAbsolute,textfileSysSummaryNormalized);
  
  
  asymErrCalculator->plotUncertainties(plotsDir);
  
  cout << "Everything ok!" << endl;
  return 0;
}

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to calculate asymmetric errors. It creates plots and tables with uncertainties.  

Usage:  
  CalculateAsymmetricErrors [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>             	  Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	  Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --configBinning <configBinning>         Path to file with binning. [default: TtbarDiffCrossSection/data/optBinningND/ttbar_y_vs_ttbar_mass_vs_t1_pt.env]
  --multiDimensionalPlotsConfig <string>         Path to config file with plots settings. [default: TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env]
  --configSysNames <configfile>           Config file with sysnames. [default: TtbarDiffCrossSection/data/sys_names.env]
  --variable <string>                 	  Variable name. [default: total_cross_section]
  --level <string>                        ParticleLevel or PartonLevel. [default: ParticleLevel]
  --plotsDir <string>                     Directory to store plots. [default: sys_errors_summary_plots]
  --systematicUncertaintiesDir <string>   Directory to store latex tables with uncertainties. [default: systematicUncertainties]
  --covariancesDir <string>               Directory with covariance matrices from pseudo-experiments. [default: covariances]
  --systematicHistosDir <string>          Relative path to output directory from mainDir/root.<mc_samples_production>/ [default: systematics_histos]
  --dimension <string>                    Dimension of distributions. [default: 1D]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";



int main(int nArg, char **arg) {
  TH1::AddDirectory(kFALSE);
  try {
    
    TString mainDir("");
    TString path_config_lumi("");
    TString path_config_binning("");
    TString pathMultiDimensionalPlotsConfig="";
    TString path_config_sysnames("");
    TString variable_name("");
    TString level("");
    TString plotsDir("");
    TString systematicUncertaintiesDir("");
    TString covariancesDir("");
    TString systematicHistosDir("");
    TString dimension=("");
    int debugLevel=0;
    
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
    
    try{
  
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
	
      try{ path_config_binning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
	
      try{ pathMultiDimensionalPlotsConfig=args["--multiDimensionalPlotsConfig"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--multiDimensionalPlotsConfig option is expected to be a string. Check input parameters."); }
      
      try{ path_config_sysnames=args["--configSysNames"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configSysNames option is expected to be a string. Check input parameters."); }
	
      try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
      
      try{ plotsDir=args["--plotsDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--plotsDir option is expected to be a string. Check input parameters."); }
      
      try{ systematicUncertaintiesDir=args["--systematicUncertaintiesDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--systematicUncertaintiesDir option is expected to be a string. Check input parameters."); }
      
      try{ covariancesDir=args["--covariancesDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--covariancesDir option is expected to be a string. Check input parameters."); }
      
      try{ systematicHistosDir=args["--systematicHistosDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--systematicHistosDir option is expected to be a string. Check input parameters."); }
      
      try{ dimension=args["--dimension"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--dimension option is expected to be a string. Check input parameters."); }
      
      try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
    
    if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
    cout << endl << "Running CalculateAsymmetricErrors for variable " << variable_name << " at " << level  << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
    
    return calculateAsymmetricErrors(mainDir, path_config_lumi, path_config_binning, pathMultiDimensionalPlotsConfig, path_config_sysnames, variable_name, level, plotsDir, systematicUncertaintiesDir, covariancesDir, systematicHistosDir, dimension, debugLevel);
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  
  return 0;
}
