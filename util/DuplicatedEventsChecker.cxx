// Peter Berta, 15.10.2015

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>
#include <tuple>
 
#include "TString.h" 
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"

#include "docopt.h"

using namespace std;

int checkForDuplicatedEvents(const TString& mainDir,const TString& fileListsDir,const TString& sampleName,const TString& fileListName, const TString& treeName,const TString& physicsVariables, int debugLevel);

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to check if there are duplicated events in the sample. 

Usage:  
  DuplicatedEventsChecker [options]
  
Options:
  -h --help                               Show help screen.
  --mainDir <mainDir>                     Directory with all outputs. [default: $TtbarDiffCrossSection_output_path]
  --sampleName <sampleName>               Sample name.  [default: MC16e.410471.PhPy8EG]
  --fileListsDir <filelistsDir>           Directory where we store filelists.  [default: TtbarDiffCrossSection/filelists]
  --fileListName <filelistName>           Name of the file with list of files.  [default: allhad.boosted.output.txt]
  --treeName <treeName>                   Tree name to check for duplicated events. [default: truth]
  --physicsVariables <varibles>           Physics variables to check if events are identival or not. [default: filter_HT]
  --debugLevel <int>                      Option to control debug messages. Higher value means more messages. [default: 0]
)";



int main(int nArg, char **arg) {

  cout << endl << endl << " ***** running  " << arg[0] << " ***** " << endl << endl;

  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true);  // Using docopt package to read parameters

  cout << "Reading parameters." << endl;  
  
  TString mainDir("");
  TString sampleName("");
  TString fileListsDir("");
  TString fileListName("");
  TString treeName("");
  TString physicsVariables(0);
  int  debugLevel(0);
  
  try{
  
    
  
    try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDire option is expected to be a string. Check input parameters."); }
      
    try{ sampleName=args["--sampleName"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--sampleName option is expected to be a string. Check input parameters."); }
      
    try{ fileListsDir=args["--fileListsDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--fileListDir option is expected to be a string. Check input parameters."); }
      
    try{ fileListName=args["--fileListName"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--fileListName option is expected to be a string. Check input parameters."); }
      
    try{ treeName=args["--treeName"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--treeName option is expected to be a string. Check input parameters."); }
     
    try{ physicsVariables=args["--physicsVariables"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--physicsVariables option is expected to be a string list. Check input parameters."); }
  
    try{ debugLevel = args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel is not an integer. Check input parameters."); }
  
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }  

  
  if(mainDir.Contains("$TtbarDiffCrossSection_output_path")) mainDir.ReplaceAll("$TtbarDiffCrossSection_output_path",gSystem->Getenv("TtbarDiffCrossSection_output_path"));

  return checkForDuplicatedEvents(mainDir,fileListsDir,sampleName,fileListName  , treeName, physicsVariables, debugLevel);
}

int checkForDuplicatedEvents(const TString& mainDir,const TString& fileListsDir,const TString& sampleName,const TString& fileListName, const TString& treeName,const TString& physicsVariables, int debugLevel){
  cout << "Checking for duplicate events." << endl;
  vector<TString> physicsVariablesVec= functions::MakeVectorTString(physicsVariables.Data());
  const int nphysicsVariables=physicsVariablesVec.size();
  
  TString filelist = fileListsDir + "/" + sampleName + "/" + fileListName;
  
  if(debugLevel>0){
    cout << "Input filelist: " << filelist << endl;
    cout << "Checking in tree: " << treeName << endl;
    cout << "debugLevel: " << debugLevel << endl;
    cout << "physicsVariables:";
    for(int i=0;i<nphysicsVariables;i++) cout << " " << physicsVariablesVec[i];
    cout << endl;
  }
  
  TString outputDir = mainDir+"/checkDuplicatedEvents/"+sampleName;
  gSystem->mkdir(outputDir,true);
  
  ofstream duplicatedEventsTextfile(outputDir+"/duplicatedEvents.txt");
  ofstream duplicatedEventNumbersTextfile(outputDir+"/duplicatedEventNumbers.txt");
  
  vector<TString> fileNames;
  TString pathToFilelists = (TString)gSystem->Getenv("TTBAR_PATH");
  
  ifstream mystream( (pathToFilelists + "/" + filelist).Data());
  if (debugLevel>0) cout << "Reading filelist " << filelist << endl;
  TString str_item;
  while ( mystream >> str_item) {
    if (debugLevel>1) cout << "Adding file " << str_item.Data() << "  to the list of files" << endl;
    fileNames.push_back(str_item.Data());
  }
  mystream.close();
  const int nfiles=fileNames.size();
  
  
  auto chain = std::make_shared<TChain>("chain");
  vector<TFile*> files(nfiles);
  
  for(int i=0;i<nfiles;i++){
    
    
    TString filename = fileNames[i];
    if (debugLevel>0) cout << "Opening file " << filename << endl;
    files[i] = TFile::Open(filename);
    if (files[i]->IsZombie()) {
      cout << "Error: file is Zombie!" << endl; 
      cout << "Exiting now!" << endl;
      exit(EXIT_FAILURE);
    }
    
    if (debugLevel>0) cout << "Adding file to the chain." << endl;
    chain->AddFile(filename, TChain::kBigNumber, treeName);
    
  }
   if (debugLevel>0) cout << "All input files added to chain." << endl;
   
   
  bool isData=filelist.Contains("AllYear.physics_Main");
  TString runNumberBranchName = "runNumber";
  
  TBranch* b_eventNumber;
  TBranch* b_runNumber;
  vector<TBranch*> b_physValues(nphysicsVariables);
  
  ULong64_t eventNumber=0;
  UInt_t runNumber=0;
  vector<float> physValues(nphysicsVariables);
  
  chain->SetBranchAddress("eventNumber",&eventNumber,&b_eventNumber);
  chain->SetBranchAddress(runNumberBranchName,&runNumber,&b_runNumber);
  for(int i=0;i<nphysicsVariables;i++)chain->SetBranchAddress(physicsVariablesVec[i],&physValues[i],&b_physValues[i]);
  
  const unsigned long long nEntries = chain->GetEntries();
  
  
  
  std::map< std::tuple<unsigned long long,unsigned long,vector<float> >,unsigned long > indexes;
  //std::map< std::tuple<unsigned long long,vector<float> >,unsigned long > indexes;
  std::map< unsigned long long,unsigned long long> indexes_eventNumbers;
  
  std::map < unsigned long long, std::vector< unsigned long long > > duplicatedEvents;
  std::map < unsigned long long, std::vector< unsigned long long > > duplicatedEventNumbers;
  
  
  const int nPrint = 100000;
  for(unsigned long long iEntry=0;iEntry<nEntries;iEntry++){
    if(iEntry % nPrint == 0 ) {
      TString perCents=Form("%4.2f",(float)iEntry/nEntries*100);
      cout << "Processing entry: " << iEntry << "/" << nEntries << " (" << perCents << "%)"<< endl;
    
    }
    
    //if(iEntry < 60000000) continue;
    
    unsigned long long localEntry = chain->LoadTree (iEntry);
    
    b_eventNumber->GetEntry(localEntry);
    b_runNumber->GetEntry(localEntry);
    for(int i=0;i<nphysicsVariables;i++)b_physValues[i]->GetEntry(localEntry);
    
    std::tuple<unsigned long long,unsigned long,vector<float> > myTuple(eventNumber,runNumber,physValues);
    //std::tuple<unsigned long long,vector<float> > myTuple(eventNumber,physValues);
    
    
    
    if(!isData){
      if ( indexes_eventNumbers.find(eventNumber) != indexes_eventNumbers.end() ) {
	
	if( indexes.find(myTuple) == indexes.end() ) {
	  
	  if (debugLevel>0)cout << "Found event with same event number which is not duplicate. eventNumber: " << eventNumber << " original entry: " << indexes_eventNumbers[eventNumber] << " Duplicated entry: " << iEntry << endl; 
	
	  if( duplicatedEventNumbers.find(eventNumber) == duplicatedEventNumbers.end() ) duplicatedEventNumbers[eventNumber].push_back(indexes_eventNumbers[eventNumber]);
	  duplicatedEventNumbers[eventNumber].push_back(iEntry);
	}
      }
      else {
	indexes_eventNumbers[eventNumber]=iEntry;
      }
    }
    
    if( indexes.find(myTuple) != indexes.end() ) {
      //if (debugLevel>0)cout << "Found duplicated event. eventNumber: " << eventNumber << " original entry: " << indexes[myTuple] << " Duplicated entry: " << iEntry << endl;
      if (debugLevel>0)cout << "Found duplicated event. eventNumber: " << eventNumber << " runNumber: " << runNumber << " original entry: " << indexes[myTuple] << " Duplicated entry: " << iEntry << endl;
      
      if( duplicatedEvents.find(eventNumber) == duplicatedEvents.end() ) duplicatedEvents[eventNumber].push_back(indexes[myTuple]);
      duplicatedEvents[eventNumber].push_back(iEntry);
  
    }
    else {
      indexes[myTuple] = iEntry;
    }
    
    
    
    
  }
  cout << endl << "--------------------------------------------" << endl;
  cout << "Printing duplicated events to " << outputDir+"/duplicatedEvents.txt" << endl;
  
  for (auto it = duplicatedEvents.begin(); it!=duplicatedEvents.end();it++){
    
    unsigned long long evtNumber = it->first;
    std::vector< unsigned long long > vec = it->second;
    const int size=vec.size();
    
    duplicatedEventsTextfile << "eventNumber: " << evtNumber;
    for(int i=0;i<size;i++){
      duplicatedEventsTextfile << " index" << i+1 << ": " << vec[i];
    }
    duplicatedEventsTextfile << endl;
  }
  
  duplicatedEventsTextfile << "Total number of duplicated events: " << duplicatedEvents.size() << ". Number of events checked: " << nEntries << endl;
  cout << "Total number of duplicated events: " << duplicatedEvents.size() << ". Number of events checked: " << nEntries << endl;
  
  
  if(!isData) {
    cout << endl << "--------------------------------------------" << endl;
    cout << "Printing events with the same event numbers which are not duplicates into " << outputDir+"/duplicatedEventNumbers.txt" << endl;
    
    for (auto it = duplicatedEventNumbers.begin(); it!=duplicatedEventNumbers.end();it++){
      unsigned long long evtNumber = it->first;
      std::vector< unsigned long long > vec = it->second;
      
      const int size=vec.size();
      
      duplicatedEventNumbersTextfile << "eventNumber: " << evtNumber;
      for(int i=0;i<size;i++){
	duplicatedEventNumbersTextfile << " index" << i+1 << ": " << vec[i];
      }
      duplicatedEventNumbersTextfile << endl;
      
    }
    duplicatedEventNumbersTextfile << "Total number of events with the same event number which are not duplicates: " << duplicatedEventNumbers.size() << ". Number of events checked: " << nEntries << endl;
    cout << "Total number of events with the same event number which are not duplicates: " << duplicatedEventNumbers.size() << ". Number of events checked: " << nEntries << endl;
    
  }
  
  duplicatedEventsTextfile.close();
  duplicatedEventNumbersTextfile.close();
  
  return duplicatedEvents.size();
  
}
