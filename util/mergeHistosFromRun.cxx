/*

  This program will add histograms and Trees from a list of root files and write them
  to a target root file. The target file is newly created and must not be
  identical to one of the source files.
         
 Syntax:
      hadd targetfile source1 source2 ...
   or
        hadd -f targetfile source1 source2 ...
        (targetfile is overwritten if it exists)
 When -the -f option is specified, one can also specify the compression
 level of the target file. By default the compression level is 1, but
 if "-f0" is specified, the target file will not be compressed.
 if "-f6" is specified, the compression level 6 will be used.
     
 For example assume 3 files f1, f2, f3 containing histograms hn and Trees Tn
    f1 with h1 h2 h3 T1
    f2 with h1 h4 T1 T2
    f3 with h5
   the result of
     hadd -f x.root f1.root f2.root f3.root
   will be a file x.root with h1 h2 h3 h4 h5 T1 T2
   where h1 will be the sum of the 2 histograms in f1 and f2
         T1 will be the merge of the Trees in f1 and f2

  The files may contain sub-directories.
  
 if the source files contains histograms and Trees, one can skip 
 the Trees with
      hadd -T targetfile source1 source2 ...
 
 If the sources and and target compression levels are identical (default),
the program uses the TChain::Merge function with option "fast", ie
 the merge will be done without  unzipping or unstreaming the baskets 
 (i.e. direct copy of the raw byte on disk). The "fast" mode is typically
 5 times faster than the mode unzipping and unstreaming the baskets.
  
 Authors: Rene Brun, Dirk Geppert, Sven A. Schmidt, sven.schmidt@cern.ch
        : rewritten from scratch by Rene Brun (30 November 2005)
           to support files with nested directories.
*/
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "RConfig.h"
#include <string>
#include "TChain.h"
#include "TFile.h"
#include "TH1.h"
#include "TKey.h"
#include "TObjString.h"
#include "Riostream.h"
#include "TClass.h"
#include "THashList.h"
#include "TStopwatch.h"
#include "THnSparse.h"
#include "THnBase.h"

#include <vector>
using namespace std;

TList *FileList;
TFile *Target, *Source;
Bool_t noTrees;
Bool_t fastMethod;


template<typename T> vector<vector<TString> > createListOfHistograms(TFile*file,vector<TString>& directories, T Class);
vector<TString> createListOfDirectories(TFile* file,TString path="");

void createStructureOfDirectories(TFile* file, vector<TString>& chain_names,vector<TString>& inner_directories);
void mergeHistograms(TFile* outputFile,vector<TFile*>& inputFiles, TString directory,vector<TString>& histnames);
void mergeBootstrapObjects(TFile* outputFile,vector<TFile*>& inputFiles,TString directory,vector<TString>& histnames);
void mergeTHnSparse(TFile* outputFile,vector<TFile*>& inputFiles, TString directory,vector<TString>& histnames);


int main( int argc, char **argv ) {
	TStopwatch stopwatch;
	stopwatch.Start();
	TH1::AddDirectory(kFALSE);
	TDirectory::AddDirectory(kFALSE);
   if ( argc < 4 || "-h" == string(argv[1]) || "--help" == string(argv[1]) ) {
     cout << "Error: Wrong number of parameters!" << endl;
     cout << "Usage: " << argv[0] << " targetfile source1 source2 [source3 ...]" << endl;
     //cout << "This program will add histograms from a list of root files and write them" << endl;
     //cout << "to a target root file. The target file is newly created and must not " << endl;
     //cout << "exist, or if -f (\"force\") is given, must not be one of the source files." << endl;
     //cout << "Supply at least two source files for this to make sense... ;-)" << endl;
     //cout << "If the first argument is -T, Trees are not merged" <<endl;
     //cout << "When -the -f option is specified, one can also specify the compression" <<endl;
     //cout << "level of the target file. By default the compression level is 1, but" <<endl;
     //cout << "if \"-f0\" is specified, the target file will not be compressed." <<endl;
     //cout << "if \"-f6\" is specified, the compression level 6 will be used." <<endl;
     //cout << "if Target and source files have different compression levels"<<endl;
     //cout << " a slower method is used"<<endl;
     return 1;
  }
  
  try {
    int debug=0;
    int ffirst=2;
    const int nInputFiles=argc - ffirst;
    //cout << nInputFiles << endl;
    
    TString outputFilename=argv[1];
    vector<TString> inputFileNames(nInputFiles);
    vector<TFile*> inputFiles(nInputFiles);
    
    for ( int i = ffirst; i < argc; i++ ){
      int ifile=i-ffirst;
      inputFileNames[ifile]=argv[i];
      cout << "Input file " << ifile + 1 << ": " << inputFileNames[ifile] << endl;
      inputFiles[ifile] = new TFile(inputFileNames[ifile]);
      if(inputFiles[ifile]->IsZombie())throw TtbarDiffCrossSection_exception((TString)"Error in loading input rootfile with from " + inputFileNames[ifile]);
    }
    
    TFile* outputFile= new TFile(outputFilename,"RECREATE");
    if(outputFile->IsZombie())throw TtbarDiffCrossSection_exception((TString)"Error in creating output rootfile in " + outputFilename);
    cout << "Output file " << outputFilename << endl;
    
    cout << "Creating list of directories." << endl;
    vector<TString> inner_directories = createListOfDirectories(inputFiles[0],"NOSYS");
    inner_directories.push_back("NOSYS");
    vector<TString> chain_names;
    functions::GetListOfNames(chain_names,inputFiles[0],TDirectory::Class());
    const int ndirectories=inner_directories.size();
    if(debug>0)for(int i=0;i<ndirectories;i++){
      cout << inner_directories[i] << endl;	
    }
    cout << "Creating list of histograms." << endl;
    vector<vector<TString> > histnamesTH1D = createListOfHistograms(inputFiles[0],inner_directories, TH1::Class());
    delete inputFiles[0];
    inputFiles[0]=new TFile(inputFileNames[0]);
    cout << "Creating list of Bootstrapped histograms." << endl;
    vector<vector<TString> > namesTH1DBootstrap = createListOfHistograms(inputFiles[0],inner_directories,TH1DBootstrap::Class());
    delete inputFiles[0];
    inputFiles[0]=new TFile(inputFileNames[0]);
    cout << "Creating list of THnSparse histograms." << endl;
    vector<vector<TString> > namesTHnSparseD=createListOfHistograms(inputFiles[0],inner_directories,THnSparse::Class());
    delete inputFiles[0];
    inputFiles[0]=new TFile(inputFileNames[0]);
    for(int i=0;i<ndirectories;i++){
      if(debug>1)cout << inner_directories[i] << endl;
      inner_directories[i].ReplaceAll("NOSYS","");	
    }
    if(debug>1){
      cout << histnamesTH1D[0].size() << " " << namesTH1DBootstrap[namesTH1DBootstrap.size() - 1 ].size() << " " << namesTHnSparseD[namesTHnSparseD.size() - 1].size() << endl;
      for(int i=0;i<(int)histnamesTH1D.size();i++)for(int j=0;j<(int)histnamesTH1D[i].size();j++) cout << "TH1D histogram: " <<  histnamesTH1D[i][j] << endl;
      for(int i=0;i<(int)namesTH1DBootstrap.size();i++)for(int j=0;j<(int)namesTH1DBootstrap[i].size();j++) cout << "TH1DBootstrap histogram: " <<  namesTH1DBootstrap[i][j] << endl;
      for(int i=0;i<(int)namesTHnSparseD.size();i++)for(int j=0;j<(int)namesTHnSparseD[i].size();j++) cout << "THnSparseD histogram: " <<  namesTHnSparseD[i][j] << endl;
    }
    createStructureOfDirectories(outputFile, chain_names,inner_directories);
    delete outputFile;
    outputFile= new TFile(outputFilename,"UPDATE");
    
    cout << "Merging histograms" << endl;
  
    const int nchains=chain_names.size(); 
    for(int ichain=0;ichain<nchains;ichain++){
    //for(int ichain=0;ichain<2;ichain++){
      cout << chain_names[ichain] << endl;
      for(int idir=0;idir<ndirectories;idir++){
	mergeHistograms(outputFile,inputFiles,chain_names[ichain] + inner_directories[idir] ,histnamesTH1D[idir] );
	if(chain_names[ichain]=="NOSYS" && (!namesTH1DBootstrap[idir].empty())){
	  TStopwatch stopwatchBootstrap;
	  stopwatchBootstrap.Start();
	  cout << "INFO: Merging BootStrapped histograms in directory " << chain_names[ichain] + inner_directories[idir] << "." << endl;
	  mergeBootstrapObjects(outputFile,inputFiles,chain_names[ichain] + inner_directories[idir] ,namesTH1DBootstrap[idir] );
	  stopwatchBootstrap.Stop();
	  cout << "Time required to merge Bootstrapped objects in the directory: " << stopwatchBootstrap.RealTime() << endl;
      
	}
	mergeTHnSparse(outputFile,inputFiles,chain_names[ichain] + inner_directories[idir] ,namesTHnSparseD[idir] );
      
      }
      
      if((ichain+1) % 10 ==0){
	delete outputFile;
	outputFile= new TFile(outputFilename,"UPDATE");
      
	for ( int ifile=0; ifile < nInputFiles; ifile++ ){
	  delete inputFiles[ifile];
	  inputFiles[ifile] = new TFile(inputFileNames[ifile]);
	}
      }
    }
    cout << "Done." << endl;
    
    
    cout << "Time spent on merging: " << stopwatch.RealTime() << endl;
    cout << endl << "Deleting Target TFile. It may take some time ..." << endl;
    stopwatch.Start();
    delete outputFile;
    cout << "Time for deleting target: " << stopwatch.RealTime() << endl;
    cout << "Deleting input TFiles" << endl;
    stopwatch.Start();
    const int nfiles=inputFiles.size();
    for(int i=0;i<nfiles;i++) delete inputFiles[i];
    cout << "Time for deleting input TFiles: " << stopwatch.RealTime() << endl;
 
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
 
  return 0;
}


vector<TString> createListOfDirectories(TFile* file,TString path){
  file->cd();
  file->cd(path);
  vector<TString> directories;
  
  functions::GetListOfNames(directories,gDirectory,TDirectory::Class(),path);
  
  for(int i=0;i<(int)directories.size();i++){
    TString directory=directories[i];
    if(path=="") cout << directory << endl;
    //cout << directory << endl;
    vector<TString> helpvec = createListOfDirectories(file,directory);
    directories.insert(directories.end(), helpvec.begin(), helpvec.end());
    //for(int j=0;j<(int)helpvec.size();j++) cout << helpvec[j] << endl;
    helpvec.clear();
  }	
  return directories;
}

template<typename T> vector<vector<TString> > createListOfHistograms(TFile*file,vector<TString>& directories, T Class){
	
  const int ndirectories=directories.size();
  vector<vector<TString> > histnames(ndirectories);
  for(int i=0;i<ndirectories;i++){
    file->cd();
    file->cd(directories[i]);
    if(!directories[i].Contains("/"))cout << "Browsing directory " << directories[i] << endl;
    functions::GetListOfNames(histnames[i],gDirectory,Class);
    
  }
  
  return histnames;
}

void createStructureOfDirectories(TFile* file, vector<TString>& chain_names,vector<TString>& inner_directories){
  const int nchains = chain_names.size();
  const int ndirs=inner_directories.size();
  file->cd();
  cout << "Creating structure of directories in output file" << endl;
  for(int ichain=0;ichain<nchains;ichain++){
  //cout << chain_names[ichain] << endl;
    for(int idir=0;idir<ndirs;idir++){
      if(inner_directories[idir]!="")file->mkdir(chain_names[ichain]+inner_directories[idir]);
    }
  }
}


void mergeHistograms(TFile* outputFile,vector<TFile*>& inputFiles,TString directory,vector<TString>& histnames){
	
  const int nhistos=histnames.size();
  const int nfiles=inputFiles.size();
  
  outputFile->cd();
  outputFile->cd(directory);
  vector<TH1*> histos(nfiles);
  for(int i=0;i<nhistos;i++){
    const TString histname =  directory + "/" + histnames[i];
    if(!histname.BeginsWith("NOSYS") && (histname.EndsWith("_half1") || histname.EndsWith("_half2"))) continue;
  
    for(int ifile=0;ifile<nfiles;ifile++){
      histos[ifile]=(TH1*)inputFiles[ifile]->Get(histname);
      if(!histos[ifile]) throw TtbarDiffCrossSection_exception((TString)"Error in loading histogram " + histname + " from " + inputFiles[ifile]->GetName());
    }
    for(int ifile=1;ifile<nfiles;ifile++)histos[0]->Add(histos[ifile]);
    histos[0]->Write(histnames[i]);
    for(int ifile=0;ifile<nfiles;ifile++)delete histos[ifile];
    
  }
	
}

void mergeBootstrapObjects(TFile* outputFile,vector<TFile*>& inputFiles,TString directory,vector<TString>& histnames){
	
  const int nhistos=histnames.size();
  const int nfiles=inputFiles.size();
  
  outputFile->cd();
  outputFile->cd(directory);
  vector<TH1DBootstrap*> histos(nfiles);
  for(int i=0;i<nhistos;i++){
    const TString histname =  directory + "/" + histnames[i];
    //cout << "Loading Bootstrapped histogram " << histname << " from input files." << endl;
    for(int ifile=0;ifile<nfiles;ifile++){
      histos[ifile]=(TH1DBootstrap*)inputFiles[ifile]->Get(histname);
      if(!histos[ifile]) throw TtbarDiffCrossSection_exception((TString)"Error in loading TH1DBootstrap " + histname + " from " + inputFiles[ifile]->GetName());
    }
    //cout << "Merging Bootstrapped histogram." << endl;
    for(int ifile=1;ifile<nfiles;ifile++)histos[0]->Add(histos[ifile]);
    //cout << "Writing merged bootstrapped histogram." << endl;
    histos[0]->Write(histnames[i]);
    //cout << "Deleting Bootstrapped inputs." << endl;
    for(int ifile=0;ifile<nfiles;ifile++)delete histos[ifile];
  }
	
}
void mergeTHnSparse(TFile* outputFile,vector<TFile*>& inputFiles, TString directory,vector<TString>& histnames){
  const int nhistos=histnames.size();
  const int nfiles=inputFiles.size();
  
  outputFile->cd();
  outputFile->cd(directory);
  THnSparseD* h,*h2;
  vector<THnSparseD*> histos(nfiles);
  for(int i=0;i<nhistos;i++){
    const TString histname =  directory + "/" + histnames[i];
    if(!histname.BeginsWith("NOSYS") && (histname.EndsWith("_half1") || histname.EndsWith("_half2"))) continue;
    
    h=(THnSparseD*)inputFiles[0]->Get(histname);
    if(!h) throw TtbarDiffCrossSection_exception((TString)"Error in loading histogram " + histname + " from " + inputFiles[0]->GetName());
    for(int ifile=1;ifile<nfiles;ifile++){
      h2=(THnSparseD*)inputFiles[ifile]->Get(histname);
      if(!h2) throw TtbarDiffCrossSection_exception((TString)"Error in loading histogram " + histname + " from " + inputFiles[ifile]->GetName());
      h->Add(h2);
      delete h2;
    }
    h->Write(histnames[i]);
    delete h;
  }
  
  

  
}


