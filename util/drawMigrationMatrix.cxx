#include "TFile.h"
#include "TAxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TMatrixT.h"
#include "TPaveText.h"
#include "TLine.h"

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/stringFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"

#include "docopt.h"


int debug(0);

static const char * USAGE =
    R"(Program to draw migration matrices. 

Usage:  
  drawMigrationMatrix [options]
  
Options:
  -h --help                         Show help screen.
  --mainDir <mainDir>               Path to main output directory. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>             Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --configBinning <configBinning>   Path to config file with binning. [default: TtbarDiffCrossSection/data/optBinningND/ttbar_y_vs_ttbar_mass_vs_t1_pt.env]
  --outputSubDir <outputSubDir>	    Output subdirectory to save plots. [default: Theory_vs_data_comparison]
  --variable <string>               Variable name. [default: total_cross_section]
  --level <string>                  ParticleLevel or PartonLevel. [default: ParticleLevel]
  --histname <string>               [default: migration]
  --histdir <string>                [default: NOSYS]
  --systematicsHistosDir <systematicsHistosDir>   Directory with systematic histos rootfiles. [default: systematics_histos]
  --dimension <dimension>           Dimension of variables of interest. [default: 1D]"
  --debugLevel <int>                Option to control debug messages. Higher value means more messages. [default: 0]
)";

TH2D* makePercHist(const TH2D* h,const double threshold=1);
TH2D* makeDummyHistogram(const TH2D* h,bool useUnitBins=true);
void drawMigration(TH2D* h, const TString& variable, const TString& level, const TString& dirname, const double textSize);
void drawBinLabels(TH2D* migra,const vector<double>& xvals,const vector<double>& yvals, const TString& precision, const double textSize);
void drawBinLabels(TH2D* migra, const HistogramNDto1DConverter* histogramConverterReco, const HistogramNDto1DConverter* histogramConverterTruth, const TString& variable, const double textSize);
void drawLines(TH2D* migra, const HistogramNDto1DConverter* histogramConverterReco, const HistogramNDto1DConverter* histogramConverterTruth);
int setupStyle();

int drawMigrationMatrix(const TString& path, const TString path_config_lumi, const TString path_config_binning,
			const TString& outputSubDir, const TString& variable, const TString& level,
			const TString& histname, const TString& histdir,
			const TString& systematicHistosPath, const TString& dimension);

int main(int nArg, char **arg) {
  
  TString path      = "";
  TString path_config_lumi="";
  TString path_config_binning="";
  TString pathMultiDimensionalPlotsConfig="";
  TString outputSubDir = "";
  TString variable = "";
  TString level = "";
  TString histname = "";
  TString histdir = "";
  TString systematicHistosPath="";
  TString dimension = "";
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true);  // Using docopt package to read parameters
  try{
    try{ path=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
    
    try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      
    try{ path_config_binning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
      
    try{ outputSubDir=args["--outputSubDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputSubDir option is expected to be a string. Check input parameters."); }

    try{ variable=args["--variable"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }

    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }

    try{ histname=args["--histname"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--histname option is expected to be a string. Check input parameters."); }

    try{ histdir=args["--histdir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--histdir option is expected to be a string. Check input parameters."); }

    try{ systematicHistosPath=args["--systematicsHistosDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--systematicsHistosDir option is expected to be a string. Check input parameters."); }
    
    try{ dimension=args["--dimension"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--dimension option is expected to be a string. Check input parameters."); }
     
    try{ debug = args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is not an integer. Check input parameters."); }
  
  } catch(const std::invalid_argument& e) {
    cout << e.what() << endl << endl;
    cout << USAGE << endl;
    return -1;
  }  

  path.ReplaceAll("$TtbarDiffCrossSection_output_path",gSystem->Getenv("TtbarDiffCrossSection_output_path"));

  return drawMigrationMatrix(path, path_config_lumi, path_config_binning, outputSubDir, variable, level, histname, histdir, systematicHistosPath, dimension);

}

//----------------------------------------------------------------------

int drawMigrationMatrix(const TString& path, const TString path_config_lumi, const TString path_config_binning,
			const TString& outputSubDir, const TString& variable, const TString& level,
			const TString& histname, const TString& histdir,
			const TString& systematicHistosPath, const TString& dimension) {
  
  
  SetAtlasStyle();
  TH1::AddDirectory(false);
  
  auto configlumi= std::make_shared<NtuplesAnalysisToolsConfig>(path_config_lumi);
  
  TString levelShort = level;
  levelShort.ReplaceAll("Level","");
  
  const TString histNameFull = histdir + "/" + variable + "_" + histname;
  
  const TString mc_samples_production = configlumi->mc_samples_production();
  const TString rootDir="root."+mc_samples_production;
  const TString pdfDir="pdf."+mc_samples_production;
  const TString pngDir="png."+mc_samples_production;
  
  const TString dirname = outputSubDir + "/" + levelShort;
  
  gSystem->mkdir(path + "/" + pdfDir + "/" + dirname,true);
  gSystem->mkdir(path + "/" + pngDir + "/" + dirname,true);
  //gSystem->mkdir(path + "/" + rootDir +"/" + g_dirname,true);
  
  TString systematicHistosPathFull=path+ "/"+rootDir+"/"+systematicHistosPath + "/" + variable + "_" + level + ".root";
  TFile* fSystematicHistos= new TFile(systematicHistosPathFull );
  if(fSystematicHistos->IsZombie()){
    cout << "Error: file with systematics histos is a zombie!" << endl;
    cout << "Attempting to load file: " << systematicHistosPathFull << endl;
    exit(-1);
  }
  
  auto migration = std::unique_ptr<TH2D>{(TH2D*)fSystematicHistos->Get(histNameFull)};
  
  if(histname=="migration") {
    
    setupStyle();
    gStyle->SetTickLength(0.03,"xys");
    
    auto percHist = std::unique_ptr<TH2D>{makePercHist(migration.get())};
    
    auto c = std::make_unique<TCanvas>("c", "canv", 800, 800 );
    gPad->SetLeftMargin( 0.13 );
    gPad->SetRightMargin( 0.15 );
    gPad->SetBottomMargin( 0.15 );
    gPad->SetTopMargin( 0.13 );
    c->cd();
    
    //percHist->SetMarkerSize(1.15);
    if(dimension=="1D" && variable=="t1_pt") { percHist->SetMarkerSize(1.6);} //JH textsize of matrix content
    if(dimension=="1D" && variable=="t1_y") { percHist->SetMarkerSize(1.6);}
    
    percHist->Draw("colz text");
    percHist->GetXaxis()->SetLabelOffset(999);
    percHist->GetYaxis()->SetLabelOffset(999);
    percHist->GetZaxis()->SetRangeUser(0,100);
    percHist->GetZaxis()->SetLabelSize(0.03);
    percHist->GetXaxis()->SetTitleSize(0.04);
    percHist->GetXaxis()->SetTitleOffset(1.8);
    percHist->GetYaxis()->SetTitleOffset(1.55);
    percHist->GetYaxis()->SetTitleSize(0.04);
    percHist->GetZaxis()->SetTitleSize(0.04);
    percHist->GetZaxis()->SetTitleOffset(1.12);
    percHist->GetZaxis()->SetTitle("Migration [%]");
    percHist->GetXaxis()->SetNdivisions(percHist->GetXaxis()->GetNbins());
    percHist->GetYaxis()->SetNdivisions(percHist->GetYaxis()->GetNbins());
    NamesAndTitles::setXtitle(percHist.get(),variable);
    percHist->GetYaxis()->SetTitle(levelShort + " level " + percHist->GetXaxis()->GetTitle());
    percHist->GetXaxis()->SetTitle((TString)"Detector level " + percHist->GetXaxis()->GetTitle());
    
    if(dimension=="1D") {
      std::vector<double> xvals,yvals;
      functions::getVectorOfBinEdges(migration->GetXaxis(),xvals);
      functions::getVectorOfBinEdges(migration->GetYaxis(),yvals);
      
      TString precision="4.2";
      if(variable=="leadingTop_pt") precision="4.3";
      const double textSize=0.03;
      drawBinLabels(percHist.get(),xvals,yvals,precision,textSize);
    }
    
    if(dimension=="ND") {
    
      percHist->GetXaxis()->SetTitle("Detector level");
      if(levelShort == "Particle") percHist->GetYaxis()->SetTitle("Particle level");
      if(levelShort == "Parton") percHist->GetYaxis()->SetTitle("Parton level");
      percHist->SetMarkerSize(1.4);
      percHist->GetXaxis()->SetTitleSize(0.04);
      percHist->GetYaxis()->SetTitleSize(0.04);
      percHist->GetXaxis()->SetTitleOffset(1.78); //t1_pt only
      percHist->GetYaxis()->SetTitleOffset(1.78);
    
      auto histogramConverterReco = std::make_unique<HistogramNDto1DConverter>(path_config_binning.Data());
      histogramConverterReco->initializeOptBinning((variable+"_"+levelShort+"_reco").Data());
      histogramConverterReco->initialize1DBinning();
      
      auto histogramConverterTruth = std::make_unique<HistogramNDto1DConverter>(path_config_binning.Data());
      histogramConverterTruth->initializeOptBinning((variable+"_"+levelShort+"_truth").Data());
      histogramConverterTruth->initialize1DBinning();
      
      drawBinLabels(percHist.get(), histogramConverterReco.get(), histogramConverterTruth.get(), variable, 0.014);
      drawLines(percHist.get(), histogramConverterReco.get(), histogramConverterTruth.get());
      
    }
    
    TString generalInfo = NamesAndTitles::ATLASString("") + ", " + NamesAndTitles::energyString();
    
    auto paveText = std::unique_ptr<TPaveText>(functions::makePaveText({generalInfo},0.04,0.15,0.8,0.93));
    paveText->Draw();
    
    c->SaveAs(path + "/" + pdfDir + "/" + dirname + "/" + "migration_" + variable + "_" + level + "_" + histdir +".pdf");
    c->SaveAs(path + "/" + pngDir + "/" + dirname + "/" + "migration_" + variable + "_" + level + "_" + histdir +".png");
    
    
  }
  
  
  
  return 0;
}

//----------------------------------------------------------------------

TH2D* makePercHist(const TH2D* h,const double threshold) {
  // Returns matrix with rows normalized to 100, values below threshold are set to 0
  const TAxis* xaxis=h->GetXaxis();
  const TAxis* yaxis=h->GetYaxis();
  
  const int nCols=xaxis->GetNbins();
  const int nRows=yaxis->GetNbins();
  
  TH2D* hist = makeDummyHistogram(h);
  
  for(int i=0;i<nRows;i++) {
    const int ybin = i+1;
    const double integral = h->Integral(1, nCols, ybin, ybin);
    
    for(int j=0;j<nCols;j++) {
      const int xbin = j+1;
      
      double content = h->GetBinContent(xbin,ybin) / integral * 100;
      content = std::round(content);
      cout << "******: " << xbin << " " << ybin << " " << content << endl;
      if(content < threshold) content = 0;
      hist->SetBinContent(xbin,ybin,content);
    
    }
  }
  
  
  return hist;
}

//----------------------------------------------------------------------

TH2D* makeDummyHistogram(const TH2D* h,bool useUnitBins) {
  TH2D* hDummy;
  if(useUnitBins) {
    const int nbinsx = h->GetXaxis()->GetNbins();
    const int nbinsy = h->GetYaxis()->GetNbins();
    hDummy = new TH2D("h_dummy","",nbinsx,0,nbinsx,nbinsy,0,nbinsy);
  }
  else {
    hDummy=(TH2D*)h->Clone("h_dummy");
    hDummy->Reset();
  }
  return hDummy;
}

//----------------------------------------------------------------------

void drawBinLabels(TH2D* migra,const vector<double>& xvals,const vector<double>& yvals, const TString& precision, const double textSize) {

  const int nbinsx = migra->GetXaxis()->GetNbins();
  const int nbinsy = migra->GetXaxis()->GetNbins();
  
  TLatex labelX,labelY;
  labelX.SetTextSize(textSize);
  labelX.SetTextAngle(-45); //JH
  //labelX.SetTextAngle(0); //JH only t1_pt
  labelX.SetTextAlign(23);
  //labelX.SetTextAlign(22); //only for t1_pt
  labelY.SetTextSize(textSize);
  labelY.SetTextAlign(32);
  
  TString s = (TString)"%" + precision + "f";

  for (Int_t k=0;k<=nbinsx;k++){
    Double_t ylabel = migra->GetYaxis()->GetBinLowEdge(1) - 0.4*(migra->GetYaxis()->GetBinWidth(1));
    Double_t xlow = xvals[k];
    //Double_t xnew = (migra->GetXaxis()->GetBinLowEdge(k+1) ); //  +  migra->GetXaxis()->GetBinUpEdge(k+1))/2; JH  for t1_pt 
    Double_t xnew = (migra->GetXaxis()->GetBinLowEdge(k+1)  +  migra->GetXaxis()->GetBinUpEdge(k+1))/2; //JH  
    
    labelX.DrawText(xnew,ylabel,Form(s.Data(),xlow));
    
  }
  for (Int_t k=0;k<=nbinsy;k++){
    Double_t xlabel = migra->GetXaxis()->GetBinLowEdge(1) - 0.1*(migra->GetXaxis()->GetBinWidth(1));
    Double_t ylow = yvals[k];
    Double_t ynew = migra->GetYaxis()->GetBinLowEdge(k+1);
    
    labelY.DrawText(xlabel,ynew,Form(s.Data(),ylow));
  }
}

//----------------------------------------------------------------------

void drawBinLabels(TH2D* migra, const HistogramNDto1DConverter* histogramConverterReco, const HistogramNDto1DConverter* histogramConverterTruth, const TString& variable, const double textSize) {

  
  
  TLatex labelX,labelY;
  labelX.SetTextSize(textSize);
  labelX.SetTextAlign(33);
  labelY.SetTextSize(textSize);
  labelY.SetTextAlign(33);
    
  const std::vector<std::string> binIDsReco = histogramConverterReco->getInternalAxisIDs();
  const std::vector<std::string> binIDsTruth = histogramConverterTruth->getInternalAxisIDs();
  
  const int nprojectionsReco = binIDsReco.size();
  const int nprojectionsTruth = binIDsTruth.size();
  
  
  const int nbinsReco = migra->GetXaxis()->GetNbins();
  const int nbinsTruth = migra->GetXaxis()->GetNbins();
  
  double ylabel = migra->GetYaxis()->GetBinLowEdge(1) - 0.005*(migra->GetYaxis()->GetBinWidth(1)*nbinsReco);
  double xlabel = migra->GetXaxis()->GetBinLowEdge(1) - 0.012*(migra->GetXaxis()->GetBinWidth(1)*nbinsTruth);
  
  vector<TString> xtitles = NamesAndTitles::getXtitles(variable);
  
  
  int index=1;
  for(int iproj=0;iproj<nprojectionsReco;iproj++) {
    
    const std::vector<TString> binning = histogramConverterReco->getBinningTString(binIDsReco[iproj]);
    const int nbins = binning.size()-1;
    
    Double_t xnew = migra->GetXaxis()->GetBinLowEdge(index ) + 0.5*migra->GetXaxis()->GetBinWidth(1)*nbins;
    labelX.SetTextAngle(0);
    labelX.SetTextAlign(22);
    
    vector<TString> externalBinsInfo = histogramConverterReco->getExternalBinsInfo(iproj,"binning");    
    
    for(int k=0;k<(int)externalBinsInfo.size();k++) {
      int l=externalBinsInfo.size()-1 - k;
      labelX.DrawLatex(xnew,ylabel - (0.09 + k*0.025)*(migra->GetYaxis()->GetBinWidth(1)*nbinsTruth),externalBinsInfo[l]);
    }
    
    if(iproj==0) {
      labelX.SetTextAlign(12);
      for(int k=0;k<(int)xtitles.size();k++) {
	int l=xtitles.size()-1 - k;
	double x = migra->GetXaxis()->GetBinUpEdge(nbinsReco) + 0.01*migra->GetXaxis()->GetBinWidth(1)*nbinsReco;
	if (k>0) labelX.DrawLatex(x,ylabel - (0.09 + (k-1)*0.025)*(migra->GetYaxis()->GetBinWidth(1)*nbinsTruth),xtitles[l]);
	else labelX.DrawLatex(x,ylabel - 0.02*(migra->GetYaxis()->GetBinWidth(1)*nbinsTruth),xtitles[l]);
      }
      
    }
    
    
    for(int i=0;i<nbins;i++) {
      Double_t xnew = migra->GetXaxis()->GetBinLowEdge(index) + 0.6*migra->GetXaxis()->GetBinWidth(index);
      
      labelX.SetTextAngle(45);
      labelX.SetTextAlign(33);
      
      labelX.DrawText(xnew,ylabel,(TString) "[" + binning[i] +","+binning[i+1]+"]");
      
      index++;
    }
    
    
  }
  
  
  index=1;
  for(int iproj=0;iproj<nprojectionsTruth;iproj++) {
    
    const std::vector<TString> binning = histogramConverterTruth->getBinningTString(binIDsTruth[iproj]);
    const int nbins = binning.size()-1;
    
    Double_t ynew = migra->GetYaxis()->GetBinLowEdge(index ) + 0.5*migra->GetYaxis()->GetBinWidth(1)*nbins;
    labelY.SetTextAngle(90);
    labelY.SetTextAlign(22);
    
    vector<TString> externalBinsInfo = histogramConverterTruth->getExternalBinsInfo(iproj,"binning");
    
    for(int k=0;k<(int)externalBinsInfo.size();k++) {
      int l=externalBinsInfo.size()-1 - k;
      labelY.DrawLatex(xlabel - (0.078 + k*0.025)*(migra->GetXaxis()->GetBinWidth(1)*nbinsTruth),ynew,externalBinsInfo[l]);
    }
    
    if(iproj==0) {
      labelY.SetTextAlign(12);
      for(int k=0;k<(int)xtitles.size();k++) {
	int l=xtitles.size()-1 - k;
	double y = migra->GetYaxis()->GetBinUpEdge(nbinsTruth) + 0.01*migra->GetYaxis()->GetBinWidth(1)*nbinsTruth;;
	if (k>0) labelY.DrawLatex(xlabel - (0.078 + (k-1)*0.025)*(migra->GetXaxis()->GetBinWidth(1)*nbinsTruth),y,xtitles[l]);
	else labelY.DrawLatex(xlabel - 0.02*(migra->GetXaxis()->GetBinWidth(1)*nbinsTruth),y,xtitles[l]);
      }
      
    }
    
    
    for(int i=0;i<nbins;i++) {
      Double_t ynew = migra->GetYaxis()->GetBinLowEdge(index) + 0.6*migra->GetYaxis()->GetBinWidth(index);
      
      labelY.SetTextAngle(45);
      labelY.SetTextAlign(33);
      
      labelY.DrawText(xlabel,ynew,(TString) "[" + binning[i] +","+binning[i+1]+"]");
      
      index++;
    }
    
  }

}

//----------------------------------------------------------------------

void drawLines(TH2D* migra, const HistogramNDto1DConverter* histogramConverterReco, const HistogramNDto1DConverter* histogramConverterTruth) {
  
  
  const std::vector<std::string>& binIDsReco = histogramConverterReco->getInternalAxisIDs();
  const std::vector<std::string>& binIDsTruth = histogramConverterTruth->getInternalAxisIDs();
  
  const int nbinsx= migra->GetNbinsX();
  const int nbinsy= migra->GetNbinsY();
  
  int index = 0;
  for( int i=0;i<(int)binIDsReco.size()-1;i++){
    const std::vector<double>& binning = histogramConverterReco->getBinning(binIDsReco[i]);
    
    index+=binning.size()-1;
    auto line = std::make_unique<TLine>(index,0,index,nbinsx);
    line->SetLineColor(kBlack);
    line->DrawClone("same");
  }
  
  index = 0;
  for( int i=0;i<(int)binIDsTruth.size()-1;i++){
    const std::vector<double>& binning = histogramConverterTruth->getBinning(binIDsTruth[i]);
    
    index+=binning.size()-1;
    auto line = std::make_unique<TLine>(0,index,nbinsy,index);
    line->SetLineColor(kBlack);
    line->DrawClone("same");
  }
  
}

//----------------------------------------------------------------------

int setupStyle() {
   gStyle->SetPaintTextFormat("4.0f"); //JH
   gStyle->SetOptStat(0);
   gStyle->SetMarkerSize(1.5);
   gStyle->SetOptTitle(0);

   const int Number = 3;
   //Double_t Red[Number]    = { 1.00, 0.43, 0.05};
   //Double_t Green[Number]  = { 1.00, 0.78, 0.59};
   //Double_t Blue[Number]   = { 1.00, 0.69, 0.53};
   //Double_t Length[Number] = { 0.00, 0.25, 1.00 };
   
   Double_t Red[Number]    = { 1.00, 0.00, 0.00};
   Double_t Green[Number]  = { 1.00, 0.62, 0.40};
   Double_t Blue[Number]   = { 1.00, 0.88, 0.72};
   Double_t Length[Number] = { 0.00, 0.80, 1.00 };
   
   Int_t nb=100;
   TColor::CreateGradientColorTable(Number,Length,Red,Green,Blue,nb);
   return nb;
  
}
