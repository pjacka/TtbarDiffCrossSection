
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include "TSystem.h"
#include "TCanvas.h"
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TEnv.h"
#include "TLegend.h"
#include "TStyle.h"

using std::cout;
using std::endl;
using std::cerr;

TString g_topTaggerLabel;
TString g_BTaggerLabel;
TString g_lumiString;

void setupHistograms(TH1D* signal,TH1D* background,const TString& info);
void makePlots(TString mainDir,TString ABCDdir,TString pdfDir,TString filename,TString variable,const TString& info);

int main(int nArg, char **arg) {
  TH1::AddDirectory(kFALSE);
  SetAtlasStyle();
  try{
    if(nArg!=7)throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    
    
    TString mainDir=(TString)arg[1] + "/";
    TString path_config_lumi=arg[2];
    TString mc_samples_production=arg[3];
    TString variable=arg[4];
    g_topTaggerLabel=arg[5];
    g_BTaggerLabel=arg[6];
    g_topTaggerLabel.ReplaceAll("_"," ");
    g_BTaggerLabel.ReplaceAll("_"," ");
    
    cout << "mainDir: " << mainDir << endl;
    cout << "path_config_lumi: " << path_config_lumi << endl;
    cout << "mc_samples_production: " << mc_samples_production << endl;
    cout << "variable: " << variable << endl;
    
    auto config = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
    g_lumiString=configFunctions::getLumiString(*config);
    
    cout << "Using lumi: " << g_lumiString << endl;
    
    TString filename = "histos.root";
    
    TString ABCDdir = "ABCD."+mc_samples_production + "/ABCD_tests/TopTaggingEfficiencyPlots/";
    TString pdfDir = "pdf."+mc_samples_production + "/TopTaggingEfficiencyPlots/"; 
    makePlots(mainDir,ABCDdir,pdfDir,filename,variable,"Top-tagging");
    
    ABCDdir = "ABCD."+mc_samples_production + "/ABCD_tests/BTaggingEfficiencyPlots/";
    pdfDir = "pdf."+mc_samples_production + "/BTaggingEfficiencyPlots/";
    makePlots(mainDir,ABCDdir,pdfDir,filename,variable,"B-matching");
    
    
    
    
    
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  
  
  
  return 0;
}


void makePlots(TString mainDir,TString ABCDdir,TString pdfDir,TString filename,TString variable,const TString& info){
  
    std::shared_ptr<TFile> f (TFile::Open(mainDir+ABCDdir+filename,"READ"));
    
    if(f->IsZombie() ) throw TtbarDiffCrossSection_exception("Error: Input file is zombie: " + mainDir+ABCDdir+filename);
    
    cout << f->GetName() << endl;
    
    TString outputDir=mainDir+pdfDir;
    TString outputDirPNG=outputDir;
    outputDirPNG.ReplaceAll("/pdf.","/png.");
    gSystem->mkdir(outputDir,true);
    gSystem->mkdir(outputDirPNG,true);
    
    auto c = std::make_shared<TCanvas>("c","",1);
    
    
    TString SignalPrefix = "Signal_Leading_";
    TString BackgroundPrefix = "Background_Leading_";
    TH1D* leadingJetSignalEff = (TH1D*)f->Get(SignalPrefix+variable);
    TH1D* leadingJetBackgroundEff = (TH1D*)f->Get(BackgroundPrefix+variable);
    
    setupHistograms(leadingJetSignalEff,leadingJetBackgroundEff,info);
    cout << leadingJetSignalEff->GetBinContent(1) << " " << leadingJetBackgroundEff->GetBinContent(1) << endl;
    
    auto leg = std::make_shared<TLegend>(0.67,0.84,0.87,0.92);
    leg->SetNColumns(1);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.04);
    leg->AddEntry(leadingJetSignalEff,"MC ttbar allhad");
    leg->AddEntry(leadingJetBackgroundEff,"Multijet bkg (DD)","l");
    
    //double ymin = std::min(leadingJetSignalEff->GetMinimum(),leadingJetBackgroundEff->GetMinimum());
    //double ymax = std::max(leadingJetSignalEff->GetMaximum(),leadingJetBackgroundEff->GetMaximum());
    leadingJetSignalEff->GetYaxis()->SetRangeUser(0.,1.1); 
    
    leadingJetSignalEff->Draw();
    leadingJetSignalEff->Draw("HIST SAME");
    leadingJetBackgroundEff->Draw("SAME");
    leadingJetBackgroundEff->Draw("HIST SAME");
    
    functions::WriteGeneralInfo("",g_lumiString,0.04,0.2,0.88,0.05);
    functions::WriteInfo("Leading large-R jet "+info,0.04, 0.2,0.78);
    if(info=="Top-tagging")functions::WriteInfo(g_topTaggerLabel,0.04, 0.2,0.73);
    if(info=="B-matching")functions::WriteInfo(g_BTaggerLabel,0.04, 0.2,0.73);

    leg->Draw();
    
    c->RedrawAxis();
    c->SaveAs(outputDir+"/" + variable + "_leading.pdf");
    c->SaveAs(outputDirPNG+"/" + variable + "_leading.png");
    
    
    
    
    SignalPrefix="Signal_Subleading_";
    BackgroundPrefix = "Background_Subleading_";
    TH1D* subleadingJetSignalEff = (TH1D*)f->Get(SignalPrefix+variable);
    TH1D* subleadingJetBackgroundEff = (TH1D*)f->Get(BackgroundPrefix+variable);
    
    setupHistograms(subleadingJetSignalEff,subleadingJetBackgroundEff,info);
    cout << subleadingJetSignalEff->GetBinContent(1) << " " << subleadingJetBackgroundEff->GetBinContent(1) << endl;
    
    leg = std::make_shared<TLegend>(0.67,0.84,0.87,0.92);
    leg->SetNColumns(1);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.04);
    leg->AddEntry(subleadingJetSignalEff,"MC ttbar allhad");
    leg->AddEntry(subleadingJetBackgroundEff,"Multijet bkg (DD)","l");
    
    //double ymin = std::min(subleadingJetSignalEff->GetMinimum(),subleadingJetBackgroundEff->GetMinimum());
    //double ymax = std::max(subleadingJetSignalEff->GetMaximum(),subleadingJetBackgroundEff->GetMaximum());
    subleadingJetSignalEff->GetYaxis()->SetRangeUser(0.,1.1); 
    
    subleadingJetSignalEff->Draw();
    subleadingJetSignalEff->Draw("HIST SAME");
    subleadingJetBackgroundEff->Draw("SAME");
    subleadingJetBackgroundEff->Draw("HIST SAME");
    
    functions::WriteGeneralInfo("",g_lumiString,0.04,0.2,0.88,0.05);
    functions::WriteInfo("2nd-leading large-R jet "+info,0.04, 0.2,0.78);
    if(info=="Top-tagging")functions::WriteInfo(g_topTaggerLabel,0.04, 0.2,0.73);
    if(info=="B-matching")functions::WriteInfo(g_BTaggerLabel,0.04, 0.2,0.73);
    
    leg->Draw();
    
    c->RedrawAxis();
    c->SaveAs(outputDir+"/" + variable + "_subleading.pdf");
    c->SaveAs(outputDirPNG+"/" + variable + "_subleading.png");
  
  
  
}

void setupHistograms(TH1D* signal,TH1D* background,const TString& info){
  signal->SetLineColor(1);
  signal->GetYaxis()->SetTitle(info +" efficiency");
  signal->SetMarkerStyle(1);
  
  background->SetLineColor(2);
  background->SetMarkerColor(2);
  background->SetMarkerStyle(1);
  background->SetMarkerSize(1);
  background->SetLineWidth(2);
  background->SetLineStyle(2);
  
  const int nbins=signal->GetNbinsX();
  for(int i=1;i<=nbins;i++){
    if(!std::isfinite(signal->GetBinContent(i)) )signal->SetBinContent(i,0.);
    if(!std::isfinite(background->GetBinContent(i)) )background->SetBinContent(i,0.);
    
  }
  
}

