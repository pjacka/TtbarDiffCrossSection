#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "THStack.h"
#include "TSystem.h"
#include "TEnv.h"

#include "docopt.h"

using std::cout;
using std::endl;
using std::cerr;


// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to make reco level plots from histograms stored in systematics_histos rootfiles.  

Usage:  
  drawReco [options]
  
Options:
  -h --help                                      Show help screen.
  --mainDir <mainDir>             	         Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <configfile>               	         Path to main config file. [default: TtbarDiffCrossSection/data/config.env]
  --systematicsHistosDir <systematicsHistosDir>  Directory with systematic histos rootfiles. [default: systematics_histos]
  --outputDir <outputDir>                        Directory to store plots. Will be created in maindir/pdf.<MC> . [default: RecoPlots]
  --variable <string>                  	         Variable name. [default: total_cross_section]
  --level <string>                               ParticleLevel or PartonLevel. Option is used just to identify input rootfile. [default: ParticleLevel]
  --normalizeSignal <normalizeSignal>            Option to normalize Signal to (Data - Background). [default: 1]
  --debugLevel <int>                             Option to control debug messages. Higher value means more messages. [default: 0]
)";

int drawReco(TString mainDir,TString systematicHistosPath,TString outputSubdir,TString path_config_lumi,TString variable_name,TString level,bool normalizeSignal,int debugLevel);

int main(int nArg, char **arg) {
  
  
  TString mainDir="";
  TString systematicHistosPath="";
  TString outputSubdir="";
  TString path_config_lumi="";
  TString variable_name="";
  TString level="";
  
  bool normalizeSignal=true;
  int debugLevel=0;
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
  
  try{
  
    try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
      
    try{ path_config_lumi=args["--config"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      
    try{ systematicHistosPath=args["--systematicsHistosDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--systematicsHistosDir option is expected to be a string. Check input parameters."); }
      
    try{ outputSubdir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
      
    try{ variable_name=args["--variable"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
      
    try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
    
    try{ normalizeSignal=args["--normalizeSignal"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--normalizeSignal option is expected to be an integer. Check input parameters."); }
    
    try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
      
  } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
  
  if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
  
  if(debugLevel>0){
    cout << endl << "Running drawReco for variable " << variable_name << "." << endl;
    cout << "Main directory is " << mainDir << "." << endl << endl;
  }
  
  return drawReco(mainDir,systematicHistosPath,outputSubdir,path_config_lumi,variable_name,level,normalizeSignal,debugLevel);
}

int drawReco(TString mainDir,TString systematicHistosPath,TString outputSubdir,TString path_config_lumi,TString variable_name,TString level,bool normalizeSignal,int debugLevel){  
  try {
    
    SetAtlasStyle();
    TH1::AddDirectory(kFALSE);
    
    auto configlumi = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);
    TString mc_samples_production = configlumi->mc_samples_production();
    TString lumi_string = configFunctions::getLumiString(*configlumi);
    const TString nominalTreeName = configlumi->nominalFolderName().c_str();
    
    TString filename=mainDir + "/root." + mc_samples_production + "/" + systematicHistosPath + "/" + variable_name + "_" + level + ".root";
    TFile* file_systematics= new TFile(filename);
    if(file_systematics->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " +filename); 
      return -1;
    }
    
    
    // Loading histograms
    
    TH1D* h_data=(TH1D*)file_systematics->Get(nominalTreeName + "/" + variable_name + "_data");
    vector<TH1D*> histos;
    vector<TString> names;
    vector<int> colors;
    histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/" + variable_name + "_bkg_Multijet"));names.push_back("Multijet");colors.push_back(619);
    histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "_ttHWZ"));names.push_back("t#bar{t}+W/Z/H");colors.push_back(800);
    //histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "Singletop"));names.push_back("Single top");colors.push_back(600);
    TH1D* helphist = (TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "_Wt_singletop");
    //helphist->Add((TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "_tChannel_singletop"));
    histos.push_back(helphist);names.push_back("Single top");colors.push_back(600);
    //histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "Wt_singletop"));names.push_back("Wt single top");colors.push_back(600);
    //histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "tChannel_singletop"));names.push_back("single top t-channel");colors.push_back(kGreen+3);
    histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/MCBackgrounds/" + variable_name + "_ttbar_nonallhad"));names.push_back("t#bar{t} Non All Hadr");colors.push_back(432);
    // Signal must be the last element
    histos.push_back((TH1D*)file_systematics->Get(nominalTreeName + "/" + variable_name + "_signal_reco"));names.push_back("t#bar{t} All Hadr");colors.push_back(0);
    
    TH1D* h_pseudodata=(TH1D*)file_systematics->Get(nominalTreeName + "/" + variable_name + "_pseudodata");
    
    TH1D* h_signal=(TH1D*)file_systematics->Get(nominalTreeName + "/" + variable_name + "_signal_reco");
    TH1D* h_bkg_all=(TH1D*)file_systematics->Get(nominalTreeName + "/" + variable_name + "_bkg_all");
    
    
    NamesAndTitles::setXtitle(h_data,variable_name);
    TString xtitle=h_data->GetXaxis()->GetTitle();
    TString unit=NamesAndTitles::getUnit(xtitle);
    
    
    TH1D* h_dataMinusBkg = (TH1D*)h_data->Clone();
    h_dataMinusBkg->Add(h_bkg_all,-1);
    
    
    h_pseudodata->Reset();
    
    if (debugLevel > 0) cout << "Histograms loaded." << endl;


    THStack stack("stack","stacked histograms");
    
    const int nhistos=histos.size();


    auto can= std::make_unique<TCanvas>("can","canvas 2 pads",0,0,600,600);
    can->cd();
    auto pad1 = new TPad("pad1","pad1",0,0.33,1,1,0,0,0);
    auto pad2 = new TPad("pad2","pad2",0,0,1,0.33,0,0,0);
    pad2->SetBottomMargin(0.3);
    pad2->SetTopMargin(0);
    pad2->SetRightMargin(0.05);
    pad2->SetGridy(1);
    pad2->SetTicks();
    pad1->SetBottomMargin(0.);
    pad1->SetTopMargin(0.05);
    pad1->SetRightMargin(0.05);
    pad1->SetTicks();
    pad1->Draw();
    pad2->Draw();
    pad1->cd();
    pad2->cd();
    
    
    
    
    TLegend *leg = new TLegend(0.54,0.55,0.92,0.92);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.039);
    
    
    TString nEvents_data = Form("  %3.1f",h_data->Integral());
    //double nEvents_dataD=h_data->Integral();
    TString nEntries_data=Form("  %3.1f", h_data->GetEntries());
    leg->AddEntry(h_data,"Data");
    
    
    functions::DivideByBinWidth(h_data);
    
    double sumEventsBkg=0.;
    double factor = 1.0;
    for(int i=0;i<nhistos-1;i++) sumEventsBkg+=histos[i]->Integral();
    //if(normalizeSignal) factor = (nEvents_dataD - sumEventsBkg)/histos.back()->Integral();
    if(normalizeSignal) factor = 0.67;
    
    if (debugLevel > 0) cout << "Making THStack." << endl;
    
    for(int i=0;i<nhistos;i++) {
      
      TString nEvents = Form(" %3.1f",histos[i]->Integral());
      if(normalizeSignal && (i==nhistos-1)) {
	histos[i]->Scale(factor);
	names[i]= names[i] + Form(" #color[4]{#times %.2f}",factor);
      }
      
      TString nEntries_MC=Form("  %3.1f", histos[i]->GetEntries());
      functions::DivideByBinWidth(histos[i]);
      stack.Add(histos[i]);
      
      
      h_pseudodata->Add(histos[i]);
      
      histos[i]->SetFillColor(colors[i]);
      if(i!=nhistos-1){
	histos[i]->SetLineColor(colors[i]);
	histos[i]->SetLineWidth(0);
      }
    }
    
    for(int i=nhistos-1;i>=0;i--) leg->AddEntry(histos[i],names[i],"F");
    
    if (debugLevel > 0) cout << "THStack created." << endl;
    
    TGraphAsymmErrors* graph_unc=new TGraphAsymmErrors(h_pseudodata);
    TGraphAsymmErrors* graph_stat_unc=new TGraphAsymmErrors(h_pseudodata);
    
    functions::calculateSystematicUncertainties(graph_unc,file_systematics,variable_name); // Get relative detector systematic errors
    const int nbins=h_pseudodata->GetNbinsX();
    for(int i=0;i<nbins;i++){
      int ibin=i+1;
      double bincontent = h_pseudodata->GetBinContent(ibin);
      graph_unc->SetPointEYhigh( i, sqrt( pow(graph_unc->GetErrorYhigh(i)*bincontent,2) + pow(graph_stat_unc->GetErrorYhigh(i),2) ) );
      graph_unc->SetPointEYlow(  i, sqrt( pow(graph_unc->GetErrorYlow(i)*bincontent, 2) + pow(graph_stat_unc->GetErrorYlow(i), 2) ) );
      
      double x,y;
      graph_unc->GetPoint(i,x,y);
      
      if (debugLevel > 1) cout << "bin: " << ibin << " bincontent: " << bincontent << " center point: " << y << " rel. stat. unc: " << graph_stat_unc->GetErrorYhigh(i)/bincontent << " rel. combine unc.:  " << graph_unc->GetErrorYhigh(i)/bincontent << endl;
      
    }
    
    TGraphAsymmErrors* unit_graph=functions::getUnitGraph(graph_unc);
    TGraphAsymmErrors* unit_graph_stat=functions::getUnitGraph(graph_stat_unc);
    
    
    h_data->SetLineColor(1);
    h_data->SetMarkerColor(1);
    
    double max= std::max(h_data->GetMaximum(),h_pseudodata->GetMaximum());    
    double min = histos[0]->GetMinimum(0.);
    
    
    h_data->GetYaxis()->SetRangeUser(min/2,max*1.9);
    h_data->GetYaxis()->SetTitleSize(0.07);
    h_data->GetYaxis()->SetTitleOffset(0.9);
    
    if (debugLevel > 0) cout << "Using unit: " << unit << endl;
    
    if(unit!="") h_data->GetYaxis()->SetTitle("Events / "+unit);
    else h_data->GetYaxis()->SetTitle("Events");
    
    pad1->cd();
    
    h_data->Draw();
    graph_unc->Draw("same2");
    graph_stat_unc->Draw("same2"); 
    stack.Draw("HISTsame"); 
    //    hist_data->Draw("same TEXT0");
    h_pseudodata->Draw("HISTsame");
    h_data->Draw("same");
    
    
    graph_unc->SetFillStyle(1001);
    graph_unc->SetFillColor(kGray+1);
    graph_unc->SetLineColor(kGray+1);
    graph_stat_unc->SetFillStyle(1001);
    graph_stat_unc->SetFillColor(kGray);
    graph_stat_unc->SetLineColor(kGray);
    leg->AddEntry(graph_stat_unc,"MC Stat. Unc.","F");
    leg->AddEntry(graph_unc,"MC Stat. #oplus Det. Syst. Unc.","F");
    
    leg->Draw();
    
    pad1->Update();
    pad1->RedrawAxis("g");
    pad1->RedrawAxis();
    
    pad2->cd();
    
    TH1D* ratio=(TH1D*)h_data->Clone();
    
    for(int ibin=1;ibin<=ratio->GetNbinsX();ibin++){
      double bincontent = h_pseudodata->GetBinContent(ibin)>0 ? h_data->GetBinContent(ibin)/h_pseudodata->GetBinContent(ibin) : 0.;
      ratio->SetBinContent(ibin,bincontent);
      double binerror = h_pseudodata->GetBinContent(ibin)>0 ? h_data->GetBinError(ibin)/h_pseudodata->GetBinContent(ibin) : 0.;
      ratio->SetBinError(ibin,binerror);
    }
    
    ratio->SetYTitle("Data / Pred.");
      // use this for ratio Pred./Data in the ratio plot:                                                                                                                                   //TH1D* ratio=(TH1D*)hist_allMC->Clone();
      //ratio->Divide(hist_data);
      //ratio->SetYTitle("Pred. / Data");

    ratio->GetXaxis()->SetTitleSize(0.13);
    ratio->GetXaxis()->SetTitleOffset(1.05);
    ratio->GetYaxis()->SetTitleSize(0.13);
    ratio->GetYaxis()->SetTitleOffset(0.5);
    ratio->GetYaxis()->SetNdivisions(5);
    ratio->GetXaxis()->SetLabelSize(0.123);
    ratio->GetYaxis()->SetLabelSize(0.123);
    ratio->GetYaxis()->SetNdivisions(10);
    
    ratio->GetYaxis()->SetRangeUser(0.41,1.59);
    ratio->SetLineColor(1);
    
    ratio->Draw();
    unit_graph->SetFillStyle(1001);
    unit_graph->SetFillColor(kGray+1);
    unit_graph->SetLineColor(kGray+1);
    unit_graph_stat->SetFillStyle(1001);
    unit_graph_stat->SetFillColor(kGray);
    unit_graph_stat->SetLineColor(kGray);
    unit_graph->Draw("same2");
    unit_graph_stat->Draw("same2");
      //   unit_graph->Draw("samelx");
    ratio->Draw("same");
      
    pad2->RedrawAxis("g"); 
    pad2->RedrawAxis(); 
      
    pad1->cd();
    functions::WriteGeneralInfo("Signal region",lumi_string,0.049,0.2,0.87,0.05);
    functions::WriteInfo(NamesAndTitles::analysisLabel(), 0.049, 0.2, 0.72);
    
    TString suffix = normalizeSignal ? "_normalized" : "";
    TString pdfdir=mainDir + "/pdf."+ mc_samples_production + suffix + "/" +outputSubdir;
    TString pngdir=mainDir + "/png."+ mc_samples_production + suffix + "/" +outputSubdir;
    
    gSystem->mkdir(pdfdir,true);
    gSystem->mkdir(pngdir,true);
    
    can->SaveAs(pdfdir + "/" + variable_name + ".pdf");
    can->SaveAs(pngdir + "/" + variable_name + ".png");
    
    pad1->cd();
    
    h_data->GetYaxis()->SetRangeUser(min/5,max*600);
    
    pad1->SetLogy();
    
    can->SaveAs(pdfdir + "/" + variable_name + "_log.pdf");
    can->SaveAs(pngdir + "/" + variable_name + "_log.png");
    
    
    // Fraction ratio plots use always not normalized spectra
    histos.back()->Scale(1./factor);
    h_pseudodata->Reset();
    for(TH1D* h : histos) h_pseudodata->Add(h);
    
    THStack frac("fractions","stacked fractions");
    leg->Clear();
    
    for(size_t i=0;i<histos.size();i++) {
      histos[i]->Divide(h_pseudodata);
      frac.Add((TH1D*)histos[i]);
      leg->AddEntry(histos[i],names[i],"F");
    }
    
    frac.SetMaximum(2.);
    frac.SetMinimum(0.);
    
    can= std::make_unique<TCanvas>("can_frac","canvas fractions",0,0,600,600);
    can->cd();
    
    frac.Draw("HIST");
    leg->SetX1NDC(0.6);
    leg->Draw();
    NamesAndTitles::setXtitle(frac.GetHistogram(),variable_name);
    frac.GetHistogram()->GetYaxis()->SetTitle("Event fraction");
    functions::WriteGeneralInfo("Signal region",lumi_string,0.049,0.2,0.87,0.05);
    
    can->SaveAs(pdfdir + "/" + variable_name + "_bkg_frac.pdf");
    can->SaveAs(pngdir + "/" + variable_name + "_bkg_frac.png");
    
    can= std::make_unique<TCanvas>("can_dataMinusBkg","data minus bkg",0,0,600,600);
    can->cd();
    ratio = (TH1D*)h_signal->Clone();
    ratio->Divide(h_dataMinusBkg);
    ratio->Draw();
    NamesAndTitles::setXtitle(ratio,variable_name);
    ratio->GetYaxis()->SetTitle("Signal / (Data - Bkg)");
    ratio->GetYaxis()->SetRangeUser(0.8,1.7);
    
    functions::WriteGeneralInfo("Signal region",lumi_string,0.049,0.2,0.87,0.05);
    
    can->SaveAs(pdfdir + "/" + variable_name + "_dataMinusBkg.pdf");
    can->SaveAs(pngdir + "/" + variable_name + "_dataMinusBkg.png"); 
    
    
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();return -1;}
  
  cout << "drawReco: Done." << endl;
  return 0;
}
