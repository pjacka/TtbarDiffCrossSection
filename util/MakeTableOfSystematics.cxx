
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"


using std::cout;
using std::endl;
using std::cerr;

int main(int nArg, char **arg) {
	cout << endl << "Running ProducePseudoexperiments for variable " << arg[5] << endl << endl;
	TH1::AddDirectory(kFALSE);
	try {
		if(nArg!=7){
			cout << "Error: Wrong number of input parameters!" << endl;
			throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
		}
		
	}catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
	
	cout << "Everything ok!" << endl;
	return 0;
}

