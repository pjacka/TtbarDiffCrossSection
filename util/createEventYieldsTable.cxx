
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "CovarianceCalculator/SystematicHistos.h"
#include "CovarianceCalculator/AsymmetricErrorsCalculator.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"



#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TEnv.h"


using std::cout;
using std::endl;
using std::cerr;


void printLine(ofstream& textfile,TH1D* h,TString name);
Double_t CalculateDetSys(Double_t nominal, vector<Double_t> shifts);
Double_t CalculateDetSysStat(Double_t nominal, vector<Double_t> shifts, Double_t stat_unc);


int main(int nArg, char **arg) {
   TH1::AddDirectory(kFALSE);
   SetAtlasStyle();
   try {
      if(nArg!=6)throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 


      TString mainDir=arg[1];
      TString path_config_files=arg[2];
      TString path_config_lumi=arg[3];
      TString variable_name=arg[4];
      TString level=arg[5];

      TString systematics_histos_name="systematics_histos"; 

      auto ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(path_config_lumi);

      TString mc_samples_production = ttbarConfig->mc_samples_production();
      const TString nominal_folder_name = ttbarConfig->nominalFolderName().c_str();

      TString rootDir="root."+mc_samples_production;
      TString texDir="tex."+mc_samples_production;


      TFile* file_systematics= new TFile(mainDir+"/root."+mc_samples_production+"/"+systematics_histos_name+"/" + variable_name + "_" + level + ".root");
      if(file_systematics->IsZombie()){
         throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/root."+mc_samples_production+"/"+systematics_histos_name+"/" + variable_name + "_" + level + ".root"); 
         return -1;
      }

      //TString SystematicUncertaintiesFolder="SystematicUncertainties";
      //if(dimension=="2D")SystematicUncertaintiesFolder="SystematicUncertainties2D";

      //gSystem->mkdir(mainDir + "/tex/"+SystematicUncertaintiesFolder,true);
      //ofstream textfileSysFullAbsolute((mainDir + "/tex/"+SystematicUncertaintiesFolder+"/Table_" + level + "_" + variable_name + "_Absolute.tex" ).Data());
      //ofstream textfileSysFullNormalized((mainDir + "/tex/"+SystematicUncertaintiesFolder+"/Table_" + level + "_" + variable_name + "_Normalized.tex" ).Data());
      //ofstream textfileSysSummaryAbsolute((mainDir + "/tex/"+SystematicUncertaintiesFolder+"/SummaryTable_" + level + "_" + variable_name + "_Absolute.tex" ).Data());
      //ofstream textfileSysSummaryNormalized((mainDir + "/tex/"+SystematicUncertaintiesFolder+"/SummaryTable_" + level + "_" + variable_name + "_Normalized.tex" ).Data());


      SystematicHistos* sysHistos= new SystematicHistos(file_systematics,variable_name,*ttbarConfig);

      //sysHistos->prepareListOfSystematics(); 

      TH1D* h_nonallhad=(TH1D*)file_systematics->Get(nominal_folder_name + "/MCBackgrounds/"+variable_name+"_ttbar_nonallhad");
      TH1D* h_Wt=(TH1D*)file_systematics->Get(nominal_folder_name + "/MCBackgrounds/"+variable_name+"_Wt_singletop");
      //TH1D* h_tChan=(TH1D*)file_systematics->Get(nominal_folder_name + "/MCBackgrounds/"+variable_name+"_tChannel_singletop");
      TH1D* h_ttHWZ=(TH1D*)file_systematics->Get(nominal_folder_name + "/MCBackgrounds/"+variable_name+"_ttHWZ");
      //TH1D* h_MC_region_H_nominal=(TH1D*)file_systematics->Get(nominal_folder_name + "/SidebandRegions/"+variable_name+"_MC_H");




      OneSysHistos* nominal = sysHistos->getNominalHistos();

      Double_t Data =         nominal->data->GetBinContent(1);
      Double_t Data_err =     nominal->data->GetBinError(1);
      Double_t Pred =         nominal->pseudodata->GetBinContent(1);
      Double_t Pred_err =     nominal->pseudodata->GetBinError(1);
      Double_t bkg_all =      nominal->bkg_all->GetBinContent(1);
      Double_t bkg_all_err =  nominal->bkg_all->GetBinError(1);

      //Double_t signal_reco  = nominal->signal_reco->GetBinContent(1) ;
      //Double_t nonallhad    = h_nonallhad->GetBinContent(1) 			;
      //Double_t Wt           = h_Wt->GetBinContent(1) 				;
      //Double_t tChan        = h_tChan->GetBinContent(1) 				;
      //Double_t ttHWZ        = h_ttHWZ->GetBinContent(1) 				;
      //Double_t bkg_MC       = nominal->bkg_MC->GetBinContent(1) 		;
      //Double_t bkg_Multijet	= nominal->bkg_Multijet->GetBinContent(1) ;
      //Double_t MC_region_H_nominal	= h_MC_region_H_nominal->GetBinContent(1) ;
      
      
     //cout << "MC_region_H_nominal: " << MC_region_H_nominal << endl; 
      
      vector<TString> systNamesSingles = sysHistos->getListOfSystSingle();
      vector<pair<TString,TString> >  systNamesPairs = sysHistos->getListOfSystPairs();
      
      vector<Double_t> signal_shifts;
      vector<Double_t> nonallhad_shifts;
      vector<Double_t> Wt_shifts;
      //vector<Double_t> tChan_shifts;
      vector<Double_t> ttHWZ_shifts;
      vector<Double_t> multijet_shifts;
      vector<Double_t> bkg_all_shifts;
      vector<Double_t> bkg_MC_shifts;
      vector<Double_t> Pred_shifts;

      TString tableDir1=mainDir +"/" + texDir;
      TString tableDir=mainDir +"/" + texDir + "/EventYields/";
      cout << tableDir << endl;
      gSystem->mkdir(tableDir1); 
      gSystem->mkdir(tableDir); 

      ofstream textfile(tableDir+"EventYields.tex");
      //ofstream textfile_multijet(tableDir+"Multijet_sys.tex");
      //ofstream textfile_signal(tableDir+"Signal_sys.tex");

            //double nonallhad_var_up, nonallhad_var_down, nonallhad_var, nonallhad_rel_unc_up, nonallhad_rel_unc_down;
            //double Wt_var_up, Wt_var_down, Wt_var, Wt_rel_unc_up, Wt_rel_unc_down;
            //double tChan_var_up, tChan_var_down, tChan_var, tChan_rel_unc_up, tChan_rel_unc_down;
            //double ttHWZ_var_up, ttHWZ_var_down, ttHWZ_var, ttHWZ_rel_unc_up, ttHWZ_rel_unc_down;


      //for ( pair<TString,TString> systName : systNamesPairs) {


         //TH1D* h_nonallhad_sys_first  =(TH1D*)file_systematics->Get((TString)systName.first+"/MCBackgrounds/"+variable_name+"_ttbar_nonallhad");
         //TH1D* h_Wt_sys_first         =(TH1D*)file_systematics->Get((TString)systName.first+"/MCBackgrounds/"+variable_name+"Wt_singletop");
         //TH1D* h_tChan_sys_first      =(TH1D*)file_systematics->Get((TString)systName.first+"/MCBackgrounds/"+variable_name+"tChannel_singletop");
         //TH1D* h_ttHWZ_sys_first      =(TH1D*)file_systematics->Get((TString)systName.first+"/MCBackgrounds/"+variable_name+"_ttHWZ");

         //TH1D* h_nonallhad_sys_second =(TH1D*)file_systematics->Get((TString)systName.second+"/MCBackgrounds/"+variable_name+"_ttbar_nonallhad");
         //TH1D* h_Wt_sys_second        =(TH1D*)file_systematics->Get((TString)systName.second+"/MCBackgrounds/"+variable_name+"Wt_singletop");
         //TH1D* h_tChan_sys_second     =(TH1D*)file_systematics->Get((TString)systName.second+"/MCBackgrounds/"+variable_name+"tChannel_singletop");
         //TH1D* h_ttHWZ_sys_second     =(TH1D*)file_systematics->Get((TString)systName.second+"/MCBackgrounds/"+variable_name+"_ttHWZ");
         
         //cout << systName.first << " " << h_nonallhad_sys_first->GetBinContent(1) << " " << h_Wt_sys_first->GetBinContent(1) << " " << h_tChan_sys_first->GetBinContent(1) << " "  << h_ttHWZ_sys_first->GetBinContent(1) << endl;
         
         ////cout << "***---*** line: " << __LINE__ << endl;

         //nonallhad_var_up   = nonallhad - h_nonallhad_sys_second->GetBinContent(1);
         //nonallhad_var_down = nonallhad - h_nonallhad_sys_first->GetBinContent(1);

         //if ((nonallhad_var_up*nonallhad_var_down)<0.){nonallhad_var=(std::abs(nonallhad_var_up) + std::abs(nonallhad_var_down))/2.;}
         //else{nonallhad_var=std::max(std::abs(nonallhad_var_up),std::abs(nonallhad_var_down));}
         
         //nonallhad_shifts.push_back(nonallhad_var) ;
      
         //Wt_var_up   = Wt - h_Wt_sys_second->GetBinContent(1);
         //Wt_var_down = Wt - h_Wt_sys_first->GetBinContent(1);

         //if ((Wt_var_up*Wt_var_down)<0.){Wt_var=(std::abs(Wt_var_up) + std::abs(Wt_var_down))/2.;}
         //else{Wt_var=std::max(std::abs(Wt_var_up),std::abs(Wt_var_down));}

         //Wt_shifts.push_back(Wt_var) ;

         //tChan_var_up   = tChan - h_tChan_sys_second->GetBinContent(1);
         //tChan_var_down = tChan - h_tChan_sys_first->GetBinContent(1);

         //if ((tChan_var_up*tChan_var_down)<0.){tChan_var=(std::abs(tChan_var_up) + std::abs(tChan_var_down))/2.;}
         //else{tChan_var=std::max(std::abs(tChan_var_up),std::abs(tChan_var_down));}
      
         //tChan_shifts.push_back(tChan_var) ;

         //ttHWZ_var_up   = ttHWZ - h_ttHWZ_sys_second->GetBinContent(1);
         //ttHWZ_var_down = ttHWZ - h_ttHWZ_sys_first->GetBinContent(1);

         //if ((ttHWZ_var_up*ttHWZ_var_down)<0.){ttHWZ_var=(std::abs(ttHWZ_var_up) + std::abs(ttHWZ_var_down))/2.;}
         //else{ttHWZ_var=std::max(std::abs(ttHWZ_var_up),std::abs(ttHWZ_var_down));}
      
         //ttHWZ_shifts.push_back(ttHWZ_var) ;


      //}

      //for ( TString systName : systNamesSingles) {


         //TH1D* h_nonallhad_sys  =(TH1D*)file_systematics->Get((TString)systName+"/MCBackgrounds/"+variable_name+"_ttbar_nonallhad");
         //TH1D* h_Wt_sys         =(TH1D*)file_systematics->Get((TString)systName+"/MCBackgrounds/"+variable_name+"Wt_singletop");
         //TH1D* h_tChan_sys      =(TH1D*)file_systematics->Get((TString)systName+"/MCBackgrounds/"+variable_name+"tChannel_singletop");
         //TH1D* h_ttHWZ_sys      =(TH1D*)file_systematics->Get((TString)systName+"/MCBackgrounds/"+variable_name+"_ttHWZ");


         //cout << systName << ":::::: " << h_nonallhad_sys->GetBinContent(1) << " " << h_Wt_sys->GetBinContent(1) << " " << h_tChan_sys->GetBinContent(1) << " "  << h_ttHWZ_sys->GetBinContent(1) << endl;
         

      ////nonallhad_shifts.push_back(h_nonallhad_sys->GetBinContent(1)) ;
      
      ////Wt_shifts.push_back(h_Wt_sys->GetBinContent(1)) ;
      
      ////tChan_shifts.push_back(h_tChan_sys->GetBinContent(1)) ;
      
      ////ttHWZ_shifts.push_back(h_ttHWZ_sys->GetBinContent(1)) ;


      //}



      //vector<pair<OneSysHistos*, OneSysHistos*>> systPairs = sysHistos->getSystPaired();

      //textfile_multijet << "\\documentclass[12pt]{article}" << endl;
      //textfile_multijet << "\\usepackage{longtable}" << endl;
      //textfile_multijet << "\\usepackage{xcolor}" << endl;
      //textfile_multijet << "\\begin{document}" << endl;
      //textfile_multijet << "\\begin{longtable}{| p{.80\\textwidth} | p{.10\\textwidth} | p{.10\\textwidth}| }" << endl;
      //textfile_multijet << "Syst name  & shift & rel unc. \\\\ \\hline" << endl;

      //textfile_signal << "\\documentclass[12pt,landscape]{article}" << endl;
      //textfile_signal << "\\usepackage{longtable}" << endl;
      //textfile_signal << "\\usepackage{xcolor}" << endl;
      //textfile_signal << "\\begin{document}" << endl;
      //textfile_signal << "\\begin{longtable}{| p{.80\\textwidth} | p{.10\\textwidth} | p{.10\\textwidth}| }" << endl;
      //textfile_signal << "Syst name  & shift & rel unc. \\\\ \\hline" << endl;
 
      //double rel_unc = 999;
      //double multijet_var_up, multijet_var_down, multijet_var, multijet_rel_unc_up, multijet_rel_unc_down;
      //double signal_var_up, signal_var_down, signal_var, signal_rel_unc_up, signal_rel_unc_down;
      //double bkg_all_var_up, bkg_all_var_down, bkg_all_var, bkg_all_rel_unc_up, bkg_all_rel_unc_down;
      //double bkg_MC_var_up, bkg_MC_var_down, bkg_MC_var, bkg_MC_rel_unc_up, bkg_MC_rel_unc_down;
      //double Pred_var_up, Pred_var_down, Pred_var, Pred_rel_unc_up, Pred_rel_unc_down;
      //string isRed;



      ////for ( pair<OneSysHistos*, OneSysHistos*> systPair : systPairs){
      //for (std::size_t i = 0; i != systPairs.size(); ++i) {

         //isRed="";
         //multijet_var_up=-999;
         //multijet_var_down = -999;
         //multijet_var=-999;

         ////signal_shifts.push_back   (systPair.first->signal_reco->GetBinContent(1));
         ////signal_shifts.push_back   (systPair.second->signal_reco->GetBinContent(1));

         ////multijet_shifts.push_back (systPairs[i].first->bkg_Multijet->GetBinContent(1));
         ////multijet_shifts.push_back (systPairs[i].second->bkg_Multijet->GetBinContent(1));
         ////multijet_shifts.push_back (systPair.first->bkg_Multijet->GetBinContent(1));
         ////multijet_shifts.push_back (systPair.second->bkg_Multijet->GetBinContent(1));

         ////bkg_all_shifts.push_back  (systPair.first->bkg_all->GetBinContent(1));
         ////bkg_all_shifts.push_back  (systPair.second->bkg_all->GetBinContent(1));

         ////bkg_MC_shifts.push_back  (systPair.first->bkg_MC->GetBinContent(1));
         ////bkg_MC_shifts.push_back  (systPair.second->bkg_MC->GetBinContent(1));        

         ////Pred_shifts.push_back     (systPair.first->pseudodata->GetBinContent(1));
         ////Pred_shifts.push_back     (systPair.second->pseudodata->GetBinContent(1));

         ////cout << "TTbar allhad SYST down: " << Form("%4.2f", systPair.first->signal_reco->GetBinContent(1)) << endl;

         ////signal_var_up=signal_reco - systPairs[i].second->signal_reco->GetBinContent(1);
         //TH1D *h_sig_reg_h_up      =(TH1D*)file_systematics->Get((TString)systNamesPairs[i].second+"/SidebandRegions/"+variable_name+"_MC_O");
         //signal_var_up = MC_region_H_nominal - h_sig_reg_h_up->GetBinContent(1);

         ////signal_var_down =signal_reco - systPairs[i].first->signal_reco->GetBinContent(1);
         //TH1D *h_sig_reg_h_down      =(TH1D*)file_systematics->Get((TString)systNamesPairs[i].first+"/SidebandRegions/"+variable_name+"_MC_O");
         //signal_var_down = MC_region_H_nominal - h_sig_reg_h_down->GetBinContent(1);
         
         
         //if ((signal_var_up*signal_var_down)<0.){signal_var=(std::abs(signal_var_up) + std::abs(signal_var_down))/2.;}
         //else{signal_var=std::max(std::abs(signal_var_up),std::abs(signal_var_down));}

         //signal_shifts.push_back (signal_var);
         ////signal_shifts.push_back (signal_var_down);


         //multijet_var_up=bkg_Multijet - systPairs[i].second->bkg_Multijet->GetBinContent(1);
         //multijet_var_down =bkg_Multijet - systPairs[i].first->bkg_Multijet->GetBinContent(1);

         //if ((multijet_var_up*multijet_var_down)<0.){multijet_var=(std::abs(multijet_var_up) + std::abs(multijet_var_down))/2.;}
         //else{multijet_var=std::max(std::abs(multijet_var_up),std::abs(multijet_var_down));}

         ////multijet_shifts.push_back (multijet_var_up);
         //multijet_shifts.push_back (multijet_var);


         //bkg_all_var_up=bkg_all - systPairs[i].second->bkg_all->GetBinContent(1);
         //bkg_all_var_down =bkg_all - systPairs[i].first->bkg_all->GetBinContent(1);

         //if ((bkg_all_var_up*bkg_all_var_down)<0.){bkg_all_var=(std::abs(bkg_all_var_up) + std::abs(bkg_all_var_down))/2.;}
         //else{bkg_all_var=std::max(std::abs(bkg_all_var_up),std::abs(bkg_all_var_down));}

         //bkg_all_shifts.push_back (bkg_all_var);


         //bkg_MC_var_up=std::abs(bkg_MC - systPairs[i].second->bkg_MC->GetBinContent(1));
         //bkg_MC_var_down =std::abs(bkg_MC - systPairs[i].first->bkg_MC->GetBinContent(1));

         //if ((bkg_MC_var_up*bkg_MC_var_down)<0.){bkg_MC_var=(std::abs(bkg_MC_var_up) + std::abs(bkg_MC_var_down))/2.;}
         //else{bkg_MC_var=std::max(std::abs(bkg_MC_var_up),std::abs(bkg_MC_var_down));}

         //bkg_MC_shifts.push_back (bkg_MC_var);


         //Pred_var_up=std::abs(Pred - systPairs[i].second->pseudodata->GetBinContent(1));
         //Pred_var_down =std::abs(Pred - systPairs[i].first->pseudodata->GetBinContent(1));

         //if ((Pred_var_up*Pred_var_down)<0.){Pred_var=(std::abs(Pred_var_up) + std::abs(Pred_var_down))/2.;}
         //else{Pred_var=std::max(std::abs(Pred_var_up),std::abs(Pred_var_down));}

         //Pred_shifts.push_back (Pred_var);



         //signal_rel_unc_up = signal_var_up /MC_region_H_nominal*100;
         //if (std::abs(signal_rel_unc_up) > 1.) isRed = "\\color{red} ";
         //textfile_signal << " \\verb!" <<  systNamesPairs[i].second << " ! & " << Form("%4.1f",signal_var_up ) << " & " << isRed << Form("%4.1f", signal_rel_unc_up) << "\\% \\\\ \\hline" << endl;

         //isRed = "";
         //signal_rel_unc_down = signal_var_down /MC_region_H_nominal*100;
         //if (std::abs(signal_rel_unc_down) > 1.) isRed = "\\color{red} ";
         //textfile_signal << "\\verb!" <<  systNamesPairs[i].first << " ! & " << Form("%4.1f",signal_var_down ) << " & " << isRed << Form("%4.1f", signal_rel_unc_down) << "\\% \\\\ \\hline" << endl;



         //multijet_rel_unc_up = multijet_var_up /bkg_Multijet*100;
         //if (std::abs(multijet_rel_unc_up) > 1.) isRed = "\\color{red} ";
         //textfile_multijet << "\\verb!" <<  systNamesPairs[i].second << " ! & " << Form("%4.1f",multijet_var_up ) << " & " << isRed << Form("%4.1f", multijet_rel_unc_up) << "\\% \\\\ \\hline" << endl;

         //isRed = "";
         //multijet_rel_unc_down = multijet_var_down /bkg_Multijet*100;
         //if (std::abs(multijet_rel_unc_down) > 1.) isRed = "\\color{red} ";
         //textfile_multijet << "\\verb!" <<  systNamesPairs[i].first << " ! & " << Form("%4.1f",multijet_var_down ) << " & " << isRed << Form("%4.1f", multijet_rel_unc_down) << "\\% \\\\ \\hline" << endl;
      //}
      
      //vector< OneSysHistos*> systSingles = sysHistos->getSystSingle();

      //cout << "+++++: " << systSingles.size() << endl;
      ////for ( OneSysHistos* systSingle : systSingles){
       //for (std::size_t i = 0; i != systSingles.size(); ++i) {
        
         //signal_shifts.push_back   (std::abs(signal_reco -systSingles[i]->signal_reco->GetBinContent(1)));
         
         //multijet_shifts.push_back (std::abs(bkg_Multijet - systSingles[i]->bkg_Multijet->GetBinContent(1)));
         
         //bkg_all_shifts.push_back  (std::abs(bkg_all - systSingles[i]->bkg_all->GetBinContent(1)));
         
         //bkg_MC_shifts.push_back  (std::abs(bkg_MC - systSingles[i]->bkg_MC->GetBinContent(1)));
         
         //Pred_shifts.push_back     (std::abs(Pred - systSingles[i]->pseudodata->GetBinContent(1)));

      ////cout << "TTbar allhad SYST down: " << Form("%4.2f", systPair.first->signal_reco->GetBinContent(1)) << endl;
      
        //textfile_signal << "\\scriptsize \\verb!" <<  systNamesSingles[i] << " ! & " << Form("%4.1f", signal_reco - systSingles[i]->signal_reco->GetBinContent(1)) << " & " <<  Form("%4.1f", (signal_reco - systSingles[i]->signal_reco->GetBinContent(1))/signal_reco*100) << "\\% \\\\ \\hline" << endl;
        
        //textfile_multijet << "\\scriptsize \\verb!" <<  systNamesSingles[i] << " ! & " << Form("%4.1f", std::abs(bkg_Multijet - systSingles[i]->bkg_Multijet->GetBinContent(1))) << " & " <<  Form("%4.1f", std::abs(bkg_Multijet - systSingles[i]->bkg_Multijet->GetBinContent(1))/bkg_Multijet*100) << "\\% \\\\ \\hline" << endl;
   //}

      //textfile_multijet << "\\end{longtable}" << endl;
      //textfile_multijet << "\\end{document}" << endl;

      //textfile_signal << "\\end{longtable}" << endl;
      //textfile_signal << "\\end{document}" << endl;

      //cout << " signal det sys:            " << CalculateDetSys(signal_reco, signal_shifts) << endl;
      //cout << " nonallhad det sys:         " << CalculateDetSys(nonallhad, nonallhad_shifts) << endl;
      //cout << " Wt det sys:                " << CalculateDetSys(Wt, Wt_shifts) << endl;
      //cout << " tChan det sys:             " << CalculateDetSys(tChan, tChan_shifts) << endl;
      //cout << " ttHWZ det sys:             " << CalculateDetSys(ttHWZ, ttHWZ_shifts) << endl;
      //cout << " MC background det sys:     " << CalculateDetSys(bkg_MC, bkg_MC_shifts) << endl;
      //cout << " Multijet det sys:          " << CalculateDetSys(bkg_Multijet, multijet_shifts) << endl;
      //cout << " All bkg det sys:           " << CalculateDetSys(bkg_all, bkg_all_shifts) << endl;
      //cout << " Prediction det sys:        " << CalculateDetSys(Pred, Pred_shifts) << endl;
      
      //cout << " signal det sys + stat:            " << CalculateDetSysStat(signal_reco, signal_shifts, nominal->signal_reco->GetBinError(1)) << endl;
      //cout << " nonallhad det sys + stat:         " << CalculateDetSysStat(nonallhad, nonallhad_shifts,h_nonallhad->GetBinError(1) ) << endl;
      //cout << " Wt det sys + stat:                " << CalculateDetSysStat(Wt, Wt_shifts, h_Wt->GetBinError(1) ) << endl;
      //cout << " tChan det sys + stat:             " << CalculateDetSysStat(tChan, tChan_shifts,h_tChan->GetBinError(1)  ) << endl;
      //cout << " ttHWZ det sys + stat:             " << CalculateDetSysStat(ttHWZ, ttHWZ_shifts, h_ttHWZ->GetBinError(1)) << endl;
      //cout << " MC background det sys + stat:     " << CalculateDetSysStat(bkg_MC, bkg_MC_shifts, nominal->bkg_MC->GetBinError(1)) << endl;
      //cout << " Multijet det sys + stat:          " << CalculateDetSysStat(bkg_Multijet, multijet_shifts,nominal->bkg_Multijet->GetBinError(1) ) << endl;
      //cout << " All bkg det sys + stat:           " << CalculateDetSysStat(bkg_all, bkg_all_shifts,nominal->bkg_all->GetBinError(1) ) << endl;
      //cout << " Prediction det sys + stat:        " << CalculateDetSysStat(Pred, Pred_shifts, nominal->pseudodata->GetBinError(1)) << endl;    
      






      cout << "TTbar allhad: " << Form("%4.2f", nominal->signal_reco->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->signal_reco->GetBinError(1)) << endl;
      cout << "TTbar non-allhad: " << Form("%4.2f", h_nonallhad->GetBinContent(1)) << " +- " << Form("%4.2f", h_nonallhad->GetBinError(1)) << endl;
      cout << "Wt single-top: " << Form("%4.2f", h_Wt->GetBinContent(1)) << " +- " << Form("%4.2f", h_Wt->GetBinError(1)) << endl;
      //cout << "Single-top t-channel: " << Form("%4.2f", h_tChan->GetBinContent(1)) << " +- " << Form("%4.2f", h_tChan->GetBinError(1)) << endl;
      cout << "tt+HWZ: " << Form("%4.2f", h_ttHWZ->GetBinContent(1)) << " +- " << Form("%4.2f", h_ttHWZ->GetBinError(1)) << endl;
      cout << "MC background: " << Form("%4.2f", nominal->bkg_MC->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->bkg_MC->GetBinError(1)) << endl;
      cout << "Multijet: " << Form("%4.2f", nominal->bkg_Multijet->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->bkg_Multijet->GetBinError(1)) << endl;
      cout << "All background: " << Form("%4.2f", nominal->bkg_all->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->bkg_all->GetBinError(1)) << endl;
      cout << "Prediction: " << Form("%4.2f", nominal->pseudodata->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->pseudodata->GetBinError(1)) << endl;
      cout << "Data: " << Form("%4.0f", nominal->data->GetBinContent(1)) << " +- " << Form("%4.0f", nominal->data->GetBinError(1)) << endl;
      cout << "Data / Pred.: " << Form("%4.2f", nominal->data->GetBinContent(1) / nominal->pseudodata->GetBinContent(1) ) << " +- " << Form("%4.2f", (Data/Pred)*( (Data_err/Data) + (Pred_err/Pred) ) ) << endl;
      cout << "All bkg / Pred.: " << Form("%4.3f", nominal->bkg_all->GetBinContent(1) / nominal->pseudodata->GetBinContent(1) ) << " +- " << Form("%4.3f", (bkg_all/Pred)*( (bkg_all_err/bkg_all) + (Pred_err/Pred) ) ) << endl;


      //textfile << "TTbar allhad: " << Form("%4.2f", nominal->signal_reco->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->signal_reco->GetBinError(1)) << endl;
      //textfile << "TTbar non-allhad: " << Form("%4.2f", h_nonallhad->GetBinContent(1)) << " +- " << Form("%4.2f", h_nonallhad->GetBinError(1)) << endl;
      //textfile << "Wt single-top: " << Form("%4.2f", h_Wt->GetBinContent(1)) << " +- " << Form("%4.2f", h_Wt->GetBinError(1)) << endl;
      //textfile << "tt+HWZ: " << Form("%4.2f", h_ttHWZ->GetBinContent(1)) << " +- " << Form("%4.2f", h_ttHWZ->GetBinError(1)) << endl;
      //textfile << "MC background: " << Form("%4.2f", nominal->bkg_MC->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->bkg_MC->GetBinError(1)) << endl;
      //textfile << "Multijet: " << Form("%4.2f", nominal->bkg_Multijet->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->bkg_Multijet->GetBinError(1)) << endl;
      //textfile << "All background: " << Form("%4.2f", nominal->bkg_all->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->bkg_all->GetBinError(1)) << endl;
      //textfile << "Prediction: " << Form("%4.2f", nominal->pseudodata->GetBinContent(1)) << " +- " << Form("%4.2f", nominal->pseudodata->GetBinError(1)) << endl;
      //textfile << "Data: " << Form("%4.0f", nominal->data->GetBinContent(1)) << " +- " << Form("%4.0f", nominal->data->GetBinError(1)) << endl;

      TH1D* ratio = (TH1D*)nominal->data->Clone();
      ratio->Divide(nominal->pseudodata);
      TH1D* ratio_bkg_pred = (TH1D*)nominal->bkg_all->Clone();
      ratio_bkg_pred->Divide(nominal->pseudodata);

      textfile << "\\begin{tabular}{c|c} \\hline" << endl;
      printLine(textfile,nominal->signal_reco,"\\ttbar allhad");
      printLine(textfile,h_nonallhad,"\\ttbar non-allhad");
      printLine(textfile,h_Wt,"Wt single-top");
      //printLine(textfile,h_tChan,"single-top t-channel");
      printLine(textfile,h_ttHWZ,"tt+HWZ");
      //textfile << "\\hline" << endl; 
      //printLine(textfile,nominal->bkg_MC,"MC background");
      textfile << "\\hline" << endl;
      printLine(textfile,nominal->bkg_Multijet,"Multijet");
      printLine(textfile,nominal->bkg_all,"All background");
      textfile << "\\hline" << endl;
      printLine(textfile,nominal->pseudodata,"Prediction");
      printLine(textfile,nominal->data,"Data");
      printLine(textfile,ratio,"Data / Pred.");
      printLine(textfile,ratio_bkg_pred,"All bkg / Pred.");
      textfile << "\\end{tabular}" << endl;


      textfile.close();


   } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}

   cout << "Everything ok!" << endl;
   return 0;
}


void printLine(ofstream& textfile,TH1D* h,TString name) {
   TString precision="%4.0";
   if (name=="Data") precision="%4.0";
   if (name=="Data / Pred." || name=="All bkg / Pred.") precision="%4.3";
   textfile << name << " & " << Form((precision+"f").Data(), h->GetBinContent(1)) << " $\\pm$ " << Form((precision+"f").Data(), h->GetBinError(1)) << " \\\\ \\hline" << endl;


}


Double_t CalculateDetSys(Double_t /*nominal*/, vector<Double_t> shifts) {
  Double_t det_sys_sqr=0;
  for ( Double_t shift : shifts){
    //det_sys_sqr+=(nominal-shift)*(nominal-shift);
    det_sys_sqr+=(shift)*(shift);
  }
  return sqrt(det_sys_sqr); 
}

Double_t CalculateDetSysStat(Double_t /*nominal*/, vector<Double_t> shifts, Double_t stat_unc) {
  Double_t det_sys_sqr=0;
  for ( Double_t shift : shifts){
    //det_sys_sqr+=(nominal-shift)*(nominal-shift);
    det_sys_sqr+=(shift)*(shift);
  }
  return sqrt(det_sys_sqr+(stat_unc*stat_unc)); 
}
