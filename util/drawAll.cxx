#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"
#include "TROOT.h"
#include "Math/Random.h"
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TEnv.h"


#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "HelperFunctions/AtlasStyle.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"

#include "docopt.h"

using namespace std;


void loadMultijetFile(TString mainDir,TString qcd,TEnv* config);
void loadMCBackgroundFiles(TString mainDir,TEnv* config);
void loadSignalFiles(TString mainDir,TString signal,TEnv* config);


bool loadHistos(TString sys, vector<TString>& selections,TString multijetRegion,TString level,TString name,TH1D*& h_data,vector<TH1D*>& histos);
bool loadMCHistos(TString sys, vector<TString>& selections,TString multijetRegion,TString level,TString name,vector<TH1D*>& histos);
double getSignalNormFactor(TH1D* h_data,vector<TH1D*>& histos);
void normalizeSignal(TH1D* h_data,vector<TH1D*>& histos);
void rebinMCHistos(vector<TH1D*>& histos,int rebin);
void rebinMCHistos(vector<TH1D*>& histos,const vector<double>& binning);
void rebinHistos(TH1D* h_data,vector<TH1D*>& histos,int rebin);
void rebinHistos(TH1D*& h_data,vector<TH1D*>& histos,const vector<double>& binning);
void divideByBinWidthHistos(TH1D* h_data,vector<TH1D*>& histos);
void scaleHistosWithLumi(double lumi,vector<TH1D*>& histos);
void makeStack(THStack*& hstack,vector<TH1D*>& histos);
void makeHistAllMC(TH1D*& histAllMC,vector<TH1D*>& histos);
void getNEvents(vector<TH1D*>& histos,vector<TString>& nevents);
void getNEntries(vector<TH1D*>& histos,vector<TString>& nentries);

void GetRecoSys(TH1D*& hist_sys,TH1D* hist_nominal,TString sys, vector<TString>& selections,TString multijetRegion,TString level,TString name, bool doSym, bool doNorm, float norm_factor);
int histSetLimits(TH1* hist, double scaleFactor);

TGraphAsymmErrors* GetUnitGraph(TGraphAsymmErrors* graph);

TFile* g_file_data;
vector<TFile*> g_files;
vector<TString> g_names;
vector<int> g_colors;
double g_lumi;
double g_ttbar_SF;
int g_iNonAllHadrFile;
int g_iSignalFile;
int g_iQCD;
int g_rebin;

bool g_useLumi;
bool g_doRebin;
bool g_doSys;
bool g_doNorm;
bool g_useTTBarSF;
std::vector<double> g_binning;

TString g_nominalTreeName;


TString g_mc_samples_production;


int drawAll(TString mainDir,TString configsPath,TString qcd,TString signal,TString mc_production, bool doSys,bool doNorm,bool useLumi,bool doRebin,bool useTTBarSF,int debugLevel);

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to make reco level plots from all 1D histograms stored in rootfiles produced by produceHistos.  

Usage:  
  drawAll [options]
  
Options:
  -h --help                     Show help screen.
  --mainDir <mainDir>           Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --configsPath <configsPath>   Path to configuration files. [default: $TTBAR_PATH/TtbarDiffCrossSection/data]
  --qcd <qcd>                   Multijetbackground estimate method. [default: ABCD16_BBB_S9_tight]
  --signal <signal>             Option to specify signal. [default: nominal]
  --mcProd <mcProd>             Sets MC production. Available: MC16a, MC16d, MC16e, All. [default: All]
  --doSys <doSys>               Activates systematic uncertainties calculation. [default: 1]
  --doNorm <doNorm>             Option to activate signal normalization to (Data - Background). [default: 1]
  --useLumi <useLumi>           If active histograms will be scaled by luminosity. [default: 0]
  --doRebin <doRebin>           Activates rebinning. [default: 0]
  --useTTBarSF <useTTBarSF>     Activates scaling of ttbar samples with cross-sections. [default: 0]
  --debugLevel <int>            Option to control debug messages. Higher value means more messages. [default: 0]
)";



int main(int nArg, char **arg) {
  
  TString mainDir       = "";
  TString configsPath        = "";
  TString qcd           = "";
  TString signal        = "";
  TString mc_production = "";
  bool doSys            = false;
  bool doNorm           = false;
  bool useLumi         = false;
  bool doRebin          = false;
  bool useTTBarSF     = false;
  int debugLevel        = 0;
  
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
  
  try{
  
    try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
    
    try{ configsPath=args["--configsPath"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--configsPath option is expected to be a string. Check input parameters."); }
    
    try{ qcd=args["--qcd"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--qcd option is expected to be a string. Check input parameters."); }
    
    try{ signal=args["--signal"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--signal option is expected to be a string. Check input parameters."); }
    
    try{ mc_production=args["--mcProd"].asString();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--mcProd option is expected to be a string. Check input parameters."); }
    
    try{ doSys=args["--doSys"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doSys option is expected to be an integer. Check input parameters."); }
      
    try{ doNorm=args["--doNorm"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doNorm option is expected to be an integer. Check input parameters."); }
      
    try{ useLumi=args["--useLumi"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--useLumi option is expected to be an integer. Check input parameters."); }
      
    try{ doRebin=args["--doRebin"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--doRebin option is expected to be an integer. Check input parameters."); }
      
    try{ useTTBarSF=args["--useTTBarSF"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--useTTBarSF option is expected to be an integer. Check input parameters."); }
      
    try{ debugLevel=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
      { throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
      
  } catch(const std::invalid_argument& e) {
      cout << e.what() << endl << endl;
      cout << USAGE << endl;
      return -1;
    }
  
  if (mainDir=="$TtbarDiffCrossSection_output_path") mainDir=gSystem->Getenv("TtbarDiffCrossSection_output_path"); // If not set the default value is $TtbarDiffCrossSection_output_path
  configsPath.ReplaceAll("$TTBAR_PATH",gSystem->Getenv("TTBAR_PATH"));
  
  
  if(debugLevel>0){
    cout << "Main directory is " << mainDir << "." << endl << endl;
  }
  
  return drawAll(mainDir,configsPath,qcd,signal,mc_production,doSys,doNorm,useLumi,doRebin,useTTBarSF,debugLevel);
}  

int drawAll(TString mainDir,TString configsPath,TString qcd,TString signal,TString mc_production, bool doSys,bool doNorm,bool useLumi,bool doRebin,bool useTTBarSF, int debugLevel){

  SetAtlasStyle();
  
  
  g_doSys=doSys;
  g_doNorm=doNorm;
  g_useLumi=useLumi;
  g_doRebin=doRebin;
  g_useTTBarSF=useTTBarSF;

  auto configlumi= std::make_unique<NtuplesAnalysisToolsConfig>(configsPath+"/config.json");
  g_ttbar_SF=configlumi->ttbar_SF();
  const TString nominalTreeName = configlumi->nominalFolderName().c_str();
  g_nominalTreeName = configlumi->nominalFolderName().c_str();
  g_mc_samples_production = mc_production;
  g_lumi = configFunctions::getLumi(*configlumi);
  if(g_lumi < 0.) {
    cout << "Unknown mc samples production: " << g_mc_samples_production << endl;
    exit(-1);
  }
  TString lumi_string = configFunctions::getLumiString(*configlumi);
  if (g_useTTBarSF)
    g_ttbar_SF=1;


  if(signal=="FSR_up") g_nominalTreeName="ISR_muR05_muF05_Var3cUp";
  if(signal=="FSR_down") g_nominalTreeName="ISR_muR20_muF20_Var3cDown";
  if(signal=="ALPHAS_FSR_up") g_nominalTreeName="FSR_muRfac_20";
  if(signal=="ALPHAS_FSR_down") g_nominalTreeName="FSR_muRfac_05";
  
  if(signal != "nominal")
    g_doSys=false;  // Systematics available only for nominal sample
  
  gErrorIgnoreLevel = kInfo;
  
  double norm_factor=-999;
  
  TString dirname="recoLevelPlots";  
  dirname = dirname + "_Signal_"+ signal + "_Multijet_" + qcd;
  
  TString pathToFigures=mainDir;
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFont(42);
  TH1::AddDirectory(kFALSE);

  TCanvas *can=new TCanvas("can","canvas",0,0,900,600);
  can->SetTicks();
  
  TCanvas *can2=new TCanvas("can2","canvas 2 pads",0,0,600,600);
  can2->cd();
  TPad *pad1 = new TPad("pad1","pad1",0,0.33,1,1,0,0,0);
  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.33,0,0,0);
  pad2->SetBottomMargin(0.3);
  pad2->SetTopMargin(0);
  pad2->SetRightMargin(0.05);
  pad2->SetGridy(1);
  pad2->SetTicks();
  pad1->SetBottomMargin(0.025);
  pad1->SetTopMargin(0.05);
  pad1->SetRightMargin(0.05);
  pad1->SetTicks();
  pad1->Draw();
  pad2->Draw();
  pad1->cd();
  pad2->cd();
  
  TEnv* config= new TEnv(configsPath+"/datasetnames.env");
  
  TString data_filename="";
  if(g_mc_samples_production=="MC16a") data_filename=config->GetValue("data_filename","");
  else if(g_mc_samples_production=="MC16c" || g_mc_samples_production=="MC16d") data_filename=config->GetValue("data17_filename","");
  else if(g_mc_samples_production=="MC16e" || g_mc_samples_production=="MC16f") data_filename=config->GetValue("data18_filename","");
  else if(g_mc_samples_production=="All") data_filename=config->GetValue("data_AllYears_filename","");
//  TString signal_filename=config->GetValue("signal_filename","");
  
  if(debugLevel > 0) {
    cout << "using luminosity string: " <<  lumi_string << endl;
    cout << "using data filename: " << data_filename << endl;
  }
//  cout << signal_filename << endl;  
  g_file_data = new TFile(mainDir+data_filename,"READ");

  g_iNonAllHadrFile=-1;
  
  loadMultijetFile(mainDir,qcd,config);
  loadMCBackgroundFiles(mainDir,config);
  loadSignalFiles(mainDir,signal,config);
  
  
  int nFiles_MC=g_files.size();

  g_files[g_iQCD]->cd();
  vector<TString> systematics=functions::GetSubdirectories(0);
  gDirectory->cd(g_nominalTreeName);
  TString selection="";
  if(qcd.Contains("ABCD16"))selection="Leading_1t_1b_Recoil_1t_1b_tight";
  else selection="2t_incl_2b_incl_tight";
  
  if(qcd.EndsWith("regionK")) selection="Leading_1t_0b_Recoil_1t_1b_tight";
  if(qcd.EndsWith("regionL")) selection="Leading_0t_1b_Recoil_1t_1b_tight";
  if(qcd.EndsWith("regionM")) selection="Leading_1t_1b_Recoil_1t_0b_tight";
  if(qcd.EndsWith("regionN")) selection="Leading_1t_1b_Recoil_0t_1b_tight";
  
  
  if(qcd.EndsWith("regionKS")) selection="Leading_1t_0b_Recoil_1t_1b_tight;Leading_1t_1b_Recoil_1t_1b_tight";
  if(qcd.EndsWith("regionLS") || qcd.EndsWith("regionLS_ljet1_substructure")) selection="Leading_0t_1b_Recoil_1t_1b_tight;Leading_1t_1b_Recoil_1t_1b_tight";
  if(qcd.EndsWith("regionMS")) selection="Leading_1t_1b_Recoil_1t_0b_tight;Leading_1t_1b_Recoil_1t_1b_tight";
  if(qcd.EndsWith("regionNS") || qcd.EndsWith("regionNS_ljet2_substructure")) selection="Leading_1t_1b_Recoil_0t_1b_tight;Leading_1t_1b_Recoil_1t_1b_tight";
  
  
  
  
  vector<TString> selections;
  vector<TString> levels;
  levels.push_back("RecoLevel");
  levels.push_back("unfolding");
  // preparing control plots only for these selections to save time:
  selections.push_back(selection);

  g_files[nFiles_MC-1]->cd();

 
  for (int isel=0;isel<(int)selections.size();++isel)for(unsigned int iLevel=0;iLevel< levels.size();iLevel++){
    TString sel=selections[isel];
    if(debugLevel>0) cout << endl << endl << endl << "preparing plots for selection: " << sel << endl;

    TString sel_label=sel;
    TString multijetRegion=sel;
    if(sel=="Leading_1t_1b_Recoil_1t_1b_tight") {sel_label="Signal Region";      }
    if(sel=="Leading_0t_1b_Recoil_1t_1b_tight") {sel_label="Validation region L";}
    if(sel=="Leading_1t_0b_Recoil_1t_1b_tight") {sel_label="Validation region K";}
    if(sel=="Leading_1t_1b_Recoil_0t_1b_tight") {sel_label="Validation region N";}
    if(sel=="Leading_1t_1b_Recoil_1t_0b_tight") {sel_label="Validation region M";}
    if(sel=="Leading_0t_1b_Recoil_1t_1b_tight;Leading_1t_1b_Recoil_1t_1b_tight"){ sel_label="Regions L+S"; multijetRegion="regionLS";}
    if(sel=="Leading_1t_0b_Recoil_1t_1b_tight;Leading_1t_1b_Recoil_1t_1b_tight"){ sel_label="Regions K+S"; multijetRegion="regionKS";}
    if(sel=="Leading_1t_1b_Recoil_0t_1b_tight;Leading_1t_1b_Recoil_1t_1b_tight"){ sel_label="Regions N+S"; multijetRegion="regionNS";}
    if(sel=="Leading_1t_1b_Recoil_1t_0b_tight;Leading_1t_1b_Recoil_1t_1b_tight"){ sel_label="Regions M+S"; multijetRegion="regionMS";}
    
    TString outputDirSuffix= g_mc_samples_production + "/"+multijetRegion+"/"+dirname;
    if(g_doNorm) outputDirSuffix=g_mc_samples_production + "_normalized/"+multijetRegion+"/"+dirname; // Different directory for normalized histos
    
    gSystem->mkdir(pathToFigures+"png." + outputDirSuffix,1);
    gSystem->mkdir(pathToFigures+"pdf." + outputDirSuffix,1);
    gSystem->mkdir(pathToFigures+"C." + outputDirSuffix,1);
    gSystem->mkdir(pathToFigures+"root." + outputDirSuffix,1);

    stringstream input_stringstream(sel.Data());
    vector<TString> selections;
    std::string token;
    while( getline(input_stringstream,token,';') ){
      selections.push_back(token.c_str());
    }

    g_files[g_iNonAllHadrFile]->cd(g_nominalTreeName + "/"+selections[0]+"/" + levels[iLevel]);
    if(debugLevel>0) cout << g_nominalTreeName + "/"+sel+"/" + levels[iLevel] << endl;
    if(debugLevel>0) cout << "Creating maps with histograms " << endl;
    map<TString, TH1D*> histos1D_recoLevel =functions::GetTH1Ds(2);
    if(debugLevel>0) cout << "TH1D maps created" << endl;
    map<TString, TH2D*> histos2D_recoLevel =functions::GetTH2Ds(0);
    map<TString, TProfile*> profiles_recoLevel =functions::GetTProfiles(0);
    if(debugLevel>0) cout << "Maps created" << endl;
    g_files[nFiles_MC-1]->cd(g_nominalTreeName + "/"+sel+"/" + levels[iLevel]);
    
    if(debugLevel>0) cout << "number of histos at reco level: " << histos1D_recoLevel.size() << endl;
    for (map<TString, TH1D*>::iterator it=histos1D_recoLevel.begin();it!=histos1D_recoLevel.end();++it){
      TString name=it->first;
      if(name.Contains("_half")) continue;
      
      if (debugLevel>0) cout << endl << "name of histogram: " << name << endl;
      if (levels[iLevel]=="unfolding" && name.Contains("_fine")) continue;
      g_rebin=1;
      if (g_doRebin)if (sel.Contains("2t_incl") && name.Contains("_fine")) g_rebin=1;
      if (g_doRebin)if (name.Contains("MET") || name.Contains("split12_fine") || name.Contains("tau21_fine") || name.Contains("tau32_fine") || name.Contains("mass_fine")) g_rebin=1;
      TString fullName=g_nominalTreeName + "/"+sel+"/" + levels[iLevel] + "/"+name;
      
      g_binning.clear();
      
      TH1D* hist_data=0;
      

      vector<TH1D*> histos_MC;
      THStack* stack=0;
      TH1D* hist_allMC=0;
      
      vector<TString> nEvents,nEntries;
      


      if(!loadHistos(g_nominalTreeName, selections,multijetRegion,levels[iLevel],name, hist_data,histos_MC) ){
        continue;
      }
      hist_data->GetXaxis()->Print(); 

      getNEvents(histos_MC,nEvents);
      getNEntries(histos_MC,nEntries);
      
      TString nEvents_data=Form("  %3.1f", hist_data->Integral());
      TString nEntries_data=Form("%3.1f", hist_data->GetEntries());
      
      if(g_doNorm){
        norm_factor = getSignalNormFactor(hist_data,histos_MC); // Per histogram normalization
        //norm_factor = 0.67; // Normalization factor taken from single-bin distribution
        histos_MC[g_iSignalFile]->Scale(norm_factor);
      }
      if(g_doRebin)
        rebinHistos(hist_data,histos_MC,g_rebin);
      if(g_binning.size() > 1)
        rebinHistos(hist_data,histos_MC,g_binning);
      
      
      if(g_useLumi)scaleHistosWithLumi(g_lumi,histos_MC);
      divideByBinWidthHistos(hist_data,histos_MC);
      makeStack(stack,histos_MC);
      makeHistAllMC(hist_allMC,histos_MC);
      
      TLegend *leg = new TLegend(0.57,0.915,0.96,0.915);
      if(name.Contains("ttbar_deltaphi"))leg = new TLegend(0.25,0.91,0.65,0.91);
      leg->SetNColumns(1);
      leg->SetFillColor(0);
      leg->SetFillStyle(0);
      leg->SetBorderSize(0);
      leg->SetTextFont(42);
      leg->SetTextSize(0.035);
      
      const double legRowHeight=0.039;
      
      //leg->AddEntry(hist_data,"Data"+nEvents_data+" ["+nEntries_data+"]");
      //leg->AddEntry(hist_data,"Data"+nEvents_data);
      leg->AddEntry(hist_data,"Data");
    
      float ymin=10000;
      float ymax=-1;
      
      for (int ifile=0; ifile<nFiles_MC;++ifile) {
      
        if (ifile==g_iNonAllHadrFile || ifile==g_iSignalFile)
          histos_MC[ifile]->Scale(g_ttbar_SF);
            
        histos_MC[ifile]->SetFillColor(g_colors[ifile]);
        if (ifile!=nFiles_MC-1)
          histos_MC[ifile]->SetLineColor(g_colors[ifile]);
        histos_MC[ifile]->SetYTitle("Events");
        
        if (ifile==g_iQCD)
          functions::ChangeYRange(histos_MC[ifile],ymin,ymax);
      }
      
      TString NormFactor=Form("%.2f",norm_factor);
      for (int ifile=nFiles_MC-1; ifile>-1;--ifile) {
        if (!g_names[ifile].Contains("Multijet")) {
          if (g_doNorm && (g_names[ifile].Contains("t#bar{t} Allhad")))
            leg->AddEntry(histos_MC[ifile],g_names[ifile]+"#color[4]{ #times"+NormFactor+"}","F");
          else
            leg->AddEntry(histos_MC[ifile],g_names[ifile],"F");
        }
        else
          leg->AddEntry(histos_MC[ifile],g_names[ifile],"F");
      }

      functions::ChangeYRange(hist_data,ymin,ymax);

      functions::ChangeYRange(hist_allMC,ymin,ymax);

      TGraphAsymmErrors* graph_unc=new TGraphAsymmErrors(hist_allMC);
      TGraphAsymmErrors* graph_stat_unc=(TGraphAsymmErrors*)graph_unc->Clone();
      
      
      if(g_doSys)for (int isys=0;isys<(int)systematics.size();++isys) {
        TString sys=systematics[isys];
        if (sys==g_nominalTreeName) continue;
        if (sys.EndsWith("_up")|| sys.EndsWith("_1up")) continue;
              cout << "preparing systematics: " << sys << endl;
        TH1D* reco_pseudodata_sys=0;
        GetRecoSys(reco_pseudodata_sys,hist_allMC,sys,selections,multijetRegion,levels[iLevel],name, false, g_doNorm,norm_factor);
        TH1D* reco_pseudodata_sys2=0;
        if (sys.EndsWith("_down")|| sys.EndsWith("_1down")){
          TString sys_up=sys;
          sys_up.ReplaceAll("_down","_up");
          sys_up.ReplaceAll("_1down","_1up");
          if (!sys.Contains("JET_JER") && (!sys.Contains("JET_MassRes"))) GetRecoSys(reco_pseudodata_sys2,hist_allMC,sys_up,selections,multijetRegion,levels[iLevel],name, false, g_doNorm,norm_factor);
          if (!reco_pseudodata_sys2){
            if(debugLevel>0) cout << "missing systematics. Skipping." << endl;
            continue;
          }
        }
        if (sys=="jer" || sys=="FatJPtR" || sys=="Wjets_shape_iqopt3" || sys=="Wjets_shape_ptjmin25" || sys=="MU_MS_SMEAR" || sys=="MU_ID_SMEAR"){
          GetRecoSys(reco_pseudodata_sys2,hist_allMC,sys,selections,multijetRegion,levels[iLevel],name, true, g_doNorm,norm_factor);
          if(g_doRebin)reco_pseudodata_sys2->Rebin(g_rebin);
          if (!reco_pseudodata_sys2){
            if(debugLevel>0) cout << "missing systematics. Skipping." << endl;
            continue;
          }
        } 

        for (int ibin=1;ibin<graph_unc->GetN()+1;++ibin) {
          double low=graph_unc->GetErrorYlow(ibin-1);
          double high=graph_unc->GetErrorYhigh(ibin-1);
          double diff2=0;
          double diff1=reco_pseudodata_sys->GetBinContent(ibin)-hist_allMC->GetBinContent(ibin);
          if (reco_pseudodata_sys2) diff2=reco_pseudodata_sys2->GetBinContent(ibin)-hist_allMC->GetBinContent(ibin);
          double high_sys=0;
          double low_sys=0;
          if (diff1>diff2 && diff1>0) high_sys=diff1;
          if (diff2>diff1 && diff2>0) high_sys=diff2; 
          if (diff1<diff2 && diff1<0) low_sys=diff1;
          if (diff2<diff1 && diff2<0) low_sys=diff2; 
          double new_low=sqrt(pow(low,2)+pow(low_sys,2));
          double new_high=sqrt(pow(high,2)+pow(high_sys,2));
          graph_unc->SetPointEYlow(ibin-1,new_low);
          graph_unc->SetPointEYhigh(ibin-1,new_high);
        }


      }
      if(debugLevel>0) {
        cout << endl << endl << "Printing stat uncertainty of the prediction:"; //JH
        for (int ibin=1;ibin<graph_unc->GetN()+1;++ibin)
          cout << " " << graph_stat_unc->GetErrorYlow(ibin-1);
        cout << endl;
        cout << "Printing stat. + sys. uncertainty of the prediction:";
        for (int ibin=1;ibin<graph_unc->GetN()+1;++ibin) cout << " " << graph_unc->GetErrorYlow(ibin-1);
        cout << endl << endl << endl;
      }

      double yTitleOffsetSF=1.0;
     
      hist_data->GetYaxis()->SetTitleSize(0.065);
      hist_data->GetYaxis()->SetLabelSize(0.06);
      hist_data->GetXaxis()->SetLabelSize(0.0);
      hist_data->GetYaxis()->SetTitleOffset(1.*yTitleOffsetSF);
      hist_data->SetYTitle("Events");
     

      float ymax_log=ymax*(1+pow(ymax/ymin,1./2.));// pow(10,log10(ymax/ymin)*1.4);
      float ymin_log=ymin/2;
	 
      if (name.Contains("MET") || name.Contains("D12") || name.Contains("delta"))
        ymax_log=ymax*(1+pow(ymax/ymin,1./1.9));
            
      stack->SetMinimum(ymin);
      stack->SetMaximum(ymax*1.9);        
      hist_data->GetYaxis()->SetRangeUser(0.01*ymin,ymax*1.8);

      if (name.Contains("_eta") && !name.Contains("y_minus_eta")) {
        stack->SetMaximum(ymax*2);
        hist_data->GetYaxis()->SetRangeUser(0.01*ymin,ymax*2);
        double x_max_eta = 2.5;
        
        if (name.Contains("fatJet") or name.Contains("top") or name.Contains("Top"))
          x_max_eta = 2.0;
        else 
          x_max_eta = 2.5;
        
        hist_data->GetXaxis()->SetRangeUser(0.0,x_max_eta);
      }
      if (name.Contains("_tau32")) {
        double x_max_tau32 = 1.0;
        hist_data->GetXaxis()->SetRangeUser(0.0,x_max_tau32);
      }
      
      if (name.Contains("_phi")){
        stack->SetMaximum(ymax*1.92);
        hist_data->GetYaxis()->SetRangeUser(0.01*ymin,ymax*1.92);
      }
      pad1->cd();
            
      hist_data->Draw();
      if(g_doSys){
        graph_unc->SetFillStyle(1001);
        graph_unc->SetFillColor(kGray+1);
        graph_unc->SetLineColor(kGray+1);
        graph_stat_unc->SetFillStyle(1001);
        graph_stat_unc->SetFillColor(kGray);
        graph_stat_unc->SetLineColor(kGray);
      }
      else {
        graph_stat_unc->SetFillStyle(1001);
        graph_stat_unc->SetFillColor(kGray+1);
        graph_stat_unc->SetLineColor(kGray+1);
      }
      if(g_doSys)graph_unc->Draw("same2");
      graph_stat_unc->Draw("same2"); 
      stack->Draw("HISTsame"); 
      hist_data->Draw("same");
      
      leg->AddEntry(graph_stat_unc,"MC Stat. Unc.","F");
      if(g_doSys)leg->AddEntry(graph_unc,"MC Stat. #oplus Det. Syst. Unc.","F");
      
      leg->Draw();
      leg->SetY1( leg->GetY1() - legRowHeight * leg->GetNRows() );
      TString yaxis_type="Events";
      TString yaxis_unit=" / 1.0"; 
      if (name.Contains("_pt") || name.Contains("_energy") || name.Contains("_mass")  || name.Contains("MET")  || name.Contains("MTW")  || name.Contains("_D12") ) yaxis_unit=" / GeV";
      if (name.Contains("ttbar_mass"))yaxis_unit=" / TeV";
      
      stack->GetYaxis()->SetTitle(yaxis_type+yaxis_unit);
      hist_data->GetYaxis()->SetTitle(yaxis_type+yaxis_unit);

      pad1->Update();
      pad1->RedrawAxis("g");
      pad1->RedrawAxis();
      
      pad2->cd();
      
      TGraphAsymmErrors* unit=GetUnitGraph(graph_unc);
      TGraphAsymmErrors* unit_stat=GetUnitGraph(graph_stat_unc);
      // use this for ratio Data/Pred. in the ratio plots:                                                                                                                            
      TH1D* ratio=(TH1D*)hist_data->Clone();
      for(int ibin=1;ibin<=ratio->GetNbinsX();ibin++){
        double bincontent = hist_allMC->GetBinContent(ibin)>0 ? hist_data->GetBinContent(ibin)/hist_allMC->GetBinContent(ibin) : 0.;
        ratio->SetBinContent(ibin,bincontent);
        double binerror = hist_allMC->GetBinContent(ibin)>0 ? hist_data->GetBinError(ibin)/hist_allMC->GetBinContent(ibin) : 0.;
        ratio->SetBinError(ibin,binerror);
      }

      ratio->SetYTitle("Data / Pred.");
      ratio->GetXaxis()->SetTitleSize(0.13);
      ratio->GetXaxis()->SetTitleOffset(1.05);
      ratio->GetYaxis()->SetTitleSize(0.13);
      ratio->GetYaxis()->SetTitleOffset(0.5*yTitleOffsetSF);
      ratio->GetXaxis()->SetLabelSize(0.123);
      ratio->GetYaxis()->SetLabelSize(0.123);
      ratio->GetYaxis()->SetNdivisions(10);
      if (sel.Contains("2t_incl") || sel.Contains("Leading_"))
        ratio->GetYaxis()->SetRangeUser(0.4001,1.599);
      else
        ratio->GetYaxis()->SetRangeUser(0.2,3.99);
      
      ratio->SetLineColor(1);
      
      ratio->Draw();
      unit->Draw("same2");
      unit_stat->Draw("same2");
      //   unit->Draw("samelx");
      ratio->Draw("same");
      
      pad2->RedrawAxis("g"); 
      pad2->RedrawAxis(); 
      
      pad1->cd();
      functions::WriteGeneralInfo(sel_label,lumi_string,0.049,0.2,0.87,0.05);
     
      TString signalSampleInfo="";
      if(signal=="nominal")signalSampleInfo="POWHEG+Pythia8";
      if(signal=="PS")signalSampleInfo="POWHEG+Herwig7";
      if(signal=="ME")signalSampleInfo="MG5_aMCatNLO+Pythia8";
      if(signal=="FSRup")signalSampleInfo="POWHEG+Pythia8,IFSR up";
      if(signal=="FSRdown")signalSampleInfo="POWHEG+Pythia8,IFSR down";
      if(signal=="ALPHAS_FSR_up")signalSampleInfo="Pow+Py8,#alpha_{s} FSR up";
      if(signal=="ALPHAS_FSR_down")signalSampleInfo="Pow+Py8,#alpha_{s}  FSR down";
      if(debugLevel>0) cout << "signalSampleInfo: " << signalSampleInfo << endl;
      functions::WriteInfo(NamesAndTitles::analysisLabel(), 0.049, 0.2, 0.72);
    
      can2->SaveAs(pathToFigures+"pdf." + outputDirSuffix+"/"+name+".pdf");
      can2->SaveAs(pathToFigures+"png." + outputDirSuffix+"/"+name+".png");

      stack->SetMinimum(ymin_log);
      stack->SetMaximum(ymax_log);
      hist_data->GetYaxis()->SetRangeUser(ymin_log,ymax_log);
      pad1->Update();
      pad1->cd();
      gPad->SetLogy();

      can2->SaveAs(pathToFigures+"pdf." + outputDirSuffix + "/" + name + "_log.pdf");
      can2->SaveAs(pathToFigures+"png." + outputDirSuffix + "/" + name + "_log.png");
      gPad->SetLogy(0);
    }
  }
  
  return 0;
}



bool loadHistos(TString sys, vector<TString>& selections,TString multijetRegion,TString level,TString name,TH1D*& h_data,vector<TH1D*>& histos){
  
  TH1D* helphist=0;
  const int nselections=selections.size();
  
  for(int isel=0;isel<nselections;isel++){
    
    TString fullName = sys + "/" + selections[isel] + "/" + level + "/" + name;
    
    helphist=(TH1D*)g_file_data->Get(fullName);
    if (!helphist){
       cout << "\nError: Missing histogram!\nhistname: " << fullName << "\nfilename: " << g_file_data->GetName() << endl << endl;
       return false;
    }
    if(isel==0)h_data=(TH1D*)helphist->Clone();
    else h_data->Add(helphist);
  }
  
  return loadMCHistos(sys, selections,multijetRegion,level,name,histos);
}

bool loadMCHistos(TString sys, vector<TString>& selections,TString multijetRegion,TString level,TString name,vector<TH1D*>& histos){
  TH1D* helphist=0;
  const int nselections=selections.size();
  int nFilesMC=g_files.size();
  histos.resize(nFilesMC);
  
  TString fullName;
  
  for (int ifile=0; ifile<nFilesMC;++ifile){
    if(ifile==g_iQCD)continue;
    for(int isel=0;isel<nselections;isel++){
    
      fullName = sys + "/" + selections[isel] + "/" + level + "/" + name;    
      if( (ifile == g_iSignalFile) && (sys==g_nominalTreeName) ) fullName= g_nominalTreeName + "/" + selections[isel] + "/" + level + "/" + name;
    
    
      
      helphist=(TH1D*)g_files[ifile]->Get(fullName);
      if (!helphist){
	 cout << "\nError: Missing histogram!\nhistname: " << fullName << "\nfilename: " << g_files[ifile]->GetName() << endl << endl;
	 return false;
      }
      if(isel==0)histos[ifile] = (TH1D*)helphist->Clone();
      else histos[ifile]->Add(helphist);
      
    }
  }
  helphist=(TH1D*)g_files[g_iQCD]->Get(sys + "/" + multijetRegion + "/" + level + "/" + name);
  if (!helphist) helphist=(TH1D*)g_files[g_iQCD]->Get((TString)g_nominalTreeName + "/" + multijetRegion + "/" + level + "/" + name);
  if (!helphist){
     cout << "\nError: Missing histogram with multijet background estimate!\nhistname: " << name << "\nfilename: " << g_files[g_iQCD]->GetName() << endl << endl;
     return false;
  }
  histos[g_iQCD] = (TH1D*)helphist->Clone();
  
  return true;
}


double getSignalNormFactor(TH1D* h_data,vector<TH1D*>& histos){  
  const int nhistos=histos.size();
  const double data_integral=h_data->Integral();
  double bkg_integral(0.);
  double signal_integral(0.);
  for(int i=0;i<nhistos;i++){
    if(i!=g_iSignalFile)bkg_integral+=histos[i]->Integral();
    else signal_integral+=histos[i]->Integral();
  }
  return (data_integral - bkg_integral)/signal_integral;
}

void rebinMCHistos(vector<TH1D*>& histos,int rebin){
  const int nhistos=histos.size();
  for(int i=0;i<nhistos;i++){
    histos[i]->Rebin(rebin);
  }
}

void rebinMCHistos(vector<TH1D*>& histos,const vector<double>& binning){
  const int nhistos=histos.size();
  for(int i=0;i<nhistos;i++){
    histos[i] = (TH1D*)histos[i]->Rebin(binning.size()-1,histos[i]->GetName(),&binning[0]);
  }
}

void rebinHistos(TH1D* h_data,vector<TH1D*>& histos,int rebin){
  h_data->Rebin(rebin);
  rebinMCHistos(histos,rebin);
}

void rebinHistos(TH1D*& h_data,vector<TH1D*>& histos,const vector<double>& binning) {
  
  h_data = (TH1D*)h_data->Rebin(binning.size()-1,h_data->GetName(),&binning[0]);
  rebinMCHistos(histos,binning);
}

void divideByBinWidthHistos(TH1D* h_data,vector<TH1D*>& histos){
  functions::DivideByBinWidth(h_data);
  const int nhistos=histos.size();
  for(int i=0;i<nhistos;i++){
    functions::DivideByBinWidth(histos[i]);
  }
}

void scaleHistosWithLumi(double lumi,vector<TH1D*>& histos){
  const int nhistos=histos.size();
  for(int i=0;i<nhistos;i++){
    if(i==g_iQCD)continue;
    histos[i]->Scale(lumi*1000.);
  } 
}

void makeStack(THStack*& hstack,vector<TH1D*>& histos){
  const int nhistos=histos.size();
  
  hstack = new THStack("stack","stacked histograms");    
  for(int i=0;i<nhistos;i++){
    hstack->Add(histos[i]);
  }
}

void makeHistAllMC(TH1D*& histAllMC,vector<TH1D*>& histos){
  const int nhistos=histos.size();

  histAllMC=(TH1D*)histos[0]->Clone();
  for(int i=1;i<nhistos;i++){
    histAllMC->Add(histos[i]);
  }
}

void getNEvents(vector<TH1D*>& histos,vector<TString>& nevents){
  const int nhistos=histos.size();
  nevents.resize(nhistos);
  for(int i=0;i<nhistos;i++)nevents[i]=Form("  %3.1f", histos[i]->Integral());
  
  
}
void getNEntries(vector<TH1D*>& histos,vector<TString>& nentries){
  const int nhistos=histos.size();
  nentries.resize(nhistos);
  for(int i=0;i<nhistos;i++)nentries[i]=Form("%.0f",histos[i]->GetEntries());
  
}


TGraphAsymmErrors* GetUnitGraph(TGraphAsymmErrors* graph){
  TGraphAsymmErrors* unit=(TGraphAsymmErrors*)graph->Clone();
  for (int i=0;i<graph->GetN();++i){
    double x,y;
    graph->GetPoint(i,x,y);
    unit->SetPoint(i,x,1);
    double low=unit->GetErrorYlow(i);
    double high=unit->GetErrorYhigh(i);
    //    cout << "i: " << i << "  x: " << x << "  y: " << y << "  ey_low: " << low << "  ey_high: " << high << endl;
    double rel_low= y!=0. ? low/y : 0.;
    double rel_high=y!=0. ? high/y: 0.;
    if (low < 0 || y<1e-5 || std::isnan(rel_low) || std::isinf(rel_low)) rel_low=0;
    if (high < 0 || y<1e-5 || std::isnan(rel_high) || std::isinf(rel_high)) rel_high=0;
    // use this for ratio Data/Pred. in the ratio plots:
    unit->SetPointEYlow(i,rel_low);
    unit->SetPointEYhigh(i,rel_high);
    // use this for ratio Pred./Data in the ratio plot:
    //unit->SetPointEYlow(i,rel_high);
    //unit->SetPointEYhigh(i,rel_low);
  }
  return unit;
}


void GetRecoSys(TH1D*& hist_sys,TH1D* hist_nominal,TString sys, vector<TString>& selections,TString multijetRegion,TString level,TString name, bool doSym, bool doNorm, float norm_factor){
  
  vector<TH1D*> histos;
  loadMCHistos(sys,selections,multijetRegion,level,name,histos);
  
  if(doNorm){
    histos[g_iSignalFile]->Scale(norm_factor);
  }
  if(g_doRebin)rebinMCHistos(histos,g_rebin);
  if(g_binning.size()>1) {
    rebinMCHistos(histos,g_binning);
  }
  if(g_useLumi)scaleHistosWithLumi(g_lumi,histos);

  const int nfiles=g_files.size();
  for (int ifile=0; ifile<nfiles;ifile++){
    if(ifile==0)hist_sys=(TH1D*)histos[ifile]->Clone();
    else hist_sys->Add(histos[ifile]);
    
  }
  
  functions::DivideByBinWidth(hist_sys);
  
  
  if (doSym){
    hist_sys->Scale(-1);
    hist_sys->Add(hist_nominal,2);
  }
  
  
}




//draw ttbar mass in TeV scale
int histSetLimits(TH1* hist, double scaleFactor)
{
  /* does not work for variable size binning
  double xmin = hist->GetXaxis()->GetXmin() * scaleFactor;
  double xmax = hist->GetXaxis()->GetXmax() * scaleFactor;
  //cout << "xmin, xmax limits: " << xmin << " " << xmax << endl;

  hist->GetXaxis()->SetLimits(xmin, xmax);
  */
  
  const TArrayD* ar1 = hist->GetXaxis()->GetXbins();
  TArrayD ar2;

  ar1->Copy(ar2);

  for (int it=0; it < ar1->GetSize(); it++)  {
    ar2.SetAt( ar1->At(it) * scaleFactor, it);
  }
  
  hist->GetXaxis()->Set( ar2.GetSize()-1, ar2.GetArray() );
  
  return 0;  
}

int histSetLimits(TGraph* hist, double scaleFactor)
{
  /* does not work for variable size binning
  double xmin = hist->GetXaxis()->GetXmin() * scaleFactor;
  double xmax = hist->GetXaxis()->GetXmax() * scaleFactor;
  //cout << "xmin, xmax limits: " << xmin << " " << xmax << endl;

  hist->GetXaxis()->SetLimits(xmin, xmax);
  */
  
  const TArrayD* ar1 = hist->GetXaxis()->GetXbins();
  TArrayD ar2;

  ar1->Copy(ar2);

  for (int it=0; it < ar1->GetSize(); it++)  {
    ar2.SetAt( ar1->At(it) * scaleFactor, it);
  }
  
  hist->GetXaxis()->Set( ar2.GetSize()-1, ar2.GetArray() );
  
  return 0;  
}

void loadMultijetFile(TString mainDir,TString qcd,TEnv* config){
  
   
   
   /*// ttH
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttH125_dilep_343365_filename",""),"READ"));
    g_names.push_back("ttH dilep;
    g_colors.push_back(801);
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttH125_semilep_343366_filename",""),"READ"));
    g_names.push_back("ttH semilep");
    g_colors.push_back(801);
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttH125_allhad_343367_filename",""),"READ"));
    g_names.push_back("ttH allhad");
    g_colors.push_back(801);
    
    // ttW
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttW_410155_filename",""),"READ"));
    g_names.push_back("ttW");
    g_colors.push_back(801);
    
    // ttZqq
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttZqq_410157_filename",""),"READ"));
    g_names.push_back("ttZqq");
    g_colors.push_back(801);*/  


  if(qcd=="MC"){
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("mc_dijet_PowhegPythia8",""),"READ")); 
    g_names.push_back("PowhegPythia dijets");
    g_colors.push_back(619);
  }
  if( qcd == "MM"){
    g_files.push_back(new TFile(mainDir +"QCDBackgroundEstimationMM/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619); 
  }
  if( qcd == "MM_v2"){
    g_files.push_back(new TFile(mainDir +"QCDBackgroundEstimationMM_v2/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619); 
  }
  if( qcd == "MM_4x4"){
    g_files.push_back(new TFile(mainDir +"MM_4x4_QCD_Estimate/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619);
  }
  if( qcd == "MM_4x4_noMC"){
    g_files.push_back(new TFile(mainDir +"MM_4x4_noMC_QCD_Estimate/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619);
  }
  if( qcd == "MM_2x2"){
    g_files.push_back(new TFile(mainDir +"MM_2x2_QCD_Estimate/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619);
  }
  if( qcd == "MM16"){
    g_files.push_back(new TFile(mainDir +"QCDBackgroundEstimationMM16/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619); 
  }
  if( qcd == "MM16_v2"){
    g_files.push_back(new TFile(mainDir +"QCDBackgroundEstimationMM16_v2/allhad.boosted.output.root","READ"));
    g_names.push_back("MultijetMM");
    g_colors.push_back(619); 
  }
  if( qcd == "ABCD9_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD9_Averaged",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD4_BBB_Averaged_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD4_BBB_Averaged",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD9_modified_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD9_Averaged_modified",""),"READ"));  
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD9_BBB_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD9_BBB_Averaged",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD9_BBB_modified_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD9_BBB_Averaged_modified",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD9_BBB_Combined_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD9_BBB_Combined",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_loose"){
    g_files.push_back(new TFile(mainDir +"QCDBackgroundEstimation_ABCD16_loose/allhad.boosted.output.root","READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_loose"){
    g_files.push_back(new TFile(mainDir +"QCDBackgroundEstimation_ABCD16_BinByBin_loose/allhad.boosted.output.root","READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_Averaged",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_Averaged",""),"READ"));
    g_names.push_back("Multijet");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_S1_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_S1",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_S9_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_S9",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_bjets_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_bjets",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_bjet1_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_bjet1",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_ljet1_tau32_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_ljet1_tau32",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_ljet2_tau32_tight"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_ljet2_tau32",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionK"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionK",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionL"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionL",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionM"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionM",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionN"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionN",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionKS"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionKS",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionLS"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionLS",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionMS"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionMS",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionNS"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionNS",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionLS_ljet1_substructure"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionLS_ljet1_substructure",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  if( qcd == "ABCD16_BBB_regionNS_ljet2_substructure"){
    g_files.push_back(new TFile(mainDir + "/ABCD."+ g_mc_samples_production + "/" + config->GetValue("ABCD16_BBB_regionNS_ljet2_substructure",""),"READ"));
    g_names.push_back("Multijet ");
    g_colors.push_back(619);
  }
  
   // MC dijets
  if(qcd=="MC_LO"){
    g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("mc_dijet_Pythia8",""),"READ"));
    g_names.push_back("Pythia8 JZxW dijets");
    g_colors.push_back(619);
  }  

  g_iQCD=g_files.size()-1;
  
}

void loadMCBackgroundFiles(TString mainDir,TEnv* config){
  
  //ttHWZ
  g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttHWZ_filename",""),"READ"));
  g_names.push_back("t#bar{t}+W/Z/H");
  g_colors.push_back(800); //801

  // Single top
  g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("singletop_filename",""),"READ"));
  g_names.push_back("Single top");
  g_colors.push_back(600);
  
  // non ttbar background
  g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." +config->GetValue("ttbar_nonallhad_filename",""),"READ"));
  g_names.push_back("t#bar{t} Non Allhad");
  g_iNonAllHadrFile=g_names.size()-1;
  g_colors.push_back(432); //796
  
}
void loadSignalFiles(TString mainDir,TString signal,TEnv* config){
  
  // signal
  if(signal=="410007") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410007_filename",""),"READ"));
  if(signal=="410234") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410234_filename",""),"READ"));
  if(signal=="410506") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_nominal_filename",""),"READ"));
  if(signal=="410521") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410521_filename",""),"READ"));
  if(signal=="410522") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410522_filename",""),"READ"));
  if(signal=="410530") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410530_filename",""),"READ"));
  if(signal=="410008") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410008_filename",""),"READ"));
  if(signal=="410045") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410045_filename",""),"READ"));
  if(signal=="410160") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("signal_410160_filename",""),"READ"));
  if(signal=="410471") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_410471_filename",""),"READ"));
  if(signal=="nominal") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_nominal_filename",""),"READ"));
  if(signal=="PS") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_PhH713_filename",""),"READ"));
  if(signal=="PhH713") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_PhH713_filename",""),"READ"));
  if(signal=="PhH704") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_PhH704_filename",""),"READ"));
  if(signal=="ME") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_ME_filename",""),"READ"));
  if(signal=="PhPy8MECoff") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_PhPy8MECoff_filename",""),"READ"));
  if(signal=="hdamp") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_hdamp_filename",""),"READ"));
  if(signal=="ISR_down") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_nominal_filename",""),"READ"));
  if(signal=="ALPHAS_FSR_down") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_nominal_filename",""),"READ"));
  if(signal=="ALPHAS_FSR_up") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_nominal_filename",""),"READ"));
  if(signal=="Sherpa") g_files.push_back(new TFile(mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_sherpa_filename",""),"READ"));
  
  g_names.push_back("t#bar{t} Allhad");
  g_iSignalFile=g_files.size()-1;
  g_colors.push_back(0);
  
  cout << mainDir + g_mc_samples_production + "." + config->GetValue("ttbar_signal_nominal_filename","") << endl;
  cout << g_files[g_iSignalFile]->GetName() << endl;
  
}




