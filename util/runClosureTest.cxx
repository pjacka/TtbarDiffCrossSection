
#include "CovarianceCalculator/ClosureTestMaker.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"

#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <memory>
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TSystem.h"

#include "docopt.h"

using namespace std;

void setupClosureTestMaker(ClosureTestMaker* closureTestMaker, TFile* f, const TString& subdir, const TString& variableName);

void drawComparison(TH1D* unfolded,
		    TH1D* truth,
		    const TString& dirname,
		    const TString& figureName,
		    const vector<TString>& testResult,
		    const double y2min,
		    const double y2max);

void drawComparionND(TH1D* unfolded,
		   TH1D* truth,
		   const HistogramNDto1DConverter* histogramConverterND,
		   MultiDimensionalPlotsSettings* plotSettingsND,
		   const TString& variable,
		   const TString& level,
		   const TString& dirname,
		   const TString& figureName,
		   const vector<TString>& testResult,
		   const double ymin,
		   const double ymax);

int runClosureTest(const TString& mainDir,
		 const TString& pathConfigLumi,
		 const TString& pathConfigBinning,
		 const TString& pathMultiDimensionalPlotsConfig,
		 const TString& outputDir,
		 const TString& closureTestHistosDir,
		 const TString& variableName,
		 const TString& level,
		 const TString& dimension,
		 const int debug);

// This is not just print message! It is used to define input parameters and default values!
// See documentation in https://github.com/docopt/docopt.cpp
static const char * USAGE =
    R"(Program to run closure test of unfolding procedure.  

Usage:  
  runClosureTest [options]
  
Options:
  -h --help                                      Show help screen.
  --mainDir <mainDir>                            Main directory for all output. [default: $TtbarDiffCrossSection_output_path]
  --config <config>                              Path to ttbar config file. [default: $TTBAR_PATH/TtbarDiffCrossSection/data/config.env]
  --configBinning <configBinning>                Path to file with binning. [default: TtbarDiffCrossSection/data/optBinningND/ttbar_y_vs_ttbar_mass_vs_t1_pt.env]
  --multiDimensionalPlotsConfig <string>         Path to config file with plots settings. [default: TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env]
  --variable <variable>                          Variable name. [default: total_cross_section]
  --level <level>                                ParticleLevel or PartonLevel. [default: ParticleLevel]
  --dimension <dimension>                        1D or ND. [default: 1D]
  --closureTestHistosDir <stressTestHistosDir>    Directory with closure test histos rootfiles. [default: systematics_histos]
  --outputDir <outputDir>                        Directory to store output. Will be created in maindir/pdf.<MC> . [default: ClosureTestResults]
  --debugLevel <int>                             Option to control debug messages. Higher value means more messages. [default: 0]
)";


int main(int nArg, char **arg) {
  SetAtlasStyle();
  TH1::AddDirectory(kFALSE);
  int result=0;
  try {
    
    TString mainDir="";
    TString pathConfigLumi="";
    TString pathConfigBinning("");
    TString pathMultiDimensionalPlotsConfig="";
    TString variableName="";
    TString level="";
    TString dimension="";
    TString closureTestHistosDir="";
    TString outputDir="";
    int debug=0;
    
    std::map<std::string, docopt::value> args = docopt::docopt(USAGE, {arg+1, arg + nArg}, true); // Using docopt package to read parameters
  
    try{
    
      try{ mainDir=args["--mainDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--mainDir option is expected to be a string. Check input parameters."); }
	
      try{ pathConfigLumi=args["--config"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--config option is expected to be a string. Check input parameters."); }
      cout << "Loading configBinning" << endl;
      try{ pathConfigBinning=args["--configBinning"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--configBinning option is expected to be a string. Check input parameters."); }
	      cout << "Loading multiDimensionalPlotsConfig" << endl;

      try{ pathMultiDimensionalPlotsConfig=args["--multiDimensionalPlotsConfig"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--multiDimensionalPlotsConfig option is expected to be a string. Check input parameters."); }

      try{ closureTestHistosDir=args["--closureTestHistosDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--closureTestHistosDir option is expected to be a string. Check input parameters."); }
	
      try{ outputDir=args["--outputDir"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--outputDir option is expected to be a string. Check input parameters."); }
	
      try{ variableName=args["--variable"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--variable option is expected to be a string. Check input parameters."); }
	
      try{ level=args["--level"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--level option is expected to be a string. Check input parameters."); }
	cout << "Loading dimension" << endl;
      try{ dimension=args["--dimension"].asString();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--dimension option is expected to be a string. Check input parameters."); }
      
      try{ debug=args["--debugLevel"].asLong();} catch(const std::invalid_argument& e)
	{ throw std::invalid_argument("--debugLevel option is expected to be an integer. Check input parameters."); }
	
    } catch(const std::invalid_argument& e) {
	cout << e.what() << endl << endl;
	cout << USAGE << endl;
	return -1;
      }
    cout << "Loading parameters done." << endl;
    if (mainDir.Contains("$TtbarDiffCrossSection_output_path")) mainDir.ReplaceAll("$TtbarDiffCrossSection_output_path",gSystem->Getenv("TtbarDiffCrossSection_output_path")); // If not set the default value is $TtbarDiffCrossSection_output_path
    if (pathConfigLumi.Contains("$TTBAR_PATH")) pathConfigLumi.ReplaceAll("$TTBAR_PATH",gSystem->Getenv("TTBAR_PATH"));
    
    
    
    result = runClosureTest(mainDir,pathConfigLumi,pathConfigBinning, pathMultiDimensionalPlotsConfig,outputDir,closureTestHistosDir,variableName,level,dimension,debug);
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  return result;
  
}
    
int runClosureTest(const TString& mainDir,
		 const TString& pathConfigLumi,
		 const TString& pathConfigBinning,
		 const TString& pathMultiDimensionalPlotsConfig,
		 const TString& outputDir,
		 const TString& closureTestHistosDir,
		 const TString& variableName,
		 const TString& level,
		 const TString& dimension,
		 const int /*debug*/){
   
  auto config = std::make_unique<NtuplesAnalysisToolsConfig>(pathConfigLumi.Data());  
  TString mc_samples_production = config->mc_samples_production();
  
  std::unique_ptr<HistogramNDto1DConverter> histogramConverterND_truth;
  std::unique_ptr<MultiDimensionalPlotsSettings> plotSettingsND;
  TString levelShort=level;
  levelShort.ReplaceAll("Level","");
  
  if(dimension=="ND") {
    histogramConverterND_truth = std::make_unique<HistogramNDto1DConverter>(pathConfigBinning.Data());
    histogramConverterND_truth->initializeOptBinning((variableName + "_"+levelShort+"_truth").Data());
    histogramConverterND_truth->initialize1DBinning();
    
    plotSettingsND = std::make_unique<MultiDimensionalPlotsSettings>(pathMultiDimensionalPlotsConfig.Data());
  }
  
  
  
  TString rootDir=(TString)"root."+mc_samples_production;
  
  const TString dirname_png = mainDir + "/" + "png." + mc_samples_production + "/" + outputDir + "/";
  const TString dirname_pdf = mainDir + "/" + "pdf." + mc_samples_production + "/" + outputDir + "/";
  gSystem->mkdir(dirname_png,true);
  gSystem->mkdir(dirname_pdf,true);
  
  
  auto fileClosureTestHistos = std::make_unique<TFile>(mainDir+"/"+rootDir+"/"+closureTestHistosDir+"/" + variableName + "_" + level + ".root");
  if(fileClosureTestHistos->IsZombie()){
    throw TtbarDiffCrossSection_exception((TString)"Error in loading rootfile with data from " + mainDir+"/"+rootDir+"/"+closureTestHistosDir+"/" + variableName + "_" + level + ".root"); 
    return -1;
  }
  
  delete gRandom;
  gRandom = new TRandom3;
  gRandom->SetSeed(42);
  
  const TString subdir = "nominal/ClosureTest";
  
  auto closureTestMaker = std::make_unique<ClosureTestMaker>();
  
  setupClosureTestMaker(closureTestMaker.get(),fileClosureTestHistos.get(), subdir, variableName);
  closureTestMaker->setNToys(100000);
  closureTestMaker->setDebugLevel(1);
  
  closureTestMaker->runPseudoExperiments();
  
  const double pValue = closureTestMaker->getPValue();
  const double chi2 = closureTestMaker->getChi2();
  const int ndf = closureTestMaker->getNDF();
  
  std::cout << "Closure test result: p-value: " << pValue  << " chi2/ndf: " << chi2 << "/" << ndf << std::endl;
  
  vector<TString> testResult = {Form("p-value: %4.2f",pValue),Form("chi2/ndf: %4.2f/%i",chi2,ndf)};
  
  
  auto hResult = std::make_unique<TH1D>("hResult","p-value, chi2, ndf",3,0,3);
  hResult->SetBinContent(1,pValue);
  hResult->SetBinContent(2,chi2);
  hResult->SetBinContent(3,ndf);
  hResult->GetXaxis()->SetBinLabel(1,"p-value");
  hResult->GetXaxis()->SetBinLabel(2,"chi2");
  hResult->GetXaxis()->SetBinLabel(3,"ndf");
  
  const TString outputDirFull = mainDir + "/" + rootDir + "/" + outputDir;
  gSystem->mkdir(outputDirFull,true);
  
  auto truth1 = std::unique_ptr<TH1D>{(TH1D*)closureTestMaker->getTruth1()->Clone()};
  auto unfolded = std::unique_ptr<TH1D>{(TH1D*)closureTestMaker->getUnfolded()->Clone()};
  const TMatrixT<double>* covariance = closureTestMaker->getCovariance();
  
  auto outputFile = std::make_unique<TFile>(outputDirFull + "/" + variableName + "_" + level + ".root","recreate");
  
  hResult->Write("result");
  truth1->Write("truth1");
  unfolded->Write("unfolded");
  
  outputFile->Close();
  
  functions::setBinErrors(unfolded.get(),covariance);
  functions::SetErrorsToZero(truth1.get());
  
  TString figureName=variableName + "_" + level;
  
  if(dimension=="1D") {
    double y2min=0.9;
    double y2max=1.1;
    drawComparison(unfolded.get(),truth1.get(),dirname_pdf,figureName+".pdf",testResult,y2min,y2max);
    drawComparison(unfolded.get(),truth1.get(),dirname_png,figureName+".png",testResult,y2min,y2max);
  }
  else if(dimension=="ND") {
    double ymin=0.9;
    double ymax=1.1;
    drawComparionND(unfolded.get(),
		    truth1.get(),
		    histogramConverterND_truth.get(),
		    plotSettingsND.get(),
		    variableName,
		    level,
		    dirname_pdf,
		    figureName,
		    testResult,
		    ymin,
		    ymax);
  }
  
  
  return 0;
}

void setupClosureTestMaker(ClosureTestMaker* closureTestMaker,TFile* f, const TString& subdir, const TString& variableName) {
  
  auto hReco1 = std::shared_ptr<TH1D>{(TH1D*)f->Get(subdir + "/" + variableName + "_reco1")};
  auto hReco2 = std::shared_ptr<TH1D>{(TH1D*)f->Get(subdir + "/" + variableName + "_reco2")};
  auto hTruth1 = std::shared_ptr<TH1D>{(TH1D*)f->Get(subdir + "/" + variableName + "_truth1")};
  auto hTruth2 = std::shared_ptr<TH1D>{(TH1D*)f->Get(subdir + "/" + variableName + "_truth2")};
  auto hMigration1 = std::shared_ptr<TH2D>{(TH2D*)f->Get(subdir + "/" + variableName + "_migration1")};
  auto hMigration2 = std::shared_ptr<TH2D>{(TH2D*)f->Get(subdir + "/" + variableName + "_migration2")};
  
  closureTestMaker->setReco1(hReco1);
  closureTestMaker->setReco2(hReco2);
  closureTestMaker->setTruth1(hTruth1);
  closureTestMaker->setTruth2(hTruth2);
  closureTestMaker->setMigration1(hMigration1);
  closureTestMaker->setMigration2(hMigration2);
  
}

void drawComparison(TH1D* unfolded,
		    TH1D* truth,
		    const TString& dirname,
		    const TString& figureName,
		    const vector<TString>& testResult,
		    const double ymin,
		    const double ymax) {
  
  auto ratio = std::unique_ptr<TH1D>{(TH1D*)unfolded->Clone()};
  ratio->Divide(truth);
 
  ratio->GetYaxis()->SetTitle("Unfolded / Truth");
  
  const double xmin_legend=0.55;
  const double ymin_legend=0.8;
  const double ymax_legend=0.89;
  const double xmax_legend=0.90; 
  
  auto leg = std::make_shared<TLegend>(xmin_legend,ymin_legend,xmax_legend,ymax_legend);
  leg->SetFillStyle(0);
  leg->SetTextSize(0.035);
  leg->SetFillColor(0);
  leg->SetBorderSize(0);
  
  std::vector<TH1D*> histos={ratio.get()};
  
  leg->AddEntry(ratio.get(),"Unfolded / Truth");
  for(const TString& res : testResult) leg->AddEntry((TObject*)nullptr,res,"");
  
  TString lumi="";
 
  functions::plotHistogramsWithFixedYRange(histos,leg.get(),dirname+'/',figureName,lumi,ymin,ymax);
  
}

void drawComparionND(TH1D* unfolded,
		   TH1D* truth,
		   const HistogramNDto1DConverter* histogramConverterND,
		   MultiDimensionalPlotsSettings* plotSettingsND,
		   const TString& variable,
		   const TString& level,
		   const TString& dirname,
		   const TString& figureName,
		   const vector<TString>& testResult,
		   const double ymin,
		   const double ymax)
{
  auto ratio = std::unique_ptr<TH1D>{(TH1D*)unfolded->Clone()};
  ratio->Divide(truth);
  
  vector<TH1D*> ratioProj = histogramConverterND->makeProjections(ratio.get(),"ratio_proj");
  const int nprojections = ratioProj.size();
  
  vector<TString> generalInfo;
  generalInfo.push_back("#bf{#it{ATLAS}} Simulation " +NamesAndTitles::getResultsStatus()+", #sqrt{s}=13 TeV");
  if(level.Contains("Parton")) generalInfo.push_back("Parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV");
  else generalInfo.push_back("Fiducial phase-space");
  
  //for(const TString& res : testResult) generalInfo.push_back(res);
  
  
  const int nlinesInfo=generalInfo.size();
  
  int nPerLine(0),nlines(0);
  functions::getGridSize(nprojections,nlines,nPerLine);
  
  const double canvas_xsize = (1000./3) * nPerLine;
  const double canvas_ysize = 175.*(nlines+1);
  
  plotSettingsND->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, std::max(nlinesInfo,(int)testResult.size()+1));
  
  auto can = std::unique_ptr<TCanvas>(plotSettingsND->makeCanvas("can"));
  can->cd();
  vector<vector<shared_ptr<TPad>>> pads = plotSettingsND->makeArrayOfPads(nprojections);
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable);
  
  int iproj=0;
  for(int iline=0;iline<nlines;iline++) {
    int nInLine = pads[iline].size();
    
    for(int iInLine=0;iInLine<nInLine;iInLine++) {
      TPad* currentPad = pads[iline][iInLine].get();
      
      plotSettingsND->setHistLabelStyle(ratioProj[iproj]);
            
      if(iInLine ==0 && iline ==0) { 
	ratioProj[iproj]->GetYaxis()->SetTitle("Unfolded / Truth");
      }
      else {
	ratioProj[iproj]->GetYaxis()->SetTitle("");
      }
      if(iline != nlines-1) { 
	ratioProj[iproj]->GetXaxis()->SetTitle("");
      }
      else {
	ratioProj[iproj]->GetXaxis()->SetTitle(xtitles.back());
      }
      
      currentPad->cd();
      
      std::vector<TString> externalBinInfo = histogramConverterND->getExternalBinsInfo(iproj);
      TPaveText* paveText = functions::makePaveText(externalBinInfo, plotSettingsND->textSizePad(), 0.35, 0.75, 0.93); // xmin,xmax,ymax
      paveText->SetFillStyle(0);
      
      
      ratioProj[iproj]->GetYaxis()->SetRangeUser(ymin,ymax);
      
      ratioProj[iproj]->Draw("p");
      
      paveText->Draw();
      
      currentPad->RedrawAxis("g"); 
      currentPad->RedrawAxis();
      
      iproj++;
    }
  }
  
  can->cd();
  
  TPaveText* paveTextGeneralInfo = plotSettingsND->makePaveText(generalInfo);
  paveTextGeneralInfo->Draw();
  
  auto leg = unique_ptr<TLegend>(plotSettingsND->makeLegend(0.6,0.98));
  leg->SetFillStyle(0);
  leg->SetNColumns(1);
  leg->AddEntry( ratio.get(), "Unfolded / Truth", "p" );
  for(const TString& res : testResult) leg->AddEntry((TObject*)nullptr,res,"");
  leg->Draw();
  
  TString dirname_png=dirname;
  dirname_png.ReplaceAll("pdf.","png.");
  
  can->SaveAs(dirname+"/"+figureName+".pdf");
  can->SaveAs(dirname_png+"/"+figureName+".png");
  
  for (TH1D* h : ratioProj) delete h;
  
}

