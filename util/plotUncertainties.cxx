#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TEnv.h"
#include "TSystem.h"


using namespace std;


TH1D* histogramFromCovariance(TMatrixT<double>& cov, TH1D* centralValues); 

void setupHistStyle(TH1* hist,int color,int markerstyle,double linewidth,int linestyle){
  hist->SetLineColor(color);
  hist->SetMarkerColor(color);
  hist->SetMarkerSize(1.2);
  hist->SetMarkerStyle(markerstyle);
  hist->SetLineWidth(linewidth);
  hist->SetLineStyle(linestyle);
}


int main(int nArg, char **arg) {
  cout << "Plotting uncertainties for variable: " << arg[5] << endl;
  
  try {
    if(nArg!=7){
      cout << "Error: Wrong number of input parameters!" << endl;
      throw TtbarDiffCrossSection_exception("Error: Wrong number of input parameters!"); 
    }
  
    TString mainDir=arg[1];
    TString path_config_files=arg[2];
    TString path_config_lumi=arg[3];
    TString signal=arg[4];
    TString variable_name=arg[5];
    TString level=arg[6];
  
    TString name=variable_name;
    name.ReplaceAll("_fine","");
  
    TString pathToInputRootfile=mainDir + "/root/covariances/" + variable_name + "_" + level + ".root";
    TString pathToModelingRootfile=mainDir + "/root/generatorSystematics/genSystematics_using_signal_" + signal + "/" + name + "_" + level + ".root";
    
    TFile* inputFile = new TFile(pathToInputRootfile);
    if(inputFile->IsZombie()){
      throw TtbarDiffCrossSection_exception("Error: Input file is a zombie!!!");
    }
    
    TFile* modelingFile= new TFile(pathToModelingRootfile);
    if(modelingFile->IsZombie()){
      throw TtbarDiffCrossSection_exception("Error: File with signal modeling covariances is a zombie!!!");
    }
    
    map<TString,TMatrixT<double>* > covariances_all;
    
    vector<TString> listOfCovariances,listOfModelingCovariances;
    functions::GetListOfNames(listOfCovariances,inputFile,TMatrixT<double>::Class() );
    functions::GetListOfNames(listOfModelingCovariances,modelingFile,TMatrixT<double>::Class() );
    
    int nCovariances=listOfCovariances.size();
    int nModelingCovariances=listOfModelingCovariances.size();
    for(int i=0;i<nCovariances;i++){
      //cout << listOfCovariances[i] << endl;
      
      covariances_all[listOfCovariances[i]] = (TMatrixT<double>*)inputFile->Get(listOfCovariances[i]);
    }
    for(int i=0;i<nModelingCovariances;i++){
      cout << listOfModelingCovariances[i] << endl;
      covariances_all[listOfModelingCovariances[i]] = (TMatrixT<double>*)modelingFile->Get(listOfModelingCovariances[i]);
    }
    
    // Loading central values
    
    TH1D* centralValuesAbsolute= (TH1D*)inputFile->Get(name +"_centralValues");
    TH1D* centralValuesRelative= (TH1D*)inputFile->Get(name +"_centralValuesNormalized");
    
    
    
    
    
    
    
    
    const TString method1="MCBased";
    const TString method2=""; 
    //const TString method1="DataBased";
    //const TString method2="Alternative";
    
    const TString signalModelingMethod1="_WithMultijetShifted";
    const TString signalModelingMethod2="_";
    //const TString signalModelingMethod1="";
    //const TString signalModelingMethod2="_Alternative_";
    
    
    vector<TMatrixT<double> > covariancesAbsolute,covariancesRelative;
    vector<TH1D*> relativeUncertaintiesAbsolute;
    vector<TH1D*> relativeUncertaintiesRelative;
    
    vector<TString> subnames,legend_names;
    vector<int> color,markerstyle,linestyle;
    vector<double> linewidth;
    
    subnames.push_back("AllUnc");color.push_back(1);markerstyle.push_back(1);linewidth.push_back(2);linestyle.push_back(1); legend_names.push_back("Total");
    subnames.push_back("OnlyDataStat");color.push_back(2);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("Data stat");
    subnames.push_back("OnlyMCStat");color.push_back(3);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("MC stat");
    subnames.push_back("OnlySys");color.push_back(4);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("Detector sys");
    
    
    
    //cout << name+"_cov" + method1 +"AllUnc" + method2 << endl;
    for(int i=0;i<(int)subnames.size();i++){
      covariancesAbsolute.push_back(*covariances_all[name+"_cov" + method1 + subnames[i] + method2]);
      covariancesRelative.push_back(*covariances_all[name+"_cov" + method1 + subnames[i] + method2+"Normalized"]);
    }
    
    vector<TString> modelingSubnames;
    //modelingSubnames.push_back("totalCovariance");color.push_back(6);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("Signal modeling");
    //modelingSubnames.push_back("totalPDFCovariance");color.push_back(7);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("PDF");
    modelingSubnames.push_back("MatrixElementCovariance");color.push_back(6);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("Matrix Element");
    modelingSubnames.push_back("PartonShowerCovariance");color.push_back(5);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("PartonShowering");
    modelingSubnames.push_back("FSRCovariance");color.push_back(4);markerstyle.push_back(20);linewidth.push_back(2);linestyle.push_back(2);legend_names.push_back("IFSR");
    
    
    vector<TString> allSubnames=subnames;
    allSubnames.insert(allSubnames.end(),modelingSubnames.begin(),modelingSubnames.end());
    
    
    
    
    for(int i=0;i<(int)modelingSubnames.size();i++){
      //cout << modelingSubnames[i] + signalModelingMethod1 + signalModelingMethod2 + variable_name + "_" + level << endl;
      //cout << modelingSubnames[i] + signalModelingMethod1 + "_normalized" + signalModelingMethod2 + variable_name + "_" + level << endl;
      if(!modelingSubnames[i].Contains("totalPDFCovariance")){
	covariancesAbsolute.push_back(*covariances_all[modelingSubnames[i] + signalModelingMethod1 + signalModelingMethod2 + variable_name + "_" + level]);
	covariancesRelative.push_back(*covariances_all[modelingSubnames[i] + signalModelingMethod1 + "_normalized" + signalModelingMethod2 + variable_name + "_" + level]);
      }
      else{
	covariancesAbsolute.push_back(*covariances_all[modelingSubnames[i] + "_" + variable_name + "_" + level]);
	covariancesRelative.push_back(*covariances_all[modelingSubnames[i] + "_normalized_" + variable_name + "_" + level]);
      }
    }
    
    
    TLegend *leg = new TLegend(0.58,0.48,0.85,0.8);
    leg->SetNColumns(1);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextFont(42);
    leg->SetTextSize(0.03);
    
    
    for(int i=0;i<(int)covariancesAbsolute.size();i++){
	    
      relativeUncertaintiesAbsolute.push_back(histogramFromCovariance(covariancesAbsolute[i],centralValuesAbsolute));
      setupHistStyle(relativeUncertaintiesAbsolute[i],color[i],markerstyle[i],linewidth[i],linestyle[i]);
      relativeUncertaintiesAbsolute[i]->SetYTitle("Relative Uncertainty");
      NamesAndTitles::setXtitle(relativeUncertaintiesAbsolute[i],name);
      cout << "Printing relative uncertainties for absolute diff cross section: " << allSubnames[i] << endl;
      for(int ibin=1;ibin<=relativeUncertaintiesAbsolute[i]->GetNbinsX();ibin++) cout << relativeUncertaintiesAbsolute[i]->GetBinContent(ibin) << " ";
      cout << endl;
      //covariancesAbsolute[i].Print();
    }
    
    
    for(int i=0;i<(int)covariancesRelative.size();i++){
	    
      relativeUncertaintiesRelative.push_back(histogramFromCovariance(covariancesRelative[i],centralValuesRelative));
      setupHistStyle(relativeUncertaintiesRelative[i],color[i],markerstyle[i],linewidth[i],linestyle[i]);
      relativeUncertaintiesRelative[i]->SetYTitle("Relative Uncertainty");
      NamesAndTitles::setXtitle(relativeUncertaintiesRelative[i],name);
      
      leg->AddEntry(relativeUncertaintiesRelative[i],legend_names[i]);
      
      cout << "Printing relative uncertainties for Relative diff cross section: " << allSubnames[i] << endl;
      for(int ibin=1;ibin<=relativeUncertaintiesRelative[i]->GetNbinsX();ibin++) cout << relativeUncertaintiesRelative[i]->GetBinContent(ibin) << " ";
      cout << endl;
      //covariancesRelative[i].Print();
    }
    
    TEnv* configlumi= new TEnv(path_config_lumi);
    double lumi = configlumi->GetValue("lumi",0.0)/1000.;
    ostringstream strs;
    lumi >= 10. ? strs.precision(3) : strs.precision(2);
    strs << lumi;
    TString lumi_string =strs.str().c_str();
    
    
    
    TString dirname=mainDir+"/pdf/sys_errors_summary_plots/";
    gSystem->mkdir(dirname);
    
    bool use_logscale=false;
    
    functions::plotHistograms(relativeUncertaintiesRelative,leg,dirname,level+ "_" + name + "_rel.pdf" ,lumi_string,use_logscale);
    
    functions::plotHistograms(relativeUncertaintiesAbsolute,leg,dirname,level+ "_" + name + "_abs.pdf",lumi_string,use_logscale);
    
    
    dirname=mainDir+"/png/sys_errors_summary_plots/";
    gSystem->mkdir(dirname);
    
    functions::plotHistograms(relativeUncertaintiesRelative,leg,dirname,level+ "_" + name + "_rel.png" ,lumi_string,use_logscale);
    
    functions::plotHistograms(relativeUncertaintiesAbsolute,leg,dirname,level+ "_" + name + "_abs.png",lumi_string,use_logscale);
    
    
    delete modelingFile;
    delete inputFile;
  } catch(const TtbarDiffCrossSection_exception& l){l.printMessage();cout << "Exiting now!" << endl;return -1;}
  
  cout << "Everything ok!" << endl;
  return 0;
}






TH1D* histogramFromCovariance(TMatrixT<double>& cov, TH1D* centralValues){
	
	TH1D* relativeErrors=(TH1D*)centralValues->Clone();
	const int nbins=relativeErrors->GetNbinsX();
	
	for(int i=0;i<nbins;i++){
		relativeErrors->SetBinContent(i+1,sqrt(cov[i][i])/centralValues->GetBinContent(i+1));
		relativeErrors->SetBinError(i+1,0.001);
	}
	return relativeErrors;
}













