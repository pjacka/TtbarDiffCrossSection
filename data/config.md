Table of Contents
=========================

**[runSettings](#runsettings)**

**[outputSettings](#outputsettings)**

**[samplesSettings](#samplessettings)**

**[HtFilterSettings](#htfiltersettings)**

**[toolsSettings](#toolssettings)**

**[taggersSettings](#taggerssettings)**

**[luminositySettings](#luminositysettings)**

**[signalModelingSettings](#signalmodelingsettings)**

**[PDFSettings](#pdfsettings)**

**[directories](#directories)**

runSettings
============================

Json string containing general run settings and miscellaneous settings.

It allows to select levels to process via `runReco, runParticle, runParton` options. 

Parton and particle level processing can be also activated/deactivated using the `useTruth` option.

`mc_samples_production` identifies which production will be used for final results. The option is used during output histogram analysis. 

`useTriggers` option activates or deactivates triggers.

`sumWeightsFile` option points to a file with sums of weights for all samples used in the analysis, all productions, and all systematic branches.

`stressTestReweightingConfig` option points to the json file
with the stress test reweighting configuration. The reweighting function can be defined using `formula` or it can be loaded from a root file. `formula` allows to define
reweighting as a function of multiple variables. See [example](data/stress_test_reweighting_config.json).

`qcd_method` option is currently not used - marked for deletion.

`useRecoTruthMatching` - activates reco truth matching when filling migration matrices. `false` is recommended.

`RCJetsHistString`, `varRCJetBranchName`, `varRCJetsHistString`: options used to select reclustered jets. Multiple reclustered jet collections may be saved inside ntuples.

`topPartonDefinition`: `afterFSR` or `beforeFSR`


outputSettings
============================

Processed data are filled into histograms or other objects.

`histogramSettings`: structured option with histogram definitions.

- `histos_config`: Array of json files with control histograms definitions. Control histograms are used for validations and better understanding of physics processes. See [histos.json](data/histos.json). Variables for histograms are defined in [RecoLevelVariables.cxx](Root/RecoLevelVariables.cxx)

- `unfolding_histos_config`, and `unfolding_histosND_config` options: Arrays of json files with main single-differential (multi-differential) histograms definitions. Histograms are constructed at all levels and migration matrices are provided. See [unfolding_histos.json](data/unfolding_histos.json) and [unfolding_histosND.json](data/unfolding_histosND.json). Variables filled into histograms are defined in
[MeasuredVariables.cxx](Root/MeasuredVariables.cxx).

- `useFineHistograms`: No longer used - will be removed soon.
- `fillClosureTestHistos`: Enables filling closure test histograms for main histograms.

samplesSettings
============================

Settings related with simulated samples configuration.

`ttbar_SF`: Scale factor used to adjust ttbar samples normalization.

`factor_to_ttbar_cross_section`: Inverse branching ratio from ttbar inclusive to ttbar all-hadronic channel.

`normalizeToNominalCrossSection`: If true: alternative modeling samples keep the same overall normalization as the nominal sample (default).

`samplesXsectionsFile`: Path to file with samples cross-sections, k-factors and filter efficiencies

`samplesDSIDs`: identifiers of signal and background samples. Backround sampels are classified into categories.





HtFilterSettings
============================
This option will be moved under samplesSettings soon.
Ht-filter is used to populate more tails of distributions in simulated samples. 
These samples are then combined with inclusive sample. Therefore, regions which are already covered by
filtered samples have to be removed from the inclusive sample.

`applyHtFilterCut`: Activate Ht filtering.

`cutValue`: value of Ht filter cut

`samplesToApplyHtFilterCut`: identifiers of samples where HtFilter is used 


toolsSettings
============================
Structured option used for event reconstruction and event processing. Separate set of tools can be used for the reco level (`recoLevelTools`), particle level (`particleLevelTools`), and parton level (`partonLevelTools`). Each of tree options contains the same set of settings:

- `toolsLoaderName`: Identifies toals loader - handles tools loading in the code.

- `toolsLoaderSettings`: Options passed to tools loader to select tools. This includes
  - `RecoToolName`: Identificatier of event reconstruction tool
  - `SelectionToolName`: Event selection tool identifier
  - `ClassificationToolName`: Identifier of the tool which classifies events into regions (SR, CR, VR)
  - `ReweightingToolName`: Identifier of the tool responsible of simulated samples weights calculation. This includes nominal weights and systematic variations.

- `eventReconstructionToolSettings`, `eventSelectionToolSettings`, `eventClassificationToolSettings`, and `eventReweightingToolSettings`: Settings which are passed to individual tools. Their structure depends on selected tool.


taggersSettings
============================

`bTagging` and `topTagging` settings. Options allow to select b-tagger, b-tagged objects, and top-tagger. It is possible to enable/disable top-tagging uncertainties.


luminositySettings
============================

This option is used to setup luminosity and its uncertainty for various periods of time. 


signalModelingSettings
============================

Options used to setup signal modeling branches.

`doSignalModeling`: Activates/deactivates signal modeling uncertinaty calculation.

`IFSRSettings`: Selects systematic variations for `ISR` and `FSR` uncertainties.
- `FSRBranches` and `ISRBranches` provides a mapping between name of the uncertainty and position in weights vector.


PDFSettings
============================

Parton Distrubtion Function uncertainties settings.

`usePDF`: Activates/deactivates PDF uncertainties

`PDFBranches`: Structured option to select PDFs. This allows various PDF set settings with nominal and systematic variations.


directories
============================

A mapping between signal modelling uncertainties names and branch names.



