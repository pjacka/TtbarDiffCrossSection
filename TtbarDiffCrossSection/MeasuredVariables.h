#ifndef MeasuredVariables_h
#define MeasuredVariables_h

#include "NtuplesAnalysisToolsCore/MeasuredVariablesBase.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"

typedef ReconstructedObjects::ReconstructedObject Object_t;

class MeasuredVariables : virtual public MeasuredVariablesBase {
  
  public:

  MeasuredVariables();
  virtual ~MeasuredVariables() = default;
  
  virtual bool initialize();
  virtual void calculateMeasuredVariables(const ReconstructedEvent&);
  
  ClassDef(MeasuredVariables,1)

};
#endif
