#ifndef TTBarEventReaderReco_h
#define TTBarEventReaderReco_h

#include <string>

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventReaderToolBase.h"
#include "NtuplesAnalysisToolsConfiguration/LargeRJetType.h"

#include "TtbarDiffCrossSection/RecoTreeTopCPToolkit.h"

#include "AsgTools/AsgTool.h"

class TTBarEventReaderReco : public EventReaderToolBase {

protected:
  
  std::unique_ptr<RecoTreeTopCPToolkit> m_treeReader;
  nlohmann::json m_treeReaderSettings;
  bool m_isData;
  
  LargeRJetType m_largeRJetType;         // Switch between standard antikt10 jets (LJETS), reclustered jets (RCJETS) and variable-R reclustered jets (VarRCJETS)
  std::string m_sysName;
    
  virtual void readSmallRJets(ReconstructedEvent& event) const;
  virtual void readLargeRJets(ReconstructedEvent& event) const;
  virtual void readElectrons(ReconstructedEvent& event) const;
  virtual void readMuons(ReconstructedEvent& event) const;
  virtual void readMET(ReconstructedEvent& event) const;
  virtual void readTrigger(ReconstructedEvent& event) const;
  virtual void readGenWeights(ReconstructedEvent &event) const;

public:

  TTBarEventReaderReco(const std::string& name);
  virtual ~TTBarEventReaderReco () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual StatusCode initChain(const std::unordered_map<std::string, std::string>& settings) override;
  virtual ReconstructedEvent* readEntry(const ULong64_t ientry) override;
  virtual StatusCode initialize() override;
  virtual void print() const override;
  
  virtual void readMetadata(TFile& f) override;
  
  ClassDefOverride(TTBarEventReaderReco,2)

};
#endif

