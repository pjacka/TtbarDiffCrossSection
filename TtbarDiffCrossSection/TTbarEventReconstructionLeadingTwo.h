#ifndef TTbarEventReconstructionLeadingTwo_h
#define TTbarEventReconstructionLeadingTwo_h

#include <string>


#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "TtbarDiffCrossSection/ObjectSelectionTool.h"


class TTbarEventReconstructionLeadingTwo : public ObjectSelectionTool {

protected:

  double m_minMassCut;

public:

  TTbarEventReconstructionLeadingTwo(const std::string& name);
  virtual ~TTbarEventReconstructionLeadingTwo () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;

  virtual void execute(ReconstructedEvent& event) const override;

  ClassDefOverride(TTbarEventReconstructionLeadingTwo,3)

};
#endif

