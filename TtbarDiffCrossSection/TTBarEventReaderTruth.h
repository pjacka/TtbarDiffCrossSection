#ifndef TTBarEventReaderTruth_h
#define TTBarEventReaderTruth_h

#include <string>

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "NtuplesAnalysisToolsCore/EventReaderToolBase.h"
#include "NtuplesAnalysisToolsConfiguration/LargeRJetType.h"

#include "TtbarDiffCrossSection/TruthTreeTopCPToolkit.h"

#include "AsgTools/AsgTool.h"

class TTBarEventReaderTruth : public EventReaderToolBase {

protected:
  
  std::unique_ptr<TruthTreeTopCPToolkit> m_treeReader;
  nlohmann::json m_treeReaderSettings;

  virtual void readGenWeights(ReconstructedEvent& event) const;

public:

  TTBarEventReaderTruth(const std::string& name);
  virtual ~TTBarEventReaderTruth () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual StatusCode initChain(const std::unordered_map<std::string, std::string>& settings) override;
  virtual ReconstructedEvent* readEntry(const ULong64_t ientry) override;
  virtual StatusCode initialize() override;
  virtual void print() const override;
  
  ClassDefOverride(TTBarEventReaderTruth,2)

};
#endif

