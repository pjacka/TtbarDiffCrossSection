#ifndef TTbarEventReconstructionClosestMass_h
#define TTbarEventReconstructionClosestMass_h

#include <string>


#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "TtbarDiffCrossSection/ObjectSelectionTool.h"

class TTbarEventReconstructionClosestMass : public ObjectSelectionTool {

protected:

  double m_minMassCut;
  double m_topMass;
  double m_minPt1Cut;
  double m_minPt2Cut;
  
public:

  TTbarEventReconstructionClosestMass(const std::string& name);
  virtual ~TTbarEventReconstructionClosestMass () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual void execute(ReconstructedEvent& event) const override;

  ClassDefOverride(TTbarEventReconstructionClosestMass,3)

};
#endif

