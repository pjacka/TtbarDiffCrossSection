#ifndef TruthTreeTopCPToolkit_h
#define TruthTreeTopCPToolkit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include <nlohmann/json.hpp>

#include "NtuplesAnalysisToolsCore/TreeTopCPToolkitBase.h"

class TruthTreeTopCPToolkit final : public TreeTopCPToolkitBase
{
protected:
   TString m_topQuarkDefinition;    // afterFSR or beforeFSR
   TString m_ttbarSystemDefinition; // afterFSR, beforeFSR, fromDecay_afterFSR, or fromDecay_beforeFSR
   TString m_bQuarkDefinition;      // afterFSR or beforeFSR
   TString m_WBosonDefinition;      // afterFSR or beforeFSR
   TString m_WBosonDecayDefinition; // afterFSR or beforeFSR

   void initGenWeights();
   void initSpinVariables();
   void initTopVariables(const TString &whenDefined);
   void initTTBarVariables(const TString &whenDefined);
   void initBVariables(const TString &whenDefined);
   void initWVariables(const TString &whenDefined);
   void initWDecayVariables(const TString &whenDefined);

public:
   // Declaration of leaf types
   ULong64_t eventNumber;
   UInt_t mcChannelNumber;
   UInt_t runNumber;
   Float_t Ttbar_MC_W_from_t_eta;
   Float_t Ttbar_MC_W_from_t_m;
   Int_t Ttbar_MC_W_from_t_pdgId;
   Float_t Ttbar_MC_W_from_t_phi;
   Float_t Ttbar_MC_W_from_t_pt;
   Float_t Ttbar_MC_W_from_tbar_eta;
   Float_t Ttbar_MC_W_from_tbar_m;
   Int_t Ttbar_MC_W_from_tbar_pdgId;
   Float_t Ttbar_MC_W_from_tbar_phi;
   Float_t Ttbar_MC_W_from_tbar_pt;
   Float_t Ttbar_MC_Wdecay1_from_t_eta;
   Float_t Ttbar_MC_Wdecay1_from_t_m;
   Int_t Ttbar_MC_Wdecay1_from_t_pdgId;
   Float_t Ttbar_MC_Wdecay1_from_t_phi;
   Float_t Ttbar_MC_Wdecay1_from_t_pt;
   Float_t Ttbar_MC_Wdecay1_from_tbar_eta;
   Float_t Ttbar_MC_Wdecay1_from_tbar_m;
   Int_t Ttbar_MC_Wdecay1_from_tbar_pdgId;
   Float_t Ttbar_MC_Wdecay1_from_tbar_phi;
   Float_t Ttbar_MC_Wdecay1_from_tbar_pt;
   Float_t Ttbar_MC_Wdecay2_from_t_eta;
   Float_t Ttbar_MC_Wdecay2_from_t_m;
   Int_t Ttbar_MC_Wdecay2_from_t_pdgId;
   Float_t Ttbar_MC_Wdecay2_from_t_phi;
   Float_t Ttbar_MC_Wdecay2_from_t_pt;
   Float_t Ttbar_MC_Wdecay2_from_tbar_eta;
   Float_t Ttbar_MC_Wdecay2_from_tbar_m;
   Int_t Ttbar_MC_Wdecay2_from_tbar_pdgId;
   Float_t Ttbar_MC_Wdecay2_from_tbar_phi;
   Float_t Ttbar_MC_Wdecay2_from_tbar_pt;
   Float_t Ttbar_MC_b_from_t_eta;
   Float_t Ttbar_MC_b_from_t_m;
   Int_t Ttbar_MC_b_from_t_pdgId;
   Float_t Ttbar_MC_b_from_t_phi;
   Float_t Ttbar_MC_b_from_t_pt;
   Float_t Ttbar_MC_bbar_from_tbar_eta;
   Float_t Ttbar_MC_bbar_from_tbar_m;
   Int_t Ttbar_MC_bbar_from_tbar_pdgId;
   Float_t Ttbar_MC_bbar_from_tbar_phi;
   Float_t Ttbar_MC_bbar_from_tbar_pt;
   Float_t Ttbar_MC_t_eta;
   Float_t Ttbar_MC_t_m;
   Int_t Ttbar_MC_t_pdgId;
   Float_t Ttbar_MC_t_phi;
   Float_t Ttbar_MC_t_pt;
   Float_t Ttbar_MC_tbar_eta;
   Float_t Ttbar_MC_tbar_m;
   Int_t Ttbar_MC_tbar_pdgId;
   Float_t Ttbar_MC_tbar_phi;
   Float_t Ttbar_MC_tbar_pt;
   Float_t Ttbar_MC_ttbar_eta;
   Float_t Ttbar_MC_ttbar_m;
   Float_t Ttbar_MC_ttbar_phi;
   Float_t Ttbar_MC_ttbar_pt;
   Float_t spin_QEttbarExample_cos_phi;
   Float_t spin_QEttbarExample_cos_theta_helicity_m;
   Float_t spin_QEttbarExample_cos_theta_helicity_p;
   Float_t spin_QEttbarExample_cos_theta_raxis_m;
   Float_t spin_QEttbarExample_cos_theta_raxis_p;
   Float_t spin_QEttbarExample_cos_theta_transverse_m;
   Float_t spin_QEttbarExample_cos_theta_transverse_p;
   Float_t weight_mc;

   // List of branches
   TBranch *b_eventNumber;                                //!
   TBranch *b_mcChannelNumber;                            //!
   TBranch *b_runNumber;                                  //!
   TBranch *b_Ttbar_MC_W_from_t_eta;                      //!
   TBranch *b_Ttbar_MC_W_from_t_m;                        //!
   TBranch *b_Ttbar_MC_W_from_t_pdgId;                    //!
   TBranch *b_Ttbar_MC_W_from_t_phi;                      //!
   TBranch *b_Ttbar_MC_W_from_t_pt;                       //!
   TBranch *b_Ttbar_MC_W_from_tbar_eta;                   //!
   TBranch *b_Ttbar_MC_W_from_tbar_m;                     //!
   TBranch *b_Ttbar_MC_W_from_tbar_pdgId;                 //!
   TBranch *b_Ttbar_MC_W_from_tbar_phi;                   //!
   TBranch *b_Ttbar_MC_W_from_tbar_pt;                    //!
   TBranch *b_Ttbar_MC_Wdecay1_from_t_eta;                //!
   TBranch *b_Ttbar_MC_Wdecay1_from_t_m;                  //!
   TBranch *b_Ttbar_MC_Wdecay1_from_t_pdgId;              //!
   TBranch *b_Ttbar_MC_Wdecay1_from_t_phi;                //!
   TBranch *b_Ttbar_MC_Wdecay1_from_t_pt;                 //!
   TBranch *b_Ttbar_MC_Wdecay1_from_tbar_eta;             //!
   TBranch *b_Ttbar_MC_Wdecay1_from_tbar_m;               //!
   TBranch *b_Ttbar_MC_Wdecay1_from_tbar_pdgId;           //!
   TBranch *b_Ttbar_MC_Wdecay1_from_tbar_phi;             //!
   TBranch *b_Ttbar_MC_Wdecay1_from_tbar_pt;              //!
   TBranch *b_Ttbar_MC_Wdecay2_from_t_eta;                //!
   TBranch *b_Ttbar_MC_Wdecay2_from_t_m;                  //!
   TBranch *b_Ttbar_MC_Wdecay2_from_t_pdgId;              //!
   TBranch *b_Ttbar_MC_Wdecay2_from_t_phi;                //!
   TBranch *b_Ttbar_MC_Wdecay2_from_t_pt;                 //!
   TBranch *b_Ttbar_MC_Wdecay2_from_tbar_eta;             //!
   TBranch *b_Ttbar_MC_Wdecay2_from_tbar_m;               //!
   TBranch *b_Ttbar_MC_Wdecay2_from_tbar_pdgId;           //!
   TBranch *b_Ttbar_MC_Wdecay2_from_tbar_phi;             //!
   TBranch *b_Ttbar_MC_Wdecay2_from_tbar_pt;              //!
   TBranch *b_Ttbar_MC_b_from_t_eta;                      //!
   TBranch *b_Ttbar_MC_b_from_t_m;                        //!
   TBranch *b_Ttbar_MC_b_from_t_pdgId;                    //!
   TBranch *b_Ttbar_MC_b_from_t_phi;                      //!
   TBranch *b_Ttbar_MC_b_from_t_pt;                       //!
   TBranch *b_Ttbar_MC_bbar_from_tbar_eta;                //!
   TBranch *b_Ttbar_MC_bbar_from_tbar_m;                  //!
   TBranch *b_Ttbar_MC_bbar_from_tbar_pdgId;              //!
   TBranch *b_Ttbar_MC_bbar_from_tbar_phi;                //!
   TBranch *b_Ttbar_MC_bbar_from_tbar_pt;                 //!
   TBranch *b_Ttbar_MC_t_eta;                             //!
   TBranch *b_Ttbar_MC_t_m;                               //!
   TBranch *b_Ttbar_MC_t_pdgId;                           //!
   TBranch *b_Ttbar_MC_t_phi;                             //!
   TBranch *b_Ttbar_MC_t_pt;                              //!
   TBranch *b_Ttbar_MC_tbar_eta;                          //!
   TBranch *b_Ttbar_MC_tbar_m;                            //!
   TBranch *b_Ttbar_MC_tbar_pdgId;                        //!
   TBranch *b_Ttbar_MC_tbar_phi;                          //!
   TBranch *b_Ttbar_MC_tbar_pt;                           //!
   TBranch *b_Ttbar_MC_ttbar_eta;                         //!
   TBranch *b_Ttbar_MC_ttbar_m;                           //!
   TBranch *b_Ttbar_MC_ttbar_phi;                         //!
   TBranch *b_Ttbar_MC_ttbar_pt;                          //!
   TBranch *b_spin_QEttbarExample_cos_phi;                //!
   TBranch *b_spin_QEttbarExample_cos_theta_helicity_m;   //!
   TBranch *b_spin_QEttbarExample_cos_theta_helicity_p;   //!
   TBranch *b_spin_QEttbarExample_cos_theta_raxis_m;      //!
   TBranch *b_spin_QEttbarExample_cos_theta_raxis_p;      //!
   TBranch *b_spin_QEttbarExample_cos_theta_transverse_m; //!
   TBranch *b_spin_QEttbarExample_cos_theta_transverse_p; //!
   TBranch *b_weight_mc;                                  //!

   TruthTreeTopCPToolkit(const nlohmann::json &json);
   virtual ~TruthTreeTopCPToolkit() = default;

   virtual void initChain(TChain *tree, const TString &sys_name = "") override;
   virtual void print() override;

   ClassDef(TruthTreeTopCPToolkit, 2)
};

#endif
