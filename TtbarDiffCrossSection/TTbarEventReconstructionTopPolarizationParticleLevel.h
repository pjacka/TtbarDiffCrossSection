#ifndef TTbarEventReconstructionTopPolarizationParticleLevel_h
#define TTbarEventReconstructionTopPolarizationParticleLevel_h

#include <string>


#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionClosestMass.h"

class TTbarEventReconstructionTopPolarizationParticleLevel : public TTbarEventReconstructionClosestMass {

protected:

  double m_minSubjetPtFrac;
  
  
public:

  TTbarEventReconstructionTopPolarizationParticleLevel(const std::string& name);
  virtual ~TTbarEventReconstructionTopPolarizationParticleLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual void execute(ReconstructedEvent& event) const override;

  ClassDefOverride(TTbarEventReconstructionTopPolarizationParticleLevel,4)

};
#endif

