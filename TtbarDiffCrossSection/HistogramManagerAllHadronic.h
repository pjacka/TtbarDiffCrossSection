//  Peter Berta, 15.10.2015

#ifndef HistogramManagerAllHadronic_h
#define HistogramManagerAllHadronic_h

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "NtuplesAnalysisToolsCore/TTbarHistogram.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsConfiguration/LargeRJetType.h"
#include "NtuplesAnalysisToolsCore/MeasuredVariablesBase.h"

#include <nlohmann/json.hpp>

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <sstream>
#include <iostream>
#include <fstream>

#include "TGraph.h"
#include "TString.h"
#include "TChain.h"
#include "TEnv.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "THnSparse.h"
#include "TLorentzVector.h"
#include "TProfile.h"
#include "TSystem.h"
#include "TRandom3.h"

#include "BootstrapGenerator/BootstrapGenerator.h"
#include "BootstrapGenerator/TH1DBootstrap.h"




using namespace std;
  
class HistogramManagerAllHadronic {
  
 public: 

  HistogramManagerAllHadronic(std::shared_ptr<const NtuplesAnalysisToolsConfig> ttbarConfig, bool isSignal, int debug);
  virtual ~HistogramManagerAllHadronic();  

  void readConfig();
  int checkBinning(const vector<double>& binning,const string& type,const string& variable_name,const string& configname);
  void setRangeVariables();
  void constructHistos();
  void constructHistosOneLevel(const string& level_name);
  void constructUnfoldingHistos(const nlohmann::json& histConfigUnfolding, const string& observable, const string& level);
  void constructUnfoldingHistosND(const nlohmann::json& histConfigUnfoldingND, const string& observable, const string& level);
    
  void writeMeasuredHistos(const TString& subdirectory);
  void writeRecoHistos(const TString& subdirectory);
  
  void setupBootStrap(bool doBootstrap,int nBootstrapPseudoexperiments);
  
  void fillEventHistosOneLevel();
  void fillEventHistos(bool passedRecoLevelCuts, bool passedParticleLevelCuts, bool passedPartonLevelCuts, bool particleMatched, bool partonMatched);
  void fillMeasuredHistos(const string& level, bool passedTruthLevelCuts, bool passedMatching);
  void fillTruthLevelHistos(const std::string& level, const double weight);

  // JK:
  void fillSpecialTopPtHistos(TString level_name, double pt1, double pt2, double phi1, double phi2);
  void initBootsTrap(UInt_t runno, ULong64_t evtno,ULong64_t channo);


  void setNumberOfEvents(double nEvents);
  void setWeight(float weight);
  void setInfo(int channelNumber,ULong64_t eventNumber,long runNumber);
  
  void printExtremeValues() const;
  const std::map<std::string,double>& getMaxValues() const;
  const std::map<std::string,double>& getMinValues() const;
  
  inline void setFillClosureTestHistos(bool fillClosureTestHistos) {m_fillClosureTestHistos=fillClosureTestHistos;}
  
  inline std::string suffixReco() const {return "_RecoLevel";}
  inline std::string prefixPassedTruthNoReco(const std::string& level) const {return level+"LevelCutsPassed_noRecoLevelCuts_";}
  inline std::string suffixPassedTruthNoReco(const std::string& level) const {return "_"+level+"Level";}
  inline std::string prefixPassedTruthAndReco(const std::string& level) const {return level+"LevelCutsPassed_RecoLevelCutsPassed_";}
  inline std::string suffixMigration() const {return "_migration";}
  inline std::string suffixMatchingEff() const {return "_matchingEfficiencyDenominator";}
  inline std::string suffixResolution() const {return "_resolution";}
  
  inline void setMeasuredVariablesReco(const MeasuredVariablesBase& variables) {m_measuredVariablesReco = &variables;}
  inline void setMeasuredVariablesParticle(const MeasuredVariablesBase& variables) {m_measuredVariablesParticle = &variables;}
  inline void setMeasuredVariablesParton(const MeasuredVariablesBase& variables) {m_measuredVariablesParton = &variables;}
  inline void setRecoLevelVariables(const MeasuredVariablesBase& variables) {m_recoLevelVariables = &variables;}

 private:

  std::shared_ptr<const NtuplesAnalysisToolsConfig> m_ttbarConfig;
  
  int m_debug;
  int m_isSignal;
  bool m_DoBootsTrap;
  int m_Nreplicas;
  BootstrapGenerator* m_Bootstrap_Gen;
  
  bool m_fillClosureTestHistos;
  
  std::vector<std::string> m_histConfigs;
  std::vector<std::string> m_histUnfolingConfigs;
  std::vector<std::string> m_histUnfolingConfigsND;

  float m_weight;
  ULong64_t m_eventNumber;
  int m_channelNumber;
  long m_runNumber;
  
  int m_nPseudoExperiments;
  int m_ipseudoExperiment;
  int m_nPseudoExperiments_particle_vs_parton;
  int m_ipseudoExperiment_particle_vs_parton;
  bool m_recoLevelUnfoldingHistosFilled;
  
  std::map<std::string,std::unique_ptr<TTbarHistogramBase>> m_measuredHistos;
  std::map<std::string,std::unique_ptr<TTbarHistogramBase>> m_recoHistos;

  bool m_particleMatched;
  bool m_partonMatched;
  
  const MeasuredVariablesBase* m_measuredVariablesReco;
  const MeasuredVariablesBase* m_measuredVariablesParticle;
  const MeasuredVariablesBase* m_measuredVariablesParton;
  const MeasuredVariablesBase* m_recoLevelVariables;


  TRandom3 *m_random;  
  
  ClassDef(HistogramManagerAllHadronic,1)
};

#endif

