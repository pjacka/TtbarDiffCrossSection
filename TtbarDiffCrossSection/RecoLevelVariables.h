#ifndef RecoLevelVariables_h
#define RecoLevelVariables_h

#include "NtuplesAnalysisToolsCore/MeasuredVariablesBase.h"
#include "NtuplesAnalysisToolsConfiguration/LargeRJetType.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"

#include <string>

typedef ReconstructedObjects::ReconstructedObject Object_t;

class RecoLevelVariables : virtual public MeasuredVariablesBase {
public:
  RecoLevelVariables();
  virtual ~RecoLevelVariables() = default;
  
  virtual bool initialize();
  virtual void calculateLevelVariables(const ReconstructedEvent& event);
  void setLargeRJetType(const LargeRJetType& largeRJetType){m_largeRJetType=largeRJetType;}
  float pseudoQw(const Object_t& jet) const;
  
protected: 

  bool initializeKinematicVariables(const std::string& particle);
  bool initializeSubstructureVariables(const std::string& particle);
  bool initializeLJetVariables(const std::string& particle);
  bool initializeRCJetVariables(const std::string& particle);
  bool initializeInclusiveKinematicVariables(const std::string& particle);
  void calculateKinematicVariables(const ReconstructedObjects::FourMom_t& vec, const std::string& particle);
  void calculateSubstructureVariables(const Object_t& jet, const std::string& particle);
  void calculateLJetVariables(const Object_t& jet, const std::string& particle);
  void calculateRCJetVariables(const Object_t& jet, const std::string& particle);
  void calculateInclusiveKinematicVariables(const ObjectContainer_t& vec, const std::string& particle);
  
  LargeRJetType m_largeRJetType;
  ClassDef(RecoLevelVariables,1)
};

#endif
