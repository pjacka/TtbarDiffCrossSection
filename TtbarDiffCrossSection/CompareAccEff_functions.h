////////////////////////////////////////////////////////////
//                  Ota Zaplatilek                        //
//                     headred                            //
//   Compare Acceptancy and Efficiency of different MC    //
//                    14.11.2017                          //
////////////////////////////////////////////////////////////


#ifndef CompareAccEff_functions_H
#define CompareAccEff_functions_H


#include "HelperFunctions/functions.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "HelperFunctions/AtlasStyle.h"
#include "HelperFunctions/MultiDimensionalPlotsSettings.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"


#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include <algorithm>
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"


using namespace std;

vector<TString> getVecOfHistNames(TString keyString, TFile* filein ); // const char* filein);

void setEffStyle(TH1D* efficiencyOfTruthLevelCuts, TH1D* efficiencyOfRecoLevelCuts, TH1D* matchingEff=0);

void plotAccEffInOnePlotND(TH1D* efficiencyOfTruthLevelCuts, TH1D* efficiencyOfRecoLevelCuts, const HistogramNDto1DConverter* histogramConverterND_reco, MultiDimensionalPlotsSettings* plotSettingsND,const TString& dirname,const TString& level, const TString& variable);

void PlotAccEffInOnePlot(TH1D* efficiencyOfTruthLevelCuts, TH1D* efficiencyOfRecoLevelCuts,
			 TString pathToFigures,
			 TString mc_samples_production,
			 TString subdir,
			 TString level,
			 TString spectrum,
			 TString label,TString dimension,vector<double>& binning_x,vector<vector<double> >& binning_y);


void PlotAllAccInOnePlot( const vector<TH1D*>& vec_EffHists,
			  const vector<TH1D*>& vec_AccHists,  
			  const vector<TString>& vec_LegendName,
			  const vector<int>& vec_colorUnc,
			  const vector<int>& vec_colorMarkers,
			  TString nameHist,
			  TString outputPath,
			  TString level,
			  TString label);
  
TH1D DrawOneAcc(TH1D* hist,
		TString str_histVar,
		TString level,
		TString label,
		int colorMarker,
		int colorUnc,
		TPad* pad,
		TString AccEff,
		bool bool_ratio); 
			 
			 
void PlotVectorInOnePlot( const vector<TH1D*>& vec_Hists, 
			  const vector<TString>& vec_LegendName,
			  const vector<int>& vec_colorUnc,
			  const vector<int>& vec_colorMarkers,
			  TString nameHist,
			  TString outputPath,
			  TString level,
			  TString label,
			  TString str_AccEff,
			  bool bool_ratio); 


void PlotVectorIn2Pads( const vector<TH1D*>& vec_Hists,
			const vector<TString>& vec_LegendName,
			const vector<int>& vec_colorUnc,
			const vector<int>& vec_colorMarkers,
			TString nameHist,
			TString outputPath,	
			TString level,
			TString label,
			TString str_AccEff);
						
#endif
