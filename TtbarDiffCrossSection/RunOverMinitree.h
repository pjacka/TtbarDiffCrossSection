#ifndef RunOverMinitree_h
#define RunOverMinitree_h

#include "TtbarDiffCrossSection/HistogramManagerAllHadronic.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/LargeRJet.h"
#include "NtuplesAnalysisToolsCore/AnaToolsLoaderBase.h"
#include "NtuplesAnalysisToolsCore/EventReweightingToolBase.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObject.h"
#include "NtuplesAnalysisToolsCore/EventReaderToolBase.h"
#include "NtuplesAnalysisToolsCore/KeyIndexMapper.h"
#include "NtuplesAnalysisToolsCore/SystematicBranchesFilterBase.h"

// NNLO reweighting class
#include "TTbarNNLOReweighter/TTbarNNLORecursiveRew.h"
// PMG tool loading sample cross-section
#include "PMGAnalysisInterfaces/IPMGCrossSectionTool.h"

#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <iomanip> // std::setprecision
#include <utility>
#include <memory>

#include "TString.h"
#include "TTree.h"
#include "TEnv.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TRandom.h"

class TFile;
class TTree;
class SampleMetadata;

typedef ReconstructedObjects::ReconstructedObject Object_t;

class RunOverMinitree
{

public:
  RunOverMinitree(const TString &filelistname, const TString &outputName, const TString &configName, bool doDetSys, bool doSysPDF, bool doSysAlphaSFSR, bool doSysIFSR, bool doReweighting, int doBootstrap, int nEvents, int debugLevel);
  virtual ~RunOverMinitree();

  // Interface functions
  void execute();
  void initialize();
  void finalize();

protected:
  void loadTrees();
  void readConfig(const TString &configName);
  void initializeTools();
  void initializeClassificationTools();
  void initializeSelectionTools();
  void initializeRecoTools();

  void executeSystematicVariation(const TString &sys_name);
  void initializePartonChain(const TString &sys_name);
  void initializeParticleChain(const TString &sys_name);
  void initializeRecoChain(const TString &sys_name);

  bool prepareMatchedParticleLevel(const ReconstructedEvent &event);
  bool prepareMatchedPartonLevel(const ReconstructedEvent &event);

  void filterSystematicVariations();
  bool passSysNameFilter(const TString &sysName) const;

  void addSystematicVariations();
  void addSystematicVariation(const TString &sysName);
  void addDetectorSystematicVariation(const TString &sysName);
  void addGeneratorSystematicVariation(const TString &sysName);

  void readMetadata(TFile &f);

  void multiplyEventWeights(double weight);

  void printInterestingEvent(Minitree &t);
  void printExtremeValues();

  double getSampleCrossSectionWithKFactorAndFilterEfficiency(const int dsid);

  bool checkForDuplicatedEvents();

  void extractInformationAboutSampleFromMetadata(const SampleMetadata &metadata);
  double loadSumWeights(const std::string &mcProduction, const std::string &dsid, const std::string &suffix);
  void initializeHistogramManagers(); // Run for each sys branch
  void loopPartonLevel();
  void loopParticleLevel();
  void loopDetectorLevel();
  void writeHistograms();
  void deleteHistograms();

  bool passedHtFilterCut(const float &filter_Ht);

  int checkMatching(const Object_t &t1Reco, const Object_t &t2Reco, const Object_t &t1Truth, const Object_t &t2Truth, const double dRmax = 1.0);
  void calculateMeasuredVariables(bool passedParticle, bool passedParton);

private:
  // Properties
  int m_debug;
  TString m_filelistName;
  TString m_outputName;
  int m_nEvents;
  TString m_configName;

  bool m_doDetSys;
  bool m_doSysPDF;
  bool m_doOtherGenSys;
  bool m_doReweighting;
  int m_nBootstrapPseudoexperiments;

  // Variables initialized in the Initialize() function
  std::unique_ptr<SystematicBranchesFilterBase> m_sysBranchesFilter;
  bool m_doBootstrap;
  bool m_isData;
  TFile *m_output;
  TString m_ttbarPath;
  bool m_runAllEvents;
  int m_nEvents_parton;
  int m_nEvents_particle;
  std::unique_ptr<TRandom3> m_random;

  std::unique_ptr<AnaToolsLoaderBase> m_toolsLoaderReco;
  std::unique_ptr<AnaToolsLoaderBase> m_toolsLoaderParticle;
  std::unique_ptr<AnaToolsLoaderBase> m_toolsLoaderParton;

  std::unique_ptr<EventReweightingToolBase> m_stressTestReweightingTool;

  std::unique_ptr<ReconstructedEvent> m_recoLevel;
  std::unique_ptr<ReconstructedEvent> m_particleLevel;
  std::unique_ptr<ReconstructedEvent> m_partonLevel;

  std::unique_ptr<MeasuredVariablesBase> m_measuredVariablesReco;
  std::unique_ptr<MeasuredVariablesBase> m_measuredVariablesParticle;
  std::unique_ptr<MeasuredVariablesBase> m_measuredVariablesParton;
  std::unique_ptr<MeasuredVariablesBase> m_recoLevelVariables;

  // Variables set from the NtuplesAnalysisToolsConfig file
  std::shared_ptr<const NtuplesAnalysisToolsConfig> m_ttbarConfig;
  double m_HtFilterCutValue;
  vector<int> m_samplesToApplyHtFilterCut;
  double m_lumi;
  std::unique_ptr<PMGTools::IPMGCrossSectionTool> m_sampleCrossSectionTool;
  vector<int> m_signalDSIDs;
  bool m_isNominal;
  std::vector<TString> m_reweightingIDs;
  bool m_useRCJetsSubstructure;
  TString m_qcd_method;
  bool m_useEWSF;
  bool m_useTruth;
  std::unique_ptr<TTbarNNLORecursiveRew> m_NNLOReweighter;

  // Variables initialized in the LoadTrees() function
  TChain *m_chain_nominal;
  TChain *m_chain_parton;
  TChain *m_chain_particle;
  std::vector<double> m_sumGenWeights;
  double m_sumWeights_nominal;
  double m_sumWeights_sys;
  double m_nEventsInFilelist;
  std::map<TString, TChain*> m_chains_reco;
  std::map<TString, TChain*> m_chains_particle;
  std::map<TString, TChain*> m_chains_parton;
  std::vector<TString> m_sys_names;
  std::vector<TString> m_topTaggingSysNames;
  TH1D *m_hist_noWeights_all;
  TH1D *m_hist_scale_factors;

  // Information extracted from the first entry in the reco tree
  ULong64_t m_eventNumber;
  int m_channelNumber;
  int m_runNumber;
  double m_crossSectionWithKFactorAndFilterEfficiency;
  TString m_mcSamplesProduction;
  bool m_isSignal;
  bool m_applyHtFilterCut;

  TString m_sysName;

  // Chain identifiers
  bool m_isPDFChain;
  bool m_isOtherGenChain;

  bool m_createMigrationMatrices;
  bool m_createTruthLevelHistos;

  double m_weight;

  TH1D *m_h_cutflow;
  TH1D *m_h_alphaS_weights_198;
  TH1D *m_h_alphaS_weights_199;
  TH1D *m_h_cutflow_noweights;
  TH1D *m_h_cutflow_particle;
  TH1D *m_h_cutflow_noweights_particle;
  TH1D *m_h_cutflow_particle_after_reco;
  TH1D *m_h_cutflow_noweights_particle_after_reco;
  TH1D *m_h_cutflow_parton;
  TH1D *m_h_cutflow_noweights_parton;

  TString m_treeName;

  int m_nEvents_reco;

  vector<HistogramManagerAllHadronic *> m_hisMans;
  bool m_isReweightingChain;
  bool m_particleMatched;
  bool m_partonMatched;

  KeyIndexMapper m_partonTreeIndexes;
  KeyIndexMapper m_particleTreeIndexes;

  std::set<std::tuple<unsigned int, unsigned long long, float, float, float>> m_processedEvents;
  int m_skippedEvents;

  ClassDef(RunOverMinitree, 1)
};
#endif
