#ifndef TTbarEventReweightingTool_h
#define TTbarEventReweightingTool_h

#include <string>
#include <vector>

#include "TH1D.h"
#include "TString.h"
#include "TFile.h"

#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventReweightingToolBase.h"

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"


class TTbarEventReweightingTool : public EventReweightingToolBase {

protected:

  TString m_sysName;
  
  double m_sumWeights;  
  double m_xsecWithKfactor;
  
  bool m_isSingleTop_tChannel;
  bool m_isSingleTop_WtChannel;
  bool m_is_ttWChannel;
  bool m_is_ttZChannel;
  bool m_is_ttHChannel;
  bool m_is_ttbar_nonallhad;
  
  double m_lumi;
  double m_lumiUnc;
  
  bool m_useHtFilterEfficiencyCorrections;
  std::unique_ptr<TFile> m_HtFilterEfficiencyCorrectionsFile;
  std::vector<int> m_samplesWithHtFilterEfficiencyCorrection;
  
  StatusCode identifySample(const int channelNumber, const nlohmann::json& json);
  StatusCode initializeEfficiencyCorrections(const nlohmann::json& json);
  
  
  double getBtagSF() const;
  double getTopTaggingSF(const ReconstructedEvent& event) const;
  double getBackgroundXsecSF() const;
  
  void multiplyEventWeights(const double weight);
  
  void shiftLumi(double& lumi) const;
  
public:

  TTbarEventReweightingTool(const std::string& name);
  virtual ~TTbarEventReweightingTool () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual StatusCode initialize() override;
  virtual StatusCode execute(ReconstructedEvent& sysName) override;

  ClassDefOverride(TTbarEventReweightingTool,2)

};
#endif

