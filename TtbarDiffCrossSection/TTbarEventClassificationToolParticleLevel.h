#ifndef TTbarEventClassificationToolParticleLevel_h
#define TTbarEventClassificationToolParticleLevel_h

#include <string>
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventClassificationToolBase.h"

class TTbarEventClassificationToolParticleLevel : public EventClassificationToolBase
{
protected:
  std::string m_bTagDecoration;

public:
  TTbarEventClassificationToolParticleLevel(const std::string &name);
  virtual ~TTbarEventClassificationToolParticleLevel() = default;

  virtual StatusCode readJSONConfig(const nlohmann::json &json);
  virtual void execute(const ReconstructedEvent &event);

  ClassDef(TTbarEventClassificationToolParticleLevel, 2)
};
#endif
