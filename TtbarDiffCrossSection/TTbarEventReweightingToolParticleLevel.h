#ifndef TTbarEventReweightingToolParticleLevel_h
#define TTbarEventReweightingToolParticleLevel_h

#include <string>
#include <vector>

#include "TH1D.h"
#include "TString.h"
#include "TFile.h"

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"

#include "NtuplesAnalysisToolsCore/EventReweightingToolBase.h"

class TTbarEventReweightingToolParticleLevel : public EventReweightingToolBase {

protected:

  double m_lumi;
  double m_sumWeights;  
  double m_xsecWithKfactor;
  
  std::unique_ptr<TFile> m_HtFilterEfficiencyCorrectionsFile;
  std::vector<int> m_samplesWithHtFilterEfficiencyCorrection;  
  void multiplyEventWeights(const double weight);
  
  
public:

  TTbarEventReweightingToolParticleLevel(const std::string& name);
  virtual ~TTbarEventReweightingToolParticleLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual StatusCode initialize() override;
  virtual StatusCode execute(ReconstructedEvent& event) override;

  ClassDefOverride(TTbarEventReweightingToolParticleLevel,2)

};
#endif

