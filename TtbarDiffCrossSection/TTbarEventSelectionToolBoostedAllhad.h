#ifndef TTbarEventSelectionToolBoostedAllhad_h
#define TTbarEventSelectionToolBoostedAllhad_h

#include <string>
#include <vector>
#include <unordered_map>

#include "TH1D.h"

#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventSelectionToolBase.h"

class TTbarEventSelectionToolBoostedAllhad : public EventSelectionToolBase {

protected:

  TH1D* m_cutflow;
  TH1D* m_cutflow_noweights;
  double m_useTriggers;


  double m_leptonVetoPt;
  double m_top1PtMin;
  double m_top2PtMin;
  double m_top1MassMin;
  double m_top1MassMax;
  double m_top2MassMin;
  double m_top2MassMax;
  
  std::string m_bTagDecoration;
  std::string m_topTagDecoration;
  std::unordered_map<std::string, std::vector<std::string>> m_triggersMap;

  bool passedTrigger(const ReconstructedEvent& event) const;
  
  

public:

  TTbarEventSelectionToolBoostedAllhad(const std::string& name);
  virtual ~TTbarEventSelectionToolBoostedAllhad () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual bool execute(const ReconstructedEvent&, bool fillCutflow=true) const override;
  virtual TH1D* initCutflowHistogram(const std::string& name) const override;

  ClassDefOverride(TTbarEventSelectionToolBoostedAllhad,2)

};
#endif

