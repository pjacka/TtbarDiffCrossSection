#ifndef TTbarEventSelectionToolParticleLevel_h
#define TTbarEventSelectionToolParticleLevel_h

#include <string>

#include "TH1D.h"

#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventSelectionToolBase.h"

class TTbarEventSelectionToolParticleLevel : public EventSelectionToolBase {

protected:
  
  TH1D* m_cutflow;
  TH1D* m_cutflow_noweights;

  double m_leptonVetoPt;
  double m_top1PtMin;
  double m_top2PtMin;
  double m_top1MassMin;
  double m_top1MassMax;
  double m_top2MassMin;
  double m_top2MassMax;
  
  std::string m_bTagDecoration;


public:

  TTbarEventSelectionToolParticleLevel(const std::string& name);
  virtual ~TTbarEventSelectionToolParticleLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual bool execute(const ReconstructedEvent&, bool fillCutflow=true) const override;
  virtual TH1D* initCutflowHistogram(const std::string& name) const override;

  ClassDefOverride(TTbarEventSelectionToolParticleLevel,2)

};
#endif

