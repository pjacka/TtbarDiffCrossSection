#ifndef RecoTreeTopCPToolkit_h
#define RecoTreeTopCPToolkit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include <nlohmann/json.hpp>

#include "NtuplesAnalysisToolsCore/TreeTopCPToolkitBase.h"

class RecoTreeTopCPToolkit final : public TreeTopCPToolkitBase
{
protected:
   bool m_isData;
   TString m_bTaggerName;
   bool m_bTaggerUseContinuousWP;
   TString m_bTaggerContinuousWP;
   bool m_bTaggerUseFixedWP;
   TString m_bTaggerFixedWP;
   bool m_useBTaggerWeights;
   TString m_topTaggerName;
   TString m_electron_selection;
   TString m_muon_selection;

   void initGenWeights();
   void initTopTagger();
   void initTriggers();
   void initBTagger();
   void initLargeRJets();
   void initSmallRJets();
   void initElectrons();
   void initMuons();
   void initMET();

public:
   // Declaration of leaf types
   std::vector<float> *ljet_eta;
   std::vector<float> *ljet_phi;
   std::vector<unsigned char> *bootstrapWeights;
   ULong64_t eventNumber;
   UInt_t mcChannelNumber;
   UInt_t runNumber;
   Bool_t trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100;
   Bool_t trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15;
   Bool_t trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1J100;
   Bool_t trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15;
   Bool_t trigPassed_HLT_2j330_a10t_lcw_jes_30smcINF;
   Bool_t trigPassed_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111;
   Bool_t trigPassed_HLT_ht1000_L1J100;
   Bool_t trigPassed_HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j360_a10_sub_L1J100;
   Bool_t trigPassed_HLT_j360_a10r_L1J100;
   Bool_t trigPassed_HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100;
   Bool_t trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1J100;
   Bool_t trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j420_a10_lcw_L1J100;
   Bool_t trigPassed_HLT_j420_a10r_L1J100;
   Bool_t trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100;
   Bool_t trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111;
   Bool_t trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100;
   Bool_t trigPassed_HLT_j440_a10_lcw_subjes_L1SC111;
   Bool_t trigPassed_HLT_j440_a10r_L1SC111;
   Bool_t trigPassed_HLT_j460_a10_lcw_subjes_L1J100;
   Bool_t trigPassed_HLT_j460_a10_lcw_subjes_L1SC111;
   Bool_t trigPassed_HLT_j460_a10_lcw_subjes_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j460_a10r_L1J100;
   Bool_t trigPassed_HLT_j460_a10r_L1SC111;
   Bool_t trigPassed_HLT_j460_a10r_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100;
   Bool_t trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j460_a10t_lcw_jes_L1J100;
   Bool_t trigPassed_HLT_j460_a10t_lcw_jes_L1SC111;
   Bool_t trigPassed_HLT_j460_a10t_lcw_jes_L1SC111_CJ15;
   Bool_t trigPassed_HLT_j480_a10_lcw_subjes_L1J100;
   Bool_t trigPassed_HLT_j480_a10_lcw_subjes_L1SC111;
   Bool_t trigPassed_HLT_j480_a10r_L1J100;
   Bool_t trigPassed_HLT_j480_a10r_L1SC111;
   Bool_t trigPassed_HLT_j480_a10t_lcw_jes_L1J100;
   Bool_t trigPassed_HLT_j480_a10t_lcw_jes_L1SC111;
   std::vector<int> *el_IFFClass;
   std::vector<float> *el_charge;
   std::vector<float> *el_eta;
   std::vector<float> *el_phi;
   std::vector<float> *jet_eta;
   std::vector<char> *jet_bTagged;
   std::vector<int> *jet_bTagger_continuous_quantile;
   std::vector<float> *jet_bTagger_score;
   std::vector<float> *jet_phi;
   std::vector<int> *mu_IFFClass;
   std::vector<float> *mu_charge;
   std::vector<float> *mu_eta;
   std::vector<float> *mu_phi;
   std::vector<float> *ljet_topTagger_SF;
   std::vector<float> *ljet_topTagger_effSF;
   std::vector<float> *ljet_topTagger_efficiency;
   std::vector<int> *ljet_topTagger_passMass;
   std::vector<float> *ljet_topTagger_score;
   std::vector<int> *ljet_topTagger_tagged;
   std::vector<int> *ljet_GhostBHadronsFinalCount;
   std::vector<int> *ljet_GhostCHadronsFinalCount;
   std::vector<float> *ljet_Qw;
   std::vector<int> *ljet_TruthLabel;
   std::vector<float> *ljet_Split12;
   std::vector<float> *ljet_Split23;
   std::vector<float> *ljet_Tau1_wta;
   std::vector<float> *ljet_Tau2_wta;
   std::vector<float> *ljet_Tau3_wta;
   std::vector<float> *ljet_Tau4_wta;
   std::vector<float> *ljet_m;
   std::vector<float> *ljet_pt;
   Float_t weight_pileup;
   Float_t weight_bTag_SF;
   Float_t weight_mc;
   Float_t weight_jvt_effSF;
   Float_t weight_leptonSF_tight;
   Char_t pass_ljets;
   std::vector<char> *el_select;
   std::vector<float> *el_e;
   std::vector<float> *el_pt;
   std::vector<char> *jet_select_baselineJvt;
   std::vector<float> *jet_e;
   std::vector<float> *jet_pt;
   std::vector<char> *mu_select;
   std::vector<float> *mu_e;
   std::vector<float> *mu_TTVA_effSF;
   std::vector<float> *mu_isol_effSF;
   std::vector<float> *mu_reco_effSF;
   std::vector<float> *mu_pt;
   Float_t met_met;
   Float_t met_phi;
   Float_t met_significance;
   Float_t met_sumet;

   // List of branches
   TBranch *b_ljet_eta;                                                                         //!
   TBranch *b_ljet_phi;                                                                         //!
   TBranch *b_bootstrapWeights;                                                                 //!
   TBranch *b_eventNumber;                                                                      //!
   TBranch *b_mcChannelNumber;                                                                  //!
   TBranch *b_runNumber;                                                                        //!
   TBranch *b_trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100;           //!
   TBranch *b_trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15;     //!
   TBranch *b_trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1J100;                                //!
   TBranch *b_trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15;                          //!
   TBranch *b_trigPassed_HLT_2j330_a10t_lcw_jes_30smcINF;                                       //!
   TBranch *b_trigPassed_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111;                               //!
   TBranch *b_trigPassed_HLT_ht1000_L1J100;                                                     //!
   TBranch *b_trigPassed_HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15; //!
   TBranch *b_trigPassed_HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15;                      //!
   TBranch *b_trigPassed_HLT_j360_a10_sub_L1J100;                                               //!
   TBranch *b_trigPassed_HLT_j360_a10r_L1J100;                                                  //!
   TBranch *b_trigPassed_HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15; //!
   TBranch *b_trigPassed_HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15;                      //!
   TBranch *b_trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100;             //!
   TBranch *b_trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15;       //!
   TBranch *b_trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1J100;                                 //!
   TBranch *b_trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15;                           //!
   TBranch *b_trigPassed_HLT_j420_a10_lcw_L1J100;                                               //!
   TBranch *b_trigPassed_HLT_j420_a10r_L1J100;                                                  //!
   TBranch *b_trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100;                                 //!
   TBranch *b_trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111;                                //!
   TBranch *b_trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100;                                 //!
   TBranch *b_trigPassed_HLT_j440_a10_lcw_subjes_L1SC111;                                       //!
   TBranch *b_trigPassed_HLT_j440_a10r_L1SC111;                                                 //!
   TBranch *b_trigPassed_HLT_j460_a10_lcw_subjes_L1J100;                                        //!
   TBranch *b_trigPassed_HLT_j460_a10_lcw_subjes_L1SC111;                                       //!
   TBranch *b_trigPassed_HLT_j460_a10_lcw_subjes_L1SC111_CJ15;                                  //!
   TBranch *b_trigPassed_HLT_j460_a10r_L1J100;                                                  //!
   TBranch *b_trigPassed_HLT_j460_a10r_L1SC111;                                                 //!
   TBranch *b_trigPassed_HLT_j460_a10r_L1SC111_CJ15;                                            //!
   TBranch *b_trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100;                      //!
   TBranch *b_trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15;                //!
   TBranch *b_trigPassed_HLT_j460_a10t_lcw_jes_L1J100;                                          //!
   TBranch *b_trigPassed_HLT_j460_a10t_lcw_jes_L1SC111;                                         //!
   TBranch *b_trigPassed_HLT_j460_a10t_lcw_jes_L1SC111_CJ15;                                    //!
   TBranch *b_trigPassed_HLT_j480_a10_lcw_subjes_L1J100;                                        //!
   TBranch *b_trigPassed_HLT_j480_a10_lcw_subjes_L1SC111;                                       //!
   TBranch *b_trigPassed_HLT_j480_a10r_L1J100;                                                  //!
   TBranch *b_trigPassed_HLT_j480_a10r_L1SC111;                                                 //!
   TBranch *b_trigPassed_HLT_j480_a10t_lcw_jes_L1J100;                                          //!
   TBranch *b_trigPassed_HLT_j480_a10t_lcw_jes_L1SC111;                                         //!
   TBranch *b_el_IFFClass;                                                                      //!
   TBranch *b_el_charge;                                                                        //!
   TBranch *b_el_eta;                                                                           //!
   TBranch *b_el_phi;                                                                           //!
   TBranch *b_jet_eta;                                                                          //!
   TBranch *b_jet_bTagged;                                                                      //!
   TBranch *b_jet_bTagger_continuous_quantile;                                                  //!
   TBranch *b_jet_bTagger_score;                                                                //!
   TBranch *b_jet_phi;                                                                          //!
   TBranch *b_mu_IFFClass;                                                                      //!
   TBranch *b_mu_charge;                                                                        //!
   TBranch *b_mu_eta;                                                                           //!
   TBranch *b_mu_phi;                                                                           //!
   TBranch *b_ljet_topTagger_SF;                                                                //!
   TBranch *b_ljet_topTagger_effSF;                                                             //!
   TBranch *b_ljet_topTagger_efficiency;                                                        //!
   TBranch *b_ljet_topTagger_passMass;                                                          //!
   TBranch *b_ljet_topTagger_score;                                                             //!
   TBranch *b_ljet_topTagger_tagged;                                                            //!
   TBranch *b_ljet_GhostBHadronsFinalCount;                                                     //!
   TBranch *b_ljet_GhostCHadronsFinalCount;                                                     //!
   TBranch *b_ljet_Qw;                                                                          //!
   TBranch *b_ljet_TruthLabel;                                                                  //!
   TBranch *b_ljet_Split12;                                                                     //!
   TBranch *b_ljet_Split23;                                                                     //!
   TBranch *b_ljet_Tau1_wta;                                                                    //!
   TBranch *b_ljet_Tau2_wta;                                                                    //!
   TBranch *b_ljet_Tau3_wta;                                                                    //!
   TBranch *b_ljet_Tau4_wta;                                                                    //!
   TBranch *b_ljet_m;                                                                           //!
   TBranch *b_ljet_pt;                                                                          //!
   TBranch *b_weight_pileup;                                                                    //!
   TBranch *b_weight_bTag_SF;                                                                   //!
   TBranch *b_weight_mc;                                                                        //!
   TBranch *b_weight_jvt_effSF;                                                                 //!
   TBranch *b_weight_leptonSF_tight;                                                            //!
   TBranch *b_pass_ljets;                                                                       //!
   TBranch *b_el_select;                                                                        //!
   TBranch *b_el_e;                                                                             //!
   TBranch *b_el_pt;                                                                            //!
   TBranch *b_jet_select_baselineJvt;                                                           //!
   TBranch *b_jet_e;                                                                            //!
   TBranch *b_jet_pt;                                                                           //!
   TBranch *b_mu_select;                                                                        //!
   TBranch *b_mu_e;                                                                             //!
   TBranch *b_mu_TTVA_effSF;                                                                    //!
   TBranch *b_mu_isol_effSF;                                                                    //!
   TBranch *b_mu_reco_effSF;                                                                    //!
   TBranch *b_mu_pt;                                                                            //!
   TBranch *b_met_met;                                                                          //!
   TBranch *b_met_phi;                                                                          //!
   TBranch *b_met_significance;                                                                 //!
   TBranch *b_met_sumet;                                                                        //!

   RecoTreeTopCPToolkit(const nlohmann::json &json, bool isData);
   virtual ~RecoTreeTopCPToolkit() = default;

   virtual void initChain(TChain *tree, const TString &sysName) override;
   virtual void print() override;

   ClassDef(RecoTreeTopCPToolkit, 2)
};

#endif
