#ifndef TTbarEventClassificationToolBoostedAllhad_h
#define TTbarEventClassificationToolBoostedAllhad_h

#include <string>

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventClassificationToolBase.h"

class TTbarEventClassificationToolBoostedAllhad : public EventClassificationToolBase {
protected:
  
  std::string m_bTagDecoration;
  std::string m_topTagDecoration;

public:

  TTbarEventClassificationToolBoostedAllhad(const std::string& name);
  virtual ~TTbarEventClassificationToolBoostedAllhad () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json);
  virtual void execute(const ReconstructedEvent& event);

  ClassDef(TTbarEventClassificationToolBoostedAllhad,2)

};
#endif

