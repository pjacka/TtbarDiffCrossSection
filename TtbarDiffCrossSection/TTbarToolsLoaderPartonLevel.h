#ifndef TTbarToolsLoaderPartonLevel_h
#define TTbarToolsLoaderPartonLevel_h

#include <string>
#include "NtuplesAnalysisToolsCore/AnaToolsLoaderBase.h"
class TTbarToolsLoaderPartonLevel : public AnaToolsLoaderBase {
public:

  TTbarToolsLoaderPartonLevel(const std::string& name);
  virtual ~TTbarToolsLoaderPartonLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json);
  virtual StatusCode initialize();

  ClassDef(TTbarToolsLoaderPartonLevel,3)

};
#endif

