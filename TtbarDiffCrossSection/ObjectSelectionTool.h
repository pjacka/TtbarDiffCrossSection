#ifndef ObjectSelectionTool_h
#define ObjectSelectionTool_h

#include <string>

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/EventReconstructionToolBase.h"
#include "NtuplesAnalysisToolsConfiguration/LargeRJetType.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObject.h"

#include "AsgTools/AsgTool.h"


typedef ReconstructedObjects::ReconstructedObject Object_t;

class ObjectSelectionTool : public EventReconstructionToolBase {

protected:

  bool m_isReco;
  
  LargeRJetType m_largeRJetType;         // Switch between standard antikt10 jets (LJETS), reclustered jets (RCJETS) and variable-R reclustered jets (VarRCJETS)
  
  bool m_useTrackJets; // Activates track jets
  
  bool m_useVarRCJets;             // Variable-R reclustered jets will be used only if it is set to True
  bool m_useVarRCJetsSubstructure;  // Activates VarRCJETS substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available
  
  bool m_useRCJets;                // Reclustered jets will be used only if it is set to True
  bool m_useRCJetsSubstructure;        // Activates RCJets substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available
  
  
  // small-R jets object selection cuts
  double m_jet_eta_max;
  double m_jet_pt_min;
  
  // small-R B-jets definition
  std::string m_bTaggerType;
  std::string m_bTaggerAttribute;
  int m_bTaggerMinCutInteger;
  
  
  
  // large-R jets object selection cuts
  double m_ljet_eta_max;
  double m_ljet_m_min;
  double m_ljet_pt_min;
  double m_ljet_pt_max;
  double m_ljet_mOverPt_max;
  bool m_ljet_particle_useRapidity;
  
  // rc jets object selection cuts
  double m_rcjet_eta_max;
  double m_rcjet_m_min;
  double m_rcjet_pt_min;
  double m_rcjet_pt_max;
  double m_rcjet_mOverPt_max;
  
  // variable-R rc jets object selection cuts
  double m_vrcjet_eta_max;
  double m_vrcjet_m_min;
  double m_vrcjet_pt_min;
  double m_vrcjet_pt_max;
  double m_vrcjet_mOverPt_max;
  
  // Electron object selection cuts
  double m_el_eta_max;
  double m_el_pt_min;
  
  // Muon object selection cuts
  double m_mu_eta_max;
  double m_mu_pt_min;

  virtual void readObjectsSetup(const nlohmann::json& json);

  virtual void readSmallRJetsSetup(const nlohmann::json& json);
  virtual void readSmallRBJetsSetup(const nlohmann::json& json);
  virtual void readLargeRJetsSetup(const nlohmann::json& json);
  virtual void readRCJetsSetup(const nlohmann::json& json);
  virtual void readVarRCJetsSetup(const nlohmann::json& json);
  virtual void readElectronsSetup(const nlohmann::json& json);
  virtual void readMuonsSetup(const nlohmann::json& json);
   
  virtual bool passedSmallRJetsObjectSelections(const Object_t& jet) const;
  virtual bool passedLargeRJetsObjectSelections(const Object_t& jet) const;
  virtual bool passedRCJetsObjectSelections(const Object_t& jet, float rcJet_m_tmp) const;
  virtual bool passedVarRCJetsObjectSelections(const Object_t& jet, float rcJet_m_tmp) const;
  virtual bool passedElectronObjectSelections(const Object_t& electron) const;
  virtual bool passedMuonObjectSelections(const Object_t& muon) const;
  
  virtual void selectSmallRJets(ReconstructedEvent& event) const;
  virtual void selectSmallRBJets(ReconstructedEvent& event) const;
  virtual void selectTrackJets(ReconstructedEvent& event) const;
  virtual void selectLargeRJets(ReconstructedEvent& event) const;
  virtual void selectRCJets(ReconstructedEvent& event) const;
  virtual void selectVarRCJets(ReconstructedEvent& event) const;
  virtual void selectElectrons(ReconstructedEvent& event) const;
  virtual void selectMuons(ReconstructedEvent& event) const;

public:

  ObjectSelectionTool(const std::string& name);
  virtual ~ObjectSelectionTool () = default;


  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual void execute(ReconstructedEvent& event) const override;
  
  ClassDefOverride(ObjectSelectionTool,2)

};
#endif

