#ifndef MMefficiencies_h
#define MMefficiencies_h
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <iomanip>      // std::setprecision
#include <utility>

#include "TString.h"
#include "TTree.h"
#include "TEnv.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TH1D.h"
#include "TVectorD.h"

#include "NtuplesAnalysisToolsCore/LargeRJet.h"
#include "TtbarDiffCrossSection/HistogramManagerAllHadronic.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "NtuplesAnalysisToolsCore/Minitree_truth.h"

class MMefficiencies{
public:
  int const itt,iLt,itL,iLL;
  float ljet1_pt,ljet1_eta,ljet1_phi,ljet1_m,ljet1_e,ljet1_y,ljet1_split12,ljet1_tau21,ljet1_tau32,
	   ljet2_pt,ljet2_eta,ljet2_phi,ljet2_m,ljet2_e,ljet2_y,ljet2_split12,ljet2_tau21,ljet2_tau32;
  float ttbar_mass,ttbar_pt,ttbar_eta,ttbar_phi,ttbar,ttbar_e,ttbar_y,deltaphi;
  float weight;
  float njets,nbjets;
  bool ljet1_btag,ljet1_toptag,ljet2_btag,ljet2_toptag;
  
  TTree * minitree_CR1;
  
  std::vector<std::vector<TH1D*>> histos;
  MMefficiencies();
  MMefficiencies(TFile*);
  virtual ~MMefficiencies () = default;
  
  
  void Fill(LargeRJet&,LargeRJet&,int const,int const,float weight);
  void Fill_v2(LargeRJet&,LargeRJet&,int const,int const,float weight);
  void Fill(LargeRJet&,LargeRJet&,int const,int const,int const,float weight);
  void Init_minitree(TTree*);
  void Init_histos();
  void Write();
  void DestroyHistograms();
  ClassDef(MMefficiencies,1)
};
#endif
