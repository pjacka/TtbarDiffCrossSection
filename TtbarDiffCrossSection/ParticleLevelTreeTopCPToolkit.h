#ifndef ParticleLevelTreeTopCPToolkit_h
#define ParticleLevelTreeTopCPToolkit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include <nlohmann/json.hpp>

#include "NtuplesAnalysisToolsCore/TreeTopCPToolkitBase.h"

class ParticleLevelTreeTopCPToolkit final : public TreeTopCPToolkitBase
{
protected:
   void initLargeRJets();
   void initSmallRJets();
   void initElectrons();
   void initMuons();
   void initMET();
   void initGenWeights();
public:
   // Declaration of leaf types
   ULong64_t eventNumber;
   UInt_t mcChannelNumber;
   Int_t num_truth_bjets_nocuts;
   Int_t num_truth_cjets_nocuts;
   UInt_t runNumber;
   std::vector<float> *el_charge;
   std::vector<unsigned int> *el_origin;
   std::vector<unsigned int> *el_type;
   std::vector<float> *el_e;
   std::vector<float> *el_eta;
   std::vector<float> *el_phi;
   std::vector<float> *el_pt;
   std::vector<int> *jet_nGhosts_bHadron;
   std::vector<int> *jet_nGhosts_cHadron;
   std::vector<float> *jet_e;
   std::vector<float> *jet_eta;
   std::vector<float> *jet_phi;
   std::vector<float> *jet_pt;
   std::vector<float> *ljet_D2;
   std::vector<int> *ljet_GhostBHadronsFinalCount;
   std::vector<int> *ljet_GhostCHadronsFinalCount;
   std::vector<float> *ljet_Qw;
   std::vector<float> *ljet_Tau1_wta;
   std::vector<float> *ljet_Tau2_wta;
   std::vector<float> *ljet_Tau3_wta;
   std::vector<float> *ljet_e;
   std::vector<float> *ljet_eta;
   std::vector<float> *ljet_phi;
   std::vector<float> *ljet_pt;
   std::vector<float> *mu_charge;
   std::vector<unsigned int> *mu_origin;
   std::vector<unsigned int> *mu_type;
   std::vector<float> *mu_e;
   std::vector<float> *mu_eta;
   std::vector<float> *mu_phi;
   std::vector<float> *mu_pt;
   Float_t weight_mc;
   Char_t pass_ljets;
   Float_t met_met;
   Float_t met_phi;

   // List of branches
   TBranch *b_eventNumber;                  //!
   TBranch *b_mcChannelNumber;              //!
   TBranch *b_num_truth_bjets_nocuts;       //!
   TBranch *b_num_truth_cjets_nocuts;       //!
   TBranch *b_runNumber;                    //!
   TBranch *b_el_charge;                    //!
   TBranch *b_el_origin;                    //!
   TBranch *b_el_type;                      //!
   TBranch *b_el_e;                         //!
   TBranch *b_el_eta;                       //!
   TBranch *b_el_phi;                       //!
   TBranch *b_el_pt;                        //!
   TBranch *b_jet_nGhosts_bHadron;          //!
   TBranch *b_jet_nGhosts_cHadron;          //!
   TBranch *b_jet_e;                        //!
   TBranch *b_jet_eta;                      //!
   TBranch *b_jet_phi;                      //!
   TBranch *b_jet_pt;                       //!
   TBranch *b_ljet_D2;                      //!
   TBranch *b_ljet_GhostBHadronsFinalCount; //!
   TBranch *b_ljet_GhostCHadronsFinalCount; //!
   TBranch *b_ljet_Qw;                      //!
   TBranch *b_ljet_Tau1_wta;                //!
   TBranch *b_ljet_Tau2_wta;                //!
   TBranch *b_ljet_Tau3_wta;                //!
   TBranch *b_ljet_e;                       //!
   TBranch *b_ljet_eta;                     //!
   TBranch *b_ljet_phi;                     //!
   TBranch *b_ljet_pt;                      //!
   TBranch *b_mu_charge;                    //!
   TBranch *b_mu_origin;                    //!
   TBranch *b_mu_type;                      //!
   TBranch *b_mu_e;                         //!
   TBranch *b_mu_eta;                       //!
   TBranch *b_mu_phi;                       //!
   TBranch *b_mu_pt;                        //!
   TBranch *b_weight_mc;                    //!
   TBranch *b_pass_ljets;                   //!
   TBranch *b_met_met;                      //!
   TBranch *b_met_phi;                      //!

   ParticleLevelTreeTopCPToolkit(const nlohmann::json &json);
   virtual ~ParticleLevelTreeTopCPToolkit() = default;

   virtual void initChain(TChain *tree, const TString &sysName) override;
   virtual void print() override;

   ClassDef(ParticleLevelTreeTopCPToolkit, 2)
};

#endif
