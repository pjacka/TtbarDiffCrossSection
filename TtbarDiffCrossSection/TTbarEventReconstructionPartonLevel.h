#ifndef TTbarEventReconstructionPartonLevel_h
#define TTbarEventReconstructionPartonLevel_h

#include <string>


#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree_truth.h"
#include "NtuplesAnalysisToolsCore/EventReconstructionToolBase.h"

class TTbarEventReconstructionPartonLevel : public EventReconstructionToolBase {

protected:

  bool m_storeW;
  bool m_storeB;
  
  
public:

  TTbarEventReconstructionPartonLevel(const std::string& name);
  virtual ~TTbarEventReconstructionPartonLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual void execute(ReconstructedEvent& event) const override;

  ClassDefOverride(TTbarEventReconstructionPartonLevel,2)

};
#endif

