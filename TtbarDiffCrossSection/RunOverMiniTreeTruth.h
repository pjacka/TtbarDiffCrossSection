#ifndef RunOverMiniTreeTruth_h
#define RunOverMiniTreeTruth_h

#include "NtuplesAnalysisToolsCore/Minitree_truth.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
//#include "TtbarDiffCrossSection/FatJet.h"

class TString;
class TChain;
class TH1D;

class RunOverMiniTreeTruth {
 
public :

  RunOverMiniTreeTruth(TString filelistname, TString outputName, int nEvents);
  virtual ~RunOverMiniTreeTruth();

  int ReadConfig();
  int LoadTree();
  int Initialise();
  int BookHistos();
  int Execute();
  int FillHistos();
  int Finalise();

  
  //void GetObjectsFromMinitree(Minitree* t,ReconstructedEvent* level,bool isReco);
  //void FindParticleLevel(ULong64_t eventNumber,bool & passed_particleLevel_cuts,vector<int> &selections_passed_ABCDsimple_tight_particle,vector<int> &selections_passed_ABCD16_tight_particle);
  //bool FindPartonLevel(ULong64_t eventNumber);
  //Long64_t FindIndex(ULong64_t eventNumber,vector<pair<ULong64_t,Long64_t> > const &indeces);
  //double GetCrossSection(int channel);
  //bool Passed_PartonLevel_Selections(T& LeadingJet,T& RecoilJet){
  //		if(RecoilJet.Pt() < 350.) return false;
  //		if(LeadingJet.Pt() < 500. ) return false;
  //		return true;
  //	}
  //static bool function_used_for_sorting(std::pair<double,int> i,std::pair<double, int> j);
  //static bool function_used_for_sorting_indeces(std::pair<ULong64_t,Long64_t> i,std::pair<ULong64_t,Long64_t> j);

  
private :

  //settings
  int m_debug;
  int m_GeV;
  TString m_rootCoreBin;

  int m_nEvents_parton; //nEvents to process

  //inputs
  TString           m_filelistname;
  TChain           *m_chain_parton;
  TString           m_treeName;
  Minitree_truth   *m_parton;
  std::unique_ptr<ReconstructedEvent> m_partonLevelInfo;

  //outputs
  TString m_outputName;
  TFile  *m_output;

  //output histos
  TH1D* h_top_pt_cmp;
  TH1D* h_top_eta_cmp;
  TH1D* h_top_phi_cmp;
  TH1D* h_top_m_cmp;

  TH1D* h_top_pt;
  TH1D* h_top_eta;
  TH1D* h_top_phi;
  TH1D* h_top_m;

  TH1D* h_top_pt_SC;
  TH1D* h_top_eta_SC;
  TH1D* h_top_phi_SC;
  TH1D* h_top_m_SC;

  ClassDef(RunOverMiniTreeTruth,1)
};
#endif

