#ifndef HistStore_H
#define HistStore_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "THnSparse.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"


#include "TtbarDiffCrossSection/FileStore.h"
#include "HelperFunctions/HistogramNDto1DConverter.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"

#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

using namespace std;

class Pseudoexperiment;
class NtuplesAnalysisToolsConfig;

class HistStore{
protected:
  bool m_nominalHistosLoaded;
  bool m_doRebin;
  bool m_useResolutionHistos;
  
  TString m_nominalTreeName;
  
  TH1D* m_hist_template; // histogram with correct binning and empty bins  
  TH1D* m_hist_template_truth; // histogram with correct binning and empty bins  
  
  std::shared_ptr<HistogramNDto1DConverter> m_histogramConverterND;
  std::shared_ptr<HistogramNDto1DConverter> m_histogramConverterNDTruth;
  
  TH1D* m_hist_data;
  TH1D* m_hist_signal_nominal;
  TH1D* m_hist_bkg_MC_nominal;
  TH1D* m_hist_bkg_Multijet_nominal;
  TH1D* m_hist_bkg_all_nominal;
  TH1D* m_hist_pseudodata_nominal;
  vector<TH1D*> m_hist_data_16regions_nominal;
  vector<TH1D*> m_hist_MC_16regions_nominal;
  vector<TH1D*> m_hist_MC_backgrounds_nominal;
  vector<TH1D*> m_hist_MC_backgrounds_sys;
  
  TH2D* m_migration_nominal;
  TH2D* m_resolution_nominal;
  TH1D* m_EffOfRecoLevelCutsNominator_nominal;
  TH1D* m_EffOfTruthLevelCutsNominator_nominal;
  TH1D* m_matchingEfficiencyRecoTruth_nominal;
  TH1D* m_hist_signal_truth_nominal;
  
  std::unique_ptr<TH1D> m_hist_signal_nominal_half1;
  std::unique_ptr<TH1D> m_hist_signal_nominal_half2;
  std::unique_ptr<TH1D> m_hist_signal_truth_nominal_half1;
  std::unique_ptr<TH1D> m_hist_signal_truth_nominal_half2;
  std::unique_ptr<TH2D> m_migration_nominal_half1;
  std::unique_ptr<TH2D> m_migration_nominal_half2;
  
  TH1D* m_hist_data_sys;
  TH1D* m_hist_signal_sys;
  TH1D* m_hist_bkg_MC_sys;
  TH1D* m_hist_bkg_Multijet_sys;
  vector<TH1D*> m_hist_data_16regions_sys;
  vector<TH1D*> m_hist_MC_16regions_sys;
  TH1D* m_hist_bkg_all_sys;
  TH1D* m_hist_pseudodata_sys;
  TH2D* m_hist_migration_sys;
  TH1D* m_hist_EffOfRecoLevelCutsNominator_sys;
  TH1D* m_hist_EffOfTruthLevelCutsNominator_sys;
  TH1D* m_hist_signal_truth_sys;  
  
  TH1DBootstrap* m_bootstrap_hist_data;
  TH1DBootstrap* m_bootstrap_hist_signal;
  TH1DBootstrap* m_bootstrap_bkg_MC;
  TH1DBootstrap* m_bootstrap_bkg_Multijet;
  TH1DBootstrap* m_bootstrap_bkg_all;
  TH1DBootstrap* m_bootstrap_pseudodata;
  vector<TH1DBootstrap*> m_bootstrap_data_16regions;
  vector<TH1DBootstrap*> m_bootstrap_MC_16regions;
  
  RooUnfoldResponse* m_response_nominal;
  
  // systematics
  vector<TString> m_sys_chain_names_all, m_sys_chain_names_single, m_sys_chain_names_pdf;
  vector<pair<TString,TString> > m_sys_chain_names_paired;
  vector<TString> m_reweighted_chain_names;
  
  int m_nsys_all,m_nsys_pairs,m_nsys_single,m_nsys_pdf;

  double m_lumi_shifted;
  
  vector<TH1D*> m_sys_histos_signal_all;
  vector<TH1D*> m_sys_histos_bkg_MC_all;
  vector<TH1D*> m_sys_histos_bkg_Multijet_all;
  vector<vector<TH1D*> > m_sys_histos_MC_16regions_all;
  vector<TH1D*> m_sys_histos_bkg_all_all;
  vector<TH1D*> m_sys_histos_pseudodata_all;
  
  vector<TH2D*> m_sys_migration_all;
  vector<TH1D*> m_sys_EffOfRecoLevelCutsNominator_all;
  vector<TH1D*> m_sys_EffOfTruthLevelCutsNominator_all;
  vector<TH1D*> m_sys_histos_signal_truth_all;

  std::shared_ptr<TEnv> m_config_tfiles,m_config_binning;
  std::unique_ptr<NtuplesAnalysisToolsConfig> m_ttbarConfig;
  TString m_config_binning_name;
  
  double m_ttbarSF;
  double m_lumi;
  TString m_mainDir;
  TString m_dirname,m_variable_name,m_level;
  TString m_signal; // DSID of signal ntuple
  
  
  FileStore* m_fileStore;
  int m_counter;
  const TString m_gap;
  
  vector<double> m_binning,m_binning_truth;
  vector<double> m_binning_x,m_binning_y;
  vector<double> m_binning_x_truth,m_binning_y_truth;
  int m_nbins_reco,m_nbins_truth;
  int m_nbins_x,m_nbins_x_truth;
  int m_nbins_y,m_nbins_y_truth;
  
  //16 regions specifications
  int m_nregions;
  int m_iRegionA;
  int m_iRegionB;
  int m_iRegionC;
  int m_iRegionD;
  int m_iRegionE;
  int m_iRegionF;
  int m_iRegionG;
  int m_iRegionH;
  int m_iRegionI;
  int m_iRegionJ;
  int m_iRegionK;
  int m_iRegionL;
  int m_iRegionM;
  int m_iRegionN;
  int m_iRegionO;
  int m_iRegionP;
  vector<TString> m_ABCD16_folder_names,m_ABCD16_region_names;

  int m_debug;

public:
  HistStore();
  virtual ~HistStore () = default;
  
  void initialize(FileStore* fileStore,int debugLevel, const TString& config, const TString& configBinningName);
  inline void doRebin(bool doRebin){m_doRebin=doRebin;}
  inline bool doRebin(){return m_doRebin;}
  
  void useResolutionHistos(bool useResolutionHistos=true){m_useResolutionHistos=useResolutionHistos;}
  void load_nominal_histos(const TString& histname,const TString& level);
  void WriteNominalHistos(TFile*f);
  void delete_nominal_histos();
  
  void load_nominal_histosND(const TString& histname,const TString& level);
  void WriteNominalHistosND(TFile*f);
  void delete_nominal_histosND();
  
  void load_one_sys_histos(const TString& chain_name);	
  void delete_one_sys_histos();
  void write_one_sys_histos(TFile*f,const TString& chain_name);	
  
  void load_one_sys_histosND(const TString& chain_name);	
  void delete_one_sys_histosND();
  void write_one_sys_histosND(TFile*f,const TString& chain_name);	
  
  //void load_systematic_histos();
  void load_bootstrapped_histos();
  void set_ABCD16_indexes();
  void set_ABCD16_folder_names();
  void prepare_binning();
  void prepare_binningND();

  TH1D* Calculate_ABCD16_estimate(const vector<TH1D*>& data,const vector<TH1D*>& MC,bool calculate_errors=false);
  TH1DBootstrap* Calculate_ABCD16_estimate(const vector<TH1DBootstrap*>& data,const vector<TH1DBootstrap*>& MC,bool calculate_errors=false);
  
  void WriteAllSysHistos(TFile* f,const TString& option="1D");
  void WriteBootstrapHistos(TFile* f);
  void WriteSignalModelingHistos(TFile* f, const TString& histname, const TString& sysName, const TString& fileName, const TString& chainName="nominal");
  void WriteSignalModelingHistosND(TFile* f, const TString& histname, const TString& sysName, const TString& fileName, const TString& chainName="nominal");
  void WriteEFTHistos(TFile* f);
  void PrepareStressTestInputs(TFile* f,const TString& option="",const int imin=1,const int imax=32);
  
  ClassDef(HistStore,1)

};




#endif

