#ifndef TTBarEventReaderParticleLevel_h
#define TTBarEventReaderParticleLevel_h

#include <string>

#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "NtuplesAnalysisToolsCore/EventReaderToolBase.h"
#include "NtuplesAnalysisToolsConfiguration/LargeRJetType.h"

#include "TtbarDiffCrossSection/ParticleLevelTreeTopCPToolkit.h"

#include "AsgTools/AsgTool.h"

class TTBarEventReaderParticleLevel : public EventReaderToolBase {

protected:
  
  std::unique_ptr<ParticleLevelTreeTopCPToolkit> m_treeReader;
  nlohmann::json m_treeReaderSettings;
      
  virtual void readSmallRJets(ReconstructedEvent& event) const;
  virtual void readLargeRJets(ReconstructedEvent& event) const;
  virtual void readElectrons(ReconstructedEvent& event) const;
  virtual void readMuons(ReconstructedEvent& event) const;
  virtual void readMET(ReconstructedEvent& event) const;
  virtual void readGenWeights(ReconstructedEvent& event) const;

public:

  TTBarEventReaderParticleLevel(const std::string& name);
  virtual ~TTBarEventReaderParticleLevel () = default;


  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual StatusCode initChain(const std::unordered_map<std::string, std::string>& settings) override;
  virtual ReconstructedEvent* readEntry(const ULong64_t ientry) override;
  virtual StatusCode initialize() override;
  virtual void print() const override;
  
  ClassDefOverride(TTBarEventReaderParticleLevel,2)

};
#endif

