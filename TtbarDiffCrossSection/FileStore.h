#ifndef FileStore_H
#define FileStore_H

#include "TLorentzVector.h"
#include <vector>


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TAxis.h"
#include "TGaxis.h"
#include "TSystem.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TProfile.h"
#include "TStyle.h"
#include "TROOT.h"
#include "THStack.h"
#include "TGraphAsymmErrors.h"
#include "TMatrixT.h"
#include "TLegend.h"
#include "TPad.h"
#include "TEnv.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TObject.h"

#include "BootstrapGenerator/TH1Bootstrap.h"
#include "BootstrapGenerator/TH1DBootstrap.h"
#include "BootstrapGenerator/TH2Bootstrap.h"
#include "BootstrapGenerator/TH2DBootstrap.h"


#include "../RooUnfold/src/RooUnfoldResponse.h"
#include "../RooUnfold/src/RooUnfoldBayes.h"
#include "../RooUnfold/src/RooUnfoldBinByBin.h"
#include "../RooUnfold/src/RooUnfoldSvd.h"
#include "../RooUnfold/src/RooUnfoldInvert.h"

#include "HelperFunctions/TtbarDiffCrossSection_exception.h"

using namespace std;

class NtuplesAnalysisToolsConfig;

class FileStore{
protected:
	TString m_mc_samples_production;
  TString m_nominalTreeName;
	vector<TFile*> m_input_tfiles; // 0 ... data, 1 ... signal, 2-end ... MC_background 
	int m_n_input_tfiles;
	
	map<TString,std::unique_ptr<TFile>> m_special_tfiles;
	int m_n_special_tfiles;

	TEnv *m_config_tfiles,*m_config_binning;

	TString m_mainDir;
	TString m_dirname,m_variable_name;
	TString m_level;
	
	const TString m_gap;
	
	
	int m_counter;
	vector<TString> m_list_of_histnames;
	
	//16 regions specifications
	int m_nregions;
	int m_iRegionA;
	int m_iRegionB;
	int m_iRegionC;
	int m_iRegionD;
	int m_iRegionE;
	int m_iRegionF;
	int m_iRegionG;
	int m_iRegionH;
	int m_iRegionI;
	int m_iRegionJ;
	int m_iRegionK;
	int m_iRegionL;
	int m_iRegionM;
	int m_iRegionN;
	int m_iRegionO;
	int m_iRegionP;
	vector<TString> m_ABCD16_folder_names;


	// systematics
	vector<TString> m_sys_chain_names_all, m_sys_chain_names_single,m_sys_chain_names_pdf;
	vector<pair<TString,TString> > m_sys_chain_names_paired;
	vector<TString> m_reweighted_chain_names;
	int m_nsys_all,m_nsys_pairs,m_nsys_single,m_nsys_pdf;
	int m_nreweighted_chains;
	
	double m_lumi;
	double m_ttbarSF;
	int m_debug;
	

public:
	FileStore();
	virtual ~FileStore () = default;
	
	void initialize(const TString& mainDir,int debugLevel,const TString& config_tfiles_name, const TString& config_lumi_name, const TString& config_binning_name);
	void load_input_tfiles();
	void load_special_tfiles();
	void load_stress_test_tfiles();
	void reload_input_tfiles();

	void create_list_of_histnames();
	
	template<typename T> void get_16regions_histos(vector<T*>& hist_data,vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname);
	template<typename T> void get_16regions_histos_MC_only(vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname);
	template<typename T> void get_16regions_histos_MC_for_pdf(vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname);
	template<typename T> void get_16regions_special_histos_MC_only(TString filename,vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname);
	template<typename T> bool get_16regions_histos(TFile* f,vector<T*>& histos,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	
	void get_16regions_BootstrapHistos(vector<TH1DBootstrap*>& hist_data,vector<TH1DBootstrap*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname);
	bool get_16regions_BootstrapHistos(TFile* f,vector<TH1DBootstrap*>& histos,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	void get_16regions_BootstrapHistos_MC_only(vector<TH1DBootstrap*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname);
	
	TObject* getObject(TFile* file,const TString& chain_name,const TString& level, const TString& histname);
	TH1* getHistogramObject(int ifile,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	TH1* getHistogramObject(TFile* file,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	TH1* getSpecialHistogram(TString filename,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	
	TH1DBootstrap* getBootstrapHistogram(int ifile,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	TH1DBootstrap* getBootstrapHistogram(TFile* file,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi=false);
	
	
	void set_ABCD16_indexes();
	void set_ABCD16_folder_names();
	void prepare_binning(TString variable_name);
	void prepare_list_of_systematics();
	void get_list_of_systematics(vector<TString>& all,vector<pair<TString,TString> >& paired,vector<TString>& single,vector<TString>& pdf);
	void make_list_of_reweighted_chains();
	inline const vector<TString>& get_list_of_reweighted_chains() const {return m_reweighted_chain_names;}
	
	inline TFile* get_input_tfile(int i){return m_input_tfiles[i];}
	inline TFile* get_special_tfile(const TString& filename){return m_special_tfiles[filename].get();}
	inline int get_n_input_tfiles(){return m_n_input_tfiles;};
	
	ClassDef(FileStore,1)
};


template<typename T> void FileStore::get_16regions_histos(vector<T*>& hist_data,vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname){
  if(!get_16regions_histos(m_input_tfiles[0],hist_data,chain_name,level,histname,false)) throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");
  get_16regions_histos_MC_only(hist_MC, chain_name, level, histname);
}
template<typename T> void FileStore::get_16regions_histos_MC_only(vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname){
  if(!get_16regions_histos(m_input_tfiles[1],hist_MC,chain_name,level,histname))
    throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");
  
  for(int i=0;i<15;i++)
    hist_MC[i]->Scale(m_ttbarSF);
  
  vector<T*> histos;
  for(int ifile=2;ifile<m_n_input_tfiles;ifile++){

    if(!get_16regions_histos(m_input_tfiles[ifile],histos,chain_name,level,histname)){throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");}



    for(int i=0;i<15;i++) {
      if(ifile==2)histos[i]->Scale(m_ttbarSF);
      hist_MC[i]->Add(histos[i]);
      delete histos[i];
    }

    histos.clear();
  }
}

template<typename T> void FileStore::get_16regions_histos_MC_for_pdf(vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname){
  if(!get_16regions_histos(m_input_tfiles[1],hist_MC,chain_name,level,histname))
  	throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");
  for(int i=0;i<15;i++)hist_MC[i]->Scale(m_ttbarSF);
  vector<T*> histos;
  for(int ifile=2;ifile<m_n_input_tfiles;ifile++){
    if(!get_16regions_histos(m_input_tfiles[ifile],histos,m_nominalTreeName,level,histname))
		throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");
    for(int i=0;i<15;i++) {
      if(ifile==2)histos[i]->Scale(m_ttbarSF);
      hist_MC[i]->Add(histos[i]);
      delete histos[i];
    }
    histos.clear();
  }
}

template<typename T> void FileStore::get_16regions_special_histos_MC_only(TString filename,vector<T*>& hist_MC,const TString& chain_name,const TString& level, const TString& histname){
  if(!get_16regions_histos(m_special_tfiles[filename].get(),hist_MC,chain_name,level,histname))throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");

  cout << "*****: ln:" << __LINE__ << " chain_name: "<<  chain_name << " level: " << level  << endl;

  for(int i=0;i<15;i++)hist_MC[i]->Scale(m_ttbarSF);
  vector<T*> histos;
  for(int ifile=2;ifile<m_n_input_tfiles;ifile++){
    if(!get_16regions_histos(m_input_tfiles[ifile],histos,m_nominalTreeName,level,histname))
		throw TtbarDiffCrossSection_exception("Error: Failed to load histograms");
    for(int i=0;i<15;i++) {
      if(ifile==2)histos[i]->Scale(m_ttbarSF);
      hist_MC[i]->Add(histos[i]);
      delete histos[i];
    }
    histos.clear();
  }
}

template<typename T> bool FileStore::get_16regions_histos(TFile* f,vector<T*>& histos,const TString& chain_name,const TString& level, const TString& histname,const bool use_lumi){
  TString name;
  TString path;
  histos.resize(15);
  for(int i=0;i<15;i++){
    path = chain_name + "/" + m_ABCD16_folder_names[i] + "/" + level;
    name = path + "/" +histname;
    histos[i] =(T*)f->Get(name);
    if(use_lumi)histos[i]->Scale(m_lumi);
  }
  return true;	
}

#endif
