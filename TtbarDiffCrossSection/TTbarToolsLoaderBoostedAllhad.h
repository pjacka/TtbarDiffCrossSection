#ifndef TTbarToolsLoaderBoostedAllhad_h
#define TTbarToolsLoaderBoostedAllhad_h

#include <string>
#include "NtuplesAnalysisToolsCore/AnaToolsLoaderBase.h"

class TTbarToolsLoaderBoostedAllhad : public AnaToolsLoaderBase {
protected:

  bool m_isReco;

public:

  TTbarToolsLoaderBoostedAllhad(const std::string& name);
  virtual ~TTbarToolsLoaderBoostedAllhad () = default;

  virtual StatusCode initialize();
  virtual StatusCode readJSONConfig(const nlohmann::json& json);


  ClassDef(TTbarToolsLoaderBoostedAllhad,3)

};
#endif

