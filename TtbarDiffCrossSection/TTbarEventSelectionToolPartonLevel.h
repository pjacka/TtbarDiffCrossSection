#ifndef TTbarEventSelectionToolPartonLevel_h
#define TTbarEventSelectionToolPartonLevel_h

#include <string>

#include "TH1D.h"

#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"

#include "NtuplesAnalysisToolsCore/EventSelectionToolBase.h"

class TTbarEventSelectionToolPartonLevel : public EventSelectionToolBase {

protected:
  
  TH1D* m_cutflow;
  TH1D* m_cutflow_noweights;
  double m_top1PtMin;
  double m_top2PtMin;
public:

  TTbarEventSelectionToolPartonLevel(const std::string& name);
  virtual ~TTbarEventSelectionToolPartonLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual bool execute(const ReconstructedEvent&, bool fillCutflow=true) const override;
  virtual TH1D* initCutflowHistogram(const std::string& name) const override;

  ClassDefOverride(TTbarEventSelectionToolPartonLevel,2)

};
#endif

