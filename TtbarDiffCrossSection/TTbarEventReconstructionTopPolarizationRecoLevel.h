#ifndef TTbarEventReconstructionTopPolarizationRecoLevel_h
#define TTbarEventReconstructionTopPolarizationRecoLevel_h

#include <string>


#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/Minitree.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionClosestMass.h"

class TTbarEventReconstructionTopPolarizationRecoLevel : public TTbarEventReconstructionClosestMass {

protected:

  double m_minSubjetPtFrac;
  
  
public:

  TTbarEventReconstructionTopPolarizationRecoLevel(const std::string& name);
  virtual ~TTbarEventReconstructionTopPolarizationRecoLevel () = default;

  virtual StatusCode readJSONConfig(const nlohmann::json& json) override;
  virtual void execute(ReconstructedEvent& event) const override;

  ClassDefOverride(TTbarEventReconstructionTopPolarizationRecoLevel,4)

};
#endif

