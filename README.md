Table of Contents
=========================

**[Description](#description)**

**[Installation](#installation)**

**[Setup environment](#setup-environment)**

**[Instructions for developers](#instructions-for-developers)**

**[Preparing ntuples](#preparing-input-ntuples)**

**[Running the code](#running-the-code)**

**[Histograms definitions](#histograms-definitions)**

**[Producing unfolded plots](#producing-unfolded-plots)**

**[Producing Event displays with ATLANTIS](#producing-event-displays-with-atlantis)**



Description
=======================
This package is a framework for ttbar differential cross-section analyses.

## Contacts :
- Petr Jacka <petr.jacka@cern.ch>

# Installation

**1. Make working directory and navigate inside:**

```bash
mkdir TopPolarization
cd TopPolarization
```

**2. Make `source` and `build` directory**


a)  Without local changes in athena packages (recommended)

Create directories:
```bash
mkdir source build
```

Copy athena files - stored locally on nfs19:
```bash
cp -r /mnt/nfs19/jacka/AthenaFiles/.vscode .
cp /mnt/nfs19/jacka/AthenaFiles/CMakeLists.txt source/
cp /mnt/nfs19/jacka/AthenaFiles/version.txt source/
```




b) With sparse checkout of athena packages (enable local modifications in athena) - not tested

```bash
mkdir build
git atlas init-workdir -b 25.2 https://:@gitlab.cern.ch:8443/<$CERN_USER_NAME>/athena.git
cd athena
```
and add any package you want to modify using

```bash
git atlas addpkg <package_name>
```
```bash
cd ..
```


**3. Setup Environment**

el9 container is required to setup appropriate AnalysisBase version.

- Create `.bashrc` and `.bash_profile` files in your home directory if not exist

- One of them is expected to contain environment settings the other one is sourcing 

- Assuming the `.bash_profile` containing environment settings, `.bashrc` should contain

```bash
if [ -f ~/.bash_profile ]; then
  source ~/.bash_profile
fi
```

- Make sure that `.bash_profile` does not contain a string sourcing `.bashrc` file to avoid a cyclic execution

- Place your environment settings into `.bash_profile` 

- Place the following functions into `.bash_profile`

  - Function to start container
```bash
function startContainer {
  /cvmfs/atlas.cern.ch/repo/containers/sw/apptainer/x86_64-el7/1.2.2/bin/apptainer exec \
  --env "PS1=$PS1" \
  -B /cvmfs:/cvmfs \
  -B /mnt:/mnt \
  -B /home:/home \
  /cvmfs/atlas.cern.ch/repo/containers/fs/singularity/x86_64-almalinux9 \
  /bin/bash
}
```
  - TopPolarizationSetup function (to be executed inside container)

```bash
function TopPolarizationSetup {
  
  cd ${HOME}/TopPolarization/build
  source ../source/TtbarDiffCrossSection/scripts/setup_atlas.sh
  asetup --restore
  lsetup git
  lsetup panda
  export RUCIO_ACCOUNT=<CERN_USER_NAME>
  lsetup rucio
  lsetup pyami
  source x86_64-*/setup.sh
  cd ../source
  source TtbarDiffCrossSection/scripts/setup.sh
}
```


- (optional) Place the following lines into `.bash_profile` to make the bash prompt more friendly. Customize it according to your wishes.
```bash
# BASH prompt settings
PS1="\u@\H:\W:\$ "
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -a'
```

- Git setup:
```bash
lsetup git
```

- Kerberos setup:
```
kinit -r7d <cern-user-name>@CERN.CH
```

Navigate to build directory (Change the path accordingly)
```bash
cd ~/TopPolarization/build/
```

AnalysisBase setup. Version is not very important if working without local modifications in athena
```bash
asetup AnalysisBase,25.2.10,here
cd ..
```

**4. To check out and set up the code, simply do:**

```bash
cd source
# Clone using kerberos authentization (require valid kerberos ticket)
git clone -b Rel25 https://:@gitlab.cern.ch:8443/pjacka/TtbarDiffCrossSection.git
# Or clone via ssh (add ssh key into your profile)
git clone -b Rel25 ssh://git@gitlab.cern.ch:7999/pjacka/TtbarDiffCrossSection.git
```

**5. Download necessary packages:**

**NtuplesAnalysisTools package:**
```bash
# Clone using kerberos authentization (require valid kerberos ticket)
git clone --recurse-submodules -b Rel25 https://:@gitlab.cern.ch:8443/pjacka/NtuplesAnalysisTools.git
# Or clone via ssh (add ssh key into your profile)
git clone --recurse-submodules -b Rel25 ssh://git@gitlab.cern.ch:7999/pjacka/NtuplesAnalysisTools.git
```


**Package with bootstrap method tools:**
```bash
git clone https://:@gitlab.cern.ch:8443/cjmeyer/BootstrapGenerator.git
```

**Package with NNLO reweighting tools:**
```bash
git clone https://:@gitlab.cern.ch:8443/pinamont/TTbarNNLOReweighter.git
```
Fix CMakeLists.txt:
Open `TTbarNNLOReweighter/CMakeLists.txt` and remove lines 
```
atlas_depends_on_subdirs(PUBLIC
                       Tools/PathResolver)
```

**RooUnfold package:**
```bash
git clone -b 2.1 https://:@gitlab.cern.ch:8443/RooUnfold/RooUnfold.git
```

**docopt.cpp package:**

Package to handle input parameters. See documentation in https://github.com/docopt/docopt.cpp
```bash
git clone https://github.com/docopt/docopt.cpp.git docopt
```

**6. Setup:**
```bash
source TtbarDiffCrossSection/scripts/setup.sh 
```

**7. Compile all packages:**

```bash
cd ../build
```

a) Without checkout of athena packages

```bash
cmake ../source && make -j8 && source x86_64-*/setup.sh
```

b) With sparse checkout of athena packages

```bash
cmake ../athena/Projects/WorkDir && make -j8 && source x86_64-*/setup.sh
```

**8. Setup environment variables in**
```bash
TtbarDiffCrossSection/scripts/setup.sh
```
setup the environment variables in a similar way as other users do


Setup environment
=======================

Start alma9 linux container using predefined function in your `.bash_profile`:
```bash
startContainer
```
and setup the environment inside the container
```bash
TopPolarizationSetup
```



If needed, compile packages (inside build directory) using
```bash
cmake ../source && make -j8 && source x86_64-*/setup.sh
```
or
```bash
cmake ../athena/Projects/WorkDir && cmake --build ./ && source x86_64-*/setup.sh
```
depending on your installation.

Tip: If you work with the same AnalysisBase if you didn't add any new source or heather file you can skip `cmake ../source`, i.e.
```bash
make -j8 && source x86_64-*/setup.sh
```
It is much faster

N.B.: If AnalysisBase version has changed it is required to clean the build directory and compile everything again


Instructions for developers
=======================

Global git settings:
```
git config --global user.email "<user.name>@cern.ch"
git config --global user.name "<shortUserName>"
git config pull.rebase true
git config --global diff.colorMoved zebra # optional
```

Making changes:

**1. Create your own branch** 
```bash
git fetch
git checkout -b Rel25-<YOUR_NAME> Rel25 --no-track
git remote add upstream origin
```
Your branch is based on Rel25 branch. You can develop your code in there


**2. Sending your changes into your personal branch**

Use
```bash
git status -uno
```
to list your local changes (-uno is used to skip not-tracked files)

Select files you want to send to your branch
```bash
git add <path-to-files>
```
Commit changes to your branch
```bash
git commit -m "<Write info about update here>"
git push -u origin Rel25-<YOUR_NAME>
```

**3. Creating merge request to Rel25 branch**

After you finish the point 3 you will see a link to create the merge request. You can also create the MR directly from
https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/merge_requests/new

Go to the link and create the MR.

Developers can approve their own merge requests and merge it but it is recommendend to ask somebody else to review their changes before the approval.

**4. Resolving conflicts**

Conflicts may happen if more developers work on the same file. The conflicts should be resolved by rebasing your branch with Rel25 branch:

```bash
git fetch origin Rel25
git rebase origin/Rel25
# When using submodules, use:
git submodule update --init --recursive
```

**5. Download necessary packages:**

**NtuplesAnalysisTools package:**
```bash
# Clone using 
``` 

This will attempt to rebase your local branch with origin/Rel25 and it will stop when the conflict happens.

Use 
```bash
git status -uno
```
to list conficted files (shown both modified)

Open the file with conflict, resolve the conflict and use
```bash
git add <path-to-file>
```
After all conflicts resolved
```bash
git rebase --continue
```
Repeat these steps until all conflicts are resolved. Then, use force push to your remote branch
```bash
git push -f
```







Preparing input ntuples (Update is needed)
=======================
Ntuples are made using TTbarDiffXsTools package. 
See [instructions](https://gitlab.cern.ch:8443/atlasphys-top/xs/TTbarDiffXsTools/blob/Rel21/data/AllHadronicBoosted/RunningGridJobs.md).



Running the code
=======================

**1: [Setup environment](#setup-environment)**

**2: Make filelists:**

From ntuples downloaded locally:

```bash
cd TtbarDiffCrossSection/filelists/
./MakeFilelists.sh
cd ../../
```

From ntuples stored on local group grid disk:

This requires to setup grid proxy and rucio:
(When running in container: `lsetup rucio` is needed to enable the `voms-proxy-init` command.)

```bash
voms-proxy-init -voms atlas -valid 96:00
lsetup panda
export RUCIO_ACCOUNT=pjacka
lsetup rucio
lsetup pyami
```

```bash
cd TtbarDiffCrossSection/filelists/
./MakeFilelists_SE_all.sh
cd ../../
```

You should see several files in the directory TtbarDiffCrossSection/filelists/ . Try to open one. You should see the path to certain input minitree.

Split filelists for jobs (Assuming that filelists with all files already exists)
```bash
./TtbarDiffCrossSection/scripts/SplitFileLists_All.sh 
```

**3: Framework configuration:**

The analysis framework is configured via [config.json](data/config.json). See [config.md](data/config.md) for detailed description.

**4: Run over ntuples:**

In the case, that ntuples are stored on grid, it is required to have a valid grid proxy:
(When running in container: `lsetup rucio` is needed to enable the `voms-proxy-init` command.)
```bash
voms-proxy-init -voms atlas -valid 96:00
```
Rucio should not be setup. Start new session (container) in the case that rucio is already setup. 

**Run locally** (for testing):
```bash
TtbarDiffCrossSection/scripts/run.sh
```
and set sample to 'all'
```bash
sample=all 
```
to run over all important samples. You can also select other options to run only one sample

Now execute the command
```bash
./TtbarDiffCrossSection/scripts/run.sh
```
 - run the script for 
	- 'all' (for)
	- 'reweighting1', 'reweighting2' (needed for Unfolding.sh)
 - If you want to run with doSys=1, please follow the instructions to run jobs on fzu farm

 - Script will create output rootfiles in $TtbarDiffCrossSection_output_path directory

**Run condor jobs (recommended):**

- Submit all jobs

    Open 
    
    ```bash
    /TtbarDiffCrossSection/condor_scripts/SubmitFarm_All.sh
    ``` 
    and select jobs for run
    
    ```bash
    ./TtbarDiffCrossSection/condor_scripts/SubmitFarm_All.sh
    ```
- Submit specific job
  
    ```bash
    ./TtbarDiffCrossSection/condor_scripts/SubmitFarm.sh
    ```

  This will create dagman jobs. Rootfiles from splitted filelists will be merged automatically 
  after all jobs for one sample are finished.
  
  You can check status of your jobs using
  
  ```bash
  condor_q
  ```
  
  When job finish you will receive email with info. Please check whether exit status is zero. 
  
  Log files are stored in 
  ```bash
  $TtbarDiffCrossSection_output_path/batch/produce_hists/logs/
  ```
  
  If you need to access ntuples on local group grid disk, 
  make sure you have access to those ntuples and 
  you have created grid proxy before job submition using
  
  ```bash
  voms-proxy-init -voms atlas -valid 96:00
  ```
  
  In order to get access to ntuples on local group grid disk, you need to setup two environment variables
  
  ```bash
  export X509_USER_PROXY=$HOME/.x509up
  export RUCIO_ACCOUNT=<CERN_USERNAME>
  ```


**5: Merge samples**

We are merging rootfiles corresponding to different samples. For example, we need to merge sliced samples or we need to sum contributions of similar processes.

Local run:

```bash
./TtbarDiffCrossSection/scripts/mergeSamples.sh
```

**Condor jobs(recommended):**

```bash
./TtbarDiffCrossSection/condor_scripts/mergeSamplesCondor.py
``` 
 
 
**6: run ABCD_tools to get ABCD multijet background estimate**

Run locally:

```bash
./NtuplesAnalysisTools/ABCD_Tools/scripts/ABCD_Tools.sh
```

Condor jobs (recommended):

```bash
./TtbarDiffCrossSection/condor_scripts/ABCD_Tools.sh
```


Many ABCD estimates are created in directories
```bash
$TtbarDiffCrossSection_output_path/ABCD.<MC_PROD>/ABCD16_*
$TtbarDiffCrossSection_output_path/ABCD.<MC_PROD>/ABCD9_*
```

The current nominal estimate is saved in 
```bash
$TtbarDiffCrossSection_output_path/ABCD.<MC_PROD>/ABCD16_BBB_S9_tight
```
Information about ABCD inputs is saved in textfiles in
```bash
$TtbarDiffCrossSection_output_path/ABCD.<MC_PROD>/ABCD_hist_info
```

Results of many tests of the ABCD procedure are saved in 
```bash
$TtbarDiffCrossSection_output_path/ABCD.<MC_PROD>/ABCD_tests
```

**7: Draw Data/MC comparison at detector level for all kind of distributions**

Locally

```bash
./TtbarDiffCrossSection/macros/DrawAll.sh 
```

Condor jobs (recommended):

For full set of plots (Controlled by dagman)
```bash
./TtbarDiffCrossSection/condor_scripts/DrawAllCondor.py
```
User can define two optional parameters
```bash
-f (--force): Using force to overwrite previous results (if exists)
-s (--suppress_notifications):  Notifications from individual jobs will be suppressed
```


For only specific set of plots one can still use
```bash
./TtbarDiffCrossSection/condor_scripts/DrawAllCondor.sh
```

Plots with nominal configuration are saved in 
```bash
$TtbarDiffCrossSection_output_path/*/Leading_1t_1b_Recoil_1t_1b_tight/recoLevelPlots_Signal_nominal_Multijet_ABCD16_BBB_S9_tight
```
where * is pdf.MC_PROD or png.MC_PROD, where MC_PROD is MC16a, MC16d or All. Plots with signal normalized to (Data - Bkg) are saved in pdf.MC_PROD_normalized directory. 

Plots with other configurations (different signal sample, ABCD estimate) are saved in directories with similar names. 
 


**8: Create unfolding plots**
```bash
./TtbarDiffCrossSection/macros/Unfolding.sh
```
Creates testing plots with unfolding variables

Perform several tests of the unfolding procedure

Results are saved in 
```bash
$TtbarDiffCrossSection_output_path/pdf.MC_PROD/Leading_1t_1b_Recoil_1t_1b_tight/Unfolding_reweighted*
```

**9: Prepare unfolded spectra**

This incudes calculation of uncertainties using pseudoexperiments and creation of plots 
with summary of uncertainties and summary tables.

**Condor jobs (recommended):**

```bash
./TtbarDiffCrossSection/condor_scripts/prepareUnfoldedSpectraCondor.sh
```

**Running locally:**


- Prepare histograms in a compact form
  ```bash
  ./TtbarDiffCrossSection/scripts/PrepareSystematicHistos.sh
  ```
  Nominal and shifted histograms will be saved in 
  ```bash
  $TtbarDiffCrossSection_output_path/root.MC_PROD/systematics_histos
  ```
  Bootstrap histos will be saved in 
  ```bash
  $TtbarDiffCrossSection_output_path/root.MC_PROD/bootstrap
  ```
  Stress test histos will be saved in 
  ```bash
  $TtbarDiffCrossSection_output_path/root.MC_PROD/stress_test_histos
  ```
  
  Histograms are rebined during creation of files. One can setup the new binning in 
  [data/unfolding_histos_optimizedBinning.env](https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/blob/Rel25/data/unfolding_optimizedBinning.env) and in
  [data/unfolding_histos2D_optimizedBinning.env](https://gitlab.cern.ch/pjacka/TtbarDiffCrossSection/blob/Rel25/data/optBinningND) for ND distributions.
  


- Produce systematic tables and covariance matrices for generator systematics
  ```bash
  ./TtbarDiffCrossSection/scripts/runGeneratorSystematics.sh
  ```
  Using rootfiles in 
  ```bash
  $TtbarDiffCrossSection_output_path/root.MC_PROD/systematics_histos as input
  ```
  Will produce modeling covarinces - will be saved in  
  ```bash
  $TtbarDiffCrossSection_output_path/root.MC_PROD/generatorSystematics/genSystematics_using_signal_410506
  ```
- Run pseudoexperiments to propagate uncertainties through unfolding
  ```bash
  ./TtbarDiffCrossSection/scripts/ProducePseudoexperiments.sh
  ```
  using $TtbarDiffCrossSection_output_path/root.MC_PROD/systematics_histos, $TtbarDiffCrossSection_output_path/root/bootstrap 
  and $TtbarDiffCrossSection_output_path/root.MC_PROD/generatorSystematics/genSystematics_using_signal_410506 as an input
    
  Program will create covariances for differential cross-sections at parton and particle level..
  Output is saved in 
  ```bash
  $TtbarDiffCrossSection_output_path/root.MC_PROD/covariances
  ```
  Latex tables with final results also created - saved in 
  ```bash
  $TtbarDiffCrossSection_output_path/tex.MC_PROD/final_results
  ```

- Make sumarry tables with uncertainties, full breakdown tables and plots with uncertainties
  ```bash
  ./TtbarDiffCrossSection/scripts/CalculateAsymmetricErrors.sh
  ```
  Tables are saved in 
  ```bash
  $TtbarDiffCrossSection_output_path/tex.MC_PROD/SystematicUncertainties
  ```



**10: Create concatenated histograms with all spectra for big covariance**
```bash
./TtbarDiffCrossSection/scripts/PrepareConcatenatedHistograms.sh 
```
The concatenated histogram are stored in 
```bash
$TtbarDiffCrossSection_output_path/root.MC_PROD/systematics_histos with name concatenated_<level>.root
```



**11: make data vs. theory comparison plots and chi-square tests**

Old script:
```bash
./TtbarDiffCrossSection/scripts/drawTheoryVsDataComparisonPlots.sh
```
New script:
```bash
./TtbarDiffCrossSection/scripts/drawTheoryVsDataComparison.sh
```
  
Creates final plots at parton and particle level and creates inputs for Riccardos scripts. Plots are saved in 
```bash
$TtbarDiffCrossSection_output_path/pdf.MC_PROD/Theory_vs_data_comparison
```
Inputs for Riccardos scripts are saved in 
```bash
$TtbarDiffCrossSection_output_path/root.MC_PROD/Theory_vs_data_comparison
```
**12: make nice style plots using Riccardos scripts (see below for instruction)**


## Histograms definitions

The way how to define histograms is described in [documentation/ConfigurationFiles.md](documentation/ConfigurationFiles.md#configuration-of-histograms). (obsolete)

## Producing unfolded plots

**1. get the code**

In athena directory

```bash
  git clone -b Rel21 https://:@gitlab.cern.ch:8443/atlasphys-top/xs/TTbarDiffXsTools.git
  cd TTbarDiffXsTools
```
**2. setup root**

**3. prepare running directory:**
```bash
   mkdir run; 
   cd scripts/TTAllHadBoosted/ 
   cp MakePlotsUnfolded.py PlottingToolkit.py rootlogon.C AtlasStyle.* AtlasUtils.C Datasets.py Defs.py ../../run
   cd ../../run
   #make link to unfolded historgram
   ln -s $TtbarDiffCrossSection_output_path/root/Theory_vs_data_comparison unfolding_results
```
**4.  make plots**
```bash
   ./MakePlotsUnfolded.py -c  plots_parton_abs.xml
   ./MakePlotsUnfolded.py -c  plots_parton_rel.xml
   #./MakePlotsUnfolded.py -c  plots_particle_rel.xml
   #./MakePlotsUnfolded.py -c  plots_particle_abs.xml
```


## Producing Event displays with ATLANTIS 

### To start Atlantis:
- **run directly from web** (need java webstart):
[http://atlantis.web.cern.ch/atlantis/AtlantisJava-09-16-06-07-webstart/atlantis.jnlp](http://atlantis.web.cern.ch/atlantis/AtlantisJava-09-16-06-07-webstart/atlantis.jnlp)


- **OR: install locally**: download from 
  [http://atlantis.web.cern.ch/atlantis/](http://atlantis.web.cern.ch/atlantis/)
  and unpack and run '
  ```bash
  java -jar atlantis.jar
  ```
- **OR:** 
```bash
setupATLAS
lsetup atlantis
runAtlantis
```
- **as an input, one needs .xml file** (contains info about a given event)
  - the input files for interesting event are here:
  ```bash
       ui8:/home/lysak/DiffXsecBoosted/EventDisplay
  ```
- **I also created the configuration files** (they are saved in same directories)

- **to run atlantis with a given config file and input XML file, execute, e.g.:**
   ```bash
   atlantis -c Atlantis-config_284285_v2.xml -s JiveXML_284285_3682216408.xml
   ```
   
**source of info:**
  
  http://atlantis.web.cern.ch/atlantis/
  
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/Atlantis


