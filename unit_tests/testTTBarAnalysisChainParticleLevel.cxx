#include "TtbarDiffCrossSection/TTbarEventClassificationToolParticleLevel.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionTopPolarizationParticleLevel.h"
#include "TtbarDiffCrossSection/TTbarEventSelectionToolParticleLevel.h"
#include "NtuplesAnalysisToolsCore/EventReweightingToolCommon.h"
#include "NtuplesAnalysisToolsCore/EventWeightSFToolLoaderCommon.h"
#include "TtbarDiffCrossSection/ObjectSelectionTool.h"
#include "TtbarDiffCrossSection/TTBarEventReaderParticleLevel.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/SampleMetadata.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"
#include "TCanvas.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

#include "TopEvent/EventTools.h"

int main(int /*nArg*/, char ** /* arg*/)
{

    // MC Signal, DSID: 601237
    const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";

    auto chain = std::make_unique<TChain>();

    chain->AddFile(file.c_str(), TChain::kBigNumber, "particleLevel");

    const ULong64_t nentries = std::min<ULong64_t>(10000, chain->GetEntries());
    std::cout << chain->GetEntries() << std::endl;

    auto f = std::unique_ptr<TFile>{TFile::Open(file.c_str())};

    std::cout << "Reading event reader config" << std::endl;
    const nlohmann::json configEventReader = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReaderParticleLevelConfig.json"));

    std::cout << "Object selection config" << std::endl;
    const nlohmann::json configObjectSelection = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarObjectSelectionParticleLevelConfig.json"));

    const nlohmann::json configEventReweighting = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReweightingToolTruthLevelConfig.json"));

    std::cout << "Event selection config" << std::endl;
    const nlohmann::json configEventSelection = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventSelectionToolParticleLevelConfig.json"));

    const nlohmann::json configEventClassification = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventClassificationParticleLevelConfig.json"));

    const nlohmann::json configLumi = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/luminositySettingsConfig.json"));

    auto reader = std::make_unique<TTBarEventReaderParticleLevel>("RecoTreeReader");

    top::check(reader->readJSONConfig(configEventReader), "Failed to read RecoTreeReader json config!");
    reader->setChain(chain.get());
    reader->readMetadata(*f);
    top::check(reader->initialize(), "Failed to initialize RecoTreeReader!");

    const SampleMetadata &metadata = reader->getMetadata();
    const double lumi = configFunctions::getLumi(configLumi, metadata.getMCProduction());
    std::cout << "Lumi: " << lumi << std::endl;

    auto object_selection_tool = std::make_unique<TTbarEventReconstructionTopPolarizationParticleLevel>("ObjectSelectionToolParticleLevel");
    top::check(
        object_selection_tool->setProperty("isReco", false),
        "Failed to setup reco level reconstruction tool");

    top::check(
        object_selection_tool->setProperty("debugLevel", 10),
        "Failed to setup reco level event reconstruction tool");

    top::check(
        object_selection_tool->readJSONConfig(configObjectSelection),
        "Failed to read reconstruction tool config");

    auto event_reweighting_tool = std::make_unique<EventReweightingToolCommon>("EventReweightingToolParticleLevel");

    top::check(
        event_reweighting_tool->setProperty("lumi", lumi),
        "Failed to setup particle level event reweighting tool");

    top::check(
        event_reweighting_tool->setProperty("xsecWithKfactor", 839.),
        "Failed to setup particle level event reweighting tool");

    top::check(
        event_reweighting_tool->setProperty("sumWeights", 1.e+07),
        "Failed to setup particle level event reweighting tool");

    event_reweighting_tool->setEventSFToolLoader(std::make_unique<EventWeightSFToolLoaderCommon>("testLoader"));

    top::check(
        event_reweighting_tool->readJSONConfig(configEventReweighting),
        "Failed to read event reweighting tool config");

    auto event_selection_tool = std::make_unique<TTbarEventSelectionToolParticleLevel>("EventSelectionToolParticleLevel");
    top::check(
        event_selection_tool->setProperty("debugLevel", 10),
        "Failed to setup reco level event reconstruction tool");

    top::check(
        event_selection_tool->readJSONConfig(configEventSelection),
        "Failed to read event selection tool config");

    TH1D *h_cutflow = event_selection_tool->initCutflowHistogram("cutflow");
    TH1D *h_cutflow_noweights = event_selection_tool->initCutflowHistogram("cutflow_noweights");
    top::check(event_selection_tool->setProperty("cutflow", h_cutflow), "Failed to setup reco level event selection tool");
    top::check(event_selection_tool->setProperty("cutflowNoWeights", h_cutflow_noweights), "Failed to setup reco level event selection tool");

    top::check(
        event_selection_tool->initialize(),
        "Failed to setup reco level event reconstruction tool");

    auto event_classification_tool = std::make_unique<TTbarEventClassificationToolParticleLevel>("TTbarEventClassificationToolParticleLevel");
    top::check(
        event_classification_tool->readJSONConfig(configEventClassification),
        "Failed to read event classification tool config");

    //   const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "NOSYS"}};
    const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "GEN_MUR05_MUF1_PDF260000"}};
    top::check(reader->initChain(chain_settings), "Failed to initialize chain in RecoTreeReader!");

    // Simple event loop
    for (ULong64_t ientry = 0; ientry < nentries; ientry++)
    {
        auto event = std::unique_ptr<ReconstructedEvent>{reader->readEntry(ientry)};
        std::cout << "ientry: " << ientry << std::endl;
        reader->print();
        event->print();
        object_selection_tool->execute(*event);
        event->print();
        std::cout << "Weight_mc: " << event->getAttribute<float>("weight_mc") << std::endl;

        top::check(
            event_reweighting_tool->execute(*event),
            "Failed to execute parton level event reweighting tool");
        const double weight = event_reweighting_tool->getWeight();

        std::cout << "Generator weight: " << weight << std::endl;

        h_cutflow_noweights->Fill(1);
        h_cutflow->Fill(1, weight);

        bool selection_passed = event_selection_tool->execute(*event, true);
        std::cout << "Selected event: " << selection_passed << std::endl
                  << std::endl;

        if (!selection_passed)
            continue;
        event_classification_tool->execute(*event);

        const std::vector<int> &region_indexes = event_classification_tool->getSelections();
        const int region_index = std::distance(std::begin(region_indexes), std::find_if(std::begin(region_indexes), std::end(region_indexes), [](auto x)
                                                                                        { return x != 0; }));
        std::cout << "Selected region: " << region_index << " Signal region: " << (region_index == 15) << std::endl;
    }
    TCanvas c("c");
    h_cutflow_noweights->Draw("text");
    c.SaveAs("cutflow_noweights_particle_level.png");
    h_cutflow->Draw("text");
    c.SaveAs("cutflow_particle_level.png");
    std::cout << "Percentage of selected events: " << h_cutflow_noweights->GetBinContent(h_cutflow_noweights->GetNbinsX()) / h_cutflow_noweights->GetBinContent(1) * 100 << "%." << std::endl;

    return 0;
}
