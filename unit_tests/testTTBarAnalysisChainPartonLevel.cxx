#include "TtbarDiffCrossSection/TTbarEventReconstructionPartonLevel.h"
#include "TtbarDiffCrossSection/TTbarEventSelectionToolPartonLevel.h"
#include "NtuplesAnalysisToolsCore/EventReweightingToolCommon.h"
#include "NtuplesAnalysisToolsCore/EventWeightSFToolLoaderCommon.h"
#include "TtbarDiffCrossSection/TTBarEventReaderTruth.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/SampleMetadata.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"
#include "TCanvas.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

#include "TopEvent/EventTools.h"

int main(int /*nArg*/, char ** /* arg*/)
{

    // MC Signal, DSID: 601237
    const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";
    auto f = std::unique_ptr<TFile>{TFile::Open(file.c_str())};

    auto chain = std::make_unique<TChain>();

    chain->AddFile(file.c_str(), TChain::kBigNumber, "truth");

    const ULong64_t nentries = std::min<ULong64_t>(10000, chain->GetEntries());
    std::cout << chain->GetEntries() << std::endl;

    const nlohmann::json configEventReader = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReaderTruthConfig.json"));

    const nlohmann::json configRecoTool = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarObjectSelectionRecoConfig.json"));

    const nlohmann::json configEventReweighting = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReweightingToolTruthLevelConfig.json"));

    const nlohmann::json configEventSelection = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventSelectionToolTruthConfig.json"));

    const nlohmann::json configLumi = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/luminositySettingsConfig.json"));

    auto reader = std::make_unique<TTBarEventReaderTruth>("TruthTreeReader");

    top::check(reader->readJSONConfig(configEventReader), "Failed to read TruthTreeReader json config!");
    reader->setChain(chain.get());
    reader->readMetadata(*f);
    top::check(reader->initialize(), "Failed to initialize TruthTreeReader!");

    const SampleMetadata &metadata = reader->getMetadata();
    const double lumi = configFunctions::getLumi(configLumi, metadata.getMCProduction());
    std::cout << "Lumi: " << lumi << std::endl;

    auto reco_tool = std::make_unique<TTbarEventReconstructionPartonLevel>("EventReconstructionParton");

    top::check(
        reco_tool->setProperty("debugLevel", 10),
        "Failed to setup reco level event reconstruction tool");

    top::check(
        reco_tool->readJSONConfig(configRecoTool),
        "Failed to read reconstruction tool config");

    auto event_selection_tool = std::make_unique<TTbarEventSelectionToolPartonLevel>("EventSelectionToolPartonLevel");
    top::check(
        event_selection_tool->setProperty("debugLevel", 10),
        "Failed to setup reco level event reconstruction tool");

    top::check(
        event_selection_tool->readJSONConfig(configEventSelection),
        "Failed to read event selection tool config");

    TH1D *h_cutflow = event_selection_tool->initCutflowHistogram("cutflow");
    TH1D *h_cutflow_noweights = event_selection_tool->initCutflowHistogram("cutflow_noweights");
    top::check(event_selection_tool->setProperty("cutflow", h_cutflow), "Failed to setup reco level event selection tool");
    top::check(event_selection_tool->setProperty("cutflowNoWeights", h_cutflow_noweights), "Failed to setup reco level event selection tool");

    top::check(
        event_selection_tool->initialize(),
        "Failed to setup reco level event reconstruction tool");

    auto event_reweighting_tool = std::make_unique<EventReweightingToolCommon>("EventReweightingToolParton");

    top::check(
        event_reweighting_tool->setProperty("lumi", lumi),
        "Failed to setup particle level event reweighting tool");

    top::check(
        event_reweighting_tool->setProperty("xsecWithKfactor", 839.),
        "Failed to setup particle level event reweighting tool");

    top::check(
        event_reweighting_tool->setProperty("sumWeights", 1.e+07),
        "Failed to setup particle level event reweighting tool");

    event_reweighting_tool->setEventSFToolLoader(std::make_unique<EventWeightSFToolLoaderCommon>("testLoader"));

    top::check(
        event_reweighting_tool->readJSONConfig(configEventReweighting),
        "Failed to read event reweighting tool config");

    const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "NOSYS"}};
    // const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "GEN_MUR05_MUF1_PDF260000"}};
    top::check(reader->initChain(chain_settings), "Failed to initialize chain in TruthTreeReader!");

    // Simple event loop
    for (ULong64_t ientry = 0; ientry < nentries; ientry++)
    {
        auto event = std::unique_ptr<ReconstructedEvent>{reader->readEntry(ientry)};
        std::cout << "ientry: " << ientry << std::endl;
        const auto &top = event->getConstRef<Object_t>("top");
        std::cout << "Top quark pt, eta, phi, m: " << std::endl;
        std::cout << top.Pt() << " " << top.Eta() << " " << top.Phi() << " " << top.M() << std::endl;
        const auto &antitop = event->getConstRef<Object_t>("antitop");
        std::cout << "Antitop quark pt, eta, phi, m: " << std::endl;
        std::cout << antitop.Pt() << " " << antitop.Eta() << " " << antitop.Phi() << " " << antitop.M() << std::endl;

        reco_tool->execute(*event);
        const auto &top1 = event->getConstRef<Object_t>("top1");
        std::cout << "Leading top quark pt, eta, phi, m: " << std::endl;
        std::cout << top1.Pt() << " " << top1.Eta() << " " << top1.Phi() << " " << top1.M() << std::endl;

        std::cout << "Weight_mc: " << event->getAttribute<float>("weight_mc") << std::endl;

        top::check(
            event_reweighting_tool->execute(*event),
            "Failed to execute parton level event reweighting tool");
        const double weight = event_reweighting_tool->getWeight();

        std::cout << "Generator weight: " << weight << std::endl;

        h_cutflow_noweights->Fill(1);
        h_cutflow->Fill(1, weight);

        std::cout << "Selected event: " << event_selection_tool->execute(*event, false) << std::endl
                  << std::endl;

        if (event_selection_tool->execute(*event, true))
        {
            std::cout << "EVENT SELECTED!" << std::endl
                      << std::endl;
        }
    }

    TCanvas c("c");
    h_cutflow_noweights->Draw("text");
    c.SaveAs("cutflow_noweights_parton_level.png");
    h_cutflow->Draw("text");
    c.SaveAs("cutflow_parton_level.png");

    std::cout << "Percentage of selected events: " << h_cutflow_noweights->GetBinContent(h_cutflow_noweights->GetNbinsX()) / h_cutflow_noweights->GetBinContent(1) * 100 << "%." << std::endl;

    return 0;
}
