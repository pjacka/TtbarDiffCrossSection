#include "TtbarDiffCrossSection/TTBarEventReaderReco.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

#include "TopEvent/EventTools.h"

int main(int /*nArg*/, char** /* arg*/) {
  
  // MC Signal, DSID: 601237
  const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";
  // Data22
  // const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/7e/82/user.pjacka.42327543._000047.output.root";
  
  auto f = std::unique_ptr<TFile>{TFile::Open(file.c_str())};

  auto chain = std::make_unique<TChain>();
  
  chain->AddFile(file.c_str(), TChain::kBigNumber, "reco");
  
  const ULong64_t nentries = std::min<ULong64_t>(10000, chain->GetEntries());
  std::cout << chain->GetEntries() << std::endl;
  
  const nlohmann::json configJSON = nlohmann::json::parse(
    std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReaderRecoConfig.json")
  );
  
  auto reader = std::make_unique<TTBarEventReaderReco>("RecoTreeReader");
  reader->readMetadata(*f);
  
  top::check(reader->readJSONConfig(configJSON), "Failed to read RecoTreeReader json config!");
  reader->setChain(chain.get());
  
  top::check(reader->initialize(), "Failed to initialize RecoTreeReader!");
  
  
  
  // const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "NOSYS"}};
  const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "JET_JER_dijet_R10_jesEffNP4_PseudoData__1down"}};
  // const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "GEN_MUR05_MUF1_PDF260000"}};
  top::check(reader->initChain(chain_settings), "Failed to initialize chain in RecoTreeReader!");
  
  // Simple event loop
  for(ULong64_t ientry = 0; ientry < nentries; ientry++) {
    auto event = std::unique_ptr<ReconstructedEvent>{reader->readEntry(ientry)};
    
    std::cout << "ientry: " << ientry << std::endl;
    event->print();
    reader->print();
    std::cout << "Weight_mc: " << event->getAttribute<float>("weight_mc") << std::endl;
  }
  
  
  return 0;
}
