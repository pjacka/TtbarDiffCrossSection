#include "TtbarDiffCrossSection/RecoTreeTopCPToolkit.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>


int main(int /*nArg*/, char** /* arg*/) {
  
  // MC Signal, DSID: 601237
  const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";
  bool isData = false;
  // Data22
  // const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/7e/82/user.pjacka.42327543._000047.output.root";
  // const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/4a/10/user.pjacka.42412340._000001.output.root";
  // bool isData = true;
  
  auto chain = std::make_unique<TChain>();
  chain->AddFile(file.c_str(), TChain::kBigNumber, "reco");
  const ULong64_t nentries = chain->GetEntries();
  std::cout << "Nentries: " << nentries << std::endl;
  
  // const std::string sys_name = "NOSYS";
  const std::string sys_name = "EG_RESOLUTION_ALL__1down";
    
  const nlohmann::json configJSON = nlohmann::json::parse(
    std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testRecoTreeConfig.json")
  );
  
  auto minitree = std::make_unique<RecoTreeTopCPToolkit>(configJSON, isData);
  
  std::cout << "Initialize chain" << std::endl;
  minitree->initChain(chain.get(), sys_name);
  std::cout << "Initialize chain done" << std::endl;
  for(ULong64_t ientry = 0; ientry < nentries; ientry++) {
    minitree->getEntry(ientry);
    
    if(ientry % 100 == 0)
      std::cout << "ientry: " << ientry << "/" << nentries << std::endl;
    minitree->print();
  }
  
  return 0;
}
