#include "TtbarDiffCrossSection/TTbarEventClassificationToolBoostedAllhad.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionTopPolarizationRecoLevel.h"
#include "NtuplesAnalysisToolsCore/EventReweightingToolCommon.h"
#include "NtuplesAnalysisToolsCore/EventWeightSFToolLoaderCommon.h"

#include "TtbarDiffCrossSection/TTbarEventSelectionToolBoostedAllhad.h"
#include "TtbarDiffCrossSection/ObjectSelectionTool.h"
#include "TtbarDiffCrossSection/TTBarEventReaderReco.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "NtuplesAnalysisToolsCore/SampleMetadata.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"
#include "TCanvas.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

#include "TopEvent/EventTools.h"

int main(int /*nArg*/, char ** /* arg*/)
{

    // MC Signal, DSID: 601237
    const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";
    // Data22
    // const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/7e/82/user.pjacka.42327543._000047.output.root";

    auto chain = std::make_unique<TChain>();

    chain->AddFile(file.c_str(), TChain::kBigNumber, "reco");

    const ULong64_t nentries = std::min<ULong64_t>(10000, chain->GetEntries());
    std::cout << chain->GetEntries() << std::endl;

    auto f = std::unique_ptr<TFile>{TFile::Open(file.c_str())};

    const nlohmann::json configEventReader = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReaderRecoConfig.json"));

    const nlohmann::json configObjectSelection = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarObjectSelectionRecoConfig.json"));

    const nlohmann::json configEventSelection = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventSelectionToolRecoConfig.json"));

    const nlohmann::json configEventReweighting = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReweightingToolRecoConfig.json"));

    const nlohmann::json configEventClassification = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventClassificationRecoConfig.json"));

    const nlohmann::json configLumi = nlohmann::json::parse(
        std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/luminositySettingsConfig.json"));

    auto reader = std::make_unique<TTBarEventReaderReco>("RecoTreeReader");

    top::check(reader->readJSONConfig(configEventReader), "Failed to read RecoTreeReader json config!");
    reader->setChain(chain.get());
    reader->readMetadata(*f);
    top::check(reader->initialize(), "Failed to initialize RecoTreeReader!");

    const SampleMetadata &metadata = reader->getMetadata();
    bool isData = (metadata.getDataType() == "data");
    double lumi = 1;
    double lumiUnc = 0.02;
    if (!isData)
    {
        lumi = configFunctions::getLumi(configLumi, metadata.getMCProduction());
        lumiUnc = configFunctions::getLumiUnc(configLumi, metadata.getMCProduction());
    }
    std::cout << "Lumi: " << lumi << " Rel. uncertainty: " << lumiUnc << " %" << std::endl;

    auto object_selection_tool = std::make_unique<TTbarEventReconstructionTopPolarizationRecoLevel>("ObjectSelectionToolReco");
    top::check(
        object_selection_tool->setProperty("isReco", true),
        "Failed to setup reco level reconstruction tool");

    top::check(
        object_selection_tool->setProperty("debugLevel", 10),
        "Failed to setup reco level event reconstruction tool");

    top::check(
        object_selection_tool->readJSONConfig(configObjectSelection),
        "Failed to read reconstruction tool config");

    auto event_selection_tool = std::make_unique<TTbarEventSelectionToolBoostedAllhad>("EventSelectionToolReco");

    top::check(
        event_selection_tool->setProperty("debugLevel", 10),
        "Failed to setup reco level event reconstruction tool");

    top::check(
        event_selection_tool->readJSONConfig(configEventSelection),
        "Failed to read event selection tool config");

    auto event_reweighting_tool = std::make_unique<EventReweightingToolCommon>("EventReweightingToolReco");

    top::check(
        event_reweighting_tool->setProperty("lumi", lumi),
        "Failed to setup reco level event reweighting tool");
    top::check(
        event_reweighting_tool->setProperty("xsecWithKfactor", 839.),
        "Failed to setup particle level event reweighting tool");

    top::check(
        event_reweighting_tool->setProperty("sumWeights", 1.e+07),
        "Failed to setup particle level event reweighting tool");

    event_reweighting_tool->setEventSFToolLoader(std::make_unique<EventWeightSFToolLoaderCommon>("testLoader"));

    top::check(
        event_reweighting_tool->readJSONConfig(configEventReweighting),
        "Failed to read event reweighting tool config");

    TH1D *h_cutflow = event_selection_tool->initCutflowHistogram("cutflow");
    TH1D *h_cutflow_noweights = event_selection_tool->initCutflowHistogram("cutflow_noweights");
    top::check(event_selection_tool->setProperty("cutflow", h_cutflow), "Failed to setup reco level event selection tool");
    top::check(event_selection_tool->setProperty("cutflowNoWeights", h_cutflow_noweights), "Failed to setup reco level event selection tool");

    top::check(
        event_selection_tool->initialize(),
        "Failed to setup reco level event reconstruction tool");

    auto event_classification_tool = std::make_unique<TTbarEventClassificationToolBoostedAllhad>("TTbarEventClassificationToolBoostedAllhad");
    top::check(
        event_classification_tool->readJSONConfig(configEventClassification),
        "Failed to read event classification tool config");

    //   const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "NOSYS"}};
    //   const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "JET_JER_dijet_R10_jesEffNP4_PseudoData__1down"}};
    const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "GEN_MUR05_MUF1_PDF260000"}};

    top::check(reader->initChain(chain_settings), "Failed to initialize chain in RecoTreeReader!");

    double sum_weights = 0.;
    // Simple event loop
    for (ULong64_t ientry = 0; ientry < nentries; ientry++)
    {
        auto event = std::unique_ptr<ReconstructedEvent>{reader->readEntry(ientry)};
        std::cout << "ientry: " << ientry << std::endl;
        reader->print();
        event->print();
        object_selection_tool->execute(*event);
        event->print();
        std::cout << "Weight_mc: " << event->getAttribute<float>("weight_mc") << std::endl;

        sum_weights += event->getAttribute<float>("weight_mc");

        top::check(
            event_reweighting_tool->execute(*event),
            "Failed to execute reco level event reweighting tool");

        const double weight = event_reweighting_tool->getWeight();

        h_cutflow_noweights->Fill(1);
        h_cutflow->Fill(1, weight);

        // top::check(event_selection_tool->setProperty("weight", event->getAttribute<float>("weight_mc")),"");
        bool selection_passed = event_selection_tool->execute(*event, true);

        std::cout << "Selected event: " << selection_passed << std::endl
                  << std::endl;

        if (!selection_passed)
            continue;
        event_classification_tool->execute(*event);

        const std::vector<int> &region_indexes = event_classification_tool->getSelections();
        const std::vector<std::string> &region_names = event_classification_tool->getSelectionNames();
        const unsigned int region_index = std::distance(std::begin(region_indexes), std::find_if(std::begin(region_indexes), std::end(region_indexes), [](auto x)
                                                                                                 { return x != 0; }));
        std::cout << "Selected region: " << region_index << std::endl
                  << "    Region name: " << region_names[region_index] << std::endl
                  << "    Classified as signal region: "
                  << (region_index == event_classification_tool->getSignalRegionIndex())
                  << std::endl;
    }
    TCanvas c("c");
    h_cutflow_noweights->Draw("text");
    c.SaveAs("cutflow_noweights_reco_level.png");
    h_cutflow->Draw("text");
    c.SaveAs("cutflow_reco_level.png");

    std::cout << "Percentage of selected events: " << h_cutflow_noweights->GetBinContent(h_cutflow_noweights->GetNbinsX()) / h_cutflow_noweights->GetBinContent(1) * 100 << "%." << std::endl;
    std::cout << "Sum weights in reco tree: " << sum_weights << std::endl;

    return 0;
}
