#include "TtbarDiffCrossSection/ParticleLevelTreeTopCPToolkit.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>


int main(int /*nArg*/, char** /* arg*/) {
  
  // MC Signal, DSID: 601237
  const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";

  auto chain = std::make_unique<TChain>();
  
  chain->AddFile(file.c_str(), TChain::kBigNumber, "particleLevel");
  
  const ULong64_t nentries = chain->GetEntries();
  std::cout << nentries << std::endl;
  
  const nlohmann::json configJSON = nlohmann::json::parse(
    std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testParticleLevelTreeConfig.json")
  );
  
  auto minitree = std::make_unique<ParticleLevelTreeTopCPToolkit>(configJSON);
  
  minitree->initChain(chain.get(), "GEN_MUR1_MUF1_PDF260001");
  
  for(ULong64_t ientry = 0; ientry < nentries; ientry++) {
    minitree->getEntry(ientry);
    
    std::cout << "ientry: " << ientry << std::endl;
    minitree->print();
  }
  
  
  return 0;
}
