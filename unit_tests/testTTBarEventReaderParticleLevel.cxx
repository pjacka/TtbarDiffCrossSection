#include "TtbarDiffCrossSection/TTBarEventReaderParticleLevel.h"
#include "NtuplesAnalysisToolsCore/ReconstructedEvent.h"
#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TSystem.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>

#include "TopEvent/EventTools.h"

int main(int /*nArg*/, char** /* arg*/) {
  
  // MC Signal, DSID: 601237
  const std::string file = "root://se1.farm.particle.cz:1094//atlas/atlaslocalgroupdisk/rucio/user/pjacka/96/72/user.pjacka.43228944._000001.output.root";

  auto chain = std::make_unique<TChain>();
  
  chain->AddFile(file.c_str(), TChain::kBigNumber, "particleLevel");
  
  const ULong64_t nentries = std::min<ULong64_t>(10000, chain->GetEntries());
  std::cout << chain->GetEntries() << std::endl;
  
  
  
  const nlohmann::json configJSON = nlohmann::json::parse(
    std::ifstream((std::string)gSystem->Getenv("TTBAR_PATH") + "/TtbarDiffCrossSection/unit_tests/testTTBarEventReaderParticleLevelConfig.json")
  );
  
  auto reader = std::make_unique<TTBarEventReaderParticleLevel>("ParticleLevelTreeReader");
  
  top::check(reader->readJSONConfig(configJSON), "Failed to read ParticleLevelTreeReader json config!");
  reader->setChain(chain.get());
  
  top::check(reader->initialize(), "Failed to initialize ParticleLevelTreeReader!");
  
  const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "NOSYS"}};
  // const std::unordered_map<std::string, std::string> chain_settings = {{"sys_name", "GEN_MUR05_MUF1_PDF260000"}};
  
  top::check(reader->initChain(chain_settings), "Failed to initialize chain in ParticleLevelTreeReader!");
  
  // Simple event loop
  for (ULong64_t ientry = 0; ientry < nentries; ientry++) {
    auto event = std::unique_ptr<ReconstructedEvent>{reader->readEntry(ientry)};
    event->setName("Particle level event");
    
    std::cout << "ientry: " << ientry << std::endl;
    event->print();
    reader->print();
  }
  
  
  return 0;
}
