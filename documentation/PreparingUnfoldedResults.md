Table of Contents
=========================

**[Creating unfolding inputs](#creating-unfolding-inputs)**

**[Preparing histograms for 2D unfolding](#preparing-histograms-for-2d-unfolding)**

**[Propagating uncertainties through unfolding](#propagating-uncertainties-through-unfolding)**

**[Signal modeling uncertainties](#signal-modeling-uncertainties)**

**[Running pseudo-experiments](#running-pseudo-experiments)**

**[Making plots](#making-plots)**

## **Creating unfolding inputs**
Framework to propagate uncertainties through unfolding requires input histograms in a special format. Histograms for each variable and level (Particle or Truth) should be saved in a separate root file. 
The histograms are required to be organized into directories corresponding to systematic branches and they should have the same name.

Root files with histograms are created using executables: [PrepareSystematicHistos](util/PrepareSystematicHistos.cxx), [PrepareSystematicHistosND](util/PrepareSystematicHistosND.cxx)
These executables interacts with two classes:

**1. FileStore ([heather](TtbarDiffCrossSection/FileStore.h),[source](Root/FileStore.cxx))**
This class is used to open root files created when running over ntuples and read histograms from them. Root files have special structure and naming conventions are used to name individual histograms. 
FileStore follows these convention when reading histograms. This helps to navigate easily through the rootfiles.

**2. HistStore ([heather](TtbarDiffCrossSection/HistStore.h),[source](Root/HistStore.cxx))**
HistStore uses FileStore to load various histograms. These histograms are then classified according to the type of systematic uncertainties and saved into output file in required format. 
HistStore also supports rebinning histograms. Histograms in an input rootfile have typically fine binning and they are rebinned when unfolding inputs are created. 

### **Preparing histograms for ND unfolding**

Preparing histograms for ND unfolding is a little-bit more complicated since histograms are required to be transformed into contacenated 1D histograms. 
This also requires to transform ND migration matrix into 2D concatenated matrix.
For this purpose, class HistogramNDto1DConverter is created ([heather](https://gitlab.cern.ch/pjacka/TTbarHelperFunctions/TTbarHelperFunctions/HistogramNDto1DConverter.h),[source](https://gitlab.cern.ch/pjacka/TTbarHelperFunctions/Root/HistogramNDto1DConverter.cxx)).
This class allows to perform rebinning of ND histograms in such a way that binning of internal variable can be different in different bins of external variable. The new binning is configured from config file
([1D binning](data/unfolding_histos_optimizedBinning.env),[ND binning](data/optBinningND)).
The binning info is saved into the nominal directory in the output root file. 

## **Propagating uncertainties through unfolding**
Uncertainties are propagated using tools from [TTbarCovarianceCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator) package. It includes calculation of the full covariance.

### **Signal modeling uncertainties (Old)**

Signal modeling uncertainties are calculated using [runGeneratorSystematics](util/runGeneratorSystematics.cxx) executable.
It interacts with [GeneratorSystematicsBase](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/GeneratorSystematicsBase.h) class. 
This class loads distrubutions from alternative samples and unfold them to the truth level where they are compared with a truth distribution. 
It supports also alternative algorithm to propagate uncertainties: 
Nominal distributions are unfolded using shifted corrections and they are compared with a nominal truth distributions.

Input histograms are loaded by [Signal_histos](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/Signal_histos.h).

Covariances related with signal modeling are then saved into root files from where they can be loaded once the final covariance is calculated.

### **Running pseudo-experiments**
All types of uncertainties including signal modeling are propagated through unfolding using pseudo-experiments. 
Reco-level distributions and unfolding corrections are shifted coherently using generated random numbers.
The covariance from pseudo-experiments is combined with signal modeling covariance after all pseudo-experiments are done.

Pseudo-experiments are processed with [ProducePseudoexperiments](util/ProducePseudoexperiments.cxx) executable. 
1. Unfolding inputs are loaded using [SystematicHistos](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/SystematicHistos.h) class. It loads histograms corresponding to all systematic histograms in the initialization phase. Shifts corresponding to 1-sigma are stored in std::vector format.
2. Random pseudo-experiments at reco level are calculated using [PseudoexperimentsCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/PseudoexperimentsCalculator.h). Individual pseudo-experiments are stored in the class [Pseudoexperiment](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/Pseudoexperiment.h).
3. Pseudo-experiments are unfolded to truth level using a class [CovarianceCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/CovarianceCalculator.h) which interacts with RooUnfold package. Covariance is made from sum over all pseudo-experiments using the standard procedure.
4. Random post-unfolding shifts are calculated for signal modeling variations and combined with unfolded pseudo-experiments. 

## **Making plots**
### **Summary plots with sources of uncertainties**
Plots are made with [CalculateAsymmetricErrors](util/CalculateAsymmetricErrors.cxx) which interacts with
[AsymmetricErrorsCalculator](https://gitlab.cern.ch/pjacka/TTbarCovarianceCalculator/blob/Rel21/TTbarCovarianceCalculator/AsymmetricErrorsCalculator.h) class. 
This class also provides latex tables with relative uncertainties.


### **Plots with final results**
[drawTheoryVsDataComparisonPlots](util/drawTheoryVsDataComparisonPlots.cxx) command is used to make final plots and chi-squared tests with SM predictions. 



