Table of Contents
=========================

**[Main class](#main-class)**

**[Input files](#input-files)**

**[Reading branches from trees](#reading-branches-from-trees)**

**[Event reconstruction](#event-reconstruction)**

**[Manipulating with histograms](#manipulating-with-histograms)**


Main class
=======================
Trees stored in ntuples are processed with RunOverMinitree class ([heather](TtbarDiffCrossSection/RunOverMinitree.h),[source](Root/RunOverMinitree.cxx)). 
We simply loop over the trees and fill histograms. It loops over parton level, particle level and reco level trees.
The output is a single .root file containing histograms. Histograms are filled for each systematic variation separately. 
They are stored in folders named after systematic branches. Otherwise, they have the same names.

Input files
=======================
Input files are ntuples produced using the AnalysisTop-21.2 extended with the [TTBarDiffXsTools package](https://gitlab.cern.ch:8443/atlasphys-top/xs/TTbarDiffXsTools/tree/Rel21). 
Ntuples contain nominal, systematic, particle level and parton level trees.
Input ntuples are passed to the class using a text file containing a list of files to be processed.


Reading branches from trees
=======================
Reading branches is handled using [Minitree](TtbarDiffCrossSection/Minitree.h) and [Minitree_truth](TtbarDiffCrossSection/Minitree_truth.h) classes. 
The Minitree class is used for the detector level and the particle level trees while the Minitree_truth class handles only the truth tree (parton level). 
Both classes are similar to those generated using TTree::MakeClass() method. However, they contain additional functions to switch easily between different jet and taggers definitions. 

Event reconstruction
======================
Events are reconstructed using `RunOverMinitree::GetObjectsFromMinitree` function.
Reconstructed events are stored in [TTbarAllHadronic](TtbarDiffCrossSection/TTbarAllHadronic.h) class. Each level has its own instance of the class.


Manipulating with histograms
=======================
All interactions with histograms is handled by [HistogramManagerAllHadronic](TtbarDiffCrossSection/HistogramManagerAllHadronic.h) class. It handles creating, 
filling, and deleting histograms. It supports TH1D, TH2D and THnSparseD histograms. 
The class loads histograms definitions using from config files including their binning (See [ConfigurationFiles.md](documentation/ConfigurationFiles.md#configuration-of-histograms)). 
However, the filling of the histograms has to be coded directly in the class.

