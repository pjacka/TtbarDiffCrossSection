Table of Contents
=========================

**[Main config file](#main-config-file)**

**[Configuration of histograms](#configuration-of-histograms)**

**[Optimized binning](#optimized-binning)**

**[Dataset names](#dataset-names)**


Main config file
=======================
Configuration of the framework is handled using NtuplesAnalysisToolsConfig class from NtuplesAnalysisTools package (see detailed description in 
[heather](https://gitlab.cern.ch/pjacka/NtuplesAnalysisTools/-/blob/Rel25/NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h) and [source](https://gitlab.cern.ch/pjacka/NtuplesAnalysisTools/-/blob/Rel25/NtuplesAnalysisToolsConfiguration/Root/NtuplesAnalysisToolsConfig.cxx)
).

The class loads parameters from the main config file. [data/config.json](data/config.json) is used as default main config file and it kept compatible with the recent version of the framework.

Configuration of histograms
=======================

Histograms are fully configurable through config files except of special cases. It is possible to add or remove a histogram without any modification in the code. 
However, one still needs to check if variable is on the list of calculated variables. Reco level variables are defined in [RecoLevelVariables](Root/RecoLevelVariables.cxx). 
It is required to match names in **RecoLevelVariables::initialize()** function. This class also handles calculation of variables from reconstructed event stored in [ReconstructedEvent](https://gitlab.cern.ch/pjacka/NtuplesAnalysisTools/-/blob/Rel25/NtuplesAnalysisToolsCore/Root/ReconstructedEvent.cxx).
Variables for unfolding are defined and calculated in [MeasuredVariables](Root/MeasuredVariables.cxx). **RecoLevelVariables** and **MeasuredVariables** classes are derived from the same base class and they have similar structure. 
However, MeasuredVariables class is much simpler since the set of measured variables is much smaller.

**1. 1D histograms for reco level studies only are defined in the config file [data/histos.json](data/histos.json)**

Example:
```
"fatJet1_pt": {
        "Xaxis": {
            "Binning": {
                "Edges": [500,525,560,600,640,680,730,800,1000,2000]
            },
            "Title": "p_{T}^{t,1} [GeV]",
            "Variable": "LJet1_pt"
        },
        "Ytitle": "N_{events}/GeV"
    }

```
Histogram will be saved with name="fatJet1_pt". "Variable" option have to match the name registered in the RecoLevelVariables class. 
There are two possible binning definitions: equidistant and unequal binning. The equidistant binning is setup using three numbers: One positive integer and two floating point numbers.
The unequal binning is setup using a set of bin edges. 
 
**2. 2D histograms for reco level studies only are defined in the config file [data/histos2D.json](data/histos2D.json)**

Example:
```
"ttbar_Ht_vs_filter_Ht": {
        "Xaxis": {
            "Binning": {
                "Edges": [0.5,0.8,0.9,1.000,1.100,1.200,1.300,1.500,2.000,2.5,3.0,5.0]
            },
            "Title": "ttbar Ht [TeV]",
            "Variable": "ttbar_Ht"
        },
        "Yaxis": {
            "Binning": {
                "Edges": [0.5,0.8,0.9,1.000,1.100,1.200,1.300,1.500,2.000,2.5,3.0,5.0]
            },
            "Title": "filter Ht [TeV]",
            "Variable": "filter_HT"
        }
    

```
Configuration is the extension of the 1D case with one additional axis description. "Variable" option have to match the name registered in the RecoLevelVariables class.

**3. 1D histograms used for 1D unfolding are defined in the config file [data/unfolding_histos.json](data/unfolding_histos.json)**

Example:
```
"t1_pt": {
        "Binning": {
            "Reco": {
                "Edges": [0.5,0.525,0.53,0.54,0.55,0.56,0.565,0.57,0.58,0.59,0.595,0.6,0.605,0.62,0.64,0.645,0.66,0.68,0.7,0.73,0.735,0.74,0.76,0.8,0.85,0.9,1,1.1,1.2,1.5,2.0]
            },
            "Particle": {
                "Edges": [0.5,0.525,0.53,0.54,0.55,0.56,0.565,0.57,0.58,0.59,0.595,0.6,0.605,0.62,0.64,0.645,0.66,0.68,0.7,0.73,0.735,0.74,0.76,0.8,0.85,0.9,1,1.1,1.2,1.5,2.0]
            },
            "Parton": {
                "Edges": [0.5,0.525,0.53,0.54,0.55,0.56,0.565,0.57,0.58,0.59,0.595,0.6,0.605,0.62,0.64,0.645,0.66,0.68,0.7,0.73,0.735,0.74,0.76,0.8,0.85,0.9,1,1.1,1.2,1.5,2.0]
            },
            "Reso": {
                "Nbins": 40,
                "Min": -0.1,
                "Max": 0.1
            }
        },
        "Xtitle": "p_{T}^{t,1} [TeV]",
        "Ytitle": "d #sigma_{t#bar{t}} / d p_{T}^{t,1} [pb TeV^{ -1 }]",
        "Variable": "t1_pt"
    }

```
Binning for spectra at particle- and parton-level are defined separately. However, in the current implementation,  
reco distributions are shared between particle- and parton-level measurements to save CPU time.

"reso" option is used to setup resolution plots. "t1_pt" should match variable name registered in the MeasuredVariables class.


**4. ND histograms used for ND unfolding are defined in the config file [data/unfolding_histosND.json](data/unfolding_histosND.json)**

Example:
```
"t1_pt_vs_t2_pt": {
        "axis0": {
            "Binning": {
                "Reco": {
                    "Edges": [0.5,0.525,0.55,0.575,0.6,0.625,0.65,0.675,0.7,0.75,0.8,0.85,0.9,1,1.1,1.2,2]
                },
                "Particle": {
                    "Edges": [0.5,0.525,0.55,0.575,0.6,0.625,0.65,0.675,0.7,0.75,0.8,0.85,0.9,1,1.1,1.2,2]
                },
                "Parton": {
                    "Edges": [0.5,0.525,0.55,0.575,0.6,0.625,0.65,0.675,0.7,0.75,0.8,0.85,0.9,1,1.1,1.2,2]
                }
            },
            "Title": "p_{T}^{t,1} [TeV]",
            "Variable": "t1_pt"
        },
        "axis1": {
            "Binning": {
                "Reco": {
                    "Edges": [0.35,0.375,0.4,0.425,0.45,0.475,0.5,0.525,0.55,0.575,0.6,0.65,0.7,0.75,0.8,0.9,1,1.2,2]
                },
                "Particle": {
                    "Edges": [0.35,0.375,0.4,0.425,0.45,0.475,0.5,0.525,0.55,0.575,0.6,0.65,0.7,0.75,0.8,0.9,1,1.2,2]
                },
                "Parton": {
                    "Edges": [0.35,0.375,0.4,0.425,0.45,0.475,0.5,0.525,0.55,0.575,0.6,0.65,0.7,0.75,0.8,0.9,1,1.2,2]
                }
            },
            "Title": "p_{T}^{t,2} [TeV]",
            "Variable": "t2_pt"
        }
    }

```
Similar configuration as in 1D case. One needs to specify binnings, titles and variables for multiple axes separately.

One can define custom config files with different binning of the histograms and change path to config files in the [Main config file](#main-config-file).

Histograms are controlled in the code using maps. Maps use names passed from config files to access histograms. 


The special histograms which have different properties can be setup manually in [HistogramManagerAllHadronic::constructHistos()](Root/HistogramManagerAllHadronic.cxx).


Optimized binning
=================
We store optimized binning for unfolded spectra in separate config files: [for 1D spectra](data/unfolding_histos_optimizedBinning.env), [for ND spectra](TtbarDiffCrossSection/data/optBinningND/t1_pt_vs_ttbar_mass.env).
This binning is used to rebin histograms produced when ntuples are processed. Therefore, the optimized binning must be compatible with original binning (bin edges).


Dataset names
==============
When we process ntuples we create many root-files with histograms which are stored in directories with simplified dataset names. 
In order to avoid changes in the code everytime we change dataset names, we created a config file with the mapping of the names. 
This mapping is stored in [data/datasetnames.env](data/datasetnames.env). Dataset names configuration is loaded using TEnv class and 
it is used in several scripts which read root-files with histograms. 
