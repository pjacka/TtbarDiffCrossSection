#!/usr/bin/env python

import os,sys
import ROOT as root
import numpy as np
import tools


root.PyConfig.IgnoreCommandLineOptions = True
root.gROOT.SetBatch(True)
  
def getBinLabels():
  return ["Initial","GRID selection","Trigger", "ljet2 pt > 350","ljet1 pt > 500","|ljet1 m - 172.5| < 50",
	  "|ljet2 m - 172.5| < 50", "2 b-matches","0t0t","0t1t","1t0t","1t1t"]

def getBinLabelsLatex():
  return ["Initial","GRID selection", "Trigger", "ljet2 pt $>$ 350","ljet1 pt $>$ 500","80 $<$ ljet1 m $<$ 300",
	  "80 $<$ ljet2 m $<$ 300", "2 b-matches","0t0t","0t1t","1t0t","1t1t"]

def setBinLabels(h):
  labels=getBinLabels()
  for i,label in enumerate(labels):
    h.GetXaxis().SetBinLabel(i+1,label)

def printTexTables(f,h):
  cuts=getBinLabelsLatex()
  
  f.write("\\begin{tabular}{c|c} \\hline\n")
  for j,cut in enumerate(cuts):
    if(j==0):
      continue
    f.write(cut)
    f.write(" & ")
    f.write('{:8.0f}'.format(h.GetBinContent(j+1)))
    f.write(" $\\pm$ ")
    f.write('{:8.1f}'.format(h.GetBinError(j+1)))
    f.write(" \\\\ \\hline\n")
  f.write("\\end{tabular}\n")

def writeLevelVsLevelCutflowComparisonTable(f,vec):
  k = 1
  l = 1
  f.write("\\begin{tabular}{c|c|c|c|c|c} \\hline\n")
  f.write("Cut & Reco & Particle & Parton & Particle/Reco & Parton/Particle \\\\ \\hline\n")
  for i in range(1,9):
    cutLabel = vec[0].GetXaxis().GetBinLabel(i)
    line = cutLabel.replace('<','$<$')
    line = line.replace('>','$>$')
    line += ' & '
    
    
    if i==1 or i==4 or i==5:
      line += str(vec[0].GetBinContent(i)) + ' & ' + str(vec[1].GetBinContent(k)) + ' & ' + str(vec[2].GetBinContent(l)) + ' & ' + str(vec[1].GetBinContent(k)/vec[0].GetBinContent(i)) + ' & ' + str(vec[2].GetBinContent(l)/vec[1].GetBinContent(k))
      k+=1
      l+=1
    elif i==2 or i > 5:
      line += str(vec[0].GetBinContent(i)) + ' & ' + str(vec[1].GetBinContent(k)) + ' & & ' + str(vec[1].GetBinContent(k)/vec[0].GetBinContent(i)) + ' & '
      k+=1
    else:
      line += str(vec[0].GetBinContent(i)) + ' & & & & '
  
    line += ' \\\\ \\hline'
    f.write(line+'\n')
    print(line)
  f.write("\\end{tabular}\n")


def makeDType():
  return np.dtype({
      'names':['file', 'legend', 'sysBranch', 'color'],
      'formats':[object, object, object, np.int32] })

if __name__ == "__main__":


  ttbarPath=os.environ['TTBAR_PATH']
  basePath=os.environ['TtbarDiffCrossSection_output_path'] 
  configPath=ttbarPath+'/TtbarDiffCrossSection/data/'
  
  
  
  # including compiled C++ functions
  tools.cpplibs.addIncludes()
  # set atlas style
  # root.gInterpreter.ProcessLine('#include "TTbarHelperFunctions/plottingFunctions.h"')
  # root.gSystem.Load('libTTbarHelperFunctionsLib.so')
  root.functions.setStyle()
  # tools.plottingFunctions.setStyle()
  
  
  configFiles= root.TEnv(configPath + "/datasetnames.env")
  ttbarConfig = root.TTBarConfig(configPath + "/config.env")
  
  mcSamplesProduction=str(ttbarConfig.mc_samples_production())
  texDir="tex."+mcSamplesProduction
  pdfDir="pdf."+mcSamplesProduction
  outputDirPDF=basePath + '/' + pdfDir +'/' + 'cutflow'
  outputDirPNG=outputDirPDF.replace('/pdf.','/png.')
  
  root.gSystem.mkdir(outputDirPDF,True)
  root.gSystem.mkdir(outputDirPNG,True)
  
  print(mcSamplesProduction)
  
  
  
  
  lumi = root.functions.getLumi(ttbarConfig.getTEnv(),mcSamplesProduction)
  lumiString = str(root.NamesAndTitles.getLumiString(ttbarConfig))
  print(lumiString)
  
  
  
  if mcSamplesProduction=="MC16a":
    dataFile="data_filename"
    dataLegend="Data 15+16"
  if mcSamplesProduction=="MC16d":
    dataFile="data17_filename"
    dataLegend="Data 17"
  if mcSamplesProduction=="MC16e":
    dataFile="data18_filename"
    dataLegend="Data 18"
  if mcSamplesProduction=="All":
    dataFile="data_AllYears_filename"
    dataLegend="Data Run 2"
  
  dt = makeDType()
  print(dt)
  
  predictions = np.array([
    
    #      <filename>                        |    <legend>                        | <sysBranch>        | <color>
    #("ttbar_signal_nominal_filename"         ,"Powheg+Pyhia8 nominal"             , "nominal"          , 2),
    # ("signal_410471_filename"                ,"Powheg+Pyhia8 nominal 410471"      , "nominal"          , 3),
    # ("signal_410428_filename"                ,"Powheg+Pyhia8 nominal 410428"      , "nominal"          , 4),
    # ("signal_410429_filename"                ,"Powheg+Pyhia8 nominal 410429"      , "nominal"          , 5),
    # ("signal_410444_filename"                ,"Powheg+Pyhia8 nominal 410444"      , "nominal"          , 6),
    # ("ttbar_nonallhad_filename"              ,"NonAllHad"                         , "nominal"          , 7),
    # ("Wt_singletop_filename"                 ,"Wt single-top"                     , "nominal"          , 8),
    # ("tChannel_singletop_filename"           ,"Single top t-channel"              , "nominal"          , 9),
    # ("ttHWZ_filename"                        ,"ttHWZ"                             , "nominal"          , 10),
    # ("ttbar_signal_nominal_filename"           ,"Powheg+Pyhia8 ISR up"             , "ISR_muR05_muF05"             , 3),
    # ("ttbar_signal_nominal_filename"         ,"Powheg+Pyhia8 ISR down"           , "ISR_muR20_muF20"             , 4),
    # ("ttbar_signal_nominal_filename"           ,"Powheg+Pyhia8 NNPDF_var005"             , "NNPDF_var005"             , 3),
    # ("ttbar_signal_nominal_filename"         ,"Powheg+Pyhia8 NNPDF_var006"           , "NNPDF_var006"             , 4),
    # ("ttbar_signal_nominal_filename" ,"Powheg+Pyhia8 #alpha_{s} FSR down" , "FSR_muRfac_20", 3),
    # ("ttbar_signal_nominal_filename"   ,"Powheg+Pyhia8 #alpha_{s} FSR up"   , "FSR_muRfac_05"  , 4),
    #("ttbar_signal_PhH713_filename"              ,"Powheg+Herwig7.1.3"                    , "nominal"          , 3),
    # ("ttbar_signal_PhH704_filename"              ,"Powheg+Herwig7.0.4"                    , "nominal"          , 3),
    #("ttbar_signal_ME_filename"              ,"aMCatNLO+Pythia8"                  , "nominal"          , 4),
     (dataFile                                , dataLegend                         , "nominal"          , 1),
    ], dtype=dt) 
 
  print(predictions.itemsize)
  
  print(predictions['file'])
  
  textSize=root.gStyle.GetLegendTextSize()
  ymax=0.92
  ymin=ymax - len(predictions)*textSize*1.15
  
  leg = root.TLegend(0.515,ymin,0.92,ymax);
  leg.SetTextSize(textSize)
  tableDir1=basePath +"/" + texDir;
  tableDir=basePath +"/" + texDir + "/CutFlow/";
  root.gSystem.mkdir(tableDir,True); 
  
  
  
  files = []
  cutflowReco = []
  cutflowRecoNoWeights = []
  cutflowParticle = []
  cutflowParticleNoWeights = []
  cutflowParton = []
  cutflowPartonNoWeights = []
  
  
  for pred in predictions:
    if pred['file'].find('data') < 0:
      filename = basePath+"/" + mcSamplesProduction + "." + configFiles.GetValue(pred['file'],"")
    else:
      filename = basePath+"/" + configFiles.GetValue(pred['file'],"")
    #filename="/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r60_DNNTaggerTopQuarkInclusive80_LJets_TriggerTest//MC16a.410471.PhPy8EG/allhad.boosted.output.root"
    #filename="/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r60_DNNTaggerTopQuarkInclusive80_LJets_TriggerTest/data2017_AllYear.physics_Main/allhad.boosted.output.root"
    f = root.TFile(filename)
    files.append(f)
    print(filename)
  
    sysBranch=pred['sysBranch']
    cutflowReco.append(f.Get(sysBranch+"/h_cutflow"));
    cutflowRecoNoWeights.append(f.Get(sysBranch+"/h_cutflow_noweights"));
    cutflowParticle.append(f.Get(sysBranch+"/h_cutflow_particle"));
    cutflowParticleNoWeights.append(f.Get(sysBranch+"/h_cutflow_noweights_particle"));
    cutflowParton.append(f.Get(sysBranch+"/h_cutflow_parton"));
    cutflowPartonNoWeights.append(f.Get(sysBranch+"/h_cutflow_noweights_parton"));
    #setBinLabels(cutflowReco[-1])
    #setBinLabels(cutflowRecoNoWeights[-1])
    #setBinLabels(cutflowParticle[-1])
    #setBinLabels(cutflowParticleNoWeights[-1])
  
    leg.AddEntry(cutflowReco[-1],pred['legend'])
    
    figureName=cutflowReco[-1].GetName() + "_" + pred['file'].replace('_filename','')
    tools.plottingFunctions.plotCutflowHistogram(cutflowReco[-1],outputDirPDF+'/'+figureName)
    
    with open(tableDir+figureName+'.tex','w') as f:
      printTexTables(f,cutflowReco[-1])
    
    figureName=cutflowRecoNoWeights[-1].GetName() + "_" + pred['file'].replace('_filename','')
    tools.plottingFunctions.plotCutflowHistogram(cutflowRecoNoWeights[-1],outputDirPDF+'/'+figureName)
    
    figureName=cutflowParticle[-1].GetName() + "_" + pred['file'].replace('_filename','')
    tools.plottingFunctions.plotCutflowHistogram(cutflowParticle[-1],outputDirPDF+'/'+figureName)

    figureName=cutflowParticleNoWeights[-1].GetName() + "_" + pred['file'].replace('_filename','')
    tools.plottingFunctions.plotCutflowHistogram(cutflowParticleNoWeights[-1],outputDirPDF+'/'+figureName)
    
    
    figureName=cutflowParton[-1].GetName() + "_" + pred['file'].replace('_filename','')
    tools.plottingFunctions.plotCutflowHistogram(cutflowParton[-1],outputDirPDF+'/'+figureName)

    figureName=cutflowPartonNoWeights[-1].GetName() + "_" + pred['file'].replace('_filename','')
    tools.plottingFunctions.plotCutflowHistogram(cutflowPartonNoWeights[-1],outputDirPDF+'/'+figureName)
    
    
    cutflowReco[-1].SetLineWidth(2)
    cutflowReco[-1].SetLineColor(int(pred['color']))
    cutflowReco[-1].SetMarkerColor(int(pred['color']))
        
    cutflowParticle[-1].SetLineWidth(2)
    cutflowParticle[-1].SetLineColor(int(pred['color']))
    cutflowParticle[-1].SetMarkerColor(int(pred['color']))
        
    cutflowParton[-1].SetLineWidth(2)
    cutflowParton[-1].SetLineColor(int(pred['color']))
    cutflowParton[-1].SetMarkerColor(int(pred['color']))
        
    
    
  y2name="Other / Nominal";
  tools.plottingFunctions.plotHistograms(cutflowReco,leg,outputDirPDF,'cutflow_all',cutflowReco[0], y2name, 0.7, 1.35, lumiString,useLogScale=True)
  tools.plottingFunctions.plotHistograms(cutflowParticle,leg,outputDirPDF,'cutflow_all_particle',cutflowParticle[0], y2name, 0.7, 1.35, lumiString,useLogScale=True)
  tools.plottingFunctions.plotHistograms(cutflowParton,leg,outputDirPDF,'cutflow_all_parton',cutflowParticle[0], y2name, 0.7, 1.35, lumiString,useLogScale=True)


  with open(tableDir + 'cutflow_level_vs_level.tex','w') as f:
    vec = [cutflowReco[0],cutflowParticle[0],cutflowParton[0]]
    writeLevelVsLevelCutflowComparisonTable(f,vec)
    
    
