#!/usr/bin/env python

import os
from ROOT import gROOT,gStyle,gSystem,TEnv,TFile,TF1,TCanvas

gROOT.SetBatch(True)

# del gStyle
# gStyle = TStyle("ATLAS","Atlas style")

gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()
gStyle.SetErrorX()

#xAnalysis path -> $TTBAR_PATH
ttbarPath=os.environ['TTBAR_PATH']
basePath=os.environ['TtbarDiffCrossSection_output_path']

config = TEnv(ttbarPath + "/" + "TtbarDiffCrossSection/data/config.env" )

rootDir = "root."+config.GetValue("mc_samples_production","")
pngDir = "png."+config.GetValue("mc_samples_production","")
pdfDir = "pdf."+config.GetValue("mc_samples_production","")

outputPath = basePath + "/" + rootDir + "/" + "reweightingHistos"
outputPathPNG = basePath + "/" + pngDir + "/" + "reweightingHistos"
outputPathPDF = basePath + "/" + pdfDir + "/" + "reweightingHistos"

inputFilePath = outputPath + "/" + "topMassReweighting.root"
outputFilePath = outputPath + "/" + "reweightingHistos.root"

print("input file", inputFilePath)
print("output file", outputFilePath)







inputFile = TFile.Open(inputFilePath)
outputFile = TFile.Open(outputFilePath,"UPDATE")


## Loading histos from input file

# Names of histograms
keys = [ "171p5", "171p6", "171p7", "171p8", "171p9",
	 "172p0", "172p1", "172p2", "172p3", "172p4", 
	 "172p5", "172p6", "172p7", "172p8", "172p9",
	 "173p0", "173p1", "173p2", "173p3", "173p4", "173p5"]
# Loading histograms
histos = {}
for key in keys:
  histos[key] = inputFile.Get(key)


# Making ratios

ratios = {}
for it in histos.items():
  if it[0]=="172p5": 
    continue
  
  hRatio = it[1].Clone("ratio_top_mass_" + it[0])
  hRatio.Divide(histos["172p5"])
  ratios[it[0]] = hRatio
  
# Fuction to ratios
func = TF1("func","[0]*(pow(x*x - [1],2) + [2])/(pow(x*x - [3],2) + [4])", 100,250);
func.SetParameters(1.,172.5*172.5,172.5*172.5*1.9*1.9,172.5*172.5,172.5*172.5*1.9*1.9);

# func = TF1("func","[0]*(pow(x - [1],2) + [2]*[2]/4)/(pow(x - [3],2) + [4]*[4]/4)", 100,250);
# func.SetParameters(1.,172.5,1.9,172.5,1.9);



outputFile.cd()

gSystem.mkdir(outputPathPDF)
gSystem.mkdir(outputPathPNG)

for it in ratios.items():
  it[1].Fit(func,"WL")
  print (it[1],it[1].GetName())
  f = it[1].GetFunction("func")
  f.SetLineColor(2)
  
  c = TCanvas("c")
  it[1].Draw()
  c.SaveAs(outputPathPDF + "/" + it[1].GetName() + ".pdf")
  c.SaveAs(outputPathPNG + "/" + it[1].GetName() + ".png")
  
  outputFile.WriteObject(it[1],it[1].GetName())
  del c

outputFile.Close()
