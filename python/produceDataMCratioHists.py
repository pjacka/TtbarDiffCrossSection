#!/usr/bin/env python

"""Script to calculate Data-to-MC ratios used in stress test.  

Usage:  
  ./produceDataMCratioHists.py [options]
  
Options:
  -h --help                     Show help screen.
  --variable -v <string>        Variable name. [default: leadingTop_pt]
  --level -l <string>           Level. [default: PartonLevel]
  --inputDir -i <string>        Directory with input root files. [default: systematics_histos]
  --outputDir -d <string>       Directory to store ouput. [default: ./]
  --outputFile -o <string>      Ouput file name. [default: histRatio.root]
"""

import docopt
import os, sys
import ROOT as root

root.PyConfig.IgnoreCommandLineOptions = True
root.gROOT.SetBatch(True)

if __name__ == "__main__":
  
  arguments = docopt.docopt(__doc__)
  variable = arguments["--variable"]
  level = arguments["--level"]
  inputDir = arguments["--inputDir"]
  outputDir = arguments["--outputDir"]
  outputFile = arguments["--outputFile"]
  
  root.gROOT.SetStyle("ATLAS")
  root.gROOT.ForceStyle()
  root.gStyle.SetErrorX()
  
  
  outDirPDF=outputDir.replace("/root.","/pdf.",1)
  outDirPNG=outputDir.replace("/root.","/png.",1)

  print (outputDir)
  root.gSystem.mkdir(outputDir,True)
  root.gSystem.mkdir(outDirPDF,True)
  root.gSystem.mkdir(outDirPNG,True)
  
  mainDir = os.getenv('TtbarDiffCrossSection_output_path')
  pathHistos = mainDir + '/root.All/' + inputDir
  inFileName = pathHistos + '/' + variable + '_' + level + '.root'
  inFile = root.TFile(inFileName,"READ")

  histData   = inFile.Get("nominal/" + variable + "_data")
  histSignal = inFile.Get("nominal/" + variable + "_signal_reco")
  histBckg   = inFile.Get("nominal/" + variable + "_bkg_all")

  hist = histData.Clone()
  hist.Add(histBckg, -1.0)
  hist.Scale(1./hist.Integral())
  
  histSignal.Scale(1./histSignal.Integral())

  hist.Divide(histSignal)

  hist.GetYaxis().SetTitle("(data - bckg)/signal")
  hist.SetStats(False)
  
  histClone = hist.Clone()
  
  nbins = hist.GetNbinsX()
  
  c = root.TCanvas("c")
  
  hist.Draw()
  hist.Fit("pol1","+")
  
  f1 = hist.GetFunction("pol1")
  f1.SetName("linear_k_1")
  f1.SetLineColor(root.kGreen)
 
  k=-1.
  for i in range(1,nbins+1):
    hist.SetBinContent(i,1. + k*(histClone.GetBinContent(i)-1))
    hist.SetBinError(i,abs(k)*histClone.GetBinError(i))
 
  hist.Fit("pol1","+")
  f2 = hist.GetFunction("pol1")
  f2.SetName("linear_k_m1")
  
  for i in range(1,nbins+1):
    hist.SetBinContent(i,histClone.GetBinContent(i))
    hist.SetBinError(i,histClone.GetBinError(i))
  
  c.SaveAs( outDirPNG + "/reweightingCurve_" + variable + ".png")
  c.SaveAs( outDirPDF + "/reweightingCurve_" + variable + ".pdf")

  outFile = root.TFile(outputDir + '/' + outputFile, "UPDATE")
  outFile.cd()
  hist.Write("ratio_" + variable )
  outFile.Close()


