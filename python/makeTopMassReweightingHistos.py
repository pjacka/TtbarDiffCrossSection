#!/usr/bin/env python

import os
import ROOT as root
import numpy as np
# from array import array


def normalize(hist):
  nbins= hist.GetXaxis().GetNbins()
  hist.Scale(1./hist.Integral())
  for ibin in range(1,nbins+1):
    hist.SetBinContent(ibin,hist.GetBinContent(ibin)/hist.GetBinWidth(ibin))
    hist.SetBinError(ibin,hist.GetBinError(ibin)/hist.GetBinWidth(ibin))
  

ttbarPath=os.environ['TTBAR_PATH']
basePath=os.environ['TtbarDiffCrossSection_output_path']

config = root.TEnv(ttbarPath + "/" + "TtbarDiffCrossSection/data/config.env" )

rootDir = "root."+config.GetValue("mc_samples_production","")

filelistsDir = ttbarPath + "/" + "TtbarDiffCrossSection/filelists"
inputFilelistPath = filelistsDir + "/" + "MC16a.410471.PhPy8EG/allhad.boosted.output.txt"

outputPath = basePath + "/" + rootDir + "/" + "reweightingHistos"
root.gSystem.mkdir(outputPath,True)

outputFilePath = outputPath + "/" + "topMassReweighting.root"

print("input filelist", inputFilelistPath)
print("output file", outputFilePath)


# Reading filelist and opening files
filelist = file(inputFilelistPath,"r")
fileNames = filelist.read().splitlines()
files = []
for filename in fileNames:
  print filename
  files.append(root.TFile.Open(filename))

# Adding files into chaine
truth = root.TChain()
for f in files:
  truth.AddFile(f.GetName(), root.TChain.kBigNumber, "truth")



binning_left = np.array([100,125,140,150,152.5,155,157.5,160,162,163,164,165,166,167,168,168.5,169,169.5,169.7,169.8,169.9],'d')
binning_right = np.array([175,175.1,175.2,175.3,175.5,176,176.5,177,178,179,180,181,182,183,185,187.5,190,195,200,210,225,250],'d')
binning_middle = np.linspace(170,175,1000,endpoint=False)
binning = np.hstack((binning_left,binning_middle,binning_right))

if not np.all(np.diff(binning) > 0):
  raise ValueError("binning is not in increasing order")

nbins = len(binning)-1

fOutput = root.TFile(outputFilePath,"RECREATE")


t_m = "MC_t_afterFSR_m/1000"
tbar_m = "MC_tbar_afterFSR_m/1000"
weight = "weight_mc"


# nentries = truth.GetEntries()

# print ("Number of entries in tchain",nentries)

## RDataFrame based way
root.ROOT.EnableImplicitMT() # Enable multiple threads
dt = np.dtype({
      'names':['name', 'title', 'variable'],
      'formats':['<S300', '<S300', '<S300']})


names = np.array([
  ("171p5","top: m = 171.5 GeV",t_m + " - 1.0"),
  ("171p6","top: m = 171.6 GeV",t_m + " - 0.9"),
  ("171p7","top: m = 171.7 GeV",t_m + " - 0.8"),
  ("171p8","top: m = 171.8 GeV",t_m + " - 0.7"),
  ("171p9","top: m = 171.9 GeV",t_m + " - 0.6"),
  ("172p0","top: m = 172.0 GeV",t_m + " - 0.5"),
  ("172p1","top: m = 172.1 GeV",t_m + " - 0.4"),
  ("172p2","top: m = 172.2 GeV",t_m + " - 0.3"),
  ("172p3","top: m = 172.3 GeV",t_m + " - 0.2"),
  ("172p4","top: m = 172.4 GeV",t_m + " - 0.1"),
  ("172p5","top: m = 172.5 GeV",t_m),
  ("172p6","top: m = 172.6 GeV",t_m + " + 0.1"),
  ("172p7","top: m = 172.7 GeV",t_m + " + 0.2"),
  ("172p8","top: m = 172.8 GeV",t_m + " + 0.3"),
  ("172p9","top: m = 172.9 GeV",t_m + " + 0.4"),
  ("173p0","top: m = 173.0 GeV",t_m + " + 0.5"),
  ("173p1","top: m = 173.1 GeV",t_m + " + 0.6"),
  ("173p2","top: m = 173.2 GeV",t_m + " + 0.7"),
  ("173p3","top: m = 173.3 GeV",t_m + " + 0.8"),
  ("173p4","top: m = 173.4 GeV",t_m + " + 0.9"),
  ("173p5","top: m = 173.5 GeV",t_m + " + 1.0"),
  ],dt)



histos=[]

d = root.RDataFrame(truth)
for i,it in enumerate(names):
  variable = "mass_" + str(i)
  print(variable,it['variable'])
  histos.append(d.Define(variable,it['variable']).Histo1D(root.RDF.TH1DModel(it['name'],it['title'],nbins,binning),variable,weight))

  
stopwatch = root.TStopwatch()
stopwatch.Start()

print("Starting event loop")

for h in histos:
  normalize(h)
  fOutput.WriteObject(h.GetPtr(),h.GetName())

print("Time to fill entries into histograms",stopwatch.RealTime())

print("Histograms are saved in: " + fOutput.GetName())

fOutput.Close()




