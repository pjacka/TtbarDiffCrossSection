import ROOT as root
root.PyConfig.IgnoreCommandLineOptions = True

def addIncludes():
  root.gInterpreter.ProcessLine('#include "TTbarHelperFunctions/latexTablesFunctions.h"')
  root.gInterpreter.ProcessLine('#include "TTbarHelperFunctions/functions.h"')
  root.gInterpreter.ProcessLine('#include "TTbarHelperFunctions/histManipFunctions.h"')
  root.gInterpreter.ProcessLine('#include "TTbarHelperFunctions/plottingFunctions.h"')
  root.gInterpreter.ProcessLine('#include "TTbarHelperFunctions/namesAndTitles.h"')
  
  
  # loadBootstrapGenerator()
  # loadRooUnfold()
  # loadHelperFunctions()
  # loadCovarianceCalculator()
  # loadABCDTools()
  # loadTtbarDiffCrossSection()
  

def loadBootstrapGenerator():
  root.gSystem.Load('libBootstrapGeneratorLib.so')

def loadRooUnfold():
  root.gSystem.Load('libRooUnfold.so')

def loadHelperFunctions():
  root.gSystem.Load('libTTbarHelperFunctionsLib.so')

def loadCovarianceCalculator():
  root.gSystem.Load('libTTbarCovarianceCalculatorLib.so')

def loadABCDTools():
  root.gSystem.Load('libABCD_ToolsLib.so')

def loadTtbarDiffCrossSection():
  root.gSystem.Load('libTtbarDiffCrossSectionLib.so')
