import ROOT as root
root.PyConfig.IgnoreCommandLineOptions = True
import os,sys
from . import cpplibs

cpplibs.addIncludes()

def setStyle():
  root.gROOT.SetStyle("ATLAS");
  root.gStyle.SetErrorX();
  root.gStyle.SetLegendBorderSize(0);
  root.gStyle.SetLegendFillColor(0);
  root.gStyle.SetLegendFont(42);
  root.gStyle.SetLegendTextSize(0.05);

def plotCutflowHistogram(h,figureName):
  c=root.TCanvas("c","")
  
  h.Draw("text0")
  h.DrawClone("same")
  
  c.SetLogy()
  figureNamePNG = figureName.replace("/pdf.","/png.")
  c.SaveAs(figureName+".pdf")
  c.SaveAs(figureNamePNG+".png")


def plotHistograms(hist,leg,dirname,name, lumi,useLogScale,yminFac=0.1,ymaxFac=0.5):


  for h in hist:
    h.SetMarkerSize(0.7)
  
  minValue = sys.float_info.max
  maxValue = -sys.float_info.max
  for h in hist:
    maxValue=max(maxValue,root.functions.getMaximum(h))
    minValue=min(minValue,root.functions.getMinimum(h,0.))
    
  maxFinal = 0.
  minFinal = 0.
  
  if useLogScale:
    maxFinal=maxValue*(1+pow(maxValue/minValue,ymaxFac))
    minFinal=minValue/(1+pow(maxValue/minValue,yminFac))
  else:
    maxFinal=maxValue + (maxValue - minValue)*ymaxFac
    minFinal=minValue - (maxValue - minValue)*yminFac
    
  
  hDummy = root.TH2D("","",hist[0].GetXaxis().GetNbins(),hist[0].GetXaxis().GetXmin(),hist[0].GetXaxis().GetXmax(),2,minFinal,maxFinal)
  hDummy.SetStats(False)
  hDummy.SetXTitle(hist[0].GetXaxis().GetTitle())
  hDummy.SetYTitle(hist[0].GetYaxis().GetTitle())
  hDummy.GetYaxis().SetTitleSize(0.07)
  hDummy.GetYaxis().SetTitleOffset(1.)
  hDummy.GetYaxis().SetLabelSize(0.06)
  
  
  c = root.TCanvas("c","")
  pad1 = root.TPad("pad1","pad1",0,0,1,1,0,0,0)
  pad1.SetBottomMargin(0.15)
  pad1.SetTopMargin(0.05)
  pad1.SetRightMargin(0.05)
  pad1.SetTicks()
  if useLogScale:
    pad1.SetLogy()
  pad1.Draw()
  pad1.cd()
  
  hDummy.Draw()
  
  root.functions.WriteGeneralInfo("",lumi,0.06,0.2,0.85,0.05)
  
  for h in hist:
    if h.InheritsFrom("TGraphAsymmErrors"):
      h.Draw("E2 same")
    else:
      h.Draw("E1 same")
  hist[0].Draw("E1 same")  
  
  leg.Draw()

  pad1.RedrawAxis("g")

  figureName=dirname + '/' + name
  figureNamePNG=figureName.replace('/pdf.','/png.')
  
  c.SaveAs(figureName+".pdf")
  c.SaveAs(figureNamePNG+".png")


def plotHistogramsTwoPads(hist,leg,dirname,name,hd, y2name, y2min, y2max, lumi,useLogScale):
          
  hdenumerator = hd.Clone("hdenumerator")
  ratios = []

  for h in hist:
    ratios.append(h.Clone())
    h.SetMarkerSize(0.7)
  
  minValue = sys.float_info.max
  maxValue = -sys.float_info.max
  for h in hist:
    maxValue=max(maxValue,root.functions.getMaximum(h))
    minValue=min(minValue,root.functions.getMinimum(h,0.))
    
  maxFinal = 0.
  minFinal = 0.
  
  if useLogScale:
    maxFinal=maxValue*(1+pow(maxValue/minValue,1./2.))
    minFinal=minValue/(1+pow(maxValue/minValue,1./4.))
  else:
    maxFinal=1.5*maxValue
    minFinal=minValue/1.3
    
  
  hDummy = root.TH2D("","",hist[0].GetXaxis().GetNbins(),hist[0].GetXaxis().GetXmin(),hist[0].GetXaxis().GetXmax(),2,minFinal,maxFinal)
  hDummy.SetStats(False)
  hDummy.SetXTitle(hist[0].GetXaxis().GetTitle())
  hDummy.SetYTitle(hist[0].GetYaxis().GetTitle())
  hDummy.GetYaxis().SetTitleSize(0.075)
  hDummy.GetYaxis().SetTitleOffset(0.6)
  hDummy.GetYaxis().SetLabelSize(0.06)
  
  
  c = root.TCanvas("c","")
  
  pad1 = root.TPad("pad1","pad1",0,0.33,1,1,0,0,0)
  pad2 = root.TPad("pad2","pad2",0,0,1,0.33,0,0,0)
  pad2.SetBottomMargin(0.3)
  pad2.SetTopMargin(0)
  pad2.SetRightMargin(0.05)
  pad2.SetGridy(1)
  pad2.SetTicks()
  pad1.SetBottomMargin(0.)
  pad1.SetTopMargin(0.05)
  pad1.SetRightMargin(0.05)
  pad1.SetTicks()
  if useLogScale:
    pad1.SetLogy()
  pad1.Draw()
  pad2.Draw()
  pad1.cd()
  pad2.cd()
  
  pad1.cd()
  
  hDummy.Draw()
  
  root.functions.WriteGeneralInfo("",lumi,0.06,0.2,0.85,0.05)
  
  for h in hist:
    h.Draw("same")
  leg.Draw()

  h=hist[0]

  h2 = root.TH2D("","",h.GetXaxis().GetNbins(),h.GetXaxis().GetXmin(),h.GetXaxis().GetXmax(),2,y2min,y2max)
  if(h.GetXaxis().GetLabels()):
    for i in range(1,h.GetXaxis().GetNbins()+1):
      h2.GetXaxis().SetBinLabel(i,h.GetXaxis().GetBinLabel(i))
    
  h2.SetStats(False)
  h2.SetXTitle(h.GetXaxis().GetTitle())
  h2.SetYTitle(y2name)
  h2.GetXaxis().SetTitleSize(0.15)
  h2.GetXaxis().SetTitleOffset(0.85)
  h2.GetYaxis().SetTitleSize(0.11)
  h2.GetYaxis().SetTitleOffset(0.38)
  h2.GetYaxis().SetNdivisions(10)
  h2.GetXaxis().SetLabelSize(0.123)
  h2.GetYaxis().SetLabelSize(0.1)
  
  pad2.cd()
  h2.Draw()
  for r in ratios: 
    r.Divide(hdenumerator)
    r.Draw("same")

  figureName=dirname + '/' + name
  figureNamePNG=figureName.replace('/pdf.','/png.')
  
  c.SaveAs(figureName+".pdf")
  c.SaveAs(figureNamePNG+".png")
  
