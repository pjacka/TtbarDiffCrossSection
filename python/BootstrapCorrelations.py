#!/usr/bin/env python

import os, sys
from ROOT import *
from array import array

from math import sqrt 

from ProgressBar import *

#xAnalysis path -> $TTBAR_PATH
xAnalysis_path=os.environ['TTBAR_PATH']
gSystem.Load( xAnalysis_path+"/RooUnfold/libRooUnfold.so")

OutputPath=os.environ['TtbarDiffCrossSection_output_path']
reco_basedir=OutputPath
#reco_basedir = "../AnalysisTop/run/output/"

sf_MC = 1.0

#############################

pretty_names_observables = {
  'topCandidate_pt' : "p_{T}^{t,rand}",
  'leadingTop_pt'   : "p_{T}^{t,1}",
  'leadingTop_y' : "|y^{t,1}|",
  'subleadingTop_pt'   : "p_{T}^{t,2}",
  'subleadingTop_y' : "|y^{t,2}|",
  'ttbar_mass'      : "m^{t#bar{t}}",
  'ttbar_pt'     : "p_{T}^{t#bar{t}}",
  'ttbar_y' : "|y^{t#bar{t}}|",
  'pout' : "|p_{out}^{t#bar{t}}|",
  'ttbar_deltaphi' : "#Delta #phi^{t#bar{t}}",
  'H_tt_pt'   : "H_{T}^{t#bar{t}}",
  'z_tt_pt'   :  "z_{T}^{t#bar{t}}",
  'chi_ttbar'  : "#chi^{t#bar{t}}",
  'y_boost'     : "y_{B}^{t#bar{t}}",
  'cos_theta_star' : "cos#theta^{*}",
  'inclusive_top_pt' : "p_{T}^{t,inc}",
  'inclusive_top_y' : "|y^{t,incl}|",
  'randomTop_y' : "|y^{t,rand}|",
  'y_star' : "y*",
}

#############################

phspace = "Parton"

Particle="ParticleLevelCutsPassed_RecoLevelCutsPassed_"
Parton="PartonLevelCutsPassed_RecoLevelCutsPassed_"
obs_end="_fine_migration_rebined_switchedAxis_"
observables = [
	"topCandidate_pt",
	"randomTop_y",
	"leadingTop_pt",
	"leadingTop_y",
	"subleadingTop_pt",
	"subleadingTop_y",
	"ttbar_mass",
	"ttbar_pt",
	"ttbar_y",
	"chi_ttbar",
	"ttbar_deltaphi",
	"pout",
	"y_boost",
	"cos_theta_star",
	"H_tt_pt",
	]

numberOfObs=0
for obs in observables:
    ObsName=obs
    obs=Parton+ObsName+obs_end
    observables[numberOfObs]=obs
    numberOfObs+=1
	

lvl     = ""

meas = "abs"
if len(sys.argv) > 1:
   meas = sys.argv[1] # 'rel' or 'abs'

n_replicas = 1000
if len(sys.argv) > 2:
  n_replicas = int( sys.argv[2] )

nitr  = 4

n_observables = len( observables )

print "INFO: Phase-space: %s" % phspace
print "INFO: Measurement: %s" % meas
print "INFO: Observables: %s" % observables
print "INFO: Region: %s"      %  lvl 
print "INFO: Number of pseudo-experiments(replicas) =", n_replicas
print "INFO: Regularization parameter =", nitr

# Read in histograms
# TO BE CHANGED! (TBC)
f_unf  = TFile.Open( reco_basedir+"/root/bootstrap/unfolded.root" )

nbins = {}
n_bins_tot = 0
# read once to get the binning
for obs in observables:
   # TBC!
   hname = "%s1" % ( obs )
   h = f_unf.Get( hname )
   print hname
   nbins[obs] = h.GetNbinsX()

   print "INFO: %10s nbins = %i" % ( obs, nbins[obs] )
   n_bins_tot += nbins[obs]
print "INFO: total number of bins: %i" % n_bins_tot

# Main loop
principal = TPrincipal( n_bins_tot, "ND" )

progress = ProgressBar( 0, n_replicas, 77, mode='static', char="#" )

print "INFO: Loop over %i replicas" % n_replicas
for itoy in range( n_replicas ):

  toy = array( 'd', [0.]*n_bins_tot )

  k = 0
  for obs in observables:
     # TBC!
     hname = "%s%i" % ( obs, itoy+1 )
     h_absXs = f_unf.Get(hname) #!!! get the unfolded replica from the root file!
     #print hname
     h_absXs.SetName( "unfolded_%s" % (obs ) )
  

     area = h_absXs.Integral()
     for i in range(nbins[obs]):
       y = h_absXs.GetBinContent(i+1)
       if meas == "rel": y /= area
       toy[k] = y
       k += 1

     h_absXs.Delete() 
  
     

  # append toy
  principal.AddRow( toy )

  progress.increment_amount()
  print progress, "\r",

# end main loop
print

principal.MakePrincipals()

cov = principal.GetCovarianceMatrix()

# convert to TH2D
h2_cov = TH2D( "covariance", "Covariance matrix (Absolute cross-sections)", n_bins_tot, 0.5, n_bins_tot+0.5, n_bins_tot, 0.5, n_bins_tot+0.5 )
for i in range(n_bins_tot):
  for j in range(n_bins_tot):
      h2_cov.SetBinContent( i+1, j+1, cov(i,j) )

h2_corr = TH2D( "correlation", "Correlation matrix (Absolute cross-sections)", n_bins_tot, 0.5, n_bins_tot+0.5, n_bins_tot, 0.5, n_bins_tot+0.5 )
for i in range(n_bins_tot):
  sigma_i = h2_cov.GetBinContent( i+1, i+1 )
  for j in range(n_bins_tot):
    sigma_j = h2_cov.GetBinContent( j+1, j+1 )

    Cij = h2_cov.GetBinContent( i+1, j+1 )
    cij = Cij / sqrt( sigma_i * sigma_j )
    h2_corr.SetBinContent( i+1, j+1, cij )
h2_corr.SetMaximum(  1.0 )
h2_corr.SetMinimum( -1.0 )

k = 0
for obs in observables:
  for i in range(nbins[obs]):
	key=obs.replace(Parton, "")
	key=key.replace(obs_end, "")
	#print key
	label = "%s - %i" % ( pretty_names_observables[key], i+1 )
	h2_cov.GetXaxis().SetBinLabel( k+1, label )
	h2_cov.GetYaxis().SetBinLabel( k+1, label )
	h2_corr.GetXaxis().SetBinLabel( k+1, label )
	h2_corr.GetYaxis().SetBinLabel( k+1, label )

	k += 1 

ofilename = reco_basedir+"/root/bootstrap/mcov"+meas+".root"
ofile = TFile.Open( ofilename, "RECREATE" )

cov.Write( "Mcov" )
h2_cov.Write( "covariance" )
h2_corr.Write( "correlation" )

ofile.Close()
print "INFO: output file created", ofile.GetName()
