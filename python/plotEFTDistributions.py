#!/usr/bin/env python

import xml.etree.ElementTree as et
import ROOT as root
import math
import numpy as np
import tools
import ctypes

root.PyConfig.IgnoreCommandLineOptions = True
root.gROOT.SetBatch(True)

rng = np.random

def makeDType():
  return np.dtype({
      'names':['id', 'label', 'latex' , 'min', 'max', 'color' ],
      'formats':[object, object, object, object, object, object] })


def makeOptions(nobservables,nmeasurements,nuncertainties,nnuisance):
  options="""<options
NObservables="{}"
NMeasurements="{}"
NUncertainties="{}"
NNuisance="{}"
></options>
  """.format(nobservables, nmeasurements,nuncertainties,nnuisance)

  return options


def makeObservable(bin,name,min,max):
  
  observable="""<observable{bin}
name="{name}"
min="{min}"
max="{max}"
></observable{bin}>""".format(bin=bin-1,name=name,min=min,max=max)
  
  return observable


def makeUncertainties():
  uncertainties="""<uncertainties
uncertainty0="total"
></uncertainties>
  """
  return uncertainties
  
def makeMeasurement(id,observable,name,active,value,unc):
  measurement="""<measurement{id}
observable="{observable}"
name="{name}"
active="{active}"
value="{value}"
uncertainty0="{unc}"
></measurement{id}>""".format(id=id-1,observable=observable,name=name,active=active,value=value,unc=unc)
  return measurement

def makeCorrelationMatrix(id,cor):
  correl="<CorrelationMatrix{}\n>".format(id)
  
  num_rows, num_cols = cor.shape
  
  for i in range(0,num_cols):
    for j in range(0,num_cols):
      correl+="{} ".format(cor[i][j])
    correl+="\n"
  
  correl+="</CorrelationMatrix{}>\n".format(id)
  return correl
  

def loadParameters(coeffInfo,variable):
  
  
  pathEFT="/mnt/nfs19/hejbal/EFT_TTBarAllhadBoosted/results/official_samples/v001"
  param_p1={}
  param_p2={}
  
  fileBaseName = pathEFT + "/" + "diffXsec_" 

  for coeff  in coeffInfo:
    
    idstr = "ID" + coeff['id']
    fileName = fileBaseName + idstr + "_" + variable + ".root"
    f = root.TFile.Open(fileName)
    
    h_p1 = f.Get("p1_"+idstr+"_"+variable)
    h_p2 = f.Get("p2_"+idstr+"_"+variable)
    
    param_p1[idstr] = []
    param_p2[idstr] = []
    for ibin in range(1,h_p1.GetNbinsX()+1):
      param_p1[idstr].append(h_p1.GetBinContent(ibin))
      param_p2[idstr].append(h_p2.GetBinContent(ibin))
    
  return (param_p1,param_p2)


def calculateEFTDistr(pred,coeff,param_p1,param_p2):
  
  distr = root.functions.histFromGraph(pred)
  distr.Sumw2()
  

  for i in range(0,nbins):
    bincontent = pred.GetPointY(i) + coeff*param_p1[i] + coeff*coeff*param_p2[i]
    distr.SetBinContent(i+1,bincontent)

  return distr
  
  
def makeLegend(xmin=0.515,ymin=0.6,xmax=0.92,ymax=0.92):
  textSize=root.gStyle.GetLegendTextSize()  
  leg = root.TLegend(xmin,ymin,xmax,ymax);
  leg.SetTextSize(textSize)
  
  return leg
  

def chi2Test(h1,h2,cov):
  
  x1 = root.functions.tMatrixFromHistogram(h1)
  x2 = root.functions.tMatrixFromHistogram(h2)

  diff = x1;
  diff-=x2;
  covInverse = cov.Clone()
  covInverse = covInverse.Invert()
  
  chi2 = root.functions.chi2Statistics(diff, covInverse)
  ndf = diff.GetNcols()
  pValue = root.TMath.Prob(chi2,ndf)

  return pValue,chi2,ndf


root.functions.setStyle()

# Setting paths to measured variables in .root format (central values and correlation matrices) 
ttbarOutputPath=root.gSystem.Getenv("TtbarDiffCrossSection_output_path")
print(ttbarOutputPath)
ttbarPath = root.gSystem.Getenv("TTBAR_PATH")
matrixPath=ttbarPath+"/TtbarDiffCrossSection/data/MATRIX/NNLO_HT2_NNPDF31/hists_MATRIX_NNLO_HT2_NNPDF31.root"



fullOutputPath=ttbarOutputPath+"/"+"pdf.All" + "/" + "eftFitterInputs"
useMatrixUncertainty=1

root.gSystem.mkdir(fullOutputPath,1)


centralValuesDir=ttbarOutputPath+"/"+"root.All/TheoryVsDataComparison_newModeling"
covariancesDir=ttbarOutputPath+"/"+"root.All/covariances"

level="Parton"

variables=(
# "total_cross_section",
# "randomTop_pt",
# "randomTop_y",
"t1_pt",
# "t1_y",
# "t2_pt",
# "t2_y",
# "ttbar_mass",
# "ttbar_pt",
# "ttbar_y",
# "chi_ttbar",
# "y_boost",
# "pout",
# "H_tt_pt",
# "ttbar_deltaphi",
# "cos_theta_star",
# "delta_pt"
)


changeUnits=(
"randomTop_pt",
"t1_pt",
"t2_pt",
"ttbar_mass",
"ttbar_pt",
"pout",
"H_tt_pt",
"delta_pt"
)


dt = makeDType()


coeffInfo = np.array([
    
    # <id> | <label> | <latex> | <min> | <max> | <color>
    # ('16' ,   'ctg'   ,'ctg'   , '-0.2' , '0.2') ,
    # ('17' ,   'ctgi'  ,'ctgi'  , '-8' , '8') ,
    ('51' ,   'cqq83' ,'C_{Qq}^{3,8}' , '-0.46' , '0.25' , '1') ,
    ('52' ,   'cqq81' ,'C_{Qq}^{1,8}' , '-0.55' , '0.2', '2') ,
    # ('51' ,   'cqq83' ,'C_{Qq}^{3,8}' , '-0.6' , '0.45' , '2') ,
    # ('51' ,   'cqq83' ,'C_{Qq}^{3,8}' , '-0.8' , '0.6' , '4') ,
    # ('52' ,   'cqq81' ,'C_{Qq}^{1,8}' , '-1.1' , '0.5', '2') ,
    # ('53' ,   'cqu8' , 'C_{Qu}^{8}' , '-0.5' , '0.3', '1')  ,
    # ('54' ,   'cqd8' , 'C_{Qd}^{8}' , '-0.7' , '0.5', '1')  ,
    # ('55' ,   'ctq8' , 'C_{tq}^{8}' , '-0.5' , '0.3', '1')  ,
    # ('56' ,   'ctu8' , 'C_{tu}^{8}' , '-0.5' , '0.3', '1')  ,
    # ('57' ,   'ctd8' , 'C_{td}^{8}' , '-0.7' , '0.5', '1')  ,
    # ('58' ,   'cqq13' ,'cqq13' , '-10' , '10') ,
    # ('59' ,   'cqq11' ,'cqq11' , '-10' , '10') ,
    # ('60' ,   'cqu1' , 'cqu1' , '-10' , '10')  ,
    # ('61' ,   'cqd1' , 'cqd1' , '-10' , '10')  ,
    # ('62' ,   'ctq1' , 'ctq1' , '-10' , '10')  ,
    # ('63' ,   'ctu1' , 'ctu1' , '-10' , '10')  ,
    # ('64' ,   'ctd1' , 'ctd1' , '-10' , '10')  ,
    # ('65' ,   'cqq1' , 'cqq1' , '-100' , '100')  ,
    # ('66' ,   'cqq8' , 'cqq8' , '-100' , '100')  ,
    # ('67' ,   'cqt1' , 'cqt1' , '-100' , '100')  ,
    # ('68' ,   'cqb1' , 'cqb1' , '-100' , '100')  ,
    # ('70' ,   'ctb1' , 'ctb1' , '-100' , '100')  ,
    # ('71' ,   'cqt8' , 'cqt8' , '-100' , '100')  ,
    # ('72' ,   'cqb8' , 'cqb8' , '-100' , '100')  ,
    # ('73' ,   'ctb8' , 'ctb8' , '-100' , '100')  ,
    # ('94' ,   'cqtqb1','cqtqb1', '-100' , '100') ,
    # ('95' ,   'cqtqb8', 'cqtqb8', '-100' , '100') ,
    # ('96' ,   'cqtqb1i','cqtqb1i','-100' , '100') ,
    # ('97' ,   'cqtqb8i','cqtqb8i','-100' , '100')
    ], dtype=dt)



for variable in variables:
  covarianceFilePath=covariancesDir+"/"+variable+"_"+level+"Level.root"
  centralValuesFilePath=centralValuesDir+"/"+level+"."+variable+"."+"Data"+".root"
  
  fileCovariances=root.TFile(covarianceFilePath)
  fileCentralValues=root.TFile(centralValuesFilePath)
  fileMATRIX=root.TFile(matrixPath)
  
  data = fileCovariances.Get(variable+"_centralValues")
  
  graph_unc = root.functions.graphFromHist(data);
  cov = fileCovariances.Get(variable+"_covDataBasedStatSysAndSigMod")
  root.functions.setErrors(graph_unc,cov)
  
  graph_unc.SetFillStyle(1001);
  graph_unc.SetFillColor(root.kGray+1);
  graph_unc.SetLineColor(root.kGray+1);
  
  graph_unc_ratio = root.functions.getUnitGraph(graph_unc)
  
  print(fileCovariances.GetName())
  
  nameConversion = {
    "t1_pt":"leadingTop_pt",
    "t1_y":"leadingTop_y",
    "t2_pt":"subleadingTop_pt",
    "t2_y":"subleadingTop_y"
  }
  
  variableConverted=variable
  if variable in nameConversion:
    variableConverted=nameConversion[variable]
  
  matrixPred = fileMATRIX.Get(variableConverted+"_total")
  
  nbins = matrixPred.GetN();
  # values = matrixPred.GetY();
  
  factor=2.1882987 # Switch to all channels cross-section
  #Changing units from TeV^-1 to GeV^-1 for some variables
  # if variable in changeUnits:
    # factor/=1000 
  
  lumi = 138965.16

  # data.Scale(1./data.Integral('width'))


  
  centralValues = matrixPred.Clone()
  for i in range(0,nbins):
    centralValues.SetPointY(i,matrixPred.GetPointY(i)*matrixPred.GetErrorX(i)*2*lumi/1000.)
    
  centralValues.Print()


  root.functions.changeUnits(matrixPred,0.001)
  matrixPredRatio = root.functions.histFromGraph(matrixPred)
  matrixPredRatio.Scale(1.0/factor)
  # root.functions.DivideByBinWidth(matrixPredRatio)
  matrixPredRatio.Divide(data)
  matrixPredRatio.SetLineColor(root.kGreen)
  matrixPredRatio.SetMarkerColor(root.kGreen)
 
  param_p1,param_p2 = loadParameters(coeffInfo,variable)

  

  lumiString='139 fb^{-1}'
  ytitle = "#frac{Prediction}{Data}"
  xtitle = str(root.NamesAndTitles.getVariableLatex(variable))
  unit = str(root.NamesAndTitles.getDifferentialVariableUnit(variable))
  # unit = unit.replace("TeV","GeV")
  if unit != "":
    xtitle = xtitle + " [" +unit+"]"
    
    
  outputDirPDF = fullOutputPath
  outputDirPNG = outputDirPDF.replace("/pdf.","/png.")
  root.gSystem.mkdir(outputDirPNG,True)

  ratiosCombined = []
  legCombined = makeLegend(xmin=0.5,ymin=0.66)
  legCombined.SetNColumns(2)

  for coeff in coeffInfo:

    idstr="ID" + coeff['id']
    eftDistrDown = calculateEFTDistr(centralValues,float(coeff['min']),param_p1[idstr],param_p2[idstr])
    eftDistrUp = calculateEFTDistr(centralValues,float(coeff['max']),param_p1[idstr],param_p2[idstr])
    
    root.functions.changeUnits(eftDistrDown,0.001)
    root.functions.changeUnits(eftDistrUp,0.001)
    eftDistrDown.Scale(1000./lumi/factor)
    eftDistrUp.Scale(1000./lumi/factor)
    # eftDistrDown.Scale(1./eftDistrDown.Integral())
    # eftDistrUp.Scale(1./eftDistrUp.Integral())
    root.functions.DivideByBinWidth(eftDistrDown)
    root.functions.DivideByBinWidth(eftDistrUp)
    
    
    eftDistrDown.Print()
    eftDistrUp.Print()
  
    leg = makeLegend(xmin=0.5,ymin=0.72)
    leg.SetNColumns(2)
    
    
    pValue,chi2,ndf = chi2Test(data,eftDistrDown,cov)
    print ("Chi2 test resutls " + coeff['label'] + " up:",pValue,chi2,ndf)
    pValue,chi2,ndf = chi2Test(data,eftDistrUp,cov)
    print ("Chi2 test resutls " + coeff['label'] + " down:",pValue,chi2,ndf)
    
    ratioDown = eftDistrDown.Clone()
    ratioDown.Divide(data)
    # root.functions.divideHistogramByGraph(ratioDown,centralValues)
    
    ratioUp = eftDistrUp.Clone()
    ratioUp.Divide(data)

    # root.functions.divideHistogramByGraph(ratioUp,centralValues)
    ratioUp.SetLineStyle(2)
    
    leg.AddEntry(ratioDown,coeff['latex'] + " = " + str(coeff['min']))
    leg.AddEntry(ratioUp,coeff['latex'] + " = " + str(coeff['max']))
    leg.AddEntry(matrixPredRatio,"SM NNLO")
    leg.AddEntry(graph_unc_ratio,"Stat+Sys")
    
    
    ratios = [ratioDown,graph_unc_ratio,ratioUp,matrixPredRatio]
  
    for ratio in ratios:
      ratio.GetXaxis().SetTitle(xtitle)
      ratio.GetYaxis().SetTitle(ytitle)
    
    tools.plottingFunctions.plotHistograms(ratios,leg,outputDirPDF,variable+"_"+coeff['label'], lumiString,useLogScale=False)
  
    if coeff['label'] == 'cqq83' or coeff['label'] == 'cqq81' :
      ratioDown.SetLineColor(int(coeff['color']))
      ratioUp.SetLineColor(int(coeff['color']))
      ratioDown.SetMarkerColor(int(coeff['color']))
      ratioUp.SetMarkerColor(int(coeff['color']))
      ratiosCombined.append(ratioDown)
      ratiosCombined.append(ratioUp)
      legCombined.AddEntry(ratioDown,coeff['latex'] + " = " + str(coeff['min']))
      legCombined.AddEntry(ratioUp,coeff['latex'] + " = " + str(coeff['max']))

  # for i in range(0,nbins):
    # print(matrixHist.GetBinContent(i+1))
    # print(eftDistr.GetBinCenter(i+1))
    #print(centralValues.GetPointX(i))
    # print(centralValues.GetPointY(i))
  
  ratiosCombined.append(matrixPredRatio)
  ratiosCombined.insert(1,graph_unc_ratio)
  legCombined.AddEntry(matrixPredRatio,"SM NNLO")
  legCombined.AddEntry(graph_unc_ratio,"Stat+Sys")
  tools.plottingFunctions.plotHistograms(ratiosCombined,legCombined,outputDirPDF,variable+"_combined", lumiString,useLogScale=False,yminFac=0.1,ymaxFac=0.8)

  

