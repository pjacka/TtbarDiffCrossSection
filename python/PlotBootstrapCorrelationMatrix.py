#!/usr/bin/env python

import sys, os
from array import array

from ROOT import *

gROOT.SetBatch(1)

#gROOT.Macro("./rootlogon.C")
xAnalysis_path=os.environ['TTBAR_PATH']
gROOT.LoadMacro(xAnalysis_path+"/TtbarDiffCrossSection/python/AtlasUtils.C")
OutputPath=os.environ['TtbarDiffCrossSection_output_path']

#meas = "Absolute"
meas = "Relative"

if len(sys.argv) > 1:
   meas = sys.argv[1] # 'rel' or 'abs'

#############################

pretty_names_observables = {
  'topCandidate_pt' : "p_{T}^{t}",
  'leadingTop_pt'   : "p_{T}^{t,1}",
  'leadingTop_y' : "|y^{t,1}|",
  'subleadingTop_pt'   : "p_{T}^{t,2}",
  'subleadingTop_y' : "|y^{t,2}|",
  'ttbar_mass'      : "m^{t#bar{t}}",
  'ttbar_pt'     : "p_{T}^{t#bar{t}}",
  'ttbar_y' : "|y^{t#bar{t}}|",
  'pout' : "|p_{out}^{t#bar{t}}|",
  'ttbar_deltaphi' : "#Delta #phi^{t#bar{t}}",
  'H_tt_pt'   : "H_{T}^{t#bar{t}}",
  'z_tt_pt'   :  "z_{T}^{t#bar{t}}",
  'chi_ttbar'  : "#chi^{t#bar{t}}",
  'y_boost'     : "y_{B}^{t#bar{t}}",
  'cos_theta_star' : "cos#theta^{*}",
  'inclusive_top_pt' : "p_{T}^{t,inc}",
  'inclusive_top_y' : "|y^{t,incl}|",
  'randomTop_y' : "|y^{t,rand}|",
  'y_star' : "y*",
}

#############################

phspace = "parton"

Particle="ParticleLevelCutsPassed_RecoLevelCutsPassed_"
Parton="PartonLevelCutsPassed_RecoLevelCutsPassed_"
obs_end="_fine_migration_rebined_switchedAxis_"

observables = [
	"topCandidate_pt",
	"randomTop_y",
	"leadingTop_pt",
	"leadingTop_y",
	"subleadingTop_pt",
	"subleadingTop_y",
	"ttbar_mass",
	"ttbar_pt",
	"ttbar_y",
	"chi_ttbar",
	"ttbar_deltaphi",
	"pout",
	"y_boost",
	"cos_theta_star",
	"H_tt_pt",
	]	

nbins_tot = {
    "topCandidate_pt" : 7,
    "randomTop_y"  : 8,
    "leadingTop_pt" : 8,
    "leadingTop_y" : 8,
    "subleadingTop_pt" : 7,
    "subleadingTop_y" : 8,
    "ttbar_mass" : 10,
    "ttbar_pt" : 8,
    "ttbar_y" : 8,
    "chi_ttbar" : 7,
    "ttbar_deltaphi" : 4,
    "pout" : 7,
    "y_boost" : 7,
    "y_star" : 7,
    "cos_theta_star" : 6,
    "H_tt_pt" : 10,
    "inclusive_top_pt" : 9,
    "inclusive_top_y"  : 8,
    "z_tt_pt" : 8,
}

########################################################################


def dump_matrix( h ):
    for row in h:
        for col in row:
            print "%3.2f " % col,
        print
    print


########################################################################


def SetPalette( colset = "green" ):
    if colset == "green":
        stops = array( 'd', [ 0.00, 0.25, 1.00 ] )
        
        red   = array( 'd', [ 0.67, 0.43, 0.05 ] )
        green = array( 'd', [ 0.86, 0.78, 0.59 ] )
        blue  = array( 'd', [ 0.79, 0.69, 0.53 ] )
    
    if colset == "blue":
        stops = array( 'd', [ 0.00, 0.25, 1.00 ] )
        
        red   = array( 'd', [ 0.39, 0.00, 0.00 ] )
        green = array( 'd', [ 0.80, 0.62, 0.40 ] )
        blue  = array( 'd', [ 0.93, 0.88, 0.72 ] )

    if colset == "blue2redB":
        stops = array( 'd', [ 0.00, 0.35, 0.65, 1.00 ] )
        
        # 286CP / 284CP / x / 177CP / 7627CP
        red   = array( 'd', [ 0.13, 0.45, 0.96, 0.74 ] )
        green = array( 'd', [ 0.31, 0.67, 0.69, 0.29 ] )
        blue  = array( 'd', [ 0.60, 0.85, 0.72, 0.14 ] )

    if colset == "blue2red":
        stops = array( 'd', [ 0.00, 0.05, 0.50, 0.95, 1.00 ] )
        # 286CP / 284CP / x / 177CP / 7627CP
        red   = array( 'd', [ 0.13, 0.45, 1.00, 0.96, 0.74 ] )
        green = array( 'd', [ 0.31, 0.67, 1.00, 0.69, 0.29 ] )
        blue  = array( 'd', [ 0.60, 0.85, 1.00, 0.72, 0.14 ] )

    TColor.CreateGradientColorTable( len(stops), stops, red, green, blue, 100 )
#gStyle.SetPalette(1)


########################################################################

def DrawGrid( h ):
    line = TLine()
    line.SetLineWidth( 1 )
    line.SetLineColor( kWhite )
    
    xaxis = h.GetXaxis()
    yaxis = h.GetYaxis()
    
    #xaxis.SetMoreLogLabels()
    #yaxis.SetMoreLogLabels()
    
    for binx in range( 0, h.GetNbinsX() ):
        xmin = xaxis.GetBinLowEdge( binx + 1 )
        
        if not xmin == xaxis.GetXmin():
            line.DrawLine( xmin, yaxis.GetXmin(), xmin, yaxis.GetXmax() )
    
    for biny in range( 0, h.GetNbinsY() ):
        ymin = yaxis.GetBinLowEdge( biny + 1 )
        
        if not ymin == yaxis.GetXmin():
            line.DrawLine( xaxis.GetXmin(), ymin, xaxis.GetXmax(), ymin )


def DrawGridObservables( h, nbins_tot ):
   line = TLine()
   line.SetLineWidth( 1 )
   line.SetLineColor( kGray+1 )
  
   xaxis = h.GetXaxis()
   yaxis = h.GetYaxis()

   k = 0
   l = 0
   for obs in observables:
      #print obs, "bin ", k, k-l
      xmin = xaxis.GetBinLowEdge( k+1 )
      if not xmin == xaxis.GetXmin():
          line.DrawLine( xmin, yaxis.GetXmin(), xmin, yaxis.GetXmax() )   
      ymin = yaxis.GetBinLowEdge( k + 1 )
      if not ymin == yaxis.GetXmin():
          line.DrawLine( xaxis.GetXmin(), ymin, xaxis.GetXmax(), ymin )
      l=k
      k += nbins_tot[obs]


########################################################################

def ATLAS_LABEL_custom( x, y, color ):
    ATLAS_LABEL( x, y, color )
    #myText( x+0.13, y,     color, "work in progress     #int Ldt = 4.7 fb^{-1}" )
#myText( x,     y-0.15, color, "#int Ldt = 4.7 fb^{-1}" )

    myText( x+0.17, y, color, " #sqrt{s} = 13 TeV, 36.1 fb^{-1}" )


########################################################################


def MakeCanvas():
    sizex = 1800
    #sizey = int( sizex * 1.35 )
    c = TCanvas( "Response_Matrix", "Response_Matrix", sizex, sizex )
    
    gPad.SetLeftMargin( 0.05 )
    gPad.SetRightMargin( 0.15 )
    gPad.SetBottomMargin( 0.05 )
    gPad.SetTopMargin( 0.20 )
    
    #c.SetLogy()
    #c.SetLogx()
    #c.SetLogz()
    
    return c


########################################################################

def MakeAllAbsValues( matrix ):
    hm = matrix.Clone( "abs" )
    hm.SetMinimum(-1.)
    hm.SetMaximum(1.)
    return hm
    for i in range( matrix.GetNbinsX() ):
        for j in range( matrix.GetNbinsY() ):
            v = matrix.GetBinContent( i+1, j+1 )
            hm.SetBinContent( i+1, j+1, abs(v) )
    
    return hm

########################################################################


def MakeUniformBins( h ):
   nbins = h.GetNbinsX()

   hnew = TH2D( "hcorr", "hcorr", nbins, 1, nbins, nbins, 1, nbins )

   for i in range(nbins):
      for j in range(nbins):
         y = h.GetBinContent( i+1, j+1 )
         hnew.SetBinContent( i+1, j+1, y )
   return hnew

def TMatrixToTH2( m ):
   nbins = m.GetNrows()
   h = TH2D( "hcorr", "hcorr", nbins, 1, nbins, nbins, 1, nbins )
   for i in range(nbins):
      for j in range(nbins):
        mij = m[i][j]
        if not mij == mij: mij = 0.
        h.SetBinContent( i+1, j+1, mij )
   return h

#######################################################################


#produce plot
SetPalette( "blue2red" )
gStyle.SetPaintTextFormat( "3.2f%" )
gStyle.SetHistMinimumZero()

if phspace=="parton": infilename = OutputPath+"/root/bootstrap_dataStat/PartonLevel.root"
if phspace=="particle": infilename = OutputPath+"/root/bootstrap_dataStat/ParticleLevel.root"
#infilename = sys.argv[1]


#meas = infilename.split('/')[-1].split('.')[-2]

#phspace = "parton" # if len(sys.argv) < 3 else sys.argv[3]

infile = TFile.Open( infilename )

hcorr  = infile.Get( meas + "/Correlation" )

nbinstot = hcorr.GetNbinsX()

#for i in range(nbinstot):
#   hcorr.GetXaxis().SetBinLabel( i+1, "%i" % (i+1) )
#   hcorr.GetYaxis().SetBinLabel( i+1, "%i" % (i+1) )

hcorr_abs = MakeAllAbsValues( hcorr ) #needed for color gradient
hcorr_abs.GetXaxis().SetLabelSize(0.01)
hcorr_abs.GetYaxis().SetLabelSize(0.01)

#doesn't work
#palette = TPaletteAxis( hcorr_abs.GetListOfFunctions().FindObject("palette") )
#palette.SetLabelSize(0.05)
#hcorr_abs.GetListOfFunctions().FindObject("palette").SetLabelSize(0.05)
#hcorr.SetMarkerSize(0.7)

c = MakeCanvas()

hcorr_abs.SetStats(0)
hcorr_abs.Draw( "colz" )
DrawGrid( hcorr )
DrawGridObservables( hcorr, nbins_tot )
#hcorr.Draw("text same")

ATLAS_LABEL_custom( 0.05, 0.95, kBlack )
gStyle.SetOptTitle(0)

text = TLatex()
text.SetNDC()
text.SetTextSize(0.04)
#text.DrawLatex(  0.1, 0.87, "Bin-bin Correlation strength" )

phasespace = "Parton level" if phspace == "parton" else "Fiducial"
meas_pretty =  "Absolute" if meas == "Absolute" else "Relative"
text.DrawLatex( 0.05, 0.90, "%s phase-space statistical correlations" % phasespace )
text.DrawLatex( 0.05, 0.85, "%s cross-sections" % (meas_pretty) )
hcorr_abs.GetXaxis().SetTickLength(0)
hcorr_abs.GetYaxis().SetTickLength(0)

hcorr_abs.GetXaxis().SetLabelSize(0.006)
hcorr_abs.GetYaxis().SetLabelSize(0.006)


gPad.RedrawAxis()
#palette1=hcorr_abs.GetListOfFunctions().FindObject("palette")
#print palette1
#palette1.SetX2NDC(0.92);

for ext in [ "pdf", "png", "C" ]:
   c.SaveAs( OutputPath+"/root/bootstrap_dataStat/"+meas_pretty+"_bootstrap_correlations_PartonLevel.%s" % (ext) )
