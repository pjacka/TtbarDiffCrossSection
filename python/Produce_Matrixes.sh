#!/bin/bash

cd ${TTBAR_PATH}/TtbarDiffCrossSection/python/

./compile.sh

python BootstrapCorrelations.pyc abs
python BootstrapCorrelations.pyc rel

python PlotBootstrapCorrelationMatrix.pyc abs
python PlotBootstrapCorrelationMatrix.pyc rel 
