#!/usr/bin/env python

import ROOT
import os

def processDirectory(filelist_path, output_file):
  dir_name = os.path.basename(os.path.normpath(filelist_path))
  dir_name = dir_name[:dir_name.rfind('.')].lower()


  root_files = []

  for filename in os.listdir(filelist_path):
    if filename.endswith(".txt"):
      with open(os.path.join(filelist_path, filename), 'r') as file:
        root_files.extend(line.strip() for line in file)

  histograms = {}
  for root_file in root_files:
    file = ROOT.TFile.Open(root_file)
    for key in file.GetListOfKeys():
      hist_name = key.GetName()
      if hist_name.startswith("CutBookkeeper_"):
        hist = file.Get(hist_name)
        if hist_name in histograms:
          histograms[hist_name].Add(hist)
        else:
          histograms[hist_name] = hist.Clone()
          histograms[hist_name].SetDirectory(0)
    file.Close()

  output_dir = output_file.mkdir(dir_name)
  output_dir.cd()

  for hist in histograms.values():
    hist_name = hist.GetName()
    print(f"{hist_name}: sumweights: {hist.GetBinContent(2)}")
    suffix = ""
    if "GEN" in hist_name:
        suffix = "GEN" + hist_name.split("GEN")[-1]
    elif "NOSYS" in hist_name:
        suffix = "NOSYS"
    else:
        raise ValueError(f"Unknown suffix for {hist_name}")
    new_name = f"sum_weights_{suffix}"
    hist.SetName(new_name)
    hist.Write()


filelists = os.path.join("TtbarDiffCrossSection", "filelists")
directories = [os.path.join(filelists, d) for d in os.listdir(filelists) if os.path.isdir(os.path.join(filelists, d)) and d.startswith("MC23")]

output_dir = os.getenv("TtbarDiffCrossSection_output_path")

output_file = ROOT.TFile(os.path.join(output_dir, "sumWeights.root"), "RECREATE")

for directory in directories:
  print("Processing " + directory)
  processDirectory(directory, output_file)

output_file.Close()


