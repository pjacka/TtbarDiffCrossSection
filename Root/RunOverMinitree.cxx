#include "TtbarDiffCrossSection/RunOverMinitree.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "TtbarDiffCrossSection/TTbarToolsLoaderBoostedAllhad.h"
#include "TtbarDiffCrossSection/TTbarToolsLoaderPartonLevel.h"
#include "TtbarDiffCrossSection/MeasuredVariables.h"
#include "TtbarDiffCrossSection/RecoLevelVariables.h"
#include "NtuplesAnalysisToolsCore/SystematicBranchesFilterCommon.h"
#include "NtuplesAnalysisToolsCore/StressTestReweightingTool.h"
#include "NtuplesAnalysisToolsCore/SampleMetadata.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "PMGTools/PMGCrossSectionTool.h"
#include "Math/VectorUtil.h"
#include "TopEvent/EventTools.h"
#include <nlohmann/json.hpp> // nlohmann::json classes to parse json files
#include <format>

ClassImp(RunOverMinitree)

    RunOverMinitree::RunOverMinitree(const TString &filelistname, const TString &outputName, const TString &configName, bool doDetSys, bool doSysPDF, bool doSysAlphaSFSR, bool doSysIFSR, bool doReweighting, int doBootstrap, int nEvents, int debugLevel) : m_debug(debugLevel),
                                                                                                                                                                                                                                                               m_filelistName(filelistname),
                                                                                                                                                                                                                                                               m_outputName(outputName),
                                                                                                                                                                                                                                                               m_nEvents(nEvents),
                                                                                                                                                                                                                                                               m_configName(configName),
                                                                                                                                                                                                                                                               m_doDetSys(doDetSys),
                                                                                                                                                                                                                                                               m_doSysPDF(doSysPDF),
                                                                                                                                                                                                                                                               m_doReweighting(doReweighting),
                                                                                                                                                                                                                                                               m_nBootstrapPseudoexperiments(doBootstrap)
{
  if (m_debug > 0)
    std::cout << std::endl
              << "****** RunOverMinitree::RunOverMinitree *******" << std::endl;
  m_doOtherGenSys = doSysAlphaSFSR || doSysIFSR;
}

// ---------------------------------------------------------------------

void RunOverMinitree::initialize()
{
  if (m_debug > 0)
    std::cout << std::endl
              << " ***** RunOverMinitree::Initialise() *****" << std::endl;

  m_ttbarPath = gSystem->Getenv("TTBAR_PATH");

  this->readConfig(m_configName);

  m_output = new TFile(m_outputName, "RECREATE", "All histos", 1);

  m_doBootstrap = m_nBootstrapPseudoexperiments > 0;

  string s = m_outputName.Data();
  std::size_t found = s.find_last_of("/");
  if (m_debug > 1)
  {
    std::cout << " path: " << s.substr(0, found + 1) << '\n';
    std::cout << " file: " << s.substr(found + 1) << '\n';
  }
  string str = s.substr(0, found);
  found = str.find_last_of("/");

  m_random = std::make_unique<TRandom3>();

  m_runAllEvents = (m_nEvents < 0);

  this->loadTrees();

  if (m_isData)
  {
    if (m_debug > 1)
      std::cout << "the input file is Data" << std::endl;
    // m_doSys    = false;  // not doing systematics for data
    m_doSysPDF = false;
    m_doOtherGenSys = false;
    m_doReweighting = false;
  }
  else if (m_debug > 1)
  {
    std::cout << "the input file is not Data" << std::endl;
  }

  if (m_debug > 0)
  {
    std::cout << "m_doDetSys: " << m_doDetSys << std::endl;
    std::cout << "m_doSysPDF: " << m_doSysPDF << std::endl;
    std::cout << "m_doOtherGenSys: " << m_doOtherGenSys << std::endl;
    std::cout << "m_doReweighting: " << m_doReweighting << std::endl;
  }

  int nEventsInChainParton = m_chain_parton->GetEntries();
  m_nEvents_parton = m_nEvents;
  if (m_nEvents < 0 || m_nEvents > nEventsInChainParton)
    m_nEvents_parton = nEventsInChainParton;
  int nEventsInChainParticle = m_chain_particle->GetEntries();
  m_nEvents_particle = m_nEvents;
  if (m_nEvents < 0 || m_nEvents > nEventsInChainParticle)
    m_nEvents_particle = nEventsInChainParticle;

  m_output->cd();
}

//----------------------------------------------------------------------

void RunOverMinitree::readConfig(const TString &configName)
{
  if (m_debug > 0)
    std::cout << std::endl
              << " ***** RunOverMinitree::ReadConfig: Parameters read from config: *****" << std::endl;

  if (m_debug > 1)
    std::cout << "Looking for config file with name: " << configName << "." << std::endl;

  m_configName = configFunctions::resolvePath(configName.Data()).c_str();

  if (m_debug > 0)
    std::cout << "RunOverMinitree::ReadConfig: Loading settings from: " << m_configName << std::endl;

  m_ttbarConfig = std::make_shared<const NtuplesAnalysisToolsConfig>(m_configName);

  if (m_debug > 0)
    std::cout << "Getting dsid from filename: " << std::endl;
  m_channelNumber = functions::getDSID(m_filelistName.Data());
  if (m_debug > 0)
    std::cout << "Channel number (DSID): " << m_channelNumber << std::endl;

  m_useEWSF = m_ttbarConfig->useEWSF();
  m_useTruth = m_ttbarConfig->useTruth();

  m_measuredVariablesReco = std::make_unique<MeasuredVariables>();
  m_measuredVariablesParticle = std::make_unique<MeasuredVariables>();
  m_measuredVariablesParton = std::make_unique<MeasuredVariables>();

  m_measuredVariablesReco->setDebugLevel(m_debug);
  m_measuredVariablesParticle->setDebugLevel(m_debug);
  m_measuredVariablesParton->setDebugLevel(m_debug);

  RecoLevelVariables *recoLevelVariables = new RecoLevelVariables();
  recoLevelVariables->setLargeRJetType(m_ttbarConfig->largeRJetType());
  recoLevelVariables->setDebugLevel(m_debug);
  m_recoLevelVariables = std::unique_ptr<MeasuredVariablesBase>{recoLevelVariables};

  m_useRCJetsSubstructure = m_ttbarConfig->useRCJetsSubstructure();

  m_signalDSIDs = m_ttbarConfig->signalDSIDs();
  if (m_signalDSIDs.size() == 0)
  {
    std::cout << "Error: Signal dsids not set" << std::endl;
    exit(-1);
  }

  m_applyHtFilterCut = m_ttbarConfig->applyHtFilterCut();
  m_HtFilterCutValue = m_ttbarConfig->HtFilterCutValue();
  m_samplesToApplyHtFilterCut = m_ttbarConfig->samplesToApplyHtFilterCut();

  if (m_debug > 0)
  {
    std::cout << "Running with large-R jet type " << m_ttbarConfig->largeRJetType() << std::endl;
    std::cout << "Top-tagger name: " << m_ttbarConfig->topTaggerName() << std::endl;
    std::cout << "Running with event selection:" << std::endl;
    std::cout << "m_debug: " << m_debug << std::endl;
    std::cout << "m_useEWSF: " << m_useEWSF << std::endl;
    std::cout << "m_useTruth: " << m_useTruth << std::endl;
  }

  if (m_doReweighting)
  {

    const std::string &configStressTest = configFunctions::resolvePath(m_ttbarConfig->stressTestReweightingConfig().Data());
    std::cout << "Loading stress test config from: " << configStressTest << std::endl;

    const nlohmann::json reweightingJSON = nlohmann::json::parse(
        std::ifstream(configStressTest));
    m_reweightingIDs.clear();
    for (auto &item : reweightingJSON.items())
    {
      m_reweightingIDs.push_back(item.key().c_str());
    }
  }

  if (m_debug > 0)
    std::cout << "Initializing sampleXsection" << std::endl;
  m_sampleCrossSectionTool = std::make_unique<PMGTools::PMGCrossSectionTool>();
  m_sampleCrossSectionTool->readInfosFromFiles(std::vector<std::string>{m_ttbarConfig->samplesXsectionsFile().Data()});
  top::check(m_sampleCrossSectionTool->initialize(), "RunOverMinitree: Failed to initialize sample cross-section tool");

  if (m_debug > 0)
    std::cout << "Initializing NNLO reweighter" << std::endl;
  int sampleID = 0;
  if (m_channelNumber == 601237)
    sampleID = 0;

  m_NNLOReweighter = std::make_unique<TTbarNNLORecursiveRew>(sampleID, /*systVar =*/0, /* type = */ 2, /*autoInit = */ false);
  m_NNLOReweighter->SetInputDirectory((m_ttbarPath + "/TTbarNNLOReweighter/data/data_3d").Data());
  m_NNLOReweighter->Init();

  initializeTools();
}

//----------------------------------------------------------------------

void RunOverMinitree::initializeTools()
{

  // JSON config containing configuration of all tools
  const nlohmann::json &toolsSettings = m_ttbarConfig->eventToolsSettingsJSON();

  if (m_debug > 0)
    std::cout << "Initializing systematic branches filter" << std::endl;

  const nlohmann::json &systematicBranchesFilterConfig = jsonFunctions::readValueJSON<nlohmann::json>(toolsSettings, "SystematicBranchesFilter");

  const std::string systematicBranchesFilterName = jsonFunctions::readValueJSON<std::string>(systematicBranchesFilterConfig, "FilterName");
  if (systematicBranchesFilterName == "SystematicBranchesFilterCommon")
    m_sysBranchesFilter = std::make_unique<SystematicBranchesFilterCommon>("Systematic branches filter");
  else
    throw std::runtime_error("Unknown systematic branches filter name. Check your config.");

  top::check(m_sysBranchesFilter->setProperty("doDetSys", m_doDetSys), "RunOverMinitree: Failed to initialize systematic branches filter");
  top::check(m_sysBranchesFilter->setProperty("doSysPDF", m_doSysPDF), "RunOverMinitree: Failed to initialize systematic branches filter");
  top::check(m_sysBranchesFilter->setProperty("doOtherGenSys", m_doOtherGenSys), "RunOverMinitree: Failed to initialize systematic branches filter");

  const nlohmann::json &systematicBranchesFilterSettings = jsonFunctions::readValueJSON<nlohmann::json>(systematicBranchesFilterConfig, "FilterSettings");
  top::check(m_sysBranchesFilter->readJSONConfig(systematicBranchesFilterSettings), "RunOverMinitree: Failed to initialize systematic branches filter");
  top::check(m_sysBranchesFilter->initialize(), "RunOverMinitree: Failed to initialize systematic branches filter");

  // Preparing reco level tools loader
  const nlohmann::json &recoLevelToolsConfig = jsonFunctions::readValueJSON<nlohmann::json>(toolsSettings, "recoLevelTools");
  const std::string recoToolsLoaderName = jsonFunctions::readValueJSON<std::string>(recoLevelToolsConfig, "toolsLoaderName");
  if (recoToolsLoaderName == "ToolsLoaderBoostedAllhad")
  {
    m_toolsLoaderReco = std::make_unique<TTbarToolsLoaderBoostedAllhad>("ToolsLoaderReco");
  }
  else
  {
    throw std::runtime_error("Unknown reco level tools loader name. Check your config.");
  }

  top::check(m_toolsLoaderReco->setProperty("debugLevel", m_debug), "Failed to setup reco level reconstruction tool");
  // top::check(m_toolsLoaderReco->setProperty("isReco",true),"Failed to setup reco level reconstruction tool");
  // top::check(m_toolsLoaderReco->setProperty("configString",m_ttbarConfig->recoLevelToolsLoaderConfigString()),"Failed to setup reco level reconstruction tool");
  top::check(m_toolsLoaderReco->readJSONConfig(recoLevelToolsConfig), "Failed to read reco level tools reconstruction config"); // Reading configuration from json
  top::check(m_toolsLoaderReco->initialize(), "Failed to initialize reco level reconstruction tool");
  m_toolsLoaderReco->print();

  // Preparing particle level tools loader
  const nlohmann::json &particleLevelToolsConfig = jsonFunctions::readValueJSON<nlohmann::json>(toolsSettings, "particleLevelTools");
  const std::string particleToolsLoaderName = jsonFunctions::readValueJSON<std::string>(particleLevelToolsConfig, "toolsLoaderName");
  if (particleToolsLoaderName == "ToolsLoaderBoostedAllhad")
  {
    m_toolsLoaderParticle = std::make_unique<TTbarToolsLoaderBoostedAllhad>("ToolsLoaderParticle");
  }
  else
  {
    throw std::runtime_error("Unknown particle level tools loader name. Check your config.");
  }

  top::check(m_toolsLoaderParticle->setProperty("debugLevel", m_debug), "Failed to setup particle level reconstruction tool");
  // top::check(m_toolsLoaderParticle->setProperty("isReco",false),"Failed to setup particle level reconstruction tool");
  top::check(m_toolsLoaderParticle->readJSONConfig(particleLevelToolsConfig), "Failed to read particle level tools reconstruction config"); // Reading configuration from json
  top::check(m_toolsLoaderParticle->initialize(), "Failed to initialize particle level reconstruction tool");
  m_toolsLoaderParticle->print();

  // Preparing parton level tools loader
  const nlohmann::json &partonLevelToolsConfig = jsonFunctions::readValueJSON<nlohmann::json>(toolsSettings, "partonLevelTools");
  const std::string partonToolsLoaderName = jsonFunctions::readValueJSON<std::string>(partonLevelToolsConfig, "toolsLoaderName");
  if (partonToolsLoaderName == "PartonLevelToolsLoader")
  {
    m_toolsLoaderParton = std::make_unique<TTbarToolsLoaderPartonLevel>("ToolsLoaderParton");
  }
  else
  {
    throw std::runtime_error("Unknown parton level tools loader name. Check your config.");
  }

  top::check(m_toolsLoaderParton->setProperty("debugLevel", m_debug), "Failed to setup parton level reconstruction tool");
  top::check(m_toolsLoaderParton->readJSONConfig(partonLevelToolsConfig), "Failed to read parton level tools reconstruction config"); // Reading configuration from json
  top::check(m_toolsLoaderParton->initialize(), "Failed to initialize parton level reconstruction tool");
  m_toolsLoaderParton->print();
}

//----------------------------------------------------------------------

void RunOverMinitree::readMetadata(TFile &f)
{

  m_toolsLoaderReco->eventReaderTool()->readMetadata(f);
  auto hist_sys_names = std::unique_ptr<TH1D>{(TH1D *)f.Get("listOfSystematics")};
  for (int isys = 1; isys <= hist_sys_names->GetXaxis()->GetNbins(); isys++)
  {
    m_sys_names.push_back(hist_sys_names->GetXaxis()->GetBinLabel(isys));
  }
  // Ensuring that nominal is the first element in the vector
  auto it = std::find(m_sys_names.begin(), m_sys_names.end(), m_ttbarConfig->nominalFolderName().c_str());
  if (it != m_sys_names.end())
    m_sys_names.erase(it);
  m_sys_names.insert(m_sys_names.begin(), m_ttbarConfig->nominalFolderName().c_str());

  m_toolsLoaderParticle->eventReaderTool()->readMetadata(f);
  m_toolsLoaderParton->eventReaderTool()->readMetadata(f);

  const SampleMetadata &metadata = m_toolsLoaderReco->eventReaderTool()->getMetadata();

  m_isData = (metadata.getDataType() == "data");
  m_channelNumber = metadata.getDSID();
}

//----------------------------------------------------------------------

void RunOverMinitree::loadTrees()
{
  if (m_debug > 0)
    std::cout << std::endl
              << " ***** RunOverMinitree::LoadTrees *****" << std::endl;
  // load the filelist
  vector<string> fileList;
  ifstream mystream(m_filelistName.Data());
  if (m_debug > 0)
    std::cout << "Reading filelist " << m_filelistName << std::endl;
  TString str_item;
  while (mystream >> str_item)
  {
    if (m_debug > 0)
      std::cout << "Adding file " << str_item.Data() << "  to the list of files" << std::endl;
    fileList.push_back(str_item.Data());
  }
  mystream.close();

  const size_t fileListSize = fileList.size();

  TString prefix_for_hist = "passed_preselection_LJETS_2015/";

  m_treeName = "reco";

  if (m_debug > 0)
    std::cout << "Name of reco TTree: " << m_treeName << std::endl;

  m_chain_nominal = new TChain();
  m_chain_parton = new TChain();
  m_chain_particle = new TChain();

  m_sumWeights_nominal = 0.;
  m_nEventsInFilelist = 0.;

  if (m_debug > -1)
    std::cout << "Opening files in full filelist." << std::endl;

  m_sys_names = {};

  vector<TFile *> files;
  for (size_t iFile = 0; iFile < fileListSize; iFile++)
  {

    if ((m_debug > -1) && (iFile + 1) % 100 == 0)
      std::cout << iFile + 1 << "/" << fileListSize << " files opened." << std::endl;

    const TString filename = fileList[iFile].c_str();

    if (m_debug > 0)
      std::cout << "Opening file: " << filename << std::endl;
    TFile *file = TFile::Open(filename);
    if (m_debug > 0)
      std::cout << "File opened." << std::endl;

    if (!file || file->IsZombie())
    {
      std::cout << "Error: file is Zombie!" << std::endl;
      std::cout << "Exiting now!" << std::endl;
      exit(EXIT_FAILURE);
    }

    if (iFile == 0)
    {
      this->readMetadata(*file);
      this->filterSystematicVariations();
    }

    files.push_back(file);

    if (m_isData)
    {
      m_nEventsInFilelist++;
    }
    else
    {
      const std::string &sumWeightsKey = m_toolsLoaderReco->eventReaderTool()->getMetadata().getSumWeightsHistogramKey();

      std::unique_ptr<TH1D> h_sumWeightsInFile{(TH1D *)file->Get((sumWeightsKey + "_" + "NOSYS").c_str())};
      m_nEventsInFilelist += h_sumWeightsInFile->GetBinContent(2);
      std::cout << h_sumWeightsInFile->GetName() << ": " << h_sumWeightsInFile->GetBinContent(2) << std::endl;

      if (m_debug > 1)
        std::cout << "getting histogram fullCutFlow_genWeights" << std::endl;
      TH1D *noWeights = (TH1D *)file->Get("cflow_EventInfo__ljets_NOSYS");

      if (m_debug > 1)
        std::cout << "adding this histogram for this file" << std::endl;
      if (iFile == 0)
      {
        if (noWeights)
        {
          m_hist_noWeights_all = (TH1D *)noWeights->Clone();
          m_hist_noWeights_all->Reset();
        }
      }

      if (m_debug > 1)
        std::cout << "the histogram object already created. Using TH1:Add" << std::endl;
      if (noWeights)
        m_hist_noWeights_all->Add(noWeights);
    }

    if (m_debug > 1)
      std::cout << "adding this file to nominal physics chain" << std::endl;
    m_chain_nominal->AddFile(filename, TChain::kBigNumber, m_treeName);

    if (m_isData)
      continue;
    if (file->GetListOfKeys()->Contains("truth"))
      m_chain_parton->AddFile(filename, TChain::kBigNumber, "truth");
    if (file->GetListOfKeys()->Contains("particleLevel"))
      m_chain_particle->AddFile(filename, TChain::kBigNumber, "particleLevel");
  }

  if (m_debug > -1)
    std::cout << "All files opened." << std::endl;

  this->addSystematicVariations();

  if (m_debug > 0)
    std::cout << "Done adding files to chain." << std::endl;

  if (m_isData)
    return;

  if (m_debug > 0)
  {
    std::cout << "Number of chains: " << m_chains_reco.size() << std::endl
              << "Number of selected systematic names:     " << m_sys_names.size() << std::endl;
  }
}

//----------------------------------------------------------------------

void RunOverMinitree::execute()
{

  for (size_t ichain = 0; ichain < m_sys_names.size(); ichain++)
  {
    const TString &sys_name = m_sys_names[ichain];
    if (m_debug > 0)
      std::cout << std::endl
                << "Running over TTree: " << sys_name << std::endl
                << "Progress: " << ichain + 1 << "/" << m_sys_names.size() << std::endl;

    this->executeSystematicVariation(sys_name);
  }
}

// ----------------------------------------------------------------------

RunOverMinitree::~RunOverMinitree()
{
  delete m_output;
}

// ---------------------------------------------------------------------

void RunOverMinitree::filterSystematicVariations()
{
  std::vector<TString> sysNamesFilteredGen, sysNamesFilteredDet;
  for (const TString &sysName : m_sys_names) {
    if (m_sysBranchesFilter->passFilter(sysName.Data()))
    {
      if (m_sysBranchesFilter->isDetectorSystematic(sysName.Data()))
        sysNamesFilteredDet.push_back(sysName);
      else
        sysNamesFilteredGen.push_back(sysName);
    }
  }

  for (const std::string& sysName : m_sysBranchesFilter->getAdditionalBranches())
    sysNamesFilteredDet.push_back(sysName.c_str());

  m_sys_names = sysNamesFilteredDet;
  m_sys_names.insert(std::end(m_sys_names), std::begin(sysNamesFilteredGen), std::end(sysNamesFilteredGen));
}

// ---------------------------------------------------------------------

void RunOverMinitree::addSystematicVariations()
{
  for (const TString &sysName : m_sys_names)
  {
    std::cout << "Adding systematic variation: " << sysName << std::endl;
    addSystematicVariation(sysName);
  }
}

//----------------------------------------------------------------------

void RunOverMinitree::addSystematicVariation(const TString &sysName)
{
  if (m_sysBranchesFilter->isGeneratorSystematic(sysName.Data()))
    addGeneratorSystematicVariation(sysName);
  else
    addDetectorSystematicVariation(sysName);
}

//----------------------------------------------------------------------

void RunOverMinitree::addDetectorSystematicVariation(const TString &sysName)
{
  m_chains_reco[sysName] = m_chain_nominal;
  if (sysName == "NOSYS")
  {
    m_chains_particle[sysName] = m_chain_particle;
    m_chains_parton[sysName] = m_chain_parton;
  }
}

//----------------------------------------------------------------------

void RunOverMinitree::addGeneratorSystematicVariation(const TString &sysName)
{
  m_chains_reco[sysName] = m_chain_nominal;
  m_chains_particle[sysName] = m_chain_particle;
  m_chains_parton[sysName] = m_chain_parton;
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::executeSystematicVariation(const TString &sys_name)
{
  if (m_debug > 0)
    std::cout << std::endl
              << " ***** RunOverMinitree::Execute() for systematic type: " << sys_name << " *****" << std::endl;
  m_sysName = sys_name;

  m_isNominal = (m_sysName == "NOSYS") || (m_sysName == "nnlo_rew");
  m_isPDFChain = m_sysBranchesFilter->isPDFSystematic(m_sysName.Data());
  m_isOtherGenChain = m_sysBranchesFilter->isOtherGenSystematic(m_sysName.Data());
  m_isReweightingChain = (std::find(
                              std::begin(m_reweightingIDs),
                              std::end(m_reweightingIDs),
                              sys_name) != std::end(m_reweightingIDs));

  if (m_ttbarConfig->runReco())
    initializeRecoChain(sys_name);

  if (m_ttbarConfig->runParticle() && m_chains_particle.count(sys_name) > 0)
    initializeParticleChain(sys_name);

  if (m_ttbarConfig->runParton() && m_chains_parton.count(sys_name) > 0)
    initializePartonChain(sys_name);

  extractInformationAboutSampleFromMetadata(m_toolsLoaderReco->eventReaderTool()->getMetadata());

  if (m_isReweightingChain)
  {
    m_stressTestReweightingTool = std::make_unique<StressTestReweightingTool>((std::string) "Reweighting tool: " + sys_name.Data());
    top::check(
        m_stressTestReweightingTool->setProperty(
            "configPath",
            configFunctions::resolvePath(m_ttbarConfig->stressTestReweightingConfig().Data())),
        "StressTestReweightingTool: Failed to set property: configPath");
    top::check(
        m_stressTestReweightingTool->setProperty(
            "variables",
            (const MeasuredVariablesBase *)m_measuredVariablesParton.get()),
        "StressTestReweightingTool: Failed to set property: variables");
    top::check(
        m_stressTestReweightingTool->setProperty(
            "reweightingID",
            sys_name.Data()),
        "StressTestReweightingTool: Failed to set property: reweightingID");
    top::check(
        m_stressTestReweightingTool->initialize(),
        "StressTestReweightingTool: Failed to set initialize");
  }

  // Histograms should be initialized after all tools are initialized to prevent issues when saving histograms
  this->initializeHistogramManagers();

  if (m_debug > 0)
  {
    std::cout << std::format(
        R"(Starting event loops:
Is data: {}
Weighted events in filelist: {}
Normalized cross-section: {}
Total number of weighted events: {}
)",
        m_isData, m_nEventsInFilelist, m_crossSectionWithKFactorAndFilterEfficiency, m_sumWeights_nominal);
  }

  if (m_createTruthLevelHistos)
  {
    if (m_ttbarConfig->runParton())
      this->loopPartonLevel();
    if (m_ttbarConfig->runParticle()) // Must be after parton level loop
      this->loopParticleLevel();
  }

  if (m_ttbarConfig->runReco())
    this->loopDetectorLevel();

  this->writeHistograms();
  this->deleteHistograms();
}

//----------------------------------------------------------------------

void RunOverMinitree::initializeRecoChain(const TString &sys_name)
{
  TChain *chain = m_chains_reco[sys_name];
  const int nEventsInChain = chain->GetEntries();

  m_nEvents_reco = m_nEvents;
  if (m_nEvents_reco < 0 || m_nEvents_reco > nEventsInChain)
    m_nEvents_reco = nEventsInChain;
  if (m_debug > 0)
    std::cout << std::endl
              << "nEvents reco tree: " << m_nEvents_reco << std::endl;

  m_toolsLoaderReco->eventReaderTool()->setChain(chain);
  top::check(m_toolsLoaderReco->eventReaderTool()->initialize(), "RunOverMinitree::Execute: Failed to initialize m_toolsLoaderReco->eventReaderTool()!");
  const std::unordered_map<std::string, std::string> chain_settings = {
      {"sys_name", sys_name.Data()}};
  top::check(m_toolsLoaderReco->eventReaderTool()->initChain(chain_settings), "Failed to initialize chain in RecoTreeReader!");
}

//----------------------------------------------------------------------

void RunOverMinitree::initializeParticleChain(const TString &sys_name)
{
  TChain *chain_particle = m_chains_particle[sys_name];
  m_toolsLoaderParticle->eventReaderTool()->setChain(chain_particle);
  top::check(m_toolsLoaderParticle->eventReaderTool()->initialize(), "RunOverMinitree::initializeChain: Failed to initialize m_toolsLoaderReco->eventReaderTool()!");
  const std::unordered_map<std::string, std::string> chain_settings = {
      {"sys_name", sys_name.Data()}};
  top::check(m_toolsLoaderParticle->eventReaderTool()->initChain(chain_settings), "RunOverMinitree::initializeChain: Failed to initialize chain in RecoTreeReader!");
}

//----------------------------------------------------------------------

void RunOverMinitree::initializePartonChain(const TString &sys_name)
{
  TChain *chain_parton = m_chains_parton[sys_name];
  m_toolsLoaderParton->eventReaderTool()->setChain(chain_parton);
  top::check(m_toolsLoaderParton->eventReaderTool()->initialize(), "RunOverMinitree::initializePartonChain: Failed to initialize m_toolsLoaderParton->eventReaderTool()!");
  const std::unordered_map<std::string, std::string> chain_settings = {
      {"sys_name", sys_name.Data()}};
  top::check(m_toolsLoaderParton->eventReaderTool()->initChain(chain_settings), "RunOverMinitree::initializePartonChain: Failed to initialize chain in RecoTreeReader!");
}

//----------------------------------------------------------------------

double RunOverMinitree::loadSumWeights(const std::string &mcProduction, const std::string &dsid, const std::string &suffix)
{

  auto sumWeightsFile = std::unique_ptr<TFile>{TFile::Open(m_ttbarPath + "/" + m_ttbarConfig->sumWeightsFile())};
  const TString histName = (mcProduction + "." + dsid + "/sum_weights_" + suffix).c_str();

  if (m_debug > 1)
    std::cout << "Loading sumWeights histogram: " << histName << std::endl;
  auto sumWeightsHistogram = std::unique_ptr<TH1D>{(TH1D *)sumWeightsFile->Get(histName)};

  sumWeightsHistogram->SetDirectory(0);
  double sumWeights = sumWeightsHistogram->GetBinContent(2);

  if (m_channelNumber >= 361020 && m_channelNumber <= 361032)
  { // Special normalization for dijet samples
    sumWeights = sumWeightsHistogram->GetBinContent(1);
  }
  sumWeightsFile->Close();
  return sumWeights;
}

//----------------------------------------------------------------------

double RunOverMinitree::getSampleCrossSectionWithKFactorAndFilterEfficiency(const int dsid)
{

  const double crossSectionWithKFactor = m_sampleCrossSectionTool->getSampleXsection(dsid);
  if (m_debug > 0)
  {
    const double amiCrossSection = m_sampleCrossSectionTool->getAMIXsection(dsid);
    const double kFactor = m_sampleCrossSectionTool->getKfactor(dsid);
    const double filterEfficiency = m_sampleCrossSectionTool->getFilterEff(dsid);
    std::cout << "Sample cross-section from PMG database:" << std::endl
              << "  Raw cross-section: " << amiCrossSection << std::endl
              << "  K-factor: " << kFactor << std::endl
              << "  filter efficiency: " << filterEfficiency << std::endl
              << "  Cross-section with K-factor: " << amiCrossSection * kFactor << std::endl
              << "  Cross-section with K-factor and filter efficiency: " << crossSectionWithKFactor << std::endl;
  }
  return crossSectionWithKFactor;
}

//----------------------------------------------------------------------

void RunOverMinitree::extractInformationAboutSampleFromMetadata(const SampleMetadata &metadata)
{

  m_channelNumber = metadata.getDSID();
  m_crossSectionWithKFactorAndFilterEfficiency = 1;
  m_isSignal = false;
  if (!m_isData)
  {
    m_crossSectionWithKFactorAndFilterEfficiency = getSampleCrossSectionWithKFactorAndFilterEfficiency(m_channelNumber);
    m_sumWeights_nominal = loadSumWeights(metadata.getMCProduction(), std::to_string(metadata.getDSID()), "NOSYS");
    if (m_sysBranchesFilter->isGeneratorSystematic(m_sysName.Data()))
      m_sumWeights_sys = loadSumWeights(metadata.getMCProduction(), std::to_string(metadata.getDSID()), m_sysName.Data());
    else
      m_sumWeights_sys = m_sumWeights_nominal;
    nlohmann::json luminositySettings = jsonFunctions::readValueJSON<nlohmann::json>(m_ttbarConfig->getJSON(), "luminositySettings");
    m_lumi = configFunctions::getLumi(luminositySettings, metadata.getMCProduction()) * 1000;
    const double lumiUnc = configFunctions::getLumiUnc(luminositySettings, metadata.getMCProduction());

    m_isSignal = (std::find(m_signalDSIDs.begin(), m_signalDSIDs.end(), m_channelNumber) != m_signalDSIDs.end());

    if (m_doOtherGenSys && (!m_isSignal))
    {
      std::cout << "Error: Other gen systematics switched on but sample is not on the list of signal samples!" << std::endl;
      std::cout << "Exiting now!" << std::endl;
      exit(-1);
    }

    if (m_applyHtFilterCut)
    {
      m_applyHtFilterCut = std::find(m_samplesToApplyHtFilterCut.begin(), m_samplesToApplyHtFilterCut.end(), m_channelNumber) != m_samplesToApplyHtFilterCut.end();
      if (m_applyHtFilterCut)
      {
        if (m_debug > 0)
          std::cout << "Using HtFilter cut: Ht < " << m_HtFilterCutValue << " [GeV]." << std::endl;
      }
    }

    // Setting up reweighting tool for the reco level tree processing
    auto initReweightingTool = [this, &lumiUnc](EventReweightingToolBase &reweighting_tool)
    {
      top::check(reweighting_tool.setProperty("lumi", m_lumi), "Failed to setup reweighting tool: lumi");
      top::check(reweighting_tool.setProperty("xsecWithKfactor", m_crossSectionWithKFactorAndFilterEfficiency), "Failed to setup reweighting tool: cross-section");
      top::check(reweighting_tool.setProperty("sumWeights", m_sumWeights_sys), "Failed to setup reweighting tool: sumGenWeights");
      top::check(reweighting_tool.initialize(), "Failed to initialize reweighting tool");
    };
    initReweightingTool(*m_toolsLoaderReco->reweightingTool());

    if (m_isSignal)
    {
      if (m_ttbarConfig->runParticle())
        initReweightingTool(*m_toolsLoaderParticle->reweightingTool());
      if (m_ttbarConfig->runParton())
        initReweightingTool(*m_toolsLoaderParton->reweightingTool());
    }
  }
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::initializeHistogramManagers()
{
  m_createTruthLevelHistos = false;
  m_createMigrationMatrices = false;
  if (m_useTruth && m_isSignal && (m_isNominal || m_isPDFChain || m_isOtherGenChain || m_isReweightingChain))
    m_createTruthLevelHistos = true;

  if (m_useTruth && m_isSignal)
    m_createMigrationMatrices = true;
  bool fillClosureTestHistos = (m_useTruth && m_isSignal && m_isNominal && m_ttbarConfig->fillClosureTestHistos());

  m_output->cd();
  m_output->mkdir(m_sysName);
  m_output->cd(m_sysName);

  m_h_cutflow = m_toolsLoaderReco->selectionTool()->initCutflowHistogram("h_cutflow");
  m_h_cutflow_noweights = m_toolsLoaderReco->selectionTool()->initCutflowHistogram("h_cutflow_noweights");
  m_h_cutflow_particle = m_toolsLoaderParticle->selectionTool()->initCutflowHistogram("h_cutflow_particle");
  m_h_cutflow_noweights_particle = m_toolsLoaderParticle->selectionTool()->initCutflowHistogram("h_cutflow_noweights_particle");
  m_h_cutflow_particle_after_reco = m_toolsLoaderParticle->selectionTool()->initCutflowHistogram("h_cutflow_particle_after_reco");
  m_h_cutflow_noweights_particle_after_reco = m_toolsLoaderParticle->selectionTool()->initCutflowHistogram("h_cutflow_noweights_particle_after_reco");
  m_h_cutflow_parton = m_toolsLoaderParton->selectionTool()->initCutflowHistogram("h_cutflow_parton");
  m_h_cutflow_noweights_parton = m_toolsLoaderParton->selectionTool()->initCutflowHistogram("h_cutflow_noweights_parton");

  top::check(m_toolsLoaderReco->selectionTool()->setProperty("cutflow", m_h_cutflow), "Failed to setup reco level event selection tool");
  top::check(m_toolsLoaderReco->selectionTool()->setProperty("cutflowNoWeights", m_h_cutflow_noweights), "Failed to setup reco level event selection tool");
  top::check(m_toolsLoaderParton->selectionTool()->setProperty("cutflow", m_h_cutflow_parton), "Failed to setup parton level event selection tool");
  top::check(m_toolsLoaderParton->selectionTool()->setProperty("cutflowNoWeights", m_h_cutflow_noweights_parton), "Failed to setup parton level event selection tool");
  top::check(m_toolsLoaderParticle->selectionTool()->setProperty("cutflow", m_h_cutflow_particle), "Failed to setup particle level event selection tool");
  top::check(m_toolsLoaderParticle->selectionTool()->setProperty("cutflowNoWeights", m_h_cutflow_noweights_particle), "Failed to setup particle level event selection tool");

  if (m_isData)
  {
    m_h_cutflow->Fill(1, -1);
    m_h_cutflow_particle_after_reco->Fill(1, -1);
    m_h_cutflow_particle->Fill(1, -1);
  }
  else
  {
    m_h_cutflow->Fill(1, m_crossSectionWithKFactorAndFilterEfficiency * m_lumi * m_nEventsInFilelist / m_sumWeights_sys);
    m_h_cutflow_noweights->Fill(1., m_hist_noWeights_all->GetBinContent(1));
    m_h_cutflow_particle->Fill(1, m_crossSectionWithKFactorAndFilterEfficiency * m_lumi * m_nEventsInFilelist / m_sumWeights_sys);
    m_h_cutflow_noweights_particle->Fill(1., m_hist_noWeights_all->GetBinContent(1));
    m_h_cutflow_parton->Fill(1, m_crossSectionWithKFactorAndFilterEfficiency * m_lumi * m_nEventsInFilelist / m_sumWeights_sys);
    m_h_cutflow_noweights_parton->Fill(1., m_hist_noWeights_all->GetBinContent(1));
    m_h_cutflow_particle_after_reco->Fill(1, m_crossSectionWithKFactorAndFilterEfficiency * m_lumi * m_nEventsInFilelist / m_sumWeights_sys);
    m_h_cutflow_noweights_particle_after_reco->Fill(1., m_hist_noWeights_all->GetBinContent(1));
  }

  const std::vector<std::string> &selectionNames = m_toolsLoaderReco->classificationTool()->getSelectionNames();
  const size_t index_signal_region = m_toolsLoaderReco->classificationTool()->getSignalRegionIndex();

  m_hisMans.resize(selectionNames.size());

  for (size_t isel = 0; isel < selectionNames.size(); ++isel)
  {
    const std::string &selection = selectionNames[isel];
    m_output->mkdir(m_sysName + "/" + selection.c_str());
    m_output->cd(m_sysName + "/" + selection.c_str());

    if (m_debug > 1)
      std::cout << "Creating histogram manager: " << selection << " "
                << m_createMigrationMatrices << " "
                << (m_createMigrationMatrices && (isel == index_signal_region))
                << std::endl;

    m_hisMans[isel] = new HistogramManagerAllHadronic(
        m_ttbarConfig,
        m_createMigrationMatrices && (isel == index_signal_region),
        m_debug);

    if (isel == index_signal_region)
      m_hisMans[isel]->setFillClosureTestHistos(fillClosureTestHistos);

    m_hisMans[isel]->setupBootStrap(m_doBootstrap && m_isNominal, m_nBootstrapPseudoexperiments);
    m_hisMans[isel]->setMeasuredVariablesReco(*m_measuredVariablesReco);
    m_hisMans[isel]->setMeasuredVariablesParticle(*m_measuredVariablesParticle);
    m_hisMans[isel]->setMeasuredVariablesParton(*m_measuredVariablesParton);
    m_hisMans[isel]->setRecoLevelVariables(*m_recoLevelVariables);
    m_hisMans[isel]->constructHistos();
    m_output->cd(m_sysName);
  }
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::loopPartonLevel()
{

  vector<HistogramManagerAllHadronic *> hisMansWithPartonLevel;
  hisMansWithPartonLevel.push_back(m_hisMans[m_toolsLoaderReco->classificationTool()->getSignalRegionIndex()]); // the signal region for tight ABCD16 selections
  int nSelectionsWithPartonLevel = hisMansWithPartonLevel.size();

  const SampleMetadata &metadata = m_toolsLoaderParton->eventReaderTool()->getMetadata();
  m_channelNumber = metadata.getDSID();

  m_partonTreeIndexes.clear();

  int nSelectedEvents_parton = 0;
  if (m_debug > -1)
    std::cout << std::endl
              << "looping in parton TTree" << std::endl;
  for (Long64_t ientry = 0; ientry < m_nEvents_parton; ientry++)
  {
    if (ientry % 50000 == 0)
    {
      TString progress = Form("%3.2f", ientry / float(m_nEvents_parton) * 100);
      if (m_debug > -1)
        std::cout << "--> running over parton TTree: " << ientry << "/" << m_nEvents_parton << " (" << progress.Data() << "%)" << " Selected: " << nSelectedEvents_parton << std::endl;
    }

    m_partonLevel = std::unique_ptr<ReconstructedEvent>{m_toolsLoaderParton->eventReaderTool()->readEntry(ientry)};
    m_toolsLoaderParton->recoTool()->execute(*m_partonLevel);

    m_eventNumber = m_partonLevel->getAttribute<ULong64_t>("EventNumber");
    m_runNumber = m_partonLevel->getAttribute<UInt_t>("RunNumber");

    m_partonTreeIndexes.insertIndex(*m_partonLevel, ientry);

    const Object_t &top = m_partonLevel->getConstRef<Object_t>("top");
    const Object_t &antitop = m_partonLevel->getConstRef<Object_t>("antitop");

    top::check(m_toolsLoaderParton->reweightingTool()->execute(*m_partonLevel), "Failed to execute reweighting tool");
    double weight = m_toolsLoaderParton->reweightingTool()->getWeight();

    if (m_sysName == "nnlo_rew")
    {
      ReconstructedObjects::FourMom_t ttbar = top + antitop;
      weight *= m_NNLOReweighter->GetWeight(
          top.Pt(),
          antitop.Pt(),
          ttbar.M(),
          ttbar.Pt());
    }

    if (!m_toolsLoaderParton->selectionTool()->execute(*m_partonLevel))
    {
      continue;
    }

    nSelectedEvents_parton++;

    m_measuredVariablesParton->calculateMeasuredVariables(*m_partonLevel);

    double weight_reweighting = 1.;
    if (m_isReweightingChain)
    {
      top::check(m_stressTestReweightingTool->execute(*m_partonLevel),
                 "Failed to execute stress test reweighting tool at the parton level");
      weight_reweighting = m_stressTestReweightingTool->getWeight();
    }

    weight *= weight_reweighting;

    for (int isel = 0; isel < nSelectionsWithPartonLevel; ++isel)
    {
      hisMansWithPartonLevel[isel]->setInfo(m_channelNumber, m_eventNumber, m_runNumber);
      hisMansWithPartonLevel[isel]->fillTruthLevelHistos("Parton", weight);
    }
  } // end loop over parton events
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::loopParticleLevel()
{

  m_particleTreeIndexes.clear();

  int nSelectedEvents_particle = 0;
  double weights_checksum = 0.;

  const SampleMetadata &metadata = m_toolsLoaderParticle->eventReaderTool()->getMetadata();
  m_channelNumber = metadata.getDSID();

  if (m_debug > -1)
    std::cout << std::endl
              << "looping in particle TTree" << std::endl;
  for (Long64_t ientry = 0; ientry < m_nEvents_particle; ++ientry)
  {
    if (ientry % 1000 == 0)
    {
      TString progress = Form("%3.2f", ientry / float(m_nEvents_particle) * 100);
      if (m_debug > -1)
        std::cout << "--> running over particle TTree: " << ientry << "/" << m_nEvents_particle << " (" << progress.Data() << "%)" << " Selected: " << nSelectedEvents_particle << std::endl;
    }
    nSelectedEvents_particle++;

    if (m_debug > 2)
      std::cout << "Particle Level: Getting entry." << std::endl;
    m_particleLevel = std::unique_ptr<ReconstructedEvent>{m_toolsLoaderParticle->eventReaderTool()->readEntry(ientry)};
    m_toolsLoaderParticle->recoTool()->execute(*m_particleLevel);

    m_eventNumber = m_particleLevel->getAttribute<ULong64_t>("EventNumber");
    m_runNumber = m_particleLevel->getAttribute<UInt_t>("RunNumber");

    m_particleTreeIndexes.insertIndex(*m_particleLevel, ientry);

    // Checking if we need to load parton level tree
    if (m_isReweightingChain ||
        (m_sysName == "nnlo_rew") ||
        (m_doOtherGenSys && m_isOtherGenChain) ||
        (m_doSysPDF && m_isPDFChain))
    {
      this->prepareMatchedPartonLevel(*m_particleLevel);
      m_measuredVariablesParton->calculateMeasuredVariables(*m_partonLevel);
    }

    // Calculate weight
    top::check(m_toolsLoaderParticle->reweightingTool()->execute(*m_particleLevel), "Failed to execute reweighting tool");
    m_weight = m_toolsLoaderParticle->reweightingTool()->getWeight();

    double weight_reweighting = 1.;

    if (m_isReweightingChain)
    {
      top::check(m_stressTestReweightingTool->execute(*m_partonLevel), "Failed to execute stress test reweighting tool at the particle level");
      weight_reweighting = m_stressTestReweightingTool->getWeight();
    }

    if (m_sysName == "nnlo_rew")
    {
      const Object_t &top = m_partonLevel->getConstRef<Object_t>("top");
      const Object_t &antitop = m_partonLevel->getConstRef<Object_t>("antitop");
      ReconstructedObjects::FourMom_t ttbar = top + antitop;
      weight_reweighting *= m_NNLOReweighter->GetWeight(top.Pt(), antitop.Pt(), ttbar.M(), ttbar.Pt());
    }

    m_weight *= weight_reweighting;

    // Applying event selection
    if (!m_toolsLoaderParticle->selectionTool()->execute(*m_particleLevel))
      continue;

    m_toolsLoaderParticle->classificationTool()->execute(*m_particleLevel);
    const std::vector<int> &selections = m_toolsLoaderParticle->classificationTool()->getSelections();
    const size_t signal_region_index = m_toolsLoaderParticle->classificationTool()->getSignalRegionIndex();

    std::shared_ptr<Object_t> randomTop = m_particleLevel->getPtr<Object_t>("top1");
    if (m_random->Integer(2) == 1)
      randomTop = m_particleLevel->getPtr<Object_t>("top2");
    m_particleLevel->setPtr<Object_t>("topRandom", randomTop);

    if (selections[signal_region_index] == 1)
      weights_checksum += m_weight;

    int sumSelections = std::accumulate(std::begin(selections), std::end(selections), 0);

    if (sumSelections > 0)
      m_measuredVariablesParticle->calculateMeasuredVariables(*m_particleLevel);

    for (size_t isel = 0; isel < selections.size(); ++isel)
    {
      if (selections[isel] == 1)
      {
        m_hisMans[isel]->setInfo(m_channelNumber, m_eventNumber, m_runNumber);
        m_hisMans[isel]->fillTruthLevelHistos("Particle", m_weight);
      }
    }
  }

  if (m_debug > 0)
    std::cout << "Sum of weights in the particle level signal region: " << weights_checksum << std::endl;
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::loopDetectorLevel()
{

  m_skippedEvents = 0;

  // double eventsAfterRecoCutInThisMinitree(0);
  double nSelectedEvents(0);
  double nSelectedEvents_MCweight(0);

  TStopwatch time;
  time.Start();

  m_processedEvents.clear();

  const SampleMetadata &metadata = m_toolsLoaderReco->eventReaderTool()->getMetadata();
  m_channelNumber = metadata.getDSID();

  if (m_debug > -1)
    std::cout << std::endl
              << "looping in reco level TTree" << std::endl
              << "Branch name: " << m_sysName << std::endl;
  for (Long64_t ientry = 0; ientry < m_nEvents_reco; ientry++)
  {
    if (m_debug > 1)
      std::cout << std::endl
                << std::endl
                << "Processing entry: " << ientry << std::endl;

    if (ientry % 1000 == 0)
    {
      TString progress = Form("%3.2f", ientry / float(m_nEvents_reco) * 100);
      if (m_debug > -1)
        std::cout << "--> " << ientry << "/" << m_nEvents_reco << " (" << progress.Data() << "%)" << " Selected: " << (int)nSelectedEvents << std::endl;
    }

    m_recoLevel = std::unique_ptr<ReconstructedEvent>{m_toolsLoaderReco->eventReaderTool()->readEntry(ientry)};
    m_toolsLoaderReco->recoTool()->execute(*m_recoLevel);

    m_eventNumber = m_recoLevel->getAttribute<ULong64_t>("EventNumber");
    m_runNumber = m_recoLevel->getAttribute<UInt_t>("RunNumber");

    bool passedPartonLevel = false;
    bool passedParticleLevel = false;
    bool partonLevelLoaded = false;

    // Setup event weights
    m_weight = 1.;

    // Weights are loaded from the truth tree in these chains
    if (!m_isData && (m_isReweightingChain || (m_sysName == "nnlo_rew") || (m_doOtherGenSys && m_isOtherGenChain) || (m_doSysPDF && m_isPDFChain)))
    {
      passedPartonLevel = this->prepareMatchedPartonLevel(*m_recoLevel);
      m_measuredVariablesParton->calculateMeasuredVariables(*m_partonLevel);
      partonLevelLoaded = true;
    }

    top::check(m_toolsLoaderReco->reweightingTool()->execute(*m_recoLevel), "Failed to execute reweighting tool");
    m_weight = m_toolsLoaderReco->reweightingTool()->getWeight();

    if (!m_isData)
    {
      double weight_reweighting = 1;

      if (m_isReweightingChain)
      {
        top::check(m_stressTestReweightingTool->execute(*m_partonLevel),
                   "Failed to execute stress test reweighting tool at the reco level");
        weight_reweighting = m_stressTestReweightingTool->getWeight();
      }

      if (m_sysName == "nnlo_rew")
      {
        const Object_t &top = m_partonLevel->getConstRef<Object_t>("top");
        const Object_t &antitop = m_partonLevel->getConstRef<Object_t>("antitop");
        ReconstructedObjects::FourMom_t ttbar = top + antitop;
        weight_reweighting *= m_NNLOReweighter->GetWeight(top.Pt(), antitop.Pt(), ttbar.M(), ttbar.Pt());
      }

      if (m_debug > 3)
        std::cout << " reweighting weight: " << weight_reweighting << std::endl;
      this->multiplyEventWeights(weight_reweighting);
    }

    bool passed_selections = m_toolsLoaderReco->selectionTool()->execute(*m_recoLevel);
    if (!passed_selections)
      continue;

    m_toolsLoaderReco->classificationTool()->execute(*m_recoLevel);
    std::vector<int> selections_reco = m_toolsLoaderReco->classificationTool()->getSelections();
    size_t signal_region_index = m_toolsLoaderReco->classificationTool()->getSignalRegionIndex();

    nSelectedEvents++;

    if (!m_isData)
    {
      if (m_createMigrationMatrices && selections_reco[signal_region_index] == 1)
      {
        passedParticleLevel = this->prepareMatchedParticleLevel(*m_recoLevel);
        // m_selections_passed_ABCD16_tight_particle = m_toolsLoaderParticle->classificationTool()->getSelections();
        if (!partonLevelLoaded)
        {
          passedPartonLevel = this->prepareMatchedPartonLevel(*m_recoLevel);
          m_measuredVariablesParton->calculateMeasuredVariables(*m_partonLevel);
          partonLevelLoaded = true;
        }
      }
    } // no isData

    // Event should be always properly defined after selection
    m_recoLevelVariables->calculateLevelVariables(*m_recoLevel);

    if (m_debug > 2)
      std::cout << "the total weight for this event is: " << m_weight << std::endl;

    // Check for duplicate events in all regions. Duplicated events are skipped in data. Job is let to fail if running over MC samples.
    bool passedCheck = checkForDuplicatedEvents();
    if (m_isData && !passedCheck)
    {
      std::cout << "Warning: found duplicated event in data." << std::endl;
      continue;
    }
    if (!m_isData && !passedCheck)
    {
      std::cout << "Error: found duplicated event at reco level." << std::endl;
      ;
      // exit(-1);
    }

    bool passedRecoLevel = true;

    for (size_t isel = 0; isel < selections_reco.size(); ++isel)
    {
      m_hisMans[isel]->setInfo(m_channelNumber, m_eventNumber, m_runNumber);
      m_hisMans[isel]->setWeight(m_weight);

      if (m_debug > 2)
        std::cout << "filling histograms for selection " << isel << std::endl;
      if (selections_reco[isel] == 1)
      {
        bool passed_particle = (passedParticleLevel && m_toolsLoaderParticle->classificationTool()->getSelections().at(isel) == 1);

        calculateMeasuredVariables(passed_particle, passedPartonLevel);

        m_hisMans[isel]->fillEventHistos(
            passedRecoLevel,
            passed_particle,
            passedPartonLevel,
            m_particleMatched,
            m_partonMatched);
        // printout of interesting events in signal region
      }
    }
    if (m_debug > 2)
      std::cout << "end of event" << std::endl;
  } // reco event loop
  if (m_debug > -1)
  {
    std::cout << "Real time in event loop: " << time.RealTime() << std::endl;
  }
  if (m_debug > 0)
  {
    std::cout << "Number of events in the reco minitree: " << m_nEvents_reco << std::endl;
    std::cout << "Selected Events: " << nSelectedEvents << std::endl;
    std::cout << "Skipped Events: " << m_skippedEvents << std::endl;
    std::cout << "Selected Events weighted by MC weight: " << nSelectedEvents_MCweight << std::endl;
    std::cout << "Total number of events used for weighting: " << m_sumWeights_sys << std::endl;
    std::cout << "Cross section with K-factor: " << m_crossSectionWithKFactorAndFilterEfficiency << std::endl;

    // printExtremeValues();
  }
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::writeHistograms()
{

  if (m_debug > 0)
    std::cout << "writing histograms" << std::endl;

  size_t n_selections = m_toolsLoaderReco->classificationTool()->getSelectionNames().size();
  const std::vector<std::string> &selections = m_toolsLoaderReco->classificationTool()->getSelectionNames();

  for (size_t isel = 0; isel < n_selections; ++isel)
  {
    m_hisMans[isel]->writeMeasuredHistos(selections[isel] + "/unfolding/");
    if (m_ttbarConfig->runReco())
      m_hisMans[isel]->writeRecoHistos(selections[isel] + "/RecoLevel/");
  }

  m_output->cd();
  if (m_debug > 0)
    std::cout << "writing histograms to output file" << std::endl;
  m_output->Write();
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::deleteHistograms()
{

  if (m_debug > 0)
    std::cout << "deleting histograms" << std::endl;

  size_t n_selections = m_toolsLoaderReco->classificationTool()->getSelectionNames().size();
  for (size_t isel = 0; isel < n_selections; ++isel)
  {
    if (m_debug > 1)
      std::cout << isel + 1 << "/" << n_selections << std::endl;
    delete m_hisMans[isel];
  }

  if (m_debug > 0)
    std::cout << "deleting cutflow histograms" << std::endl;
  delete m_h_cutflow;
  delete m_h_cutflow_particle;
  delete m_h_cutflow_noweights;
  delete m_h_cutflow_noweights_particle;
  delete m_h_cutflow_particle_after_reco;
  delete m_h_cutflow_noweights_particle_after_reco;
  delete m_h_cutflow_parton;
  delete m_h_cutflow_noweights_parton;

  m_output->Close();
  delete m_output;
  // m_output->ReOpen("UPDATE");
  if (m_debug > 0)
    std::cout << "Reloading ouptut rootfile" << std::endl;
  m_output = TFile::Open(m_outputName, "UPDATE", "All histos", 1);

  if (m_output->IsZombie())
  {
    std::cout << "ERROR: Problem with reloading output rootfile: " << m_output->GetName() << std::endl;
    std::cout << "Exiting now!" << std::endl;
    exit(EXIT_FAILURE);
  }
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::finalize()
{
  if (m_debug > 0)
    std::cout << std::endl
              << " ***** RunOverMinitree::Finalise() *****" << std::endl;

  delete m_chain_nominal;
  delete m_chain_parton;
  delete m_chain_particle;

  if (m_debug > 0)
    std::cout << "Done!" << std::endl
              << std::endl
              << std::endl
              << std::endl;
}

// ------------------------------------------------------------------------------------------------------------------------

bool RunOverMinitree::prepareMatchedParticleLevel(const ReconstructedEvent &event)
{
  if (m_debug > 4)
    std::cout << std::endl
              << " ***** RunOverMinitree::prepareMatchedParticleLevel() *****" << std::endl;

  Long64_t imatching = m_particleTreeIndexes.getMatchingIndex(event);
  if (imatching < 0)
    return false;

  m_particleLevel = std::unique_ptr<ReconstructedEvent>{m_toolsLoaderParticle->eventReaderTool()->readEntry(imatching)};
  m_toolsLoaderParticle->recoTool()->execute(*m_particleLevel);
  bool passedParticleLevel = m_toolsLoaderParticle->selectionTool()->execute(*m_particleLevel, false);
  if (passedParticleLevel)
    m_toolsLoaderParticle->classificationTool()->execute(*m_particleLevel);

  return passedParticleLevel;
}

bool RunOverMinitree::prepareMatchedPartonLevel(const ReconstructedEvent &event)
{
  if (m_debug > 2)
    std::cout << std::endl
              << "RunOverMinitree::prepareMatchedPartonLevel()" << std::endl;
  Long64_t imatching = m_partonTreeIndexes.getMatchingIndex(event);
  if (imatching < 0)
    return false;

  m_partonLevel = std::unique_ptr<ReconstructedEvent>{m_toolsLoaderParton->eventReaderTool()->readEntry(imatching)};
  m_toolsLoaderParton->recoTool()->execute(*m_partonLevel);
  bool passedPartonLevel = m_toolsLoaderParton->selectionTool()->execute(*m_partonLevel, false);
  return passedPartonLevel;
}

//----------------------------------------------------------------------

int RunOverMinitree::checkMatching(const Object_t &t1Reco, const Object_t &t2Reco, const Object_t &t1Truth, const Object_t &t2Truth, const double dRmax)
{

  const double dR11 = ROOT::Math::VectorUtil::DeltaR(t1Reco, t1Truth);
  const double dR12 = ROOT::Math::VectorUtil::DeltaR(t1Reco, t2Truth);
  const double dR21 = ROOT::Math::VectorUtil::DeltaR(t2Reco, t1Truth);
  const double dR22 = ROOT::Math::VectorUtil::DeltaR(t2Reco, t2Truth);

  if (dR11 < dRmax && dR22 < dRmax)
    return 1;
  else if (dR12 < dRmax && dR21 < dRmax)
    return 2;
  else
    return 0;
}

//----------------------------------------------------------------------

void RunOverMinitree::calculateMeasuredVariables(bool passedParticle, bool passedParton)
{

  std::shared_ptr<Object_t> t1Reco = m_recoLevel->getPtr<Object_t>("top1");
  std::shared_ptr<Object_t> t2Reco = m_recoLevel->getPtr<Object_t>("top2");

  m_random->SetSeed(m_eventNumber + m_channelNumber); // !!!!!!!!!!!!!!!????????????????????
  int random = m_random->Integer(2) == 1;

  std::shared_ptr<Object_t> topReco = (random == 0) ? t1Reco : t2Reco;

  m_recoLevel->setPtr<Object_t>("topRandom", topReco);

  m_measuredVariablesReco->calculateMeasuredVariables(*m_recoLevel);

  m_particleMatched = false;
  m_partonMatched = false;

  if (passedParticle)
  {
    std::shared_ptr<Object_t> t1Particle = m_particleLevel->getPtr<Object_t>("top1");
    std::shared_ptr<Object_t> t2Particle = m_particleLevel->getPtr<Object_t>("top2");

    if (m_ttbarConfig->useRecoTruthMatching())
    {
      m_particleMatched = (checkMatching(*t1Reco, *t2Reco, *t1Particle, *t2Particle) == 1);
    }
    else
    {
      m_particleMatched = true;
    }

    if (m_particleMatched)
    {
      std::shared_ptr<Object_t> topParticle = (random == 0) ? t1Particle : t2Particle;
      m_particleLevel->setPtr<Object_t>("topRandom", topParticle);
      m_measuredVariablesParticle->calculateMeasuredVariables(*m_particleLevel);
    }
  }

  if (passedParton)
  {
    std::shared_ptr<Object_t> t1Parton = m_partonLevel->getPtr<Object_t>("top1");
    std::shared_ptr<Object_t> t2Parton = m_partonLevel->getPtr<Object_t>("top2");

    if (m_ttbarConfig->useRecoTruthMatching())
    {
      m_partonMatched = (checkMatching(*t1Reco, *t2Reco, *t1Parton, *t2Parton) == 1);
    }
    else
    {
      m_partonMatched = true;
    }

    if (m_partonMatched)
    {
      std::shared_ptr<Object_t> topParton = (random == 0) ? t1Parton : t2Parton;
      m_partonLevel->setPtr<Object_t>("topRandom", topParton);
      m_measuredVariablesParton->calculateMeasuredVariables(*m_partonLevel);
    }
  }
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::multiplyEventWeights(double weight)
{
  m_weight *= weight;
}

// ------------------------------------------------------------------------------------------------------------------------

bool RunOverMinitree::passedHtFilterCut(const float &filter_Ht)
{
  if (filter_Ht < m_HtFilterCutValue * 1000.)
  {
    if (m_debug > 4)
      std::cout << "Event passed Ht filter cut. filter_HT = " << filter_Ht / 1000 << std::endl;
    return true;
  }
  if (m_debug > 4)
    std::cout << "Event didn't pass Ht filter cut. filter_HT = " << filter_Ht / 1000 << std::endl;
  return false;
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::printInterestingEvent(Minitree & /*t*/)
{

  // m_recoLevel already in GeV
  const auto &ljets = m_recoLevel->getObjectCollection("LargeRJets");
  const Object_t &ljet1 = *ljets[0];
  const Object_t &ljet2 = *ljets[1];

  ReconstructedObjects::FourMom_t ttbar = ljet1 + ljet2;

  if ((ljet1.Pt() > 1200) ||
      (ttbar.M() > 2800) ||
      (ttbar.Pt() > 650) ||
      (ttbar.Pt() < 10) ||
      ((ljet1.Pt() < 510) && (ljet2.Pt() < 360)) ||
      ((ljet1.getAttribute<float>("tau32") < 0.3) && (ljet2.Pt() < 0.3)))
  {
    // original ntuples in MeV
    //(t.reco_t1_pt > 1200000) ||
    //(t.reco_tt_m  > 2800000) ||
    //(t.reco_tt_pt >  650000) ) {
    std::cout << "!!! printout of interesting event !!!: " << std::endl;
    std::cout << "cutting variables (top1/2_pt, ttbar_M, ttbar_pt, top1/2_tau32 :"
              << ljet1.Pt() << "  "
              << ljet2.Pt() << "  "
              << ttbar.M() << "  "
              << ttbar.Pt() << "  "
              << ljet1.getAttribute<float>("tau32") << "  "
              << ljet2.getAttribute<float>("tau32") << std::endl;
    // std::cout << " run/event number reco_t1_pt reco_t2_pt reco_tt_m reco_tt_pt ljet_pt[0] ljet_pt[1]: " << std::endl;
    // std::cout << t.runNumber << " " << t.eventNumber << " " << t.reco_t1_pt << " " << t.reco_t2_pt << " "
    //	 << t.reco_tt_m << " " << t.reco_tt_pt << " " << t.ljet_pt->at(0) << " " ;
    // if (t.ljet_pt->size() > 1) std::cout << t.ljet_pt->at(1) << " ";
    // std::cout << std::endl;
    // t.Print();
    m_recoLevel->print("label");
    std::cout << "!!! end of interesting event !!!: " << std::endl;
  } // end of selection of interesting events
}

// ------------------------------------------------------------------------------------------------------------------------

void RunOverMinitree::printExtremeValues()
{
  size_t n_selections = m_toolsLoaderReco->classificationTool()->getSelectionNames().size();
  std::vector<std::map<std::string, double>> maxValues(n_selections);
  std::vector<std::map<std::string, double>> minValues(n_selections);

  std::map<std::string, double> maxValuesJoined;
  std::map<std::string, double> minValuesJoined;

  for (size_t isel = 0; isel < n_selections; ++isel)
  {
    if (isel == 0)
    {
      maxValuesJoined = m_hisMans[isel]->getMaxValues();
      minValuesJoined = m_hisMans[isel]->getMinValues();
    }
    else
    {
      for (auto &it : maxValuesJoined)
      {
        const std::string &name = it.first;
        double newValue = m_hisMans[isel]->getMaxValues().at(name);
        if (newValue > it.second)
          it.second = newValue;
      }

      for (auto &it : minValuesJoined)
      {
        const std::string &name = it.first;
        double newValue = m_hisMans[isel]->getMinValues().at(name);
        if (newValue < it.second)
          it.second = newValue;
      }
    }
  }
  std::cout << "Printing extreme values for measured variables after preselection " << std::endl;
  for (auto &it : maxValuesJoined)
  {
    const std::string &name = it.first;
    const double maxValue = it.second;
    const double minValue = minValuesJoined[name];
    std::cout << "    Variable name: " << name << " Min value: " << minValue << " Max value: " << maxValue << std::endl;
  }
  std::cout << "Printing extreme values for measured variables in the signal region " << std::endl;
  m_hisMans[m_toolsLoaderReco->classificationTool()->getSignalRegionIndex()]->printExtremeValues();
}

// ------------------------------------------------------------------------------------------------------------------------

bool RunOverMinitree::checkForDuplicatedEvents()
{ // Return true if passed check.

  // Check for duplicate events in all regions. std::set::insert function takes care of check.
  const ObjectContainer_t &largeRJets = m_recoLevel->getObjectCollection("LargeRJets");
  float ljet1_pt = (largeRJets.size() > 0) ? largeRJets[0]->Pt() : -999.;
  float ljet1_eta = (largeRJets.size() > 0) ? largeRJets[0]->Eta() : -999.;

  if (!m_processedEvents.insert(
                            std::tuple<unsigned int, unsigned long long, float, float, float>(
                                m_runNumber,
                                m_eventNumber,
                                1000,
                                ljet1_pt,
                                ljet1_eta))
           .second)
  {
    TString x = "";
    if (!m_isData)
      x = Form(" filter_HT: %f", 1000.);
    if (m_debug > -1)
      std::cout << "Error: Found duplicate event: runNumber:"
                << m_runNumber
                << " eventNumber: "
                << m_eventNumber
                << x
                << " ljet1_pt: "
                << ljet1_pt
                << std::endl;
    m_skippedEvents++;
    return false;
  }
  return true;
}
