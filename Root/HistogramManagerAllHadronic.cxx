#include "TtbarDiffCrossSection/HistogramManagerAllHadronic.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <math.h>
#include "TEnv.h"
#include "TString.h"

ClassImp(HistogramManagerAllHadronic)

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::setupBootStrap(bool doBootstrap,int nBootstrapPseudoexperiments){
  m_DoBootsTrap = doBootstrap; //! m_isSignal; !!!!!HACKED!!!!!!!
  if(m_debug>1) cout << "Set m_DoBootsTrap to " << m_DoBootsTrap << endl;
  if (m_DoBootsTrap) {
    m_Nreplicas = nBootstrapPseudoexperiments;
    if(m_debug>1) cout << "OK, creating an instance of the bootstrap generator with " << m_Nreplicas << " replicas!" << endl;
    m_Bootstrap_Gen = new BootstrapGenerator("Gen", "Gen", m_Nreplicas); 
  }
  
}

//----------------------------------------------------------------------


HistogramManagerAllHadronic::HistogramManagerAllHadronic(std::shared_ptr<const NtuplesAnalysisToolsConfig> ttbarConfig,bool isSignal, int debug): 
	m_debug(debug), m_Bootstrap_Gen(0) {  
  
  if (m_debug>1) cout << endl << " ***** HistogramManagerAllHadronic::HistogramManagerAllHadronic *****" << endl;

  m_ttbarConfig = ttbarConfig;

  m_isSignal=isSignal;
  m_DoBootsTrap = false;
  
  m_fillClosureTestHistos=false;
  
  string ttbarPath = gSystem->Getenv("TTBAR_PATH");

  m_random = new TRandom3();
  
  this->readConfig();
}

//----------------------------------------------------------------------

int HistogramManagerAllHadronic::checkBinning(const vector<double>& binning,const string& type,const string& variable_name,const string& configname){
  const int binning_size=binning.size();
  if(binning_size<2){
    cout << "Error in " << configname <<": variable " << variable_name << "\n" << "Binning is set using less than 2 numbers" << endl;
    return 0;
  }
  else if(type=="equidistant"){
    if(binning_size!=3){
      cout << "Error in " << configname <<": variable " << variable_name << "\n" << "Type is set to be equidistant but binning is not defined by three numbers! \n Exiting now!" << endl;
      return 0;
    }
    else if(!functions::checkEquidistantBinning(binning[0],binning[1],binning[2])){
      cout << "Error in " << configname <<": variable " << variable_name << "\n" << "Check binning! \n Equidistant binning should be defined using one positive integer and floating point numbers xmin < xmax \n Exiting now!" << endl;
      return 0;
    }
    else return 1;
  }
  else if(type=="unequal"){
    if(!functions::checkBinning(binning_size-1,&binning[0])){
      cout << "Error in " << configname <<": Discovered incorrect binning for variable " << variable_name << "\n" << " \n Exiting now!" << endl;
    }
    else return 2;
    
  }
  else{
    if(binning_size==3 && functions::checkEquidistantBinning(binning[0],binning[1],binning[2])) return 1;
    else if(functions::checkBinning(binning_size-1,&binning[0])) return 2;
    else {
      cout << "Error in " << configname <<": Discovered incorrect binning for variable " << variable_name << "\n" << " \n Exiting now!" << endl;
      return 0;
    }
  }
  return 0;
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::readConfig(){
  
  if (m_debug>1) cout << endl << " ***** HistogramManagerAllHadronic::ReadConfig  *****" << endl;
  
  m_histConfigs = m_ttbarConfig->histos_config();
  for(std::string& config : m_histConfigs) {
    config = configFunctions::resolvePath(config);
  }
  
  m_histUnfolingConfigs = m_ttbarConfig->unfolding_histos_config();
  m_histUnfolingConfigsND = m_ttbarConfig->unfolding_histosND_config();
  
  for(std::string& config : m_histUnfolingConfigs) {
    config = configFunctions::resolvePath(config);
  }
  
  for(std::string& config : m_histUnfolingConfigsND) {
    config = configFunctions::resolvePath(config);
  }
}

//----------------------------------------------------------------------

HistogramManagerAllHadronic::~HistogramManagerAllHadronic(){
  if (m_debug>1) cout << endl << " ***** HistogramManagerAllHadronic::destructor  *****" << endl;
  delete m_random;
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::constructHistosOneLevel(const string& level_name){
  if (m_debug>1) cout << endl << " ***** HistogramManagerAllHadronic::constructHistosOneLevel  *****" << endl;

  gDirectory->mkdir(level_name.c_str());
  gDirectory->cd(level_name.c_str());
  
  for(const std::string& histConfigPath: m_histConfigs) {
  
    const nlohmann::json histConfig = nlohmann::json::parse(std::ifstream(histConfigPath));
  
  
    for (auto &item : histConfig.items()) {
      const std::string& variableName = item.key();
           
      const nlohmann::json& histJSON = jsonFunctions::readValueJSON<nlohmann::json>(histConfig,item.key());
      
      const nlohmann::json& xaxis = jsonFunctions::readValueJSON<nlohmann::json>(histJSON,"Xaxis");
      
      const std::vector<double> binningx = configFunctions::readBinning(jsonFunctions::readValueJSON<nlohmann::json>(xaxis,"Binning"));
      
      std::string varStringX = jsonFunctions::readValueJSON<std::string>(xaxis,"Variable");
      
      const std::string xtitle = jsonFunctions::readValueJSON<std::string>(xaxis,"Title","");
      
      const std::string title = jsonFunctions::readValueJSON<std::string>(histJSON,"Title","");
      
      if(histJSON.find("Yaxis") != std::end(histJSON)) {
        // 2D
        
        const nlohmann::json& yaxis = jsonFunctions::readValueJSON<nlohmann::json>(histJSON,"Yaxis");
        
        const std::vector<double> binningy = configFunctions::readBinning(
          jsonFunctions::readValueJSON<nlohmann::json>(yaxis,"Binning"));
            
        std::string varStringY = jsonFunctions::readValueJSON<std::string>(yaxis,"Variable");
        
        const std::string ytitle = jsonFunctions::readValueJSON<std::string>(yaxis,"Title","");
        
        const std::string ztitle = jsonFunctions::readValueJSON<std::string>(histJSON,"Ztitle","");

        TH2D* helphist = new TH2D(
          variableName.c_str(),
          (TString)title.c_str() + ";" + xtitle.c_str() + ";" + ytitle.c_str() + ";" + ztitle.c_str(),
          binningx.size()-1,
          &binningx[0],
          binningy.size()-1,
          &binningy[0]);
            
        std::vector<std::string> variableVector = {varStringX,varStringY};
        
        if(m_debug>1) {
          std::cout << "Variable name: " << variableName << " keys:";
          for(const string& s : variableVector) std::cout << " " << s;
          std::cout << std::endl;
        }
        
        std::vector<const double*> vecPtrs;
        for(const string& s : variableVector) {
          
          if(m_debug>1) std::cout << "Key: " << s << " Pointer: " << m_recoLevelVariables->getConstPtr(s) << std::endl;
          
          vecPtrs.push_back(m_recoLevelVariables->getConstPtr(s));
        }
        m_recoHistos[variableName] = std::make_unique<TTbarHistogram2D>(helphist,vecPtrs);
	
	
	
      } else {
        const std::string ytitle = jsonFunctions::readValueJSON<std::string>(histJSON,"Ytitle","");
        
        // 1D
        TH1D* helphist = new TH1D(
          variableName.c_str(),
          (TString)title.c_str() + ";" + xtitle.c_str() + ";" + ytitle.c_str(),
          binningx.size()-1,
          &binningx[0]);
        
        // Inclusive distributions (multiple variables filled into histogram per event)
        if(functions::beginsWith(varStringX,"Inclusive_")) {
          
          functions::replaceAll(varStringX,"Inclusive_","");
          if(m_debug>2) std::cout << "Pointer: " << m_recoLevelVariables->getConstVectorPtr(varStringX) << std::endl;
          m_recoHistos[variableName] = std::make_unique<TTbarHistogram1DInclusive>(helphist,m_recoLevelVariables->getConstVectorPtr(varStringX));
        }
        // Single variable is filled into histogram per event
        else {
          
          if(m_debug>2) std::cout << "Pointer: " << m_recoLevelVariables->getConstPtr(varStringX) << std::endl;
          
          const double *varPtr = m_recoLevelVariables->getConstPtr(varStringX);
          m_recoHistos[variableName] = std::make_unique<TTbarHistogram1D>(helphist,varPtr);
          
        }
      } // End if 1D histogram 
    } // End loop over histograms
  } // End loop over configs
  
  
  gDirectory->cd("../");
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::constructHistos() {
  if (m_debug>1) cout << endl << " ***** HistogramManagerAllHadronic::constructHistos  *****" << endl;


  this->constructHistosOneLevel("RecoLevel");
  //if (m_isSignal){
  //  this->constructHistosOneLevel("ParticleLevel");
  //  this->constructHistosOneLevel("PartonLevel");
  //}

  gDirectory->mkdir("unfolding");
  gDirectory->cd("unfolding");
  
  const std::string suffixReco = this->suffixReco();
  
  
  for(std::string& config : m_histUnfolingConfigs) {
  
    const nlohmann::json histConfigUnfolding = nlohmann::json::parse(std::ifstream(config));
    
  
    for (auto& item : histConfigUnfolding.items()) {
    
      const std::string observable = item.key();
      
      if (m_debug>1)cout << "Creating histograms for observable " << observable << endl;
      
      const nlohmann::json& histJSON = jsonFunctions::readValueJSON<nlohmann::json>(histConfigUnfolding,item.key());
	
      const nlohmann::json& binningJSON = jsonFunctions::readValueJSON<nlohmann::json>(histJSON,"Binning");
	
      const std::vector<double> binning = configFunctions::readBinning(jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,"Reco"));
      
      const int nbins = binning.size()-1;
      
      if(nbins>0) {
        const std::string variableName = jsonFunctions::readValueJSON<std::string>(histJSON,"Variable");
        const double* varReco = m_measuredVariablesReco->getConstPtr(variableName);
        
        const std::string xtitle = jsonFunctions::readValueJSON<std::string>(histJSON,"Xtitle","");
        
        TH1D* helphist = new TH1D((observable+suffixReco).c_str(),(";"+xtitle+";N_{events}").c_str(),nbins,&binning[0]);
        m_measuredHistos[observable+suffixReco] = std::make_unique<TTbarHistogram1D>(helphist,varReco); // MeasuredHistos takes ownership over TH1D
        
        if(m_fillClosureTestHistos) {
          helphist = new TH1D((observable+suffixReco+"_half1").c_str(),(";"+xtitle+";N_{events}").c_str(),nbins,&binning[0]);
          m_measuredHistos[observable+suffixReco+"_half1"] = std::make_unique<TTbarHistogram1D>(helphist,varReco);
          helphist = new TH1D((observable+suffixReco+"_half2").c_str(),(";"+xtitle+";N_{events}").c_str(),nbins,&binning[0]);
          m_measuredHistos[observable+suffixReco+"_half2"] = std::make_unique<TTbarHistogram1D>(helphist,varReco);
        }
        if (m_DoBootsTrap) {
          TH1DBootstrap* helphist = new TH1DBootstrap((observable+"_bootstrap"+suffixReco).c_str(),(";"+xtitle+";N_{events}").c_str(),nbins,&binning[0], m_Nreplicas, m_Bootstrap_Gen);
          m_measuredHistos[observable+"_bootstrap"+suffixReco] = std::make_unique<TTbarHistogram1DBootstrap>(helphist,varReco); // Adding bootstrap histos into the map
        }
      }
      
      this->constructUnfoldingHistos(histConfigUnfolding, observable,"Particle");
      this->constructUnfoldingHistos(histConfigUnfolding, observable,"Parton");    
    }
  }
  
  for(std::string& config : m_histUnfolingConfigsND) {
    const nlohmann::json histConfigUnfoldingND = nlohmann::json::parse(std::ifstream(config));
    
    for (auto& item : histConfigUnfoldingND.items()) {
      
      const string& observable = item.key();
      const nlohmann::json& histJSON = jsonFunctions::readValueJSON<nlohmann::json>(histConfigUnfoldingND,observable);
	  
      std::vector<vector<double> > binning_reco;
      std::vector<TString> axis_titles;
      std::vector<const double*> variablesReco;
      
      if(m_debug > 2) {
        std::cout << "Making ND histogram:" << std::endl;
        std::cout << "Observable: " << observable << std::endl;
      }
      
      bool constructReco = true;
      
      // Reading axis configurations
      for(auto& axisItems : histJSON.items()) {
	
        const nlohmann::json& axisJSON = axisItems.value();
        const nlohmann::json& binningJSON = jsonFunctions::readValueJSON<nlohmann::json>(axisJSON,"Binning");
        
        binning_reco.push_back(configFunctions::readBinning(
          jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,"Reco")));
          
        axis_titles.push_back(jsonFunctions::readValueJSON<std::string>(axisJSON,"Title").c_str());
        std::string variableName = jsonFunctions::readValueJSON<std::string>(axisJSON,"Variable");
        variablesReco.push_back(m_measuredVariablesReco->getConstPtr(variableName));
        if(binning_reco.back().size()==0) constructReco=false;
      }
      
      if(constructReco) {
        m_measuredHistos[observable+suffixReco] = std::make_unique<TTbarHistogramND>(functions::makeTHnSparseDHistogram((observable+suffixReco).c_str(),axis_titles,binning_reco),variablesReco);
        if(m_fillClosureTestHistos) {
          m_measuredHistos[observable+suffixReco + "_half1"] = std::make_unique<TTbarHistogramND>(functions::makeTHnSparseDHistogram((observable+suffixReco + "_half1").c_str(),axis_titles,binning_reco),variablesReco);
          m_measuredHistos[observable+suffixReco + "_half2"] = std::make_unique<TTbarHistogramND>(functions::makeTHnSparseDHistogram((observable+suffixReco + "_half2").c_str(),axis_titles,binning_reco),variablesReco);
        }
      }
      this->constructUnfoldingHistosND(histConfigUnfoldingND, observable,"Particle");
      this->constructUnfoldingHistosND(histConfigUnfoldingND, observable,"Parton");
    }
  }
  
  gDirectory->cd("../");
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::constructUnfoldingHistos(const nlohmann::json& histConfigUnfolding, const string& observable, const string& level){
  if (m_debug>1) cout << endl << "*****HistogramManagerAllHadronic::constructUnfoldingHistos ***** " << endl;
  if (m_isSignal) {
    
    if (m_debug>2) cout << "Observable: " << observable << " level: " << level << endl;
    
    string nameTruthPassed=prefixPassedTruthAndReco(level)+observable;
    string nameMigration = nameTruthPassed + suffixMigration();
    string nameResolution = nameTruthPassed + suffixResolution();
    string nameMatchingEff = nameTruthPassed + suffixMatchingEff();
    string nameTruthPassedNoReco=prefixPassedTruthNoReco(level)+observable+suffixPassedTruthNoReco(level);
    
    TString tlevel=level.c_str();
    
    const nlohmann::json& histJSON = jsonFunctions::readValueJSON<nlohmann::json>(histConfigUnfolding,observable);
      
    const nlohmann::json& binningJSON = jsonFunctions::readValueJSON<nlohmann::json>(histJSON,"Binning");
      
    const std::vector<double> binning_reco = configFunctions::readBinning(
      jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,"Reco"));
    
    const std::vector<double> binning_truth = configFunctions::readBinning(
      jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,level));
    
    const std::vector<double> binning_reso = configFunctions::readBinning(
      jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,"Reso"));
    
    const int nbins_truth=binning_truth.size()-1;
    const int nbins_reco=binning_reco.size()-1;
    const int nbins_reso= binning_reso.size() -1;
    
    const TString xtitle = jsonFunctions::readValueJSON<std::string>(histJSON,"Xtitle","").c_str();
    const TString ytitle = jsonFunctions::readValueJSON<std::string>(histJSON,"Ytitle","").c_str();
      
    std::string variableName = jsonFunctions::readValueJSON<std::string>(histJSON,"Variable");
    const double* varReco = m_measuredVariablesReco->getConstPtr(variableName);
    const double* varTruth = 0; 
    if(level=="Particle") varTruth = m_measuredVariablesParticle->getConstPtr(variableName);
    if(level=="Parton") varTruth = m_measuredVariablesParton->getConstPtr(variableName);
    std::vector<const double*> vecVariables = {varReco,varTruth};
    
    if(nbins_reso>0 && nbins_truth>0) {
      TH2D* helphist2D = new TH2D(nameResolution.c_str(),";(Reco - Truth) "+ xtitle+";"+tlevel+" level "+xtitle+";N_{events}",nbins_reso,&(binning_reso[0]),nbins_reso/2,binning_truth[0],binning_truth[nbins_truth]);
      m_measuredHistos[nameResolution] = std::make_unique<TTbarHistogram2DResolution>(helphist2D,vecVariables);
    }
  
    if(nbins_reco>0 && nbins_truth>0) {
      TH2D* helphist2D = new TH2D(nameMigration.c_str(),";Reco level "+ xtitle+";"+tlevel+" level "+ytitle+";N_{events}",nbins_reco,&(binning_reco[0]),nbins_truth,&(binning_truth[0]));
      m_measuredHistos[nameMigration] = std::make_unique<TTbarHistogram2D>(helphist2D,vecVariables);
      
      TH1D* helphist1D = new TH1D(nameMatchingEff.c_str(),(TString)";"+"Reco level "+xtitle+";N_{events}",nbins_reco,&(binning_reco[0])); // denominator for matching efficiency 
      m_measuredHistos[nameMatchingEff] = std::make_unique<TTbarHistogram1D>(helphist1D,varReco);
      
      if(m_fillClosureTestHistos) {
        helphist2D = new TH2D((nameMigration+"_half1").c_str(),";Reco level "+ xtitle+";"+tlevel+" level "+ytitle+";N_{events}",nbins_reco,&(binning_reco[0]),nbins_truth,&(binning_truth[0]));
        m_measuredHistos[nameMigration+"_half1"] = std::make_unique<TTbarHistogram2D>(helphist2D,vecVariables);
        helphist2D = new TH2D((nameMigration+"_half2").c_str(),";Reco level "+ xtitle+";"+tlevel+" level "+ytitle+";N_{events}",nbins_reco,&(binning_reco[0]),nbins_truth,&(binning_truth[0]));
        m_measuredHistos[nameMigration+"_half2"] = std::make_unique<TTbarHistogram2D>(helphist2D,vecVariables);
      }
      
    }
    
    if(nbins_truth>0) {
      TH1D* helphist1D = new TH1D(nameTruthPassedNoReco.c_str(),";"+tlevel+" level "+xtitle+";N_{events}",nbins_truth,&(binning_truth[0])); // denominator for efficiency  
      m_measuredHistos[nameTruthPassedNoReco] = std::make_unique<TTbarHistogram1D>(helphist1D,varTruth);
      
      if(m_fillClosureTestHistos) {
        helphist1D = new TH1D((nameTruthPassedNoReco+"_half1").c_str(),";"+tlevel+" level "+xtitle+";N_{events}",nbins_truth,&(binning_truth[0]));
        m_measuredHistos[nameTruthPassedNoReco+"_half1"] = std::make_unique<TTbarHistogram1D>(helphist1D,varTruth);
        helphist1D = new TH1D((nameTruthPassedNoReco+"_half2").c_str(),";"+tlevel+" level "+xtitle+";N_{events}",nbins_truth,&(binning_truth[0]));
        m_measuredHistos[nameTruthPassedNoReco+"_half2"] = std::make_unique<TTbarHistogram1D>(helphist1D,varTruth);
      }
      
    }
    
  }
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::constructUnfoldingHistosND(const nlohmann::json& histConfigUnfoldingND, const string& observable, const string& level){
  if (m_debug>1) cout << endl << "*****HistogramManagerAllHadronic::constructUnfoldingHistosND ***** " << endl;
  if (m_debug>1) cout << "Entering constructUnfoldingHistosND function " << m_isSignal << endl;
  if (m_isSignal) {
    
    string nameTruthPassed=prefixPassedTruthAndReco(level)+observable;
    string nameMigration = nameTruthPassed + suffixMigration();
    string nameMatchingEff = nameTruthPassed + suffixMatchingEff();
    string nameTruthPassedNoReco=prefixPassedTruthNoReco(level)+observable+suffixPassedTruthNoReco(level);
  
    const nlohmann::json& histJSON = jsonFunctions::readValueJSON<nlohmann::json>(histConfigUnfoldingND, observable);
  
    vector<vector<double> > binning_reco;
    vector<TString> axis_titles_reco;
    vector<vector<double> > binning_truth;
    vector<TString> axis_titles_truth;
    vector<const double*> variablesReco;
    vector<const double*> variablesTruth;
    
    if(m_debug > 2) {
      std::cout << "Making ND histogram:" << std::endl;
      std::cout << "Observable: " << observable << std::endl;
    }
    
    bool constructReco = true;
    bool constructTruth = true;
    
    // Reading axis configurations
    for(auto& axisItems : histJSON.items()) {
      
      const nlohmann::json& axisJSON = axisItems.value();
      const nlohmann::json& binningJSON = jsonFunctions::readValueJSON<nlohmann::json>(axisJSON,"Binning");
      
      binning_reco.push_back(configFunctions::readBinning(jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,"Reco")));
      binning_truth.push_back(configFunctions::readBinning(jsonFunctions::readValueJSON<nlohmann::json>(binningJSON,level)));
      
      TString title = jsonFunctions::readValueJSON<std::string>(axisJSON,"Title").c_str();
      axis_titles_reco.push_back((TString)"Reco level " + title);
      axis_titles_truth.push_back((TString)"Reco level " + title);
      
      std::string variableName = jsonFunctions::readValueJSON<std::string>(axisJSON,"Variable");
      variablesReco.push_back(m_measuredVariablesReco->getConstPtr(variableName));
      if(level=="Particle") {
        variablesTruth.push_back(m_measuredVariablesParticle->getConstPtr(variableName));
      }
      if(level=="Parton") {
        variablesTruth.push_back(m_measuredVariablesParton->getConstPtr(variableName));
      }
      
      if(binning_reco.back().size() == 0) constructReco=false;
      if(binning_truth.back().size() == 0) constructTruth = false;
    }
    
    vector<vector<double> > binning_joined;
    vector<TString > axis_titles_joined;
    vector<const double*> variablesJoined;
    
    functions::joinVectors(axis_titles_reco,axis_titles_truth,axis_titles_joined);
    functions::joinVectors(binning_reco,binning_truth,binning_joined);
    functions::joinVectors(variablesReco,variablesTruth,variablesJoined);
    
    if(constructReco && constructTruth) {
      THnSparseD* helphist = functions::makeTHnSparseDHistogram(nameMigration.c_str(),axis_titles_joined,binning_joined);
      m_measuredHistos[nameMigration] = std::make_unique<TTbarHistogramND>(helphist,variablesJoined);
      
      helphist = functions::makeTHnSparseDHistogram(nameMatchingEff.c_str(),axis_titles_reco,binning_reco); // denominator for matching efficiency 
      m_measuredHistos[nameMatchingEff] = std::make_unique<TTbarHistogramND>(helphist,variablesReco);
      
      if(m_fillClosureTestHistos) {
        helphist = functions::makeTHnSparseDHistogram((nameMigration+"_half1").c_str(),axis_titles_joined,binning_joined);
        m_measuredHistos[nameMigration+"_half1"] = std::make_unique<TTbarHistogramND>(helphist,variablesJoined);
        helphist = functions::makeTHnSparseDHistogram((nameMigration+"_half2").c_str(),axis_titles_joined,binning_joined);
        m_measuredHistos[nameMigration+"_half2"] = std::make_unique<TTbarHistogramND>(helphist,variablesJoined);
      }
    }
    
    if(constructTruth) {
      THnSparseD* helphist = functions::makeTHnSparseDHistogram(nameTruthPassedNoReco.c_str(),axis_titles_truth,binning_truth); // denominator for efficiency 
      m_measuredHistos[nameTruthPassedNoReco] = std::make_unique<TTbarHistogramND>(helphist,variablesTruth);
      if(m_fillClosureTestHistos) {
        helphist = functions::makeTHnSparseDHistogram((nameTruthPassedNoReco+"_half1").c_str(),axis_titles_truth,binning_truth); // denominator for efficiency 
        m_measuredHistos[nameTruthPassedNoReco+"_half1"] = std::make_unique<TTbarHistogramND>(helphist,variablesTruth);
        helphist = functions::makeTHnSparseDHistogram((nameTruthPassedNoReco+"_half2").c_str(),axis_titles_truth,binning_truth); // denominator for efficiency 
        m_measuredHistos[nameTruthPassedNoReco+"_half2"] = std::make_unique<TTbarHistogramND>(helphist,variablesTruth); 
      }
    }
    
  }
  if (m_debug>1) cout << "Leaving constructUnfoldingHistosND function " << m_isSignal << endl;
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::fillEventHistosOneLevel(){
  if (m_debug>2) cout << endl << endl << " *****HistogramManagerAllHadronic::fillEventHistosOneLevel()  *****" << endl;
  
  for(auto& it : m_recoHistos) {
    if (m_debug>3) {
      std::cout << "Filling reco histogram with key: " << it.first << std::endl;
      it.second->print();
    }
    it.second->fill(m_weight);
  }
      
  if (m_debug>2) cout << endl << "HistogramManagerAllHadronic::fillEventHistosOneLevel() ended" << endl;
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::fillEventHistos(bool passedRecoLevelCuts, bool passedParticleLevelCuts, bool passedPartonLevelCuts, bool particleMatched, bool partonMatched){
  if (m_debug>2) cout << endl << " *****HistogramManagerAllHadronic::fillEventHistos()  *****" << endl;
  
  m_particleMatched = particleMatched;
  m_partonMatched = partonMatched;
  
  if (passedRecoLevelCuts) this->fillEventHistosOneLevel();
  //if (m_isSignal){
    //if (passedParticleLevelCuts) this->fillEventHistosOneLevel(m_particleLevel,m_particleLevelVariables.get());
    //if (passedPartonLevelCuts) this->fillEventHistosOneLevel(m_partonLevel,m_partonLevelVariables.get());
  //}

  if (m_debug>2) cout << endl << "HistogramManagerAllHadronic::fillEventHistos(): filling unfolding histos" << endl;
  m_recoLevelUnfoldingHistosFilled=false;
  if (passedRecoLevelCuts){
    fillMeasuredHistos("Particle", passedParticleLevelCuts,m_particleMatched);
    fillMeasuredHistos("Parton", passedPartonLevelCuts,m_partonMatched);
  }
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::writeMeasuredHistos(const TString& subdirectory) {
  TString current_path=(TString)gDirectory->GetPath();
  TString path= (TString)gDirectory->GetPath() + "/" + subdirectory;
  gDirectory->cd(path);
  
  for (auto it = m_measuredHistos.begin(); it != m_measuredHistos.end(); ++it) {
    it->second->write();
  }

  gDirectory->cd(current_path);
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::writeRecoHistos(const TString& subdirectory) {
  TString current_path=(TString)gDirectory->GetPath();
  
  TString path= (TString)gDirectory->GetPath() + "/" + subdirectory;
  gDirectory->cd(path);
  
  for (auto it = m_recoHistos.begin(); it != m_recoHistos.end(); ++it) {
    it->second->write();
  }

  gDirectory->cd(current_path);
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::initBootsTrap(UInt_t runno, ULong64_t evtno, ULong64_t channo)
{
  if (m_DoBootsTrap && m_Bootstrap_Gen) {
    //cout << "Initialized Bootstrap with runno,evtno as " << runno << "," << evtno << endl;
    m_Bootstrap_Gen->Generate(runno,evtno,channo);
  }
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::fillTruthLevelHistos(const std::string& level, const double weight) {
  if (m_debug>1) cout << endl << "*****HistogramManagerAllHadronic::fillTruthLevelHistos ***** " << endl;
  
  
  
  if (m_isSignal) {
    
    const string prefix=prefixPassedTruthNoReco(level);
    const string suffix = suffixPassedTruthNoReco(level);
    
    const string suffixHalf = (m_eventNumber%2 == 1) ? (suffix + "_half1") : (suffix + "_half2");
    
    // Lambda function to check if histogram in the map should be filled
    auto filterFunction = [&](const string& key) {
      if(functions::beginsWith(key,prefix)) {
        if (functions::endsWith(key, suffix) || functions::endsWith(key, suffixHalf)) return true;
      }
      return false;
    };
    
    for(auto& it : m_measuredHistos) {
      bool passedCheck = filterFunction(it.first);
      if(m_debug > 3) {
        std::cout << "Processing key: " << it.first << std::endl;
        std::cout << "Should be filled: " << passedCheck << std::endl;
        it.second->print();
      }
      if(passedCheck) it.second->fill(weight);
    }
  }
}	

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::fillMeasuredHistos(const string& level, bool passedTruthLevelCuts, bool passedMatching) {
  
  if (m_debug>1) cout << endl << "*****HistogramManagerAllHadronic::fillUnfoldingHistos_level_vs_level ***** " << endl;
  
  if (m_DoBootsTrap && !m_recoLevelUnfoldingHistosFilled) {
    this->initBootsTrap(m_runNumber, m_eventNumber, m_channelNumber);
  }
    
  const string suffixRecoLevel = this->suffixReco();
  const string closureTestString = (m_eventNumber%2 == 1) ? "_half1" : "_half2";
  const string prefixPassedAll=this->prefixPassedTruthAndReco(level);
  const string suffixMatchingEfficiency=this->suffixMatchingEff();
  const string suffixMigra=this->suffixMigration();
  const string suffixRes=this->suffixResolution();
  
  bool passedTruthLevelCutsWithMatching= passedTruthLevelCuts && passedMatching;
  
  // Lambda function to check if histogram in the map should be filled
  auto filterFunction = [&,this](const string& key) {
    // Check if this is reco level histogram
    if(functions::endsWith(key,suffixRecoLevel) || functions::endsWith(key,suffixRecoLevel+closureTestString)) {
      if(!m_recoLevelUnfoldingHistosFilled) return true;
      else return false; // We don't want to fill reco histograms twice
    }
    
    // Other histograms are filled only in signal samples
    if (!m_isSignal) return false;
    if(!functions::beginsWith(key,prefixPassedAll)) return false;
    
    // Filling Matching efficiency denominator
    if(!passedTruthLevelCuts) return false;
    if(functions::endsWith(key,suffixMatchingEfficiency)) return true;
    
    // Filling migration matrices and resolution plots
    if(!passedTruthLevelCutsWithMatching) return false;
    if(functions::endsWith(key,suffixMigra) || functions::endsWith(key,suffixMigra+closureTestString)) return true;
    if(functions::endsWith(key,suffixRes)) return true;
    
    
    return false;
  };
  
  // Filling histograms
  for (auto& it : m_measuredHistos) {
    bool passedCheck = filterFunction(it.first);
    
    if(m_debug > 3) {
      std::cout << "Processing key: " << it.first << std::endl;
      std::cout << "Should be filled: " << passedCheck << std::endl;
      it.second->print();
    }
    if(passedCheck) {
      it.second->fill(m_weight);
    }
  }
  
  if(!m_recoLevelUnfoldingHistosFilled) m_recoLevelUnfoldingHistosFilled=true; // Setting to truth --> Reco level histos will not be called in next call
  
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::setInfo(int channelNumber,ULong64_t eventNumber,long runNumber){
  m_channelNumber=channelNumber;
  m_eventNumber=eventNumber;
  m_runNumber=runNumber;
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::setWeight(float weight){
  m_weight=weight;
}

//----------------------------------------------------------------------

void HistogramManagerAllHadronic::printExtremeValues() const {
  std::cout << "HistogramManagerAllHadronic::Printing extreme values." << std::endl;
  m_measuredVariablesReco->printExtremeValues();
}

//----------------------------------------------------------------------

const std::map<std::string,double>& HistogramManagerAllHadronic::getMaxValues() const {
  return m_measuredVariablesReco->getMaxValues();
}

//----------------------------------------------------------------------

const std::map<std::string,double>& HistogramManagerAllHadronic::getMinValues() const {
  return m_measuredVariablesReco->getMinValues();
}
