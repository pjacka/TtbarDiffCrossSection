#include "TtbarDiffCrossSection/MMefficiencies.h"
ClassImp(MMefficiencies)

using namespace std;

MMefficiencies::MMefficiencies():itt(0),iLt(1),itL(2),iLL(3)
{

}

void MMefficiencies::Init_histos(){
	
	//TString rootcorebin = gSystem->Getenv("ROOTCOREBIN");
//TEnv* config = new TEnv(rootcorebin + "/data/TtbarDiffCrossSection/histos.env");
 // cout << itt << " " << itL << " " << iLt << " " << iLL << endl;
vector<TH1D*> helpvec(4);
//double binning_leading_pt[]{450,500,550,650,750,850,1200,2000};
double binning_leading_pt[]{450,550,700,850,1200,2000};
helpvec[itt] = new TH1D("leading_pt_tt","",5,binning_leading_pt);
helpvec[iLt] = new TH1D("leading_pt_Lt","",5,binning_leading_pt);
helpvec[itL] = new TH1D("leading_pt_tL","",5,binning_leading_pt);
helpvec[iLL] = new TH1D("leading_pt_LL","",5,binning_leading_pt);
histos.push_back(helpvec);

//double binning_recoil_pt[]{450,500,550,650,750,850,1200,2000};
double binning_recoil_pt[]{450,550,700,850,1200,2000};
helpvec[itt] = new TH1D("recoil_pt_tt","",5,binning_recoil_pt);
helpvec[iLt] = new TH1D("recoil_pt_Lt","",5,binning_recoil_pt);
helpvec[itL] = new TH1D("recoil_pt_tL","",5,binning_recoil_pt);
helpvec[iLL] = new TH1D("recoil_pt_LL","",5,binning_recoil_pt);
histos.push_back(helpvec);
double binning_leading_eta[]{0,0.4,0.8,1.4,2.0};
helpvec[itt] = new TH1D("leading_eta_tt","",4,binning_leading_eta);
helpvec[iLt] = new TH1D("leading_eta_Lt","",4,binning_leading_eta);
helpvec[itL] = new TH1D("leading_eta_tL","",4,binning_leading_eta);
helpvec[iLL] = new TH1D("leading_eta_LL","",4,binning_leading_eta);
histos.push_back(helpvec);
//double binning_recoil_eta[]{-2.5,-1.5,-0.7,0,0.7,1.5,2.5};
double binning_recoil_eta[]{0,0.4,0.8,1.4,2.0};
helpvec[itt] = new TH1D("recoil_eta_tt","",4,binning_recoil_eta);
helpvec[iLt] = new TH1D("recoil_eta_Lt","",4,binning_recoil_eta);
helpvec[itL] = new TH1D("recoil_eta_tL","",4,binning_recoil_eta);
helpvec[iLL] = new TH1D("recoil_eta_LL","",4,binning_recoil_eta);
histos.push_back(helpvec);
//double binning_leading_m[]{20,100,150,160,170,180,190,200,300,400,600,1000};
double binning_leading_m[]{100,150,200,250,300,400,600,1000};
helpvec[itt] = new TH1D("leading_m_tt","",7,binning_leading_m);
helpvec[iLt] = new TH1D("leading_m_Lt","",7,binning_leading_m);
helpvec[itL] = new TH1D("leading_m_tL","",7,binning_leading_m);
helpvec[iLL] = new TH1D("leading_m_LL","",7,binning_leading_m);
histos.push_back(helpvec);
//double binning_recoil_m[]{20,100,150,160,170,180,190,200,300,400,600,1000};
double binning_recoil_m[]{100,150,200,250,300,400,600,1000};
helpvec[itt] = new TH1D("recoil_m_tt","",7,binning_recoil_m);
helpvec[iLt] = new TH1D("recoil_m_Lt","",7,binning_recoil_m);
helpvec[itL] = new TH1D("recoil_m_tL","",7,binning_recoil_m);
helpvec[iLL] = new TH1D("recoil_m_LL","",7,binning_recoil_m);
histos.push_back(helpvec);
//double binning_leading_e[]{20,100,150,160,170,180,190,200,300,400,600,1000};
helpvec[itt] = new TH1D("leading_e_tt","",5,500,1900);
helpvec[iLt] = new TH1D("leading_e_Lt","",5,500,1900);
helpvec[itL] = new TH1D("leading_e_tL","",5,500,1900);
helpvec[iLL] = new TH1D("leading_e_LL","",5,500,1900);
histos.push_back(helpvec);
//double binning_recoil_e[]{20,100,150,160,170,180,190,200,300,400,600,1000};
helpvec[itt] = new TH1D("recoil_e_tt","",5,500,1900);
helpvec[iLt] = new TH1D("recoil_e_Lt","",5,500,1900);
helpvec[itL] = new TH1D("recoil_e_tL","",5,500,1900);
helpvec[iLL] = new TH1D("recoil_e_LL","",5,500,1900);
histos.push_back(helpvec);
//double binning_leading_e[]{20,100,150,160,170,180,190,200,300,400,600,1000};
helpvec[itt] = new TH1D("leading_y_tt","",5,0,2);
helpvec[iLt] = new TH1D("leading_y_Lt","",5,0,2);
helpvec[itL] = new TH1D("leading_y_tL","",5,0,2);
helpvec[iLL] = new TH1D("leading_y_LL","",5,0,2);
histos.push_back(helpvec);
//double binning_recoil_e[]{20,100,150,160,170,180,190,200,300,400,600,1000};
helpvec[itt] = new TH1D("recoil_y_tt","",5,0,2);
helpvec[iLt] = new TH1D("recoil_y_Lt","",5,0,2);
helpvec[itL] = new TH1D("recoil_y_tL","",5,0,2);
helpvec[iLL] = new TH1D("recoil_y_LL","",5,0,2);
histos.push_back(helpvec);
double binning_ttbar_mass[]{1100,1200,1300,1500,2500};
//double binning_ttbar_mass[]{900,1000,1150,1300,1500,2000,2500};
helpvec[itt] = new TH1D("ttbar_mass_tt","",4,binning_ttbar_mass);
helpvec[iLt] = new TH1D("ttbar_mass_Lt","",4,binning_ttbar_mass);
helpvec[itL] = new TH1D("ttbar_mass_tL","",4,binning_ttbar_mass);
helpvec[iLL] = new TH1D("ttbar_mass_LL","",4,binning_ttbar_mass);
histos.push_back(helpvec);
helpvec[itt] = new TH1D("deltaphi_tt",";ttbar_deltaphi;number of events",5,2.6,TMath::Pi());
helpvec[iLt] = new TH1D("deltaphi_Lt",";ttbar_deltaphi;number of events",5,2.6,TMath::Pi());
helpvec[itL] = new TH1D("deltaphi_tL",";ttbar_deltaphi;number of events",5,2.6,TMath::Pi());
helpvec[iLL] = new TH1D("deltaphi_LL",";ttbar_deltaphi;number of events",5,2.6,TMath::Pi());
histos.push_back(helpvec);
helpvec[itt] = new TH1D("njets_tt","",15,0,15);
helpvec[iLt] = new TH1D("njets_Lt","",15,0,15);
helpvec[itL] = new TH1D("njets_tL","",15,0,15);
helpvec[iLL] = new TH1D("njets_LL","",15,0,15);
histos.push_back(helpvec);
helpvec[itt] = new TH1D("nbjets_tt","",15,0,15);
helpvec[iLt] = new TH1D("nbjets_Lt","",15,0,15);
helpvec[itL] = new TH1D("nbjets_tL","",15,0,15);
helpvec[iLL] = new TH1D("nbjets_LL","",15,0,15);
histos.push_back(helpvec);


 for(unsigned int i=0;i<histos.size();i++) for(unsigned int j=0;j<histos[i].size();j++) histos[i][j]->Sumw2();
	
	
	
	
	
	
}
MMefficiencies::MMefficiencies(TFile* file):itt(0),iLt(1),itL(2),iLL(3)
{
  file->cd();
  file->cd("nominal");
  vector<TString> names; 
  
  TIter nextkey( gDirectory->GetListOfKeys()); 
  TKey* key=0;
  while( (key = (TKey*)nextkey())){
    
    TObject *obj=key->ReadObj();
    if (obj->IsA()->InheritsFrom(TH1D::Class())){
        // cout << key->GetName() << endl;
         string name = key->GetName();
         bool isKnownName=0;
         for(unsigned int i=0;i<names.size();i++){
             if(name == names[i]){
                isKnownName=1;break;
             }
         }
         if(isKnownName) continue;
         names.push_back(name);
    }
  }
  TString nominal="nominal/";
  vector<TH1D*> helpvec(4);
  for(unsigned int i=0;i<names.size();i++){
      
      if (names[i].Contains("njets")){
          helpvec[itt] = new TH1D("njets_tt","",15,0,15);
          helpvec[iLt] = new TH1D("njets_Lt","",15,0,15);
          helpvec[itL] = new TH1D("njets_tL","",15,0,15);
          helpvec[iLL] = new TH1D("njets_LL","",15,0,15); 
          histos.push_back(helpvec);
      }
      else if(names[i].Contains("nbjets")){
         helpvec[itt] = new TH1D("nbjets_tt","",15,0,15);
         helpvec[iLt] = new TH1D("nbjets_Lt","",15,0,15);
         helpvec[itL] = new TH1D("nbjets_tL","",15,0,15);
         helpvec[iLL] = new TH1D("nbjets_LL","",15,0,15);
         histos.push_back(helpvec);
      }
      else{
         helpvec[itt] = (TH1D*)file->Get(nominal + names[i]);
         helpvec[itt]->SetName(names[i] + "_tt");
         helpvec[itL] = (TH1D*)file->Get(nominal + names[i]);
         helpvec[itL]->SetName(names[i] + "_tL");
         helpvec[iLt] = (TH1D*)file->Get(nominal + names[i]);
         helpvec[iLt]->SetName(names[i] + "_Lt");
         helpvec[iLL] = (TH1D*)file->Get(nominal + names[i]);
         helpvec[iLL]->SetName(names[i] + "_LL");
         histos.push_back(helpvec);
      }
  }
//  cout << "histos size: " << histos.size() << " " << histos[5][itt]->GetName() << endl;


}
void MMefficiencies::Write(){
for(unsigned int i=0;i<histos.size();i++) for(unsigned int j=0;j<histos[i].size();j++) histos[i][j]->Write();
}
void MMefficiencies::Fill(LargeRJet& Leading,LargeRJet& Recoil,int const m_njets,int const m_nbjets,float m_weight){

      ljet1_pt = Leading.Pt();
      ljet1_eta = Leading.Eta();
      ljet1_phi = Leading.Phi();
      ljet1_m = Leading.M();
      ljet1_e = Leading.E();
      ljet1_y = abs(Leading.Rapidity());
      ljet1_split12 = Leading.split12();
      ljet1_tau21 = Leading.tau21();
      ljet1_tau32 = Leading.tau32();
      ljet2_pt = Recoil.Pt();
      ljet2_eta = Recoil.Eta();
      ljet2_phi = Recoil.Phi();
      ljet2_m = Recoil.M();
      ljet2_e = Recoil.E();
      ljet2_y = abs(Recoil.Rapidity());
      ljet2_split12 = Recoil.split12();
      ljet2_tau21 = Recoil.tau21();
      ljet2_tau32 = Recoil.tau32();
      TLorentzVector ttbar=Leading + Recoil;
      ttbar_mass = ttbar.M();
      ttbar_pt = ttbar.Pt();
      ttbar_eta = ttbar.Eta();
      ttbar_phi = ttbar.Phi();
      ttbar_e = ttbar.E();
      ttbar_y = abs(ttbar.Rapidity());
      deltaphi = abs(Leading.DeltaPhi(Recoil));
      ljet1_btag = Leading.isBTagged();
      ljet1_toptag = Leading.isTopTagged();
      ljet2_btag = Recoil.isBTagged();
      ljet2_toptag = Recoil.isTopTagged();
      njets=m_njets;
      nbjets=m_nbjets;
      weight=m_weight;


     bool leadingT = Leading.isTopTagged() && Leading.isBTagged();
     bool recoilT = Recoil.isTopTagged() && Recoil.isBTagged();

     if(leadingT && recoilT) { 
         Fill(Leading,Recoil,njets,nbjets,itt,weight);
       
     }
     else if(leadingT) {
         Fill(Leading,Recoil,njets,nbjets,itL,weight);
     }
     else if(recoilT){
         Fill(Leading,Recoil,njets,nbjets,iLt,weight);
     }
     else {
         Fill(Leading,Recoil,njets,nbjets,iLL,weight);

     }
}
void MMefficiencies::Fill_v2(LargeRJet& Leading,LargeRJet& Recoil,int const njets,int const nbjets,float weight){
     if(Leading.isTopTagged() && Recoil.isTopTagged()) { 
         Fill(Leading,Recoil,njets,nbjets,itt,weight);
       
     }
     else if(Leading.isTopTagged()) {
         Fill(Leading,Recoil,njets,nbjets,itL,weight);
     }
     else if(Recoil.isTopTagged()){
         Fill(Leading,Recoil,njets,nbjets,iLt,weight);
     }
     else {
         Fill(Leading,Recoil,njets,nbjets,iLL,weight);

     }
}
void MMefficiencies::Fill(LargeRJet& Leading,LargeRJet& Recoil,int const njets,int const nbjets,int const i,float weight){
   int const index_leading_pt(0);
   int const index_recoil_pt(1);
   int const index_leading_eta(2);
   int const index_recoil_eta(3);
   int const index_leading_m(4);
   int const index_recoil_m(5);
   int const index_leading_e(6);
   int const index_recoil_e(7);
   int const index_leading_y(8);
   int const index_recoil_y(9);
   int const index_ttbar_mass(10);
   int const index_deltaphi(11);
   int const index_njets(12);
   int const index_nbjets(13);
   
   TLorentzVector ttbar= Leading + Recoil;
   histos[index_leading_pt][i]->Fill(Leading.Pt(),weight);
   histos[index_recoil_pt][i]->Fill(Recoil.Pt(),weight);
   histos[index_leading_eta][i]->Fill(fabs(Leading.Eta()),weight);
   histos[index_recoil_eta][i]->Fill(fabs(Recoil.Eta()),weight);
   histos[index_leading_m][i]->Fill(Leading.M(),weight);
   histos[index_recoil_m][i]->Fill(Recoil.M(),weight);
   histos[index_leading_e][i]->Fill(Leading.E(),weight);
   histos[index_recoil_e][i]->Fill(Recoil.E(),weight);
   histos[index_leading_y][i]->Fill(Leading.Rapidity(),weight);
   histos[index_recoil_y][i]->Fill(Recoil.Rapidity(),weight);
   histos[index_ttbar_mass][i]->Fill(ttbar.M(),weight);
   histos[index_deltaphi][i]->Fill(fabs(Leading.DeltaPhi(Recoil)),weight);
   histos[index_njets][i]->Fill(njets,weight);
   histos[index_nbjets][i]->Fill(nbjets,weight);

}

void MMefficiencies::Init_minitree(TTree* minitree){


  minitree->Branch("ljet1_pt",&ljet1_pt,"ljet1_pt/F");
  minitree->Branch("ljet1_eta",&ljet1_eta,"ljet1_eta/F");
  minitree->Branch("ljet1_phi",&ljet1_phi,"ljet1_phi/F");
  minitree->Branch("ljet1_m",&ljet1_m,"ljet1_m/F");
  minitree->Branch("ljet1_e",&ljet1_e,"ljet1_e/F");
  minitree->Branch("ljet1_y",&ljet1_y,"ljet1_y/F");
  minitree->Branch("ljet1_split12",&ljet1_split12,"ljet1_split12/F");
  minitree->Branch("ljet1_tau21",&ljet1_tau21,"ljet1_tau21/F");
  minitree->Branch("ljet1_tau32",&ljet1_tau32,"ljet1_tau32/F");
  minitree->Branch("ljet1_btag",&ljet1_btag,"ljet1_btag/O");
  minitree->Branch("ljet1_toptag",&ljet1_toptag,"ljet1_toptag/O");
  minitree->Branch("ljet2_pt",&(ljet2_pt),"ljet2_pt/F");
  minitree->Branch("ljet2_eta",&ljet2_eta,"ljet2_eta/F");
  minitree->Branch("ljet2_phi",&ljet2_phi,"ljet2_phi/F");
  minitree->Branch("ljet2_m",&ljet2_m,"ljet2_m/F");
  minitree->Branch("ljet2_e",&ljet2_e,"ljet2_e/F");
  minitree->Branch("ljet2_y",&ljet2_y,"ljet2_y/F");
  minitree->Branch("ljet2_split12",&ljet2_split12,"ljet2_split12/F");
  minitree->Branch("ljet2_tau21",&ljet2_tau21,"ljet2_tau21/F");
  minitree->Branch("ljet2_tau32",&ljet2_tau32,"ljet2_tau32/F");
  minitree->Branch("ljet2_btag",&ljet2_btag,"ljet2_btag/O");
  minitree->Branch("ljet2_toptag",&ljet2_toptag,"ljet2_toptag/O");
  minitree->Branch("ttbar_pt",&ttbar_pt,"ttbar_pt/F");
  minitree->Branch("ttbar_eta",&ttbar_eta,"ttbar_eta/F");
  minitree->Branch("ttbar_phi",&ttbar_phi,"ttbar_phi/F");
  minitree->Branch("ttbar_e",&ttbar_e,"ttbar_e/F");
  minitree->Branch("ttbar_y",&ttbar_y,"ttbar_y/F");
  minitree->Branch("ttbar_mass",&ttbar_mass,"ttbar_mass/F");
  minitree->Branch("deltaphi",&deltaphi,"deltaphi/F");
  minitree->Branch("njets",&njets,"njets/F");
  minitree->Branch("nbjets",&nbjets,"nbjets/F");
  minitree->Branch("weight",&weight,"weight/F");






}

void MMefficiencies::DestroyHistograms(){
	
	const int size = histos.size();
	const int size2 = size > 0 ? histos[0].size() : -1;
	for(int i=0;i<size;i++)for(int j=0;j<size2;j++){
		delete histos[i][j];
	}
	histos.clear();
}

