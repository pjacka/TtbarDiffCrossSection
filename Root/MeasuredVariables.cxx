#include "TtbarDiffCrossSection/MeasuredVariables.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "Math/RotationZ.h"

#include <float.h>

#include "Math/VectorUtil.h"

ClassImp(MeasuredVariables)

using ROOT::Math::VectorUtil::DeltaPhi;

MeasuredVariables::MeasuredVariables() : MeasuredVariablesBase::MeasuredVariablesBase() {
  
  if(!initialize()) throw TtbarDiffCrossSection_exception("Error: Failed to initialize measured variables");
  
}

//----------------------------------------------------------------------

bool MeasuredVariables::initialize() {
  
  std::vector<std::string> variables_vec = {
    "totalCrossSection",
    "t1_pt", "t1_y", "t1_m", "t1_eta", "t1_phi",
    "t2_pt", "t2_y", "t2_m", "t2_eta", "t2_phi",
    "deltaPt", "deltaPhi", "deltaEta", "deltaY",
    "top_pt", "top_y", "top_m",
    "ttbar_pt", "ttbar_y", "ttbar_m", "ttbar_chi", "ttbar_pout",
    "cosThetaStar", "y_boost", "H_tt_pt", "y_star",
    "z_tt_pt", "t1_m_over_pt", "t2_m_over_pt",
    "b1_pt", "b1_y", "b1_m", "b1_eta", "b1_phi", "b1_pt_frac", "b1_cos_theta",
    "W1_pt", "W1_y", "W1_m", "W1_eta", "W1_phi", "W1_pt_frac", "W1_cos_theta",
    "b2_pt", "b2_y", "b2_m", "b2_eta", "b2_phi", "b2_pt_frac", "b2_cos_theta",
    "W2_pt", "W2_y", "W2_m", "W2_eta", "W2_phi", "W2_pt_frac", "W2_cos_theta",
    "b1_cms_pt", "b1_cms_y", "b1_cms_m", "b1_cms_eta", "b1_cms_phi", "b1_cms_cos_theta",
    "W1_cms_pt", "W1_cms_y", "W1_cms_m", "W1_cms_eta", "W1_cms_phi", "W1_cms_cos_theta",
    "b2_cms_pt", "b2_cms_y", "b2_cms_m", "b2_cms_eta", "b2_cms_phi", "b2_cms_cos_theta",
    "W2_cms_pt", "W2_cms_y", "W2_cms_m", "W2_cms_eta", "W2_cms_phi", "W2_cms_cos_theta",
    "b1_b2_delta_phi", "b1_b2_delta_y", "b1_b2_delta_eta",
    "W1_W2_delta_phi", "W1_W2_delta_y", "W1_W2_delta_eta",
    "b1_b2_cms_delta_phi", "b1_b2_cms_delta_y", "b1_b2_cms_delta_eta",
    "W1_W2_cms_delta_phi", "W1_W2_cms_delta_y", "W1_W2_cms_delta_eta",
    "t1_and_W1_planes_phi", "t1_and_W1_planes_pt", "t1_and_W1_planes_px", "t1_and_W1_planes_py",
    "t2_and_W2_planes_phi", "t2_and_W2_planes_pt", "t2_and_W2_planes_px", "t2_and_W2_planes_py"
  };
  
  // Variables are registered into a map
  for (const std::string& var : variables_vec) {
    if (!insertVariable(var)) return false;
  }
  // Min and max values maps are initialized as well
  for(const auto& it : m_variables) {
    m_maxValues[it.first] = -FLT_MAX; 
    m_minValues[it.first] = FLT_MAX; 
  }
  
  return true;
}

//----------------------------------------------------------------------

void MeasuredVariables::calculateMeasuredVariables(const ReconstructedEvent& event) {
  
  //resetValues(); // No need to reset values, all values are filled per event
  
  const Object_t& t1 = event.getConstRef<Object_t>("top1");
  const Object_t& t2 = event.getConstRef<Object_t>("top2");
  const Object_t& top = event.getConstRef<Object_t>("topRandom");
  ReconstructedObjects::FourMom_t ttbar =  t1 + t2;
  
  const Object_t& b1 = t1.getConstRef<Object_t>("BCandidate");
  const Object_t& W1 = t1.getConstRef<Object_t>("WCandidate");
  
  const Object_t& b2 = t2.getConstRef<Object_t>("BCandidate");
  const Object_t& W2 = t2.getConstRef<Object_t>("WCandidate");
  
  const ReconstructedObjects::FourMom_t b1_cms = ReconstructedObjects::transformToCMS(b1, t1);
  const ReconstructedObjects::FourMom_t W1_cms = ReconstructedObjects::transformToCMS(W1, t1);
  
  const ReconstructedObjects::FourMom_t b2_cms = ReconstructedObjects::transformToCMS(b2, t2);
  const ReconstructedObjects::FourMom_t W2_cms = ReconstructedObjects::transformToCMS(W2, t2);
    
  
  //const ReconstructedObjects::FourMom_t t1_top_frame = ReconstructedObjects::rotateToDirection(t1, t1);
  //const ReconstructedObjects::FourMom_t W1_top_frame = ReconstructedObjects::rotateToDirection(W1, t1);
  //const ReconstructedObjects::FourMom_t b1_top_frame = ReconstructedObjects::rotateToDirection(b1, t1);
  //const ReconstructedObjects::FourMom_t t2_top_frame = ReconstructedObjects::rotateToDirection(t2, t2);
  //const ReconstructedObjects::FourMom_t W2_top_frame = ReconstructedObjects::rotateToDirection(W2, t2);
  //const ReconstructedObjects::FourMom_t b2_top_frame = ReconstructedObjects::rotateToDirection(b2, t2);

  
  //std::cout << "t1 orig coordinates: X: " << t1.X() << " Y: " << t1.Y() << " Z: " << t1.Z() << " E: " << t1.E() << std::endl;
  //std::cout << "t1 in boost coordinates: X: " << t1_top_frame.X() << " Y: " << t1_top_frame.Y() << " Z: " << t1_top_frame.Z() << " E: " << t1_top_frame.E() << std::endl;
  //std::cout << "W1 orig coordinates: X: " << W1.X() << " Y: " << W1.Y() << " Z: " << W1.Z() << " E: " << W1.E() << std::endl;
  //std::cout << "W1 in boost coordinates: X: " << W1_top_frame.X() << " Y: " << W1_top_frame.Y() << " Z: " << W1_top_frame.Z() << " E: " << W1_top_frame.E() << std::endl;
  //std::cout << "b1 orig coordinates: X: " << b1.X() << " Y: " << b1.Y() << " Z: " << b1.Z() << " E: " << b1.E() << std::endl;
  //std::cout << "b1 in boost coordinates: X: " << b1_top_frame.X() << " Y: " << b1_top_frame.Y() << " Z: " << b1_top_frame.Z() << " E: " << b1_top_frame.E() << std::endl;
  //std::cout << std::endl;
  //std::cout << "t2 orig coordinates: X: " << t2.X() << " Y: " << t2.Y() << " Z: " << t2.Z() << " E: " << t2.E() << std::endl;
  //std::cout << "t2 in boost coordinates: X: " << t2_top_frame.X() << " Y: " << t2_top_frame.Y() << " Z: " << t2_top_frame.Z() << " E: " << t2_top_frame.E() << std::endl;
  //std::cout << "W2 orig coordinates: X: " << W2.X() << " Y: " << W2.Y() << " Z: " << W2.Z() << " E: " << W2.E() << std::endl;
  //std::cout << "W2 in boost coordinates: X: " << W2_top_frame.X() << " Y: " << W2_top_frame.Y() << " Z: " << W2_top_frame.Z() << " E: " << W2_top_frame.E() << std::endl;
  //std::cout << "b2 orig coordinates: X: " << b2.X() << " Y: " << b2.Y() << " Z: " << b2.Z() << " E: " << b2.E() << std::endl;
  //std::cout << "b2 in boost coordinates: X: " << b2_top_frame.X() << " Y: " << b2_top_frame.Y() << " Z: " << b2_top_frame.Z() << " E: " << b2_top_frame.E() << std::endl;
  //std::cout << std::endl;

  //const double t1_and_W1_planes_phi = getAngleBetweenBoostedTopAndWPlanes(t1, W1, b1);
  //const double t1_and_W1_planes_pt = getTopPtInWTransversePlane(t1, W1, b1);
  //const double t1_and_W1_planes_px = t1_and_W1_planes_pt * std::cos(t1_and_W1_planes_phi);
  //const double t1_and_W1_planes_py = t1_and_W1_planes_pt * std::sin(t1_and_W1_planes_phi);
  
  //ROOT::Math::XYVector t1_and_W1_planes_px_py = getTopPxPyInWTransversePlane(t1, W1, b1);
  
  
  //const double t2_and_W2_planes_phi = getAngleBetweenBoostedTopAndWPlanes(t2, W2, b2);
  //const double t2_and_W2_planes_pt = getTopPtInWTransversePlane(t2, W2, b2);
  //const double t2_and_W2_planes_px = t2_and_W2_planes_pt * std::cos(t2_and_W2_planes_phi);
  //const double t2_and_W2_planes_py = t2_and_W2_planes_pt * std::sin(t2_and_W2_planes_phi);

  //std::cout << "cos(phi): " << std::cos(t1_and_W1_planes_phi) << " phi: "  << " " << t1_and_W1_planes_phi << std::endl;
  //std::cout << "Px: " << t1_and_W1_planes_px << " Py: " <<  t1_and_W1_planes_py << " Pt: " << t1_and_W1_planes_pt << std::endl;
  //std::cout << "Px: " << t1_and_W1_planes_px_py.X() << " Py: " <<  t1_and_W1_planes_px_py.Y() << " Pt: " << t1_and_W1_planes_px_py.R() << " Phi: " << t1_and_W1_planes_px_py.Phi() << std::endl;
  //if(t1_and_W1_planes_py < 0) std::cout << "Py < 0: " << t1_and_W1_planes_py << std::endl;
  
  
  
  
  const double unit = 0.001; // Converting from GeV to TeV
  
  const double top_pt = top.Pt() * unit;
  const double top_mass = top.M();
  const double top_y_abs = std::abs(top.Rapidity());
  
  const double t1_pt = t1.Pt() * unit;
  const double t2_pt = t2.Pt() * unit;
  const double t1_y = t1.Rapidity();
  const double t2_y = t2.Rapidity();
  const double t1_y_abs = std::abs(t1_y);
  const double t2_y_abs = std::abs(t2_y);
  const double t1_eta = t1.Eta();
  const double t2_eta = t2.Eta();
  const double t1_phi = t1.Phi();
  const double t2_phi = t2.Phi();
  
  const double ttbar_mass = ttbar.M() * unit;
  const double ttbar_pt = ttbar.Pt() * unit;
  const double ttbar_y_abs = std::abs(ttbar.Rapidity());
  
  const double t1_mass = t1.M() * unit;
  const double t2_mass = t2.M() * unit;
  
  const double t1_m_over_pt = t1_mass / t1_pt;
  const double t2_m_over_pt = t2_mass / t2_pt;
  
  const double H_tt_pt = t1_pt + t2_pt;
  const double ttbar_deltaphi = std::abs(DeltaPhi(t1, t2));
  
  const double z_tt_pt = std::min(t1_pt,t2_pt) / std::max(t1_pt, t2_pt);
  
  const double pout = ReconstructedObjects::pout(t1, t2) * unit;
  
  const double y_star = 0.5 * std::abs(t2_y-t1_y);
  const double y_boost = 0.5 * std::abs(t2_y+t1_y);
 
  const double chi_ttbar = exp(2. * std::abs(y_star));

  const double cos_theta_star = ReconstructedObjects::cosThetaStar(t1, ttbar);
  
  
  
  const double b1_pt = b1.Pt();
  const double b1_y = b1.Rapidity();
  const double b1_y_abs = std::abs(b1_y);
  const double b1_eta = b1.Eta();
  const double b1_phi = b1.Phi();
  const double b1_mass = b1.M();
  const double b1_pt_frac = b1_pt * unit / t1_pt;
  const double b1_cos_theta = ReconstructedObjects::cosTheta(b1, t1);
  
  const double W1_pt = W1.Pt();
  const double W1_y = W1.Rapidity();
  const double W1_y_abs = std::abs(W1_y);
  const double W1_eta = W1.Eta();
  const double W1_phi = W1.Phi();
  const double W1_mass = W1.M();
  const double W1_pt_frac = W1_pt * unit / t1_pt;
  const double W1_cos_theta = ReconstructedObjects::cosTheta(W1, t1);
  
  const double b2_pt = b2.Pt();
  const double b2_y = b2.Rapidity();
  const double b2_y_abs = std::abs(b2_y);
  const double b2_eta = b2.Eta();
  const double b2_phi = b2.Phi();
  const double b2_mass = b2.M();
  const double b2_pt_frac = b2_pt * unit / t2_pt;
  const double b2_cos_theta = ReconstructedObjects::cosTheta(b2, t2);
  
  const double W2_pt = W2.Pt();
  const double W2_y = W2.Rapidity();
  const double W2_y_abs = std::abs(W2_y);
  const double W2_eta = W2.Eta();
  const double W2_phi = W2.Phi();
  const double W2_mass = W2.M();
  const double W2_pt_frac = W2_pt * unit / t2_pt;
  const double W2_cos_theta = ReconstructedObjects::cosTheta(W2, t2);
  
  const double b1_b2_delta_phi = b1_phi - b2_phi;
  const double b1_b2_delta_eta = b1_eta - b2_eta;
  const double b1_b2_delta_y = b1_y - b2_y;
  
  const double W1_W2_delta_phi = W1_phi - W2_phi;
  const double W1_W2_delta_eta = W1_eta - W2_eta;
  const double W1_W2_delta_y = W1_y - W2_y;
  
  const double b1_cms_pt = b1_cms.Pt();
  const double b1_cms_y = b1_cms.Rapidity();
  const double b1_cms_y_abs = std::abs(b1_cms_y);
  const double b1_cms_eta = b1_cms.Eta();
  const double b1_cms_phi = b1_cms.Phi();
  const double b1_cms_mass = b1_cms.M();
  const double b1_cms_cos_theta = ReconstructedObjects::cosTheta(b1_cms, t1);
  
  const double W1_cms_pt = W1_cms.Pt();
  const double W1_cms_y = W1_cms.Rapidity();
  const double W1_cms_y_abs = std::abs(W1_cms_y);
  const double W1_cms_eta = W1_cms.Eta();
  const double W1_cms_phi = W1_cms.Phi();
  const double W1_cms_mass = W1_cms.M();
  const double W1_cms_cos_theta = ReconstructedObjects::cosTheta(W1_cms, t1);
  
  const double b2_cms_pt = b2_cms.Pt();
  const double b2_cms_y = b2_cms.Rapidity();
  const double b2_cms_y_abs = std::abs(b2_cms_y);
  const double b2_cms_eta = b2_cms.Eta();
  const double b2_cms_phi = b2_cms.Phi();
  const double b2_cms_mass = b2_cms.M();
  const double b2_cms_cos_theta = ReconstructedObjects::cosTheta(b2_cms, t2);
  
  const double W2_cms_pt = W2_cms.Pt();
  const double W2_cms_y = W2_cms.Rapidity();
  const double W2_cms_y_abs = std::abs(W2_cms_y);
  const double W2_cms_eta = W2_cms.Eta();
  const double W2_cms_phi = W2_cms.Phi();
  const double W2_cms_mass = W2_cms.M();
  const double W2_cms_cos_theta = ReconstructedObjects::cosTheta(W2_cms, t2);
  
  const double b1_b2_cms_delta_phi = b1_phi - b2_phi;
  const double b1_b2_cms_delta_eta = b1_eta - b2_eta;
  const double b1_b2_cms_delta_y = b1_y - b2_y;
  
  const double W1_W2_cms_delta_phi = W1_cms_phi - W2_cms_phi;
  const double W1_W2_cms_delta_eta = W1_cms_eta - W2_cms_eta;
  const double W1_W2_cms_delta_y = W1_cms_y - W2_cms_y;
  
  
  setValue("totalCrossSection", 0.5);
  setValue("t1_pt", t1_pt);
  setValue("t1_m", t1_mass);
  setValue("t1_y", t1_y_abs);
  setValue("t1_eta", t1_eta);
  setValue("t1_phi", t1_phi);
  setValue("t2_pt", t2_pt);
  setValue("t2_m", t2_mass);
  setValue("t2_y", t2_y_abs);
  setValue("t2_eta", t2_eta);
  setValue("t2_phi", t2_phi);
  setValue("top_pt",top_pt);
  setValue("top_m", top_mass);
  setValue("top_y", top_y_abs);
  setValue("ttbar_pt", ttbar_pt);
  setValue("ttbar_m", ttbar_mass);
  setValue("ttbar_y", ttbar_y_abs);
  setValue("deltaPt", std::abs(t1_pt-t2_pt));
  setValue("deltaPhi", ttbar_deltaphi);
  setValue("deltaY", std::abs(t1_y-t2_y));
  setValue("deltaEta", std::abs(t1_eta-t2_eta));
  setValue("ttbar_chi", chi_ttbar);
  setValue("ttbar_pout", pout);
  setValue("cosThetaStar", cos_theta_star);
  setValue("y_boost", y_boost);
  setValue("H_tt_pt", H_tt_pt);
  setValue("y_star", y_star);
  setValue("z_tt_pt", z_tt_pt);
  setValue("t1_m_over_pt", t1_m_over_pt);
  setValue("t2_m_over_pt", t2_m_over_pt);
  setValue("b1_pt", b1_pt);
  setValue("b1_m", b1_mass);
  setValue("b1_y", b1_y_abs);
  setValue("b1_eta", b1_eta);
  setValue("b1_phi", b1_phi);
  setValue("b1_pt_frac", b1_pt_frac);
  setValue("b1_cos_theta", b1_cos_theta);
  setValue("W1_pt", W1_pt);
  setValue("W1_m", W1_mass);
  setValue("W1_y", W1_y_abs);
  setValue("W1_eta", W1_eta);
  setValue("W1_phi", W1_phi);
  setValue("W1_pt_frac", W1_pt_frac);
  setValue("W1_cos_theta", W1_cos_theta);
  setValue("b2_pt", b2_pt);
  setValue("b2_m", b2_mass);
  setValue("b2_y", b2_y_abs);
  setValue("b2_eta", b2_eta);
  setValue("b2_phi", b2_phi);
  setValue("b2_pt_frac", b2_pt_frac);
  setValue("b2_cos_theta", b2_cos_theta);
  setValue("W2_pt", W2_pt);
  setValue("W2_m", W2_mass);
  setValue("W2_y", W2_y_abs);
  setValue("W2_eta", W2_eta);
  setValue("W2_phi", W2_phi);
  setValue("W2_pt_frac", W2_pt_frac);
  setValue("W2_cos_theta", W2_cos_theta);
  setValue("b1_b2_delta_phi", b1_b2_delta_phi);
  setValue("b1_b2_delta_eta", b1_b2_delta_eta);
  setValue("b1_b2_delta_y", b1_b2_delta_y);
  setValue("W1_W2_delta_phi", W1_W2_delta_phi);
  setValue("W1_W2_delta_eta", W1_W2_delta_eta);
  setValue("W1_W2_delta_y", W1_W2_delta_y);
  setValue("b1_cms_pt", b1_cms_pt);
  setValue("b1_cms_m", b1_cms_mass);
  setValue("b1_cms_y", b1_cms_y_abs);
  setValue("b1_cms_eta", b1_cms_eta);
  setValue("b1_cms_phi", b1_cms_phi);
  setValue("b1_cms_cos_theta", b1_cms_cos_theta);
  setValue("W1_cms_pt", W1_cms_pt);
  setValue("W1_cms_m", W1_cms_mass);
  setValue("W1_cms_y", W1_cms_y_abs);
  setValue("W1_cms_eta", W1_cms_eta);
  setValue("W1_cms_phi", W1_cms_phi);
  setValue("W1_cms_cos_theta", W1_cms_cos_theta);
  setValue("b2_cms_pt", b2_cms_pt);
  setValue("b2_cms_m", b2_cms_mass);
  setValue("b2_cms_y", b2_cms_y_abs);
  setValue("b2_cms_eta", b2_cms_eta);
  setValue("b2_cms_phi", b2_cms_phi);
  setValue("b2_cms_cos_theta", b2_cms_cos_theta);
  setValue("W2_cms_pt",W2_cms_pt);
  setValue("W2_cms_m", W2_cms_mass);
  setValue("W2_cms_y", W2_cms_y_abs);
  setValue("W2_cms_eta", W2_cms_eta);
  setValue("W2_cms_phi", W2_cms_phi);
  setValue("W2_cms_cos_theta", W2_cms_cos_theta);
  setValue("b1_b2_cms_delta_phi", b1_b2_cms_delta_phi);
  setValue("b1_b2_cms_delta_eta", b1_b2_cms_delta_eta);
  setValue("b1_b2_cms_delta_y", b1_b2_cms_delta_y);
  setValue("W1_W2_cms_delta_phi", W1_W2_cms_delta_phi);
  setValue("W1_W2_cms_delta_eta", W1_W2_cms_delta_eta);
  setValue("W1_W2_cms_delta_y", W1_W2_cms_delta_y);
  

  setMaxValues();
  setMinValues();

}
