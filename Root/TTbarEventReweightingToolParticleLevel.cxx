#include "TtbarDiffCrossSection/TTbarEventReweightingToolParticleLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

#include "TopEvent/EventTools.h"
#include "TSystem.h"
ClassImp(TTbarEventReweightingToolParticleLevel)


TTbarEventReweightingToolParticleLevel::TTbarEventReweightingToolParticleLevel(const std::string& name) : EventReweightingToolBase(name) {
  m_xsecWithKfactor = {-1.};
  m_HtFilterEfficiencyCorrectionsFile = 0;
  m_samplesWithHtFilterEfficiencyCorrection = {410429,410431,410444,410445};
  m_sumWeights = -1.;
  
  declareProperty("xsecWithKfactor", m_xsecWithKfactor);
  declareProperty("sumWeights", m_sumWeights);
  declareProperty("lumi", m_lumi);  
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingToolParticleLevel::readJSONConfig(const nlohmann::json& /*json*/) {
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingToolParticleLevel::initialize() {
  top::check(EventReweightingToolBase::initialize(),m_name+": Failed to initialize");
  
  //top::check(m_tree,m_name+": Reco tree not loaded");
      
  //top::check(m_xsecWithKfactor>0,m_name+": xsecWithKfactor is not set before initialization");
    
  //if(m_ttbarConfig->useFilterEfficienciesCorrections()) {
    
    //TString name = m_ttbarConfig->filterEfficienciesCorrectionsFileName();
    //if(!name.BeginsWith("/")) name = (TString)gSystem->Getenv("TTBAR_PATH")+"/"+name;
    //m_HtFilterEfficiencyCorrectionsFile = std::unique_ptr<TFile>{TFile::Open(name)};
    //if(m_HtFilterEfficiencyCorrectionsFile==0 || m_HtFilterEfficiencyCorrectionsFile->IsZombie()) {
      //std::cout << "Error: Failed to open file with Ht-filter efficiency corrections. " << name << std::endl;
      //exit(-2);
    //}
    
  //}
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingToolParticleLevel::execute(ReconstructedEvent& event) {
  const double weight_mc = event.getAttribute<float>("weight_mc");  
  
  m_weight = weight_mc * m_xsecWithKfactor * m_lumi / m_sumWeights;
  event.setAttribute<double>("weight", m_weight);
  
  return StatusCode::SUCCESS;
}



//----------------------------------------------------------------------

void TTbarEventReweightingToolParticleLevel::multiplyEventWeights(const double weight) {
  m_weight*=weight;
}
