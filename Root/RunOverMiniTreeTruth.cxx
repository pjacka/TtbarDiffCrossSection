#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <math.h>
#include <iomanip>      // std::setprecision
#include <utility>

#include "TString.h"
#include "TChain.h"
#include "TEnv.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TSystem.h"
#include "TH1D.h"


#include "TtbarDiffCrossSection/RunOverMiniTreeTruth.h"
ClassImp(RunOverMiniTreeTruth)

using namespace std;

RunOverMiniTreeTruth::RunOverMiniTreeTruth(TString filelistname, TString outputName, int nEvents)
  : m_filelistname(filelistname), m_outputName(outputName) {

  if (m_debug > 0) cout << endl << "****** RunOverMiniTreeTruth::RunOverMiniTreeTruth *******" << endl;
  
  string s = m_outputName.Data();
  size_t found = s.find_last_of("/");
  if (m_debug > 0) cout << " path: " << s.substr(0,found+1) << endl;
  if (m_debug > 0) cout << " file: " << s.substr(found+1)   << endl;
  
  if (m_debug > 0) cout << "m_outputName: " << m_outputName << endl;

  //should go to config...
  m_debug = 5;
  m_treeName = "truth";
  m_GeV   = 1000;

  m_rootCoreBin = gSystem->Getenv("ROOTCOREBIN");
  ReadConfig();

  m_chain_parton = new TChain(); 
  LoadTree();

  Initialise();
  
  int nEvents_in_chain_parton = m_chain_parton->GetEntries();

  m_nEvents_parton = nEvents;
  if (nEvents < 0 || nEvents > nEvents_in_chain_parton)  m_nEvents_parton = nEvents_in_chain_parton;

  BookHistos();

}



int RunOverMiniTreeTruth::ReadConfig(){
  /*
  if (m_debug>0) cout << endl << " ***** RunOverMiniTreeTruth::ReadConfig: Parameters read from config: *****" << endl;

  TEnv config(m_rootCoreBin+"/data/TtbarDiffCrossSection/config.env");
  TEnv cuts(m_rootCoreBin+"/data/TtbarDiffCrossSection/cuts.env");
  m_debug=config.GetValue("Debug", 1);
  */

  return 0;
}



RunOverMiniTreeTruth::~RunOverMiniTreeTruth(){
  delete m_output;
}

int RunOverMiniTreeTruth::LoadTree(){

  if (m_debug > 0) cout << endl << " ***** RunOverMiniTreeTruth::LoadTrees *****" << endl;

  // load the filelist
  vector<string> fileList;
  ifstream mystream(m_filelistname.Data());

  if (m_debug > 0) cout << "Reading filelist " << m_filelistname << endl;

  TString str_item;
  while ( mystream >> str_item) {
    if (m_debug > 0) cout << "Adding file " << str_item.Data() << "  to the list of files" << endl;
    fileList.push_back(str_item.Data());
  }
  mystream.close();

  if (m_debug > 0) cout << "name of TTree: " << m_treeName << endl;

  for (unsigned int i = 0; i < fileList.size(); i++){   

    TString filename = fileList[i].c_str();

    if (m_debug > 1) cout << "...trying to add file to chain " << fileList[i].c_str() << endl;

    if (m_debug > 1) cout << "initializing TFile" << endl;

    TFile* file = TFile::Open(filename);
    if (file->IsZombie()) {
      cout << "Error: file is Zombie!" << endl; 
      cout << "Exiting now!" << endl;
      exit(EXIT_FAILURE);
    }

    m_chain_parton->AddFile(filename, TChain::kBigNumber, m_treeName); 
  }

  if (m_debug > 0) cout << "Done adding files to chain." << endl;

  return 0;
}


int RunOverMiniTreeTruth::Initialise() {

  if (m_debug > 0) cout << endl << " ***** RunOverMiniTreeTruth::Initialise() *****" << endl;

  m_output = new TFile(m_outputName,"RECREATE","truth histos", 1); 
  
  m_partonLevelInfo = std::make_unique<ReconstructedEvent>();

  if (m_debug > 0) cout << "initializing Minitree" << endl;
  
  m_parton = new Minitree_truth(m_chain_parton);
  m_parton->InitTops("afterFSR");
  m_parton->InitSCTops("afterFSR");

  return 0;
}


int RunOverMiniTreeTruth::BookHistos()
{
  //to create hists in file (not in memory)
  m_output->cd();

  h_top_pt_cmp   = new TH1D("h_top_pt_cmp" ,"truth top pt after FSR (last top - top w/ Status code)" , 100, -0.1, 0.1); //GeV
  h_top_eta_cmp  = new TH1D("h_top_eta_cmp","truth top eta after FSR (last top - top w/ Status code)", 100, -0.001, 0.001);
  h_top_phi_cmp  = new TH1D("h_top_phi_cmp","truth top phi after FSR (last top - top w/ Status code)", 100, -0.1, 0.1);
  h_top_m_cmp    = new TH1D("h_top_m_cmp"  ,"truth top m after FSR (last top - top w/ Status code)"  , 100, -0.1, 0.1);

  h_top_pt    = new TH1D("h_top_pt"  ,"truth top pt after FSR (last top in chain)"    , 100, 0.0, 1000.0);
  h_top_eta   = new TH1D("h_top_eta" ,"truth top eta after FSR (last top in chain)"    , 100, -5.0, 5.0);
  h_top_phi   = new TH1D("h_top_phi" ,"truth top phi after FSR (last top in chain)"    , 100, -4.0, 4.0);
  h_top_m     = new TH1D("h_top_m"   ,"truth top m after FSR (last top in chain)"    , 100, 100.0, 200.0);

  h_top_pt_SC  = new TH1D("h_top_pt_SC"  ,"truth top pt after FSR (according status code)"  , 100, 0.0, 1000.0);
  h_top_eta_SC = new TH1D("h_top_eta_SC" ,"truth top eta after FSR (according status code)" , 100, -5.0, 5.0);
  h_top_phi_SC = new TH1D("h_top_phi_SC" ,"truth top phi after FSR (according status code)" , 100, -4.0, 4.0);
  h_top_m_SC   = new TH1D("h_top_m_SC"   ,"truth top m after FSR (according status code))"  , 100, 100.0, 200.0);

  h_top_pt_cmp  ->SetXTitle("p_{T}^{TOP}(last) - p_{T}^{TOP}(SC) [GeV]");
  h_top_eta_cmp ->SetXTitle("#eta^{TOP}(last) - #eta^{TOP}(SC)");
  h_top_phi_cmp ->SetXTitle("#phi^{TOP}(last) - #phi^{TOP}(SC)");
  h_top_m_cmp   ->SetXTitle("m^{TOP}(last) - m^{TOP}(SC) [GeV]");

  return 0;
}


int RunOverMiniTreeTruth::Execute(){

  if (m_debug > 0) cout << endl << " ***** RunOverMiniTreeTruth::Execute() *****" << endl;

  //??? chain->ResetBranchAddresses();

  //int nSelectedEvents_parton=0;

  if (m_debug > 0) cout << endl << "looping in parton TTree" << endl;

  for (Long64_t ievent=0; ievent < m_nEvents_parton; ++ievent)  {

    if (ievent % 50000 == 0) {
      TString progress = Form("%3.2f", ievent/float(m_nEvents_parton) * 100);
      if (m_debug > 0) cout << "--> processing: " << ievent << " / " << m_nEvents_parton
			    << " ( " << progress.Data() << " % )" << endl;
    }

    m_parton->GetEntry(ievent);

    if (ievent == 0)  {
      int channelNumber = m_parton->mcChannelNumber;
      
      if (m_debug > 0) cout << "channel number: " << channelNumber << endl;
      
      //double xsecWithKfactor = this->GetCrossSection(channelNumber);
    }

    TLorentzVector top, antitop;

    top    .SetPtEtaPhiM(m_parton->MC_t_pt    / m_GeV, m_parton->MC_t_eta    , m_parton->MC_t_phi    ,m_parton->MC_t_m    /m_GeV);
    antitop.SetPtEtaPhiM(m_parton->MC_tbar_pt / m_GeV, m_parton->MC_tbar_eta , m_parton->MC_tbar_phi ,m_parton->MC_tbar_m /m_GeV);
      
    TLorentzVector leadingTop    = top;
    TLorentzVector subleadingTop = antitop;
    if (top.Pt() < antitop.Pt()){
      leadingTop    = antitop;
      subleadingTop = top;
    }

    //if (!Passed_PartonLevel_Selections(leadingTop,subleadingTop))continue;

    FillHistos();
  } //end loop over parton events

  /*
  cout << "Real time in event loop: " << time.RealTime() << endl; 
  cout << "Number of events in the minitree after all reco cuts: " << eventsAfterRecoCutInThisMinitree  << endl;
  cout << "Number of events in the reco minitree: " <<  m_nEvents_reco << endl;
  cout << "Selected Events: " << nSelectedEvents << endl;
  cout << "Skipped Events: " << skippedEvents << endl;
  cout << "Selected Events weighted by MC weight: " << nSelectedEvents_MCweight << endl;
  cout << "Total number of events used for weighting: " << m_nEvents_total << endl;
  cout << "Cross section with K-factor: " <<  xsecWithKfactor << endl;
  */
  
  if (m_debug > 0) cout << endl << " ***** end of RunOverMiniTreeTruth::Execute() *****" << endl;

  return 0;
}



int RunOverMiniTreeTruth::FillHistos()
{
  double weight = m_parton->weight_mc;

  if (m_debug > 10) cout << "event weight: " << weight << endl;
  if (m_debug > 11) cout << "top pt: " << m_parton->MC_t_pt << " " << m_parton->MC_t_SC_pt << endl;
 
  h_top_pt_cmp  ->Fill( (m_parton->MC_t_pt  - m_parton->MC_t_SC_pt) / m_GeV , weight);
  h_top_eta_cmp ->Fill(m_parton->MC_t_eta - m_parton->MC_t_SC_eta, weight);
  h_top_phi_cmp ->Fill(m_parton->MC_t_phi - m_parton->MC_t_SC_phi, weight);
  h_top_m_cmp   ->Fill( (m_parton->MC_t_m   - m_parton->MC_t_SC_m) / m_GeV  , weight);

  h_top_pt_cmp  ->Fill( (m_parton->MC_tbar_pt  - m_parton->MC_tbar_SC_pt) / m_GeV , weight);
  h_top_eta_cmp ->Fill(m_parton->MC_tbar_eta - m_parton->MC_tbar_SC_eta, weight);
  h_top_phi_cmp ->Fill(m_parton->MC_tbar_phi - m_parton->MC_tbar_SC_phi, weight);
  h_top_m_cmp   ->Fill( (m_parton->MC_tbar_m   - m_parton->MC_tbar_SC_m) /m_GeV  , weight);

  h_top_pt    ->Fill(m_parton->MC_t_pt / m_GeV   , weight);
  h_top_eta   ->Fill(m_parton->MC_t_eta   , weight);
  h_top_phi   ->Fill(m_parton->MC_t_phi   , weight);
  h_top_m     ->Fill(m_parton->MC_t_m  / m_GeV   , weight);

  h_top_pt    ->Fill(m_parton->MC_tbar_pt  / m_GeV    , weight);
  h_top_eta   ->Fill(m_parton->MC_tbar_eta   , weight);
  h_top_phi   ->Fill(m_parton->MC_tbar_phi   , weight);
  h_top_m     ->Fill(m_parton->MC_tbar_m   / m_GeV    , weight);

  h_top_pt_SC  ->Fill(m_parton->MC_t_SC_pt  / m_GeV    , weight);
  h_top_eta_SC ->Fill(m_parton->MC_t_SC_eta   , weight);
  h_top_phi_SC ->Fill(m_parton->MC_t_SC_phi   , weight);
  h_top_m_SC   ->Fill(m_parton->MC_t_SC_m   / m_GeV    , weight);

  h_top_pt_SC  ->Fill(m_parton->MC_tbar_SC_pt  / m_GeV   , weight);
  h_top_eta_SC ->Fill(m_parton->MC_tbar_SC_eta   , weight);
  h_top_phi_SC ->Fill(m_parton->MC_tbar_SC_phi   , weight);
  h_top_m_SC   ->Fill(m_parton->MC_tbar_SC_m   / m_GeV   , weight);

  if (m_debug > 3) {
    if ( (m_parton->MC_t_pt  - m_parton->MC_t_SC_pt) != 0.0)  {
      cout << m_parton->runNumber << " " << m_parton->eventNumber << " "
	   << m_parton->MC_t_pt << " " << m_parton->MC_t_SC_pt << endl;
    }
  }
  return 0;
}

int RunOverMiniTreeTruth::Finalise() {

  if (m_debug > 0) cout << endl << " ***** RunOverMiniTreeTruth::Finalise() *****" << endl;

  if (m_debug > 0) cout << "writing histograms to output file" << endl;

  m_output->Write();
  m_output->Close();

  if (m_debug > 0) cout << endl << " ***** end of RunOverMiniTreeTruth::Finalise() *****" << endl;

  return 0;
}


/*
void RunOverMiniTreeTruth::GetObjectsFromMinitree(Minitree* t,ReconstructedEvent* level, bool isReco){
  if (m_debug>4) cout << endl << " ***** RunOverMiniTreeTruth::GetObjectsFromMinitree() *****" << endl;
        
  int iLeading=-1;
  int iRecoil=-1;  
  int nFatJets=t->ljet_pt->size();
  //if(nFatJets < 2) cout << "nFatJets: " << nFatJets << endl;
  level->SetMET(t->met_met/1000.);
  
  //vector<pair<double,int> > pt_fatjets;
  vector<FatJet> fatJets;
  float ljets_Ht(0.);
  int counter(0);
  for (int ijet=0;ijet<nFatJets;++ijet){
    // Object selections
    if(abs(t->ljet_eta->at(ijet)) > 2.0 ) continue;
    if(t->ljet_pt->at(ijet) > 3000000.) continue;
    //if(t->ljet_m->at(ijet) < 50000.) continue;
    if(t->ljet_pt->at(ijet) < 250000.) continue;
    if(t->ljet_m->at(ijet) > t->ljet_pt->at(ijet)) continue;
    counter++;
    //FatJet found_ljet(t->ljet_pt->at(ijet)/1000.,t->ljet_eta->at(ijet),t->ljet_phi->at(ijet),t->ljet_m->at(ijet)/1000.,t->ljet_tau32->at(ijet),t->ljet_tau21->at(ijet),t->ljet_sd12->at(ijet),t->ljet_topTag50->at(ijet),t->ljet_topTag80->at(ijet),isReco ? t->ljet_isWTaggedMed->at(ijet) : 0);
    FatJet found_ljet(t->ljet_pt->at(ijet)/1000.,t->ljet_eta->at(ijet),t->ljet_phi->at(ijet),t->ljet_m->at(ijet)/1000.,t->ljet_tau32->at(ijet),t->ljet_tau21->at(ijet),0.,t->ljet_topTag50->at(ijet),t->ljet_topTag80->at(ijet),isReco ? t->ljet_isWTaggedMed->at(ijet) : 0);
    if (isReco) found_ljet.SetSplit12( t->ljet_sd12->at(ijet) /1000.);
    fatJets.push_back(found_ljet);
    if(counter==1)iLeading = ijet;
    if(counter==2)iRecoil = ijet;
    ljets_Ht += t->ljet_pt->at(ijet)/1000.;
    //cout << "ljets_Ht: " << ljets_Ht << endl;
    //pt_fatjets.push_back(std::make_pair(t->ljet_pt->at(ijet),ijet));
    if(ijet>0 && t->ljet_pt->at(ijet) > t->ljet_pt->at(ijet-1)) cout << "Warning: ljets are not ordered according to pt" << endl;
  }

  const int n_fatjets= fatJets.size();
  //if(fatJets.size() > 1)cout << fatJets[0].Pt() << " " << fatJets[1].Pt() << endl;
  
  //vector<FatJet> ljets = level->GetFatJets();
  //if(fatJets.size() > 1)cout << ljets[0].Pt() << " " << ljets[1].Pt() << endl;
  //if(fatJets.size() > 1)cout << level->GetFatJet1().Pt() << " " << level->GetFatJet2().Pt() << endl;
   level->Set_ljets_Ht(ljets_Ht);
  //sort(pt_fatjets.begin(),pt_fatjets.end(),function_used_for_sorting);
  //if (nFatJets>0) iLeading=pt_fatjets[0].second;
  //if (nFatJets>1) iRecoil=pt_fatjets[1].second;
  //cout << "pt_fatjets: " << t->ljet_pt->at(iLeading) << endl;
  //cout << "pt_fatjets: " << t->ljet_pt->at(iLeading) << endl;
  
  
 //if (m_debug>5) cout << "RunOverMiniTreeTruth::GetObjectsFromMinitree(): getting large-R jets" << endl;
 // if (iLeading>=0)   level->SetFatJet1(t->ljet_pt->at(iLeading)/1000.,t->ljet_eta->at(iLeading),t->ljet_phi->at(iLeading),t->ljet_e->at(iLeading)/1000.,t->ljet_tau32->at(iLeading),t->ljet_tau21->at(iLeading),0.,t->ljet_topTag50->at(iLeading),t->ljet_topTag80->at(iLeading));
 // else   level->SetFatJet1(1e-4,1e-4,0,1e-3,-1,-1,-1,0,0);
 // if (iRecoil>=0)   level->SetFatJet2(t->ljet_pt->at(iRecoil)/1000.,t->ljet_eta->at(iRecoil),t->ljet_phi->at(iRecoil),t->ljet_e->at(iRecoil)/1000.,t->ljet_tau32->at(iRecoil),t->ljet_tau21->at(iRecoil),0.,t->ljet_topTag50->at(iRecoil),t->ljet_topTag80->at(iRecoil));
 // else   level->SetFatJet2(1e-4,1e-4,0,1e-3,-1,-1,-1,0,0);
  
  if (m_debug>5) cout << "RunOverMiniTreeTruth::GetObjectsFromMinitree(): getting small-R jets" << endl;
  vector<TLorentzVector> bjets;
  vector<TLorentzVector> jets;
  level->SetJetFirst(0.,0.,0.,0.);
  float jets_Ht(0.);
  for (int ijet=0;ijet<(int)t->jet_pt->size();++ijet){
    if (t->jet_pt->at(ijet) < 25000. || abs(t->jet_eta->at(ijet))>2.5 ) continue;
    TLorentzVector jet;
    jets_Ht += t->jet_pt->at(ijet)/1000.;
    if (m_debug>6) cout << "RunOverMiniTreeTruth::GetObjectsFromMinitree(): jet " << ijet << endl;
    
    jet.SetPtEtaPhiE(t->jet_pt->at(ijet)/1000.,t->jet_eta->at(ijet),t->jet_phi->at(ijet),t->jet_e->at(ijet)/1000.);
    if (m_debug>6) cout << "RunOverMiniTreeTruth::GetObjectsFromMinitree(): found 4-momentum for jet " << ijet << endl;
    jets.push_back(jet);
    if (isReco){
      //if(t->jet_mv2c20->at(ijet)>0.7110) bjets.push_back(jet);  //70% working point
      //if(t->jet_mv2c20->at(ijet)>0.4803) bjets.push_back(jet);  //77% working point
      if (t->jet_mv2c10->at(ijet)>0.8244) bjets.push_back(jet);  // 70% working point
      //if (t->jet_mv2c10->at(ijet)>0.6459) bjets.push_back(jet);  // 77% working point
      //if (t->jet_mv2c20->at(ijet)>-0.4434) bjets.push_back(jet);  // 77% working point
      //if (t->jet_mv2c20->at(ijet)>-0.0436) bjets.push_back(jet);  // 70% working point (as it is in TTbarDiffXsTools 7.12.2015)
    }
    else{
      if (t->jet_nGhosts_bHadron->at(ijet)>0) bjets.push_back(jet);
    }
    if (ijet==0)  level->SetJetFirst(jet);
  }
  if(t->el_pt->size() >0 || t->mu_pt->size() > 0) cout << "Found lepton" << endl;
  if(n_fatjets>0) fatJets[0].SetLeadingSubjet(jets);
  if(n_fatjets>1) fatJets[1].SetLeadingSubjet(jets);
  level->SetFatJets(fatJets);
  level->Set_el_n(t->el_pt->size());
  level->Set_mu_n(t->mu_pt->size());
  level->Set_jets_Ht(jets_Ht);
  level->SetNJets(jets.size());
  level->SetNBJets(bjets.size());
  level->SetJets(jets);
  level->SetBJets(bjets); 
  level->SetFatJetsBtagInfo(); // Must be after level->SetBJets(bjets);
	//if(iRecoil>0){
		//FatJet leading = level->GetFatJet1();
		//FatJet recoil = level->GetFatJet2();
		//if(leading.TopTag50Info())cout << leading.Pt() << " " << leading.Eta() << " " << leading.IsBTagged() << " " << leading.TopTag50Info() << endl;
	//}
  if(!isReco){
		//cout << t->ljet_nGhosts_bHadron->size() << " " << iLeading << endl;
		//if(iLeading >=0 ) cout << t->ljet_nGhosts_bHadron->at(iLeading) << endl;
		//if(iRecoil >0 ) cout << t->ljet_nGhosts_bHadron->at(iRecoil) << endl;
		//if(iLeading >=0 && t->ljet_nGhosts_bHadron->at(iLeading) > 0) cout << t->ljet_nGhosts_bHadron->at(iLeading) << endl;
		level->SetFatJetsBtagInfoParticle(iLeading >=0 && t->ljet_nGhosts_bHadron->at(iLeading)>0,iRecoil>0 && t->ljet_nGhosts_bHadron->at(iRecoil)>0);   
  }
  if (m_debug>4) cout << "RunOverMiniTreeTruth::GetObjectsFromMinitree(): all objects found" << endl;
}
*/


/*
void RunOverMiniTreeTruth::FindParticleLevel(ULong64_t eventNumber,bool& passed_particleLevel_cuts,vector<int> &selections_passed_ABCDsimple_tight_particle,vector<int> &selections_passed_ABCD16_tight_particle){
	if (m_debug>4) cout << endl << " ***** RunOverMiniTreeTruth::FindParticleLevel() *****" << endl;
	Long64_t imatching=this->FindIndex(eventNumber,m_particle_indeces);
	if (imatching<0){
		for(int i=0; i<(int)selections_passed_ABCDsimple_tight_particle.size();i++) selections_passed_ABCDsimple_tight_particle[i]=0;
		for(int i=0; i<(int)selections_passed_ABCD16_tight_particle.size();i++) selections_passed_ABCD16_tight_particle[i]=0;
		passed_particleLevel_cuts=0;
		return;
	}
	m_particle->GetEntryParticle(imatching);
	//m_particle->GetEntry(imatching);
	this->GetObjectsFromMinitree(m_particle,m_particleLevel,false);
	//this->Make_Selections_ABCDsimple(selections_passed_ABCDsimple_loose_particle, m_particle, 80);
	//this->Make_Selections_ABCDsimple(selections_passed_ABCDsimple_tight_particle, m_particle, 50);
	FatJet LeadingJet= m_particleLevel->GetFatJet1();
	FatJet RecoilJet= m_particleLevel->GetFatJet2();
  
	
	passed_particleLevel_cuts = Passed_ParticleLevel_Selections(LeadingJet,RecoilJet,m_particleLevel->GetNJets(),m_h_cutflow_particle_after_reco,m_h_cutflow_noweights_particle_after_reco);
	if(passed_particleLevel_cuts){
		
		bool tagged=LeadingJet.IsBTagged() && RecoilJet.IsBTagged();
		//bool tagged=LeadingJet.IsBTagged() && RecoilJet.IsBTagged() && LeadingJet.TopTag50Info() && RecoilJet.TopTag50Info();
		//bool tagged=1;
		selections_passed_ABCDsimple_tight_particle[8]=tagged;
		selections_passed_ABCD16_tight_particle[15]=tagged;
		
	}

}



bool RunOverMiniTreeTruth::FindPartonLevel(ULong64_t eventNumber){
  if (m_debug>2) cout << endl << "RunOverMiniTreeTruth::FindPartonLevel()" << endl;
  Long64_t imatching=this->FindIndex(eventNumber,m_parton_indeces);
  if (imatching<0){
    //cout << "WARNING: event number not found in parton TTree!!! Something is wrong!" << endl;
    return false;
  }
  m_parton->GetEntry(imatching);
  vector<FatJet> fatJets;

  if (m_parton->MC_t_pt>m_parton->MC_tbar_pt){
    fatJets.push_back(FatJet(m_parton->MC_t_pt/1000.,m_parton->MC_t_eta,m_parton->MC_t_phi,m_parton->MC_t_m/1000.,-1,-1,-1,-1,-1,0));
    fatJets.push_back(FatJet(m_parton->MC_tbar_pt/1000.,m_parton->MC_tbar_eta,m_parton->MC_tbar_phi,m_parton->MC_tbar_m/1000.,-1,-1,-1,-1,-1,0));	  
    // m_partonLevel->SetFatJet1(m_parton->MC_t_pt/1000.,m_parton->MC_t_eta,m_parton->MC_t_phi,m_parton->MC_t_m/1000.,-1,-1,-1,-1,-1,0);
    // m_partonLevel->SetFatJet2(m_parton->MC_tbar_pt/1000.,m_parton->MC_tbar_eta,m_parton->MC_tbar_phi,m_parton->MC_tbar_m/1000.,-1,-1,-1,-1,-1,0);
  }
  else{
    fatJets.push_back(FatJet(m_parton->MC_tbar_pt/1000.,m_parton->MC_tbar_eta,m_parton->MC_tbar_phi,m_parton->MC_tbar_m/1000.,-1,-1,-1,-1,-1,0));
    fatJets.push_back(FatJet(m_parton->MC_t_pt/1000.,m_parton->MC_t_eta,m_parton->MC_t_phi,m_parton->MC_t_m/1000.,-1,-1,-1,-1,-1,0));
    // m_partonLevel->SetFatJet1(m_parton->MC_tbar_pt/1000.,m_parton->MC_tbar_eta,m_parton->MC_tbar_phi,m_parton->MC_tbar_m/1000.,-1,-1,-1,-1,-1,0);
    // m_partonLevel->SetFatJet2(m_parton->MC_t_pt/1000.,m_parton->MC_t_eta,m_parton->MC_t_phi,m_parton->MC_t_m/1000.,-1,-1,-1,-1,-1,0);
  }
  m_partonLevel->SetFatJets(fatJets);


  TLorentzVector vec(0,0,0,0);
  m_partonLevel->SetJetFirst(vec);

  return true;
}



Long64_t RunOverMiniTreeTruth::FindIndex(ULong64_t eventNumber,vector<pair<ULong64_t,Long64_t> > const &indeces){
  if (m_debug>2) cout << endl << "RunOverMiniTreeTruth::FindIndex()" << endl;
  Long64_t index=-1;
  Long64_t n=indeces.size();
  int nIterations=2*log(n)/log(2);
  Long64_t lowerBound=0;
  Long64_t upperBound=n-1;
  //cout << "event Number: " << eventNumber << "  nIterations: " <<  nIterations << endl;
  for (int i=0;i<nIterations;++i){
    Long64_t test=(upperBound+lowerBound)/2;
    //  cout << i << "   testing event number from truth TTree: " << indeces[test].first << " lowerBound: " << lowerBound << " upperBound: " << upperBound  << endl;
    ULong64_t test_eventNumber=indeces[test].first;
    if (eventNumber==test_eventNumber){
      index=indeces[test].second;
      break;
    }
    if (eventNumber==indeces[test+1].first){
      index=indeces[test+1].second;
      break;
    }
    if (eventNumber>test_eventNumber) lowerBound=test;
    else upperBound=test;
  }
  if (m_debug>5){
    if (index<0) cout << "RunOverMiniTreeTruth::FindIndex: event number not found in truth TTree" << endl;
    else  cout << "RunOverMiniTreeTruth::FindIndex:  found index: " << index << endl;
  }
  return index;
}
*/


/*
double RunOverMiniTreeTruth::GetCrossSection(int channel){
  double xsec=-1;
  double kfactor=1;
  double filter=1;
 switch(channel){
  case 410007: case 410008: xsec=696.21;filter=0.457; kfactor=1.195; break;
  //case 410007: case 410008: xsec=695.99; kfactor=1.1977; filter=0.457; break;
  case 410024: xsec= 	315.99  ; kfactor=1.17; break;
  case 410043: xsec= 	715.49 ; filter=0.4562; break;
  case 410044: xsec=677.55  ; filter=0.45618; break;
  case 410045: xsec=650.62 ; filter=0.45617; break;
  case 410046: xsec=608.53  ; filter=0.45625; break;
  case 410160: xsec=679.12   ; filter=0.45589;kfactor=1.2248;  break;
  case 410234: xsec=248.87;filter=0.457; kfactor=3.3421; break;
  case 410506: xsec=249.89; kfactor=1.521; break;
  case 410521: xsec=281.98 ; kfactor=1.348; break;
  case 410522: xsec=219.1; kfactor=1.735; break;
  case 410530: xsec=249.88; kfactor=1.521; break;
//  case 410161: xsec=679.12   ; filter=0.45589;kfactor=1.2248  break;
//  case 410162: xsec=679.12   ; filter=0.45589;kfactor=1.2248  break;
//  case 410163: xsec=679.12   ; filter=0.45589;kfactor=1.2248  break;
  case 410000: case 410001: case 410002: xsec=377.9932; kfactor=1.1949; break;
  case 410013: xsec=34.009; kfactor=1.054; break;
  case 410014: xsec=33.989; kfactor=1.054; break;
  case 410066: xsec=0.17656; kfactor=1.247; break;
  case 410067: xsec=0.14062; kfactor=1.247; break;
  case 410068: xsec=0.1368; kfactor=1.247; break;
  case 410080: xsec=0.0091622; kfactor=1.0042 ; break;
  case 410081: xsec=0.0080975; kfactor=1.2231; break;
  case 426001: xsec=421630000000; filter=0.0034956; break;
  case 426002: xsec=48024000000; filter=0.0004107; break;
  case 426003: xsec=1908400000; filter=0.00014988; break;
  case 426004: xsec=173620000; filter=1.8654e-05; break;
  case 426005: xsec=5259400; filter=1.1846e-05; break;
  case 426006: xsec=276120; filter=8.3645e-06; break;
  case 426007: xsec=26410; filter=6.671e-06; break;
  case 426008: xsec=77.298; filter=8.3211e-05; break;
  case 426009: xsec=2.747; filter=9.0501e-05; break;
  case 304623: xsec=23.146; filter=1.0; break;
  case 304624: xsec=1.4823; filter=1.0; break;
  case 304625: xsec=0.27757; filter=1.0; break;
  case 304626: xsec=0.035315; filter=1.0; break;
  case 304627: xsec=0.0038008; filter=1.0; break;
  case 304628: xsec=10.455; filter=1.0; break;
  case 304629: xsec=0.67488; filter=1.0; break;
  case 304630: xsec=0.12507; filter=1.0; break;
  case 304631: xsec=0.015797; filter=1.0; break;
  case 304632: xsec=0.001705; filter=1.0; break;
  case 301322: xsec=8.9804; filter=1.0; break;
  case 301323: xsec=8.7408; filter=1.0; break;
  case 301324: xsec=3.1229; filter=1.0; break;
  case 301325: xsec=1.1247; filter=1.0; break;
  case 301326: xsec=0.46031; filter=1.0; break;
  case 301327: xsec=0.20689; filter=1.0; break;
  case 301328: xsec=0.099997; filter=1.0; break;
  case 301329: xsec=0.051336; filter=1.0; break;
  case 301330: xsec=0.027465; filter=1.0; break;
  case 301331: xsec=0.015241; filter=1.0; break;
  case 301332: xsec=0.0086794; filter=1.0; break;
  case 361021: xsec=78420000000.0; filter=0.00065705; break;
  case 361022: xsec=2433100000.0; filter=0.00033141; break;
  case 361023: xsec=26454000.0; filter=0.00032182; break;
  case 361024: xsec=254630.0; filter=0.00052982; break;
  case 361025: xsec=4553.5; filter=0.00092255; break;
  case 361026: xsec=257.53; filter=0.00094016; break;
  case 361027: xsec=16.215; filter=0.00039282; break;
  case 361028: xsec=0.62502; filter=0.010162; break;
  case 361029: xsec=0.019639; filter=0.012054; break;
  case 361030: xsec=0.0011962; filter=0.0058935; break;
  case 361031: xsec=4.2258e-05; filter=0.0027015; break;
  case 361032: xsec=1.0367e-06; filter=0.00042502; break;
  case 343365: xsec=0.05343; break;
  case 343366: xsec=0.22276; break;
  case 343367: xsec=0.23082; break;
  case 410155: xsec=0.5483;kfactor=1.1; break;
  case 410157: xsec=0.52771;kfactor=1.11; break;

  default: cout << "Error in function RunOverMiniTreeTruth::GetCrossSection: Unknown channel number" << endl;
 }

  return xsec*kfactor*filter;
}


bool RunOverMiniTreeTruth::function_used_for_sorting(std::pair<double,int> i,std::pair<double, int> j){
  return (i.first > j.first);
}


bool RunOverMiniTreeTruth::function_used_for_sorting_indeces(std::pair<ULong64_t,Long64_t> i,std::pair<ULong64_t,Long64_t> j){
  return (i.first < j.first);
}

*/
