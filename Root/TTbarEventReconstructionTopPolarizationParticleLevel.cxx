#include "TtbarDiffCrossSection/TTbarEventReconstructionTopPolarizationParticleLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventReconstructionTopPolarizationParticleLevel)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventReconstructionTopPolarizationParticleLevel::TTbarEventReconstructionTopPolarizationParticleLevel(const std::string& name) : TTbarEventReconstructionClosestMass(name)
{
  m_minSubjetPtFrac = 0.;
  
}

//----------------------------------------------------------------------

StatusCode TTbarEventReconstructionTopPolarizationParticleLevel::readJSONConfig(const nlohmann::json& json) {
  
  try {
    if(!TTbarEventReconstructionClosestMass::readJSONConfig(json)) {
      throw std::runtime_error("TTbarEventReconstructionTopPolarizationParticleLevel: Failed to read TTbarEventReconstructionClosestMass configuration.");
    }
    
    m_minSubjetPtFrac = jsonFunctions::readValueJSON<double>(json, "minSubjetPtFrac", m_minSubjetPtFrac);
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventReconstructionTopPolarizationParticleLevel::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventReconstructionTopPolarizationParticleLevel::execute(ReconstructedEvent& event) const {
  // Executing object selection algorithm
  TTbarEventReconstructionClosestMass::execute(event);
  
  //std::cout << "TTbarEventReconstructionTopPolarizationParticleLevel::execute" << std::endl;
  
  if(event.isAvailable("top1") && event.isAvailable("top2")) {
  
    Object_t& top1 = event.getRef<Object_t>("top1");
    Object_t& top2 = event.getRef<Object_t>("top2");
    
    if(!(top1.isAvailable("MatchedSmallRJets") && top2.isAvailable("MatchedSmallRJets"))) {
      throw std::runtime_error("TTbarEventReconstructionTopPolarizationParticleLevel: Top quarks has no subjets.");
    }
    
    
    const ObjectContainer_t& top1MatchedSmallRJets = top1.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
    const ObjectContainer_t& top2MatchedSmallRJets = top2.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
    
    auto findSubjetIndex = [](const ObjectContainer_t& jets, const double ptMin) {
      
      int bCandidateIndex = -1;
      double currentPt = -FLT_MAX;
      for(size_t i = 0; i < jets.size();i++) {
        if(jets[i]->getAttribute<bool>("BTagged")) {
          const double jetPt = jets[i]->Pt();
          if(jetPt > std::max(currentPt, ptMin)) {
            currentPt = jetPt;
            bCandidateIndex = i;
          }
        }
      }
      return bCandidateIndex;
    };
    
    
    if(top1MatchedSmallRJets.size() > 0) {
      
      const double ptMin = m_minSubjetPtFrac * top1.Pt();
      
      const int bCandidateIndex = findSubjetIndex(top1MatchedSmallRJets, ptMin);
            
      std::shared_ptr<Object_t> WCandidate = 
        ReconstructedObjects::getSupplementaryObject(top1MatchedSmallRJets, bCandidateIndex, ptMin);
      
      if(bCandidateIndex >= 0) {
        std::shared_ptr<Object_t> bCandidate = top1MatchedSmallRJets[bCandidateIndex];
        top1.setPtr<Object_t>("BCandidate", bCandidate);
      }
      
      top1.setAttribute<int>("BCandidate_index", bCandidateIndex);
      
      top1.setPtr<Object_t>("WCandidate", WCandidate);
    }
    
    if(top2MatchedSmallRJets.size() > 0) {
      
      const double ptMin = m_minSubjetPtFrac * top2.Pt();
      
      const int bCandidateIndex = findSubjetIndex(top2MatchedSmallRJets, ptMin);
            
      std::shared_ptr<Object_t> WCandidate = 
        ReconstructedObjects::getSupplementaryObject(top2MatchedSmallRJets, bCandidateIndex, ptMin);
      
      if(bCandidateIndex >= 0) {
        std::shared_ptr<Object_t> bCandidate = top2MatchedSmallRJets[bCandidateIndex];
        top2.setPtr<Object_t>("BCandidate", bCandidate);
      }
      
      top2.setAttribute<int>("BCandidate_index", bCandidateIndex);
      
      top2.setPtr<Object_t>("WCandidate", WCandidate);
    }
    
  }
}



