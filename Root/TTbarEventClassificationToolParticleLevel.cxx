#include "TtbarDiffCrossSection/TTbarEventClassificationToolParticleLevel.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"
#include "NtuplesAnalysisToolsCore/LargeRJet.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

ClassImp(TTbarEventClassificationToolParticleLevel) typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventClassificationToolParticleLevel::TTbarEventClassificationToolParticleLevel(const std::string &name) : EventClassificationToolBase(name)
{
  m_selections.resize(16, 0);
  m_selectionNames = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"};
}

//----------------------------------------------------------------------

StatusCode TTbarEventClassificationToolParticleLevel::readJSONConfig(const nlohmann::json &json)
{
  try
  {
    if (!EventClassificationToolBase::readJSONConfig(json))
      return StatusCode::FAILURE;
    m_bTagDecoration = jsonFunctions::readValueJSON<std::string>(json, "BTagDecoration");

    if (m_selectionNames.size() != 16)
      throw std::out_of_range("Error in TTbarEventClassificationToolParticleLevel constructor: m_selectionNames must have a length of 16.");
  }
  catch (const std::runtime_error &e)
  {
    throw std::runtime_error((std::string) "Error in TTbarEventClassificationToolParticleLevel::readJSONConfig:\n" + e.what());
  }
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventClassificationToolParticleLevel::execute(const ReconstructedEvent &event)
{
  try
  {
    if (!event.isAvailable("top1") || !event.isAvailable("top2"))
      throw(std::out_of_range("TTbarEventClassificationToolParticleLevel::execute: 'top1' or 'top2' objects not found!"));

    const Object_t &top1 = event.getConstRef<Object_t>("top1");
    const Object_t &top2 = event.getConstRef<Object_t>("top2");

    m_selections.assign(m_selections.size(), 0); // Reset selections to zero

    m_selections[m_signalRegionIndex] = top1.getAttribute<bool>(m_bTagDecoration) * top2.getAttribute<bool>(m_bTagDecoration);
  }
  catch (const std::out_of_range &e)
  {
    throw std::out_of_range{(std::string)"TTbarEventClassificationToolParticleLevel::execute: " + e.what()};
  } catch (const std::bad_any_cast &e)
  {
    throw std::out_of_range{(std::string)"TTbarEventClassificationToolParticleLevel::execute: bad cast"};
  }
}
