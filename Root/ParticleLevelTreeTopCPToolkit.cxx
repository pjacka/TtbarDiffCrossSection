#include "TtbarDiffCrossSection/ParticleLevelTreeTopCPToolkit.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include <iostream>

ClassImp(ParticleLevelTreeTopCPToolkit)

    using namespace std;

void ParticleLevelTreeTopCPToolkit::print()
{
  // Print contents of current entry.
  // m_chain->Show();

  cout << "run/event number, mcChannelNumber:" << endl;
  cout << runNumber << "  " << eventNumber << "  " << mcChannelNumber << "  " << endl;

  // cout << "met_et, met_phi : " << met_met << "  " << met_phi << endl;

  cout << "large-R jets info (ID, pt, eta, phi, e):" << endl;
  int njets = ljet_pt->size();
  for (int i = 0; i < njets; i++)
  {
    cout << i << ":  " << ljet_pt->at(i) << "  " << ljet_eta->at(i) << "  " << ljet_phi->at(i) << "  "
         << ljet_e->at(i) << endl;
  }

  cout << "small jets info (ID, pt, eta, phi, E):" << endl;
  njets = jet_pt->size();
  for (int i = 0; i < njets; i++)
  {
    cout << i << ":  " << jet_pt->at(i) << "  " << jet_eta->at(i) << "  "
         << jet_phi->at(i) << "  " << jet_e->at(i) << endl;
  }

  cout << "weight_mc: " << weight_mc << endl;
}

//----------------------------------------------------------------------

ParticleLevelTreeTopCPToolkit::ParticleLevelTreeTopCPToolkit(const nlohmann::json &json) : TreeTopCPToolkitBase(json)
{
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initChain(TChain *tree, const TString &sysName)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  m_sysName = sysName;

  // Set branch addresses and branch pointers
  if (!tree)
    return;
  m_chain = tree;
  m_current = -1;
  m_chain->SetMakeClass(1);

  m_chain->SetCacheSize(10000000); // 10 MB
  m_chain->SetCacheLearnEntries(100);

  if (m_chain->GetEntries() > 0)
    readBranchNames();
  else
    return;

  m_chain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  m_chain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
  m_chain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
  m_chain->SetBranchAddress("pass_ljets_NOSYS", &pass_ljets, &b_pass_ljets);

  initLargeRJets();
  initSmallRJets();
  initElectrons();
  initMuons();
  initMET();
  initGenWeights();
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initLargeRJets()
{

  ljet_D2 = 0;
  ljet_GhostBHadronsFinalCount = 0;
  ljet_GhostCHadronsFinalCount = 0;
  ljet_Qw = 0;
  ljet_Tau1_wta = 0;
  ljet_Tau2_wta = 0;
  ljet_Tau3_wta = 0;
  ljet_e = 0;
  ljet_eta = 0;
  ljet_phi = 0;
  ljet_pt = 0;

  m_chain->SetBranchAddress("ljet_D2", &ljet_D2, &b_ljet_D2);
  m_chain->SetBranchAddress("ljet_GhostBHadronsFinalCount", &ljet_GhostBHadronsFinalCount, &b_ljet_GhostBHadronsFinalCount);
  m_chain->SetBranchAddress("ljet_GhostCHadronsFinalCount", &ljet_GhostCHadronsFinalCount, &b_ljet_GhostCHadronsFinalCount);
  m_chain->SetBranchAddress("ljet_Qw", &ljet_Qw, &b_ljet_Qw);
  m_chain->SetBranchAddress("ljet_Tau1_wta", &ljet_Tau1_wta, &b_ljet_Tau1_wta);
  m_chain->SetBranchAddress("ljet_Tau2_wta", &ljet_Tau2_wta, &b_ljet_Tau2_wta);
  m_chain->SetBranchAddress("ljet_Tau3_wta", &ljet_Tau3_wta, &b_ljet_Tau3_wta);
  m_chain->SetBranchAddress("ljet_e", &ljet_e, &b_ljet_e);
  m_chain->SetBranchAddress("ljet_eta", &ljet_eta, &b_ljet_eta);
  m_chain->SetBranchAddress("ljet_phi", &ljet_phi, &b_ljet_phi);
  m_chain->SetBranchAddress("ljet_pt", &ljet_pt, &b_ljet_pt);
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initSmallRJets()
{

  jet_nGhosts_bHadron = 0;
  jet_nGhosts_cHadron = 0;
  jet_e = 0;
  jet_eta = 0;
  jet_phi = 0;
  jet_pt = 0;

  m_chain->SetBranchAddress("jet_nGhosts_bHadron", &jet_nGhosts_bHadron, &b_jet_nGhosts_bHadron);
  m_chain->SetBranchAddress("jet_nGhosts_cHadron", &jet_nGhosts_cHadron, &b_jet_nGhosts_cHadron);
  m_chain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
  m_chain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
  m_chain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
  m_chain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
  m_chain->SetBranchAddress("num_truth_bjets_nocuts", &num_truth_bjets_nocuts, &b_num_truth_bjets_nocuts);
  m_chain->SetBranchAddress("num_truth_cjets_nocuts", &num_truth_cjets_nocuts, &b_num_truth_cjets_nocuts);
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initElectrons()
{
  el_charge = 0;
  el_origin = 0;
  el_type = 0;
  el_e = 0;
  el_eta = 0;
  el_phi = 0;
  el_pt = 0;

  m_chain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
  m_chain->SetBranchAddress("el_origin", &el_origin, &b_el_origin);
  m_chain->SetBranchAddress("el_type", &el_type, &b_el_type);
  m_chain->SetBranchAddress("el_e", &el_e, &b_el_e);
  m_chain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
  m_chain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
  m_chain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initMuons()
{
  mu_charge = 0;
  mu_origin = 0;
  mu_type = 0;
  mu_e = 0;
  mu_eta = 0;
  mu_phi = 0;
  mu_pt = 0;

  m_chain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
  m_chain->SetBranchAddress("mu_origin", &mu_origin, &b_mu_origin);
  m_chain->SetBranchAddress("mu_type", &mu_type, &b_mu_type);
  m_chain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
  m_chain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
  m_chain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
  m_chain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initMET()
{
  m_chain->SetBranchAddress("met_met", &met_met, &b_met_met);
  m_chain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
}

//----------------------------------------------------------------------

void ParticleLevelTreeTopCPToolkit::initGenWeights()
{
  setBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
}
