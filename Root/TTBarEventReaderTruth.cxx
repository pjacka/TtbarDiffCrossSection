#include "TtbarDiffCrossSection/TTBarEventReaderTruth.h"
#include "HelperFunctions/functions.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObject.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "Math/VectorUtil.h"

#include "TopEvent/EventTools.h"

ClassImp(TTBarEventReaderTruth)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTBarEventReaderTruth::TTBarEventReaderTruth(const std::string& name) : EventReaderToolBase(name)
{
  
  
}

//----------------------------------------------------------------------

StatusCode TTBarEventReaderTruth::readJSONConfig(
  const nlohmann::json& json
) {
  
  try {

    m_treeReaderSettings = jsonFunctions::readValueJSON<nlohmann::json>(json, "treeReaderSettings");
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTBarEventReaderTruth::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTBarEventReaderTruth::initialize() {
  top::check(AnaToolBase::initialize(), "TTBarEventReaderTruth: Failed to initialize base tool!");
  
  m_treeReader = std::make_unique<TruthTreeTopCPToolkit>(m_treeReaderSettings);
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTBarEventReaderTruth::initChain(const std::unordered_map<std::string, std::string>& settings) {
  m_treeReader->initChain(m_chain.get(), settings.at("sys_name").c_str());
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

ReconstructedEvent* TTBarEventReaderTruth::readEntry(const ULong64_t ientry) {
  
  m_treeReader->getEntry(ientry);
  
  ReconstructedEvent* event = new ReconstructedEvent;
  // Reading parton level objects

  const auto& t = *m_treeReader;
  
  auto readParton = [](const float pt, const float eta, const float phi, const float m, int id) -> std::shared_ptr<Object_t> { 
    const float parton_energy = ReconstructedObjects::energyFromPtEtaM(pt / 1000., eta, m / 1000.);
    auto parton = std::make_shared<Object_t>(pt / 1000., eta, phi, parton_energy);
    parton->setAttribute<Int_t>("pdgId", id);
    return parton;
  };


  // Top quarks ========================================================
  // Top quark
  std::shared_ptr<Object_t> top_quark = readParton(
    t.Ttbar_MC_t_pt,
    t.Ttbar_MC_t_eta,
    t.Ttbar_MC_t_phi,
    t.Ttbar_MC_t_m,
    t.Ttbar_MC_t_pdgId
  );
  
  // Antitop quark
  std::shared_ptr<Object_t> antitop_quark = readParton(
    t.Ttbar_MC_tbar_pt,
    t.Ttbar_MC_tbar_eta,
    t.Ttbar_MC_tbar_phi,
    t.Ttbar_MC_tbar_m,
    t.Ttbar_MC_tbar_pdgId
  );
  
  // TTbar system ======================================================
  std::shared_ptr<Object_t> ttbar_system = readParton(
    t.Ttbar_MC_ttbar_pt,
    t.Ttbar_MC_ttbar_eta,
    t.Ttbar_MC_ttbar_phi,
    t.Ttbar_MC_ttbar_m,
    -666
  );
  
  // W bosons ==========================================================
  // W from top decay
  std::shared_ptr<Object_t> W_from_top = readParton(
    t.Ttbar_MC_W_from_t_pt,
    t.Ttbar_MC_W_from_t_eta,
    t.Ttbar_MC_W_from_t_phi,
    t.Ttbar_MC_W_from_t_m,
    t.Ttbar_MC_W_from_t_pdgId
  );
  
  // W from antitop decay
  std::shared_ptr<Object_t> W_from_antitop = readParton(
    t.Ttbar_MC_W_from_tbar_pt,
    t.Ttbar_MC_W_from_tbar_eta,
    t.Ttbar_MC_W_from_tbar_phi,
    t.Ttbar_MC_W_from_tbar_m,
    t.Ttbar_MC_W_from_tbar_pdgId
  );

  // B quarks ==========================================================
  // B from top decay
  std::shared_ptr<Object_t> B_from_top = readParton(
    t.Ttbar_MC_b_from_t_pt,
    t.Ttbar_MC_b_from_t_eta,
    t.Ttbar_MC_b_from_t_phi,
    t.Ttbar_MC_b_from_t_m,
    t.Ttbar_MC_b_from_t_pdgId
  );
  
  // B from antitop decay
  std::shared_ptr<Object_t> B_from_antitop = readParton(
    t.Ttbar_MC_bbar_from_tbar_pt,
    t.Ttbar_MC_bbar_from_tbar_eta,
    t.Ttbar_MC_bbar_from_tbar_phi,
    t.Ttbar_MC_bbar_from_tbar_m,
    t.Ttbar_MC_bbar_from_tbar_pdgId
  );
  
  // W from top decays =================================================
  // Decay 1 from W from top decay:
  std::shared_ptr<Object_t> decay1_from_W_from_top = readParton(
    t.Ttbar_MC_Wdecay1_from_t_pt,
    t.Ttbar_MC_Wdecay1_from_t_eta,
    t.Ttbar_MC_Wdecay1_from_t_phi,
    t.Ttbar_MC_Wdecay1_from_t_m,
    t.Ttbar_MC_Wdecay1_from_t_pdgId
  );
  
  // Decay 2 from W from top decay:
  std::shared_ptr<Object_t> decay2_from_W_from_top = readParton(
    t.Ttbar_MC_Wdecay2_from_t_pt,
    t.Ttbar_MC_Wdecay2_from_t_eta,
    t.Ttbar_MC_Wdecay2_from_t_phi,
    t.Ttbar_MC_Wdecay2_from_t_m,
    t.Ttbar_MC_Wdecay2_from_t_pdgId
  );
  W_from_top->setPtr<Object_t>("decay1", decay1_from_W_from_top);
  W_from_top->setPtr<Object_t>("decay2", decay2_from_W_from_top);
  
  // W from antitop decays =============================================
  // Decay 1 from W from antitop decay:
  std::shared_ptr<Object_t> decay1_from_W_from_antitop = readParton(
    t.Ttbar_MC_Wdecay1_from_tbar_pt,
    t.Ttbar_MC_Wdecay1_from_tbar_eta,
    t.Ttbar_MC_Wdecay1_from_tbar_phi,
    t.Ttbar_MC_Wdecay1_from_tbar_m,
    t.Ttbar_MC_Wdecay1_from_tbar_pdgId
  );
  
  // Decay 2 from W from antitop decay:
  std::shared_ptr<Object_t> decay2_from_W_from_antitop = readParton(
    t.Ttbar_MC_Wdecay2_from_tbar_pt,
    t.Ttbar_MC_Wdecay2_from_tbar_eta,
    t.Ttbar_MC_Wdecay2_from_tbar_phi,
    t.Ttbar_MC_Wdecay2_from_tbar_m,
    t.Ttbar_MC_Wdecay2_from_tbar_pdgId
  );
  
  W_from_antitop->setPtr<Object_t>("decay1", decay1_from_W_from_antitop);
  W_from_antitop->setPtr<Object_t>("decay2", decay2_from_W_from_antitop);
  
  
  // Inserting Top decays into top_quark object ========================
  top_quark->setPtr<Object_t>("WCandidate", W_from_top);
  top_quark->setPtr<Object_t>("BCandidate", B_from_top);
  
  // Inserting AntiTop decays into top_quark object ====================
  antitop_quark->setPtr<Object_t>("WCandidate", W_from_antitop);
  antitop_quark->setPtr<Object_t>("BCandidate", B_from_antitop);

  // Insertint Top, AntiTop, and TTbar decays into event ===============
  event->setPtr<Object_t>("top", top_quark);
  event->setPtr<Object_t>("antitop", antitop_quark);
  event->setPtr<Object_t>("ttbarSystem", ttbar_system);

  //Additional variables ===============================================

  event->setAttribute<UInt_t>("RunNumber",m_treeReader->runNumber);
  event->setAttribute<ULong64_t>("EventNumber",m_treeReader->eventNumber);
  event->setAttribute<ULong64_t>("ChannelNumber",m_treeReader->mcChannelNumber);
  event->setAttribute<bool>("IsData", false);
  
  this->readGenWeights(*event);

  return event;  
}

//----------------------------------------------------------------------

void TTBarEventReaderTruth::readGenWeights(ReconstructedEvent& event) const {
  event.setAttribute<float>("weight_mc", m_treeReader->weight_mc);
}

//----------------------------------------------------------------------

void TTBarEventReaderTruth::print() const {
  m_treeReader->print(); 
}
