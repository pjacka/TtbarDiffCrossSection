#include "TtbarDiffCrossSection/TTbarEventReconstructionPartonLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventReconstructionPartonLevel)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventReconstructionPartonLevel::TTbarEventReconstructionPartonLevel(const std::string& name) : EventReconstructionToolBase(name)
{
  m_storeW = false;
  m_storeB = false;
  
}

//----------------------------------------------------------------------

StatusCode TTbarEventReconstructionPartonLevel::readJSONConfig(const nlohmann::json& json) {
  
  try {

    m_storeW = jsonFunctions::readValueJSON<bool>(json, "storeW", m_storeW);
    m_storeB = jsonFunctions::readValueJSON<bool>(json, "storeB", m_storeB);
        
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventReconstructionPartonLevel::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventReconstructionPartonLevel::execute(ReconstructedEvent& event) const {

  auto top = event.getPtr<Object_t>("top");
  auto antitop = event.getPtr<Object_t>("antitop");
 
  std::shared_ptr<Object_t> top1, top2;
  
  if(top->Pt() > antitop->Pt()) {
    top1 = top;
    top2 = antitop;
  }
  else {
    top1 = antitop;
    top2 = top;
  }
  
  event.setPtr<Object_t>("topRandom", top);
  event.setPtr<Object_t>("top1", top1);
  event.setPtr<Object_t>("top2", top2);
}



