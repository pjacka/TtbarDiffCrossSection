#include "TtbarDiffCrossSection/TTBarEventReaderReco.h"
#include "HelperFunctions/functions.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObject.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "Math/VectorUtil.h"

#include "TopEvent/EventTools.h"

ClassImp(TTBarEventReaderReco)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTBarEventReaderReco::TTBarEventReaderReco(const std::string& name) : EventReaderToolBase(name)
{
  m_largeRJetType = kLJET;         // Switch between standard antikt10 jets (LJETS), reclustered jets (RCJETS) and variable-R reclustered jets (VarRCJETS)
  m_sysName = "NOSYS";
}

//----------------------------------------------------------------------

void TTBarEventReaderReco::readMetadata(TFile& f) {
  EventReaderToolBase::readMetadata(f);
  m_isData = (m_metadata->getDataType() == "data");
}


//----------------------------------------------------------------------

StatusCode TTBarEventReaderReco::readJSONConfig(
  const nlohmann::json& json
) {
  try {
    m_treeReaderSettings = jsonFunctions::readValueJSON<nlohmann::json>(json, "treeReaderSettings");
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTBarEventReaderReco::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTBarEventReaderReco::initialize() {
  top::check(AnaToolBase::initialize(), "TTBarEventReaderReco: Failed to initialize base tool!");
  
  m_treeReader = std::make_unique<RecoTreeTopCPToolkit>(m_treeReaderSettings, m_isData);
  
  return StatusCode::SUCCESS;
}

StatusCode TTBarEventReaderReco::initChain(const std::unordered_map<std::string, std::string>& settings) {
  m_treeReader->initChain(m_chain.get(), settings.at("sys_name"));
  m_sysName = settings.at("sys_name");
  
  return StatusCode::SUCCESS;
}


void TTBarEventReaderReco::readSmallRJets(ReconstructedEvent& event) const {
  //////////////////////////////////////////////
  /////// Small-R jets /////////////////////////
  //////////////////////////////////////////////

  const RecoTreeTopCPToolkit& t = *m_treeReader;


  if (m_debug>5) std::cout << "TTBarEventReaderReco: getting small-R jets" << std::endl;
  ObjectContainer_t jets;
  float jets_Ht(0.);
  for (size_t ijet=0; ijet < t.jet_pt->size(); ijet++) {
        
    if (m_debug>6)
      std::cout << "TTBarEventReaderReco::readSmallRJets(): jet " << ijet << std::endl;
    
    auto jet = std::make_shared<Object_t>(t.jet_pt->at(ijet)/1000., t.jet_eta->at(ijet), t.jet_phi->at(ijet), t.jet_e->at(ijet)/1000.); // Transforming to GeV
    jet->setName(jet->getName() + " SmallRJet");
    
    if (m_debug>6)
      std::cout << "TTBarEventReaderReco::readSmallRJets(): found 4-momentum for jet " << ijet << std::endl;
    
    // jet->setAttribute<float>("BTagger_score", t.jet_bTagger_score->at(ijet));
    jet->setAttribute<float>("BTagger_score", 0.);

    if (t.jet_bTagged)
      jet->setAttribute<bool>("BTagFixedCut", t.jet_bTagged->at(ijet));
    if (t.jet_bTagger_continuous_quantile)
      jet->setAttribute<int>("BTagQuantile", t.jet_bTagger_continuous_quantile->at(ijet));
    jets.push_back(jet);
  }
  event.setAttribute<float>("jets_Ht",jets_Ht);
  event.setAttribute<float>("weight_bTag_SF", t.weight_bTag_SF);
  
  std::sort(std::begin(jets), std::end(jets), [](const std::shared_ptr<Object_t>& lhs, const  std::shared_ptr<Object_t>& rhs) {
    return lhs->Pt() > rhs->Pt();
  });
  event.setObjectCollection("SmallRJets",std::move(jets));  
}



void TTBarEventReaderReco::readLargeRJets(ReconstructedEvent& event) const {
  
  
  const RecoTreeTopCPToolkit& t = *m_treeReader;
  size_t nLargeRJets=t.ljet_pt->size();
  //if(nLargeRJets < 2) 
  //std::cout << "nLargeRJets: " << nLargeRJets << std::endl;
  ObjectContainer_t largeRJets;
  auto indexes = std::make_shared<std::vector<int>>();
  float ljets_Ht(0.);
  int counter(0);
  //std::cout<< "line: " << __LINE__ << std::endl;
  for (size_t ijet=0;ijet<nLargeRJets;++ijet) {
      
    if (m_debug>6) {
      std::cout << ijet << ". ljet Pt: " << t.ljet_pt->at(ijet)/1000. << std::endl;
    }
        
    counter++;
    
    float ljet_energy = ReconstructedObjects::energyFromPtEtaM(t.ljet_pt->at(ijet)/1000., t.ljet_eta->at(ijet), t.ljet_m->at(ijet)/1000.);
    auto ljet = std::make_shared<Object_t>(t.ljet_pt->at(ijet)/1000.,t.ljet_eta->at(ijet),t.ljet_phi->at(ijet),ljet_energy);
    ljet->setName(ljet->getName() + " LargeRJet");
    
    ljet->setAttribute<float>("Split12",t.ljet_Split12->at(ijet) / 1000.);
    ljet->setAttribute<float>("Split23",t.ljet_Split23->at(ijet) / 1000.);
    ljet->setAttribute<float>("Qw",t.ljet_Qw->at(ijet) / 1000.);
    ljet->setAttribute<float>("Tau1",t.ljet_Tau1_wta->at(ijet));
    ljet->setAttribute<float>("Tau2",t.ljet_Tau2_wta->at(ijet));
    ljet->setAttribute<float>("Tau3",t.ljet_Tau3_wta->at(ijet));
    ljet->setAttribute<float>("Tau4",t.ljet_Tau4_wta->at(ijet));
    
    ljet->setAttribute<bool>("TopTagged", t.ljet_topTagger_tagged->at(ijet));
    ljet->setAttribute<float>("TopTagSF", t.ljet_topTagger_SF->at(ijet));
    ljet->setAttribute<float>("TopTagEffSF", t.ljet_topTagger_effSF->at(ijet));
    ljet->setAttribute<float>("TopTagEfficiency", t.ljet_topTagger_efficiency->at(ijet));
    ljet->setAttribute<bool>("TopTagPassMass", t.ljet_topTagger_passMass->at(ijet));
    ljet->setAttribute<float>("Score", t.ljet_topTagger_score->at(ijet));
    ljet->setAttribute<bool>("TopTagValidKinRange", true); // Placeholder for missing variable!!
    //ljet->setPtr<std::vector<float>>("TopTagSFVariations",topTagSFVariations);

    if (!m_isData) {
      ljet->setAttribute<int>("TruthLabel", t.ljet_TruthLabel->at(ijet));
      ljet->setAttribute<int>("GhostBHadronsFinalCount", t.ljet_GhostBHadronsFinalCount->at(ijet));
      ljet->setAttribute<int>("GhostCHadronsFinalCount", t.ljet_GhostCHadronsFinalCount->at(ijet));
    }
    
    
    
    indexes->push_back(ijet);
    largeRJets.push_back(ljet);
    
    ljets_Ht += t.ljet_pt->at(ijet) / 1000.;
  }

  event.setObjectCollection("LargeRJets",std::move(largeRJets));
  event.setPtr<std::vector<int>>("LargeRJetsIndexes",indexes);
  event.setAttribute<float>("ljets_Ht", ljets_Ht);
  
}


void TTBarEventReaderReco::readElectrons(ReconstructedEvent& event) const {
  
  const RecoTreeTopCPToolkit& t = *m_treeReader;
  
  ObjectContainer_t electrons;
  
  size_t nElectrons = t.el_pt->size();
  
  for(size_t i = 0; i < nElectrons; i++) {
    auto electron = std::make_shared<Object_t>(t.el_pt->at(i)/1000., t.el_eta->at(i), t.el_phi->at(i), t.el_e->at(i)/1000.);
    electron->setName(electron->getName() + " Electron");
    
    if (!m_isData) {
      electron->setAttribute<int>("IFFClass",t.el_IFFClass->at(i));
    }
    
    electron->setAttribute<float>("Charge",t.el_charge->at(i));
    electron->setAttribute<bool>("Selected",t.el_select->at(i));    
    electrons.push_back(electron);
  }

  event.setObjectCollection("Electrons", std::move(electrons));
}


void TTBarEventReaderReco::readMuons(ReconstructedEvent& event) const {
  
  const RecoTreeTopCPToolkit& t = *m_treeReader;
  
  ObjectContainer_t muons;
  
  size_t nMuons = t.mu_pt->size();
  
  for(size_t i = 0; i < nMuons; i++) {
    auto muon = std::make_shared<Object_t>(t.mu_pt->at(i)/1000., t.mu_eta->at(i), t.mu_phi->at(i), t.mu_e->at(i)/1000.);
    muon->setName(muon->getName() + " Muon");
    
    if (!m_isData) {
      muon->setAttribute<int>("IFFClass",t.mu_IFFClass->at(i));
      muon->setAttribute<float>("TTVA_effSF",t.mu_TTVA_effSF->at(i));
      muon->setAttribute<float>("Isol_effSF",t.mu_isol_effSF->at(i));
      muon->setAttribute<float>("Reco_effSF",t.mu_reco_effSF->at(i));
    }
    
    muon->setAttribute<float>("Charge",t.mu_charge->at(i));
    muon->setAttribute<bool>("Selected",t.mu_select->at(i));
    
    muons.push_back(muon);
  }
 
  event.setObjectCollection("Muons", std::move(muons));
}


void TTBarEventReaderReco::readMET(ReconstructedEvent& event) const {
  event.setAttribute<float>("met_met", m_treeReader->met_met/1000.);
  event.setAttribute<float>("met_phi", m_treeReader->met_phi);
}

//----------------------------------------------------------------------

void TTBarEventReaderReco::readTrigger(ReconstructedEvent& event) const {
  //Triggers 
  const RecoTreeTopCPToolkit& t = *m_treeReader;
  
  event.setAttribute<bool>("HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100", t.trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100);
  event.setAttribute<bool>("HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15", t.trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_2j330_35smcINF_a10t_lcw_jes_L1J100", t.trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1J100);
  event.setAttribute<bool>("HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15", t.trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_2j330_a10t_lcw_jes_30smcINF", t.trigPassed_HLT_2j330_a10t_lcw_jes_30smcINF);
  event.setAttribute<bool>("HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111", t.trigPassed_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111);
  event.setAttribute<bool>("HLT_ht1000_L1J100", t.trigPassed_HLT_ht1000_L1J100);
  event.setAttribute<bool>("HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15", t.trigPassed_HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15", t.trigPassed_HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j360_a10_sub_L1J100", t.trigPassed_HLT_j360_a10_sub_L1J100);
  event.setAttribute<bool>("HLT_j360_a10r_L1J100", t.trigPassed_HLT_j360_a10r_L1J100);
  event.setAttribute<bool>("HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15", t.trigPassed_HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15", t.trigPassed_HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100", t.trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100);
  event.setAttribute<bool>("HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15", t.trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j420_35smcINF_a10t_lcw_jes_L1J100", t.trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1J100);
  event.setAttribute<bool>("HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15", t.trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j420_a10_lcw_L1J100", t.trigPassed_HLT_j420_a10_lcw_L1J100);
  event.setAttribute<bool>("HLT_j420_a10r_L1J100", t.trigPassed_HLT_j420_a10r_L1J100);
  event.setAttribute<bool>("HLT_j420_a10t_lcw_jes_35smcINF_L1J100", t.trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100);
  event.setAttribute<bool>("HLT_j420_a10t_lcw_jes_35smcINF_L1SC111", t.trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111);
  event.setAttribute<bool>("HLT_j420_a10t_lcw_jes_40smcINF_L1J100", t.trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100);
  event.setAttribute<bool>("HLT_j440_a10_lcw_subjes_L1SC111", t.trigPassed_HLT_j440_a10_lcw_subjes_L1SC111);
  event.setAttribute<bool>("HLT_j440_a10r_L1SC111", t.trigPassed_HLT_j440_a10r_L1SC111);
  event.setAttribute<bool>("HLT_j460_a10_lcw_subjes_L1J100", t.trigPassed_HLT_j460_a10_lcw_subjes_L1J100);
  event.setAttribute<bool>("HLT_j460_a10_lcw_subjes_L1SC111", t.trigPassed_HLT_j460_a10_lcw_subjes_L1SC111);
  event.setAttribute<bool>("HLT_j460_a10_lcw_subjes_L1SC111_CJ15", t.trigPassed_HLT_j460_a10_lcw_subjes_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j460_a10r_L1J100", t.trigPassed_HLT_j460_a10r_L1J100);
  event.setAttribute<bool>("HLT_j460_a10r_L1SC111", t.trigPassed_HLT_j460_a10r_L1SC111);
  event.setAttribute<bool>("HLT_j460_a10r_L1SC111_CJ15", t.trigPassed_HLT_j460_a10r_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100", t.trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100);
  event.setAttribute<bool>("HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15", t.trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j460_a10t_lcw_jes_L1J100", t.trigPassed_HLT_j460_a10t_lcw_jes_L1J100);
  event.setAttribute<bool>("HLT_j460_a10t_lcw_jes_L1SC111", t.trigPassed_HLT_j460_a10t_lcw_jes_L1SC111);
  event.setAttribute<bool>("HLT_j460_a10t_lcw_jes_L1SC111_CJ15", t.trigPassed_HLT_j460_a10t_lcw_jes_L1SC111_CJ15);
  event.setAttribute<bool>("HLT_j480_a10_lcw_subjes_L1J100", t.trigPassed_HLT_j480_a10_lcw_subjes_L1J100);
  event.setAttribute<bool>("HLT_j480_a10_lcw_subjes_L1SC111", t.trigPassed_HLT_j480_a10_lcw_subjes_L1SC111);
  event.setAttribute<bool>("HLT_j480_a10r_L1J100", t.trigPassed_HLT_j480_a10r_L1J100);
  event.setAttribute<bool>("HLT_j480_a10r_L1SC111", t.trigPassed_HLT_j480_a10r_L1SC111);
  event.setAttribute<bool>("HLT_j480_a10t_lcw_jes_L1J100", t.trigPassed_HLT_j480_a10t_lcw_jes_L1J100);
  event.setAttribute<bool>("HLT_j480_a10t_lcw_jes_L1SC111", t.trigPassed_HLT_j480_a10t_lcw_jes_L1SC111);
  
}

void TTBarEventReaderReco::readGenWeights(ReconstructedEvent &event) const
{
  event.setAttribute<float>("weight_mc", m_treeReader->weight_mc);
}


ReconstructedEvent* TTBarEventReaderReco::readEntry(const ULong64_t ientry) {
  m_treeReader->getEntry(ientry);
  ReconstructedEvent* event = new ReconstructedEvent;
  // Reading object collections and event variables
  readSmallRJets(*event);
  readLargeRJets(*event);
  readElectrons(*event);
  readMuons(*event);
  readMET(*event);
  readTrigger(*event);
  readGenWeights(*event);

  //Additional variables

  //std::cout << "  runno=" << m_treeReader->runNumber << " evtno=" << m_treeReader->eventNumber << std::endl;

  event->setAttribute<UInt_t>("RunNumber", m_treeReader->runNumber);
  event->setAttribute<ULong64_t>("EventNumber", m_treeReader->eventNumber);
  event->setAttribute<ULong64_t>("ChannelNumber", m_treeReader->mcChannelNumber);
  
  // Passing metadata to event
  event->setAttribute<bool>("IsData", m_isData);
  event->setAttribute<std::string>("data_type", m_metadata->getDataType());
  event->setAttribute<std::string>("year", m_metadata->getYear());
  event->setAttribute<ULong64_t>("channel", m_metadata->getDSID());
  if(!m_isData) {
    event->setAttribute<std::string>("mc_prod", m_metadata->getMCProduction());
    event->setAttribute<std::string>("etag", m_metadata->getEtag());
  }
  
  event->setAttribute<float>("weight_pileup", m_treeReader->weight_pileup);
  event->setAttribute<float>("weight_jvt_effSF", m_treeReader->weight_jvt_effSF);
  event->setAttribute<float>("weight_leptonSF_tight", m_treeReader->weight_leptonSF_tight);
  
  // Systematic branch identificator
  event->setAttribute<std::string>("sys_name", m_sysName);

  return event;  
}

void TTBarEventReaderReco::print() const {
  
  m_treeReader->print();
  
}
