////////////////////////////////////////////////////////////
//                  Ota Zaplatilek                        //
//                  functions for                         //
//   Compare Acceptancy and Efficiency of different MC    //
//                    14.11.2017                          //
////////////////////////////////////////////////////////////

#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/plottingFunctions.h"
#include "HelperFunctions/namesAndTitles.h"
#include "CovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "CovarianceCalculator/OneSysHistos.h"
#include "HelperFunctions/AtlasStyle.h"
#include "TtbarDiffCrossSection/CompareAccEff_functions.h"

#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib> 
#include <algorithm>
#include "TString.h" 
#include "TRandom3.h"
#include "TStopwatch.h"
#include "TStyle.h"
#include "TPaveText.h"


using namespace std;





  
//***********************//
//*      FUNCTIONS      *//
//***********************//


void setEffStyle(TH1D* efficiencyOfTruthLevelCuts, TH1D* efficiencyOfRecoLevelCuts, TH1D* matchingEff) {
  
  if(efficiencyOfTruthLevelCuts) {
    efficiencyOfTruthLevelCuts->SetLineColor( kAzure-4 );
    efficiencyOfTruthLevelCuts->SetLineWidth( 0 );
    efficiencyOfTruthLevelCuts->SetFillColor( kAzure-4 );
    efficiencyOfTruthLevelCuts->SetFillStyle( 1001 );
    efficiencyOfTruthLevelCuts->SetMarkerStyle( 21 );
    efficiencyOfTruthLevelCuts->SetMarkerColor( kBlue );
  }
  
  if(efficiencyOfRecoLevelCuts) {
    efficiencyOfRecoLevelCuts->SetLineColor( kRed -10);
    efficiencyOfRecoLevelCuts->SetLineWidth( 0 );
    efficiencyOfRecoLevelCuts->SetFillColor( kRed-10 );
    efficiencyOfRecoLevelCuts->SetFillStyle( 1001 );
    efficiencyOfRecoLevelCuts->SetMarkerStyle( 20 );
    efficiencyOfRecoLevelCuts->SetMarkerColor( kRed );
  }
  
  if(matchingEff) {
    matchingEff->SetLineColor( kGreen-8 );
    matchingEff->SetLineWidth(0);
    matchingEff->SetMarkerColor( kGreen+2 );
    matchingEff->SetMarkerStyle( 22 );
    matchingEff->SetFillColor( kGreen-8 );
    matchingEff->SetFillStyle( 1001 );
  }
  
}

//----------------------------------------------------------------------

void plotAccEffInOnePlotND(TH1D* efficiencyOfTruthLevelCuts, TH1D* efficiencyOfRecoLevelCuts, const HistogramNDto1DConverter* histogramConverterND,
			  MultiDimensionalPlotsSettings* plotSettingsND,const TString& dirname,const TString& level, const TString& variable) {
  
  setEffStyle(efficiencyOfTruthLevelCuts, efficiencyOfRecoLevelCuts);
  
  vector<TH1D*> efficiencyOfTruthLevelCuts_proj = histogramConverterND->makeProjections(efficiencyOfTruthLevelCuts,"efficiencyOfTruthLevelCuts_proj");
  vector<TH1D*> efficiencyOfRecoLevelCuts_proj = histogramConverterND->makeProjections(efficiencyOfRecoLevelCuts,"efficiencyOfRecoLevelCuts_proj");
  const int nprojections=efficiencyOfTruthLevelCuts_proj.size();
  
  
  vector<TString> generalInfo;
  generalInfo.push_back("#bf{#it{ATLAS}} Simulation " +NamesAndTitles::getResultsStatus()+", #sqrt{s}=13 TeV");
  if(level.Contains("Parton")) generalInfo.push_back("Fiducial parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV");
  else generalInfo.push_back("Fiducial phase-space");
  
  const int nlinesInfo=generalInfo.size();
  
  int nPerLine(0),nlines(0);
  functions::getGridSize(nprojections,nlines,nPerLine);
  
  const double canvas_xsize = (1000./3) * nPerLine;
  const double canvas_ysize = 175.*(nlines+1);
  
  plotSettingsND->initialize(canvas_xsize, canvas_ysize, nlines, nPerLine, nlinesInfo);
  
  
  const TString figurename = level+"_"+variable+"_acceptance_efficiency";
  
  auto can = std::unique_ptr<TCanvas>(plotSettingsND->makeCanvas("can"));
  can->cd();
  vector<vector<shared_ptr<TPad>>> pads = plotSettingsND->makeArrayOfPads(nprojections);
  
  std::vector<TString> xtitles = NamesAndTitles::getXtitles(variable);
  
  int iproj=0;
  for(int iline=0;iline<nlines;iline++) {
    int nInLine = pads[iline].size();
    
    for(int iInLine=0;iInLine<nInLine;iInLine++) {
      TPad* currentPad = pads[iline][iInLine].get();
      
      plotSettingsND->setHistLabelStyle(efficiencyOfTruthLevelCuts_proj[iproj]);
      plotSettingsND->setHistLabelStyle(efficiencyOfRecoLevelCuts_proj[iproj]);
      
      setEffStyle(efficiencyOfTruthLevelCuts_proj[iproj], efficiencyOfRecoLevelCuts_proj[iproj]);
      
      if(iInLine ==0 && iline ==0) { 
	efficiencyOfRecoLevelCuts_proj[iproj]->GetYaxis()->SetTitle("Correction");
      }
      else {
	efficiencyOfRecoLevelCuts_proj[iproj]->GetYaxis()->SetTitle("");
      }
      if(iline != nlines-1) { 
	efficiencyOfRecoLevelCuts_proj[iproj]->GetXaxis()->SetTitle("");
      }
      else {
	efficiencyOfRecoLevelCuts_proj[iproj]->GetXaxis()->SetTitle(xtitles.back());
      }
      
      currentPad->cd();
      
      std::vector<TString> externalBinInfo = histogramConverterND->getExternalBinsInfo(iproj);
      TPaveText* paveText = functions::makePaveText(externalBinInfo, plotSettingsND->textSizePad(), 0.35, 0.75, 0.93); // xmin,xmax,ymax
      paveText->SetFillStyle(0);
      
      const double min = 0.;
      double max = 1.;
      max*=(1. + 0.35*externalBinInfo.size());
      
      efficiencyOfRecoLevelCuts_proj[iproj]->GetYaxis()->SetRangeUser(min,max);
      
      efficiencyOfRecoLevelCuts_proj[iproj]->Draw("p e2");
      efficiencyOfTruthLevelCuts_proj[iproj]->Draw("p e2 same");
      
      paveText->Draw();
      
      currentPad->RedrawAxis("g"); 
      currentPad->RedrawAxis();
      
      iproj++;
    }
  }
  can->cd();
  TPaveText* paveTextGeneralInfo = plotSettingsND->makePaveText(generalInfo);
  paveTextGeneralInfo->Draw();
  
  auto leg = unique_ptr<TLegend>(plotSettingsND->makeLegend(0.6,0.98));
  leg->SetNColumns(2);
  leg->AddEntry( efficiencyOfTruthLevelCuts, "Acceptance", "pf" );
  leg->AddEntry( efficiencyOfRecoLevelCuts, "Efficiency", "pf" );
  leg->Draw();
  
  
  TString dirname_png=dirname;
  dirname_png.ReplaceAll("pdf.","png.");
  
  can->SaveAs(dirname+"/"+figurename+".pdf");
  can->SaveAs(dirname_png+"/"+figurename+".png");
  
  for(int i=0;i<nprojections;i++) {
    delete efficiencyOfRecoLevelCuts_proj[i];
    delete efficiencyOfTruthLevelCuts_proj[i];
  }
  
}

//----------------------------------------------------------------------

void PlotAccEffInOnePlot(TH1D* efficiencyOfTruthLevelCuts, TH1D* efficiencyOfRecoLevelCuts,
			 TString pathToFigures,
			 TString mc_samples_production,
			 TString subdir,
			 TString level,
			 TString spectrum,
			 TString /*label*/,
			 TString dimension,
			 vector<double>& binning_x,
			 vector<vector<double> >& binning_y) {


  double canvas_x=800.;
  double canvas_y=800.;
  if(dimension=="2D"){
    canvas_x=1000.;
    canvas_y=600.;
  }

  TCanvas c("c", "canvas",0,0,canvas_x,canvas_y);

  c.cd();
  
  double ymin=0.;
  double ymax=1.35;
  
  if(dimension=="2D") ymax=1.25;
  
  setEffStyle(efficiencyOfTruthLevelCuts, efficiencyOfRecoLevelCuts);
  
  
  efficiencyOfRecoLevelCuts->GetYaxis()->SetRangeUser(ymin,ymax);
  efficiencyOfRecoLevelCuts->GetXaxis()->SetLabelSize(0.05);
  efficiencyOfRecoLevelCuts->GetXaxis()->SetLabelOffset(0.015);
  efficiencyOfRecoLevelCuts->GetYaxis()->SetLabelSize(0.05);
  efficiencyOfRecoLevelCuts->GetYaxis()->SetLabelOffset(0.012);
  efficiencyOfRecoLevelCuts->GetXaxis()->SetTitleOffset(1.2);
  //if(spectrum=="leadingTop_pt" || spectrum=="subleadingTop_pt") efficiencyOfTruthLevelCuts->SetNdivisions(10);
  
  
  if(dimension=="2D"){
    
    efficiencyOfRecoLevelCuts->GetXaxis()->SetLabelOffset(999);
    efficiencyOfRecoLevelCuts->GetXaxis()->SetTitleOffset(1.8);
    gPad->SetBottomMargin(0.18);
    
    efficiencyOfRecoLevelCuts->SetNdivisions(1);
    
  }
  
  
	
  TString xtitle = efficiencyOfTruthLevelCuts->GetXaxis()->GetTitle();
  xtitle.ReplaceAll("Detector","");
  efficiencyOfRecoLevelCuts->SetXTitle( xtitle.Data() );
  efficiencyOfRecoLevelCuts->Draw("p e2");
  efficiencyOfTruthLevelCuts->Draw("p e2 same");
  
  
  TLegend leg( 0.2, 0.77, 0.9, 0.82) ;
  leg.SetNColumns(2);
  leg.SetFillColor(0);
  if(dimension!="2D") leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  leg.SetTextSize(0.043);
  leg.SetTextFont( 42 );
  
  TH1D* h1=(TH1D*)efficiencyOfTruthLevelCuts->Clone();
  TH1D* h2=(TH1D*)efficiencyOfRecoLevelCuts->Clone();
  
  
  
  
  if(dimension=="2D"){
    TString precision="4.f";
    TString precision2="4.f";
    
    std::tie(precision,precision2) = functions::setPrecision(spectrum);
    
    TString variable1_name = xtitle;
    variable1_name.ReplaceAll("#otimes","");
    variable1_name=variable1_name(0,variable1_name.First("  "));
    
    int lineColor=kGray+1;
    int lineStyle=4;
    double lineWidth=2;
    
    
    
    
    functions::drawVerticalLines(efficiencyOfRecoLevelCuts,binning_x,binning_y,ymin,ymax,lineColor,lineStyle,lineWidth); // Draw vertical lines on variable1 bin endges
    
    
    double ticksSize=0.015;
    functions::drawTicksX(efficiencyOfRecoLevelCuts,binning_x,binning_y,ymin,ymax,ticksSize); // Draw ticks on the histogram bin edges
    
    functions::drawBinLabels2DConcatenated(efficiencyOfRecoLevelCuts,binning_x,binning_y,precision, precision2,variable1_name); // Draw bin labels corresponding to 2D concatenated distribution
    
    
  }
  
  leg.AddEntry( h1, "Acceptance", "pf" );
  leg.AddEntry( h2, "Efficiency", "pf" );
  leg.Draw();
  
  TPaveText paveText1 (0.15,0.88,0.9,0.93,"NDC"); 
  paveText1.SetFillColor(0);
  paveText1.SetBorderSize(0);
  paveText1.SetTextSize(0.043);
  paveText1.SetTextAlign(12);
  paveText1.SetTextFont( 42 );
  TPaveText paveText2(paveText1);
  paveText2.SetY1(0.825);
  paveText2.SetY2(0.875);
  
  TString info = "#bf{#it{ATLAS}} Simulation " +NamesAndTitles::getResultsStatus()+", #sqrt{s}=13 TeV";
  paveText1.AddText(info);
  info = level.Contains("Parton") ? "Fiducial parton level" : "Fiducial particle level";
  paveText2.AddText(info);
  paveText1.Draw();
  paveText2.Draw();
  
  gPad->RedrawAxis();
  
  functions::PrintCanvas(&c,pathToFigures,mc_samples_production,subdir+"/"+level+"_"+spectrum+"_acceptance_efficiency", false);

  //efficiencyOfTruthLevelCuts->GetYaxis()->SetRangeUser(0.0001,1.1);
  //efficiencyOfTruthLevelCuts->Draw("p e2");
  //efficiencyOfRecoLevelCuts->Draw("p e2 same");
  //functions::PrintCanvas(&c,pathToFigures,mc_samples_production,subdir+"/"+level+"_"+spectrum+"_acceptance_efficiency", true);
  delete h1;
  delete h2;
}








TH1D DrawOneAcc(TH1D* hist,
		TString str_histVar,
		TString /*level*/,
		TString /*label*/,
		int colorMarker,
		int colorUnc,
		TPad* pad,
		TString AccEff,
		bool bool_ratio
			 ) {

/* DrawOneAcc function prepare and set one histogram for drawing
*       TH1D* hist				... poiter to consider histogram
*		TString str_histVar,	... name of variable - for set x-axis Title
*		TString level,			... TString for short descibtion in pdf, consider parton and particle level
*		TString label,			... TString 
*		int colorMarker,		... set color of markers
*		int colorUnc,			... set color of uncerciences -> FillStyle and ColorStyle
*		TPad* pad,				... setup pad
*		TString AccEff,			... TString for distinguish "Acc" - Acceptance and "Eff" - Efficiency plots 
*		bool bool_ratio			... boolen for ratio pads true/false
*/

  pad->cd();				 
  gStyle->SetOptStat(0);	
  //double delta_y = abs( abs(y_max) - abs(y_min) );
  // set y - title for Acceptancy pads
  if (AccEff.Contains("Acc")){			  
    //hist->GetYaxis()->SetRangeUser(y_min - delta_y, y_max + delta_y*3.0); //y_min - delta_y/10.0, y_max + delta_y/3.0);
    hist->GetYaxis()->SetRangeUser(0.5,1.2); //y_min - delta_y/10.0, y_max + delta_y/3.0);
    hist->GetYaxis()->SetTitle("Acceptance");

  }
  else{
    hist->GetYaxis()->SetRangeUser(0.008,0.078);
    hist->GetYaxis()->SetTitle("Efficiency      ");

  }
  
  // for ratio pads
  if(bool_ratio == true){
	  
      hist->GetXaxis()->SetLabelSize(0.065);
      hist->GetXaxis()->SetTitleSize(0.09);

      hist->GetXaxis()->SetLabelOffset(0.015);
      hist->GetXaxis()->SetTitleOffset(1.0);

      hist->GetYaxis()->SetLabelSize(0.045);
      hist->GetYaxis()->SetTitleSize(0.045);
      hist->GetYaxis()->SetTitleOffset(1.0);
  }
  else{
      hist->SetFillStyle( 1001 );
      hist->GetXaxis()->SetLabelSize(0.05);
      hist->GetXaxis()->SetTitleSize(0.08);

      hist->GetXaxis()->SetLabelOffset(0.025);
      hist->GetXaxis()->SetTitleOffset(0.9);

      hist->GetYaxis()->SetLabelSize(0.045);
      hist->GetYaxis()->SetTitleSize(0.065);
      hist->GetYaxis()->SetTitleOffset(0.75);
  }
 
  hist->SetMarkerStyle(21);
  hist->SetMarkerSize(1);
  hist->SetMarkerColor( colorMarker );
  
  hist->SetLineStyle(1);
  hist->SetLineColor( colorMarker );
  hist->SetLineWidth(2);
  hist->SetFillColorAlpha( colorUnc, 0.65 );

  // ratio pads for acceptancies
  if(bool_ratio == true && AccEff.Contains("Acc")){
    hist->GetYaxis()->SetRangeUser(0.8,1.19);
    hist->GetYaxis()->SetTitle("Acceptance ratio = MC / MC nominal"); 
  }
  //ratio pads for efficiencies
  else if(bool_ratio == true && AccEff.Contains("Eff")){
    hist->GetYaxis()->SetRangeUser(0.55,1.29);
    if(str_histVar =="z_tt_pt" || str_histVar == "pout" || str_histVar=="ttbar_y") 	hist->GetYaxis()->SetRangeUser(0.6,1.70);
    hist->GetYaxis()->SetTitle("Efficiency ratio = MC / MC nominal"); 
  }
  //efficiecy - pad1
  if(bool_ratio == false && AccEff.Contains("Eff")){
    hist->GetYaxis()->SetRangeUser(0.01,0.4);
    hist->GetYaxis()->SetTitle("Efficiency    "); 
  }
  if (bool_ratio == true && AccEff.Contains("Only") ){
    hist->GetXaxis()->SetLabelSize(0.04);
    hist->GetXaxis()->SetTitleSize(0.05);

    hist->GetXaxis()->SetLabelOffset(0.015);
    hist->GetXaxis()->SetTitleOffset(1.2);

    hist->GetYaxis()->SetLabelSize(0.025);
    hist->GetYaxis()->SetTitleSize(0.035);
    hist->GetYaxis()->SetTitleOffset(1.1);
    
    if(AccEff.Contains("Acc")) hist->GetYaxis()->SetRangeUser(0.9,1.2);
    else if(AccEff.Contains("Eff")) hist->GetYaxis()->SetRangeUser(0.9,1.2);
  }
	
  TString xtitle = hist->GetXaxis()->GetTitle();
  xtitle.ReplaceAll("Detector","");
  hist->SetXTitle( xtitle.Data() );

  NamesAndTitles::setXtitle(hist,str_histVar);
  cout << "Setting Xtitle: " << str_histVar << " " << hist->GetXaxis()->GetTitle() << endl;
  return(*hist);
}



void PlotAllAccInOnePlot( const vector<TH1D*>& vec_EffHists,
			  const vector<TH1D*>& vec_AccHists,
			  const vector<TString>& vec_LegendName,
			  const vector<int>& vec_colorUnc,
			  const vector<int>& vec_colorMarkers,
			  TString nameHist,
			  TString outputPath,
			  TString level,
			  TString label) {
	 

  //setup canvas		
  TCanvas c("c", "canvas",0,0,800,800);
  c.cd();
    
  //setup pad1 and pad2    
  TPad *pad1 = 	new TPad("pad1","pad1",0,0.5,1,1,0,0,0);
  TPad *pad2 =	new TPad("pad2","pad2",0,0,1,0.5,0,0,0);
  pad2->SetBottomMargin(0.35);
  pad2->SetTopMargin(0.0);
  pad2->SetRightMargin(0.05);
  pad2->SetGridy(1);
  pad2->SetTicks();
  pad2->Draw();
  
  pad1->SetRightMargin(0.05);
  pad1->SetTicks();
  pad1->SetBottomMargin(0.0); 
  pad1->SetBorderMode(0);
  pad1->Draw();
  
  //setup Legend	
  TLegend leg( 0.13, 0.48, 0.93, 0.70) ;
  leg.SetNColumns(2);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  leg.SetTextFont(72);
  leg.SetTextSize(0.048); 
  leg.SetTextFont( 42 );
      
  int nHists = vec_EffHists.size();
  
    //loop over histogram saved in vector  
  for (int iHist=0;iHist <nHists; ++iHist){	
    cout << "loop over histograms to plot: " << iHist+1 << "/" << nHists << endl;

    //Call function to plot one histogram 
    bool bool_ratio = false;
    DrawOneAcc(vec_EffHists.at(iHist), nameHist, level, label, vec_colorMarkers.at(iHist), vec_colorUnc.at(iHist), pad2,  "Eff", bool_ratio); //, y_min_pad2, y_max_pad2 );
    DrawOneAcc(vec_AccHists.at(iHist), nameHist, level, label, vec_colorMarkers.at(iHist), vec_colorUnc.at(iHist), pad1, "Acc", bool_ratio); //, y_min_pad1, y_max_pad1 );
    
    //Drawing
  if( iHist == 0 ){
    pad2->cd();
    vec_EffHists.at(iHist)->Draw("p e2");
    pad1->cd();
    vec_AccHists.at(iHist)->Draw("p e2");
  }
  else if(iHist == nHists -1){
    pad2->cd();
    vec_EffHists.at(iHist)->Draw("p e2 same");
    pad1->cd();
    vec_AccHists.at(iHist)->Draw("p e2 same");
    
    pad2->cd();
    vec_EffHists.at(0)->Draw("p e2 same");	//nominal at the top
    pad1->cd();
    vec_AccHists.at(0)->Draw("p e2 same");	//nominal at the top
  }
  else{
    pad2->cd();
    vec_EffHists.at(iHist)->Draw("p e2 same");
    pad1->cd();
    vec_AccHists.at(iHist)->Draw("p e2 same");
  }
    
    pad1->cd();
    TString nameHist = vec_EffHists.at(iHist)->GetName();
    TString nameLeg = vec_LegendName.at(iHist);
    if (nameLeg.Contains("_")) nameLeg.ReplaceAll("_", " ");
  
    leg.AddEntry( vec_EffHists.at(iHist), nameLeg, "pf" );
  }
       
  
  TString info = level.Contains("Parton") ? "Parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV" : "Fiducial phase-space";
  functions::WriteGeneralInfo(info,"",0.05,0.19,0.82);
  functions::WriteInfo(info, 0.05, 0.33, 0.75);
  
  leg.Draw();
  
  
  c.cd(); // !!!! Posision of this c.cd(); is important because of pad between
  //pad1 and pad2 are coverd by pad b: between
  TPad *b = 	new TPad("b","b",0.0,0.46,1.0,0.54);
  
  b->SetRightMargin(0.05);
  b->SetBottomMargin(0.05);
  b->SetBorderMode(0);
  b->Draw();
  
  b->cd();
  //estetic conection of axis between pad1 and pad2
  // left side
  TLine *line = new TLine(0.10,0.0,0.1,0.4);
  line->Draw();
  line = new TLine(0.10,0.6,0.10,1.0);
  line->Draw();
  line = new TLine(0.076639,0.5143349,0.1224797,0.6263532);
  line->Draw();
  line = new TLine(0.076639,0.3423165,0.1224797,0.4543349);
  line->Draw();
  
  // right side  
  line = new TLine(0.95,0.0,0.95,0.4);
  line->Draw();
  line = new TLine(0.95,0.6,0.95,1.0);
  line->Draw();
  line = new TLine(0.926639,0.5143349,0.9724797,0.6263532);
  line->Draw();
  line = new TLine(0.926639,0.3423165,0.9724797,0.4543349);
  line->Draw();
  
  
  
  TString outPut_path_and_name = outputPath+"/"+nameHist+"_"+level+"Level_AllAccEff.pdf";
  cout << "save: " << outPut_path_and_name << endl;
  c.SaveAs(outPut_path_and_name);
  //cout << "\n\n\n saving canvas: " + level+"Level_"+spectrum+"_AllAcceptanceInOnePlot.pdf \n\n\n" << endl;
  //PrintCanvas(&c,outputPath,subdir+"/"+level+"Level_"+spectrum+"_AllAcceptanceInOnePlot", false);
  //PrintCanvas(&c,outputPath,subdir+"/"+level+"Level_"+spectrum+"_AllAcceptanceInOnePlot", true);

}  













void PlotVectorInOnePlot( const vector<TH1D*>& vec_Hists, 
			  const vector<TString>& vec_LegendName,
			  const vector<int>& vec_colorUnc,
			  const vector<int>& vec_colorMarkers,
			  TString nameHist,
			  TString outputPath,
			  TString level,
			  TString label,
			  TString str_AccEff,
			  bool bool_ratio) {

/*	  
 * PlotVectorInOnePlot function make pdf 1-pad plots from consider vector of hostograms
 */
  
  //setup canvas		
  TCanvas c("c", "canvas",0,0,800,800);
  c.cd();

  //setup pad1 and pad2    
  TPad *pad = 	new TPad("pad1","pad1",0.0,0.0,1,1,0,0,0);

  
  pad->SetRightMargin(0.05);
  pad->SetLeftMargin(0.1);
  pad->SetTicks();
  pad->SetBottomMargin(0.15); 
  pad->SetBorderMode(0);
  //pad->SetGridy(1);

  pad->Draw();
  
  //setup Legend	
  TLegend leg( 0.15, 0.68, 0.95, 0.77) ;
  leg.SetNColumns(2);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  leg.SetTextFont(72);
  leg.SetTextSize(0.02); 
  leg.SetTextFont( 42 );
  
  int nHists = vec_Hists.size();
      
  
  
    //loop over histogram saved in vector  
  for (int iHist = 0 ;iHist < nHists; ++iHist){	
    cout << "loop over histograms to plot: " << iHist << "/" << nHists << endl;

    //Call function to plot one histogram 
    TString AccEff;
    if (str_AccEff.Contains("Acc") || str_AccEff.Contains("acc") ) 		AccEff = "Acc";
    else if (str_AccEff.Contains("Eff") || str_AccEff.Contains("eff") )	AccEff = "Eff";
    else TtbarDiffCrossSection_exception("Error: Wrong setup variable TString str_AccEff");
    
    if ( str_AccEff.Contains("Only") ) AccEff.Append("Only");
    
    int index_ratio = iHist + 1;	// nominal sample is located at zero pozition in vectro for at legend and colors
    DrawOneAcc(vec_Hists.at(iHist), nameHist, level, label, vec_colorMarkers.at(index_ratio), vec_colorUnc.at(index_ratio), pad, AccEff, bool_ratio);
  
    //Drawing
  
    if( iHist == 0 ) vec_Hists.at(iHist)->Draw("p e2");
    else vec_Hists.at(iHist)->Draw("p e2 same");
	    
    TString nameHist = vec_Hists.at(iHist)->GetName();
    TString nameLeg = vec_LegendName.at(index_ratio); 					
    if (nameLeg.Contains("_")) nameLeg.ReplaceAll("_", " ");
  
    leg.AddEntry( vec_Hists.at(iHist), nameLeg, "pf" );				
  }
       
  
  cout << "level: " << level << endl;
  
  TString info = level.Contains("Parton") ? "Parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV" : "Fiducial phase-space";
  
  functions::WriteGeneralInfo(info,"",0.025,0.17,0.9,0.04);
  //functions::WriteInfo(info, 0.025, 0.33, 0.81);
  
  if(str_AccEff.Contains("Acc"))  functions::WriteInfo("Acceptance ratio = #frac{f_{acc}^{MC}}{f_{acc}^{Powheg + Pythia8}}"	 , 0.025, 0.155, 0.62); 	//0.3,0.35,0.7, 1);	
  if(str_AccEff.Contains("Eff"))  functions::WriteInfo("Efficiency ratio = #frac{#epsilon_{eff}^{MC}}{#epsilon_{eff}^{Powheg + Pythia8}}", 0.025, 0.155, 0.62); //0.35,0.7,);
  
  leg.Draw();
  
  
  
  TString outPut_path_and_name = outputPath+"/"+nameHist+"_"+level+"Level_All_"+str_AccEff+"_ratio.pdf";
  cout << "save: " << outPut_path_and_name << endl;
  c.SaveAs(outPut_path_and_name);
  //cout << "\n\n\n saving canvas: " + level+"Level_"+spectrum+"_AllAcceptanceInOnePlot.pdf \n\n\n" << endl;
  //PrintCanvas(&c,outputPath,subdir+"/"+level+"Level_"+spectrum+"_AllAcceptanceInOnePlot", false);
  //PrintCanvas(&c,outputPath,subdir+"/"+level+"Level_"+spectrum+"_AllAcceptanceInOnePlot", true);

}  



//*************************************
//*************************************



vector<TString> getVecOfHistNames(TString keyString, TFile* filein ){ //const char* filein){
 
  cout << "getVecOfHistNames" << endl;
  vector<TString> vec_Hists_name;
  //TFile* fin = new TFile(filein) ;
  
  //cout << "fin = new" << endl;
  //if (!fin->IsOpen()) {
  //  printf("<E> Cannot open input file %s\n",filein) ;
  //  exit(1) ;
  //}
  
  //cout << "if c1" << endl;
  
  TList* list = filein->GetListOfKeys() ;
  if (!list) { printf("<E> No keys found in file\n") ; exit(1) ; }
  TIter next(list) ;
  TKey* key ;
  TObject* obj ;
  
  while ( (key = (TKey*)next()) ) {
    obj = key->ReadObj() ;
    //if (    (strcmp(obj->IsA()->GetName(),"TProfile")!=0)  && (!obj->InheritsFrom("TH2"))   && (!obj->InheritsFrom("TH1"))     ) {
    //  printf("<W> Object %s is not 1D or 2D histogram : "
    //         "will not be converted\n",obj->GetName()) ;
    //}
    TString nameHist = obj->GetName();
    
    //printf("Histo name:%s title:%s\n",nameHist);
    if ( strstr( nameHist, keyString ) ){
      vec_Hists_name.push_back( nameHist );
      cout << "There is a match" << endl;
    }
  }
  //cout << "Return" << endl;
  return(vec_Hists_name);
}








//////////////////////////////////////////
//			PlotVectorIn2Pads			//
//////////////////////////////////////////

void PlotVectorIn2Pads( const vector<TH1D*>& vec_Hists,
			const vector<TString>& vec_LegendName,
			const vector<int>& vec_colorMarkers,
			const vector<int>& vec_colorUnc,
			TString nameHist,
			TString outputPath,
			TString level,
			TString label,
			TString str_AccEff){

/*
 * PlotVectorIn2Pads makes 2 pads pdf plots with ratio pad
 */
		
		
  //setup canvas		
  TCanvas c("c", "canvas",0,0,800,800);
  c.cd();
    
  //setup pad1 and pad2    
  TPad *pad1 = 	new TPad("pad1","pad1",0,0.5,1,1,0,0,0);
  TPad *pad2 =	new TPad("pad2","pad2",0,0,1,0.5,0,0,0);
  
  pad1->SetRightMargin(0.05);
  pad1->SetTicks();
  pad1->SetBottomMargin(0.01); 
  pad1->SetBorderMode(0);
  pad1->Draw();
  
  
  pad2->SetBottomMargin(0.35);
  pad2->SetTopMargin(0.0);
  pad2->SetRightMargin(0.05);
  pad2->SetTicks();
  pad2->SetGridy(1);
  pad2->Draw();
  

  //setup Legend	
  TLegend leg( 0.13, 0.48, 0.93, 0.70) ;
  leg.SetNColumns(2);
  leg.SetFillColor(0);
  leg.SetFillStyle(0);
  leg.SetBorderSize(0);
  leg.SetTextFont(72);
  leg.SetTextSize(0.048); 
  leg.SetTextFont( 42 );	
  
  TH1D* h_nominal = (TH1D*)vec_Hists.at(0)->Clone();
  //TH1D* h_nominal_ratio;
  cout << "pause ... h_nominal->Integral = " << h_nominal->Integral() << "\n\n\n";
  unsigned int nHists = vec_Hists.size();
  for (unsigned int iHist = 0 ;iHist < nHists; ++iHist) {	
    cout << "loop over histograms to plot: " << iHist << "/" << nHists << endl;
    
    //Call function to plot one histogram 
    
    
    TString AccEff;
    if (str_AccEff.Contains("Acc") || str_AccEff.Contains("acc") ) 		AccEff = "Acc";
    else if (str_AccEff.Contains("Eff") || str_AccEff.Contains("eff") )	AccEff = "Eff";
    else TtbarDiffCrossSection_exception("Error: Wrong setup variable TString str_AccEff");
    
    int index_ratio = iHist;// + 1;	// nominal sample is located at zero pozition in vectro for at legend and colors
    
    //pad1
    pad1->cd();
    DrawOneAcc(vec_Hists.at(iHist), nameHist, level, label, vec_colorMarkers.at(index_ratio), vec_colorUnc.at(index_ratio), pad1, AccEff, false);
    
    //Drawing
    if(iHist == 0) vec_Hists.at(iHist)->Draw("p e2");
    else if(iHist == nHists -1){
      pad1->cd();
      vec_Hists.at(iHist)->Draw("p e2 same");
      vec_Hists.at(0)->Draw("p e2 same");			//nominal at the top
    
    }
    else vec_Hists.at(iHist)->Draw("p e2 same");
    
    
    TString nameHist = vec_Hists.at(iHist)->GetName();
    TString nameLeg = vec_LegendName.at(index_ratio); 					
    if (nameLeg.Contains("_")) nameLeg.ReplaceAll("_", " ");
    
    leg.AddEntry( vec_Hists.at(iHist), nameLeg, "pf" );		
  
  
  //pad2
  //if(iHist>=1){
    TH1D* h_ratio = (TH1D*)vec_Hists.at(iHist)->Clone();
    cout << "first two bins before  divide: " << h_ratio->GetBinContent(1) << endl;
  
    h_ratio->Divide(h_ratio, h_nominal);
    
  
    cout << "eff_integral - ratio: " << h_ratio->Integral() << endl;
    cout << "first two bins afret divide: " << h_ratio->GetBinContent(1) << endl;
  
    pad2->cd();
  
  //if(iHist == 0)	h_nominal_ratio = (TH1D*)h_ratio->Clone();
    DrawOneAcc(h_ratio, nameHist, level, label, vec_colorMarkers.at(index_ratio), vec_colorUnc.at(index_ratio), pad2, AccEff, true);
    
    //Drawing
    if( iHist == 0 ) continue; 							//h_ratio->Draw("p e2");
    else if(iHist == nHists -1){
      pad2->cd();
      h_ratio->Draw("p e2 same");
      //h_nominal_ratio->Draw("p e2 same");			//nominal at the top
      
      double x_min = h_ratio->GetXaxis()->GetXmin();
      double x_max = h_ratio->GetXaxis()->GetXmax();
      TLine* line_one = new TLine (x_min, 1.0, x_max, 1.0);
      line_one->SetLineStyle(3);
      line_one->Draw();
	    
    }
    else h_ratio->Draw("p e2 same");
  }

           
           
  pad1->cd();
  leg.Draw();
  c.cd();      
  cout << "level: " << level << endl;

  TString info = level.Contains("Parton") ? "Parton level, p^{t,1}_{T} > 500 GeV, p^{t,2}_{T} > 350 GeV" : "Fiducial phase-space";

  functions::WriteGeneralInfo(info,"",0.025,0.17,0.91);
  functions::WriteInfo(info, 0.025, 0.33, 0.875);
  
  //functions::WriteGeneralInfo(label,"",0.045,0.18,0.885);
 
  //  TString str_level = level;
  //  str_level.Append(" level");

    
	//if(str_AccEff.Contains("Acc"))  functions::WriteInfo("Acceptance ratio = #frac{f_{acc}^{MC}}{f_{acc}^{Powheg + Pythia8}}"	 , 0.025, 0.155, 0.62); 	//0.3,0.35,0.7, 1);	
	//if(str_AccEff.Contains("Eff"))  functions::WriteInfo("Efficiency ratio = #frac{#epsilon_{eff}^{MC}}{#epsilon_{eff}^{Powheg + Pythia8}}", 0.025, 0.155, 0.62); //0.35,0.7,);



    
    TString outPut_path_and_name = outputPath+"/"+nameHist+"_"+level+"Level_All_"+str_AccEff+"_And_ratio.pdf";
    cout << "save: " << outPut_path_and_name << endl;
    c.SaveAs(outPut_path_and_name);
    //cout << "\n\n\n saving canvas: " + level+"Level_"+spectrum+"_AllAcceptanceInOnePlot.pdf \n\n\n" << endl;
    //PrintCanvas(&c,outputPath,subdir+"/"+level+"Level_"+spectrum+"_AllAcceptanceInOnePlot", false);
    //PrintCanvas(&c,outputPath,subdir+"/"+level+"Level_"+spectrum+"_AllAcceptanceInOnePlot", true);

}  
						
		
