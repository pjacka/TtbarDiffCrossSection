#include "TtbarDiffCrossSection/HistStore.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "CovarianceCalculator/CovarianceCalculatorFunctions.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"

ClassImp(HistStore)

    HistStore::HistStore() : m_gap("           ")
{

   m_counter = 0;
   m_nominalHistosLoaded = 0;
   m_doRebin = false;
   m_useResolutionHistos = false;
   m_hist_data_sys = nullptr;
}

//----------------------------------------------------------------------

void HistStore::initialize(FileStore *fileStore, int debugLevel, const TString &config, const TString &configBinningName)
{
   m_debug = debugLevel;
   m_fileStore = fileStore;
   m_config_binning_name = configBinningName;
   m_config_binning = std::make_shared<TEnv>(configBinningName);
   m_ttbarConfig = std::make_unique<NtuplesAnalysisToolsConfig>(config);
   m_nominalTreeName = m_ttbarConfig->nominalFolderName();
   set_ABCD16_indexes();
   set_ABCD16_folder_names();
   m_lumi = m_ttbarConfig->lumi();
   m_ttbarSF = m_ttbarConfig->ttbar_SF();
}

//----------------------------------------------------------------------

void HistStore::set_ABCD16_indexes()
{
   m_iRegionA = 0;
   m_iRegionB = 1;
   m_iRegionC = 2;
   m_iRegionD = 3;
   m_iRegionE = 4;
   m_iRegionF = 5;
   m_iRegionG = 6;
   m_iRegionH = 7;
   m_iRegionI = 8;
   m_iRegionJ = 9;
   m_iRegionK = 10;
   m_iRegionL = 11;
   m_iRegionM = 12;
   m_iRegionN = 13;
   m_iRegionO = 14;
   m_iRegionP = 15;
}

//----------------------------------------------------------------------

void HistStore::set_ABCD16_folder_names()
{
   m_ABCD16_folder_names.resize(16);
   m_ABCD16_folder_names[m_iRegionA] = "Leading_0t_0b_Recoil_0t_0b_tight";
   m_ABCD16_folder_names[m_iRegionB] = "Leading_0t_0b_Recoil_0t_1b_tight";
   m_ABCD16_folder_names[m_iRegionC] = "Leading_1t_0b_Recoil_0t_0b_tight";
   m_ABCD16_folder_names[m_iRegionD] = "Leading_1t_0b_Recoil_0t_1b_tight";
   m_ABCD16_folder_names[m_iRegionE] = "Leading_0t_0b_Recoil_1t_0b_tight";
   m_ABCD16_folder_names[m_iRegionF] = "Leading_1t_0b_Recoil_1t_0b_tight";
   m_ABCD16_folder_names[m_iRegionG] = "Leading_0t_1b_Recoil_1t_0b_tight";
   m_ABCD16_folder_names[m_iRegionH] = "Leading_0t_1b_Recoil_0t_1b_tight";
   m_ABCD16_folder_names[m_iRegionI] = "Leading_0t_1b_Recoil_0t_0b_tight";
   m_ABCD16_folder_names[m_iRegionJ] = "Leading_0t_0b_Recoil_1t_1b_tight";
   m_ABCD16_folder_names[m_iRegionK] = "Leading_1t_0b_Recoil_1t_1b_tight";
   m_ABCD16_folder_names[m_iRegionL] = "Leading_0t_1b_Recoil_1t_1b_tight";
   m_ABCD16_folder_names[m_iRegionM] = "Leading_1t_1b_Recoil_1t_0b_tight";
   m_ABCD16_folder_names[m_iRegionN] = "Leading_1t_1b_Recoil_0t_1b_tight";
   m_ABCD16_folder_names[m_iRegionO] = "Leading_1t_1b_Recoil_0t_0b_tight";
   m_ABCD16_folder_names[m_iRegionP] = "Leading_1t_1b_Recoil_1t_1b_tight";

   m_ABCD16_region_names.resize(16);
   m_ABCD16_region_names[m_iRegionA] = "A";
   m_ABCD16_region_names[m_iRegionB] = "B";
   m_ABCD16_region_names[m_iRegionC] = "C";
   m_ABCD16_region_names[m_iRegionD] = "D";
   m_ABCD16_region_names[m_iRegionE] = "E";
   m_ABCD16_region_names[m_iRegionF] = "F";
   m_ABCD16_region_names[m_iRegionG] = "G";
   m_ABCD16_region_names[m_iRegionH] = "H";
   m_ABCD16_region_names[m_iRegionI] = "I";
   m_ABCD16_region_names[m_iRegionJ] = "J";
   m_ABCD16_region_names[m_iRegionK] = "K";
   m_ABCD16_region_names[m_iRegionL] = "L";
   m_ABCD16_region_names[m_iRegionM] = "M";
   m_ABCD16_region_names[m_iRegionN] = "N";
   m_ABCD16_region_names[m_iRegionO] = "O";
   m_ABCD16_region_names[m_iRegionP] = "P";
}

//----------------------------------------------------------------------

void HistStore::load_nominal_histos(const TString &histname, const TString &level)
{
   if (m_debug > 0)
      cout << "HistStore: Loading nominal histos for variable: " << histname << endl;
   m_variable_name = histname;
   m_level = level;

   this->prepare_binning(); // Load binning from config file. Binning will be used for rebinning.

   const TString chain_name = m_nominalTreeName;
   const TString foldername = "unfolding";
   const TString name = histname + "_RecoLevel";

   m_fileStore->get_16regions_histos(m_hist_data_16regions_nominal, m_hist_MC_16regions_nominal, chain_name, foldername, name);

   if (m_doRebin)
   {
      const int nRegions = m_hist_data_16regions_nominal.size();
      for (int iRegion = 0; iRegion < nRegions; iRegion++)
      {
         m_hist_data_16regions_nominal[iRegion] = (TH1D *)m_hist_data_16regions_nominal[iRegion]->Rebin(m_nbins_reco, m_hist_data_16regions_nominal[iRegion]->GetName(), &m_binning[0]);
         m_hist_MC_16regions_nominal[iRegion] = (TH1D *)m_hist_MC_16regions_nominal[iRegion]->Rebin(m_nbins_reco, m_hist_MC_16regions_nominal[iRegion]->GetName(), &m_binning[0]);
      }
   }
   const int ntfiles = m_fileStore->get_n_input_tfiles();

   m_hist_data = (TH1D *)m_fileStore->getHistogramObject(0, chain_name, foldername, name, false);

   // checking 1D binning
   if (!functions::checkNewBinning1D(m_hist_data->GetXaxis(), m_binning))
      throw TtbarDiffCrossSection_exception((TString) "Error: New binning is not consistent with original binning!\n      Variable name: " + m_variable_name +
                                            "\n      Level: " + level);

   if (m_doRebin)
      m_hist_data = (TH1D *)m_hist_data->Rebin(m_nbins_reco, (TString)m_hist_data->GetName(), &m_binning[0]);

   m_hist_template = (TH1D *)m_hist_data->Clone();
   m_hist_template->Reset();
   m_hist_template->Sumw2();

   m_hist_signal_nominal = (TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, name);
   m_hist_signal_nominal->Scale(m_ttbarSF);
   if (m_doRebin)
      m_hist_signal_nominal = (TH1D *)m_hist_signal_nominal->Rebin(m_nbins_reco, (TString)m_hist_signal_nominal->GetName(), &m_binning[0]);

   m_hist_MC_backgrounds_nominal.resize(ntfiles - 2);
   m_hist_MC_backgrounds_nominal[0] = (TH1D *)m_fileStore->getHistogramObject(2, chain_name, foldername, name);
   m_hist_MC_backgrounds_nominal[0]->Scale(m_ttbarSF);
   if (m_doRebin)
      m_hist_MC_backgrounds_nominal[0] = (TH1D *)m_hist_MC_backgrounds_nominal[0]->Rebin(m_nbins_reco, (TString)m_hist_MC_backgrounds_nominal[0]->GetName(), &m_binning[0]);

   m_hist_bkg_MC_nominal = (TH1D *)m_hist_MC_backgrounds_nominal[0]->Clone();

   for (int ifile = 3; ifile < ntfiles; ifile++)
   {
      m_hist_MC_backgrounds_nominal[ifile - 2] = (TH1D *)m_fileStore->getHistogramObject(ifile, chain_name, foldername, name);
      if (m_doRebin)
         m_hist_MC_backgrounds_nominal[ifile - 2] = (TH1D *)m_hist_MC_backgrounds_nominal[ifile - 2]->Rebin(m_nbins_reco, (TString)m_hist_MC_backgrounds_nominal[ifile - 2]->GetName(), &m_binning[0]);
      m_hist_bkg_MC_nominal->Add(m_hist_MC_backgrounds_nominal[ifile - 2]);
   }

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";

   m_migration_nominal = (TH2D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration");
   m_migration_nominal->Scale(m_ttbarSF);
   if (m_doRebin)
      m_migration_nominal = functions::Rebin2D(m_migration_nominal, m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], "");

   if (m_useResolutionHistos)
   {
      m_resolution_nominal = (TH2D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_resolution");
      m_resolution_nominal->Scale(m_ttbarSF);
   }

   m_EffOfRecoLevelCutsNominator_nominal = m_migration_nominal->ProjectionY("nominator_for_efficiency", 1, m_migration_nominal->GetNbinsX(), "e");
   m_EffOfTruthLevelCutsNominator_nominal = m_migration_nominal->ProjectionX("nominator_for_acceptance", 1, m_migration_nominal->GetNbinsY(), "e");

   TH1D *matchingEfficiencyRecoTruthDenominator_nominal = (TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_matchingEfficiencyDenominator");
   if (m_doRebin)
      matchingEfficiencyRecoTruthDenominator_nominal = (TH1D *)matchingEfficiencyRecoTruthDenominator_nominal->Rebin(m_nbins_reco, matchingEfficiencyRecoTruthDenominator_nominal->GetName(), &m_binning[0]);

   m_matchingEfficiencyRecoTruth_nominal = (TH1D *)m_EffOfTruthLevelCutsNominator_nominal->Clone();
   m_matchingEfficiencyRecoTruth_nominal->Divide(m_matchingEfficiencyRecoTruth_nominal, matchingEfficiencyRecoTruthDenominator_nominal, 1, 1, "B");

   m_hist_signal_truth_nominal = (TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level);
   m_hist_signal_truth_nominal->Scale(m_ttbarSF);

   if (!functions::checkNewBinning1D(m_hist_signal_truth_nominal->GetXaxis(), m_binning_truth))
      throw TtbarDiffCrossSection_exception((TString) "Error: New truth binning is not consistent with original binning!\n      Variable name: " + m_variable_name +
                                            "\n      Level: " + level);
   if (m_doRebin)
      m_hist_signal_truth_nominal = (TH1D *)m_hist_signal_truth_nominal->Rebin(m_nbins_truth, (TString)m_hist_signal_truth_nominal->GetName(), &m_binning_truth[0]);

   if (m_debug > 0)
      cout << m_gap + "Calculating nominal ABCD16 estimate" << endl;

   m_hist_bkg_Multijet_nominal = Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, m_hist_MC_16regions_nominal, true);
   m_hist_bkg_all_nominal = (TH1D *)m_hist_bkg_Multijet_nominal->Clone();
   m_hist_bkg_all_nominal->Add(m_hist_bkg_MC_nominal);
   m_hist_pseudodata_nominal = (TH1D *)m_hist_bkg_all_nominal->Clone();
   m_hist_pseudodata_nominal->Add(m_hist_signal_nominal);
   m_nominalHistosLoaded = true;

   if (m_ttbarConfig->fillClosureTestHistos())
   {
      m_hist_signal_nominal_half1 = std::unique_ptr<TH1D>{(TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, name + "_half1")};
      m_hist_signal_nominal_half2 = std::unique_ptr<TH1D>{(TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, name + "_half2")};
      m_hist_signal_truth_nominal_half1 = std::unique_ptr<TH1D>{(TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level + "_half1")};
      m_hist_signal_truth_nominal_half2 = std::unique_ptr<TH1D>{(TH1D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level + "_half2")};
      m_migration_nominal_half1 = std::unique_ptr<TH2D>{(TH2D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration_half1")};
      m_migration_nominal_half2 = std::unique_ptr<TH2D>{(TH2D *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration_half2")};

      if (m_doRebin)
      {
         m_hist_signal_nominal_half1 = std::unique_ptr<TH1D>{(TH1D *)m_hist_signal_nominal_half1->Rebin(m_nbins_reco, (TString)m_hist_signal_nominal_half1->GetName(), &m_binning[0])};
         m_hist_signal_nominal_half2 = std::unique_ptr<TH1D>{(TH1D *)m_hist_signal_nominal_half2->Rebin(m_nbins_reco, (TString)m_hist_signal_nominal_half2->GetName(), &m_binning[0])};
         m_hist_signal_truth_nominal_half1 = std::unique_ptr<TH1D>{(TH1D *)m_hist_signal_truth_nominal_half1->Rebin(m_nbins_truth, (TString)m_hist_signal_truth_nominal_half1->GetName(), &m_binning_truth[0])};
         m_hist_signal_truth_nominal_half2 = std::unique_ptr<TH1D>{(TH1D *)m_hist_signal_truth_nominal_half2->Rebin(m_nbins_truth, (TString)m_hist_signal_truth_nominal_half2->GetName(), &m_binning_truth[0])};
         m_migration_nominal_half1 = std::unique_ptr<TH2D>{functions::Rebin2D(m_migration_nominal_half1.get(), m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], "")};
         m_migration_nominal_half2 = std::unique_ptr<TH2D>{functions::Rebin2D(m_migration_nominal_half2.get(), m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], "")};
      }
   }

   delete matchingEfficiencyRecoTruthDenominator_nominal;
}

//----------------------------------------------------------------------

void HistStore::load_nominal_histosND(const TString &histname, const TString &level)
{

   if (m_debug > 0)
      cout << "HistStore: Loading nominal 2D histos for variable: " << histname << endl;

   m_variable_name = histname;
   m_level = level;

   this->prepare_binningND();

   const TString chain_name = m_nominalTreeName;
   const TString foldername = "unfolding";
   const TString name = histname + "_RecoLevel";

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";

   const int ntfiles = m_fileStore->get_n_input_tfiles();

   auto helphist_reco = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(0, chain_name, foldername, name, false));
   auto helphist_truth = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level));

   if (!m_histogramConverterND->checkConsistency(helphist_reco.get()) || !m_histogramConverterNDTruth->checkConsistency(helphist_truth.get()))
   {
      throw TtbarDiffCrossSection_exception("Error in binning consistency check! New binning is not consistent with original binning.");
   }

   m_hist_data = m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_data");

   m_hist_signal_truth_nominal = m_histogramConverterNDTruth->makeConcatenatedHistogram(helphist_truth.get(), m_variable_name + "_signal_truth");
   m_hist_signal_truth_nominal->Scale(m_ttbarSF);

   helphist_reco = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, name));
   m_hist_signal_nominal = m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_signal_reco");
   m_hist_signal_nominal->Scale(m_ttbarSF);

   m_hist_MC_backgrounds_nominal.resize(ntfiles - 2);

   helphist_reco = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(2, chain_name, foldername, name));
   m_hist_MC_backgrounds_nominal[0] = m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_bkg_2");
   m_hist_MC_backgrounds_nominal[0]->Scale(m_ttbarSF);

   m_hist_bkg_MC_nominal = (TH1D *)m_hist_MC_backgrounds_nominal[0]->Clone();

   for (int ifile = 3; ifile < ntfiles; ifile++)
   {
      helphist_reco = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(ifile, chain_name, foldername, name));
      m_hist_MC_backgrounds_nominal[ifile - 2] = m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_bkg_" + functions::intToString(ifile));
      m_hist_bkg_MC_nominal->Add(m_hist_MC_backgrounds_nominal[ifile - 2]);
   }

   helphist_reco = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getObject(m_fileStore->get_input_tfile(1), chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration"));
   // helphist_reco->Scale(m_lumi);
   helphist_reco->Scale(m_ttbarSF);

   m_migration_nominal = m_histogramConverterND->make2DConcatenatedHistogram(helphist_reco.get(), m_histogramConverterNDTruth.get(), m_variable_name + "_migration");

   if (m_ttbarConfig->fillClosureTestHistos())
   {
      helphist_reco = std::unique_ptr<THnSparseD>{(THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, name + "_half1")};
      m_hist_signal_nominal_half1 = std::unique_ptr<TH1D>{m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_reco1")};

      helphist_reco = std::unique_ptr<THnSparseD>{(THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, name + "_half2")};
      m_hist_signal_nominal_half2 = std::unique_ptr<TH1D>{m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_reco2")};

      helphist_truth = std::unique_ptr<THnSparseD>{(THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level + "_half1")};
      m_hist_signal_truth_nominal_half1 = std::unique_ptr<TH1D>{m_histogramConverterNDTruth->makeConcatenatedHistogram(helphist_truth.get(), m_variable_name + "_truth1")};

      helphist_truth = std::unique_ptr<THnSparseD>{(THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level + "_half2")};
      m_hist_signal_truth_nominal_half2 = std::unique_ptr<TH1D>{m_histogramConverterNDTruth->makeConcatenatedHistogram(helphist_truth.get(), m_variable_name + "_truth2")};

      helphist_reco = std::unique_ptr<THnSparseD>{(THnSparseD *)m_fileStore->getObject(m_fileStore->get_input_tfile(1), chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration_half1")};
      m_migration_nominal_half1 = std::unique_ptr<TH2D>{m_histogramConverterND->make2DConcatenatedHistogram(helphist_reco.get(), m_histogramConverterNDTruth.get(), m_variable_name + "_migration1")};

      helphist_reco = std::unique_ptr<THnSparseD>{(THnSparseD *)m_fileStore->getObject(m_fileStore->get_input_tfile(1), chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration_half2")};
      m_migration_nominal_half2 = std::unique_ptr<TH2D>{m_histogramConverterND->make2DConcatenatedHistogram(helphist_reco.get(), m_histogramConverterNDTruth.get(), m_variable_name + "_migration2")};
   }

   m_nbins_truth = m_migration_nominal->GetNbinsY();

   m_EffOfRecoLevelCutsNominator_nominal = m_migration_nominal->ProjectionY("nominator_for_efficiency", 1, m_migration_nominal->GetNbinsX(), "e");
   m_EffOfTruthLevelCutsNominator_nominal = m_migration_nominal->ProjectionX("nominator_for_acceptance", 1, m_migration_nominal->GetNbinsY(), "e");

   helphist_reco = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(1, chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_matchingEfficiencyDenominator"));
   m_matchingEfficiencyRecoTruth_nominal = m_histogramConverterND->makeConcatenatedHistogram(helphist_reco.get(), m_variable_name + "_matchingEfficiencyRecoTruth");
   m_matchingEfficiencyRecoTruth_nominal->Divide(m_EffOfTruthLevelCutsNominator_nominal, m_matchingEfficiencyRecoTruth_nominal, 1, 1, "B");

   if (m_debug > 0)
      cout << m_gap + "Calculating nominal ABCD16 estimate" << endl;

   vector<THnSparseD *> histosData, histosMC;
   m_fileStore->get_16regions_histos(histosData, histosMC, chain_name, foldername, name);
   m_hist_data_16regions_nominal.resize(15);
   m_hist_MC_16regions_nominal.resize(15);
   for (int i = 0; i < 15; i++)
   {
      m_hist_data_16regions_nominal[i] = m_histogramConverterND->makeConcatenatedHistogram(histosData[i], m_variable_name + "_data_" + m_ABCD16_region_names[i]);
      m_hist_MC_16regions_nominal[i] = m_histogramConverterND->makeConcatenatedHistogram(histosMC[i], m_variable_name + "_MC_" + m_ABCD16_region_names[i]);

      delete histosData[i];
      delete histosMC[i];
   }
   histosData.clear();
   histosMC.clear();

   m_hist_bkg_Multijet_nominal = Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, m_hist_MC_16regions_nominal, true);
   m_hist_bkg_all_nominal = (TH1D *)m_hist_bkg_Multijet_nominal->Clone();
   m_hist_bkg_all_nominal->Add(m_hist_bkg_MC_nominal);
   m_hist_pseudodata_nominal = (TH1D *)m_hist_bkg_all_nominal->Clone();
   m_hist_pseudodata_nominal->Add(m_hist_signal_nominal);

   m_nominalHistosLoaded = true;
}

//----------------------------------------------------------------------

void HistStore::delete_nominal_histos()
{
   if (!m_nominalHistosLoaded)
      return;
   delete m_hist_data;
   delete m_hist_signal_nominal;
   delete m_hist_bkg_MC_nominal;
   delete m_hist_bkg_Multijet_nominal;
   delete m_hist_bkg_all_nominal;
   delete m_hist_pseudodata_nominal;
   for (int i = 0; i < (int)m_hist_data_16regions_nominal.size(); i++)
   {
      delete m_hist_data_16regions_nominal[i];
      delete m_hist_MC_16regions_nominal[i];
   }
   m_hist_data_16regions_nominal.clear();
   m_hist_MC_16regions_nominal.clear();

   for (size_t i = 0; i < m_hist_MC_backgrounds_nominal.size(); i++)
      delete m_hist_MC_backgrounds_nominal[i];
   m_hist_MC_backgrounds_nominal.clear();

   for (size_t i = 0; i < m_hist_MC_backgrounds_sys.size(); i++)
      delete m_hist_MC_backgrounds_sys[i];
   m_hist_MC_backgrounds_sys.clear();

   delete m_migration_nominal;
   if (m_useResolutionHistos)
      delete m_resolution_nominal;
   delete m_EffOfRecoLevelCutsNominator_nominal;
   delete m_EffOfTruthLevelCutsNominator_nominal;
   delete m_matchingEfficiencyRecoTruth_nominal;
   delete m_hist_signal_truth_nominal;
   m_nominalHistosLoaded = false;
}

//----------------------------------------------------------------------

void HistStore::delete_nominal_histosND()
{
   if (!m_nominalHistosLoaded)
      return;
   delete_nominal_histos();
   m_nominalHistosLoaded = false;
}

//----------------------------------------------------------------------

void HistStore::load_one_sys_histos(const TString &chain_name)
{

   if (!m_nominalHistosLoaded)
      throw TtbarDiffCrossSection_exception("Error in load_one_sys_histos: Nominal histos are not loaded!");

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";
   const TString foldername = "unfolding";
   const TString name = m_variable_name + "_RecoLevel";
   const int isignal = 1;
   const int ntfiles = m_fileStore->get_n_input_tfiles();
   const TString migration_name = prefix1 + prefix2 + m_variable_name + "_migration";
   const TString truth_hist_name = prefix1 + prefix3 + m_variable_name + "_" + m_level;

   bool isLargeRJERSys = functions::isLargeRJERSys(chain_name);

   if (isLargeRJERSys)
   {
      m_hist_data_sys = (TH1D *)m_fileStore->getHistogramObject(0, chain_name, foldername, name, false);
      if (m_doRebin)
         m_hist_data_sys = (TH1D *)m_hist_data_sys->Rebin(m_nbins_reco, (TString)m_hist_data_sys->GetName(), &m_binning[0]);
   }

   m_hist_signal_sys = (TH1D *)m_fileStore->getHistogramObject(isignal, chain_name, foldername, name);
   m_hist_signal_sys->Scale(m_ttbarSF);
   if (m_doRebin)
      m_hist_signal_sys = (TH1D *)m_hist_signal_sys->Rebin(m_nbins_reco, (TString)m_hist_signal_sys->GetName(), &m_binning[0]);

   // JH: For systamatic unc. calculation for backgrounds:
   // m_hist_MC_backgrounds_sys.resize(ntfiles-2);
   // std::cout << chain_name << " " << foldername << " " << name << std::endl;
   // std::cout << functions::isPDFSys(chain_name) << std::endl;
   // m_hist_MC_backgrounds_sys[0] =(TH1D*)m_fileStore->getHistogramObject(2,chain_name,foldername,name);
   // m_hist_MC_backgrounds_sys[0]->Scale(m_ttbarSF);

   // if(m_doRebin) m_hist_MC_backgrounds_sys[0] = (TH1D*)m_hist_MC_backgrounds_sys[0]->Rebin(m_nbins_reco,(TString)m_hist_MC_backgrounds_sys[0]->GetName(),&m_binning[0]);

   // m_hist_bkg_MC_sys = (TH1D*)m_hist_MC_backgrounds_sys[0]->Clone();

   // for(int ifile=3;ifile<ntfiles;ifile++) {
   // m_hist_MC_backgrounds_sys[ifile-2] = (TH1D*)m_fileStore->getHistogramObject(ifile,chain_name,foldername,name);
   // if(m_doRebin) m_hist_MC_backgrounds_sys[ifile-2] = (TH1D*)m_hist_MC_backgrounds_sys[ifile-2]->Rebin(m_nbins_reco,(TString)m_hist_MC_backgrounds_sys[ifile-2]->GetName(),&m_binning[0]);
   // m_hist_bkg_MC_sys->Add(m_hist_MC_backgrounds_sys[ifile-2]);
   //}

   m_hist_migration_sys = (TH2D *)m_fileStore->getHistogramObject(isignal, chain_name, foldername, migration_name);
   m_hist_migration_sys->Scale(m_ttbarSF);
   if (m_doRebin)
      m_hist_migration_sys = functions::Rebin2D(m_hist_migration_sys, m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], "");

   m_hist_EffOfRecoLevelCutsNominator_sys = m_hist_migration_sys->ProjectionY("nominator_for_efficiency", 1, m_hist_migration_sys->GetNbinsX(), "e");
   m_hist_EffOfTruthLevelCutsNominator_sys = m_hist_migration_sys->ProjectionX("nominator_for_acceptance", 1, m_hist_migration_sys->GetNbinsY(), "e");

   if (std::find(m_sys_chain_names_pdf.begin(), m_sys_chain_names_pdf.end(), chain_name) != m_sys_chain_names_pdf.end() ||
       std::find(m_reweighted_chain_names.begin(), m_reweighted_chain_names.end(), chain_name) != m_reweighted_chain_names.end())
   {
      m_fileStore->get_16regions_histos_MC_for_pdf(m_hist_MC_16regions_sys, chain_name, foldername, name);
      if (chain_name == "reweighted44" || chain_name == "reweighted45")
      {
         m_hist_bkg_MC_sys = (TH1D *)m_fileStore->getHistogramObject(2, chain_name, foldername, name);
         m_hist_bkg_MC_sys->Scale(m_ttbarSF);
         if (m_doRebin)
            m_hist_bkg_MC_sys = (TH1D *)m_hist_bkg_MC_sys->Rebin(m_nbins_reco, (TString)m_hist_bkg_MC_sys->GetName(), &m_binning[0]);

         TH1D *helphist = 0;
         for (int ifile = 3; ifile < ntfiles; ifile++)
         {
            helphist = (TH1D *)m_fileStore->getHistogramObject(ifile, m_nominalTreeName, foldername, name);
            if (m_doRebin)
               helphist = (TH1D *)helphist->Rebin(m_nbins_reco, (TString)helphist->GetName(), &m_binning[0]);
            m_hist_bkg_MC_sys->Add(helphist);
            delete helphist;
         }
      }
      else
      {
         m_hist_bkg_MC_sys = (TH1D *)m_hist_bkg_MC_nominal->Clone();
      }
      m_hist_signal_truth_sys = (TH1D *)m_fileStore->getHistogramObject(isignal, chain_name, foldername, truth_hist_name);
      m_hist_signal_truth_sys->Scale(m_ttbarSF);
      if (m_doRebin)
         m_hist_signal_truth_sys = (TH1D *)m_hist_signal_truth_sys->Rebin(m_nbins_truth, (TString)m_hist_signal_truth_sys->GetName(), &m_binning_truth[0]);
   }
   else
   {
      m_fileStore->get_16regions_histos_MC_only(m_hist_MC_16regions_sys, chain_name, foldername, name);
      m_hist_bkg_MC_sys = (TH1D *)m_fileStore->getHistogramObject(2, chain_name, foldername, name);
      m_hist_bkg_MC_sys->Scale(m_ttbarSF);
      if (m_doRebin)
         m_hist_bkg_MC_sys = (TH1D *)m_hist_bkg_MC_sys->Rebin(m_nbins_reco, (TString)m_hist_bkg_MC_sys->GetName(), &m_binning[0]);

      TH1D *helphist = 0;
      for (int ifile = 3; ifile < ntfiles; ifile++)
      {
         helphist = (TH1D *)m_fileStore->getHistogramObject(ifile, chain_name, foldername, name);
         if (m_doRebin)
            helphist = (TH1D *)helphist->Rebin(m_nbins_reco, (TString)helphist->GetName(), &m_binning[0]);
         m_hist_bkg_MC_sys->Add(helphist);
         delete helphist;
      }
      m_hist_signal_truth_sys = (TH1D *)m_hist_signal_truth_nominal->Clone();
   }

   if (isLargeRJERSys)
      m_fileStore->get_16regions_histos(m_fileStore->get_input_tfile(0), m_hist_data_16regions_sys, chain_name, foldername, name, false);

   if (m_doRebin)
   {
      const int nRegions = m_hist_data_16regions_nominal.size();
      for (int iRegion = 0; iRegion < nRegions; iRegion++)
      {
         m_hist_MC_16regions_sys[iRegion] = (TH1D *)m_hist_MC_16regions_sys[iRegion]->Rebin(m_nbins_reco, m_hist_MC_16regions_sys[iRegion]->GetName(), &m_binning[0]);
         if (isLargeRJERSys)
            m_hist_data_16regions_sys[iRegion] = (TH1D *)m_hist_data_16regions_sys[iRegion]->Rebin(m_nbins_reco, m_hist_data_16regions_sys[iRegion]->GetName(), &m_binning[0]);
      }
   }

   if (isLargeRJERSys)
      m_hist_bkg_Multijet_sys = Calculate_ABCD16_estimate(m_hist_data_16regions_sys, m_hist_MC_16regions_sys);
   else
      m_hist_bkg_Multijet_sys = Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, m_hist_MC_16regions_sys);
   m_hist_bkg_all_sys = (TH1D *)m_hist_bkg_MC_sys->Clone();
   m_hist_bkg_all_sys->Add(m_hist_bkg_Multijet_sys);
   m_hist_pseudodata_sys = (TH1D *)m_hist_bkg_all_sys->Clone();
   m_hist_pseudodata_sys->Add(m_hist_signal_sys);
}

//----------------------------------------------------------------------

void HistStore::load_one_sys_histosND(const TString &chain_name)
{

   if (!m_nominalHistosLoaded)
      throw TtbarDiffCrossSection_exception("Error in load_one_sys_histos: Nominal histos are not loaded!");

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";
   const TString foldername = "unfolding";
   const TString name = m_variable_name + "_RecoLevel";
   const int isignal = 1;
   const int ntfiles = m_fileStore->get_n_input_tfiles();
   const TString migration_name = prefix1 + prefix2 + m_variable_name + "_migration";
   const TString truth_hist_name = prefix1 + prefix3 + m_variable_name + "_" + m_level;

   auto helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(isignal, chain_name, foldername, name));
   m_hist_signal_sys = m_histogramConverterND->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_signal_reco");
   m_hist_signal_sys->Scale(m_ttbarSF);

   bool isLargeRJERSys = functions::isLargeRJERSys(chain_name);

   if (isLargeRJERSys)
   {
      helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(0, chain_name, foldername, name, false));
      m_hist_data_sys = m_histogramConverterND->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_data");
   }

   helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getObject(m_fileStore->get_input_tfile(1), chain_name, foldername, prefix1 + prefix2 + m_variable_name + "_migration"));
   helphist->Scale(m_ttbarSF);
   m_hist_migration_sys = m_histogramConverterND->make2DConcatenatedHistogram(helphist.get(), m_histogramConverterNDTruth.get(), m_variable_name + "_migration");

   m_hist_EffOfRecoLevelCutsNominator_sys = m_hist_migration_sys->ProjectionY("nominator_for_efficiency", 1, m_hist_migration_sys->GetNbinsX(), "e");
   m_hist_EffOfTruthLevelCutsNominator_sys = m_hist_migration_sys->ProjectionX("nominator_for_acceptance", 1, m_hist_migration_sys->GetNbinsY(), "e");

   vector<THnSparseD *> histos, histosData;
   m_hist_MC_16regions_sys.resize(15);
   if (std::find(m_sys_chain_names_pdf.begin(), m_sys_chain_names_pdf.end(), chain_name) != m_sys_chain_names_pdf.end() ||
       std::find(m_reweighted_chain_names.begin(), m_reweighted_chain_names.end(), chain_name) != m_reweighted_chain_names.end())
   {
      m_fileStore->get_16regions_histos_MC_for_pdf(histos, chain_name, foldername, name);
      // if(chain_name=="reweighted44" || chain_name=="reweighted45") {
      // helphist=std::unique_ptr<THnSparseD>((THnSparseD*)m_fileStore->getHistogramObject(2,chain_name,foldername, name));
      // m_hist_bkg_MC_sys = m_histogramConverterND->makeConcatenatedHistogram(helphist.get(),m_variable_name + "_bkg_2");
      // m_hist_bkg_MC_sys->Scale(m_ttbarSF);
      // for(int ifile=3;ifile<ntfiles;ifile++) {
      // helphist = std::unique_ptr<THnSparseD>((THnSparseD*)m_fileStore->getHistogramObject(ifile,m_nominalTreeName,foldername, name));
      // auto helphist1D = std::unique_ptr<TH1D>(m_histogramConverterND->makeConcatenatedHistogram(helphist.get(),m_variable_name + "_bkg_" + functions::intToString(ifile)));
      // m_hist_bkg_MC_sys->Add(helphist1D.get());
      // }
      //}
      // else

      m_hist_bkg_MC_sys = (TH1D *)m_hist_bkg_MC_nominal->Clone();
      helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(isignal, chain_name, foldername, truth_hist_name));
      m_hist_signal_truth_sys = m_histogramConverterNDTruth->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_signal_reco");
      m_hist_signal_truth_sys->Scale(m_ttbarSF);
   }
   else
   {
      m_fileStore->get_16regions_histos_MC_only(histos, chain_name, foldername, name);
      helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(2, chain_name, foldername, name));
      m_hist_bkg_MC_sys = m_histogramConverterND->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_bkg_2");
      m_hist_bkg_MC_sys->Scale(m_ttbarSF);
      for (int ifile = 3; ifile < ntfiles; ifile++)
      {
         helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getHistogramObject(ifile, chain_name, foldername, name));
         auto helphist1D = std::unique_ptr<TH1D>(m_histogramConverterND->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_bkg_" + functions::intToString(ifile)));
         m_hist_bkg_MC_sys->Add(helphist1D.get());
      }
      m_hist_signal_truth_sys = (TH1D *)m_hist_signal_truth_nominal->Clone();
   }
   for (int i = 0; i < 15; i++)
   {
      m_hist_MC_16regions_sys[i] = m_histogramConverterND->makeConcatenatedHistogram(histos[i], m_variable_name + "_MC_" + m_ABCD16_region_names[i]);
      delete histos[i];
   }
   histos.clear();

   if (isLargeRJERSys)
   {
      m_hist_data_16regions_sys.resize(15);
      m_fileStore->get_16regions_histos<THnSparseD>(m_fileStore->get_input_tfile(0), histosData, chain_name, foldername, name, false);
      for (int i = 0; i < 15; i++)
      {
         m_hist_data_16regions_sys[i] = m_histogramConverterND->makeConcatenatedHistogram(histosData[i], m_variable_name + "_DATA_" + m_ABCD16_region_names[i]);
         delete histosData[i];
      }
      histosData.clear();
   }

   if (isLargeRJERSys)
   {
      m_hist_bkg_Multijet_sys = Calculate_ABCD16_estimate(m_hist_data_16regions_sys, m_hist_MC_16regions_sys);
   }
   else
   {
      m_hist_bkg_Multijet_sys = Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, m_hist_MC_16regions_sys);
   }

   m_hist_bkg_all_sys = (TH1D *)m_hist_bkg_MC_sys->Clone();
   m_hist_bkg_all_sys->Add(m_hist_bkg_Multijet_sys);
   m_hist_pseudodata_sys = (TH1D *)m_hist_bkg_all_sys->Clone();
   m_hist_pseudodata_sys->Add(m_hist_signal_sys);
}

//----------------------------------------------------------------------

void HistStore::delete_one_sys_histos()
{
   if (m_hist_data_sys)
   {
      delete m_hist_data_sys;
      m_hist_data_sys = nullptr;
   }
   m_hist_data_sys = nullptr;
   delete m_hist_signal_sys;
   delete m_hist_bkg_MC_sys;
   delete m_hist_bkg_Multijet_sys;
   for (TH1D *h : m_hist_MC_16regions_sys)
      delete h;
   for (TH1D *h : m_hist_data_16regions_sys)
      delete h;
   m_hist_MC_16regions_sys.clear();
   m_hist_data_16regions_sys.clear();
   delete m_hist_bkg_all_sys;
   delete m_hist_pseudodata_sys;
   delete m_hist_migration_sys;
   delete m_hist_EffOfRecoLevelCutsNominator_sys;
   delete m_hist_EffOfTruthLevelCutsNominator_sys;
   delete m_hist_signal_truth_sys;
}

//----------------------------------------------------------------------

void HistStore::delete_one_sys_histosND()
{
   delete_one_sys_histos();
}

//----------------------------------------------------------------------

TH1D *HistStore::Calculate_ABCD16_estimate(const vector<TH1D *> &data, const vector<TH1D *> &MC, bool calculate_errors)
{
   TH1D *result = (TH1D *)data[0]->Clone();
   functions::Calculate_ABCD16_estimate(data, MC, result, calculate_errors);
   return result;
}

//----------------------------------------------------------------------

TH1DBootstrap *HistStore::Calculate_ABCD16_estimate(const vector<TH1DBootstrap *> &bootstrapData, const vector<TH1DBootstrap *> &bootstrapMC, bool calculate_errors)
{
   TH1DBootstrap *bootstrapResult = new TH1DBootstrap(*bootstrapData[0]);
   TH1D *singleEstimate = (TH1D *)bootstrapData[0]->GetNominal()->Clone();
   vector<TH1D *> data(15), MC(15);
   for (int i = 0; i < 15; i++)
   {
      data[i] = (TH1D *)bootstrapData[i]->GetNominal();
      MC[i] = (TH1D *)bootstrapMC[i]->GetNominal();
   }
   functions::Calculate_ABCD16_estimate(data, MC, singleEstimate, calculate_errors);
   bootstrapResult->SetNominal(*singleEstimate);

   const int nReplica = bootstrapData[0]->GetNReplica();
   for (int iReplica = 0; iReplica < nReplica; iReplica++)
   {
      for (int i = 0; i < 15; i++)
      {
         data[i] = (TH1D *)bootstrapData[i]->GetReplica(iReplica);
         MC[i] = (TH1D *)bootstrapMC[i]->GetReplica(iReplica);
      }
      functions::Calculate_ABCD16_estimate(data, MC, singleEstimate, calculate_errors);
      bootstrapResult->SetReplica(iReplica, *singleEstimate);
   }

   return bootstrapResult;
}

//----------------------------------------------------------------------

void HistStore::prepare_binning()
{
   TString level_short = m_level;
   level_short.ReplaceAll("Level", "");
   if (m_debug > 0)
      cout << "HistStore: Preparing binning for " << m_variable_name << " at " << level_short << " level." << endl;
   if (m_debug > 0)
      cout << m_gap + "All histograms will be rebinned during loading process." << endl;
   m_binning = functions::MakeVectorDouble(m_config_binning->GetValue(m_variable_name + "_" + level_short + "_reco", ""));
   if (m_binning.size() == 0)
      throw TtbarDiffCrossSection_exception((TString) "Error: Problem with loading reco binning from config file.");
   m_binning_truth = functions::MakeVectorDouble(m_config_binning->GetValue(m_variable_name + "_" + level_short + "_truth", ""));
   if (m_binning_truth.size() == 0)
      throw TtbarDiffCrossSection_exception((TString) "Error:Problem with loading truth binning from config file.");

   m_nbins_reco = m_binning.size() - 1;
   m_nbins_truth = m_binning_truth.size() - 1;
   if (m_debug > 1)
   {
      cout << m_gap + "Binning loaded properly." << endl;
      cout << m_gap + "Binning reco: ";
      for (int i = 0; i <= m_nbins_reco; i++)
         cout << m_binning[i] << " ";
      cout << endl
           << m_gap + "Binning truth: ";
      for (int i = 0; i <= m_nbins_truth; i++)
         cout << m_binning_truth[i] << " ";
      cout << endl;
   }
}

//----------------------------------------------------------------------

void HistStore::prepare_binningND()
{

   std::cout << m_config_binning_name << std::endl;
   m_histogramConverterND = std::make_shared<HistogramNDto1DConverter>(m_config_binning_name.Data(), m_debug);
   m_histogramConverterNDTruth = std::make_shared<HistogramNDto1DConverter>(m_config_binning_name.Data(), m_debug);

   TString levelShort = m_level;
   levelShort.ReplaceAll("Level", "");
   m_histogramConverterND->initializeOptBinning((m_variable_name + "_" + levelShort + "_reco").Data());
   m_histogramConverterND->initialize1DBinning();
   m_histogramConverterNDTruth->initializeOptBinning((m_variable_name + "_" + levelShort + "_truth").Data());
   m_histogramConverterNDTruth->initialize1DBinning();

   if (m_debug > 2)
   {
      m_histogramConverterND->print();
      m_histogramConverterNDTruth->print();
   }
}

//----------------------------------------------------------------------

void HistStore::load_bootstrapped_histos()
{
   if (m_debug > 0)
      cout << "HistStore: Loading Bootstrapped histograms." << endl;
   const int ntfiles = m_fileStore->get_n_input_tfiles();
   const TString chain_name = m_nominalTreeName;
   const TString foldername = "unfolding";
   const TString name = m_variable_name + "_RecoLevel_boots";

   m_bootstrap_hist_data = m_fileStore->getBootstrapHistogram(0, chain_name, foldername, name, false);
   m_bootstrap_hist_signal = m_fileStore->getBootstrapHistogram(1, chain_name, foldername, name);
   m_bootstrap_hist_signal->Scale(m_ttbarSF);
   m_bootstrap_bkg_MC = m_fileStore->getBootstrapHistogram(2, chain_name, foldername, name);
   m_bootstrap_bkg_MC->Scale(m_ttbarSF);
   for (int ifile = 3; ifile < ntfiles; ifile++)
      m_bootstrap_bkg_MC->Add(m_fileStore->getBootstrapHistogram(ifile, chain_name, foldername, name));

   m_fileStore->get_16regions_BootstrapHistos(m_bootstrap_data_16regions, m_bootstrap_MC_16regions, chain_name, foldername, name);

   m_bootstrap_bkg_Multijet = Calculate_ABCD16_estimate(m_bootstrap_data_16regions, m_bootstrap_MC_16regions, false);
   m_bootstrap_bkg_all = new TH1DBootstrap(*m_bootstrap_bkg_Multijet);
   m_bootstrap_bkg_all->Add(m_bootstrap_bkg_MC);
   m_bootstrap_pseudodata = new TH1DBootstrap(*m_bootstrap_bkg_all);
   m_bootstrap_pseudodata->Add(m_bootstrap_hist_signal);
}

//----------------------------------------------------------------------

void HistStore::WriteBootstrapHistos(TFile *f)
{
   if (m_debug > 0)
      cout << "HistStore: Writing bootstrap histos" << endl;
   f->mkdir(m_nominalTreeName);
   f->cd(m_nominalTreeName);
   m_bootstrap_hist_data->Write(m_variable_name + "_boots_data");
   m_bootstrap_hist_signal->Write(m_variable_name + "_boots_signal");
   m_bootstrap_bkg_MC->Write(m_variable_name + "_boots_bkg_MC");
   m_bootstrap_bkg_Multijet->Write(m_variable_name + "_boots_bkg_Multijet");
   m_bootstrap_bkg_all->Write(m_variable_name + "_boots_bkg_all");
   m_bootstrap_pseudodata->Write(m_variable_name + "_boots_pseudodata");
   f->mkdir(m_nominalTreeName + "/SidebandRegions");
   f->cd(m_nominalTreeName + "/SidebandRegions");
   for (int iregion = 0; iregion < 15; iregion++)
      m_bootstrap_data_16regions[iregion]->Write(m_variable_name + "_data_" + m_ABCD16_region_names[iregion]);
   for (int iregion = 0; iregion < 15; iregion++)
      m_bootstrap_MC_16regions[iregion]->Write(m_variable_name + "_MC_" + m_ABCD16_region_names[iregion]);
}

//----------------------------------------------------------------------

void HistStore::WriteNominalHistos(TFile *f)
{
   // Writing nominal histos
   // TODO: Make this function shorter
   f->mkdir(m_nominalTreeName);
   f->cd(m_nominalTreeName);
   m_hist_data->Write(m_variable_name + "_data");
   m_hist_signal_nominal->Write(m_variable_name + "_signal_reco");
   m_hist_bkg_MC_nominal->Write(m_variable_name + "_bkg_MC");
   m_hist_bkg_Multijet_nominal->Write(m_variable_name + "_bkg_Multijet");
   m_hist_bkg_all_nominal->Write(m_variable_name + "_bkg_all");
   m_hist_pseudodata_nominal->Write(m_variable_name + "_pseudodata");
   m_migration_nominal->Write(m_variable_name + "_migration");
   if (m_useResolutionHistos)
      m_resolution_nominal->Write(m_variable_name + "_resolution");
   m_EffOfRecoLevelCutsNominator_nominal->Write(m_variable_name + "_EffOfRecoLevelCutsNominator");
   m_EffOfTruthLevelCutsNominator_nominal->Write(m_variable_name + "_EffOfTruthLevelCutsNominator");
   m_matchingEfficiencyRecoTruth_nominal->Write(m_variable_name + "_matchingEfficiencyRecoTruth");
   m_hist_signal_truth_nominal->Write(m_variable_name + "_signal_truth");
   TH1D *h_data_minus_background = (TH1D *)m_hist_data->Clone();
   h_data_minus_background->Add(m_hist_bkg_all_nominal, -1);
   RooUnfoldResponse *response = new RooUnfoldResponse(0, 0, m_migration_nominal);
   TH1D *h_unfolded = functions::UnfoldBayes(h_data_minus_background, m_hist_signal_nominal, m_EffOfTruthLevelCutsNominator_nominal, m_hist_signal_truth_nominal, m_EffOfRecoLevelCutsNominator_nominal, response, 4, 0);
   // TH1D* h_unfolded = functions::UnfoldInvert(h_data_minus_background,m_hist_signal_nominal,m_EffOfTruthLevelCutsNominator_nominal,m_hist_signal_truth_nominal,m_EffOfRecoLevelCutsNominator_nominal,response,0);
   h_unfolded->Write(m_variable_name + "_unfolded_data_minus_bkg");
   delete h_unfolded;
   delete h_data_minus_background;
   delete response;

   f->mkdir(m_nominalTreeName + "/SidebandRegions");
   f->cd(m_nominalTreeName + "/SidebandRegions");
   for (int iregion = 0; iregion < 15; iregion++)
      m_hist_data_16regions_nominal[iregion]->Write(m_variable_name + "_data_" + m_ABCD16_region_names[iregion]);
   for (int iregion = 0; iregion < 15; iregion++)
      m_hist_MC_16regions_nominal[iregion]->Write(m_variable_name + "_MC_" + m_ABCD16_region_names[iregion]);
   f->mkdir(m_nominalTreeName + "/MCBackgrounds");
   f->cd(m_nominalTreeName + "/MCBackgrounds");

   // !!!!!!!!!!! HARDCODED!!! NEEDED TO MAKE IT CONFIGURABLE!!!!
   if (m_hist_MC_backgrounds_nominal.size() > 0)
      m_hist_MC_backgrounds_nominal[0]->Write(m_variable_name + "_ttbar_nonallhad");
   if (m_hist_MC_backgrounds_nominal.size() > 1)
      m_hist_MC_backgrounds_nominal[1]->Write(m_variable_name + "_Wt_singletop");
   // if(m_hist_MC_backgrounds_nominal.size()>2) m_hist_MC_backgrounds_nominal[2]->Write(m_variable_name + "_tChannel_singletop");
   if (m_hist_MC_backgrounds_nominal.size() > 2)
      m_hist_MC_backgrounds_nominal[2]->Write(m_variable_name + "_ttHWZ");

   if (m_ttbarConfig->fillClosureTestHistos())
   {
      f->mkdir(m_nominalTreeName + "/ClosureTest");
      f->cd(m_nominalTreeName + "/ClosureTest");
      m_hist_signal_nominal_half1->Write(m_variable_name + "_reco1");
      m_hist_signal_nominal_half2->Write(m_variable_name + "_reco2");
      m_hist_signal_truth_nominal_half1->Write(m_variable_name + "_truth1");
      m_hist_signal_truth_nominal_half2->Write(m_variable_name + "_truth2");
      m_migration_nominal_half1->Write(m_variable_name + "_migration1");
      m_migration_nominal_half2->Write(m_variable_name + "_migration2");
   }
}

//----------------------------------------------------------------------

void HistStore::WriteNominalHistosND(TFile *f)
{
   // WriteNominalHistos(f);

   // Writing nominal histos
   // TODO: Make this function shorter
   f->mkdir(m_nominalTreeName);
   f->cd(m_nominalTreeName);
   m_hist_data->Write(m_variable_name + "_data");
   m_hist_signal_nominal->Write(m_variable_name + "_signal_reco");
   m_hist_bkg_MC_nominal->Write(m_variable_name + "_bkg_MC");
   m_hist_bkg_Multijet_nominal->Write(m_variable_name + "_bkg_Multijet");
   m_hist_bkg_all_nominal->Write(m_variable_name + "_bkg_all");
   m_hist_pseudodata_nominal->Write(m_variable_name + "_pseudodata");
   m_migration_nominal->Write(m_variable_name + "_migration");
   // if (m_useResolutionHistos) m_resolution_nominal->Write(m_variable_name + "_resolution");
   m_EffOfRecoLevelCutsNominator_nominal->Write(m_variable_name + "_EffOfRecoLevelCutsNominator");
   m_EffOfTruthLevelCutsNominator_nominal->Write(m_variable_name + "_EffOfTruthLevelCutsNominator");
   m_matchingEfficiencyRecoTruth_nominal->Write(m_variable_name + "_matchingEfficiencyRecoTruth");
   m_hist_signal_truth_nominal->Write(m_variable_name + "_signal_truth");
   TH1D *h_data_minus_background = (TH1D *)m_hist_data->Clone();
   h_data_minus_background->Add(m_hist_bkg_all_nominal, -1);
   RooUnfoldResponse *response = new RooUnfoldResponse(0, 0, m_migration_nominal);
   TH1D *h_unfolded = functions::UnfoldBayes(h_data_minus_background, m_hist_signal_nominal, m_EffOfTruthLevelCutsNominator_nominal, m_hist_signal_truth_nominal, m_EffOfRecoLevelCutsNominator_nominal, response, 4, 0);
   // TH1D* h_unfolded = functions::UnfoldInvert(h_data_minus_background,m_hist_signal_nominal,m_EffOfTruthLevelCutsNominator_nominal,m_hist_signal_truth_nominal,m_EffOfRecoLevelCutsNominator_nominal,response,0);
   h_unfolded->Write(m_variable_name + "_unfolded_data_minus_bkg");
   delete h_unfolded;
   delete h_data_minus_background;
   delete response;

   f->mkdir(m_nominalTreeName + "/SidebandRegions");
   f->cd(m_nominalTreeName + "/SidebandRegions");
   for (int iregion = 0; iregion < 15; iregion++)
      m_hist_data_16regions_nominal[iregion]->Write(m_variable_name + "_data_" + m_ABCD16_region_names[iregion]);
   for (int iregion = 0; iregion < 15; iregion++)
      m_hist_MC_16regions_nominal[iregion]->Write(m_variable_name + "_MC_" + m_ABCD16_region_names[iregion]);
   f->mkdir(m_nominalTreeName + "/MCBackgrounds");
   f->cd(m_nominalTreeName + "/MCBackgrounds");

   // !!!!!!!!!!! HARDCODED!!! NEEDED TO MAKE IT CONFIGURABLE!!!!
   if (m_hist_MC_backgrounds_nominal.size() > 0)
      m_hist_MC_backgrounds_nominal[0]->Write(m_variable_name + "_ttbar_nonallhad");
   if (m_hist_MC_backgrounds_nominal.size() > 1)
      m_hist_MC_backgrounds_nominal[1]->Write(m_variable_name + "_Wt_singletop");
   // if(m_hist_MC_backgrounds_nominal.size()>2) m_hist_MC_backgrounds_nominal[2]->Write(m_variable_name + "tChannel_singletop");
   if (m_hist_MC_backgrounds_nominal.size() > 3)
      m_hist_MC_backgrounds_nominal[2]->Write(m_variable_name + "_ttHWZ");

   if (m_ttbarConfig->fillClosureTestHistos())
   {
      f->mkdir(m_nominalTreeName + "/ClosureTest");
      f->cd(m_nominalTreeName + "/ClosureTest");
      m_hist_signal_nominal_half1->Write(m_variable_name + "_reco1");
      m_hist_signal_nominal_half2->Write(m_variable_name + "_reco2");
      m_hist_signal_truth_nominal_half1->Write(m_variable_name + "_truth1");
      m_hist_signal_truth_nominal_half2->Write(m_variable_name + "_truth2");
      m_migration_nominal_half1->Write(m_variable_name + "_migration1");
      m_migration_nominal_half2->Write(m_variable_name + "_migration2");
   }
}

//----------------------------------------------------------------------

void HistStore::WriteAllSysHistos(TFile *f, const TString &option)
{

   if (m_debug > 0)
      cout << "HistStore: Loading and Writing histos from systematic branches" << endl;
   m_fileStore->prepare_list_of_systematics();
   m_fileStore->get_list_of_systematics(m_sys_chain_names_all, m_sys_chain_names_paired, m_sys_chain_names_single, m_sys_chain_names_pdf);
   m_fileStore->make_list_of_reweighted_chains();
   m_reweighted_chain_names = m_fileStore->get_list_of_reweighted_chains();
   m_nsys_all = m_sys_chain_names_all.size();
   m_nsys_pairs = m_sys_chain_names_paired.size();
   m_nsys_single = m_sys_chain_names_single.size();
   m_nsys_pdf = m_sys_chain_names_pdf.size();

   for (int isys = 0; isys < m_nsys_all; isys++)
   {
      if ((isys + 1) % 20 == 0)
      {
         if (m_debug > -1)
            cout << m_gap << isys + 1 << "/" << m_nsys_all << endl;
         m_fileStore->reload_input_tfiles();
      }
      if (m_debug > 0)
         cout << "Proccessing systematic branch: " << m_sys_chain_names_all[isys] << std::endl;

      // Skipping correlated variations - standard procedure
      // if(m_sys_chain_names_all[isys].Contains("JET_EffectiveNP_R10_Mixed1") && m_sys_chain_names_all[isys].Contains("JET_JER_dijet_R10_jesEffNP1")) continue;

      // Skipping PseudoData variations
      if (m_sys_chain_names_all[isys].Contains("JET_JERUnc") && m_sys_chain_names_all[isys].Contains("PseudoData"))
         continue;

      // Skipping uncorrelated variations - alternative procedure
      if ((m_sys_chain_names_all[isys] == "JET_EffectiveNP_R10_Mixed1__1up") ||
          (m_sys_chain_names_all[isys] == "JET_EffectiveNP_R10_Mixed1__1down") ||
          (m_sys_chain_names_all[isys] == "JET_JER_dijet_R10_jesEffNP1__1up") ||
          (m_sys_chain_names_all[isys] == "JET_JER_dijet_R10_jesEffNP1__1down"))
         continue;

      if (option == "1D")
      {
         load_one_sys_histos(m_sys_chain_names_all[isys]);
         write_one_sys_histos(f, m_sys_chain_names_all[isys]);
         delete_one_sys_histos();
      }
      else if (option == "ND")
      {
         load_one_sys_histosND(m_sys_chain_names_all[isys]);
         write_one_sys_histosND(f, m_sys_chain_names_all[isys]);
         delete_one_sys_histosND();
      }
      if (m_debug > 0)
         cout << "Proccessing  DONE systematic branch: " << m_sys_chain_names_all[isys] << std::endl;
   }
}

//----------------------------------------------------------------------

void HistStore::write_one_sys_histos(TFile *f, const TString &chain_name)
{

   f->cd();

   TString chain_name_final = chain_name;
   // chain_name_final.ReplaceAll("JET_EffectiveNP_R10_Mixed1__1up_and_JET_JER_dijet_R10_jesEffNP1","JET_EffectiveNP_R10_Mixed1_and_JET_JER_dijet_R10_jesEffNP1");
   // chain_name_final.ReplaceAll("JET_EffectiveNP_R10_Mixed1__1down_and_JET_JER_dijet_R10_jesEffNP1","JET_EffectiveNP_R10_Mixed1_and_JET_JER_dijet_R10_jesEffNP1");

   f->mkdir(chain_name_final);
   f->cd(chain_name_final);

   if (functions::isLargeRJERSys(chain_name))
      m_hist_data_sys->Write(m_variable_name + "_data");
   else
      m_hist_data->Write(m_variable_name + "_data");

   m_hist_signal_sys->Write(m_variable_name + "_signal_reco");
   m_hist_bkg_MC_sys->Write(m_variable_name + "_bkg_MC");
   m_hist_bkg_Multijet_sys->Write(m_variable_name + "_bkg_Multijet");
   m_hist_bkg_all_sys->Write(m_variable_name + "_bkg_all");
   m_hist_pseudodata_sys->Write(m_variable_name + "_pseudodata");
   m_hist_migration_sys->Write(m_variable_name + "_migration");
   m_hist_EffOfRecoLevelCutsNominator_sys->Write(m_variable_name + "_EffOfRecoLevelCutsNominator");
   m_hist_EffOfTruthLevelCutsNominator_sys->Write(m_variable_name + "_EffOfTruthLevelCutsNominator");
   m_hist_signal_truth_sys->Write(m_variable_name + "_signal_truth");
   f->mkdir(chain_name_final + "/SidebandRegions");
   f->cd(chain_name_final + "/SidebandRegions");
   if (functions::isLargeRJERSys(chain_name))
   {
      for (int iregion = 0; iregion < 15; iregion++)
         m_hist_data_16regions_sys[iregion]->Write(m_variable_name + "_data_" + m_ABCD16_region_names[iregion]);
   }
   else
   {
      for (int iregion = 0; iregion < 15; iregion++)
         m_hist_data_16regions_nominal[iregion]->Write(m_variable_name + "_data_" + m_ABCD16_region_names[iregion]);
   }

   for (int iregion = 0; iregion < 15; iregion++)
   {
      m_hist_MC_16regions_sys[iregion]->Write(m_variable_name + "_MC_" + m_ABCD16_region_names[iregion]);
   }

   // JH: For systematic unc. calculation for backgrounds
   // f->mkdir(chain_name+"/MCBackgrounds");
   // f->cd(chain_name+"/MCBackgrounds");

   //// !!!!!!!!!!! HARDCODED!!! NEEDED TO MAKE IT CONFIGURABLE!!!!
   // if(m_hist_MC_backgrounds_sys.size()>0) m_hist_MC_backgrounds_sys[0]->Write(m_variable_name + "_ttbar_nonallhad");
   // if(m_hist_MC_backgrounds_sys.size()>1) m_hist_MC_backgrounds_sys[1]->Write(m_variable_name + "_Wt_singletop");
   // if(m_hist_MC_backgrounds_sys.size()>2) m_hist_MC_backgrounds_sys[2]->Write(m_variable_name + "_tChannel_singletop");
   // if(m_hist_MC_backgrounds_sys.size()>3) m_hist_MC_backgrounds_sys[3]->Write(m_variable_name + "_ttHWZ");
}

//----------------------------------------------------------------------

void HistStore::write_one_sys_histosND(TFile *f, const TString &chain_name)
{
   write_one_sys_histos(f, chain_name);
}

//----------------------------------------------------------------------

void HistStore::WriteSignalModelingHistos(TFile *f, const TString &histname, const TString &sysName, const TString &fileName, const TString &chainName)
{
   if (m_debug > 0)
      cout << "HistStore: Writing signal modeling histograms for " << fileName << " systematics." << endl;
   if (!m_nominalHistosLoaded)
      throw TtbarDiffCrossSection_exception("Error in WriteSignalModelingHistos: Nominal histos are not loaded!");

   const TString foldername = "unfolding";
   const TString name = histname + "_RecoLevel";

   if (m_debug > 1)
      cout << "sysName: " << sysName << " fileName: " << fileName << " chainName: " << chainName << endl;
   if (!m_fileStore->get_special_tfile(fileName))
      throw TtbarDiffCrossSection_exception("Error in WriteSignalModelingHistos: File with name " + fileName + " is not loaded!");

   vector<TH1D *> histMC16regions;

   m_fileStore->get_16regions_special_histos_MC_only(fileName, histMC16regions, chainName, foldername, name);
   if (m_doRebin)
   {
      const int nRegions = histMC16regions.size();
      for (int iRegion = 0; iRegion < nRegions; iRegion++)
      {
         histMC16regions[iRegion] = (TH1D *)histMC16regions[iRegion]->Rebin(m_nbins_reco, histMC16regions[iRegion]->GetName(), &m_binning[0]);
      }
   }

   auto hist_signal = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, name));
   hist_signal->Scale(m_ttbarSF);
   if (m_doRebin)
      hist_signal = std::unique_ptr<TH1D>((TH1D *)hist_signal->Rebin(m_nbins_reco, (TString)hist_signal->GetName(), &m_binning[0]));

   if (m_debug > 1)
      cout << m_gap << hist_signal->GetName() << endl;

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";

   auto migration = std::unique_ptr<TH2D>((TH2D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix2 + m_variable_name + "_migration"));
   migration->Scale(m_ttbarSF);

   // JH
   std::unique_ptr<TH2D> resolution;
   if (m_useResolutionHistos)
   {
      resolution = std::unique_ptr<TH2D>((TH2D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix2 + m_variable_name + "_resolution"));
      resolution->Scale(m_ttbarSF);
   }

   if (m_doRebin)
      migration = std::unique_ptr<TH2D>(functions::Rebin2D(migration.get(), m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], ""));

   auto EffOfRecoLevelCutsNominator = std::unique_ptr<TH1D>(migration->ProjectionY("nominator_for_efficiency", 1, migration->GetNbinsX(), "e"));
   auto EffOfTruthLevelCutsNominator = std::unique_ptr<TH1D>(migration->ProjectionX("nominator_for_acceptance", 1, migration->GetNbinsY(), "e"));

   auto matchingEfficiencyRecoTruthDenominator_nominal = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix2 + m_variable_name + "_matchingEfficiencyDenominator"));
   if (m_doRebin)
      matchingEfficiencyRecoTruthDenominator_nominal = std::unique_ptr<TH1D>((TH1D *)matchingEfficiencyRecoTruthDenominator_nominal->Rebin(m_nbins_reco, matchingEfficiencyRecoTruthDenominator_nominal->GetName(), &m_binning[0]));

   auto matchingEfficiencyRecoTruth = std::unique_ptr<TH1D>((TH1D *)EffOfTruthLevelCutsNominator->Clone());
   matchingEfficiencyRecoTruth->Divide(matchingEfficiencyRecoTruth.get(), matchingEfficiencyRecoTruthDenominator_nominal.get(), 1, 1, "B");

   auto hist_signal_truth = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level));
   hist_signal_truth->Scale(m_ttbarSF);
   if (m_doRebin)
      hist_signal_truth = std::unique_ptr<TH1D>((TH1D *)hist_signal_truth->Rebin(m_nbins_truth, (TString)hist_signal_truth->GetName(), &m_binning_truth[0]));

   if (m_debug > 1)
      cout << m_gap + "Calculating nominal ABCD16 estimate" << endl;

   auto bkg_Multijet = std::unique_ptr<TH1D>(Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, histMC16regions));
   auto bkg_all = std::unique_ptr<TH1D>((TH1D *)bkg_Multijet->Clone());
   bkg_all->Add(m_hist_bkg_MC_nominal);
   auto pseudodata = std::unique_ptr<TH1D>((TH1D *)bkg_all->Clone());
   pseudodata->Add(hist_signal.get());

   f->cd();
   f->mkdir(sysName);
   f->cd(sysName);
   hist_signal->Write(m_variable_name + "_signal_reco");
   m_hist_bkg_MC_nominal->Write(m_variable_name + "_bkg_MC");
   bkg_Multijet->Write(m_variable_name + "_bkg_Multijet");
   bkg_all->Write(m_variable_name + "_bkg_all");
   pseudodata->Write(m_variable_name + "_pseudodata");
   migration->Write(m_variable_name + "_migration");
   // JH
   if (m_useResolutionHistos)
      resolution->Write(m_variable_name + "_resolution");
   EffOfRecoLevelCutsNominator->Write(m_variable_name + "_EffOfRecoLevelCutsNominator");
   EffOfTruthLevelCutsNominator->Write(m_variable_name + "_EffOfTruthLevelCutsNominator");
   matchingEfficiencyRecoTruth->Write(m_variable_name + "_matchingEfficiencyRecoTruth");
   hist_signal_truth->Write(m_variable_name + "_signal_truth");

   auto response = std::unique_ptr<RooUnfoldResponse>(new RooUnfoldResponse(0, 0, m_migration_nominal));
   auto h_reco_shifted = std::unique_ptr<TH1D>((TH1D *)pseudodata->Clone());
   h_reco_shifted->Add(m_hist_bkg_all_nominal, -1);
   auto h_unfolded = std::unique_ptr<TH1D>(functions::UnfoldBayes(h_reco_shifted.get(), m_hist_signal_nominal, m_EffOfTruthLevelCutsNominator_nominal, m_hist_signal_truth_nominal, m_EffOfRecoLevelCutsNominator_nominal, response.get(), 4, 0));

   h_unfolded->Write(m_variable_name + "_unfolded_signal");
   f->mkdir(sysName + "/SidebandRegions");
   f->cd(sysName + "/SidebandRegions");
   for (int iregion = 0; iregion < 15; iregion++)
      histMC16regions[iregion]->Write(m_variable_name + "_MC_" + m_ABCD16_region_names[iregion]);
   for (int iregion = 0; iregion < 15; iregion++)
      delete histMC16regions[iregion];
}

//----------------------------------------------------------------------

void HistStore::WriteSignalModelingHistosND(TFile *f, const TString &histname, const TString &sysName, const TString &fileName, const TString &chainName)
{
   if (m_debug > 0)
      cout << "HistStore: Writing ND signal modeling histograms for " << fileName << " systematics." << endl;
   if (!m_nominalHistosLoaded)
      throw TtbarDiffCrossSection_exception("Error in WriteSignalModelingHistos2D: Nominal histos are not loaded!");

   const TString foldername = "unfolding";
   const TString name = histname + "_RecoLevel";

   if (m_debug > 1)
      cout << "sysName: " << sysName << " fileName: " << fileName << " chainName: " << chainName << endl;
   if (!m_fileStore->get_special_tfile(fileName))
      throw TtbarDiffCrossSection_exception("Error in WriteSignalModelingHistos: File with name " + fileName + " is not loaded!");

   vector<TH1D *> histMC16regions(15);
   vector<THnSparseD *> histos;

   m_fileStore->get_16regions_special_histos_MC_only(fileName, histos, chainName, foldername, name);
   for (int i = 0; i < 15; i++)
   {
      histMC16regions[i] = m_histogramConverterND->makeConcatenatedHistogram(histos[i], m_variable_name + "_MC_" + m_ABCD16_region_names[i]);
      delete histos[i];
   }
   histos.clear();

   auto helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, name));
   auto hist_signal = std::unique_ptr<TH1D>(m_histogramConverterND->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_signal_reco"));
   hist_signal->Scale(m_ttbarSF);

   // cout << m_gap << hist_signal->GetName() << endl;

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";

   helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getObject(m_fileStore->get_special_tfile(fileName), chainName, foldername, prefix1 + prefix2 + m_variable_name + "_migration"));
   helphist->Scale(m_ttbarSF);
   auto migration = std::unique_ptr<TH2D>(m_histogramConverterND->make2DConcatenatedHistogram(helphist.get(), m_histogramConverterNDTruth.get(), m_variable_name + "_migration"));

   auto EffOfRecoLevelCutsNominator = std::unique_ptr<TH1D>(migration->ProjectionY("nominator_for_efficiency", 1, migration->GetNbinsX(), "e"));
   auto EffOfTruthLevelCutsNominator = std::unique_ptr<TH1D>(migration->ProjectionX("nominator_for_acceptance", 1, migration->GetNbinsY(), "e"));

   helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix2 + m_variable_name + "_matchingEfficiencyDenominator"));
   auto matchingEfficiencyRecoTruth = std::unique_ptr<TH1D>(m_histogramConverterND->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_matchingEfficiency"));
   matchingEfficiencyRecoTruth->Divide(EffOfTruthLevelCutsNominator.get(), matchingEfficiencyRecoTruth.get(), 1, 1, "B");

   helphist = std::unique_ptr<THnSparseD>((THnSparseD *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level));
   auto hist_signal_truth = std::unique_ptr<TH1D>(m_histogramConverterNDTruth->makeConcatenatedHistogram(helphist.get(), m_variable_name + "_signal_truth"));
   hist_signal_truth->Scale(m_ttbarSF);

   // cout << m_gap+"Calculating nominal ABCD16 estimate" << endl;

   auto bkg_Multijet = std::unique_ptr<TH1D>(Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, histMC16regions));
   auto bkg_all = std::unique_ptr<TH1D>((TH1D *)bkg_Multijet->Clone());
   bkg_all->Add(m_hist_bkg_MC_nominal);
   auto pseudodata = std::unique_ptr<TH1D>((TH1D *)bkg_all->Clone());
   pseudodata->Add(hist_signal.get());

   f->cd();
   f->mkdir(sysName);
   f->cd(sysName);
   hist_signal->Write(m_variable_name + "_signal_reco");
   m_hist_bkg_MC_nominal->Write(m_variable_name + "_bkg_MC");
   bkg_Multijet->Write(m_variable_name + "_bkg_Multijet");
   bkg_all->Write(m_variable_name + "_bkg_all");
   pseudodata->Write(m_variable_name + "_pseudodata");
   migration->Write(m_variable_name + "_migration");
   EffOfRecoLevelCutsNominator->Write(m_variable_name + "_EffOfRecoLevelCutsNominator");
   EffOfTruthLevelCutsNominator->Write(m_variable_name + "_EffOfTruthLevelCutsNominator");
   hist_signal_truth->Write(m_variable_name + "_signal_truth");

   auto response = std::unique_ptr<RooUnfoldResponse>(new RooUnfoldResponse(0, 0, m_migration_nominal));
   auto h_reco_shifted = std::unique_ptr<TH1D>((TH1D *)pseudodata->Clone());
   h_reco_shifted->Add(m_hist_bkg_all_nominal, -1);

   auto h_unfolded = std::unique_ptr<TH1D>(functions::UnfoldBayes(h_reco_shifted.get(), m_hist_signal_nominal, m_EffOfTruthLevelCutsNominator_nominal, m_hist_signal_truth_nominal, m_EffOfRecoLevelCutsNominator_nominal, response.get(), 4, 0));

   h_unfolded->Write(m_variable_name + "_unfolded_signal");
   f->mkdir(sysName + "/SidebandRegions");
   f->cd(sysName + "/SidebandRegions");
   for (int iregion = 0; iregion < 15; iregion++)
      histMC16regions[iregion]->Write(m_variable_name + "_MC_" + m_ABCD16_region_names[iregion]);
   for (int iregion = 0; iregion < 15; iregion++)
      delete histMC16regions[iregion];
}

//----------------------------------------------------------------------

void HistStore::WriteEFTHistos(TFile *fOutput)
{

   // std::vector<TString> fileNames {
   //"eft_507564",
   //"eft_507565",
   //"eft_507567",
   //"eft_507566"
   //};

   std::vector<TString> fileNames{
       "eft_merged"};

   const TString prefix1 = m_level + "CutsPassed_";
   const TString prefix2 = "RecoLevelCutsPassed_";
   const TString prefix3 = "noRecoLevelCuts_";
   const TString foldername = "unfolding";

   const double scaleFactor = 1.;
   // const double scaleFactor = m_ttbarConfig->lumi_dataAllYears()/m_ttbarConfig->lumi_data1516();
   // const double scaleFactor = m_ttbarConfig->factor_to_ttbar_cross_section()/m_ttbarConfig->lumi_data1516();

   std::vector<TString> allNames;

   for (TString fileName : fileNames)
   {

      if (fileName.Contains("eft"))
         continue;
      TFile *fInput = m_fileStore->get_special_tfile(fileName);

      cout << "******: " << __LINE__ << " " << fileName << endl;
      TIter nextkey(fInput->GetListOfKeys());
      cout << "******: " << __LINE__ << endl;
      TKey *key = 0;

      while ((key = (TKey *)nextkey()))
      {

         TString chainName = key->GetName();

         if (chainName == m_nominalTreeName)
            continue;
         if (chainName == "SM" && (fileName != "eft_507564"))
            continue;

         if (std::find(std::begin(allNames), std::end(allNames), chainName) != std::end(allNames))
         {
            std::cout << "Warning: Duplicated eft coefficient: " << chainName << std::endl;
            continue;
         }

         allNames.push_back(chainName);

         auto hist_signal_truth = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level));

         hist_signal_truth->Scale(scaleFactor); // Scaling from lumi1516 to lumiAllYears.
         if (m_doRebin)
            hist_signal_truth = std::unique_ptr<TH1D>((TH1D *)hist_signal_truth->Rebin(m_nbins_truth, (TString)hist_signal_truth->GetName(), &m_binning_truth[0]));
         // functions::DivideByBinWidth(hist_signal_truth.get());

         fOutput->cd();
         fOutput->mkdir(chainName);
         fOutput->cd(chainName);

         auto hist_signal = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, m_variable_name + "_RecoLevel"));
         hist_signal->Scale(m_ttbarSF);
         if (m_doRebin)
            hist_signal = std::unique_ptr<TH1D>((TH1D *)hist_signal->Rebin(m_nbins_reco, (TString)hist_signal->GetName(), &m_binning[0]));

         if (m_debug > 1)
            cout << m_gap << hist_signal->GetName() << endl;

         const TString prefix1 = m_level + "CutsPassed_";
         const TString prefix2 = "RecoLevelCutsPassed_";
         const TString prefix3 = "noRecoLevelCuts_";

         auto migration = std::unique_ptr<TH2D>((TH2D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix2 + m_variable_name + "_migration"));
         migration->Scale(m_ttbarSF);

         if (m_doRebin)
            migration = std::unique_ptr<TH2D>(functions::Rebin2D(migration.get(), m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], ""));

         auto EffOfRecoLevelCutsNominator = std::unique_ptr<TH1D>(migration->ProjectionY("nominator_for_efficiency", 1, migration->GetNbinsX(), "e"));
         auto EffOfTruthLevelCutsNominator = std::unique_ptr<TH1D>(migration->ProjectionX("nominator_for_acceptance", 1, migration->GetNbinsY(), "e"));

         auto matchingEfficiencyRecoTruthDenominator_nominal = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, chainName, foldername, prefix1 + prefix2 + m_variable_name + "_matchingEfficiencyDenominator"));
         if (m_doRebin)
            matchingEfficiencyRecoTruthDenominator_nominal = std::unique_ptr<TH1D>((TH1D *)matchingEfficiencyRecoTruthDenominator_nominal->Rebin(m_nbins_reco, matchingEfficiencyRecoTruthDenominator_nominal->GetName(), &m_binning[0]));

         auto matchingEfficiencyRecoTruth = std::unique_ptr<TH1D>((TH1D *)EffOfTruthLevelCutsNominator->Clone());
         matchingEfficiencyRecoTruth->Divide(matchingEfficiencyRecoTruth.get(), matchingEfficiencyRecoTruthDenominator_nominal.get(), 1, 1, "B");

         if (m_debug > 1)
            cout << m_gap + "Calculating nominal ABCD16 estimate" << endl;

         auto bkg_Multijet = std::unique_ptr<TH1D>(Calculate_ABCD16_estimate(m_hist_data_16regions_nominal, m_hist_MC_16regions_nominal));
         auto bkg_all = std::unique_ptr<TH1D>((TH1D *)bkg_Multijet->Clone());
         bkg_all->Add(m_hist_bkg_MC_nominal);
         auto pseudodata = std::unique_ptr<TH1D>((TH1D *)bkg_all->Clone());
         pseudodata->Add(hist_signal.get());

         hist_signal_truth->Write(m_variable_name + "_signal_truth");

         hist_signal->Write(m_variable_name + "_signal_reco");
         m_hist_bkg_MC_nominal->Write(m_variable_name + "_bkg_MC");
         bkg_Multijet->Write(m_variable_name + "_bkg_Multijet");
         bkg_all->Write(m_variable_name + "_bkg_all");
         pseudodata->Write(m_variable_name + "_pseudodata");
         migration->Write(m_variable_name + "_migration");
         // JH
         EffOfRecoLevelCutsNominator->Write(m_variable_name + "_EffOfRecoLevelCutsNominator");
         EffOfTruthLevelCutsNominator->Write(m_variable_name + "_EffOfTruthLevelCutsNominator");
         matchingEfficiencyRecoTruth->Write(m_variable_name + "_matchingEfficiencyRecoTruth");

         auto migrationSM = std::unique_ptr<TH2D>((TH2D *)m_fileStore->getSpecialHistogram(fileName, "SM", foldername, prefix1 + prefix2 + m_variable_name + "_migration"));
         migrationSM->Scale(m_ttbarSF);

         if (m_doRebin)
            migrationSM = std::unique_ptr<TH2D>(functions::Rebin2D(migrationSM.get(), m_nbins_reco, &m_binning[0], m_nbins_truth, &m_binning_truth[0], ""));

         auto EffOfRecoLevelCutsNominatorSM = std::unique_ptr<TH1D>(migrationSM->ProjectionY("nominator_for_efficiencySM", 1, migrationSM->GetNbinsX(), "e"));
         auto EffOfTruthLevelCutsNominatorSM = std::unique_ptr<TH1D>(migrationSM->ProjectionX("nominator_for_acceptanceSM", 1, migrationSM->GetNbinsY(), "e"));

         hist_signal_truth = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, "SM", foldername, prefix1 + prefix3 + m_variable_name + "_" + m_level));

         hist_signal_truth->Scale(scaleFactor); // Scaling from lumi1516 to lumiAllYears.
         if (m_doRebin)
            hist_signal_truth = std::unique_ptr<TH1D>((TH1D *)hist_signal_truth->Rebin(m_nbins_truth, (TString)hist_signal_truth->GetName(), &m_binning_truth[0]));

         hist_signal = std::unique_ptr<TH1D>((TH1D *)m_fileStore->getSpecialHistogram(fileName, "SM", foldername, m_variable_name + "_RecoLevel"));
         hist_signal->Scale(m_ttbarSF);
         if (m_doRebin)
            hist_signal = std::unique_ptr<TH1D>((TH1D *)hist_signal->Rebin(m_nbins_reco, (TString)hist_signal->GetName(), &m_binning[0]));

         auto response = std::unique_ptr<RooUnfoldResponse>(new RooUnfoldResponse(0, 0, migrationSM.get()));
         auto h_reco_shifted = std::unique_ptr<TH1D>((TH1D *)pseudodata->Clone());
         h_reco_shifted->Add(m_hist_bkg_all_nominal, -1);
         auto h_unfolded = std::unique_ptr<TH1D>(functions::UnfoldBayes(h_reco_shifted.get(), hist_signal.get(), EffOfTruthLevelCutsNominatorSM.get(), hist_signal_truth.get(), EffOfRecoLevelCutsNominatorSM.get(), response.get(), 4, 0));

         h_unfolded->Write(m_variable_name + "_unfolded_signal");

         std::cout << "Chain name: " << chainName << " file name: " << fileName << std::endl;
      }
   }
}

//----------------------------------------------------------------------

void HistStore::PrepareStressTestInputs(TFile *f, const TString &option, const int imin, const int imax)
{
   if (m_debug > 0)
      cout << "HistStore::Preparing stress test inputs." << endl;
   for (int i = imin; i <= imax; i++)
   {
      string s = to_string(i);
      TString name = "Signal_reweighted";
      name += s.c_str();
      if (m_debug > 1)
         cout << name << " " << m_variable_name << endl;
      if (option == "ND")
      {
         WriteSignalModelingHistosND(f, name, m_variable_name, m_level);
      }
      else
      {
         WriteSignalModelingHistos(f, name, m_variable_name, m_level);
      }
   }
}
