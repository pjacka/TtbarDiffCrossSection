#include "TtbarDiffCrossSection/TTBarEventReaderParticleLevel.h"
#include "HelperFunctions/functions.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObject.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "Math/VectorUtil.h"

#include "TopEvent/EventTools.h"

ClassImp(TTBarEventReaderParticleLevel)

    typedef ReconstructedObjects::ReconstructedObject Object_t;

TTBarEventReaderParticleLevel::TTBarEventReaderParticleLevel(const std::string &name) : EventReaderToolBase(name)
{
}

StatusCode TTBarEventReaderParticleLevel::readJSONConfig(
    const nlohmann::json &json)
{

  try
  {
    m_treeReaderSettings = jsonFunctions::readValueJSON<nlohmann::json>(json, "treeReaderSettings");
  }
  catch (const std::runtime_error &e)
  {

    std::cout << "Error in TTBarEventReaderParticleLevel::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTBarEventReaderParticleLevel::initialize()
{
  top::check(AnaToolBase::initialize(), "TTBarEventReaderParticleLevel: Failed to initialize base tool!");

  m_treeReader = std::make_unique<ParticleLevelTreeTopCPToolkit>(m_treeReaderSettings);

  return StatusCode::SUCCESS;
}

StatusCode TTBarEventReaderParticleLevel::initChain(const std::unordered_map<std::string, std::string> &settings)
{
  m_treeReader->initChain(m_chain.get(), settings.at("sys_name"));
  return StatusCode::SUCCESS;
}

void TTBarEventReaderParticleLevel::readSmallRJets(ReconstructedEvent &event) const
{
  const ParticleLevelTreeTopCPToolkit &t = *m_treeReader;

  if (m_debug > 5)
    std::cout << "TTBarEventReaderParticleLevel: getting small-R jets" << std::endl;
  ObjectContainer_t jets;
  float jets_Ht(0.);
  for (size_t ijet = 0; ijet < t.jet_pt->size(); ijet++)
  {

    jets_Ht += t.jet_pt->at(ijet) / 1000.;

    if (m_debug > 6)
      std::cout << "TTBarEventReaderParticleLevel::readSmallRJets(): jet " << ijet << std::endl;
    auto jet = std::make_shared<Object_t>(t.jet_pt->at(ijet) / 1000., t.jet_eta->at(ijet), t.jet_phi->at(ijet), t.jet_e->at(ijet) / 1000.); // Transforming to GeV
    jet->setName(jet->getName() + " SmallRJet");
    if (m_debug > 6)
      std::cout << "TTBarEventReaderParticleLevel::readSmallRJets(): found 4-momentum for jet " << ijet << std::endl;

    jets.push_back(jet);
    jet->setAttribute<int>("nGhosts_bHadron", t.jet_nGhosts_bHadron->at(ijet));
  }

  event.setAttribute<float>("jets_Ht", jets_Ht);
  event.setObjectCollection("SmallRJets", std::move(jets));
}

void TTBarEventReaderParticleLevel::readLargeRJets(ReconstructedEvent &event) const
{

  const ParticleLevelTreeTopCPToolkit &t = *m_treeReader;
  ObjectContainer_t largeRJets;

  size_t nLargeRJets = t.ljet_pt->size();

  auto indexes = std::make_shared<std::vector<int>>();
  float ljets_Ht(0.);
  int counter(0);
  for (size_t ijet = 0; ijet < nLargeRJets; ++ijet)
  {
    counter++;

    auto ljet = std::make_shared<Object_t>(t.ljet_pt->at(ijet) / 1000., t.ljet_eta->at(ijet), t.ljet_phi->at(ijet), t.ljet_e->at(ijet) / 1000.);
    ljet->setName(ljet->getName() + " LargeRJet");

    ljet->setAttribute<float>("Qw", t.ljet_Qw->at(ijet) / 1000.);
    ljet->setAttribute<float>("Tau1", t.ljet_Tau1_wta->at(ijet));
    ljet->setAttribute<float>("Tau2", t.ljet_Tau2_wta->at(ijet));
    ljet->setAttribute<float>("Tau3", t.ljet_Tau3_wta->at(ijet));

    ljet->setAttribute<bool>("BTaggedGhostBHadrons", t.ljet_GhostBHadronsFinalCount->at(ijet) > 0);
    ljet->setAttribute<int>("GhostBHadronsFinalCount", t.ljet_GhostBHadronsFinalCount->at(ijet));
    ljet->setAttribute<int>("GhostCHadronsFinalCount", t.ljet_GhostCHadronsFinalCount->at(ijet));

    ljet->setAttribute<int>("D2", t.ljet_D2->at(ijet));

    indexes->push_back(ijet);
    largeRJets.push_back(ljet);

    ljets_Ht += t.ljet_pt->at(ijet) / 1000.;
  }

  event.setObjectCollection("LargeRJets", std::move(largeRJets));
  event.setPtr<std::vector<int>>("LargeRJetsIndexes", indexes);
  event.setAttribute<float>("ljets_Ht", ljets_Ht);
}

void TTBarEventReaderParticleLevel::readElectrons(ReconstructedEvent &event) const
{

  const ParticleLevelTreeTopCPToolkit &t = *m_treeReader;

  ObjectContainer_t electrons;

  size_t nElectrons = t.el_pt->size();

  for (size_t i = 0; i < nElectrons; i++)
  {
    auto electron = std::make_shared<Object_t>(t.el_pt->at(i) / 1000., t.el_eta->at(i), t.el_phi->at(i), t.el_e->at(i) / 1000.);
    electron->setName(electron->getName() + " Electron");

    electron->setAttribute<float>("Charge", t.el_charge->at(i));
    electron->setAttribute<unsigned int>("Origin", t.el_origin->at(i));
    electron->setAttribute<unsigned int>("Type", t.el_type->at(i));

    electrons.push_back(electron);
  }

  event.setObjectCollection("Electrons", std::move(electrons));
}

void TTBarEventReaderParticleLevel::readMuons(ReconstructedEvent &event) const
{

  const ParticleLevelTreeTopCPToolkit &t = *m_treeReader;

  ObjectContainer_t muons;

  size_t nMuons = t.mu_pt->size();

  for (size_t i = 0; i < nMuons; i++)
  {
    auto muon = std::make_shared<Object_t>(t.mu_pt->at(i) / 1000., t.mu_eta->at(i), t.mu_phi->at(i), t.mu_e->at(i) / 1000.); // JH same as for electrons
    muon->setName(muon->getName() + " Muon");

    muon->setAttribute<float>("Charge", t.mu_charge->at(i));
    muon->setAttribute<unsigned int>("Origin", t.mu_origin->at(i));
    muon->setAttribute<unsigned int>("Type", t.mu_type->at(i));

    muons.push_back(muon);
  }

  event.setObjectCollection("Muons", std::move(muons));
}

void TTBarEventReaderParticleLevel::readMET(ReconstructedEvent &event) const
{
  event.setAttribute<float>("met_met", m_treeReader->met_met / 1000.);
  event.setAttribute<float>("met_phi", m_treeReader->met_phi);
}

//----------------------------------------------------------------------

ReconstructedEvent *TTBarEventReaderParticleLevel::readEntry(const ULong64_t ientry)
{

  m_treeReader->getEntry(ientry);

  ReconstructedEvent *event = new ReconstructedEvent;
  // Reading object collections and event variables
  readSmallRJets(*event);
  readLargeRJets(*event);
  readElectrons(*event);
  readMuons(*event);
  readMET(*event);

  event->setAttribute<UInt_t>("RunNumber", m_treeReader->runNumber);
  event->setAttribute<ULong64_t>("EventNumber", m_treeReader->eventNumber);
  event->setAttribute<ULong64_t>("ChannelNumber", m_treeReader->mcChannelNumber);
  event->setAttribute<bool>("IsData", false);
  this->readGenWeights(*event);

  return event;
}

//----------------------------------------------------------------------

void TTBarEventReaderParticleLevel::readGenWeights(ReconstructedEvent &event) const
{
  event.setAttribute<float>("weight_mc", m_treeReader->weight_mc);
}

//----------------------------------------------------------------------

void TTBarEventReaderParticleLevel::print() const
{
  m_treeReader->print();
}
