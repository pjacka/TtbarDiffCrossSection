#include "TtbarDiffCrossSection/TTbarEventSelectionToolParticleLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventSelectionToolParticleLevel)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventSelectionToolParticleLevel::TTbarEventSelectionToolParticleLevel(const std::string& name) : EventSelectionToolBase(name) {
  
  m_cutflow=nullptr;
  m_cutflow_noweights=nullptr;
  m_leptonVetoPt=0.;
  m_top1PtMin=0.;
  m_top2PtMin=0.;
  m_top1MassMin=0.;
  m_top1MassMax=0.;
  m_top2MassMin=0.;
  m_top2MassMax=0.;
  
  declareProperty("cutflow",m_cutflow);
  declareProperty("cutflowNoWeights",m_cutflow_noweights);

}

//----------------------------------------------------------------------

StatusCode TTbarEventSelectionToolParticleLevel::readJSONConfig(const nlohmann::json& json) {
  
  try {
    
    m_leptonVetoPt=jsonFunctions::readValueJSON<double>(json,"leptonVetoPt");
    m_top1PtMin=jsonFunctions::readValueJSON<double>(json,"top1PtMin");
    m_top2PtMin=jsonFunctions::readValueJSON<double>(json,"top2PtMin");
    m_top1MassMin=jsonFunctions::readValueJSON<double>(json,"top1MassMin");
    m_top1MassMax=jsonFunctions::readValueJSON<double>(json,"top1MassMax");
    m_top2MassMin=jsonFunctions::readValueJSON<double>(json,"top2MassMin");
    m_top2MassMax=jsonFunctions::readValueJSON<double>(json,"top2MassMax");
    
    m_bTagDecoration = jsonFunctions::readValueJSON<std::string>(json, "BTagDecoration");
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventSelectionToolParticleLevel::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

bool TTbarEventSelectionToolParticleLevel::execute(const ReconstructedEvent& event, bool fillCutflow) const {
 
  const double weight = fillCutflow ? event.getAttribute<double>("weight") : 1.;

  // Lepton veto cuts ************************************
  const ObjectContainer_t& electrons = event.getObjectCollection("Electrons");
  const ObjectContainer_t& muons = event.getObjectCollection("Muons");
  
  if(electrons.size() > 0 && electrons[0]->Pt() > m_leptonVetoPt) return false;
  if(muons.size() > 0 && muons[0]->Pt() > m_leptonVetoPt) return false;

  if(fillCutflow) {
    m_cutflow->Fill(2, weight);
    m_cutflow_noweights->Fill(2);
  }

  int tr = 0;


  if(!event.isAvailable("top1") || !event.isAvailable("top2")) {
    if(m_debug > 5) {
      std::cout << "TTbarEventSelectionToolParticleLevel::execute: Particle level tops not found in the event" << std::endl;
    }
    return false;
  }
  
  const Object_t& leadingJet = event.getConstRef<Object_t>("top1");
  const Object_t& recoilJet = event.getConstRef<Object_t>("top2");

	
  if(recoilJet.Pt() < m_top2PtMin) return false;
  
  if(fillCutflow) {
    m_cutflow->Fill(3+tr, weight);
    m_cutflow_noweights->Fill(3+tr);
  }

  if(leadingJet.Pt() < m_top1PtMin) return false;
  if(fillCutflow) {
    m_cutflow->Fill(4+tr, weight);
    m_cutflow_noweights->Fill(4.+tr);
  }

  if((leadingJet.M() < m_top1MassMin) || (leadingJet.M() > m_top1MassMax)) return false;
  if(fillCutflow) {
    m_cutflow->Fill(5+tr, weight);
    m_cutflow_noweights->Fill(5+tr);
  }

  if((recoilJet.M() < m_top2MassMin) || (recoilJet.M() > m_top2MassMax)) return false;
  
  if(fillCutflow) {
    
    bool leadingBTagged = leadingJet.getAttribute<bool>(m_bTagDecoration);
    bool recoilBTagged = recoilJet.getAttribute<bool>(m_bTagDecoration);
    
    m_cutflow->Fill(6+tr, weight);
    m_cutflow_noweights->Fill(6+tr);
    if(leadingBTagged) {
      m_cutflow->Fill(7+tr, weight);
      m_cutflow_noweights->Fill(7+tr);
      if(recoilBTagged) {
        m_cutflow->Fill(8+tr, weight);
        m_cutflow_noweights->Fill(8+tr);
      }
    }
  }
  
  return true;
}


//----------------------------------------------------------------------

TH1D* TTbarEventSelectionToolParticleLevel::initCutflowHistogram(const std::string& name) const {

  std::vector<std::string> binLabels;

  binLabels = {
     "Initial",
     "GRID selection",
     (std::string)"ljet2 pt > " + Form("%4.0f",m_top2PtMin),
     (std::string)"ljet1 pt > " + Form("%4.0f",m_top1PtMin),
     (std::string)Form("%4.1f",m_top1MassMin) + " < ljet1 m < " + Form("%4.1f",m_top1MassMax),
     (std::string)Form("%4.1f",m_top2MassMin) + " < ljet2 m < " + Form("%4.1f",m_top2MassMax),
     "ljet1 b-matched",
     "ljet2 b-matched"
  };

  
  TH1D* h = new TH1D(name.c_str(),"",binLabels.size(),0.5,0.5+binLabels.size());
  h->Sumw2();
  
  for(size_t i=0;i<binLabels.size();i++) h->GetXaxis()->SetBinLabel(i+1,binLabels[i].c_str());
  
  return h;
}
