#include "TtbarDiffCrossSection/TTbarEventReweightingToolPartonLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

#include "TopEvent/EventTools.h"
#include "TSystem.h"
ClassImp(TTbarEventReweightingToolPartonLevel)


TTbarEventReweightingToolPartonLevel::TTbarEventReweightingToolPartonLevel(const std::string& name) : EventReweightingToolBase(name) {
  m_xsecWithKfactor = {-1.};
  m_HtFilterEfficiencyCorrectionsFile = 0;
  m_samplesWithHtFilterEfficiencyCorrection = {410429,410431,410444,410445};
  m_sumWeights = -1.;

  declareProperty("xsecWithKfactor", m_xsecWithKfactor);
  declareProperty("sumWeights", m_sumWeights);
  declareProperty("lumi", m_lumi);  
}


//----------------------------------------------------------------------

StatusCode TTbarEventReweightingToolPartonLevel::readJSONConfig(const nlohmann::json& /*json*/) {
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingToolPartonLevel::initialize() {
  top::check(EventReweightingToolBase::initialize(),m_name+": Failed to initialize");
  
  //top::check(m_tree,m_name+": Truth tree not loaded");
  
  //top::check(m_xsecWithKfactor>0,m_name+": xsecWithKfactor is not set before initialization");
    
  //if(m_ttbarConfig->useFilterEfficienciesCorrections()) {
    
    //TString name = m_ttbarConfig->filterEfficienciesCorrectionsFileName();
    //if(!name.BeginsWith("/")) name = (TString)gSystem->Getenv("TTBAR_PATH")+"/"+name;
    //m_HtFilterEfficiencyCorrectionsFile = std::unique_ptr<TFile>{TFile::Open(name)};
    //if(m_HtFilterEfficiencyCorrectionsFile==0 || m_HtFilterEfficiencyCorrectionsFile->IsZombie()) {
      //std::cout << "Error: Failed to open file with Ht-filter efficiency corrections. " << name << std::endl;
      //exit(-2);
    //}
    
  //}
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingToolPartonLevel::execute(ReconstructedEvent& event) {
  const double weight_mc = event.getAttribute<float>("weight_mc");
  
  m_weight = weight_mc * m_xsecWithKfactor * m_lumi / m_sumWeights;
  event.setAttribute<double>("weight", m_weight);
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventReweightingToolPartonLevel::multiplyEventWeights(const double weight) {
  m_weight*=weight;
}


// ---------------------------------------------------------------------

//double TTbarEventReweightingToolPartonLevel::getFilterEfficiencyCorrection() const {
  
  //double filterEfficiencyCorrection=1.;
  
  //if (!m_ttbarConfig->useFilterEfficienciesCorrections() ||
      //std::find(std::begin(m_samplesWithHtFilterEfficiencyCorrection),
		//std::end(m_samplesWithHtFilterEfficiencyCorrection),
		//m_tree->mcChannelNumber) == std::end(m_samplesWithHtFilterEfficiencyCorrection) ||
      //(!m_isIFSRChain && !m_isFSRChain && !m_isPDFChain) ) return 1.;
  
  
  //const TString histName=Form("filterEfficiencyCorrections.%i",m_tree->mcChannelNumber);
  //auto h = std::unique_ptr<TH1D>{(TH1D*)m_HtFilterEfficiencyCorrectionsFile->Get(histName)};
  
  //if(m_isIFSRChain) {
    //const std::vector<int>& vec = m_ttbarConfig->getISRbranches().find(m_sysName.Data())->second;
    //for(int index : vec) filterEfficiencyCorrection *= h->GetBinContent(index);
  //}
  //if(m_isFSRChain) {
    //const int index = m_ttbarConfig->getFSRbranches().find(m_sysName.Data())->second;
    //filterEfficiencyCorrection *= h->GetBinContent(index);
  //}
  //if(m_isPDFChain) {
    //const int index = m_ttbarConfig->getPDFbranches().find(m_sysName.Data())->second;
    //filterEfficiencyCorrection *= h->GetBinContent(index);
  //}
  
  //if(m_debug>2) std::cout << "Filter efficiency correction: " << filterEfficiencyCorrection << " sysName: " << m_sysName << std::endl;
  
  //return filterEfficiencyCorrection;
  
//}
