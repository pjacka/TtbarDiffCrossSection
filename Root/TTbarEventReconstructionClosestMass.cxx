#include "TtbarDiffCrossSection/TTbarEventReconstructionClosestMass.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventReconstructionClosestMass)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventReconstructionClosestMass::TTbarEventReconstructionClosestMass(const std::string& name) : ObjectSelectionTool(name)
{
  m_minMassCut=0.;
  m_topMass=172.5;
  m_minPt1Cut = 500;
  m_minPt2Cut = 350;
  
}

//----------------------------------------------------------------------

StatusCode TTbarEventReconstructionClosestMass::readJSONConfig(const nlohmann::json& json) {
  
  try {
    if(!ObjectSelectionTool::readJSONConfig(json)) {
      throw std::runtime_error("TTbarEventReconstructionClosestMass: Failed to read ObjectSelectionTool configuration.");
    }
    m_minMassCut = jsonFunctions::readValueJSON<double>(json,"minMassCut");
    m_topMass = jsonFunctions::readValueJSON<double>(json,"topMass",172.5);
    m_minPt1Cut = jsonFunctions::readValueJSON<double>(json,"minPt1Cut");
    m_minPt2Cut = jsonFunctions::readValueJSON<double>(json,"minPt2Cut");
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventReconstructionClosestMass::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventReconstructionClosestMass::execute(ReconstructedEvent& event) const {
  // Executing object selection algorithm
  ObjectSelectionTool::execute(event);
  
  //std::cout << "Executing TTbarEventReconstructionClosestMass algorithm" << std::endl;
  
  // Selecting top candidates from the set of large-R jets
  std::string collectionName = "";
  if (m_largeRJetType == kLJET) collectionName = "LargeRJets";
  else if (m_largeRJetType == kRCJET) collectionName = "RCJets";
  else collectionName = "VarRCJets";
  const ObjectContainer_t& largeRJets = event.getObjectCollection(collectionName);
  
  int top1Index(-1);
  int top2Index(-1);
  
  double deltaMass(FLT_MAX);
  
  for(int i=0;i<(int)largeRJets.size();i++) {
    double m=largeRJets[i]->M();
    if(m <= m_minMassCut || largeRJets[i]->Pt() < m_minPt1Cut) continue;
    
    if(const double diff = std::abs(m-m_topMass); diff < deltaMass) {
      deltaMass=diff;
      top1Index=i;
    }
  }
  
  deltaMass=FLT_MAX;
  for(int i=0;i<(int)largeRJets.size();i++) {
    double m=largeRJets[i]->M();
    if(i == top1Index || m <= m_minMassCut || largeRJets[i]->Pt() < m_minPt2Cut) continue;
    
    if(const double diff = std::abs(m-m_topMass); diff < deltaMass) {
      deltaMass=diff;
      top2Index=i;
    }
  }
  
  if(top1Index>=0 && top2Index>=0) {
    if(largeRJets[top1Index]->Pt() < largeRJets[top2Index]->Pt()) std::swap(top1Index,top2Index);
  }
  
  //std::cout << "top1Index: " << top1Index << " top2Index: " << top2Index << std::endl;
  //std::cout << "top1 pt: " << largeRJets[top1Index]->Pt() << " top2 pt: " << largeRJets[top2Index]->Pt() << std::endl;
  //std::cout << "top1: " << largeRJets[top1Index] << " top2 pt: " << largeRJets[top2Index] << std::endl;
  
  event.setAttribute<int>("top1Index",top1Index);
  event.setAttribute<int>("top2Index",top2Index);
  if (top1Index >= 0) event.setPtr<Object_t>("top1",largeRJets[top1Index]);
  if (top2Index > 0) event.setPtr<Object_t>("top2",largeRJets[top2Index]);
  
}



