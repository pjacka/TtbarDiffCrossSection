#include "TtbarDiffCrossSection/RecoTreeTopCPToolkit.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include <iostream>

ClassImp(RecoTreeTopCPToolkit)

    using namespace std;

void RecoTreeTopCPToolkit::print()
{
  // Print contents of current entry.
  // m_chain->Show();

  cout << "run/event number, mcChannelNumber:" << endl;
  cout << runNumber << "  " << eventNumber << "  " << mcChannelNumber << "  " << endl;
  std::cout << "weight_mc: " << weight_mc << std::endl;

  // cout << "met_et, met_phi : " << met_met << "  " << met_phi << endl;

  cout << "large-R jets info (ID, pt, eta, phi, m, tau3, tagger_score):" << endl;
  int njets = ljet_pt->size();
  for (int i = 0; i < njets; i++)
  {
    cout << i << ":  " << ljet_pt->at(i) << "  " << ljet_eta->at(i) << "  " << ljet_phi->at(i) << "  "
         << ljet_m->at(i) << "  " << ljet_Tau3_wta->at(i) << " " << ljet_topTagger_score->at(i) << "  "
         << endl;
  }

  cout << "small jets info (ID, pt, eta, phi, E, b-tagger quantile, b-tagged):" << endl;
  njets = jet_pt->size();
  for (int i = 0; i < njets; i++)
  {
    cout << i << ":  " << jet_pt->at(i) << "  " << jet_eta->at(i) << "  "
         << jet_phi->at(i) << "  " << jet_e->at(i);
    if (m_bTaggerUseContinuousWP)
      std::cout << (TString) " b-tagger quantile: " << jet_bTagger_continuous_quantile->at(i);
    if (m_bTaggerUseFixedWP)
      std::cout << (TString) " b-tagged " + m_bTaggerFixedWP + ": " << !(jet_bTagged->at(i) == 0);
    std::cout << std::endl;
  }
}

//----------------------------------------------------------------------

RecoTreeTopCPToolkit::RecoTreeTopCPToolkit(const nlohmann::json &json, bool isData) : TreeTopCPToolkitBase(json)
{
  m_isData = isData;
  try
  {
    m_bTaggerName = jsonFunctions::readValueJSON<std::string>(json, "bTaggerName", "GN2v01");
    m_bTaggerUseContinuousWP = jsonFunctions::readValueJSON<bool>(json, "bTaggerUseContinuousWP", true);
    m_bTaggerContinuousWP = jsonFunctions::readValueJSON<std::string>(json, "bTaggerContinuousWP", "Continuous");
    m_bTaggerUseFixedWP = jsonFunctions::readValueJSON<bool>(json, "bTaggerUseFixedWP", false);
    m_bTaggerFixedWP = jsonFunctions::readValueJSON<std::string>(json, "bTaggerFixedWP", "FixedCutBEff_77");
    m_useBTaggerWeights = jsonFunctions::readValueJSON<bool>(json, "useBTaggerWeights", true);

    m_topTaggerName = jsonFunctions::readValueJSON<std::string>(json, "topTaggerName", "DNNTaggerTopQuarkContained50");
    m_electron_selection = jsonFunctions::readValueJSON<std::string>(json, "electronSelection", "tight");
    m_muon_selection = jsonFunctions::readValueJSON<std::string>(json, "muonSelection", "tight");
  }
  catch (const std::runtime_error &e)
  {
    std::cout << "Error in RecoTreeTopCPToolkit config:" << std::endl;
    throw e;
  }
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initChain(TChain *tree, const TString &sysName)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  m_sysName = sysName;
  bootstrapWeights = 0;

  // Set branch addresses and branch pointers
  if (!tree)
    return;
  m_chain = tree;
  m_current = -1;
  m_chain->SetMakeClass(1);

  m_chain->SetCacheSize(10000000); // 10 MB
  m_chain->SetCacheLearnEntries(100);

  if (m_chain->GetEntries() > 0)
    readBranchNames();
  else
    return;

  setBranchAddress("bootstrapWeights", &bootstrapWeights, &b_bootstrapWeights);
  setBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  setBranchAddress("runNumber", &runNumber, &b_runNumber);
  setBranchAddress("pass_ljets", &pass_ljets, &b_pass_ljets);

  if (!m_isData)
  {
    setBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
    setBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
    setBranchAddress("weight_jvt_effSF", &weight_jvt_effSF, &b_weight_jvt_effSF);
    setBranchAddress("weight_leptonSF_tight", &weight_leptonSF_tight, &b_weight_leptonSF_tight);
    initGenWeights();
  }

  initLargeRJets();
  initTopTagger();
  initSmallRJets();
  initBTagger();
  initElectrons();
  initMuons();
  initMET();
  initTriggers();
}
//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initGenWeights()
{
  setBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initLargeRJets()
{
  ljet_GhostBHadronsFinalCount = 0;
  ljet_GhostCHadronsFinalCount = 0;
  ljet_Qw = 0;
  ljet_TruthLabel = 0;
  ljet_Split12 = 0;
  ljet_Split23 = 0;
  ljet_Tau1_wta = 0;
  ljet_Tau2_wta = 0;
  ljet_Tau3_wta = 0;
  ljet_Tau4_wta = 0;
  ljet_m = 0;
  ljet_pt = 0;
  ljet_eta = 0;
  ljet_phi = 0;

  setBranchAddress("ljet_Qw", &ljet_Qw, &b_ljet_Qw);
  setBranchAddress("ljet_Split12", &ljet_Split12, &b_ljet_Split12);
  setBranchAddress("ljet_Split23", &ljet_Split23, &b_ljet_Split23);
  setBranchAddress("ljet_Tau1_wta", &ljet_Tau1_wta, &b_ljet_Tau1_wta);
  setBranchAddress("ljet_Tau2_wta", &ljet_Tau2_wta, &b_ljet_Tau2_wta);
  setBranchAddress("ljet_Tau3_wta", &ljet_Tau3_wta, &b_ljet_Tau3_wta);
  setBranchAddress("ljet_Tau4_wta", &ljet_Tau4_wta, &b_ljet_Tau4_wta);
  setBranchAddress("ljet_m", &ljet_m, &b_ljet_m);
  setBranchAddress("ljet_pt", &ljet_pt, &b_ljet_pt);
  setBranchAddress("ljet_eta", &ljet_eta, &b_ljet_eta);
  setBranchAddress("ljet_phi", &ljet_phi, &b_ljet_phi);
  if (!m_isData)
  {
    setBranchAddress("ljet_R10TruthLabel_R22v1", &ljet_TruthLabel, &b_ljet_TruthLabel);
    setBranchAddress("ljet_GhostBHadronsFinalCount", &ljet_GhostBHadronsFinalCount, &b_ljet_GhostBHadronsFinalCount);
    setBranchAddress("ljet_GhostCHadronsFinalCount", &ljet_GhostCHadronsFinalCount, &b_ljet_GhostCHadronsFinalCount);
  }
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initTopTagger()
{

  ljet_topTagger_SF = 0;
  ljet_topTagger_effSF = 0;
  ljet_topTagger_efficiency = 0;
  ljet_topTagger_passMass = 0;
  ljet_topTagger_score = 0;
  ljet_topTagger_tagged = 0;

  setBranchAddress("ljet_" + m_topTaggerName + "_SF", &ljet_topTagger_SF, &b_ljet_topTagger_SF);
  setBranchAddress("ljet_" + m_topTaggerName + "_effSF", &ljet_topTagger_effSF, &b_ljet_topTagger_effSF);
  setBranchAddress("ljet_" + m_topTaggerName + "_efficiency", &ljet_topTagger_efficiency, &b_ljet_topTagger_efficiency);
  setBranchAddress("ljet_" + m_topTaggerName + "_passMass", &ljet_topTagger_passMass, &b_ljet_topTagger_passMass);
  setBranchAddress("ljet_" + m_topTaggerName + "_score", &ljet_topTagger_score, &b_ljet_topTagger_score);
  setBranchAddress("ljet_" + m_topTaggerName + "_tagged", &ljet_topTagger_tagged, &b_ljet_topTagger_tagged);
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initSmallRJets()
{

  jet_eta = 0;
  jet_e = 0;
  jet_phi = 0;
  jet_pt = 0;
  jet_select_baselineJvt = 0;

  setBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
  setBranchAddress("jet_e", &jet_e, &b_jet_e);
  setBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
  setBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
  setBranchAddress("jet_select_baselineJvt", &jet_select_baselineJvt, &b_jet_select_baselineJvt);
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initBTagger()
{
  jet_bTagged = 0;
  jet_bTagger_continuous_quantile = 0;
  jet_bTagger_score = 0;

  // Score
  // setBranchAddress("jet_" + m_bTaggerName, &jet_bTagger_score, &b_jet_bTagger_score);

  // Working point
  if (m_bTaggerUseContinuousWP)
    setBranchAddress("jet_" + m_bTaggerName + "_" + m_bTaggerContinuousWP + "_quantile", &jet_bTagger_continuous_quantile, &b_jet_bTagger_continuous_quantile);
  if (m_bTaggerUseFixedWP)
    setBranchAddress("jet_" + m_bTaggerName + "_" + m_bTaggerFixedWP + "_select", &jet_bTagged, &b_jet_bTagged);

  // Event weight scale factors
  if (m_useBTaggerWeights && (!m_isData))
    setBranchAddress("weight_ftag_effSF_" + m_bTaggerName + "_" + m_bTaggerContinuousWP, &weight_bTag_SF, &b_weight_bTag_SF);
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initElectrons()
{
  el_IFFClass = 0;
  el_charge = 0;
  el_eta = 0;
  el_phi = 0;
  el_select = 0;
  el_e = 0;
  el_pt = 0;

  setBranchAddress("el_charge", &el_charge, &b_el_charge);
  setBranchAddress("el_eta", &el_eta, &b_el_eta);
  setBranchAddress("el_phi", &el_phi, &b_el_phi);
  setBranchAddress("el_select_" + m_electron_selection + "", &el_select, &b_el_select);
  setBranchAddress("el_e", &el_e, &b_el_e);
  setBranchAddress("el_pt", &el_pt, &b_el_pt);
  if (!m_isData)
  {
    setBranchAddress("el_IFFClass", &el_IFFClass, &b_el_IFFClass);
  }
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initMuons()
{
  mu_IFFClass = 0;
  mu_charge = 0;
  mu_eta = 0;
  mu_phi = 0;
  mu_select = 0;
  mu_e = 0;
  mu_TTVA_effSF = 0;
  mu_isol_effSF = 0;
  mu_reco_effSF = 0;
  mu_pt = 0;

  setBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
  setBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
  setBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
  setBranchAddress("mu_select_" + m_muon_selection + "", &mu_select, &b_mu_select);
  setBranchAddress("mu_e", &mu_e, &b_mu_e);

  setBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
  if (!m_isData)
  {
    setBranchAddress("mu_IFFClass", &mu_IFFClass, &b_mu_IFFClass);
    setBranchAddress("mu_TTVA_effSF_" + m_muon_selection + "", &mu_TTVA_effSF, &b_mu_TTVA_effSF);
    if (m_muon_selection == "tight")
      setBranchAddress("mu_isol_effSF_" + m_muon_selection + "", &mu_isol_effSF, &b_mu_isol_effSF);
    setBranchAddress("mu_reco_effSF_" + m_muon_selection + "", &mu_reco_effSF, &b_mu_reco_effSF);
  }
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initMET()
{
  setBranchAddress("met_met", &met_met, &b_met_met);
  setBranchAddress("met_phi", &met_phi, &b_met_phi);
  setBranchAddress("met_significance", &met_significance, &b_met_significance);
  setBranchAddress("met_sumet", &met_sumet, &b_met_sumet);
}

//----------------------------------------------------------------------

void RecoTreeTopCPToolkit::initTriggers()
{

  setBranchAddress("trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100", &trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100, &b_trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1J100);
  setBranchAddress("trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15", &trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15, &b_trigPassed_HLT_2j330_35smcINF_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1J100", &trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1J100, &b_trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1J100);
  setBranchAddress("trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15", &trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15, &b_trigPassed_HLT_2j330_35smcINF_a10t_lcw_jes_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_2j330_a10t_lcw_jes_30smcINF", &trigPassed_HLT_2j330_a10t_lcw_jes_30smcINF, &b_trigPassed_HLT_2j330_a10t_lcw_jes_30smcINF);
  setBranchAddress("trigPassed_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111", &trigPassed_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111, &b_trigPassed_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111);
  setBranchAddress("trigPassed_HLT_ht1000_L1J100", &trigPassed_HLT_ht1000_L1J100, &b_trigPassed_HLT_ht1000_L1J100);
  setBranchAddress("trigPassed_HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15", &trigPassed_HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15, &b_trigPassed_HLT_j360_60smcINF_j360_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15", &trigPassed_HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15, &b_trigPassed_HLT_j360_60smcINF_j360_a10t_lcw_jes_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j360_a10_sub_L1J100", &trigPassed_HLT_j360_a10_sub_L1J100, &b_trigPassed_HLT_j360_a10_sub_L1J100);
  setBranchAddress("trigPassed_HLT_j360_a10r_L1J100", &trigPassed_HLT_j360_a10r_L1J100, &b_trigPassed_HLT_j360_a10r_L1J100);
  setBranchAddress("trigPassed_HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15", &trigPassed_HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15, &b_trigPassed_HLT_j370_35smcINF_j370_a10sd_cssk_pf_jes_ftf_presel2j225_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15", &trigPassed_HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15, &b_trigPassed_HLT_j370_35smcINF_j370_a10t_lcw_jes_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100", &trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100, &b_trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1J100);
  setBranchAddress("trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15", &trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15, &b_trigPassed_HLT_j420_35smcINF_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1J100", &trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1J100, &b_trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1J100);
  setBranchAddress("trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15", &trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15, &b_trigPassed_HLT_j420_35smcINF_a10t_lcw_jes_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j420_a10_lcw_L1J100", &trigPassed_HLT_j420_a10_lcw_L1J100, &b_trigPassed_HLT_j420_a10_lcw_L1J100);
  setBranchAddress("trigPassed_HLT_j420_a10r_L1J100", &trigPassed_HLT_j420_a10r_L1J100, &b_trigPassed_HLT_j420_a10r_L1J100);
  setBranchAddress("trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100", &trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100, &b_trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100);
  setBranchAddress("trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111", &trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111, &b_trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111);
  setBranchAddress("trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100", &trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100, &b_trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100);
  setBranchAddress("trigPassed_HLT_j440_a10_lcw_subjes_L1SC111", &trigPassed_HLT_j440_a10_lcw_subjes_L1SC111, &b_trigPassed_HLT_j440_a10_lcw_subjes_L1SC111);
  setBranchAddress("trigPassed_HLT_j440_a10r_L1SC111", &trigPassed_HLT_j440_a10r_L1SC111, &b_trigPassed_HLT_j440_a10r_L1SC111);
  setBranchAddress("trigPassed_HLT_j460_a10_lcw_subjes_L1J100", &trigPassed_HLT_j460_a10_lcw_subjes_L1J100, &b_trigPassed_HLT_j460_a10_lcw_subjes_L1J100);
  setBranchAddress("trigPassed_HLT_j460_a10_lcw_subjes_L1SC111", &trigPassed_HLT_j460_a10_lcw_subjes_L1SC111, &b_trigPassed_HLT_j460_a10_lcw_subjes_L1SC111);
  setBranchAddress("trigPassed_HLT_j460_a10_lcw_subjes_L1SC111_CJ15", &trigPassed_HLT_j460_a10_lcw_subjes_L1SC111_CJ15, &b_trigPassed_HLT_j460_a10_lcw_subjes_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j460_a10r_L1J100", &trigPassed_HLT_j460_a10r_L1J100, &b_trigPassed_HLT_j460_a10r_L1J100);
  setBranchAddress("trigPassed_HLT_j460_a10r_L1SC111", &trigPassed_HLT_j460_a10r_L1SC111, &b_trigPassed_HLT_j460_a10r_L1SC111);
  setBranchAddress("trigPassed_HLT_j460_a10r_L1SC111_CJ15", &trigPassed_HLT_j460_a10r_L1SC111_CJ15, &b_trigPassed_HLT_j460_a10r_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100", &trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100, &b_trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1J100);
  setBranchAddress("trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15", &trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15, &b_trigPassed_HLT_j460_a10sd_cssk_pf_jes_ftf_preselj225_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j460_a10t_lcw_jes_L1J100", &trigPassed_HLT_j460_a10t_lcw_jes_L1J100, &b_trigPassed_HLT_j460_a10t_lcw_jes_L1J100);
  setBranchAddress("trigPassed_HLT_j460_a10t_lcw_jes_L1SC111", &trigPassed_HLT_j460_a10t_lcw_jes_L1SC111, &b_trigPassed_HLT_j460_a10t_lcw_jes_L1SC111);
  setBranchAddress("trigPassed_HLT_j460_a10t_lcw_jes_L1SC111_CJ15", &trigPassed_HLT_j460_a10t_lcw_jes_L1SC111_CJ15, &b_trigPassed_HLT_j460_a10t_lcw_jes_L1SC111_CJ15);
  setBranchAddress("trigPassed_HLT_j480_a10_lcw_subjes_L1J100", &trigPassed_HLT_j480_a10_lcw_subjes_L1J100, &b_trigPassed_HLT_j480_a10_lcw_subjes_L1J100);
  setBranchAddress("trigPassed_HLT_j480_a10_lcw_subjes_L1SC111", &trigPassed_HLT_j480_a10_lcw_subjes_L1SC111, &b_trigPassed_HLT_j480_a10_lcw_subjes_L1SC111);
  setBranchAddress("trigPassed_HLT_j480_a10r_L1J100", &trigPassed_HLT_j480_a10r_L1J100, &b_trigPassed_HLT_j480_a10r_L1J100);
  setBranchAddress("trigPassed_HLT_j480_a10r_L1SC111", &trigPassed_HLT_j480_a10r_L1SC111, &b_trigPassed_HLT_j480_a10r_L1SC111);
  setBranchAddress("trigPassed_HLT_j480_a10t_lcw_jes_L1J100", &trigPassed_HLT_j480_a10t_lcw_jes_L1J100, &b_trigPassed_HLT_j480_a10t_lcw_jes_L1J100);
  setBranchAddress("trigPassed_HLT_j480_a10t_lcw_jes_L1SC111", &trigPassed_HLT_j480_a10t_lcw_jes_L1SC111, &b_trigPassed_HLT_j480_a10t_lcw_jes_L1SC111);
}
