#include "TtbarDiffCrossSection/TTbarEventClassificationToolBoostedAllhad.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"
#include "NtuplesAnalysisToolsCore/LargeRJet.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

ClassImp(TTbarEventClassificationToolBoostedAllhad)

    typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventClassificationToolBoostedAllhad::TTbarEventClassificationToolBoostedAllhad(const std::string &name) : EventClassificationToolBase(name)
{
  m_selections.resize(16, 0);
  m_selectionNames = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"};
}

//----------------------------------------------------------------------

StatusCode TTbarEventClassificationToolBoostedAllhad::readJSONConfig(const nlohmann::json &json)
{
  try
  {
    if (!EventClassificationToolBase::readJSONConfig(json))
      return StatusCode::FAILURE;
    m_bTagDecoration = jsonFunctions::readValueJSON<std::string>(json, "BTagDecoration");
    m_topTagDecoration = jsonFunctions::readValueJSON<std::string>(json, "TopTagDecoration");

    if (m_selectionNames.size() != 16)
      throw std::out_of_range("Error in TTbarEventClassificationToolBoostedAllhad constructor: m_selectionNames must have a length of 16.");
  }
  catch (const std::runtime_error &e)
  {
    throw std::runtime_error((std::string) "Error in TTbarEventClassificationToolBoostedAllhad::readJSONConfig:\n" + e.what());
  }
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventClassificationToolBoostedAllhad::execute(const ReconstructedEvent &event)
{
  try
  {
    if (!event.isAvailable("top1") || !event.isAvailable("top2"))
      throw(std::out_of_range("TTbarEventClassificationToolBoostedAllhad::execute: 'top1' or 'top2' objects not found!"));

    const Object_t &top1 = event.getConstRef<Object_t>("top1");
    const Object_t &top2 = event.getConstRef<Object_t>("top2");

    m_selections.assign(m_selections.size(), 0); // Reset selections to zero

    bool toptag1 = top1.getAttribute<bool>(m_topTagDecoration);
    bool btag1 = top1.getAttribute<bool>(m_bTagDecoration);
    bool toptag2 = top2.getAttribute<bool>(m_topTagDecoration);
    bool btag2 = top2.getAttribute<bool>(m_bTagDecoration);

    if (!toptag1 && !btag1 && !toptag2 && !btag2)
      m_selections[0] = 1; // A
    else if (!toptag1 && !btag1 && !toptag2 && btag2)
      m_selections[1] = 1; // B
    else if (toptag1 && !btag1 && !toptag2 && !btag2)
      m_selections[2] = 1; // C
    else if (toptag1 && !btag1 && !toptag2 && btag2)
      m_selections[3] = 1; // D
    else if (!toptag1 && !btag1 && toptag2 && !btag2)
      m_selections[4] = 1; // E
    else if (toptag1 && !btag1 && toptag2 && !btag2)
      m_selections[5] = 1; // F
    else if (!toptag1 && btag1 && toptag2 && !btag2)
      m_selections[6] = 1; // G
    else if (!toptag1 && btag1 && !toptag2 && btag2)
      m_selections[7] = 1; // H
    else if (!toptag1 && btag1 && !toptag2 && !btag2)
      m_selections[8] = 1; // I
    else if (!toptag1 && !btag1 && toptag2 && btag2)
      m_selections[9] = 1; // J
    else if (toptag1 && !btag1 && toptag2 && btag2)
      m_selections[10] = 1; // K
    else if (!toptag1 && btag1 && toptag2 && btag2)
      m_selections[11] = 1; // L
    else if (toptag1 && btag1 && toptag2 && !btag2)
      m_selections[12] = 1; // M
    else if (toptag1 && btag1 && !toptag2 && btag2)
      m_selections[13] = 1; // N
    else if (toptag1 && btag1 && !toptag2 && !btag2)
      m_selections[14] = 1; // O
    else if (toptag1 && btag1 && toptag2 && btag2)
      m_selections[15] = 1; // P
    else
      throw(std::out_of_range("TTbarEventClassificationToolBoostedAllhad::execute: Error in indexes l0t0br0t0b!"));
  }
  catch (const std::out_of_range &e)
  {
    throw std::out_of_range{(std::string)"TTbarEventClassificationToolBoostedAllhad::execute: " + e.what()};
  } catch (const std::bad_any_cast &e)
  {
    throw std::out_of_range{(std::string)"TTbarEventClassificationToolBoostedAllhad::execute: bad cast"};
  }
}
