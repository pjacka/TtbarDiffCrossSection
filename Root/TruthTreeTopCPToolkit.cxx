#include "TtbarDiffCrossSection/TruthTreeTopCPToolkit.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include <iostream>

ClassImp(TruthTreeTopCPToolkit)

    using namespace std;

void TruthTreeTopCPToolkit::print()
{
  // Print contents of current entry.
  cout << "run/event number, mcChannelNumber:" << endl;
  cout << runNumber << "  " << eventNumber << "  " << mcChannelNumber << "  " << endl;

  // cout << "met_et, met_phi : " << met_met << "  " << met_phi << endl;

  cout << "Top " + m_topQuarkDefinition + " (pt, eta, phi, m):" << endl;

  cout << Ttbar_MC_t_pt << "  " << Ttbar_MC_t_eta << "  " << Ttbar_MC_t_phi << "  "
       << Ttbar_MC_t_m << endl;
  cout << "Antitop " + m_topQuarkDefinition + " (pt, eta, phi, m):" << endl;
  cout << Ttbar_MC_tbar_pt << "  " << Ttbar_MC_tbar_eta << "  " << Ttbar_MC_tbar_phi << "  "
       << Ttbar_MC_tbar_m << endl;
}

//----------------------------------------------------------------------

TruthTreeTopCPToolkit::TruthTreeTopCPToolkit(const nlohmann::json &json) : TreeTopCPToolkitBase(json)
{
  try
  {
    m_topQuarkDefinition = jsonFunctions::readValueJSON<std::string>(json, "topQuarkDefinition", "afterFSR");       // afterFSR or beforeFSR
    m_ttbarSystemDefinition = jsonFunctions::readValueJSON<std::string>(json, "ttbarSystemDefinition", "afterFSR"); // afterFSR, beforeFSR, fromDecay_afterFSR, or fromDecay_beforeFSR
    m_bQuarkDefinition = jsonFunctions::readValueJSON<std::string>(json, "bQuarkDefinition", "afterFSR");           // afterFSR or beforeFSR
    m_WBosonDefinition = jsonFunctions::readValueJSON<std::string>(json, "WBosonDefinition", "afterFSR");           // afterFSR or beforeFSR
    m_WBosonDecayDefinition = jsonFunctions::readValueJSON<std::string>(json, "WBosonDecayDefinition", "afterFSR"); // afterFSR or beforeFSR
  }
  catch (const std::runtime_error &e)
  {
    std::cout << "Error in TruthTreeTopCPToolkit config:" << std::endl;
    throw e;
  }
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initChain(TChain *tree, const TString &sysName)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  m_sysName = sysName;

  // Set branch addresses and branch pointers
  if (!tree)
    return;
  m_chain = tree;
  m_current = -1;
  m_chain->SetMakeClass(1);

  m_chain->SetCacheSize(10000000); // 10 MB
  m_chain->SetCacheLearnEntries(100);

  if (m_chain->GetEntries() > 0)
    readBranchNames();
  else
    return;

  setBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  setBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
  setBranchAddress("runNumber", &runNumber, &b_runNumber);

  initGenWeights();
  initSpinVariables();
  initTopVariables(m_topQuarkDefinition); // afterFSR or beforeFSR
  initTTBarVariables(m_ttbarSystemDefinition);
  initBVariables(m_bQuarkDefinition);           // afterFSR or beforeFSR
  initWVariables(m_WBosonDefinition);           // afterFSR or beforeFSR
  initWDecayVariables(m_WBosonDecayDefinition); // afterFSR or beforeFSR
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initGenWeights()
{
  setBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initSpinVariables()
{
  setBranchAddress("spin_QEttbarExample_cos_phi_NOSYS", &spin_QEttbarExample_cos_phi, &b_spin_QEttbarExample_cos_phi);
  setBranchAddress("spin_QEttbarExample_cos_theta_helicity_m_NOSYS", &spin_QEttbarExample_cos_theta_helicity_m, &b_spin_QEttbarExample_cos_theta_helicity_m);
  setBranchAddress("spin_QEttbarExample_cos_theta_helicity_p_NOSYS", &spin_QEttbarExample_cos_theta_helicity_p, &b_spin_QEttbarExample_cos_theta_helicity_p);
  setBranchAddress("spin_QEttbarExample_cos_theta_raxis_m_NOSYS", &spin_QEttbarExample_cos_theta_raxis_m, &b_spin_QEttbarExample_cos_theta_raxis_m);
  setBranchAddress("spin_QEttbarExample_cos_theta_raxis_p_NOSYS", &spin_QEttbarExample_cos_theta_raxis_p, &b_spin_QEttbarExample_cos_theta_raxis_p);
  setBranchAddress("spin_QEttbarExample_cos_theta_transverse_m_NOSYS", &spin_QEttbarExample_cos_theta_transverse_m, &b_spin_QEttbarExample_cos_theta_transverse_m);
  setBranchAddress("spin_QEttbarExample_cos_theta_transverse_p_NOSYS", &spin_QEttbarExample_cos_theta_transverse_p, &b_spin_QEttbarExample_cos_theta_transverse_p);
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initTopVariables(const TString &whenDefined)
{
  setBranchAddress("Ttbar_MC_t_" + whenDefined + "_eta", &Ttbar_MC_t_eta, &b_Ttbar_MC_t_eta);
  setBranchAddress("Ttbar_MC_t_" + whenDefined + "_m", &Ttbar_MC_t_m, &b_Ttbar_MC_t_m);
  setBranchAddress("Ttbar_MC_t_" + whenDefined + "_pdgId", &Ttbar_MC_t_pdgId, &b_Ttbar_MC_t_pdgId);
  setBranchAddress("Ttbar_MC_t_" + whenDefined + "_phi", &Ttbar_MC_t_phi, &b_Ttbar_MC_t_phi);
  setBranchAddress("Ttbar_MC_t_" + whenDefined + "_pt", &Ttbar_MC_t_pt, &b_Ttbar_MC_t_pt);
  setBranchAddress("Ttbar_MC_tbar_" + whenDefined + "_eta", &Ttbar_MC_tbar_eta, &b_Ttbar_MC_tbar_eta);
  setBranchAddress("Ttbar_MC_tbar_" + whenDefined + "_m", &Ttbar_MC_tbar_m, &b_Ttbar_MC_tbar_m);
  setBranchAddress("Ttbar_MC_tbar_" + whenDefined + "_pdgId", &Ttbar_MC_tbar_pdgId, &b_Ttbar_MC_tbar_pdgId);
  setBranchAddress("Ttbar_MC_tbar_" + whenDefined + "_phi", &Ttbar_MC_tbar_phi, &b_Ttbar_MC_tbar_phi);
  setBranchAddress("Ttbar_MC_tbar_" + whenDefined + "_pt", &Ttbar_MC_tbar_pt, &b_Ttbar_MC_tbar_pt);
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initTTBarVariables(const TString &whenDefined)
{
  setBranchAddress("Ttbar_MC_ttbar_" + whenDefined + "_eta", &Ttbar_MC_ttbar_eta, &b_Ttbar_MC_ttbar_eta);
  setBranchAddress("Ttbar_MC_ttbar_" + whenDefined + "_m", &Ttbar_MC_ttbar_m, &b_Ttbar_MC_ttbar_m);
  setBranchAddress("Ttbar_MC_ttbar_" + whenDefined + "_phi", &Ttbar_MC_ttbar_phi, &b_Ttbar_MC_ttbar_phi);
  setBranchAddress("Ttbar_MC_ttbar_" + whenDefined + "_pt", &Ttbar_MC_ttbar_pt, &b_Ttbar_MC_ttbar_pt);
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initBVariables(const TString &whenDefined)
{
  setBranchAddress("Ttbar_MC_b_" + whenDefined + "_from_t_eta", &Ttbar_MC_b_from_t_eta, &b_Ttbar_MC_b_from_t_eta);
  setBranchAddress("Ttbar_MC_b_" + whenDefined + "_from_t_m", &Ttbar_MC_b_from_t_m, &b_Ttbar_MC_b_from_t_m);
  setBranchAddress("Ttbar_MC_b_" + whenDefined + "_from_t_pdgId", &Ttbar_MC_b_from_t_pdgId, &b_Ttbar_MC_b_from_t_pdgId);
  setBranchAddress("Ttbar_MC_b_" + whenDefined + "_from_t_phi", &Ttbar_MC_b_from_t_phi, &b_Ttbar_MC_b_from_t_phi);
  setBranchAddress("Ttbar_MC_b_" + whenDefined + "_from_t_pt", &Ttbar_MC_b_from_t_pt, &b_Ttbar_MC_b_from_t_pt);
  setBranchAddress("Ttbar_MC_bbar_" + whenDefined + "_from_tbar_eta", &Ttbar_MC_bbar_from_tbar_eta, &b_Ttbar_MC_bbar_from_tbar_eta);
  setBranchAddress("Ttbar_MC_bbar_" + whenDefined + "_from_tbar_m", &Ttbar_MC_bbar_from_tbar_m, &b_Ttbar_MC_bbar_from_tbar_m);
  setBranchAddress("Ttbar_MC_bbar_" + whenDefined + "_from_tbar_pdgId", &Ttbar_MC_bbar_from_tbar_pdgId, &b_Ttbar_MC_bbar_from_tbar_pdgId);
  setBranchAddress("Ttbar_MC_bbar_" + whenDefined + "_from_tbar_phi", &Ttbar_MC_bbar_from_tbar_phi, &b_Ttbar_MC_bbar_from_tbar_phi);
  setBranchAddress("Ttbar_MC_bbar_" + whenDefined + "_from_tbar_pt", &Ttbar_MC_bbar_from_tbar_pt, &b_Ttbar_MC_bbar_from_tbar_pt);
}

void TruthTreeTopCPToolkit::initWVariables(const TString &whenDefined)
{
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_t_eta", &Ttbar_MC_W_from_t_eta, &b_Ttbar_MC_W_from_t_eta);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_t_m", &Ttbar_MC_W_from_t_m, &b_Ttbar_MC_W_from_t_m);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_t_pdgId", &Ttbar_MC_W_from_t_pdgId, &b_Ttbar_MC_W_from_t_pdgId);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_t_phi", &Ttbar_MC_W_from_t_phi, &b_Ttbar_MC_W_from_t_phi);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_t_pt", &Ttbar_MC_W_from_t_pt, &b_Ttbar_MC_W_from_t_pt);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_tbar_eta", &Ttbar_MC_W_from_tbar_eta, &b_Ttbar_MC_W_from_tbar_eta);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_tbar_m", &Ttbar_MC_W_from_tbar_m, &b_Ttbar_MC_W_from_tbar_m);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_tbar_pdgId", &Ttbar_MC_W_from_tbar_pdgId, &b_Ttbar_MC_W_from_tbar_pdgId);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_tbar_phi", &Ttbar_MC_W_from_tbar_phi, &b_Ttbar_MC_W_from_tbar_phi);
  setBranchAddress("Ttbar_MC_W_" + whenDefined + "_from_tbar_pt", &Ttbar_MC_W_from_tbar_pt, &b_Ttbar_MC_W_from_tbar_pt);
}

//----------------------------------------------------------------------

void TruthTreeTopCPToolkit::initWDecayVariables(const TString &whenDefined)
{
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_t_eta", &Ttbar_MC_Wdecay1_from_t_eta, &b_Ttbar_MC_Wdecay1_from_t_eta);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_t_m", &Ttbar_MC_Wdecay1_from_t_m, &b_Ttbar_MC_Wdecay1_from_t_m);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_t_pdgId", &Ttbar_MC_Wdecay1_from_t_pdgId, &b_Ttbar_MC_Wdecay1_from_t_pdgId);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_t_phi", &Ttbar_MC_Wdecay1_from_t_phi, &b_Ttbar_MC_Wdecay1_from_t_phi);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_t_pt", &Ttbar_MC_Wdecay1_from_t_pt, &b_Ttbar_MC_Wdecay1_from_t_pt);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_tbar_eta", &Ttbar_MC_Wdecay1_from_tbar_eta, &b_Ttbar_MC_Wdecay1_from_tbar_eta);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_tbar_m", &Ttbar_MC_Wdecay1_from_tbar_m, &b_Ttbar_MC_Wdecay1_from_tbar_m);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_tbar_pdgId", &Ttbar_MC_Wdecay1_from_tbar_pdgId, &b_Ttbar_MC_Wdecay1_from_tbar_pdgId);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_tbar_phi", &Ttbar_MC_Wdecay1_from_tbar_phi, &b_Ttbar_MC_Wdecay1_from_tbar_phi);
  setBranchAddress("Ttbar_MC_Wdecay1_" + whenDefined + "_from_tbar_pt", &Ttbar_MC_Wdecay1_from_tbar_pt, &b_Ttbar_MC_Wdecay1_from_tbar_pt);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_t_eta", &Ttbar_MC_Wdecay2_from_t_eta, &b_Ttbar_MC_Wdecay2_from_t_eta);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_t_m", &Ttbar_MC_Wdecay2_from_t_m, &b_Ttbar_MC_Wdecay2_from_t_m);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_t_pdgId", &Ttbar_MC_Wdecay2_from_t_pdgId, &b_Ttbar_MC_Wdecay2_from_t_pdgId);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_t_phi", &Ttbar_MC_Wdecay2_from_t_phi, &b_Ttbar_MC_Wdecay2_from_t_phi);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_t_pt", &Ttbar_MC_Wdecay2_from_t_pt, &b_Ttbar_MC_Wdecay2_from_t_pt);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_tbar_eta", &Ttbar_MC_Wdecay2_from_tbar_eta, &b_Ttbar_MC_Wdecay2_from_tbar_eta);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_tbar_m", &Ttbar_MC_Wdecay2_from_tbar_m, &b_Ttbar_MC_Wdecay2_from_tbar_m);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_tbar_pdgId", &Ttbar_MC_Wdecay2_from_tbar_pdgId, &b_Ttbar_MC_Wdecay2_from_tbar_pdgId);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_tbar_phi", &Ttbar_MC_Wdecay2_from_tbar_phi, &b_Ttbar_MC_Wdecay2_from_tbar_phi);
  setBranchAddress("Ttbar_MC_Wdecay2_" + whenDefined + "_from_tbar_pt", &Ttbar_MC_Wdecay2_from_tbar_pt, &b_Ttbar_MC_Wdecay2_from_tbar_pt);
}
