#include "TtbarDiffCrossSection/RecoLevelVariables.h"
#include "HelperFunctions/TtbarDiffCrossSection_exception.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"

#include <string>
#include <float.h>
#include "Math/VectorUtil.h"

ClassImp(RecoLevelVariables)

using ROOT::Math::VectorUtil::DeltaPhi;

RecoLevelVariables::RecoLevelVariables() : MeasuredVariablesBase::MeasuredVariablesBase() {
  
  if(!initialize()) throw TtbarDiffCrossSection_exception("Error: Failed to initialize reco level variables");

}

//----------------------------------------------------------------------

bool RecoLevelVariables::initialize() {
  
  initializeLJetVariables("LJet1");
  initializeLJetVariables("LJet2");
  initializeLJetVariables("LJet3");
  
  initializeKinematicVariables("J1_WCandidate");
  initializeKinematicVariables("J2_WCandidate");
  
  initializeKinematicVariables("J1_BCandidate");
  initializeKinematicVariables("J2_BCandidate");
  
  initializeKinematicVariables("J1sj1");
  initializeKinematicVariables("J2sj1");
  
  initializeKinematicVariables("J1st1");
  initializeKinematicVariables("J2st1");
  
  initializeRCJetVariables("rcJet1");
  initializeRCJetVariables("rcJet2");
  initializeRCJetVariables("vrcJet1");
  initializeRCJetVariables("vrcJet2");
  
  initializeKinematicVariables("rcJ1sj1");
  initializeKinematicVariables("rcJ2sj1");
  initializeKinematicVariables("vrcJ1sj1");
  initializeKinematicVariables("vrcJ2sj1");
  
  initializeKinematicVariables("ttbar");
  
  initializeKinematicVariables("jet1");
  initializeKinematicVariables("jet2");
  initializeKinematicVariables("jet3");
  initializeKinematicVariables("jet4");
  initializeKinematicVariables("jetFirst");
  
  initializeKinematicVariables("bjet1");
  initializeKinematicVariables("bjet2");
  
  initializeKinematicVariables("tjet1");
  initializeKinematicVariables("tjet2");
  
  initializeInclusiveKinematicVariables("LJets");
  initializeInclusiveKinematicVariables("jets");
  initializeInclusiveKinematicVariables("bjets");
  
  initializeInclusiveKinematicVariables("tjets");
  
  std::vector<std::string> otherVariables = {
    "LJets_Zt","LJets_Ht","LJet1_TruthLabel", "LJet2_TruthLabel", "LJet1_TopTagSF", "LJet2_TopTagSF",
    "tjet1_GhostBHadronsFinalPt","tjet2_GhostBHadronsFinalPt",
    "nJets","nBJets","nLJets","nrcJets","nvrcJets",
    "eventWeight", "MET", "ttbar_deltaphi",
    "ttbar_Ht","jets_Ht","jet1_QGTaggerNTrack","MET_over_SqrtHt","DeltaR_b1_b2","deltaphi_MET_ttbar","min_DeltaR_ljet1_bjets","min_DeltaR_ljet2_bjets",
    "J1sj1_BTagger_score", "J2sj1_BTagger_score", "J1sj_maxBTagger_score", "J2sj_maxBTagger_score", "J1sj_maxBTagger_score_index", "J2sj_maxBTagger_score_index",
    "jet1_BTagger_score", "jet2_BTagger_score", "jet3_BTagger_score", "jet4_BTagger_score",
    "J1st1_BTagger_score", "J2st1_BTagger_score", "J1st_maxBTagger_score", "J2st_maxBTagger_score", "J1st_maxBTagger_score_index", "J2st_maxBTagger_score_index",
    "tjet1_BTagger_score", "tjet2_BTagger_score"
  };
  
  std::vector<std::string> otherVectorVariables = {
    "LJets_MOverPt","tjets_GhostBHadronsFinalPt"
  };
  
  for(const std::string& variable : otherVariables) {
    if(!insertVariable(variable)) return false;
  }
  
  for(const std::string& variable : otherVectorVariables) {
    if(!insertVector(variable)) return false;
  }
  
  return true;
}

//----------------------------------------------------------------------

bool RecoLevelVariables::initializeKinematicVariables(const std::string& particle) {
  
  if(!insertVariable(particle+"_eta")) return false;
  if(!insertVariable(particle+"_phi")) return false;
  if(!insertVariable(particle+"_pt")) return false;
  if(!insertVariable(particle+"_m")) return false;
  if(!insertVariable(particle+"_e")) return false;
  if(!insertVariable(particle+"_y")) return false;
  if(!insertVariable(particle+"_y_minus_eta")) return false;
  if(!insertVariable(particle+"_y_minus_eta_ver2")) return false;
  
  return true;
}

//----------------------------------------------------------------------

bool RecoLevelVariables::initializeSubstructureVariables(const std::string& particle) {
  
  if(!insertVariable(particle+"_tau21")) return false;
  if(!insertVariable(particle+"_tau32")) return false;
  if(!insertVariable(particle+"_tau43")) return false;
  if(!insertVariable(particle+"_split12")) return false;
  if(!insertVariable(particle+"_split23")) return false;
  if(!insertVariable(particle+"_Qw")) return false;
  if(!insertVariable(particle+"_pseudoQw_2subjets")) return false;
  if(!insertVariable(particle+"_pseudoQw_3subjets")) return false;
  if(!insertVariable(particle+"_pseudoQw")) return false;
  if(!insertVariable(particle+"_NSubjets")) return false;
  
  return true;
}

//----------------------------------------------------------------------

bool RecoLevelVariables::initializeLJetVariables(const std::string& particle) {
  if(!initializeKinematicVariables(particle)) return false;
  if(!initializeSubstructureVariables(particle)) return false;
  
  if(!insertVariable(particle+"_bTag")) return false;
  if(!insertVariable(particle+"_deltaR")) return false;
  if(!insertVariable(particle+"_MOverPt")) return false;
  if(!insertVariable(particle+"_PassedRangeCheck")) return false;
  
  if(!insertVariable(particle+"_TopTagScore")) return false;
  
  return true;
}
//----------------------------------------------------------------------

bool RecoLevelVariables::initializeRCJetVariables(const std::string& particle) {
  if(!initializeKinematicVariables(particle)) return false;
  if(!initializeSubstructureVariables(particle)) return false;
  return true;
}

//----------------------------------------------------------------------

bool RecoLevelVariables::initializeInclusiveKinematicVariables(const std::string& particle) {
  
  if(!insertVector(particle+"_eta")) return false;
  if(!insertVector(particle+"_phi")) return false;
  if(!insertVector(particle+"_pt")) return false;
  if(!insertVector(particle+"_m")) return false;
  if(!insertVector(particle+"_e")) return false;
  if(!insertVector(particle+"_y")) return false;
  if(!insertVector(particle+"_y_minus_eta")) return false;
  if(!insertVector(particle+"_y_minus_eta_ver2")) return false;
  
  return true;
  
}

//----------------------------------------------------------------------

void RecoLevelVariables::calculateKinematicVariables(const ReconstructedObjects::FourMom_t& vec, const std::string& particle) {
  if (m_debug>2) std::cout << "Calculating variables for: " << particle << std::endl;
  setValue(particle+"_eta", std::abs(vec.Eta()));
  setValue(particle+"_phi", vec.Phi());
  setValue(particle+"_pt", vec.Pt());
  setValue(particle+"_m", vec.M());
  setValue(particle+"_e", vec.E());
  setValue(particle+"_y", std::abs(vec.Rapidity()));
  setValue(particle+"_y_minus_eta", vec.Rapidity()-vec.Eta());
  setValue(particle+"_y_minus_eta_ver2", vec.Rapidity()-vec.Eta());
  
}

//----------------------------------------------------------------------

void RecoLevelVariables::calculateSubstructureVariables(const Object_t& jet, const std::string& particle) {
  
  const float tau1 = jet.getAttribute<float>("Tau1");
  const float tau2 = jet.getAttribute<float>("Tau2");
  const float tau3 = jet.getAttribute<float>("Tau3");
  const float tau4 = jet.getAttribute<float>("Tau4");
  
  auto calculateSubjettinessRatio = [](const float numerator, const float denominator) {
    return denominator > 0. ? numerator / denominator : 0.;
  };
  
  setValue(particle+"_tau21", calculateSubjettinessRatio(tau2, tau1));
  setValue(particle+"_tau32", calculateSubjettinessRatio(tau3, tau2));
  setValue(particle+"_tau43", calculateSubjettinessRatio(tau4, tau3));
  setValue(particle+"_split12", jet.getAttribute<float>("Split12"));
  setValue(particle+"_split23", jet.getAttribute<float>("Split23"));
  setValue(particle+"_Qw", jet.getAttribute<float>("Qw"));
    
  const double psQw = pseudoQw(jet);
    
  const int nsubjets = jet.getConstRef<ObjectContainer_t>("MatchedSmallRJets").size();
  
  if(nsubjets==2) setValue(particle+"_pseudoQw_2subjets", psQw);
  if(nsubjets>2) setValue(particle+"_pseudoQw_3subjets", psQw);
  
  setValue(particle+"_pseudoQw", psQw);
  setValue(particle+"_NSubjets", nsubjets);
  
}

//----------------------------------------------------------------------

float RecoLevelVariables::pseudoQw(const Object_t& jet) const {
  
  const ObjectContainer_t& subjets = jet.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
    
  const int nsubjets= subjets.size();
  if(nsubjets<2) {
    return 0.;
  }
  else if (nsubjets==2) { 
    return std::max(subjets[0]->M(),subjets[1]->M());
  }
  else {
    double m12 = (*subjets[0] + *subjets[1]).M();
    double m13 = (*subjets[0] + *subjets[2]).M();
    double m23 = (*subjets[1] + *subjets[2]).M();
    return std::min(m12,std::min(m13,m23));
  }
  
}


//----------------------------------------------------------------------

void RecoLevelVariables::calculateLJetVariables(const Object_t& jet, const std::string& particle) {
  //if (m_debug>2) cout << endl << "****RecoLevelVariables::FillFatHistos  *****" << endl << "filling histograms of energy, pt, eta and phi for " << particle << endl;
  
  calculateKinematicVariables(jet,particle);
  calculateSubstructureVariables(jet,particle);
  
  setValue(particle+"_bTag", jet.getAttribute<bool>("MatchedToSmallRBJet"));
  //setValue(particle+"_deltaR", jet.getAttribute<float>("DeltaR"));
  setValue(particle+"_MOverPt", jet.M() / jet.Pt());
  setValue(particle + "_PassedRangeCheck", jet.getAttribute<bool>("TopTagPassMass") * jet.getAttribute<bool>("TopTagValidKinRange"));
  setValue(particle + "_TopTagScore", jet.getAttribute<float>("Score"));
}

//----------------------------------------------------------------------

void RecoLevelVariables::calculateRCJetVariables(const Object_t& jet, const std::string& particle) {
  //if (m_debug>2) cout << endl << "****RecoLevelVariables::FillRcHistos  *****" << endl << "filling histograms of energy, pt, eta and phi for " << particle << endl;
  calculateKinematicVariables(jet,particle);
  calculateSubstructureVariables(jet,particle);
}

//----------------------------------------------------------------------

void RecoLevelVariables::calculateInclusiveKinematicVariables(const ObjectContainer_t& vec, const std::string& particle) {
  if (m_debug>2) std::cout << "Calculating vector variables for: " << particle << std::endl;
  
  const int n = vec.size();
  m_vectorVariables[particle+"_eta"].resize(n);
  m_vectorVariables[particle+"_phi"].resize(n);
  m_vectorVariables[particle+"_pt"].resize(n);
  m_vectorVariables[particle+"_m"].resize(n);
  m_vectorVariables[particle+"_e"].resize(n);
  m_vectorVariables[particle+"_y"].resize(n);
  m_vectorVariables[particle+"_y_minus_eta"].resize(n);
  m_vectorVariables[particle+"_y_minus_eta_ver2"].resize(n);
  
  
  for(int i=0;i<n;i++) {
    m_vectorVariables[particle+"_eta"][i] = std::abs(vec[i]->Eta());
    m_vectorVariables[particle+"_phi"][i] = vec[i]->Phi();
    m_vectorVariables[particle+"_pt"][i] = vec[i]->Pt();
    m_vectorVariables[particle+"_m"][i] = vec[i]->M();
    m_vectorVariables[particle+"_e"][i] = vec[i]->E();
    m_vectorVariables[particle+"_y"][i] = std::abs(vec[i]->Rapidity());
    m_vectorVariables[particle+"_y_minus_eta"][i] = vec[i]->Rapidity()- vec[i]->Eta();
    m_vectorVariables[particle+"_y_minus_eta_ver2"][i] = vec[i]->Rapidity()-vec[i]->Eta();
  }
}

//----------------------------------------------------------------------

void RecoLevelVariables::calculateLevelVariables(const ReconstructedEvent& event) {
  if (m_debug>2) std::cout << "Reseting values" << std::endl;
  resetValues();

  if (m_debug>2) std::cout << "Getting objects" << std::endl;
  
  const Object_t& top1 = event.getConstRef<Object_t>("top1");
  const Object_t& top2 = event.getConstRef<Object_t>("top2");
  ReconstructedObjects::FourMom_t ttbar =  top1 + top2;
  
  bool useSmallRJets = event.objectCollectionIsAvailable("SmallRJets");
  bool useSmallRBJets = event.objectCollectionIsAvailable("SmallRBJets");
  bool useLargeRJets = event.objectCollectionIsAvailable("LargeRJets");
  bool useRCJets = event.objectCollectionIsAvailable("RCJets");
  bool useVarRCJets = event.objectCollectionIsAvailable("VarRCJets");
  bool useTrackJets = event.objectCollectionIsAvailable("TrackJets");


  //const ObjectContainer_t& electrons = event.getObjectCollection("Electrons");
  //const ObjectContainer_t& muons = event.getObjectCollection("Muons");

  if(top1.isAvailable("MatchedSmallRJets") && top2.isAvailable("MatchedSmallRJets")) {

    const ObjectContainer_t& top1MatchedSmallRJets = top1.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
    const ObjectContainer_t& top2MatchedSmallRJets = top2.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
        
    if(top1MatchedSmallRJets.size() > 0) {
      this->calculateKinematicVariables(*top1MatchedSmallRJets.at(0),"J1sj1");
      setValue("J1sj1_BTagger_score", top1MatchedSmallRJets[0]->getAttribute<float>("BTagger_score"));
      
      const int index_BTagger_score_max = top1.getAttribute<int>("BCandidate_index");
      const Object_t& J1_BCandidate = top1.getConstRef<Object_t>("BCandidate");
      const Object_t& J1_WCandidate = top1.getConstRef<Object_t>("WCandidate");
      
      calculateKinematicVariables(J1_WCandidate, "J1_WCandidate");
      calculateKinematicVariables(J1_BCandidate, "J1_BCandidate");
      setValue("J1sj_maxBTagger_score_index", index_BTagger_score_max);
      if(J1_BCandidate.isAvailable("BTagger_score")) {
	setValue("J1sj_maxBTagger_score", J1_BCandidate.getAttribute<float>("BTagger_score"));
      }
    }
    if(top2MatchedSmallRJets.size() > 0) {
      this->calculateKinematicVariables(*top2MatchedSmallRJets.at(0),"J2sj1");
      setValue("J2sj1_BTagger_score", top2MatchedSmallRJets[0]->getAttribute<float>("BTagger_score"));
      
      const int index_BTagger_score_max = top2.getAttribute<int>("BCandidate_index");
      const Object_t& J2_BCandidate = top2.getConstRef<Object_t>("BCandidate");
      const Object_t& J2_WCandidate = top2.getConstRef<Object_t>("WCandidate");
      
      calculateKinematicVariables(J2_WCandidate, "J2_WCandidate");      
      calculateKinematicVariables(J2_BCandidate, "J2_BCandidate");
      setValue("J2sj_maxBTagger_score_index", index_BTagger_score_max);
      if(J2_BCandidate.isAvailable("BTagger_score")) {
	setValue("J2sj_maxBTagger_score", J2_BCandidate.getAttribute<float>("BTagger_score"));
      }
    }
  }
  
  if(top1.isAvailable("MatchedTrackJets") && top2.isAvailable("MatchedTrackJets")) {
    
    const ObjectContainer_t& top1MatchedTrackJets = top1.getConstRef<ObjectContainer_t>("MatchedTrackJets");
    const ObjectContainer_t& top2MatchedTrackJets = top2.getConstRef<ObjectContainer_t>("MatchedTrackJets");
  
    if(top1MatchedTrackJets.size() > 0) {
      this->calculateKinematicVariables(*top1MatchedTrackJets.at(0),"J1st1");
      setValue("J1st1_BTagger_score", top1MatchedTrackJets[0]->getAttribute<float>("BTagger_score"));
      
      const int index_BTagger_score_max = ReconstructedObjects::getMaxAttributeIndex(top1MatchedTrackJets, "BTagger_score");
      setValue("J1st_maxBTagger_score_index", index_BTagger_score_max);
      setValue("J1st_maxBTagger_score", top1MatchedTrackJets[index_BTagger_score_max]->getAttribute<float>("BTagger_score"));
    }
    if(top2MatchedTrackJets.size() > 0) {
      this->calculateKinematicVariables(*top2MatchedTrackJets.at(0),"J2st1");
      setValue("J2st1_BTagger_score", top2MatchedTrackJets[0]->getAttribute<float>("BTagger_score"));
      
      const int index_BTagger_score_max = ReconstructedObjects::getMaxAttributeIndex(top2MatchedTrackJets, "BTagger_score");
      setValue("J2st_maxBTagger_score_index", index_BTagger_score_max);
      setValue("J2st_maxBTagger_score", top2MatchedTrackJets[index_BTagger_score_max]->getAttribute<float>("BTagger_score"));
    }
    
    // Getting ghostBHadronsFinalPt only for those track jets matched to reconstructed top-jets
    std::vector<double> tjets_ghostBHadronsFinalPt;
    for(const auto& tjet : top1MatchedTrackJets) {
      if(tjet->isAvailable("GhostBHadronsFinalPt")) {
	tjets_ghostBHadronsFinalPt.push_back(tjet->getAttribute<float>("GhostBHadronsFinalPt"));
      }
    }
    for(const auto& tjet : top2MatchedTrackJets) {
      if(tjet->isAvailable("GhostBHadronsFinalPt")) {
	tjets_ghostBHadronsFinalPt.push_back(tjet->getAttribute<float>("GhostBHadronsFinalPt"));
      }
    }
    setVector("tjets_GhostBHadronsFinalPt", tjets_ghostBHadronsFinalPt);
  
  }
  
  
  if (m_debug>2) std::cout << "Calculating variables" << std::endl;
  
  
  if (useTrackJets) {
    const ObjectContainer_t& trackJets = event.getObjectCollection("TrackJets");
    int nTrackJets=trackJets.size();
  
    if(nTrackJets > 0) {
      calculateKinematicVariables(*trackJets.at(0),"tjet1");
      if(trackJets.at(0)->isAvailable("GhostBHadronsFinalPt")) {
	setValue("tjet1_GhostBHadronsFinalPt",trackJets.at(0)->getAttribute<float>("GhostBHadronsFinalPt"));
      }
      setValue("tjet1_BTagger_score", trackJets[0]->getAttribute<float>("BTagger_score"));
    }
    if(nTrackJets > 1) {
      calculateKinematicVariables(*trackJets.at(1),"tjet2");
      if(trackJets.at(1)->isAvailable("GhostBHadronsFinalPt")) {
	setValue("tjet2_GhostBHadronsFinalPt",trackJets.at(1)->getAttribute<float>("GhostBHadronsFinalPt"));
      }
      setValue("tjet2_BTagger_score", trackJets[1]->getAttribute<float>("BTagger_score"));
    }
    calculateInclusiveKinematicVariables(trackJets, "tjets");
    
  }
  
  if(useLargeRJets) {
    const ObjectContainer_t& fatJets = event.getObjectCollection("LargeRJets");
    int nLargeRJets=fatJets.size();
  
    calculateInclusiveKinematicVariables(fatJets, "LJets");
  
    std::vector<double> ljets_MOverPt(nLargeRJets);
    for(int i=0;i<nLargeRJets;i++) ljets_MOverPt[i] = fatJets[i]->M()/fatJets[i]->Pt();
    setVector("LJets_MOverPt", ljets_MOverPt);
    
    
    
  
    if (nLargeRJets > 0) {
      this->calculateLJetVariables(*fatJets[0],"LJet1");
      if (fatJets[0]->isAvailable("TruthLabel")) {
	setValue("LJet1_TruthLabel", fatJets[0]->getAttribute<int>("TruthLabel"));
      }
      if (fatJets[0]->isAvailable("TopTagSF")) {
	setValue("LJet1_TopTagSF", fatJets[0]->getAttribute<float>("TopTagSF"));
      }
    }
    if (nLargeRJets > 1) {
      this->calculateLJetVariables(*fatJets[1],"LJet2");
      if (fatJets[1]->isAvailable("TruthLabel")) {
	setValue("LJet2_TruthLabel", fatJets[1]->getAttribute<int>("TruthLabel"));
      }
      if (fatJets[1]->isAvailable("TopTagSF")) {
	setValue("LJet2_TopTagSF", fatJets[1]->getAttribute<float>("TopTagSF"));
      }
    }
    if (nLargeRJets > 2) {
      this->calculateLJetVariables(*fatJets[2],"LJet3");
    }

    setValue("LJets_Ht", event.getAttribute<float>("ljets_Ht")/1000.);
    setValue("nLJets", nLargeRJets);  
  }
  
  if(useRCJets) {
    
    const ObjectContainer_t& rcJets  = event.getObjectCollection("RCJets");
    int nRCJets=rcJets.size();
    
    if(nRCJets > 0) {
      calculateRCJetVariables(*rcJets[0], "rcJet1");
      const ObjectContainer_t& rcJet1Subjets = rcJets[0]->getConstRef<ObjectContainer_t>("Subjets");
      calculateKinematicVariables(*rcJet1Subjets[0],"rcJ1sj1");
    }
    if(nRCJets > 1) {
      calculateRCJetVariables(*rcJets[1], "rcJet2");
      const ObjectContainer_t& rcJet2Subjets = rcJets[1]->getConstRef<ObjectContainer_t>("Subjets");
      calculateKinematicVariables(*rcJet2Subjets[0],"rcJ2sj1");
    }
    
    setValue("nrcJets", nRCJets);
  }
  
  if(useVarRCJets) {
    
    const ObjectContainer_t& vrcJets = event.getObjectCollection("VarRCJets");
    int nVarRCJets=vrcJets.size();
    
    if (nVarRCJets > 0) {
      calculateRCJetVariables(*vrcJets[0], "vrcJet1");
      const ObjectContainer_t& vrcJet1Subjets = vrcJets[0]->getConstRef<ObjectContainer_t>("Subjets");
      calculateKinematicVariables(*vrcJet1Subjets[0],"vrcJ1sj1");
    }
    if (nVarRCJets > 1) {
      calculateRCJetVariables(*vrcJets[1], "vrcJet2");
      const ObjectContainer_t& vrcJet2Subjets = vrcJets[1]->getConstRef<ObjectContainer_t>("Subjets");
      calculateKinematicVariables(*vrcJet2Subjets[0],"vrcJ2sj1");
    }
    
    setValue("nvrcJets", nVarRCJets);
  }
  

 
  if(useSmallRJets) {
    const ObjectContainer_t& jets = event.getObjectCollection("SmallRJets");
    int nJets=jets.size();
    setValue("nJets", nJets);
    
    if(nJets > 0) {
      calculateKinematicVariables(*jets[0],"jetFirst");
    }
    if(nJets > 0) {
      calculateKinematicVariables(*jets.at(0),"jet1");
      setValue("jet1_BTagger_score", jets[0]->getAttribute<float>("BTagger_score"));
    }
    if(nJets > 1) {
      calculateKinematicVariables(*jets.at(1),"jet2");
      setValue("jet2_BTagger_score", jets[1]->getAttribute<float>("BTagger_score"));
    }
    if(nJets > 2) {
      calculateKinematicVariables(*jets.at(2),"jet3");
      setValue("jet3_BTagger_score", jets[2]->getAttribute<float>("BTagger_score"));
    }
    if(nJets > 3) {
      calculateKinematicVariables(*jets.at(3),"jet4");
      setValue("jet4_BTagger_score", jets[3]->getAttribute<float>("BTagger_score"));
    }
    calculateInclusiveKinematicVariables(jets, "jets");
  }
  
  if(useSmallRBJets) {
    const ObjectContainer_t& bJets = event.getObjectCollection("SmallRBJets");
    int nBJets=bJets.size();
    setValue("nBJets", nBJets);
    calculateInclusiveKinematicVariables(bJets, "bjets");
    
    if(nBJets > 0) {
      this->calculateKinematicVariables(*bJets.at(0),"bjet1");
      std::vector <double> deltaR_ljet1_bjets;
      std::vector <double> deltaR_ljet2_bjets;
      for(const auto& bJet : bJets) {
	deltaR_ljet1_bjets.push_back(ReconstructedObjects::deltaRRapidityPhi(top1,*bJet));
	deltaR_ljet2_bjets.push_back(ReconstructedObjects::deltaRRapidityPhi(top2,*bJet));
      }
      setValue("min_DeltaR_ljet1_bjets",  *std::min_element(deltaR_ljet1_bjets.begin(), deltaR_ljet1_bjets.end()) );
      setValue("min_DeltaR_ljet2_bjets",  *std::min_element(deltaR_ljet2_bjets.begin(), deltaR_ljet2_bjets.end()) );
    }
    
    if(nBJets > 1) {
      this->calculateKinematicVariables(*bJets.at(1),"bjet2");
      setValue("DeltaR_b1_b2",  ReconstructedObjects::deltaRRapidityPhi(*bJets.at(0), *bJets.at(1)));
    }
  }

  if(m_debug > 2) std::cout << "All vector variables calculated" << std::endl;

  // Calculating other variables
  
  const float met_met = event.getAttribute<float>("met_met");

  setValue("eventWeight", event.getAttribute<double>("weight"));
  setValue("MET", met_met);
  setValue("ttbar_deltaphi", std::abs(DeltaPhi(top1,top2)));
  setValue("ttbar_Ht", (top1.Pt() + top2.Pt())/1000.);
  setValue("LJets_Zt", top2.Pt()/top1.Pt());
  setValue("jets_Ht", event.getAttribute<float>("jets_Ht"));
  //setValue("jet1_QGTaggerNTrack", event.get_jet1_QGTaggerNTrack());
  setValue("MET_over_SqrtHt", met_met / sqrt(event.getAttribute<float>("jets_Ht")));
 
  calculateKinematicVariables(ttbar, "ttbar");
  setValue("ttbar_m",getValue("ttbar_m")/1000.); // Transformed to TeV
  
  Object_t lMET(0.,0.,met_met, event.getAttribute<float>("met_phi"));

  setValue("deltaphi_MET_ttbar", std::abs(DeltaPhi(lMET,ttbar)));

}

