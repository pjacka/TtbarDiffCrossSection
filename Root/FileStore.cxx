#include "TtbarDiffCrossSection/FileStore.h"
#include "HelperFunctions/functions.h"
#include "HelperFunctions/histManipFunctions.h"
#include "NtuplesAnalysisToolsConfiguration/configFunctions.h"
#include "NtuplesAnalysisToolsConfiguration/NtuplesAnalysisToolsConfig.h"

ClassImp(FileStore)

FileStore::FileStore():m_gap("           "){	
  m_counter=0;
}


void FileStore::initialize(const TString& mainDir,int debugLevel, const TString& config_tfiles_name, const TString& config_lumi_name, const TString& config_binning_name) {
  
  m_debug=debugLevel;
  if(m_debug>0)cout << "Running FileStore::initializeBase(...)" <<  endl;
  
  
  
  m_mainDir=mainDir + "/";
  m_config_tfiles = new TEnv(config_tfiles_name);
  m_config_binning = new TEnv(config_binning_name);
  NtuplesAnalysisToolsConfig ttbarConfig{config_lumi_name};
  m_counter=0;
  
  set_ABCD16_indexes();
  set_ABCD16_folder_names();
  
  m_mc_samples_production=ttbarConfig.mc_samples_production();
  m_nominalTreeName = ttbarConfig.nominalFolderName();
  
  m_lumi = configFunctions::getLumi(ttbarConfig);
  if(m_lumi<0.)throw TtbarDiffCrossSection_exception((TString)"\n Error: Unknown mc_samples_production: " + m_mc_samples_production); 
  
  m_ttbarSF=ttbarConfig.ttbar_SF();
  
  
  if(m_debug>0)cout<< "FileStore: Running with " << m_mc_samples_production << " samples." << endl;
  
  
  load_input_tfiles();
  create_list_of_histnames();
  
  if(m_debug>0)cout << "Done." << endl;
}
void FileStore::load_input_tfiles(){
  if(m_debug>0)cout << "FileStore: Loading input TFiles " << endl;
  m_input_tfiles.clear();
  
  if(m_debug>0)cout << "FileStore: loading samples from " << m_mc_samples_production << " production." << endl;
  
  if(m_mc_samples_production=="MC16a")m_input_tfiles.push_back(new TFile(m_mainDir + m_config_tfiles->GetValue("data_filename","")));
  else if(m_mc_samples_production=="MC16c" || m_mc_samples_production=="MC16d") m_input_tfiles.push_back(new TFile(m_mainDir + m_config_tfiles->GetValue("data17_filename","")));
  else if(m_mc_samples_production=="MC16e" || m_mc_samples_production=="MC16f") m_input_tfiles.push_back(new TFile(m_mainDir + m_config_tfiles->GetValue("data18_filename","")));
  else m_input_tfiles.push_back(new TFile(m_mainDir + m_config_tfiles->GetValue("data_AllYears_filename","")));
  
  m_input_tfiles.push_back(new TFile(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename","")));
  m_input_tfiles.push_back(new TFile(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_nonallhad_filename","")));
  m_input_tfiles.push_back(new TFile(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("Wt_singletop_filename","")));
  //m_input_tfiles.push_back(new TFile(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("tChannel_singletop_filename","")));
  m_input_tfiles.push_back(new TFile(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttHWZ_filename","")));
  m_n_input_tfiles=m_input_tfiles.size();
  for(int i=0;i<m_n_input_tfiles;i++){
    if(m_input_tfiles[i]->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"\n Error in loading rootfile with from " + m_input_tfiles[i]->GetName() + 
						    "\n" + "Please check configuration of your script and whether rootfile exists "); 
      return;
    }
    if(m_debug>0)cout << m_gap+"File " << m_input_tfiles[i]->GetName() << " loaded." << std::endl;
  }
  if(m_debug>0)cout << m_gap+"Done." << endl;
}
void FileStore::load_special_tfiles(){
  if(m_debug>0)cout << "FileStore: Loading special TFiles " << endl;
  
  m_special_tfiles["nominal"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename",""));
  m_special_tfiles["ME"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename",""));
  m_special_tfiles["PhPy8MECoff"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename",""));
  m_special_tfiles["PhH704"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename",""));
  m_special_tfiles["PhH713"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename",""));
  m_special_tfiles["hdamp"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename",""));
  
  //m_special_tfiles["Sherpa"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_sherpa_filename",""));// Not available at the moment


  //m_special_tfiles["eft_merged"] = std::make_unique<TFile>(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("eft_merged_filename",""));

  //m_special_tfiles["eft_507564"] = std::make_unique<TFile>(m_mainDir + "MC16a" + "." + m_config_tfiles->GetValue("eft_507564_filename",""));
  //m_special_tfiles["eft_507565"] = std::make_unique<TFile>(m_mainDir + "MC16a" + "." + m_config_tfiles->GetValue("eft_507565_filename",""));
  //m_special_tfiles["eft_507566"] = std::make_unique<TFile>(m_mainDir + "MC16a" + "." + m_config_tfiles->GetValue("eft_507566_filename",""));
  //m_special_tfiles["eft_507567"] = std::make_unique<TFile>(m_mainDir + "MC16a" + "." + m_config_tfiles->GetValue("eft_507567_filename",""));

  m_n_special_tfiles=m_special_tfiles.size();
  for(auto it=m_special_tfiles.begin();it!=m_special_tfiles.end();it++){
    if(it->second->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"\n Error in loading rootfile with from " + it->second->GetName() + 
							  "\n" + "Please check configuration of your script and whether rootfile exists ");
    }
    //if(print_info)cout << m_gap << "File: " << it->second->GetName() << " with nickname " << it->first << " loaded properly." << endl;
  }
  if(m_debug>0)cout << m_gap+"Done." << endl;
}
void FileStore::load_stress_test_tfiles(){
  if(m_debug>0)cout << "FileStore: Loading special TFiles " << endl;
  //Loading stress test files
  string signal_filename=(m_mainDir + m_mc_samples_production + "." + m_config_tfiles->GetValue("ttbar_signal_nominal_filename","")).Data();
  std::size_t found = signal_filename.find_last_of("/\\");
  TString path = signal_filename.substr(0,found).c_str();
  TString file = signal_filename.substr(found+1).c_str();
  
  for(int i=1;i<=32;i++){
    string s=to_string(i); 
    m_special_tfiles[(TString)"Signal_reweighted"+s.c_str()]= std::make_unique<TFile>(path+ "_reweighted" + s.c_str() + "/" + file);
    
  }
  
  m_n_special_tfiles=m_special_tfiles.size();
  for(auto it=m_special_tfiles.begin();it!=m_special_tfiles.end();it++){
    if(it->second->IsZombie()){
      throw TtbarDiffCrossSection_exception((TString)"\n Error in loading rootfile with from " + it->second->GetName() + 
							  "\n" + "Please check configuration of your script and whether rootfile exists ");
    }
  }
  if(m_debug>0)cout << m_gap+"Done." << endl;
}
void FileStore::reload_input_tfiles(){
  for(int i=0;i<m_n_input_tfiles;i++){
    delete m_input_tfiles[i];
  }
  load_input_tfiles();
	
}
void FileStore::create_list_of_histnames(){
  gDirectory->cd((TString)m_input_tfiles[0]->GetName() + ":" + m_nominalTreeName + "/" + m_ABCD16_folder_names[15] + "/unfolding" );
  functions::GetListOfTH1DNames(m_list_of_histnames);
  if(m_debug>1) for(unsigned i=0;i<m_list_of_histnames.size();i++) cout<< m_gap << m_list_of_histnames[i] << endl;
}
void FileStore::set_ABCD16_indexes(){
  m_iRegionA=0;
  m_iRegionB=1;
  m_iRegionC=2;
  m_iRegionD=3;
  m_iRegionE=4;
  m_iRegionF=5;
  m_iRegionG=6;
  m_iRegionH=7;
  m_iRegionI=8;
  m_iRegionJ=9;
  m_iRegionK=10;
  m_iRegionL=11;
  m_iRegionM=12;
  m_iRegionN=13;
  m_iRegionO=14;
  m_iRegionP=15; 	
}
void FileStore::set_ABCD16_folder_names(){
  m_ABCD16_folder_names.resize(16);
  m_ABCD16_folder_names[m_iRegionA]="Leading_0t_0b_Recoil_0t_0b_tight";
  m_ABCD16_folder_names[m_iRegionB]="Leading_0t_0b_Recoil_0t_1b_tight";
  m_ABCD16_folder_names[m_iRegionC]="Leading_1t_0b_Recoil_0t_0b_tight";
  m_ABCD16_folder_names[m_iRegionD]="Leading_1t_0b_Recoil_0t_1b_tight";
  m_ABCD16_folder_names[m_iRegionE]="Leading_0t_0b_Recoil_1t_0b_tight";
  m_ABCD16_folder_names[m_iRegionF]="Leading_1t_0b_Recoil_1t_0b_tight";
  m_ABCD16_folder_names[m_iRegionG]="Leading_0t_1b_Recoil_1t_0b_tight";
  m_ABCD16_folder_names[m_iRegionH]="Leading_0t_1b_Recoil_0t_1b_tight";
  m_ABCD16_folder_names[m_iRegionI]="Leading_0t_1b_Recoil_0t_0b_tight";
  m_ABCD16_folder_names[m_iRegionJ]="Leading_0t_0b_Recoil_1t_1b_tight";
  m_ABCD16_folder_names[m_iRegionK]="Leading_1t_0b_Recoil_1t_1b_tight";
  m_ABCD16_folder_names[m_iRegionL]="Leading_0t_1b_Recoil_1t_1b_tight";
  m_ABCD16_folder_names[m_iRegionM]="Leading_1t_1b_Recoil_1t_0b_tight";
  m_ABCD16_folder_names[m_iRegionN]="Leading_1t_1b_Recoil_0t_1b_tight";
  m_ABCD16_folder_names[m_iRegionO]="Leading_1t_1b_Recoil_0t_0b_tight";
  m_ABCD16_folder_names[m_iRegionP]="Leading_1t_1b_Recoil_1t_1b_tight";
}



void FileStore::get_16regions_BootstrapHistos(vector<TH1DBootstrap*>& hist_data,vector<TH1DBootstrap*>& hist_MC, const TString& chain_name, const TString& level, const TString& histname){
  if(!get_16regions_BootstrapHistos(m_input_tfiles[0],hist_data,chain_name,level,histname,false)) throw TtbarDiffCrossSection_exception("Error: Failed to load Bootstrap histograms from data");
  get_16regions_BootstrapHistos_MC_only(hist_MC, chain_name, level, histname);
}
void FileStore::get_16regions_BootstrapHistos_MC_only(vector<TH1DBootstrap*>& hist_MC, const TString& chain_name, const TString& level, const TString& histname){
  if(!get_16regions_BootstrapHistos(m_input_tfiles[1],hist_MC,chain_name,level,histname))throw TtbarDiffCrossSection_exception("Error: Failed to load bootstrap histograms from MC");
  for(int i=0;i<15;i++)hist_MC[i]->Scale(m_ttbarSF);
  vector<TH1DBootstrap*> histos;
  for(int ifile=2;ifile<m_n_input_tfiles;ifile++){
    if(!get_16regions_BootstrapHistos(m_input_tfiles[ifile],histos,chain_name,level,histname)){throw TtbarDiffCrossSection_exception("Error: Failed to load bootstrap histograms from MC");}
    for(int i=0;i<15;i++) {
      if(ifile==2)histos[i]->Scale(m_ttbarSF);
      hist_MC[i]->Add(histos[i]);
      delete histos[i];
    }
    histos.clear();
  }
}

bool FileStore::get_16regions_BootstrapHistos(TFile* f,vector<TH1DBootstrap*>& histos, const TString& chain_name, const TString& level, const TString& histname,const bool use_lumi){
  TString name;
  TString path;
  TString filename;
  histos.resize(15);
  for(int i=0;i<15;i++){
    path = chain_name + "/" + m_ABCD16_folder_names[i] + "/" + level;
    name = path + "/" +histname;
    filename=f->GetName();
    histos[i]=(TH1DBootstrap*)f->Get(name);
    if(use_lumi)histos[i]->Scale(m_lumi);
  }
  return true;
  
  
}

TH1* FileStore::getHistogramObject(int ifile, const TString& chain_name, const TString& level, const TString& histname,const bool use_lumi){
  return getHistogramObject(m_input_tfiles[ifile],chain_name,level,histname,use_lumi);
}

TH1* FileStore::getHistogramObject(TFile* file, const TString& chain_name, const TString& level, const TString& histname,const bool use_lumi){
  if(m_debug>2) file->Print();
  TH1* h= (TH1D*)file->Get(chain_name + "/" + m_ABCD16_folder_names[15] + "/" + level+"/" + histname);
  if(use_lumi)h->Scale(m_lumi);
  return h;
}

TObject* FileStore::getObject(TFile* file, const TString& chain_name, const TString& level, const TString& histname){
  return file->Get(chain_name + "/" + m_ABCD16_folder_names[15] + "/" + level+"/" + histname);
}

TH1DBootstrap* FileStore::getBootstrapHistogram(int ifile, const TString& chain_name, const TString& level, const TString& histname,const bool use_lumi){
  return getBootstrapHistogram(m_input_tfiles[ifile],chain_name,level,histname,use_lumi);
}

TH1* FileStore::getSpecialHistogram(TString filename, const TString& chain_name, const TString& level, const TString& histname,const bool use_lumi){
  if(m_special_tfiles.find(filename)==m_special_tfiles.end())throw TtbarDiffCrossSection_exception((TString)"Error In FileStore::getSpecialHistogram: Unknown filename: " + filename + ".");
  return getHistogramObject(m_special_tfiles[filename].get(),chain_name,level,histname,use_lumi);
}

TH1DBootstrap* FileStore::getBootstrapHistogram(TFile* file, const TString& chain_name, const TString& level, const TString& histname,const bool use_lumi){
  TH1DBootstrap* h= (TH1DBootstrap*)file->Get(chain_name + "/" + m_ABCD16_folder_names[15] + "/" + level+"/" + histname);
  if(use_lumi)h->Scale(m_lumi);
  return h;
}

void FileStore::prepare_list_of_systematics(){
  if(m_debug>0) cout << "FileStore: Preparing list of systematics." << endl;
  int i=0;
  functions::GetListOfNames(m_sys_chain_names_all,m_input_tfiles[1],TDirectory::Class());
 

  auto it = find (m_sys_chain_names_all.begin(), m_sys_chain_names_all.end(), m_nominalTreeName);
  if (it != m_sys_chain_names_all.end())
    m_sys_chain_names_all.erase(it); 
 

  functions::classifySysNames(m_sys_chain_names_all,m_sys_chain_names_paired,m_sys_chain_names_single,m_sys_chain_names_pdf);
  
  make_list_of_reweighted_chains();
  
  m_nsys_all=m_sys_chain_names_all.size();
  m_nsys_pairs=m_sys_chain_names_paired.size();
  m_nsys_single=m_sys_chain_names_single.size();
  m_nsys_pdf=m_sys_chain_names_pdf.size();
  
  m_sys_chain_names_all.clear();
  for(int i=0;i<m_nsys_pairs;i++){
    m_sys_chain_names_all.push_back(m_sys_chain_names_paired[i].first);
    m_sys_chain_names_all.push_back(m_sys_chain_names_paired[i].second);
  }
  for(int i=0;i<m_nsys_single;i++)m_sys_chain_names_all.push_back(m_sys_chain_names_single[i]);
  for(int i=0;i<m_nsys_pdf;i++)m_sys_chain_names_all.push_back(m_sys_chain_names_pdf[i]);
  
  for(const TString & reweightedChain : m_reweighted_chain_names) m_sys_chain_names_all.push_back(reweightedChain);
  
  if(m_debug>1){
    cout << m_gap + "Info: Printing list of all systematics." << endl;
    for(i=0;i<m_nsys_all;i++) cout << m_gap  << m_sys_chain_names_all[i] << endl;
  
    cout << m_gap + "Info: Printing list of all paired systematics." << endl;
    for(i=0;i<m_nsys_pairs;i++) cout << m_gap  << m_sys_chain_names_paired[i].first << " " << m_sys_chain_names_paired[i].second << endl;
    
    cout << m_gap + "Info: Printing list of all not-paired systematics." << endl;
    for(i=0;i<m_nsys_single;i++) cout << m_gap  << m_sys_chain_names_single[i] << endl;
    
    cout << m_gap + "Info: Printing list of all pdf systematics." << endl;
    for(i=0;i<m_nsys_pdf;i++) cout << m_gap  << m_sys_chain_names_pdf[i] << endl;
    
  }
  if(m_debug>0) cout << m_gap << "Done." << endl;
}
void FileStore::get_list_of_systematics(vector<TString>& all,vector<pair<TString,TString> >& paired,vector<TString>& single,vector<TString>& pdf){
  all=m_sys_chain_names_all;
  paired=m_sys_chain_names_paired;
  single=m_sys_chain_names_single;
  pdf=m_sys_chain_names_pdf;
}

void FileStore::make_list_of_reweighted_chains(){
  m_reweighted_chain_names = functions::getReweightedTrees(m_sys_chain_names_all);
  m_nreweighted_chains=m_reweighted_chain_names.size();
}
