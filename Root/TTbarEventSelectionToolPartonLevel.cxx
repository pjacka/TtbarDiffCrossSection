#include "TtbarDiffCrossSection/TTbarEventSelectionToolPartonLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventSelectionToolPartonLevel)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventSelectionToolPartonLevel::TTbarEventSelectionToolPartonLevel(const std::string& name) : EventSelectionToolBase(name) {
  
  m_cutflow=nullptr;
  m_cutflow_noweights=nullptr;
  m_top1PtMin=0.;
  m_top2PtMin=0.;
  
  declareProperty("cutflow",m_cutflow);
  declareProperty("cutflowNoWeights",m_cutflow_noweights);  
}

//----------------------------------------------------------------------

StatusCode TTbarEventSelectionToolPartonLevel::readJSONConfig(const nlohmann::json& json) {
  
  try {
    
    m_top1PtMin=jsonFunctions::readValueJSON<double>(json,"top1PtMin");
    m_top2PtMin=jsonFunctions::readValueJSON<double>(json,"top2PtMin");

  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventSelectionToolPartonLevel::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

bool TTbarEventSelectionToolPartonLevel::execute(const ReconstructedEvent& event, bool fillCutflow) const {

  const double weight = fillCutflow ? event.getAttribute<double>("weight") : 1.;

  const Object_t& top1 = event.getConstRef<Object_t>("top1");
  const Object_t& top2 = event.getConstRef<Object_t>("top2");
  
  if(top2.Pt() < m_top2PtMin) return false;
  
  if(fillCutflow) {
    m_cutflow->Fill(2, weight);
    m_cutflow_noweights->Fill(2);
  }
  
  if(top1.Pt() < m_top1PtMin) return false;
  
  if(fillCutflow) {
    m_cutflow->Fill(3, weight);
    m_cutflow_noweights->Fill(3);
  }

  return true;
}


//----------------------------------------------------------------------

TH1D* TTbarEventSelectionToolPartonLevel::initCutflowHistogram(const std::string& name) const {
  
  std::vector<std::string> binLabels {
    "Initial",
    (std::string)"top2 pt > " + Form("%4.0f",m_top2PtMin),
    (std::string)"top1 pt > " + Form("%4.0f",m_top1PtMin),
  };
  
  TH1D* h = new TH1D(name.c_str(),"",binLabels.size(),0.5,0.5+binLabels.size());
  h->Sumw2();
  
  for(size_t i=0;i<binLabels.size();i++) h->GetXaxis()->SetBinLabel(i+1,binLabels[i].c_str());
  
  return h;
}
