#include "TtbarDiffCrossSection/TTbarEventSelectionToolBoostedAllhad.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventSelectionToolBoostedAllhad)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventSelectionToolBoostedAllhad::TTbarEventSelectionToolBoostedAllhad(const std::string& name) : EventSelectionToolBase(name) {
  
  m_cutflow = nullptr;
  m_cutflow_noweights = nullptr;
  m_useTriggers = false;
  m_leptonVetoPt = 0.;
  m_top1PtMin = 0.;
  m_top2PtMin = 0.;
  m_top1MassMin = 0.;
  m_top1MassMax = 0.;
  m_top2MassMin = 0.;
  m_top2MassMax = 0.;
  
  m_bTagDecoration = "BTaggedMatchedSmallRJets";
  
  
  declareProperty("cutflow",m_cutflow);
  declareProperty("cutflowNoWeights",m_cutflow_noweights);
}

//----------------------------------------------------------------------

StatusCode TTbarEventSelectionToolBoostedAllhad::readJSONConfig(const nlohmann::json& json) {
  
  try {
    
    m_leptonVetoPt = jsonFunctions::readValueJSON<double>(json, "leptonVetoPt");
    m_top1PtMin = jsonFunctions::readValueJSON<double>(json, "top1PtMin");
    m_top2PtMin = jsonFunctions::readValueJSON<double>(json, "top2PtMin");
    m_top1MassMin = jsonFunctions::readValueJSON<double>(json, "top1MassMin");
    m_top1MassMax = jsonFunctions::readValueJSON<double>(json, "top1MassMax");
    m_top2MassMin = jsonFunctions::readValueJSON<double>(json, "top2MassMin");
    m_top2MassMax = jsonFunctions::readValueJSON<double>(json, "top2MassMax");
    m_bTagDecoration = jsonFunctions::readValueJSON<std::string>(json, "BTagDecoration");
    m_topTagDecoration = jsonFunctions::readValueJSON<std::string>(json, "TopTagDecoration");
    m_useTriggers = jsonFunctions::readValueJSON<bool>(json, "useTriggers");
    
    const nlohmann::json triggersJSON = jsonFunctions::readValueJSON<nlohmann::json>(json, "triggers");
    
    for (auto& [key, val] : triggersJSON.items()) {
       m_triggersMap[key] = jsonFunctions::readArrayJSON<std::string>(triggersJSON, key);
    }
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventSelectionToolBoostedAllhad::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

bool TTbarEventSelectionToolBoostedAllhad::execute(const ReconstructedEvent& event, bool fillCutflow) const {
 
  const double weight = fillCutflow ? event.getAttribute<double>("weight") : 1.;
 

  // Lepton veto cuts ************************************
  const ObjectContainer_t& electrons = event.getObjectCollection("Electrons");
  const ObjectContainer_t& muons = event.getObjectCollection("Muons");
  
  
  if(electrons.size() > 0 && electrons[0]->Pt() > m_leptonVetoPt) return false;
  if(muons.size() > 0 && muons[0]->Pt() > m_leptonVetoPt) return false;

  if(fillCutflow) {
    m_cutflow->Fill(2,weight);
    m_cutflow_noweights->Fill(2);
  }

  if (m_useTriggers) {

    
    //const UInt_t runNumber = event.getAttribute<UInt_t>("RunNumber");
    
    bool passed = passedTrigger(event);
    if (!passed) return false;

    if(fillCutflow) {
      m_cutflow->Fill(3,weight);
      m_cutflow_noweights->Fill(3);
    }

  } //end if useTrigeers

  int tr = m_useTriggers ? 1 : 0;
  
  const Object_t* leadingJet = 0;
  const Object_t* recoilJet = 0;
  
  //if(recoilJet->Pt() < m_top2PtMin) {
    //std::cout << "Checking top2 pt cut:" << std::endl;
    //std::cout << "Pt: " << recoilJet->Pt() << " Cut value: " << m_top2PtMin << std::endl << " Range check: " << recoilJet->getAttribute<bool>("PassedRangeCheck") << std::endl; 
    //std::cout << (recoilJet->Pt() < m_top2PtMin) << " " << !recoilJet->getAttribute<bool>("PassedRangeCheck") << std::endl;
  //}
  
  if (!event.isAvailable("top2")) {
    //std::cout << "Top2 not available" << std::endl;
    return false;
  }
  recoilJet = &event.getConstRef<Object_t>("top2");
  if (
    (recoilJet->Pt() < m_top2PtMin) ||
    (!recoilJet->getAttribute<bool>("TopTagPassMass")) ||
    (!recoilJet->getAttribute<bool>("TopTagValidKinRange"))
  ) return false;
  
  
  
  
  //if(recoilJet->Pt() < m_top2PtMin) std::cout  << "Cut passed" << std::endl;
  
  if(fillCutflow) {
    m_cutflow->Fill(3+tr,weight);
    m_cutflow_noweights->Fill(3+tr);
  }

  if(!event.isAvailable("top1")) return false;
  leadingJet = &event.getConstRef<Object_t>("top1");
  if (
    (leadingJet->Pt() < m_top1PtMin) ||
    !leadingJet->getAttribute<bool>("TopTagPassMass") ||
    !leadingJet->getAttribute<bool>("TopTagValidKinRange")
  ) return false;
    
  if(fillCutflow) {
    m_cutflow->Fill(4+tr,weight);
    m_cutflow_noweights->Fill(4.+tr);
  }

  if((leadingJet->M() < m_top1MassMin) || (leadingJet->M() > m_top1MassMax)) {
    return false;
  }
  if(fillCutflow) {
    m_cutflow->Fill(5+tr,weight);
    m_cutflow_noweights->Fill(5+tr);
  }

  if((recoilJet->M() < m_top2MassMin) || (recoilJet->M() > m_top2MassMax)) return false;
  
  if(fillCutflow) {
    bool leadingBTagged = leadingJet->getAttribute<bool>(m_bTagDecoration);
    bool recoilBTagged = recoilJet->getAttribute<bool>(m_bTagDecoration);
    bool leadingTopTagged = leadingJet->getAttribute<bool>(m_topTagDecoration);
    bool recoilTopTagged = recoilJet->getAttribute<bool>(m_topTagDecoration);
    
    m_cutflow->Fill(6+tr,weight);
    m_cutflow_noweights->Fill(6+tr);
    
    if(leadingBTagged) {
      m_cutflow->Fill(7+tr,weight);
      m_cutflow_noweights->Fill(7+tr);
      if(recoilBTagged) {
        m_cutflow->Fill(8+tr,weight);
        m_cutflow_noweights->Fill(8+tr);
        if(leadingTopTagged) {
          if(recoilTopTagged) {
            m_cutflow->Fill(12+tr,weight);//1t1t
            m_cutflow_noweights->Fill(12+tr);
          }
          else {
            m_cutflow->Fill(10+tr,weight);//1t0t
            m_cutflow_noweights->Fill(10+tr);
          }
        }
        else {
          if(recoilTopTagged){
            m_cutflow->Fill(11+tr,weight);//0t1t
            m_cutflow_noweights->Fill(11+tr);
          }
          else{
            m_cutflow->Fill(9+tr,weight);//0t0t
            m_cutflow_noweights->Fill(9+tr);
          }
        }
      }
    }
  }
  
  return true;
}


//----------------------------------------------------------------------

bool TTbarEventSelectionToolBoostedAllhad::passedTrigger(const ReconstructedEvent& event) const {
  
  std::string year = std::to_string(event.getAttribute<int>("year"));
  std::cout << "Identified year: " << year << std::endl;

  if (!m_triggersMap.contains(year)) {
    throw std::out_of_range("TTbarEventSelectionToolBoostedAllhad::passedTrigger: year: " + year + " is not defined.");
  }
  bool passed = false;
  const std::vector<std::string>& triggersVec = m_triggersMap.at(year);
  for(const std::string& trigger : triggersVec) {
    const bool trigRes = event.getAttribute<bool>(trigger);
    if (trigRes) return true;
  }
  return passed;
}

//----------------------------------------------------------------------

TH1D* TTbarEventSelectionToolBoostedAllhad::initCutflowHistogram(const std::string& name) const {

   std::vector<std::string> binLabels;

   if (m_useTriggers) {

      binLabels = {
         "Initial",
         "GRID selection",
         "Trigger",
         (std::string)"ljet2 pt > " + Form("%4.0f",m_top2PtMin),
         (std::string)"ljet1 pt > " + Form("%4.0f",m_top1PtMin),
         (std::string)Form("%4.1f",m_top1MassMin) + " < ljet1 m < " + Form("%4.1f",m_top1MassMax),
         (std::string)Form("%4.1f",m_top2MassMin) + " < ljet2 m < " + Form("%4.1f",m_top2MassMax),
         "ljet1 b-matched",
         "ljet2 b-matched",
         "0t0t",
         "0t1t",
         "1t0t",
         "1t1t"};
   }
   else {
      binLabels = {
         "Initial",
         "GRID selection",
         (std::string)"ljet2 pt > " + Form("%4.0f",m_top2PtMin),
         (std::string)"ljet1 pt > " + Form("%4.0f",m_top1PtMin),
         (std::string)Form("%4.1f",m_top1MassMin) + " < ljet1 m < " + Form("%4.1f",m_top1MassMax),
         (std::string)Form("%4.1f",m_top2MassMin) + " < ljet2 m < " + Form("%4.1f",m_top2MassMax),
         "ljet1 b-matched",
         "ljet2 b-matched",
         "0t0t",
         "0t1t",
         "1t0t",
         "1t1t"};

   }
  
  TH1D* h = new TH1D(name.c_str(),"",binLabels.size(),0.5,0.5+binLabels.size());
  h->Sumw2();
  
  for(size_t i=0;i<binLabels.size();i++) h->GetXaxis()->SetBinLabel(i+1,binLabels[i].c_str());
  
  return h;
}
