#include "TtbarDiffCrossSection/TTbarToolsLoaderBoostedAllhad.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"

#include "TtbarDiffCrossSection/TTbarEventClassificationToolParticleLevel.h"
#include "TtbarDiffCrossSection/TTbarEventClassificationToolBoostedAllhad.h"
#include "TtbarDiffCrossSection/TTbarEventSelectionToolBoostedAllhad.h"
#include "TtbarDiffCrossSection/TTbarEventSelectionToolParticleLevel.h"
#include "TtbarDiffCrossSection/TTbarEventSelectionToolPartonLevel.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionLeadingTwo.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionClosestMass.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionTopPolarizationRecoLevel.h"
#include "TtbarDiffCrossSection/TTbarEventReconstructionTopPolarizationParticleLevel.h"
#include "TtbarDiffCrossSection/TTBarEventReaderReco.h"
#include "TtbarDiffCrossSection/TTBarEventReaderParticleLevel.h"

#include "NtuplesAnalysisToolsCore/EventReweightingToolCommon.h"
#include "NtuplesAnalysisToolsCore/EventWeightSFToolLoaderCommon.h"

#include "TtbarDiffCrossSection/TTbarEventReweightingTool.h"
#include "TtbarDiffCrossSection/TTbarEventReweightingToolParticleLevel.h"

#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

ClassImp(TTbarToolsLoaderBoostedAllhad)

    TTbarToolsLoaderBoostedAllhad::TTbarToolsLoaderBoostedAllhad(
        const std::string &name) : AnaToolsLoaderBase(name)
{
}

//----------------------------------------------------------------------

StatusCode TTbarToolsLoaderBoostedAllhad::readJSONConfig(
    const nlohmann::json &json)
{

  /*
   * toolsLoaderSettings contains all configurations
   */
  const nlohmann::json &toolsLoaderSettings =
      jsonFunctions::readValueJSON<nlohmann::json>(
          json,
          "toolsLoaderSettings");

  /*
   * isReco option is used to determine whether the tool is used
   * at reco (true) or particle (false) level
   */

  m_isReco = jsonFunctions::readValueJSON<bool>(
      toolsLoaderSettings,
      "isReco");

  std::string prefix = m_isReco ? "RecoLevel" : "ParticleLevel";

  /*
   * Configuration of event reader tools
   */

  m_eventReaderName = jsonFunctions::readValueJSON<std::string>(
      toolsLoaderSettings,
      "EventReaderToolName");

  if (m_eventReaderName == "TTBarEventReaderReco")
  {
    m_eventReaderTool = std::make_unique<TTBarEventReaderReco>(
        prefix + m_eventReaderName);
  }
  else if (m_eventReaderName == "TTBarEventReaderParticleLevel")
  {
    m_eventReaderTool = std::make_unique<TTBarEventReaderParticleLevel>(
        prefix + m_eventReaderName);
  }
  else
  {
    std::runtime_error(
        "Unknown event reader tool name. Check your config.");
  }

  top::check(m_eventReaderTool->readJSONConfig(
                 jsonFunctions::readValueJSON<nlohmann::json>(
                     json,
                     "eventReaderToolSettings")),
             "Failed to read event reader tool config");

  /*
   * Configuration of event reconstruction tools
   */
  m_recoToolName = jsonFunctions::readValueJSON<std::string>(
      toolsLoaderSettings,
      "RecoToolName");

  if (m_recoToolName == "LeadingTwoJets")
  {
    m_recoTool = std::make_unique<TTbarEventReconstructionLeadingTwo>(
        prefix + m_recoToolName);
  }
  else if (m_recoToolName == "ClosestMass")
  {
    m_recoTool = std::make_unique<TTbarEventReconstructionClosestMass>(
        prefix + m_recoToolName);
  }
  else if (m_recoToolName == "TopPolarizationRecoLevel")
  {
    m_recoTool = std::make_unique<TTbarEventReconstructionTopPolarizationRecoLevel>(
        prefix + m_recoToolName);
  }
  else if (m_recoToolName == "TopPolarizationParticleLevel")
  {
    m_recoTool = std::make_unique<TTbarEventReconstructionTopPolarizationParticleLevel>(
        prefix + m_recoToolName);
  }
  else
  {
    std::runtime_error(
        "Unknown reconstruction tool name. Check your config.");
  }

  top::check(
      m_recoTool->setProperty("isReco", m_isReco),
      "Failed to setup reco level reconstruction tool");

  top::check(
      m_recoTool->setProperty("debugLevel", m_debug),
      "Failed to setup reco level event reconstruction tool");

  top::check(
      m_recoTool->readJSONConfig(
          jsonFunctions::readValueJSON<nlohmann::json>(
              json,
              "eventReconstructionToolSettings")),
      "Failed to read reconstruction tool config");

  /*
   * Configuration of event selection tools
   */
  m_selectionToolName =
      jsonFunctions::readValueJSON<std::string>(
          toolsLoaderSettings,
          "SelectionToolName");

  if (m_selectionToolName == "BoostedAllhadronicEventSelector")
  {
    m_selectionTool =
        std::make_unique<TTbarEventSelectionToolBoostedAllhad>(
            prefix + m_selectionToolName);
  }
  else if (m_selectionToolName == "ParticleLevelEventSelector")
  {
    m_selectionTool =
        std::make_unique<TTbarEventSelectionToolParticleLevel>(
            prefix + m_selectionToolName);
  }
  else
  {
    std::runtime_error(
        "Unknown selection tool name. Check your config.");
  }

  top::check(
      m_selectionTool->setProperty("debugLevel", m_debug),
      "Failed to setup reco level event selection tool");
  top::check(m_selectionTool->readJSONConfig(
                 jsonFunctions::readValueJSON<nlohmann::json>(
                     json,
                     "eventSelectionToolSettings")),
             "Failed to read selection tool config");

  /*
   * Configuration of event classification tools
   */
  m_classificationToolName = jsonFunctions::readValueJSON<std::string>(
      toolsLoaderSettings,
      "ClassificationToolName");

  if (m_classificationToolName == "SixteenRegionsClassifier")
  {
    m_classificationTool =
        std::make_unique<TTbarEventClassificationToolBoostedAllhad>(
            prefix + m_classificationToolName);
  }
  else if (m_classificationToolName == "ParticleLevelClassifier")
  {
    m_classificationTool =
        std::make_unique<TTbarEventClassificationToolParticleLevel>(
            prefix + m_classificationToolName);
  }
  else
  {
    std::runtime_error(
        "Unknown selection tool name. Check your config.");
  }

  top::check(
      m_classificationTool->setProperty("debugLevel", m_debug),
      "Failed to setup reco level event classification tool");

  top::check(m_classificationTool->readJSONConfig(
                 jsonFunctions::readValueJSON<nlohmann::json>(
                     json,
                     "eventClassificationToolSettings")),
             "Failed to read reconstruction tool config");

  /*
   * Configuration of event reweigthing tools
   */
  m_reweightingToolName = jsonFunctions::readValueJSON<std::string>(
      toolsLoaderSettings,
      "ReweightingToolName");

  // Initializing reweighting tool
  if (m_reweightingToolName == "BoostedAllhadronicReweightingTool")
  {
    m_reweightingTool = std::make_unique<TTbarEventReweightingTool>(
        prefix + m_reweightingToolName);
  }
  else if (m_reweightingToolName == "ParticleLevelReweightingTool")
  {
    m_reweightingTool =
        std::make_unique<TTbarEventReweightingToolParticleLevel>(
            m_reweightingToolName);
  }
  else if (m_reweightingToolName == "CommonEventReweightingTool")
  {
    m_reweightingTool = std::make_unique<EventReweightingToolCommon>(prefix + m_reweightingToolName);
    std::string toolSFLoaderName = jsonFunctions::readValueJSON<std::string>(toolsLoaderSettings, "ReweightingToolSFLoaderName");
    if (toolSFLoaderName == "EventWeightSFToolLoaderCommon")
    {
      m_reweightingTool->setEventSFToolLoader(std::make_unique<EventWeightSFToolLoaderCommon>(prefix + toolSFLoaderName));
    }
    else
    {
      std::runtime_error(
          "Unknown toolSFLoader name provided for the reweighting tool. Check your config.");
    }
  }
  else
  {
    std::runtime_error(
        "Unknown reweighting tool name. Check your config.");
  }

  top::check(
      m_reweightingTool->setProperty("debugLevel", m_debug),
      "Failed to setup reco level event classification tool");

  top::check(m_reweightingTool->readJSONConfig(
                 jsonFunctions::readValueJSON<nlohmann::json>(
                     json,
                     "eventReweightingToolSettings")),
             "Failed to read reweighting tool config");

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarToolsLoaderBoostedAllhad::initialize()
{

  top::check(
      AnaToolsLoaderBase::initialize(),
      "Failed to initialize tools loader");

  top::check(
      m_recoTool->initialize(),
      "Failed to initialize reco level reconstruction tool");
  m_recoTool->print();

  top::check(
      m_selectionTool->initialize(),
      "Failed to initialize reco level event selection tool");
  m_selectionTool->print();

  top::check(
      m_classificationTool->initialize(),
      "Failed to initialize reco level event classification tool");
  m_classificationTool->print();

  return StatusCode::SUCCESS;
}
