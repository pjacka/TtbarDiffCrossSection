#include "TtbarDiffCrossSection/TTbarToolsLoaderPartonLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"

#include "TtbarDiffCrossSection/TTbarEventReconstructionPartonLevel.h"
#include "TtbarDiffCrossSection/TTbarEventSelectionToolPartonLevel.h"
#include "TtbarDiffCrossSection/TTbarEventReweightingToolPartonLevel.h"
#include "NtuplesAnalysisToolsCore/EventReweightingToolCommon.h"
#include "NtuplesAnalysisToolsCore/EventWeightSFToolLoaderCommon.h"
#include "TtbarDiffCrossSection/TTBarEventReaderTruth.h"

#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"

ClassImp(TTbarToolsLoaderPartonLevel)

    TTbarToolsLoaderPartonLevel::TTbarToolsLoaderPartonLevel(
        const std::string &name) : AnaToolsLoaderBase(name)
{
}

//----------------------------------------------------------------------

StatusCode TTbarToolsLoaderPartonLevel::readJSONConfig(
    const nlohmann::json &json)
{

  /*
   * toolsLoaderSettings contains all configurations
   */
  const nlohmann::json &toolsLoaderSettings = jsonFunctions::readValueJSON<nlohmann::json>(json, "toolsLoaderSettings");

  /*
   * Configuration of event reader tools
   */

  m_eventReaderName = jsonFunctions::readValueJSON<std::string>(toolsLoaderSettings, "EventReaderToolName");

  if (m_eventReaderName == "TTBarEventReaderPartonLevel")
  {
    m_eventReaderTool = std::make_unique<TTBarEventReaderTruth>(m_eventReaderName);
  }
  else
  {
    std::runtime_error(
        "Unknown event reader tool name. Check your config.");
  }

  /*
   * Configuration for reconstruction tool
   */

  std::string recoToolName = jsonFunctions::readValueJSON<std::string>(toolsLoaderSettings, "RecoToolName");

  if (recoToolName == "PartonLevelReconstructionTool")
  {
    m_recoTool = std::make_unique<TTbarEventReconstructionPartonLevel>(recoToolName);
  }
  else
  {
    std::runtime_error(
        "Unknown selection tool name. Check your config.");
  }

  top::check(
      m_recoTool->setProperty("debugLevel", m_debug),
      "Failed to setup parton level event selection tool");
  top::check(
      m_recoTool->readJSONConfig(jsonFunctions::readValueJSON<nlohmann::json>(json, "eventReconstructionToolSettings")),
      "Failed to read reconstruction tool config");

  /*
   * Configuration of event selection tools
   */
  m_selectionToolName = jsonFunctions::readValueJSON<std::string>(toolsLoaderSettings, "SelectionToolName");

  if (m_selectionToolName == "PartonLevelSelector")
  {
    m_selectionTool = std::make_unique<TTbarEventSelectionToolPartonLevel>(m_selectionToolName);
  }
  else
  {
    std::runtime_error(
        "Unknown selection tool name. Check your config.");
  }

  top::check(
      m_selectionTool->setProperty("debugLevel", m_debug),
      "Failed to setup parton level event selection tool");
  top::check(
      m_selectionTool->readJSONConfig(jsonFunctions::readValueJSON<nlohmann::json>(json, "eventSelectionToolSettings")),
      "Failed to read selection tool config");

  /*
   * Configuration of event reweigthing tools
   */
  m_reweightingToolName = jsonFunctions::readValueJSON<std::string>(toolsLoaderSettings, "ReweightingToolName");

  // Initializing reweighting tool
  if (m_reweightingToolName == "PartonLevelReweightingTool")
  {
    m_reweightingTool = std::make_unique<TTbarEventReweightingToolPartonLevel>(m_reweightingToolName);
  }
  else if (m_reweightingToolName == "CommonEventReweightingTool")
  {
    m_reweightingTool = std::make_unique<EventReweightingToolCommon>("Parton" + m_reweightingToolName);
    std::string toolSFLoaderName = jsonFunctions::readValueJSON<std::string>(toolsLoaderSettings, "ReweightingToolSFLoaderName");
    if (toolSFLoaderName == "EventWeightSFToolLoaderCommon")
    {
      m_reweightingTool->setEventSFToolLoader(std::make_unique<EventWeightSFToolLoaderCommon>("Parton" + toolSFLoaderName));
    }
    else
    {
      std::runtime_error(
          "Unknown toolSFLoader name provided for the reweighting tool. Check your config.");
    }
  }
  else
  {
    std::runtime_error(
        "Unknown reweighting tool name. Check your config.");
  }

  top::check(
      m_reweightingTool->setProperty("debugLevel", m_debug),
      "Failed to setup parton level event classification tool");

  top::check(
      m_reweightingTool->readJSONConfig(jsonFunctions::readValueJSON<nlohmann::json>(json, "eventReweightingToolSettings")),
      "Failed to read reweighting tool config");

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarToolsLoaderPartonLevel::initialize()
{

  top::check(
      AnaToolsLoaderBase::initialize(),
      "Failed to initialize tools loader");

  top::check(
      m_recoTool->initialize(),
      "Failed to initialize reco level reconstruction tool");
  m_recoTool->print();

  top::check(
      m_selectionTool->initialize(),
      "Failed to initialize parton level event selection tool");
  m_selectionTool->print();

  return StatusCode::SUCCESS;
}
