#include "TtbarDiffCrossSection/TTbarEventReweightingTool.h"
// #include "HelperFunctions/stringFunctions.h"
#include "TopEvent/EventTools.h"
#include "TSystem.h"

#include <stdexcept>

#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObject.h"

typedef ReconstructedObjects::ReconstructedObject Object_t;

ClassImp(TTbarEventReweightingTool)

    TTbarEventReweightingTool::TTbarEventReweightingTool(const std::string &name) : EventReweightingToolBase(name)
{
  m_sysName = "nominal";
  m_xsecWithKfactor = {-1.};
  m_HtFilterEfficiencyCorrectionsFile = 0;
  m_samplesWithHtFilterEfficiencyCorrection = {410429, 410431, 410444, 410445};
  m_sumWeights = -1.;
  m_lumi = 1.;
  m_lumiUnc = 0.;

  m_isSingleTop_tChannel = false;
  m_isSingleTop_WtChannel = false;
  m_is_ttWChannel = false;
  m_is_ttZChannel = false;
  m_is_ttHChannel = false;
  m_is_ttbar_nonallhad = false;

  declareProperty("sysName", m_sysName);
  declareProperty("xsecWithKfactor", m_xsecWithKfactor);
  declareProperty("sumWeights", m_sumWeights);
  declareProperty("lumi", m_lumi);
  declareProperty("lumiUnc", m_lumiUnc);
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingTool::readJSONConfig(const nlohmann::json & /*json*/)
{
  try
  {
  }
  catch (const std::runtime_error &e)
  {

    std::cout << "Error in TTbarEventReweightingTool::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingTool::initialize()
{
  top::check(EventReweightingToolBase::initialize(), m_name + ": Failed to initialize");

  top::check(m_xsecWithKfactor > 0, m_name + ": xsecWithKfactor is not set before initialization");

  // const nlohmann::json& fullJSON = m_ttbarConfig->getJSON();
  // int channelNumber = m_tree->mcChannelNumber;

  // const nlohmann::json& samplesSettings = jsonFunctions::readValueJSON<nlohmann::json>(fullJSON,"samplesSettings");
  // const nlohmann::json& samplesDSIDs = jsonFunctions::readValueJSON<nlohmann::json>(samplesSettings,"samplesDSIDs");

  // top::check(identifySample(channelNumber,samplesDSIDs),"TTbarEventReweightingTool: Failed to identify sample.");

  // const nlohmann::json& HtFilterSettings = jsonFunctions::readValueJSON<nlohmann::json>(fullJSON,"HtFilterSettings");
  // top::check(initializeEfficiencyCorrections(HtFilterSettings),"TTbarEventReweightingTool: Failed to setup filter efficiency corrections.");

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingTool::identifySample(const int channelNumber, const nlohmann::json &samplesDSIDs)
{

  // MC samples are identified according to their DSIDs

  // Signal
  // std::vector<int> signalDSIDs = jsonFunctions::readArrayJSON<int>(samplesDSIDs, "signal");	// DSIDs of signal samples, unfolding corrections and truth level histos will be filled

  // Background
  const nlohmann::json &backgroundDSIDs = jsonFunctions::readValueJSON<nlohmann::json>(samplesDSIDs, "background");

  auto checkChannel = [&channelNumber, &backgroundDSIDs](const std::string &key)
  {
    const std::vector<int> &dsids =
        jsonFunctions::readArrayJSON<int>(backgroundDSIDs, key, {});

    return (
        std::find(std::begin(dsids), std::end(dsids), channelNumber) != std::end(dsids));
  };

  m_isSingleTop_tChannel = checkChannel("SingleTopTChan");
  m_isSingleTop_WtChannel = checkChannel("WtSingleTop");
  m_is_ttWChannel = checkChannel("ttW");
  m_is_ttZChannel = checkChannel("ttZ");
  m_is_ttHChannel = checkChannel("ttH");
  m_is_ttbar_nonallhad = checkChannel("ttbarNonallhad");

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingTool::initializeEfficiencyCorrections(const nlohmann::json &HtFilterSettings)
{

  m_useHtFilterEfficiencyCorrections = jsonFunctions::readValueJSON<bool>(HtFilterSettings, "useFilterEfficienciesCorrections", true); // Activates corrections of filter-HT efficiencies when running with different weights

  if (m_useHtFilterEfficiencyCorrections)
  {

    TString name = jsonFunctions::readValueJSON<std::string>(HtFilterSettings, "filterEfficienciesCorrectionsFileName", "").c_str(); // Root file with filter-HT efficiencies corrections

    if (!name.BeginsWith("/"))
      name = (TString)gSystem->Getenv("TTBAR_PATH") + "/" + name;
    m_HtFilterEfficiencyCorrectionsFile = std::unique_ptr<TFile>{TFile::Open(name)};

    if (m_HtFilterEfficiencyCorrectionsFile == 0 || m_HtFilterEfficiencyCorrectionsFile->IsZombie())
    {
      std::cout << "Error: Failed to open file with Ht-filter efficiency corrections. " << name << std::endl;
      return StatusCode::FAILURE;
    }
  }

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReweightingTool::execute(ReconstructedEvent &event)
{
  m_sysName = event.getAttribute<std::string>("sys_name");
  bool isData = event.getAttribute<bool>("IsData");

  m_weight = 1.;

  if (!isData)
  {
    const double weight_pileup_SF = event.getAttribute<float>("weight_pileup");
    const double weight_mc = event.getAttribute<float>("weight_mc");
    const double weight_zvtx_SF = 1.; // No reweighting applied to Z-vertex position at the moment
    const double btagSF = event.getAttribute<float>("weight_bTag_SF");
    const double topTagSF = this->getTopTaggingSF(event);
    const double jvtSF = event.getAttribute<float>("weight_jvt_effSF");
    const double EW_weight_SF = 1.; // EW correction is disabled
    double lumi = m_lumi;
    this->shiftLumi(lumi);

    const double background_xsec_SF = this->getBackgroundXsecSF();

    // if (m_isPDFChain || m_isIFSRChain || m_isFSRChain) {
    // top::check((m_tree->eventNumber==m_treeTruth->eventNumber) &&
    //(m_tree->randomRunNumber==m_treeTruth->randomRunNumber),
    // m_name+": Inconsistent events in reco and truth trees chains when loading generator weights");

    // double weightSF{1.0};
    // if(m_isPDFChain) weightSF = getPDFWeight();
    // if(m_isIFSRChain) weightSF = getIFSRWeight();
    // if(m_isFSRChain) weightSF = getFSRWeight();

    // m_weight*=weightSF;

    //}

    m_weight = weight_mc * weight_pileup_SF * weight_zvtx_SF * btagSF * topTagSF * jvtSF * EW_weight_SF;
    m_weight *= lumi * m_xsecWithKfactor * background_xsec_SF / m_sumWeights; // Weights are normalized to lumi * xsec
  }

  event.setAttribute<double>("weight", m_weight);

  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

double TTbarEventReweightingTool::getBackgroundXsecSF() const
{

  // Applying uncertainties on samples cross-sections
  // Last check of hardcoded values: 5.2.2020 Petr Jačka

  double background_xsec_SF = 1;

  // 50% uncertainty for missing singletop-tchanel
  if (m_isSingleTop_tChannel)
  {
    if (m_sysName == "singletop_tchan_1up")
      background_xsec_SF = (1.0 + 9.04 / 216.99);
    if (m_sysName == "singletop_tchan_1down")
      background_xsec_SF = (1.0 - 7.71 / 216.99);
  }
  // https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SingleTopRefXsec
  if (m_isSingleTop_WtChannel)
  {
    if (m_sysName == "singletop_Wt_1up")
      background_xsec_SF = (1.0 + sqrt(1.80 * 1.80 + 3.40 * 3.40) / 71.7);
    if (m_sysName == "singletop_Wt_1down")
      background_xsec_SF = (1.0 - sqrt(1.80 * 1.80 + 3.40 * 3.40) / 71.7);
  }
  // https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGTTH
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CrossSectionNLOttV
  if (m_is_ttWChannel)
  {
    if (m_sysName == "ttW_1up")
      background_xsec_SF = (1.0 + sqrt(0.129 * 0.129 + 0.020 * 0.020 + 0.027 * 0.027));
    if (m_sysName == "ttW_1down")
      background_xsec_SF = (1.0 - sqrt(0.115 * 0.115 + 0.020 * 0.020 + 0.027 * 0.027));
  }
  if (m_is_ttZChannel)
  {
    if (m_sysName == "ttZ_1up")
      background_xsec_SF = (1.0 + sqrt(0.096 * 0.096 + 0.028 * 0.028 + 0.028 * 0.028));
    if (m_sysName == "ttZ_1down")
      background_xsec_SF = (1.0 - sqrt(0.113 * 0.113 + 0.028 * 0.028 + 0.028 * 0.028));
  }
  if (m_is_ttbar_nonallhad)
  {
    if (m_sysName == "ttbar_nonallhad_1up")
      background_xsec_SF = (1.0 + sqrt(19.77 * 19.77 + 35.06 * 35.06) / 831.76);
    if (m_sysName == "ttbar_nonallhad_1down")
      background_xsec_SF = (1.0 - sqrt(29.20 * 29.20 + 35.06 * 35.06) / 831.76);
  }
  // https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageAt13TeV#ttH_Process
  if (m_is_ttHChannel)
  {
    if (m_sysName == "ttH_1up")
      background_xsec_SF = (1.0 + sqrt(0.058 * 0.058 + 0.036 * 0.036 + 0.030 * 0.030 + 0.020 * 0.020));
    if (m_sysName == "ttH_1down")
      background_xsec_SF = (1.0 - sqrt(0.092 * 0.092 + 0.036 * 0.036 + 0.030 * 0.030 + 0.020 * 0.020));
  }

  return background_xsec_SF;
}

//----------------------------------------------------------------------

double TTbarEventReweightingTool::getTopTaggingSF(const ReconstructedEvent &event) const
{
  // The final top tagging scale factor is a product of two leading large-R jet scale factors. This reflects the current ttbar system reconstruction
  double topTaggerSF = 1.;

  if (!event.isAvailable("top1") || !event.isAvailable("top2"))
  {
    return topTaggerSF;
  }

  const Object_t &top1 = event.getConstRef<Object_t>("top1");
  const Object_t &top2 = event.getConstRef<Object_t>("top2");

  const double sf1 = top1.getAttribute<float>("TopTagSF");
  const double sf2 = top2.getAttribute<float>("TopTagSF");

  // if (!std::isfinite(sf1*sf2)) {
  // ATH_MSG_INFO("Event number: " << m_tree->eventNumber);
  // ATH_MSG_INFO("Leading jet: index: " << event.getAttribute<int>("top1Index") << " Scale factor: " << sf1 << " Truth label: " << top1.getAttribute<int>("TruthLabel") << " Pt,eta,mass: " << top1.Pt() << ", " <<  top1.Eta() << ", " << top1.M());
  // ATH_MSG_INFO("Subleading jet: index: " << event.getAttribute<int>("top2Index") << " Scale factor: " << sf2 << " Truth label: " << top2.getAttribute<int>("TruthLabel") << " Pt,eta,mass: " << top2.Pt() << ", " <<  top2.Eta() << ", " << top2.M());
  // ATH_MSG_INFO("File name: " << m_tree->fChain->GetFile()->GetName());
  //}

  topTaggerSF = std::isfinite(sf1) ? sf1 : 1.;
  topTaggerSF *= std::isfinite(sf2) ? sf2 : 1.;
  return topTaggerSF;
}

//----------------------------------------------------------------------

void TTbarEventReweightingTool::multiplyEventWeights(const double weight)
{
  m_weight *= weight;
}

// ---------------------------------------------------------------------

void TTbarEventReweightingTool::shiftLumi(double &lumi) const
{

  if (m_sysName == "luminosity_1up")
    lumi *= (1. + m_lumiUnc / 100.);
  if (m_sysName == "luminosity_1down")
    lumi *= (1. - m_lumiUnc / 100.);
}

// ---------------------------------------------------------------------

// double TTbarEventReweightingTool::getFilterEfficiencyCorrection() const {

// double filterEfficiencyCorrection = 1.;

// if (
//  !m_ttbarConfig->useFilterEfficienciesCorrections() ||
// std::find(
// std::begin(m_samplesWithHtFilterEfficiencyCorrection),
// std::end(m_samplesWithHtFilterEfficiencyCorrection),
// m_tree->mcChannelNumber
//) == std::end(m_samplesWithHtFilterEfficiencyCorrection) ||
//(!m_isIFSRChain && !m_isFSRChain && !m_isPDFChain)
//) return 1.;

// const TString histName=Form("filterEfficiencyCorrections.%i",m_tree->mcChannelNumber);
// auto h = std::unique_ptr<TH1D>{(TH1D*)m_HtFilterEfficiencyCorrectionsFile->Get(histName)};

// if(m_isIFSRChain) {
// const std::vector<int>& vec = m_ttbarConfig->getISRbranches().find(m_sysName.Data())->second;
// for(int index : vec) filterEfficiencyCorrection *= h->GetBinContent(index);
//}
// if(m_isFSRChain) {
// const int index = m_ttbarConfig->getFSRbranches().find(m_sysName.Data())->second;
// filterEfficiencyCorrection *= h->GetBinContent(index);
//}
// if(m_isPDFChain) {
// const int index = m_ttbarConfig->getPDFbranches().find(m_sysName.Data())->second;
// filterEfficiencyCorrection *= h->GetBinContent(index);
//}

// if(m_debug>2) std::cout << "Filter efficiency correction: " << filterEfficiencyCorrection << " sysName: " << m_sysName << std::endl;

// return filterEfficiencyCorrection;

//}
