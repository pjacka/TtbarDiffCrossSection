#include "TtbarDiffCrossSection/ObjectSelectionTool.h"
#include "HelperFunctions/functions.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "Math/VectorUtil.h"

#include <algorithm>

ClassImp(ObjectSelectionTool)

    ObjectSelectionTool::ObjectSelectionTool(const std::string &name) : EventReconstructionToolBase(name)
{
  m_isReco = true;

  m_largeRJetType = kLJET; // Switch between standard antikt10 jets (LJETS), reclustered jets (RCJETS) and variable-R reclustered jets (VarRCJETS)

  m_useTrackJets = true; // Activates track jets

  m_useVarRCJets = false;             // Variable-R reclustered jets will be used only if it is set to True
  m_useVarRCJetsSubstructure = false; // Activates VarRCJETS substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available

  m_useRCJets = false;             // Reclustered jets will be used only if it is set to True
  m_useRCJetsSubstructure = false; // Activates RCJets substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available

  // Default object selection cuts
  m_jet_eta_max = 2.5;
  m_jet_pt_min = 25.;

  m_ljet_eta_max = 2.0;
  m_ljet_m_min = 50.;
  m_ljet_pt_min = 250.;
  m_ljet_pt_max = 3000.;
  m_ljet_mOverPt_max = 1.0;
  m_ljet_particle_useRapidity = false;

  m_rcjet_eta_max = 2.0;
  m_rcjet_m_min = 50.;
  m_rcjet_pt_min = 250.;
  m_rcjet_pt_max = 3000.;
  m_rcjet_mOverPt_max = 1.0;

  m_vrcjet_eta_max = 2.0;
  m_vrcjet_m_min = 50.;
  m_vrcjet_pt_min = 250.;
  m_vrcjet_pt_max = 3000.;
  m_vrcjet_mOverPt_max = 1.0;

  m_el_eta_max = 2.47;
  m_el_pt_min = 25.;
  m_mu_eta_max = 2.5;
  m_mu_pt_min = 25.;

  declareProperty("isReco", m_isReco);
}

StatusCode ObjectSelectionTool::readJSONConfig(const nlohmann::json &json)
{

  try
  {

    const nlohmann::json &objectSettings = jsonFunctions::readValueJSON<nlohmann::json>(json, "objectSettings");

    // Switch between standard antikt10 jets (LJETS), reclustered jets (RCJETS) and variable-R reclustered jets (VarRCJETS)
    std::string largeRJetType = jsonFunctions::readValueJSON<std::string>(objectSettings, "largeRJetType", "LJETS");
    if (largeRJetType == "LJETS")
      m_largeRJetType = kLJET;
    else if (largeRJetType == "RCJETS")
      m_largeRJetType = kRCJET;
    else if (largeRJetType == "VarRCJETS")
      m_largeRJetType = kVarRCJET;
    else
      std::runtime_error("Error: Unknown type of large-R jet: " + largeRJetType + ", unable to run. Aborting!");

    m_useTrackJets = jsonFunctions::readValueJSON<bool>(objectSettings, "useTrackJets", m_useTrackJets); // Activates track jets

    m_useVarRCJets = jsonFunctions::readValueJSON<bool>(objectSettings, "useVarRCJets", m_useVarRCJets);                                     // Variable-R reclustered jets will be used only if it is set to True
    m_useVarRCJetsSubstructure = jsonFunctions::readValueJSON<bool>(objectSettings, "useVarRCJetsSubstructure", m_useVarRCJetsSubstructure); // Activates VarRCJETS substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available

    m_useRCJets = jsonFunctions::readValueJSON<bool>(objectSettings, "useRCJets", m_useRCJets);                                     // Reclustered jets will be used only if it is set to True
    m_useRCJetsSubstructure = jsonFunctions::readValueJSON<bool>(objectSettings, "useRCJetsSubstructure", m_useRCJetsSubstructure); // Activates RCJets substructure variables. These variables are not stored in all ntuples. This option must be set to false if they are not available

    // Reading configuration of reconstructed objects
    const nlohmann::json &objectSelectionJSON = jsonFunctions::readValueJSON<nlohmann::json>(json, "objectSelection");
    this->readObjectsSetup(objectSelectionJSON);
  }
  catch (const std::runtime_error &e)
  {

    std::cout << "Error in ObjectSelectionTool::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

void ObjectSelectionTool::readObjectsSetup(const nlohmann::json &json)
{

  const nlohmann::json &smallRJetsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "smallRJets");
  this->readSmallRJetsSetup(smallRJetsConfig);

  const nlohmann::json &smallRBJetsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "smallRBJets");
  this->readSmallRBJetsSetup(smallRBJetsConfig);

  const nlohmann::json &largeRJetsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "largeRJets");
  this->readLargeRJetsSetup(largeRJetsConfig);

  if (m_useRCJets)
  {
    const nlohmann::json &rcJetsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "rcJets");
    this->readRCJetsSetup(rcJetsConfig);
  }

  if (m_useVarRCJets)
  {
    const nlohmann::json &vrcJetsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "vrcJets");
    this->readRCJetsSetup(vrcJetsConfig);
  }

  const nlohmann::json &electronsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "electrons");
  this->readElectronsSetup(electronsConfig);

  const nlohmann::json &muonsConfig = jsonFunctions::readValueJSON<nlohmann::json>(json, "muons");
  this->readMuonsSetup(muonsConfig);
}

void ObjectSelectionTool::readSmallRJetsSetup(const nlohmann::json &json)
{
  m_jet_eta_max = jsonFunctions::readValueJSON<double>(json, "eta_max", m_jet_eta_max);
  m_jet_pt_min = jsonFunctions::readValueJSON<double>(json, "pt_min", m_jet_pt_min);
}

void ObjectSelectionTool::readSmallRBJetsSetup(const nlohmann::json &json)
{
  m_bTaggerType = jsonFunctions::readValueJSON<std::string>(json, "bTaggerType", "MinCutInteger");
  m_bTaggerAttribute = jsonFunctions::readValueJSON<std::string>(json, "bTaggerAttribute", "nGhosts_bHadron");
  if (m_bTaggerType == "MinCutInteger")
  {
    m_bTaggerMinCutInteger = jsonFunctions::readValueJSON<int>(json, "minCut", true);
    const std::string interpretation = jsonFunctions::readValueJSON<std::string>(json, "minQuantilesInterpretation", "1: do not pass any tager, 2: FixedCutBEff_90, 3: FixedCutBEff_85, 4: FixedCutBEff_77, 5: FixedCutBEff_70, 6: FixedCutBEff_65");
    std::cout << "B-tagging configuration explanation: " << interpretation << std::endl;
  }
}

void ObjectSelectionTool::readLargeRJetsSetup(const nlohmann::json &json)
{
  m_ljet_eta_max = jsonFunctions::readValueJSON<double>(json, "eta_max", m_ljet_eta_max);
  m_ljet_m_min = jsonFunctions::readValueJSON<double>(json, "m_min", m_ljet_m_min);
  m_ljet_pt_min = jsonFunctions::readValueJSON<double>(json, "pt_min", m_ljet_pt_min);
  m_ljet_pt_max = jsonFunctions::readValueJSON<double>(json, "pt_max", m_ljet_pt_max);
  m_ljet_mOverPt_max = jsonFunctions::readValueJSON<double>(json, "mOverPt_max", m_ljet_mOverPt_max);
  m_ljet_particle_useRapidity = jsonFunctions::readValueJSON<bool>(json, "ljet_particle_useRapidity", false);
}

void ObjectSelectionTool::readRCJetsSetup(const nlohmann::json &json)
{
  m_rcjet_eta_max = jsonFunctions::readValueJSON<double>(json, "eta_max", m_rcjet_eta_max);
  m_rcjet_m_min = jsonFunctions::readValueJSON<double>(json, "m_min", m_rcjet_m_min);
  m_rcjet_pt_min = jsonFunctions::readValueJSON<double>(json, "pt_min", m_rcjet_pt_min);
  m_rcjet_pt_max = jsonFunctions::readValueJSON<double>(json, "pt_max", m_rcjet_pt_max);
  m_rcjet_mOverPt_max = jsonFunctions::readValueJSON<double>(json, "mOverPt_max", m_rcjet_mOverPt_max);
}

void ObjectSelectionTool::readVarRCJetsSetup(const nlohmann::json &json)
{
  m_vrcjet_eta_max = jsonFunctions::readValueJSON<double>(json, "eta_max", m_vrcjet_eta_max);
  m_vrcjet_m_min = jsonFunctions::readValueJSON<double>(json, "m_min", m_vrcjet_m_min);
  m_vrcjet_pt_min = jsonFunctions::readValueJSON<double>(json, "pt_min", m_vrcjet_pt_min);
  m_vrcjet_pt_max = jsonFunctions::readValueJSON<double>(json, "pt_max", m_vrcjet_pt_max);
  m_vrcjet_mOverPt_max = jsonFunctions::readValueJSON<double>(json, "mOverPt_max", m_vrcjet_mOverPt_max);
}

void ObjectSelectionTool::readElectronsSetup(const nlohmann::json &json)
{
  m_el_eta_max = jsonFunctions::readValueJSON<double>(json, "eta_max", m_el_eta_max);
  m_el_pt_min = jsonFunctions::readValueJSON<double>(json, "pt_min", m_el_pt_min);
}

void ObjectSelectionTool::readMuonsSetup(const nlohmann::json &json)
{
  m_mu_eta_max = jsonFunctions::readValueJSON<double>(json, "eta_max", m_el_eta_max);
  m_mu_pt_min = jsonFunctions::readValueJSON<double>(json, "pt_min", m_el_pt_min);
}

void ObjectSelectionTool::selectSmallRJets(ReconstructedEvent &event) const
{
  if (m_debug > 5)
    std::cout << "ObjectSelectionTool: Selecting small-R jets" << std::endl;

  ObjectContainer_t &jets = event.getObjectCollection("SmallRJets");
  jets.erase(
      std::remove_if(jets.begin(), jets.end(), [this](const std::shared_ptr<Object_t> &jet) -> bool
                     { return !this->passedSmallRJetsObjectSelections(*jet); }),
      jets.end());

  std::sort(std::begin(jets), std::end(jets), [](const std::shared_ptr<Object_t> &lhs, const std::shared_ptr<Object_t> &rhs)
            { return lhs->Pt() > rhs->Pt(); });

  float jets_Ht = 0.;
  for (const auto &jet : jets)
  {
    jets_Ht += jet->Pt();
  }

  event.setAttribute<float>("jets_Ht", jets_Ht);
  event.setObjectCollection("SmallRJets", jets);
}

//----------------------------------------------------------------------

void ObjectSelectionTool::selectSmallRBJets(ReconstructedEvent &event) const
{
  if (m_debug > 5)
    std::cout << "ObjectSelectionTool: selecting small-R B-jets" << std::endl;
  ObjectContainer_t &jets = event.getObjectCollection("SmallRJets");

  bool useMinCutInteger = (m_bTaggerType == "MinCutInteger");

  for (auto &jet : jets)
  {
    bool isBTagged = useMinCutInteger ? (jet->getAttribute<int>(m_bTaggerAttribute) >= m_bTaggerMinCutInteger) : jet->getAttribute<bool>(m_bTaggerAttribute);
    jet->setAttribute<bool>("BTagged", isBTagged);
  }

  ObjectContainer_t bjets = jets;
  bjets.erase(
      std::remove_if(bjets.begin(), bjets.end(), [this](const std::shared_ptr<Object_t> &jet) -> bool
                     { return !jet->getAttribute<bool>("BTagged"); }),
      bjets.end());
  event.setObjectCollection("SmallRBJets", bjets);
}

//----------------------------------------------------------------------

void ObjectSelectionTool::selectTrackJets(ReconstructedEvent & /*event*/) const
{

  if (m_debug > 5)
    std::cout << "ObjectSelectionTool: Selecting track jets" << std::endl;
}

//----------------------------------------------------------------------

void ObjectSelectionTool::selectLargeRJets(ReconstructedEvent &event) const
{

  if (m_debug > 5)
    std::cout << "ObjectSelectionTool: Selecting large-R jets" << std::endl;

  ObjectContainer_t largeRJets = event.getObjectCollection("LargeRJets");

  largeRJets.erase(
      std::remove_if(largeRJets.begin(), largeRJets.end(), [this](const std::shared_ptr<Object_t> &jet) -> bool
                     { return !this->passedLargeRJetsObjectSelections(*jet); }),
      largeRJets.end());
  std::sort(std::begin(largeRJets), std::end(largeRJets), [](const std::shared_ptr<Object_t> &lhs, const std::shared_ptr<Object_t> &rhs)
            { return lhs->Pt() > rhs->Pt(); });

  float ljets_Ht = 0.;
  for (const auto &ljet : largeRJets)
  {
    ljets_Ht += ljet->Pt();
  }

  event.setAttribute<float>("ljets_Ht", ljets_Ht);

  const ObjectContainer_t &smallRJets = event.getObjectCollection("SmallRJets");

  for (auto &ljet : largeRJets)
  {
    const ObjectContainer_t &subjets = ReconstructedObjects::getMatchedObjects(*ljet, smallRJets);
    ljet->setPtrFromCopy<ObjectContainer_t>("MatchedSmallRJets", subjets);

    bool bTaggedMatchedJets = false;
    for (auto &jet : subjets)
    {
      if (jet->getAttribute<bool>("BTagged"))
      {
        bTaggedMatchedJets = true;
        break;
      }
    }
    ljet->setAttribute<bool>("MatchedToSmallRBJet", bTaggedMatchedJets);
  }

  if (m_isReco && m_useTrackJets)
  {
    const ObjectContainer_t &trackJets = event.getObjectCollection("TrackJets");
    for (auto &ljet : largeRJets)
    {
      const ObjectContainer_t &subjets = ReconstructedObjects::getMatchedObjects(*ljet, trackJets);
      ljet->setPtrFromCopy<ObjectContainer_t>("MatchedTrackJets", subjets);
    }
  }

  event.setObjectCollection("LargeRJets", largeRJets);
}

void ObjectSelectionTool::selectRCJets(ReconstructedEvent &event) const
{

  ObjectContainer_t rcJets;

  // const unsigned int nRCJets = t.rcjet_pt->size();

  // for (unsigned int ijet=0;ijet < nRCJets;++ijet) {

  // if (m_debug>6) {
  // std::cout << "rcjet_pt->at (" << ijet << "): " << t.rcjet_pt->at(ijet) << std::endl;
  // std::cout << "rcjet_eta->at(" << ijet << "): " << t.rcjet_eta->at(ijet) << std::endl;
  // std::cout << "rcjet_phi->at(" << ijet << "): " << t.rcjet_phi->at(ijet) << std::endl;
  //}

  // auto rcjet = std::make_shared<Object_t>(t.rcjet_pt->at(ijet)/1000.,t.rcjet_eta->at(ijet),t.rcjet_phi->at(ijet),t.rcjet_e->at(ijet)/1000.);
  // rcjet->setName(rcjet->getName() + " RCJet");
  //// Object selections
  // if (!passedRCJetsObjectSelections(t, ijet,rcjet->M())) continue;

  //// Setting subjets
  // const unsigned int nSubJets = t.rcjetsub_index->at(ijet).size();
  // auto subjets = std::make_shared<ObjectContainer_t>();

  // int nBTags = 0;

  // for (unsigned int j=0; j < nSubJets ;++j) {
  // if (m_debug>6) std::cout << "RunOverMinitree::GetObjectsFromMinitree(): rcsubjets " << ijet << " " << j << std::endl;

  // const int& subjetIndex=t.rcjetsub_index->at(ijet).at(j);

  // if (subjetIndex + 1 > (int)t.jet_pt->size()) {
  // throw std::out_of_range(Form("RCjets: subjet indexes are out of range: ijet: %i, subjet_index: %i, nSmallRJets: %i",ijet,subjetIndex,(int)t.jet_pt->size()));
  // continue; // Something is wrong with indexes
  // }

  // subjets->push_back(std::make_shared<Object_t>(t.jet_pt->at(subjetIndex)/1000.,t.jet_eta->at(subjetIndex),t.jet_phi->at(subjetIndex),t.jet_e->at(subjetIndex)/1000.));
  // subjets->back()->setName(rcjet->getName() + ": Subjet");

  // bool isBTagged = (m_isReco && t.jet_isbtagged->at(subjetIndex)) ||
  //(!m_isReco &&  t.jet_nGhosts_bHadron->at(subjetIndex)>0);

  // subjets->back()->setAttribute<bool>("BTagged",isBTagged);
  // if(m_isReco) {
  // subjets->back()->setAttribute<float>("DL1r",t.jet_DL1r->at(ijet));
  // }

  // nBTags += isBTagged;

  // if (m_debug>6) std::cout << j << ". subjet pt: " << t.jet_pt->at(subjetIndex)/1000. << std::endl;
  //}

  // rcjet->setAttribute<bool>("BTagged",nBTags > 0);
  // rcjet->setAttribute<int>("NBTags",nBTags);

  ////setting up subjets of reclustered jets
  // rcjet->setPtr<ObjectContainer_t>("Subjets", subjets);

  //// Substructure variables

  // if(m_useRCJetsSubstructure) {
  // rcjet->setAttribute<float>("Tau32",t.rcjet_tau32_clstr->at(ijet));
  // rcjet->setAttribute<float>("Tau21",t.rcjet_tau21_clstr->at(ijet));
  // rcjet->setAttribute<float>("Split12",t.rcjet_d12_clstr->at(ijet) / 1000.);
  // rcjet->setAttribute<float>("Split23",t.rcjet_d23_clstr->at(ijet) / 1000.);
  // rcjet->setAttribute<float>("Qw",t.rcjet_Qw_clstr->at(ijet) / 1000.);
  //}
  //// Top-tagging
  // rcjet->setAttribute<bool>("TopTagged",functions::TopSubstructureTagger(*rcjet));

  // rcJets.push_back(rcjet);

  // if(ijet>0 && t.rcjet_pt->at(ijet) > t.rcjet_pt->at(ijet-1)) {
  // if (m_debug>4) std::cout << "Warning: rcjets are not ordered according to pt" << std::endl;
  // std::sort(rcJets.begin(), rcJets.end(), [](const std::shared_ptr<Object_t>& a, const std::shared_ptr<Object_t>& b){ return a->Pt() > b->Pt(); });
  //}
  //}

  event.setObjectCollection("RCJets", rcJets);
}

void ObjectSelectionTool::selectVarRCJets(ReconstructedEvent &event) const
{

  ObjectContainer_t vrcJets;

  // const unsigned int nVarRCJets = t.vrcjet_pt->size();

  // for (unsigned int ijet=0;ijet<nVarRCJets;++ijet) {

  // auto vrcjet = std::make_shared<Object_t>(t.vrcjet_pt->at(ijet),t.vrcjet_eta->at(ijet),t.vrcjet_phi->at(ijet),t.vrcjet_e->at(ijet));
  // vrcjet->setName(vrcjet->getName() + " RCJet");
  // if(!passedVarRCJetsObjectSelections(t, ijet, vrcjet->M()) ) continue;

  //// Setting subjets
  // const unsigned int nSubJets = t.vrcjetsub_index->at(ijet).size();
  // auto subjets = std::make_shared<ObjectContainer_t>();

  // int nBTags = 0;

  // for (unsigned int j=0; j < nSubJets ;++j) {
  // if (m_debug>6) std::cout << "RunOverMinitree::GetObjectsFromMinitree(): rcsubjets " << ijet << " " << j << std::endl;

  // const int& subjetIndex=t.vrcjetsub_index->at(ijet).at(j);

  // if (subjetIndex + 1 > (int)t.jet_pt->size()) {
  // throw std::out_of_range(Form("VarRCjets: subjet indexes are out of range: ijet: %i, subjet_index: %i, nSmallRJets: %i",ijet,subjetIndex,(int)t.jet_pt->size()));
  // continue; // Something is wrong with indexes
  // }
  // subjets->push_back(std::make_shared<Object_t>(t.jet_pt->at(subjetIndex)/1000.,t.jet_eta->at(subjetIndex),t.jet_phi->at(subjetIndex),t.jet_e->at(subjetIndex)/1000.));

  // subjets->back()->setName(vrcjet->getName() + ": Subjet");
  // bool isBTagged = (m_isReco && t.jet_isbtagged->at(subjetIndex)) ||
  //(!m_isReco &&  t.jet_nGhosts_bHadron->at(subjetIndex)>0);

  // subjets->back()->setAttribute<bool>("BTagged",isBTagged);
  // if(m_isReco) {
  // subjets->back()->setAttribute<float>("DL1r",t.jet_DL1r->at(ijet));
  // }

  // nBTags += isBTagged;

  // if (m_debug>6) std::cout << j << ". subjet pt: " << t.jet_pt->at(subjetIndex)/1000. << std::endl;
  //}

  // vrcjet->setAttribute<bool>("BTagged",nBTags > 0);
  // vrcjet->setAttribute<int>("NBTags",nBTags);

  ////setting up subjets of reclustered jets
  // vrcjet->setPtr<ObjectContainer_t>("Subjets",subjets);

  //// Substructure variables
  // if(m_useRCJetsSubstructure) {
  // vrcjet->setAttribute<float>("Tau32",t.vrcjet_tau32_clstr->at(ijet));
  // vrcjet->setAttribute<float>("Tau21",t.vrcjet_tau21_clstr->at(ijet));
  // vrcjet->setAttribute<float>("Split12",t.vrcjet_d12_clstr->at(ijet) / 1000.);
  // vrcjet->setAttribute<float>("Split23",t.vrcjet_d23_clstr->at(ijet) / 1000.);
  // vrcjet->setAttribute<float>("Qw",t.vrcjet_Qw_clstr->at(ijet) / 1000.);
  //}
  //// Top-tagging
  // vrcjet->setAttribute<bool>("TopTagged",functions::TopSubstructureTagger(*vrcjet));

  // vrcJets.push_back(vrcjet);

  // if(ijet>0 && t.vrcjet_pt->at(ijet) > t.vrcjet_pt->at(ijet-1)) {
  // if (m_debug>4) std::cout << "Warning: vrcjets are not ordered according to pt" << std::endl;
  // std::sort(vrcJets.begin(), vrcJets.end(), [](const std::shared_ptr<Object_t>& a, const std::shared_ptr<Object_t>& b){ return a->Pt() > b->Pt(); });
  //}
  //}

  event.setObjectCollection("VarRCJets", vrcJets);
}

void ObjectSelectionTool::selectElectrons(ReconstructedEvent &event) const
{
  ObjectContainer_t electrons = event.getObjectCollection("Electrons");

  electrons.erase(
      std::remove_if(electrons.begin(), electrons.end(), [this](const std::shared_ptr<Object_t> &electron) -> bool
                     { return !this->passedElectronObjectSelections(*electron); }),
      electrons.end());
  std::sort(std::begin(electrons), std::end(electrons), [](const std::shared_ptr<Object_t> &lhs, const std::shared_ptr<Object_t> &rhs)
            { return lhs->Pt() > rhs->Pt(); });

  event.setObjectCollection("Electrons", electrons);
}

void ObjectSelectionTool::selectMuons(ReconstructedEvent &event) const
{
  ObjectContainer_t muons = event.getObjectCollection("Muons");

  muons.erase(
      std::remove_if(muons.begin(), muons.end(), [this](const std::shared_ptr<Object_t> &muon) -> bool
                     { return !this->passedMuonObjectSelections(*muon); }),
      muons.end());
  std::sort(std::begin(muons), std::end(muons), [](const std::shared_ptr<Object_t> &lhs, const std::shared_ptr<Object_t> &rhs)
            { return lhs->Pt() > rhs->Pt(); });

  event.setObjectCollection("Muons", muons);
}

void ObjectSelectionTool::execute(ReconstructedEvent &event) const
{

  selectSmallRJets(event);
  selectSmallRBJets(event);

  bool useTrackJets = (m_isReco && m_useTrackJets);
  if (useTrackJets)
  {
    selectTrackJets(event);
  }

  selectLargeRJets(event);

  if (m_useRCJets)
  {
    selectRCJets(event);
  }

  if (m_useVarRCJets)
  {
    selectVarRCJets(event);
  }

  selectElectrons(event);
  selectMuons(event);
}

// ------------------------------------------------------------------------------------------------------------------------

bool ObjectSelectionTool::passedLargeRJetsObjectSelections(const Object_t &jet) const
{

  if (m_debug > 4)
  {
    std::cout << "passedLargeRJetsObjectSelections: Printing cuts: " << std::endl;
    std::cout << "|eta| <= " << m_ljet_eta_max << std::endl;
    std::cout << "mass >= " << m_ljet_m_min << std::endl;
    std::cout << "pt >= " << m_ljet_pt_min << std::endl;
    std::cout << "pt <= " << m_ljet_pt_max << std::endl;
    std::cout << "mass/pt <= " << m_ljet_mOverPt_max << std::endl;
  }

  if (!m_isReco && m_ljet_particle_useRapidity)
  {
    if (std::abs(jet.Rapidity()) > m_ljet_eta_max)
      return false;
  }
  else
  {
    if (std::abs(jet.Eta()) > m_ljet_eta_max)
      return false;
  }
  if (jet.Pt() > m_ljet_pt_max)
    return false;
  if (jet.M() < m_ljet_m_min)
    return false;
  if (jet.Pt() < m_ljet_pt_min)
    return false;
  if (jet.M() > m_ljet_mOverPt_max * jet.Pt())
    return false;

  return true;
}

// ------------------------------------------------------------------------------------------------------------------------

bool ObjectSelectionTool::passedRCJetsObjectSelections(const Object_t &jet, float rcjet_m_tmp) const
{

  if (m_debug > 4)
  {
    std::cout << "passedRCJetsObjectSelections: Printing cuts: " << std::endl;
    std::cout << "|eta| <= " << m_rcjet_eta_max << std::endl;
    std::cout << "mass >= " << m_rcjet_m_min << std::endl;
    std::cout << "pt >= " << m_rcjet_pt_min << std::endl;
    std::cout << "pt <= " << m_rcjet_pt_max << std::endl;
    std::cout << "mass/pt <= " << m_rcjet_mOverPt_max << std::endl;
  }

  if (std::abs(jet.Eta()) > m_rcjet_eta_max)
    return false;
  if (jet.Pt() > m_rcjet_pt_max)
    return false;
  if (rcjet_m_tmp < m_rcjet_m_min)
    return false;
  if (jet.Pt() < m_rcjet_pt_min)
    return false;
  if (rcjet_m_tmp > m_rcjet_mOverPt_max * jet.Pt())
    return false;

  return true;
}

// ------------------------------------------------------------------------------------------------------------------------

bool ObjectSelectionTool::passedVarRCJetsObjectSelections(const Object_t &jet, float vrcjet_m_tmp) const
{

  if (m_debug > 4)
  {
    std::cout << "passedVarRCJetsObjectSelections: Printing cuts: " << std::endl;
    std::cout << "|eta| <= " << m_vrcjet_eta_max << std::endl;
    std::cout << "mass >= " << m_vrcjet_m_min << std::endl;
    std::cout << "pt >= " << m_vrcjet_pt_min << std::endl;
    std::cout << "pt <= " << m_vrcjet_pt_max << std::endl;
    std::cout << "mass/pt <= " << m_vrcjet_mOverPt_max << std::endl;
  }

  if (std::abs(jet.Eta()) > m_vrcjet_eta_max)
    return false;
  if (jet.Pt() > m_vrcjet_pt_max)
    return false;
  if (vrcjet_m_tmp < m_vrcjet_m_min)
    return false;
  if (jet.Pt() < m_vrcjet_pt_min)
    return false;
  if (vrcjet_m_tmp > m_vrcjet_mOverPt_max * jet.Pt())
    return false;

  return true;
}

// ------------------------------------------------------------------------------------------------------------------------

bool ObjectSelectionTool::passedSmallRJetsObjectSelections(const Object_t &jet) const
{
  if (m_debug > 4)
  {
    std::cout << "passedSmallRJetsObjectSelections: Printing cuts: " << std::endl;
    std::cout << "|eta| <= " << m_jet_eta_max << std::endl;
    std::cout << "pt >= " << m_jet_pt_min << std::endl;
  }
  if (jet.Pt() < m_jet_pt_min || std::abs(jet.Eta()) > m_jet_eta_max)
    return false;
  return true;
}
// ------------------------------------------------------------------------------------------------------------------------

bool ObjectSelectionTool::passedElectronObjectSelections(const Object_t &electron) const
{
  if (m_debug > 4)
  {
    std::cout << "passedElectronObjectSelections: Printing cuts: " << std::endl;
    std::cout << "|eta| <= " << m_el_eta_max << std::endl;
    std::cout << "pt >= " << m_el_pt_min << std::endl;
  }
  if (electron.Pt() < m_el_pt_min || std::abs(electron.Eta()) > m_el_eta_max)
    return false;
  return true;
}

// ------------------------------------------------------------------------------------------------------------------------

bool ObjectSelectionTool::passedMuonObjectSelections(const Object_t &muon) const
{
  if (m_debug > 4)
  {
    std::cout << "passedMuonObjectSelections: Printing cuts: " << std::endl;
    std::cout << "|eta| <= " << m_mu_eta_max << std::endl;
    std::cout << "pt >= " << m_mu_pt_min << std::endl;
  }
  if (muon.Pt() < m_mu_pt_min || std::abs(muon.Eta()) > m_mu_eta_max)
    return false;
  return true;
}
