#include "TtbarDiffCrossSection/TTbarEventReconstructionLeadingTwo.h"
#include "TopEvent/EventTools.h"

#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"


ClassImp(TTbarEventReconstructionLeadingTwo)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventReconstructionLeadingTwo::TTbarEventReconstructionLeadingTwo(const std::string& name) : ObjectSelectionTool(name)
{
  m_minMassCut=0.;
}

//----------------------------------------------------------------------

StatusCode TTbarEventReconstructionLeadingTwo::readJSONConfig(const nlohmann::json& json) {

  try {
    
    m_minMassCut = jsonFunctions::readValueJSON<double>(json,"minMassCut");
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTTbarEventReconstructionLeadingTwo::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventReconstructionLeadingTwo::execute(ReconstructedEvent& event) const {
  // Executing object selection algorithm
  ObjectSelectionTool::execute(event);
  
  // Selecting top candidates from the set of large-R jets
  std::string collectionName = "";
  if (m_largeRJetType == kLJET) collectionName = "LargeRJets";
  else if (m_largeRJetType == kRCJET) collectionName = "RCJets";
  else collectionName = "VarRCJets";
  const auto& largeRJets = event.getObjectCollection(collectionName);
  
  int top1Index(-1);
  int top2Index(-1);
  
  for(size_t i=0;i<largeRJets.size();i++) {
    if(largeRJets[i]->M()>m_minMassCut) {
      if(top1Index<0) {
	top1Index=i;
	continue;
      }
      if(top2Index<0) {
	top2Index=i;
	continue;
      }
    }
  }
  
  event.setAttribute<int>("top1Index",top1Index);
  event.setAttribute<int>("top2Index",top2Index);
  event.setPtr<Object_t>("top1",largeRJets[top1Index]);
  event.setPtr<Object_t>("top2",largeRJets[top2Index]);
  
}



