#include "TtbarDiffCrossSection/TTbarEventReconstructionTopPolarizationRecoLevel.h"
#include "NtuplesAnalysisToolsConfiguration/jsonFunctions.h"
#include "NtuplesAnalysisToolsCore/ReconstructedObjectUtils.h"
#include "TopEvent/EventTools.h"
ClassImp(TTbarEventReconstructionTopPolarizationRecoLevel)

typedef ReconstructedObjects::ReconstructedObject Object_t;

TTbarEventReconstructionTopPolarizationRecoLevel::TTbarEventReconstructionTopPolarizationRecoLevel(const std::string& name) : TTbarEventReconstructionClosestMass(name)
{
  m_minSubjetPtFrac = 0.;
  
}

//----------------------------------------------------------------------

StatusCode TTbarEventReconstructionTopPolarizationRecoLevel::readJSONConfig(const nlohmann::json& json) {
  
  try {
    if(!TTbarEventReconstructionClosestMass::readJSONConfig(json)) {
      throw std::runtime_error("TTbarEventReconstructionTopPolarizationRecoLevel: Failed to read TTbarEventReconstructionClosestMass configuration.");
    }
    
    m_minSubjetPtFrac = jsonFunctions::readValueJSON<double>(json, "minSubjetPtFrac", m_minSubjetPtFrac);
    
  } catch(const std::runtime_error& e) {
    
    std::cout << "Error in TTbarEventReconstructionTopPolarizationRecoLevel::readJSONConfig" << std::endl;
    std::cout << e.what() << std::endl;
    return StatusCode::FAILURE;
  }
  
  
  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------

void TTbarEventReconstructionTopPolarizationRecoLevel::execute(ReconstructedEvent& event) const {
  // Executing object selection algorithm
  TTbarEventReconstructionClosestMass::execute(event);
  
  if(event.isAvailable("top1") && event.isAvailable("top2")) {
  
    Object_t& top1 = event.getRef<Object_t>("top1");
    Object_t& top2 = event.getRef<Object_t>("top2");
    
    if(!(top1.isAvailable("MatchedSmallRJets") && top2.isAvailable("MatchedSmallRJets"))) {
      throw std::runtime_error("TTbarEventReconstructionTopPolarizationRecoLevel: Top quarks has no subjets.");
    }
    
    
    const ObjectContainer_t& top1MatchedSmallRJets = top1.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
    const ObjectContainer_t& top2MatchedSmallRJets = top2.getConstRef<ObjectContainer_t>("MatchedSmallRJets");
    
    
    if(top1MatchedSmallRJets.size() > 0) {
      
      const double ptMin = m_minSubjetPtFrac * top1.Pt();
      const int index_btagger_score_max = ReconstructedObjects::getMaxAttributeIndex(top1MatchedSmallRJets, "BTagger_score", ptMin);
      std::shared_ptr<Object_t> WCandidate = 
        ReconstructedObjects::getSupplementaryObject(top1MatchedSmallRJets, index_btagger_score_max, ptMin);
      
      std::shared_ptr<Object_t> bCandidate = 
        (index_btagger_score_max >= 0) ?
        top1MatchedSmallRJets[index_btagger_score_max] :
        std::make_shared<Object_t>(0.,0.,0.,0.);
      
      top1.setAttribute<int>("BCandidate_index",index_btagger_score_max);
      top1.setPtr<Object_t>("BCandidate",bCandidate);
      top1.setPtr<Object_t>("WCandidate",WCandidate);
      
    }
    if(top2MatchedSmallRJets.size() > 0) {
      
      const double ptMin = m_minSubjetPtFrac * top2.Pt();
      const int index_btagger_score_max = ReconstructedObjects::getMaxAttributeIndex(top2MatchedSmallRJets, "BTagger_score", ptMin);
      std::shared_ptr<Object_t> WCandidate = 
        ReconstructedObjects::getSupplementaryObject(top2MatchedSmallRJets, index_btagger_score_max, ptMin);
      
      std::shared_ptr<Object_t> bCandidate = 
        (index_btagger_score_max >= 0) ?
        top2MatchedSmallRJets[index_btagger_score_max] :
        std::make_shared<Object_t>(0.,0.,0.,0.);
      
      top2.setAttribute<int>("BCandidate_index", index_btagger_score_max);
      top2.setPtr<Object_t>("BCandidate",bCandidate);
      top2.setPtr<Object_t>("WCandidate",WCandidate);
      
    }
    
    
  }
}



