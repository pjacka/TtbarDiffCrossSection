#!/bin/bash

# Peter Berta, 3.11.2012, pberta@cern.ch

# define the directory "dir" where you have stored the ROOT files. This directory have to contain other directories (named after datasets) which contain the ROOT files!!!
#mainDir=/afs/cern.ch/work/p/pjacka/minitrees_old/

#source MakeFilelists_eos.sh
directories=(
            #/afs/cern.ch/work/p/pjacka/minitrees_20160617/
	        #/afs/cern.ch/work/p/pjacka/minitrees/  
            #/afs/cern.ch/work/j/jpalicka/minitrees/
            #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20160804_ntuples/
            #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20161015_ntuples/
            #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/Full-data1516/
            #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/ttH_ntuple/
            #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/weight_Generator/
           #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170131_ntuples
           #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170223_ntuples
           #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170406_ntuples
           #/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170406_ntuples/DATASET_LINKS
           #/mnt/nfs17/ATLASDiffXsecAllHadBoosted/ntuples/TOPQ_2016_09_PAPER_ntuples/DATASET_LINKS
           #/mnt/nfs17/ATLASDiffXsecAllHadBoosted/ntuples/180201.AT.21.2.16.noTrigger/DATASET_LINKS
           #/mnt/nfs17/ATLASDiffXsecAllHadBoosted/ntuples/AT.21.2.16_v02/DATASET_LINKS
	   /mnt/nfs17/ATLASDiffXsecAllHadBoosted/ntuples/AT.21.2.29/DATASET_LINKS
            )

#mainDir=/afs/cern.ch/work/p/pjacka/minitrees/


for mainDir in "${directories[@]}" 
  do
  for dir in `ls ${mainDir}` ; do
    echo "$dir"
    mkdir -p ${dir}
    > ${dir}/allhad.boosted.output.txt
    for file in `ls ${mainDir}/${dir} | grep ".root" ` ; do
      if [[ "${file}" != *.root ]]
      then
	  continue
      fi
      #sample="${file//.root}"
      #rm -f ${dir}/${sample}.txt
      echo "${mainDir}/${dir}/${file}" >> ${dir}/allhad.boosted.output.txt
      #echo "${mainDir}/${dir}/${file}" >> ${dir}/${sample}.txt
    done
  done
done

for id in `seq 1 32`; do
  rm -rf *reweighted${id}
  cp -r MC16a.410471.PhPy8EG MC16a.410471.PhPy8EG_reweighted${id}
  cp -r MC16c.410471.PhPy8EG MC16c.410471.PhPy8EG_reweighted${id}
done
