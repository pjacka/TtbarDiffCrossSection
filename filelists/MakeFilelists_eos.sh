#!/bin/bash
eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160804_ntuples/
prefix=root://eosatlas/
#foldername=mc15_13TeV.410008.aMcAtNloHerwigppEvtGen_ttbar_allhad.merge.DAOD_TOPQ1
#inputfoldername=410008
outputfilename=allhad.boosted.output.txt


inputfoldernames=(410008 410007 410000 410002 410003 410013 410014 410043 410044 410045 410046 410160 410161 410162 410163 410024 410234)
foldernames=(	mc15_13TeV.410008.aMcAtNloHerwigppEvtGen_ttbar_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1
		mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1
		mc15_13TeV.410002.PowhegPythiaEvtGen_P2012radLo_ttbar_hdamp172_up_nonallhad.DAOD_TOPQ1
		mc15_13TeV.410003.aMcAtNloHerwigppEvtGen_ttbar_nonallhad.DAOD_TOPQ1
		mc15_13TeV.410013.PowhegPythiaEvtGen.Wt_inclusive_top.TOPQ1
		mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1
		mc15_13TeV.410043.PowhegPythiaEvtGen_P2012_ttbar_hdamp171p5_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410044.PowhegPythiaEvtGen_P2012_ttbar_hdamp173p5_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410045.PowhegPythiaEvtGen_P2012_ttbar_hdamp175_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410046.PowhegPythiaEvtGen_P2012_ttbar_hdamp177p5_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410160.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttbar_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410161.PowhegPythiaEvtGen_P2012radHi_ttbar_hdamp345_down_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410162.PowhegPythiaEvtGen_P2012radLo_ttbar_hdamp172p5_up_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410163.PowhegHerwigppEvtGen_UEEE5_ttbar_hdamp172p5_allhad.merge.DAOD_TOPQ1
		mc15_13TeV.410024.Sherpa_CT10_ttbar_AllHadron_MEPS_NLO.merge.DAOD_TOPQ1
		mc15_13TeV.410234.PowhegPythia8EvtGen.allhad.DAOD_TOPQ1
)

#echo ${inputfoldernames[0]}
#echo ${inputfoldernames[1]}

function makeFilelist () {
	local prefix=$1
	local eosdir=$2
	local foldername=$3
	local inputfoldername=$4
        local outputfilename=$5

	mkdir -p $foldername
        > $foldername/$outputfilename

	#cd $foldername
	#eos ls $eosdir | grep $inputfoldername
	directories=(`eos ls $eosdir | grep $inputfoldername`)
        #echo ${#directories[@]}
	i=0
	for dir in "${directories[@]}"
	do
	   #echo `eos ls $eosdir$dir | grep ".root"`
	   x=`eos ls $eosdir$dir | grep ".root" `
	   if [ -n "$x" ]; then
	    y=(`eos ls $eosdir$dir | grep ".root"`)
	    j=0
	    for z in ${y[@]}
	    do
	    samples[i]=$prefix$eosdir$dir"/"${y[j]}
	    #echo ${samples[i]}
            #echo $foldername/${outputfilename}
	    echo ${samples[i]} >> ${foldername}/${outputfilename}
	    j=$j+1
	    i=$i+1
	    done
	   fi
   
   
	done

}



#makeFilelist $prefix $eosdir $foldername $inputfoldername $outputfilename
k=0
l=${#foldernames[@]}
l=`expr $l - 1`
for foldername in "${foldernames[@]}"
do
	#echo $foldername
	#echo ${inputfoldernames[k]}
	echo $k/$l
	makeFilelist $prefix $eosdir $foldername ${inputfoldernames[k]} $outputfilename
	k=`expr $k + 1`
done






