#!/bin/bash

#assuming rucio is setup

datasetFileList=$1
       dataType=$2 #e.g. MC23a, MC23d, data

debug=0

datasets=`cat $datasetFileList`

#TMP CHECK
#datasets="user.cheny.410532.PowhegPythia8EvtGen.DAOD_TOPQ1.e5884_s2726_r7772_r7676_p2952.AT29.20170406-00_allhad_boosted.root"

for ds in $datasets; do
  #to remove potential '/' at the end of dataset
  dsBsName=`basename $ds`

  if [[ $dataType == "data" ]]; then   
    if   [[ $dsBsName == "user."*"grp15"* ]]; then   outDirName="data2015_AllYear.physics_Main"
    elif [[ $dsBsName == "user."*"grp16"* ]]; then   outDirName="data2016_AllYear.physics_Main"
    elif [[ $dsBsName == "user."*"grp17"* ]]; then   outDirName="data2017_AllYear.physics_Main";  
    elif [[ $dsBsName == "user."*"grp18"* ]]; then   outDirName="data2018_AllYear.physics_Main";
    elif [[ $dsBsName == "user."*"grp22"* ]]; then   outDirName="data2022_AllYear.physics_Main";
    elif [[ $dsBsName == "user."*"grp23"* ]]; then   outDirName="data2023_AllYear.physics_Main";
    else outDirName=${dataType}2018_`echo $dsBsName | awk -F'.' '{print $3"."$4 }'`; fi
  else
    outDirName=$dataType.`echo $dsBsName | awk -F'.' '{print $3"."$4 }'`
  fi
  
  
  echo "output directory name: $outDirName"

  echo "getting files for dataset: $ds"
  #get absoluth paths to individual files
  
  mkdir -p ${outDirName}
  rucio list-file-replicas --pfns --rse PRAGUELCG2_LOCALGROUPDISK --protocol root $dsBsName > ${outDirName}/allhad.boosted.output.txt
  

  #rucio list-file-replicas $dsBsName | grep PRAGUELCG2_LOCALGROUPDISK |awk '{print $12}' > files_$dsBsName.txt
  ##rucio list-file-replicas $dsBsName | grep PRAGUELCG2_SCRATCHDISK |awk '{print $12}' > files_$dsBsName.txt
  
  ##change file name format to be able to read from ROOT
  #mkdir -p ${outDirName}  
  #cat files_$dsBsName.txt | sed -e 's/gsiftp:\/\//root:\/\//' -e 's/farm.particle.cz:[0-9]*/farm.particle.cz\//' \
       #>  ${outDirName}/allhad.boosted.output.txt

  #if [[ $debug -eq 0 ]]; then 
    #\rm -f files_$dsBsName.txt
  #fi
done

