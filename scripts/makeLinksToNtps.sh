#!/bin/bash

if [ $# -lt 1 ]; then 
  echo "USAGE $0 <dirName>"
  echo "   <dirName> - directory with ntuples"
  echo "   e.g. $0 /mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170406_ntuples"
  exit
fi

#defaults
  ntpDir=$1
linksDir=DATASET_LINKS

curDir=`pwd`

cd $ntpDir

directories=( 'data' 'MC16a' 'MC16c' 'MC16d' )

for directory in ${directories[@]}; do

  datasets=`ls -d $directory/*`
  echo "files: $datasets"
  
  if [[ ! -d $linksDir ]]; then  mkdir $linksDir;  chmod 777 -R $linksDir; fi 
  cd    $linksDir
  for dataset in $datasets; do
  
    if [[ $dataset == *".txt" || $dataset == *".log" || $dataset == "$linksDir" ]]; then 
	echo "skipping $dataset ..."
	continue; 
    fi
  
    outLinkName=`echo $dataset|awk -F'.' '{print $3"."$4 }'`
    outLinkName=${directory}.$outLinkName
  
    #for data: rename dataset to include year
    if [[ $dataset == *"AllYear"* ]]; then   
      if   [[ $dataset == "data/user."*"grp15"* ]]; then   outLinkName=data2015
      elif [[ $dataset == "data/user."*"grp16"* ]]; then   outLinkName=data2016;
      elif [[ $dataset == "data/user."*"grp17"* ]]; then   outLinkName=data2017;  fi
      outLinkName=$outLinkName`echo $dataset|awk -F'.' '{print "_"$3"."$4 }'`
    fi
    if [[ ! -h $outLinkName ]]; then 
      echo "creating soft link $outLinkName"
      ln -s $ntpDir/$dataset $outLinkName
    fi
  done

  cd ..
done

echo "the directory with links to datasets: $ntpDir/$linksDir"

cd $curDir
