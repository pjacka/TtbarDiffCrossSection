#default parameters
mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json

levels=(
"ParticleLevel"
)

variables=(
"total_cross_section"
)

###############

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    echo $variable
    echo $mainDir $config_files $config_lumi $variable $level 
    createEventYieldsTable $mainDir $config_files $config_lumi $variable $level 
  done
done
