#DEFAULTS:

mainDir=${TtbarDiffCrossSection_output_path}
systematicHistosPath=${mainDir}/root/systematics_histos2D
outputPath=${mainDir}/txt/2Dbinnings

levels=(
"PartonLevel"
)

variables=(
"t1_pt_vs_t2_pt"
"t1_pt_vs_delta_pt"
"t1_pt_vs_ttbar_pt"
"t1_pt_vs_ttbar_mass"
"ttbar_y_vs_t1_pt"
"ttbar_y_vs_ttbar_mass"
)


for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    optimize2DBinning ${mainDir}/ ${systematicHistosPath} ${outputPath} $variable $level
    #valgrind --tool=massif drawReco ${mainDir}/ ${systematicHistosPath} ${outputSubdir} ${config_files} ${config_lumi} $variable $level
  done
done




