#!/bin/bash

#DEFAULTS:
configName=TtbarDiffCrossSection/data/config.json
doSys=1 #run w/out systematics (if you want to run systematics, run on the farm using 'SubmitFarm_All.sh')
doSysPDF=1
doSysAlphaSFSR=1
doSysIFSR=1
doReweighting=0
doBootstrap=1000
nEvents=-1 # run over all events
debugLevel=1
prod="MC23a"

sample=signal

TtbarDiffCrossSection_output_path=${TtbarDiffCrossSection_output_path}/../topPolarization_testing

echo "root file with histograms will be saved in" $TtbarDiffCrossSection_output_path
echo "running over these samples: "${sample}

filelists=${TTBAR_PATH}/TtbarDiffCrossSection/filelists/

signal_reference="${prod}.601237.PhPy8EG"
data_reference="data2022_AllYear.physics_Main"

if [ $sample == signal ]; then
  directories=("${signal_reference}")
fi

if [ $sample == data22 ]; then
  directories=("data2022_AllYear.physics_Main")
fi
if [ $sample == data23 ]; then
  directories=("data2023_AllYear.physics_Main")
fi
if [ $sample == data ]; then
  directories=(${data_reference})
fi

# ttbar l+jets
if [ $sample == tbar_ljets ]; then
  directories=(${prod}.601229.PhPy8EG)
fi

# ttbar dilep
if [ $sample == tbar_ljets ]; then
  directories=(${prod}.601230.PhPy8EG)
fi

#Wt
if [ $sample == Wt_DR_antitop ]; then
  directories=(${prod}.601352.PhPy8EG)
fi
if [ $sample == Wt_DR_top ]; then
  directories=(${prod}.601355.PhPy8EG)
fi
if [ $sample == Wt_DS_antitop ]; then
  directories=(${prod}.601627.PhPy8EG)
fi
if [ $sample == Wt_DS_antitop ]; then
  directories=(${prod}.601631.PhPy8EG)
fi

#ttHWZ
if [ $sample == ttH_dilep ]; then
  directories=(${prod}.602637.PhPy8EG)
fi
if [ $sample == ttH_semilep ]; then
  directories=(${prod}.602638.PhPy8EG)
fi
if [ $sample == ttw_0l ]; then
  directories=(${prod}.700995.Sh_2214_ttW_0Lfilter)
fi
if [ $sample == ttw_1l ]; then
  directories=(${prod}.700996.Sh_2214_ttW_1Lfilter)
fi
if [ $sample == ttw_2l ]; then
  directories=(${prod}.700997.Sh_2214_ttW_2Lfilter)
fi

#directories=${directories_${sample}}
#directories=${!d}

echo "starting to run at: $(date)"
startTime=$(/usr/bin/date +%s) #in seconds

echo $directories
for dir in "${directories[@]}"; do

  echo $dir
  mkdir -p ${TtbarDiffCrossSection_output_path}/${dir}

  #for file in `ls ${filelists}${dir}`; do
  for file in $(ls ${filelists}/SPLITTED/${dir}); do

    #if [[ "${file}" == *.txt ]] ; then
    if [[ "${file}" == *.txt_000 ]]; then
      #if [[ "${file}" == *.txt_018 ]] ; then

      echo ${filelists}${dir}/${file}

      #tool="valgrind --tool=memcheck"
      #tool="gdb --args"
      tool=""
      #cmd="produceHistos --output ${TtbarDiffCrossSection_output_path}/${dir}/${file/.txt/.root} --filelist ${filelists}/${dir}/${file} --config $configName --doDetSys ${doSys} --doSysPDF ${doSysPDF} --doSysAlphaSFSR $doSysAlphaSFSR --doSysIFSR $doSysIFSR --doReweighting ${doReweighting} --nBootstrapReplicas ${doBootstrap} --nEvents ${nEvents} --debugLevel ${debugLevel}"
      cmd="produceHistos --output ${TtbarDiffCrossSection_output_path}/${dir}/${file/.txt_000/.root} --filelist ${filelists}/SPLITTED/${dir}/${file} --config $configName --doDetSys ${doSys} --doSysPDF ${doSysPDF} --doSysAlphaSFSR $doSysAlphaSFSR --doSysIFSR $doSysIFSR --doReweighting ${doReweighting} --nBootstrapReplicas ${doBootstrap} --nEvents ${nEvents} --debugLevel ${debugLevel}"
      #cmd="produceHistos --output ${TtbarDiffCrossSection_output_path}/${dir}/${file/.txt_034/.root} --filelist ${filelists}/SPLITTED/${dir}/${file} --config $configName --doDetSys ${doSys} --doSysPDF ${doSysPDF} --doSysAlphaSFSR $doSysAlphaSFSR --doSysIFSR $doSysIFSR --doReweighting ${doReweighting} --nBootstrapReplicas ${doBootstrap} --nEvents ${nEvents} --debugLevel ${debugLevel}"
      echo $cmd
      $tool $cmd

      echo ${filelists}${dir}/${file}
      #echo ${TtbarDiffCrossSection_output_path}/${dir}/"${file/.txt/.root}"
      echo ${TtbarDiffCrossSection_output_path}/${dir}/"${file/.txt_000/.root}"
      #echo ${TtbarDiffCrossSection_output_path}/${dir}/"${file/.txt_018/.root}"
      echo ${doSys}
      echo ${doSysPDF}
      echo ${doSysIFSR}
      echo ${doReweighting}
      echo ${doBootstrap}
      echo ${nEvents}
    fi
  done
done

endTime=$(/usr/bin/date +%s) #in seconds
echo "running ended at $(date)"
echo "running took $(expr $endTime - $startTime) seconds"
