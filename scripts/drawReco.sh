#DEFAULTS:

mainDir=${TtbarDiffCrossSection_output_path}
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
multiDimensionalPlotsConfig=TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env
LogScales=(1)
normalizeSignal=1
debugLevel=1
drawRatio=1


systematicHistosPath=systematics_histosND
outputSubdir=RecoPlotsND

variablesND=(
#"t1_phi_vs_t1_eta"
#"t2_phi_vs_t2_eta"
#"t1_pt_vs_t2_pt"
#"t1_y_vs_t2_y"
#"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
#"ttbar_y_vs_ttbar_mass_vs_t1_pt"
)

for logScale in ${LogScales[@]}
do
  for variable in ${variablesND[@]}
  do
    config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/data/optBinningND/${variable}.env
    drawRecoND --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${outputSubdir} --config ${config_lumi} --configBinning ${config_binning} --multiDimensionalPlotsConfig ${multiDimensionalPlotsConfig} --drawRatio ${drawRatio} --useLogScale ${logScale} --variable ${variable} --normalizeSignal ${normalizeSignal} --debugLevel ${debugLevel}
  done
done




systematicHistosPath=systematics_histos
outputSubdir=RecoPlots

variables=(
"total_cross_section"
"randomTop_pt"
"randomTop_y"
"t1_pt"
"t1_y"
"t2_pt"
"t2_y"
"ttbar_mass"
"ttbar_pt"
"ttbar_y"
"chi_ttbar"
"y_boost"
"pout"
"H_tt_pt"
"ttbar_deltaphi"
"cos_theta_star"
"z_tt_pt"
"y_star"
"t1_mass"
"t2_mass"
"delta_pt"
"t1_m_over_pt"
"t2_m_over_pt"
)


for variable in ${variables[@]}
do
  drawReco --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${outputSubdir} --config ${config_lumi} --variable $variable --normalizeSignal ${normalizeSignal} --debugLevel ${debugLevel}
  
  #drawReco_bkg_frac --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${outputSubdir} --config ${config_lumi} --variable $variable --normalizeSignal ${normalizeSignal} --debugLevel ${debugLevel}
  
  #valgrind --tool=massif drawReco --mainDir ${mainDir} --systematicsHistosDir ${systematicHistosPath} --outputDir ${outputSubdir} -config ${config_lumi} --variable $variable --normalizeSignal ${normalizeSignal} --debugLevel ${debugLevel}
done
     
