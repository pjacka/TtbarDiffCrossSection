#default parameters

mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
plotsDir="sys_errors_summary_plots"
systematicUncertaintiesDir="SystematicUncertainties"
covariancesDir="covariances"
systematicHistosDir="systematics_histos"
dimension="1D"
debugLevel=0

levels=(
#"PartonLevel"
"ParticleLevel"
)


variables=(
"total_cross_section"
#"randomTop_pt"
#"randomTop_y"
#"t1_pt"
#"t1_y"
#"t2_pt"
#"t2_y"
#"ttbar_mass"
#"ttbar_pt"
#"ttbar_y"
#"chi_ttbar"
#"y_boost"
#"pout"
#"H_tt_pt"
#"ttbar_deltaphi"
#"cos_theta_star"
##"delta_pt"
)
for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    cmd="CalculateAsymmetricErrors --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --plotsDir ${plotsDir} --systematicUncertaintiesDir ${systematicUncertaintiesDir} --covariancesDir ${covariancesDir} --systematicHistosDir ${systematicHistosDir} --dimension $dimension --debugLevel $debugLevel"
    echo "Running $cmd"
    $cmd 
  done
done
