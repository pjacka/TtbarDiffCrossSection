#!/bin/bash
HOSTNAME=`hostname`

echo "\$TtbarDiffCrossSection_output_path: ${TtbarDiffCrossSection_output_path}"

#mc_productions=("MC16a" "MC16d")
mc_productions=("MC16a")



for MC in ${mc_productions[@]}; do

  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_merged
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_merged_reweighted1
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_merged_reweighted2
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_H7_merged
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_aMCatNLO_merged
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_IFSRup_merged
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.ttbar_IFSRdown_merged
  mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.Wt_merged
  mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.HWZ_merged
  #mkdir -p ${TtbarDiffCrossSection_output_path}/${MC}.Dijet_merged
     
  mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/${MC}.Wt_merged/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.410646.PowhegPythia8EvtGen/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.410647.PowhegPythia8EvtGen/allhad.boosted.output.root
  
  mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/${MC}.HWZ_merged/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.345873.PowhegPythia8EvtGen/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.345874.PhPy8EG/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.345875.PhPy8EG/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.410155.aMcAtNloPythia8EvtGen/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.410156.aMcAtNloPythia8EvtGen/allhad.boosted.output.root \
      ${TtbarDiffCrossSection_output_path}/${MC}.410157.aMcAtNloPythia8EvtGen/allhad.boosted.output.root
      
  #mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/${MC}.Dijet_merged/allhad.boosted.output.root \
    #${TtbarDiffCrossSection_output_path}/${MC}.36102?.Pythia8EvtGen/allhad.boosted.output.root

done

mkdir -p ${TtbarDiffCrossSection_output_path}/MC16a.ttbar_allhad_nominal_merged
mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/MC16a.ttbar_allhad_nominal_merged/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/MC16a.410428.PhPy8EG/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/MC16a.410429.PhPy8EG/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/MC16a.410444.PhPy8EG/allhad.boosted.output.root \

mkdir -p ${TtbarDiffCrossSection_output_path}/data15_16
mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/data15_16/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/data2015_AllYear.physics_Main/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/data2016_AllYear.physics_Main/allhad.boosted.output.root





##################################################################################################################################################################################
#######  Merging All Years data and production
##################################################################################################################################################################################

mkdir -p ${TtbarDiffCrossSection_output_path}/data_AllYears
mkdir -p ${TtbarDiffCrossSection_output_path}/All.Wt_merged
mkdir -p ${TtbarDiffCrossSection_output_path}/All.HWZ_merged
#mkdir -p ${TtbarDiffCrossSection_output_path}/All.Dijet_merged
mkdir -p ${TtbarDiffCrossSection_output_path}/All.410471.PhPy8EG
mkdir -p ${TtbarDiffCrossSection_output_path}/All.410470.PhPy8EG


mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/data_AllYears/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/data2015_AllYear.physics_Main/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/data2016_AllYear.physics_Main/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/data2017_AllYear.physics_Main/allhad.boosted.output.root

mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.Wt_merged/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.Wt_merged/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.Wt_merged/allhad.boosted.output.root

mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.HWZ_merged/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.HWZ_merged/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.HWZ_merged/allhad.boosted.output.root

mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.410471.PhPy8EG/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.410471.PhPy8EG/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.410471.PhPy8EG/allhad.boosted.output.root

mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.410470.PhPy8EG/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.410470.PhPy8EG/allhad.boosted.output.root \
  ${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.410470.PhPy8EG/allhad.boosted.output.root

#mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.Dijet_merged \
  #${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.Dijet_merged/allhad.boosted.output.root \
  #${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.Dijet_merged/allhad.boosted.output.root


for id in `seq 1 32`; do
  mkdir -p ${TtbarDiffCrossSection_output_path}/All.410471.PhPy8EG_reweighted$id
  mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.410471.PhPy8EG_reweighted$id/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.410471.PhPy8EG_reweighted$id/allhad.boosted.output.root \
    ${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.410471.PhPy8EG_reweighted$id/allhad.boosted.output.root
done

#mergeHistosFromRun ${TtbarDiffCrossSection_output_path}/All.ttbar_allhad_nominal_merged/allhad.boosted.output.root \
#  ${TtbarDiffCrossSection_output_path}/${mc_productions[0]}.ttbar_allhad_nominal_merged/allhad.boosted.output.root \
#  ${TtbarDiffCrossSection_output_path}/${mc_productions[1]}.ttbar_allhad_nominal_merged/allhad.boosted.output.root


