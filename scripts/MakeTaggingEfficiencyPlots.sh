#default parameters
mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
#MC_PRODS=("MC16a")
MC_PRODS=("All")
topTaggerLabel="DNNTaggerTopQuarkContained80"
bTaggerLabel="GN2v01_FixedCutEff_85"

variables=(
"fatJet1_pt"
"fatJet1_y"	
"fatJet1_mass"
"fatJet2_pt"
"fatJet2_y"
"fatJet2_mass"
"ttbar_mass"
"ttbar_pt"
"ljets_Ht"
)

###############

for MC_PROD in ${MC_PRODS[@]}
do
  for variable in ${variables[@]}
  do
    MakeTaggingEfficiencyPlots $mainDir $config_lumi $MC_PROD $variable $topTaggerLabel ${bTaggerLabel}
  done
done
