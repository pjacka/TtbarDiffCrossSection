#!/bin/bash


vars=(
      total_cross_section
      randomTop_pt
      randomTop_y
      t1_pt
      t1_y
      t2_pt
      t2_y
      ttbar_mass
      ttbar_pt
      ttbar_y
      chi_ttbar
      y_boost
      pout
      H_tt_pt
      ttbar_deltaphi
      cos_theta_star
      #z_tt_pt
      #y_star
      t1_mass
      t2_mass
      delta_pt
      t1_m_over_pt
      t2_m_over_pt
      )


outDir=$TtbarDiffCrossSection_output_path/root.All/reweightingHistos
if [[ ! -d $outDir ]]; then  mkdir -p $outDir ; fi

outFile=reweightingHistos.root

rm -f $outFile

for var in ${vars[@]}; do
  #echo $var
  ./TtbarDiffCrossSection/python/produceDataMCratioHists.py --variable ${var} --outputDir ${outDir} --outputFile ${outFile}
  
done


./TtbarDiffCrossSection/python/makeTopMassReweightingCurves.py
