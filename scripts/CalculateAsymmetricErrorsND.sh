#default parameters
mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
plotsDir="sys_errors_summary_plotsND"
systematicUncertaintiesDir="SystematicUncertaintiesND"
covariancesDir="covariancesND"
systematicHistosDir="systematics_histosND"
dimension="ND"
debugLevel=0

levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
#"t1_pt_vs_t2_pt"
#"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"t1_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
#"ttbar_y_vs_ttbar_pt"
#"t2_pt_vs_ttbar_mass"
#"ttbar_y_vs_t2_pt"
#"t2_y_vs_t2_pt"
#"top_pt_vs_ttbar_mass"
#"ttbar_y_vs_top_pt"
#"ttbar_y_vs_ttbar_mass_vs_t1_pt"
#"ttbar_y_vs_ttbar_pt_vs_ttbar_mass"
#"top_y_vs_top_pt"
#"ttbar_y_vs_top_y"
#"ttbar_y_vs_t1_y"
#"ttbar_y_vs_t2_y"
#"t1_y_vs_ttbar_mass"
#"t2_y_vs_ttbar_mass"
#"top_y_vs_ttbar_mass"
#"ttbar_pt_vs_ttbar_mass"
#"delta_eta_vs_ttbar_mass"
#"delta_y_vs_ttbar_mass"
#"delta_phi_vs_ttbar_mass"
"t1_y_vs_t2_y"
#"delta_y_vs_t1_y"
#"t1_phi_vs_t1_eta"
#"t2_phi_vs_t2_eta"
)


for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    #cmd="CalculateAsymmetricErrors --mainDir $mainDir --outputDir $outputDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --dimension $dimension --debugLevel $debugLevel"
    configBinning=TtbarDiffCrossSection/data/optBinningND/${variable}.env
    cmd="CalculateAsymmetricErrors --mainDir $mainDir --config $config_lumi --configBinning ${configBinning} --configSysNames $config_sysnames --variable $variable --level $level --plotsDir ${plotsDir} --systematicUncertaintiesDir ${systematicUncertaintiesDir} --covariancesDir ${covariancesDir} --systematicHistosDir ${systematicHistosDir} --dimension $dimension --debugLevel $debugLevel"
    echo "Running $cmd"
    $cmd    
  done
done
