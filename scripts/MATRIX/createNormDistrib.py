#!/usr/bin/env python

#------------
#  to be used to obtain normalized distributions using individual histograms corresponding to scales
#      - distributions are normalized for each scale
#      - the scale variation is obtained by taking the spread of normalized values
#
# asssumptions:
#  - .dat files from result directory are in a given (./) directory
#  - files specific for each scale are in CV/scale.X.X directories
#------------

import sys
import os
import shutil
import math 
import array as ar
import numpy as np
import ROOT as r


debug = 10

dirWithScales = 'CV/'
relPrecis=0.001

#directory names with individual scale results
scales={ 'scale1' : 'scale.0.0',
         'scale2' : 'scale.1.1',
         'scale3' : 'scale.2.2',
         'scale4' : 'scale.3.3',
         'scale5' : 'scale.4.4',
         'scale6' : 'scale.5.5',
         'scale7' : 'scale.6.6' }
#nominal scale
scaleNom = 'scale4'


def createNormDistrib(f, outGraphName, inScaleHistName):

  grScale     = {}
  grNormScale = {}
  for sck in scales.keys():

    #read in values from files corresponding to different scales 
    grScale[sck] = createGraph( os.path.join(dirWithScales, scales[sck], inScaleHistName) )

    #create normalized graphs for each scale
    grNormScale[sck] = createNormGraph( grScale[sck] )

    
  #create final graphs: value is from nominal scale, uncertainty from spread of normalized values
  grNorm_stat  = r.TGraphAsymmErrors( grNormScale[scaleNom].GetN() )
  grNorm_syst  = r.TGraphAsymmErrors( grNormScale[scaleNom].GetN() )
  grNorm_total = r.TGraphAsymmErrors( grNormScale[scaleNom].GetN() )
  grNorm_stat.SetName(outGraphName  + "_stat")
  grNorm_syst.SetName(outGraphName  + "_syst")
  grNorm_total.SetName(outGraphName + "_total")
  
  for i in range(grNormScale[scaleNom].GetN()): 

    #get min/max values from scale graphs to get syst uncertainty
    yscale_nom = 0.0
    yscale = []
    for s in scales.keys():
      ys = grNormScale[s].GetY()[i]
      yscale.append(float(ys))
      if (s == scaleNom): yscale_nom = ys
      
    eyh = abs(max(yscale) - yscale_nom)
    eyl = abs(yscale_nom - min(yscale))

    eyh_stat = grNormScale[scaleNom].GetEY()[i]
    eyl_stat = grNormScale[scaleNom].GetEY()[i]
    
    #for stat: use only stat uncertainties
    grNorm_stat.SetPoint(i, grNormScale[scaleNom].GetX()[i], grNormScale[scaleNom].GetY()[i])
    grNorm_stat.SetPointEXlow (i, grNormScale[scaleNom].GetEX()[i])
    grNorm_stat.SetPointEXhigh(i, grNormScale[scaleNom].GetEX()[i])
    grNorm_stat.SetPointEYlow (i, eyl_stat)
    grNorm_stat.SetPointEYhigh(i, eyh_stat)

    #for syst: use only scale uncertainties
    grNorm_syst.SetPoint(i, grNormScale[scaleNom].GetX()[i], grNormScale[scaleNom].GetY()[i])
    grNorm_syst.SetPointEXlow (i, grNormScale[scaleNom].GetEX()[i])
    grNorm_syst.SetPointEXhigh(i, grNormScale[scaleNom].GetEX()[i])
    grNorm_syst.SetPointEYlow (i, eyl)
    grNorm_syst.SetPointEYhigh(i, eyh)

    #for total: combine stat and syst uncertainties in quadrature
    grNorm_total.SetPoint(i, grNormScale[scaleNom].GetX()[i], grNormScale[scaleNom].GetY()[i])
    grNorm_total.SetPointEXlow (i, grNormScale[scaleNom].GetEX()[i])
    grNorm_total.SetPointEXhigh(i, grNormScale[scaleNom].GetEX()[i])
    grNorm_total.SetPointEYlow (i, math.sqrt(eyl*eyl + eyl_stat*eyl_stat) )
    grNorm_total.SetPointEYhigh(i, math.sqrt(eyh*eyh + eyh_stat*eyh_stat) )
    
  if debug > 3:
    printGraphInfo(grNorm_stat, debug)
    printGraphInfo(grNorm_syst, debug)
    printGraphInfo(grNorm_total, debug)
                               
  return (grNorm_stat, grNorm_syst, grNorm_total)


def createGraph(distribFileName):
    
  #read in data from file
  data = np.genfromtxt(distribFileName, usecols=(0,1,2), dtype=None, 
                       names="binEdge, val, errStat")

  if debug > 5:
      print 'data:'
      print data

  #create inputs to TGraphs
  binEdge = data['binEdge']
  binCenter  = np.zeros(binEdge.size - 1)
  binErr     = np.zeros(binEdge.size - 1)

  if debug > 6:
      print 'binEdge:'
      print binEdge

  for b in range(binEdge.size-1):
      binCenter[b]  = (binEdge[b+1] + binEdge[b]) / 2.
      binErr[b]     = (binEdge[b+1] - binEdge[b]) / 2.

  if debug > 7:
      print 'binCenter,binErr:'
      print binCenter
      print binErr
      
  #the last row values are just repeat of next-to-last row, so skip them
  val     = data['val'][:-1]
  statErr = data['errStat'][:-1]

  if debug > 3:
      print 'val,statErr:'
      print val
      print statErr

  #convert numpy objects to array.array
  bc    = ar.array("f",binCenter.tolist())
  be    = ar.array("f",binErr.tolist())
  v     = ar.array("f",val.tolist())
  stae  = ar.array("f",statErr.tolist())

  #create TGraph
  n = val.size

  grStat   = r.TGraphErrors(n, bc, v, be, stae)

  return (grStat)


def createNormGraph(gr):

  grNorm = gr.Clone()

  x   = gr.GetX()
  y   = gr.GetY()
  ex  = gr.GetEX()
  ey  = gr.GetEY()

  #get total xsection within the range of histogram
  #assuming the original graph is normalized by bin width
  totalXsec = 0.
  for i in range(gr.GetN()):
    if debug > 5: print 'ex: ', ex
    binWidth = 2*ex[i]
    totalXsec += y[i] * binWidth

  if debug > 3: print 'totalXsec: ', totalXsec

  for i in range(gr.GetN()):
    grNorm.SetPoint     (i, x[i] , y[i]/totalXsec)
    grNorm.SetPointError(i, ex[i], ey[i]/totalXsec)

  return grNorm
  

def relDiff(x, y, relPrecis=0.001):
  if (x != 0):  return abs((x-y)/x) < relPrecis
  else:         return False


def printGraphInfo(gr, debug):
  n = gr.GetN()

  x   = gr.GetX()
  y   = gr.GetY()
  exh = gr.GetEXhigh()
  exl = gr.GetEXlow()
  eyh = gr.GetEYhigh()
  eyl = gr.GetEYlow()
    
  if (debug > 3):
    print 'gr, n, x, y, exh, exl, eyh, eyl:'
    print gr
    print n
    for i in range(n): print x[i], "  ",
    print
    for i in range(n): print y[i], "  ",
    print
    for i in range(n): print exh[i], "  ",
    print
    for i in range(n): print exl[i], "  ",
    print
    for i in range(n): print eyh[i], "  ",
    print
    for i in range(n): print eyl[i], "  ",
    print

 
    
if __name__ == '__main__':

    inFileName    = sys.argv[1]
    outFileName   = sys.argv[2]
    listOfHist    = sys.argv[3]
    
    #copy input to output
    shutil.copyfile(inFileName, outFileName)

    #open output file
    f = r.TFile(outFileName,"UPDATE")
    
    #read in names of hists to be summed
    data = np.genfromtxt(listOfHist, usecols=(0,1), dtype=None, 
                         names="outGraphName, scaleHist")
    
    #loop over all distributions
    ndist = data['outGraphName'].size

    if debug > 0: print 'ndist= ', ndist
    
    for id in range(ndist):
        if ndist == 1:
          outGraphName      = data['outGraphName'].tostring()
          inScaleHistName   = data['scaleHist'].tostring()
        else:
          outGraphName      = data['outGraphName'][id]
          inScaleHistName   = data['scaleHist'][id]
        
        if debug > 0: print 'outGraphName,inScaleHistName : ', outGraphName, inScaleHistName

        grNorm_stat, grNorm_syst, grNorm_total = createNormDistrib(f, outGraphName, inScaleHistName)

        #save graphs to output file
        grNorm_stat .Write()
        grNorm_syst .Write()
        grNorm_total.Write()
        
    f.Close()

