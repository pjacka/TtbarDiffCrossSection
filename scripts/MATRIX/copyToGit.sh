#!/bin/bash 

inputDir=$1
outputDir=$2

pwd=$PWD
cd $inputDir

cp  *txt $outputDir/.
cp  *log $outputDir/.
cp  `ls *root|grep -v noRndm|grep -v noNorm`  $outputDir/.

#copy dat files
cp `cat distribList.txt|awk '{print $2}'` $outputDir/

for f in `cat normalizedDistribList.txt|awk '{print $2}'`; do 
  echo $f
  for scale in `ls -d CV/*`; do 
    echo $scale 
    mkdir -p $outputDir/$scale
    cp  $scale/$f $outputDir/$scale/$f
  done
done


