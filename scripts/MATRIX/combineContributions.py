#
#
#  combine contributions like this:
#      sum = contr1 + contr2 - contr3
#
#  the input file has a form:
#     muR   muF   cross-section  num_err
#
import sys
import numpy as np

file1 = sys.argv[1]
file2 = sys.argv[2]
file3 = sys.argv[3]

print file1, file2, file3

data1 = np.genfromtxt(file1, usecols=(0,1,2,3), dtype=None, 
                      names="muR, muF, xsec, errStat", skip_header=1)
data2 = np.genfromtxt(file2, usecols=(0,1,2,3), dtype=None, 
                      names="muR, muF, xsec, errStat", skip_header=1)
data3 = np.genfromtxt(file3, usecols=(0,1,2,3), dtype=None, 
                      names="muR, muF, xsec, errStat", skip_header=1)

print data1
print data2
print data3

dataComb = data1.copy()
dataComb['xsec'] = data1['xsec'] + data2['xsec'] - data3['xsec']
dataComb['errStat'] = np.sqrt( data1['errStat']**2 + data2['errStat']**2 + data3['errStat']**2 )

condNominal = (dataComb['muR']==1) * (dataComb['muF']==1)
xsecComb    = np.select( condlist= condNominal,
                         choicelist= dataComb['xsec'] )
errCombStat = np.select( condlist= condNominal,
                         choicelist= dataComb['errStat'] )

errCombScaleUp   = (np.max(dataComb['xsec']) - xsecComb) / xsecComb * 100. 
errCombScaleDown = (np.min(dataComb['xsec']) - xsecComb) / xsecComb * 100.

print "combined cross-section: ", xsecComb , "  +-  ", errCombStat
print "scale uncertainties: +", errCombScaleUp, " %  ", errCombScaleDown, " %"

#other way of calculation, not necessary, just a cross-check
xsecNominal = [ np.select( condlist= condNominal,
                           choicelist= data1['xsec']),
                np.select( condlist= condNominal,
                           choicelist= data2['xsec']),
                np.select( condlist= condNominal,
                           choicelist= data3['xsec']) ] 
errNominal  = [ np.select( condlist= condNominal,
                           choicelist= data1['errStat']),
                np.select( condlist= condNominal,
                           choicelist= data2['errStat']),
                np.select( condlist= condNominal,
                           choicelist= data3['errStat']) ]

print xsecNominal
print errNominal

def calculteCombinedXsec(xsecNominal, errNominal):
    xsec = xsecNominal[0] + xsecNominal[1] - xsecNominal[2]
    import math
    err  = math.sqrt( np.power(errNominal,2).sum() )
    return (xsec,err)

xsecComb2,errCombStat2 = calculteCombinedXsec(xsecNominal, errNominal)

print "combined cross-section: ", xsecComb , "  +-  ", errCombStat
print "combined cross-section: ", xsecComb2, "  +-  ", errCombStat2


