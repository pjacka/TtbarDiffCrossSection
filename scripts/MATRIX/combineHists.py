#!/usr/bin/env python

#------------
#  to be used for: combine  top pT and tbar pT histograms into one random top_pT histogram
#                  (random pT=(pT_top+pT_tbar)/2)
#      - combination is performed for graphs with stat, syst and total errors:
#          - for stat: assume variables are 100% correlated --> linear sum of errors
#          - for syst: assume 100% correlation for scales --> linear sum of errors
#            (i.e. the max and min variations correspond to a given muR,muF scale variation 
#                  for both histograms)
#          - total: sum in quadrature stat and syst errors
#  - it copies input file to output and create a new histogram 'random top pT' in output file
#
# asumption: -
#------------

import sys
import shutil
import math 
import numpy as np
import ROOT as r


debug = 10


def combineGraphs(f, inHistName1, inHistName2, outHistName):
  
  gr1_stat  = f.Get(inHistName1 + "_stat")
  gr2_stat  = f.Get(inHistName2 + "_stat")
  gr1_syst  = f.Get(inHistName1 + "_syst")
  gr2_syst  = f.Get(inHistName2 + "_syst")
  gr1_total = f.Get(inHistName1 + "_total")
  gr2_total = f.Get(inHistName2 + "_total")
  
  assert gr1_stat.GetN()  == gr2_stat.GetN()
  assert gr1_syst.GetN()  == gr2_syst.GetN()
  assert gr1_total.GetN() == gr2_total.GetN()
  assert gr1_stat.GetN()  == gr1_syst.GetN()
  assert gr1_stat.GetN()  == gr1_total.GetN()
  
  grStat   = gr1_stat.Clone(outHistName + "_stat")
  grSyst   = gr1_syst.Clone(outHistName + "_syst")
  grTotal  = gr1_total.Clone(outHistName + "_total")
  
  x1 =   gr1_stat.GetX()

  y1 =   gr1_stat.GetY()
  y2 =   gr2_stat.GetY()

  eyh1stat = gr1_stat.GetEYhigh()
  eyl1stat = gr1_stat.GetEYlow()
  eyh2stat = gr2_stat.GetEYhigh()
  eyl2stat = gr2_stat.GetEYlow()

  eyh1syst = gr1_syst.GetEYhigh()
  eyl1syst = gr1_syst.GetEYlow()
  eyh2syst = gr2_syst.GetEYhigh()
  eyl2syst = gr2_syst.GetEYlow()

  eyh1total = gr1_total.GetEYhigh()
  eyl1total = gr1_total.GetEYlow()
  eyh2total = gr2_total.GetEYhigh()
  eyl2total = gr2_total.GetEYlow()


  for i in range(grStat.GetN()): 

    ycomb = (y1[i] + y2[i])/2.

    eylstat = (abs(eyl1stat[i]) + abs(eyl2stat[i]))/2.
    eyhstat = (abs(eyh1stat[i]) + abs(eyh2stat[i]))/2.
    
    grStat.SetPoint(i, x1[i], ycomb)
    grStat.SetPointEYlow (i, eylstat)
    grStat.SetPointEYhigh(i, eyhstat)

    eylsyst = (abs(eyl1syst[i]) + abs(eyl2syst[i]))/2.
    eyhsyst = (abs(eyh1syst[i]) + abs(eyh2syst[i]))/2.

    grSyst.SetPoint(i, x1[i], ycomb)
    grSyst.SetPointEYlow (i, eylsyst)
    grSyst.SetPointEYhigh(i, eyhsyst)
    
    eyltotal = math.sqrt(eylstat*eylstat + eylsyst*eylsyst)
    eyhtotal = math.sqrt(eyhstat*eyhstat + eyhsyst*eyhsyst)
    grTotal.SetPoint(i, x1[i], ycomb)
    grTotal.SetPointEYlow (i, eyltotal)
    grTotal.SetPointEYhigh(i, eyhtotal)
      
  return (grStat, grSyst, grTotal)


if __name__ == '__main__':

    inFileName  = sys.argv[1]
    outFileName = sys.argv[2]
    listOfHist  = sys.argv[3]
    
    #copy input to output
    shutil.copyfile(inFileName, outFileName)

    #open output file
    f = r.TFile(outFileName,"UPDATE")
    
    #read in data from file
    data = np.genfromtxt(listOfHist, usecols=(0,1,2), dtype=None, 
                         names="outHistName, inHist1, inHist2")
    
    #loop over all distributions
    ndist = data['outHistName'].size

    if debug > 0: print 'ndist= ', ndist
    
    for id in range(ndist):
        if ndist == 1:
          outHistName = data['outHistName'].tostring()
          inHistName1 = data['inHist1'].tostring()
          inHistName2 = data['inHist2'].tostring()
        else:
          outHistName = data['outHistName'][id]
          inHistName1 = data['inHist1'][id]
          inHistName2 = data['inHist2'][id]
        
        if debug > 0: print 'outHistName,inHistName1,inHistName2 : ', outHistName, inHistName1, inHistName2

        grStat, grSyst, grTotal = combineGraphs(f, inHistName1, inHistName2, outHistName)

        #save graphs to output file
        grStat .Write()
        grSyst .Write()
        grTotal.Write()
        
    f.Close()

