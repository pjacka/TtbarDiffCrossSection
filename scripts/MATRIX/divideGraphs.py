
import sys
import ROOT as r
import numpy as n

inF = r.TFile(sys.argv[1])

#gr1 = inF.Get("ttbar_mass_total")
#gr2 = inF.Get("norm_ttbar_mass_total")
gr1 = inF.Get("leadingTop_pt_total")
gr2 = inF.Get("norm_leadingTop_pt_total")

npoints = gr1.GetN()

gr1X     = n.array(gr1.GetX())
#gr1XerrH = n.array(gr1.GetEXhigh()[:9])
gr1XerrH = n.zeros(npoints)
for i in range(9): gr1XerrH[i] = gr1.GetEXhigh()[i]
#gr1XerrL = n.array(gr1.GetEXlow())
gr1XerrL = n.zeros(npoints)
for i in range(9): gr1XerrL[i] = gr1.GetEXlow()[i]
gr2X     = n.array(gr2.GetX())
#gr2XerrH = n.array(gr2.GetEXhigh())
gr2XerrH = n.zeros(npoints)
for i in range(9): gr2XerrH[i] = gr2.GetEXhigh()[i]
#gr2XerrL = n.array(gr2.GetEXlow())
gr2XerrL = n.zeros(npoints)
for i in range(9): gr2XerrL[i] = gr2.GetEXlow()[i]

gr1YerrH = n.zeros(npoints)
for i in range(9): gr1YerrH[i] = gr1.GetEYhigh()[i]
gr1YerrL = n.zeros(npoints)
for i in range(9): gr1YerrL[i] = gr1.GetEYlow()[i]
gr2YerrH = n.zeros(npoints)
for i in range(9): gr2YerrH[i] = gr2.GetEYhigh()[i]
gr2YerrL = n.zeros(npoints)
for i in range(9): gr2YerrL[i] = gr2.GetEYlow()[i]

gr1a = n.array(gr1.GetY())
gr2a = n.array(gr2.GetY())
gr1widths = gr1XerrH + gr1XerrL
gr2widths = gr2XerrH + gr2XerrL

grRatio = gr1a / gr2a

print gr1a
print gr2a
print grRatio
print gr1X
print gr1XerrH
print gr1XerrL
print gr2X
print gr2XerrH
print gr2XerrL
print gr1widths
print gr2widths
print gr1a/gr1widths/gr2a
print gr2a/gr2widths/gr1a #!!!this looks fine

print "spectra: absolute and normalized/width:"
print gr1a
print gr2a/gr2widths

print "errors: absolute and normalized/width (high and low):"
print gr1YerrH
print gr2YerrH/gr2widths
print gr1YerrL
print gr2YerrL/gr2widths
