#!/usr/bin/env python

#------------
#  to be used for: combine  top pT/y and tbar pT/y histograms into one random top_pT/y histogram
#                  (random pT=(pT_top+pT_tbar)/2)
#      - combination is performed for graphs with stat, syst and total errors:
#          - for stat: assume variables are 100% correlated --> linear sum of errors
#                  - taken from ROOT file (i.e. originaly from MATRIX file in results/ dir)
#          - for syst:
#                - obtain sum of histograms for each scale separately
#                    - taken from MATRIX files in run_<X>/result/result..MATRIX.NNLO.distribution/CV/
#                - take min and max out of scale variations for each bin
#          - total: sum in quadrature stat and syst errors
#  - it copies input file to output and create a new histogram 'random top pT' in output file
#
# asssumptions:
#  - .dat files from result directory are in a given (./) directory
#  - files specific for each scale are in CV/scale.X.X directories
#------------

import sys
import os
import shutil
import math 
import array as ar
import numpy as np
import ROOT as r


debug = 10

dirWithScales = 'CV/'
relPrecis=0.001

#directory names with individual scale results
scales={ 'scale1' : 'scale.0.0',
         'scale2' : 'scale.1.1',
         'scale3' : 'scale.2.2',
         'scale4' : 'scale.3.3',
         'scale5' : 'scale.4.4',
         'scale6' : 'scale.5.5',
         'scale7' : 'scale.6.6' }
#nominal scale
scaleNom = 'scale4'


def combineGraphs(f, outHistName, inROOTHistName1, inROOTHistName2, inScaleHistName1, inScaleHistName2):

  #read in values + stat. uncertainties from ROOT file
  gr1_stat  = f.Get(inROOTHistName1 + "_stat")
  gr2_stat  = f.Get(inROOTHistName2 + "_stat")

  #make sure Nbins is the same for both distributions
  assert gr1_stat.GetN()  == gr2_stat.GetN()
  
  grStat   = gr1_stat.Clone(outHistName + "_stat")
  grSyst   = gr1_stat.Clone(outHistName + "_syst")
  grTotal  = gr1_stat.Clone(outHistName + "_total")
  
  x1 =   gr1_stat.GetX()
  x2 =   gr2_stat.GetX()

  y1 =   gr1_stat.GetY()
  y2 =   gr2_stat.GetY()

  eyh1stat = gr1_stat.GetEYhigh()
  eyl1stat = gr1_stat.GetEYlow()
  eyh2stat = gr2_stat.GetEYhigh()
  eyl2stat = gr2_stat.GetEYlow()

  #read in values from files corresponding to different scales and combine
  grScale={}
  for sck in scales.keys():

    #read in inScaleHistName1
    gr1 = createGraph( os.path.join(dirWithScales, scales[sck], inScaleHistName1) )

    #read in inScaleHistName2
    gr2 = createGraph( os.path.join(dirWithScales, scales[sck], inScaleHistName2) )

    #make sure number of points the same (and the same as in ROOT file)
    assert gr1.GetN() == gr2.GetN()
    assert gr1.GetN() == gr1_stat.GetN()

    #combine graphs
    grScale[sck] = gr1.Clone(outHistName + '_' + sck)

    x1S  = gr1.GetX()
    x2S  = gr2.GetX()
    y1S  = gr1.GetY()
    y2S  = gr2.GetY()
    ex1S = gr1.GetEX()
    ex2S = gr2.GetEX()    
    ey1S = gr1.GetEY()
    ey2S = gr2.GetEY()
    
    for i in range(grScale[sck].GetN()): 

      #cross-checks
      assert relDiff(x1S[i], x1[i], relPrecis)
      assert relDiff(x2S[i], x1[i], relPrecis)
      assert relDiff(ex1S[i], ex2S[i], relPrecis)
      
      ycomb = (y1S[i] + y2S[i])/2.
      ey    = abs(ey1S[i] + ey2S[i])/2.
      
      grScale[sck].SetPoint(i, x1S[i], ycomb)
      grScale[sck].SetPointError(i, ex1S[i], ey)

      
  #create final graphs
  for i in range(grStat.GetN()): 

    #cross-check
    assert relDiff(x1[i], x2[i], relPrecis)

    ycomb = (y1[i] + y2[i])/2.

    eylstat = abs(eyl1stat[i] + eyl2stat[i])/2.
    eyhstat = abs(eyh1stat[i] + eyh2stat[i])/2.
    
    grStat.SetPoint(i, x1[i], ycomb)
    grStat.SetPointEYlow (i, eylstat)
    grStat.SetPointEYhigh(i, eyhstat)

    #get min/max values from scale graphs to get syst uncertainty
    xs = r.Double(0.0)
    ys = r.Double(0.0)
    yscale_nom = 0.0
    yscale=[]
    for s in scales.keys():
      grScale[s].GetPoint(i,xs,ys)
      yscale.append(float(ys))
      if (s == scaleNom): yscale_nom = float(ys)
      
    eyhsyst = abs(max(yscale) - yscale_nom)
    eylsyst = abs(yscale_nom - min(yscale))
    
    #cross-check the nominal scale values from ROOT file and from scale histograms are the same
    assert relDiff(ycomb, yscale_nom, relPrecis)
    
    grSyst.SetPoint(i, x1[i], ycomb)
    grSyst.SetPointEYlow (i, eylsyst)
    grSyst.SetPointEYhigh(i, eyhsyst)
    
    eyltotal = math.sqrt(eylstat*eylstat + eylsyst*eylsyst)
    eyhtotal = math.sqrt(eyhstat*eyhstat + eyhsyst*eyhsyst)
    grTotal.SetPoint(i, x1[i], ycomb)
    grTotal.SetPointEYlow (i, eyltotal)
    grTotal.SetPointEYhigh(i, eyhtotal)

  if debug > 3:
    printGraphInfo(grStat, debug)
    printGraphInfo(grSyst, debug)
    printGraphInfo(grTotal, debug)
    
  return (grStat, grSyst, grTotal)


def createGraph(distribFileName):
    
  #read in data from file
  data = np.genfromtxt(distribFileName, usecols=(0,1,2), dtype=None, 
                       names="binEdge, val, errStat")

  if debug > 5:
      print 'data:'
      print data

  #create inputs to TGraphs
  binEdge = data['binEdge']
  binCenter  = np.zeros(binEdge.size - 1)
  binErr     = np.zeros(binEdge.size - 1)

  if debug > 6:
      print 'binEdge:'
      print binEdge

  for b in range(binEdge.size-1):
      binCenter[b]  = (binEdge[b+1] + binEdge[b]) / 2.
      binErr[b]     = (binEdge[b+1] - binEdge[b]) / 2.

  if debug > 7:
      print 'binCenter,binErr:'
      print binCenter
      print binErr
      
  #the last row values are just repeat of next-to-last row, so skip them
  val     = data['val'][:-1]
  statErr = data['errStat'][:-1]

  if debug > 3:
      print 'val,statErr:'
      print val
      print statErr

  #convert numpy objects to array.array
  bc    = ar.array("f",binCenter.tolist())
  be    = ar.array("f",binErr.tolist())
  v     = ar.array("f",val.tolist())
  stae  = ar.array("f",statErr.tolist())

  #create TGraph
  n = val.size

  grStat   = r.TGraphErrors(n, bc, v, be, stae)

  return (grStat)


def relDiff(x, y, relPrecis=0.001):
  if (x != 0):  return abs((x-y)/x) < relPrecis
  else:         return False


def printGraphInfo(gr, debug):
  n = gr.GetN()

  x   = gr.GetX()
  y   = gr.GetY()
  exh = gr.GetEXhigh()
  exl = gr.GetEXlow()
  eyh = gr.GetEYhigh()
  eyl = gr.GetEYlow()
    
  if (debug > 3):
    print 'gr, n, x, y, exh, exl, eyh, eyl:'
    print gr
    print n
    for i in range(n): print x[i], "  ",
    print
    for i in range(n): print y[i], "  ",
    print
    for i in range(n): print exh[i], "  ",
    print
    for i in range(n): print exl[i], "  ",
    print
    for i in range(n): print eyh[i], "  ",
    print
    for i in range(n): print eyl[i], "  ",
    print

if __name__ == '__main__':

    inFileName    = sys.argv[1]
    outFileName   = sys.argv[2]
    listOfHist    = sys.argv[3]
    
    #copy input to output
    shutil.copyfile(inFileName, outFileName)

    #open output file
    f = r.TFile(outFileName,"UPDATE")
    
    #read in names of hists to be summed
    data = np.genfromtxt(listOfHist, usecols=(0,1,2,3,4), dtype=None, 
                         names="outHistName, inROOTHist1, inROOTHist2, inScaleHist1, inScaleHist2")
    
    #loop over all distributions
    ndist = data['outHistName'].size

    if debug > 0: print 'ndist= ', ndist
    
    for id in range(ndist):
        if ndist == 1:
          outHistName      = data['outHistName'].tostring()
          inROOTHistName1  = data['inROOTHist1'].tostring()
          inROOTHistName2  = data['inROOTHist2'].tostring()
          inScaleHistName1 = data['inScaleHist1'].tostring()
          inScaleHistName2 = data['inScaleHist2'].tostring()
        else:
          outHistName      = data['outHistName'][id]
          inROOTHistName1  = data['inROOTHist1'][id]
          inROOTHistName2  = data['inROOTHist2'][id]
          inScaleHistName1 = data['inScaleHist1'][id]
          inScaleHistName2 = data['inScaleHist2'][id]
        
        if debug > 0: print 'outHistName,inROOTHistName1,inROOTHistName2,inScaleHistName1,inScaleHistName2 : ', \
           outHistName, inROOTHistName1, inROOTHistName2, inScaleHistName1, inScaleHistName2

        grStat, grSyst, grTotal = combineGraphs(f, outHistName, inROOTHistName1, inROOTHistName2, inScaleHistName1, inScaleHistName2)

        #save graphs to output file
        grStat .Write()
        grSyst .Write()
        grTotal.Write()
        
    f.Close()

