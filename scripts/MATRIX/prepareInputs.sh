#!/bin/bash

inDir=$1

level=NNLO
if [[ $# -ge 2 ]]; then level=$2; fi

echo "input dir: $1"
echo "calculation level: $level"

#copy files fromr results dir (distributions and total xsec file)
cp `ls $inDir/${level}-run/distributions__${level}*/*ATLAS*dat|grep -v norm|grep -v 'dd'` ./
cp $inDir/${level}-run/rate_*${level}*.dat .

#copy scale files from input dir
cp -r $inDir/CV .

#delete unused files 
rm -rf CV/scale.*/{LO,NLO,NNLO}.*
