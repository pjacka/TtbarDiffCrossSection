#!/usr/bin/env python

#------------
#
# asumption: 'total_cross_section' is the label for total xsection graph
#------------

import numpy as np
import array as ar
import ROOT as r
import math as m

import sys

debug = 10

def createGraphs(distribFileName):
    
  #read in data from file
  data = np.genfromtxt(distribFileName, usecols=(0,1,2,3,5), dtype=None, 
                       names="binEdge, val, errStat, minScale, maxScale",
                       skip_header=1)

  if debug > 5:
      print 'data:'
      print data

  #create inputs to TGraphs
  binEdge = data['binEdge']
  binCenter  = np.zeros(binEdge.size - 1)
  binErrLow  = np.zeros(binEdge.size - 1)
  binErrHigh = np.zeros(binEdge.size - 1)

  if debug > 1:
      print 'binEdge:'
      print binEdge


  for b in range(binEdge.size-1):
      binCenter[b]  = (binEdge[b+1] + binEdge[b]) / 2.
      binErrLow[b]  = (binEdge[b+1] - binEdge[b]) / 2.
      binErrHigh[b] = (binEdge[b+1] - binEdge[b]) / 2.

  if debug > 2:
      print 'binCenter,ErrLow,ErrHigh:'
      print binCenter
      print binErrLow
      print binErrHigh
      
  #the last row values are just repeat of next-to-last row, so skip them
  val = data['val'][:-1]

  statErr      = data['errStat'][:-1]
  systErrLow   = data['val'][:-1] - data['minScale'][:-1]
  systErrHigh  = data['maxScale'][:-1] - data['val'][:-1]  
  totalErrLow  = np.sqrt( np.power(statErr,2) + np.power(systErrLow ,2) )
  totalErrHigh = np.sqrt( np.power(statErr,2) + np.power(systErrHigh,2) )

  if debug > 3:
      print 'val,statErr,systErrLow,systErrHigh,totalErrLow,totalErrHigh:'
      print val
      print statErr
      print systErrLow
      print systErrHigh
      print totalErrLow
      print totalErrHigh


  #convert numpy objects to array.array
  bc    = ar.array("f",binCenter.tolist())
  bel   = ar.array("f",binErrLow.tolist())
  beh   = ar.array("f",binErrHigh.tolist())
  v     = ar.array("f",val.tolist())
  stae  = ar.array("f",statErr.tolist())
  sysel = ar.array("f",systErrLow.tolist())
  syseh = ar.array("f",systErrHigh.tolist())
  totel = ar.array("f",totalErrLow.tolist())
  toteh = ar.array("f",totalErrHigh.tolist())
  
  #create TGraphs
  n = val.size

  grStat   = r.TGraphAsymmErrors(n, bc, v, bel, beh, stae , stae)
  grSys    = r.TGraphAsymmErrors(n, bc, v, bel, beh, sysel, syseh)
  grTotal  = r.TGraphAsymmErrors(n, bc, v, bel, beh, totel, toteh)

  return (grStat, grSys, grTotal)

#different type of input file used as an input for total xsection
#in standard 'total_rate__NNLO_QCD.dat' file is unextrapolated cross-section
#we want to use extrapolated values, so using 'rate_extrapolated_NNLO_QCD.dat'
def getXsec(distFile): 

    if debug > 0: print 'xsection file: ', distFile
    
    data = np.genfromtxt(distFile, usecols=(0,1,2,3), dtype=None, 
                         names="muR, muF, xsec, errStat", skip_header=1)
    
    if debug > 0: print 'xsection data: ', data
    
    condNominal = (data['muR']==1) * (data['muF']==1)
    xsec    = np.select( condlist= condNominal,
                         choicelist= data['xsec'] )
    errStat = np.select( condlist= condNominal,
                         choicelist= data['errStat'] )
 
    errScaleUp   = np.max(data['xsec']) - xsec
    errScaleDown = xsec - np.min(data['xsec'])  
    relErrScaleUp   = errScaleUp   / xsec * 100. 
    relErrScaleDown = errScaleDown / xsec * 100.
    
    print "cross-section: %7.1f +- %7.1f (stat.) +%7.1f -%7.1f (syst.)" % (xsec, errStat, errScaleUp, errScaleDown)
    print "cross-section (rel.uncert.): %7.1f +- %7.1f %% (stat.) +%7.1f -%7.1f %% (syst.)" % (xsec, errStat/xsec*100, relErrScaleUp, relErrScaleDown)

    if debug > 0 : print "scale uncertainties: +", relErrScaleUp, " %  ", relErrScaleDown, " %"

    #create TGraph
    n = 1
    totalErrLow  = m.sqrt(errScaleDown*errScaleDown + errStat*errStat)
    totalErrHigh = m.sqrt(errScaleUp*errScaleUp     + errStat*errStat)
    
    #convert numpy objects to array.array
    bc    = ar.array("f",[0.5])
    bel   = ar.array("f",[0.5])
    beh   = ar.array("f",[0.5])
    v     = ar.array("f",[xsec])
    stae  = ar.array("f",[errStat])
    sysel = ar.array("f",[errScaleDown])
    syseh = ar.array("f",[errScaleUp])
    totel = ar.array("f",[totalErrLow])
    toteh = ar.array("f",[totalErrHigh])

    if debug > 3:
      print 'val,statErr,systErrLow,systErrHigh,totalErrLow,totalErrHigh:'
      print xsec
      print errStat
      print errScaleDown
      print errScaleUp
      print totalErrLow
      print totalErrHigh
      
    grStat   = r.TGraphAsymmErrors(n, bc, v, bel, beh, stae , stae)
    grSys    = r.TGraphAsymmErrors(n, bc, v, bel, beh, sysel, syseh)
    grTotal  = r.TGraphAsymmErrors(n, bc, v, bel, beh, totel, toteh)

    return (grStat, grSys, grTotal)


    

if __name__ == '__main__':

    inFileName  = sys.argv[1]
    outFileName = sys.argv[2]

    #create output file
    f = r.TFile(outFileName,"RECREATE")
    
    #read in data from file
    data = np.genfromtxt(inFileName, usecols=(0,1), dtype=None, 
                         names="varName, distFile")
    
    #loop over all distributions
    ndist = data['varName'].size
    
    for id in range(ndist):
        var      = data['varName' ][id]
        distFile = data['distFile'][id]
        
        if debug > 0: print 'var,dist: ', var, distFile

        #special handling of total-xsection: taking from different type of file
        if var == 'total_cross_section':
          grStat, grSys, grTotal = getXsec(distFile)
        else:
          grStat, grSys, grTotal = createGraphs(distFile)

        #save graphs to output file
        grStat .Write(var + "_stat")
        grSys  .Write(var + "_syst")
        grTotal.Write(var + "_total")
        
    f.Close()

