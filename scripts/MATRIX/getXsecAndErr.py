#
#
#  get cross-section with stat and scale uncertainties
#
#  the input file has a form:
#     muR   muF   cross-section  num_err
#
import sys
import numpy as np

debug = 0

def getXsec(xsecFile): 

    if debug > 0: print secFile
    
    data = np.genfromtxt(xsecFile, usecols=(0,1,2,3), dtype=None, 
                         names="muR, muF, xsec, errStat", skip_header=1)
    
    if debug > 0: print data
    
    condNominal = (data['muR']==1) * (data['muF']==1)
    xsec    = np.select( condlist= condNominal,
                         choicelist= data['xsec'] )
    errStat = np.select( condlist= condNominal,
                         choicelist= data['errStat'] )

    errScaleUp   = (np.max(data['xsec']) - xsec) 
    errScaleDown = (np.min(data['xsec']) - xsec) 
    relErrScaleUp   = errScaleUp   / xsec * 100. 
    relErrScaleDown = errScaleDown / xsec * 100.
    
    print "cross-section: %7.1f +- %7.1f (stat.) +%7.1f %7.1f (syst.)" % (xsec, errStat, errScaleUp, errScaleDown)
    print "cross-section (rel.uncert.): %7.1f +- %7.1f %% (stat.) +%7.1f %7.1f %% (syst.)" % (xsec, errStat/xsec*100, relErrScaleUp, relErrScaleDown)
    
    if debug > 0 : print "scale uncertainties: +", relErrScaleUp, " %  ", relErrScaleDown, " %"


if __name__ == '__main__':

    inFileName  = sys.argv[1]

    #read in data from file
    data = np.genfromtxt(inFileName, usecols=(0,1), dtype=None, 
                         names="scaleDef, xsecFile",encoding=None)
    
    #loop over all distributions
    ndef = data['scaleDef'].size
    
    for id in range(ndef):
        scaleDef = data['scaleDef' ][id]
        xsecFile = data['xsecFile'][id]
        
        if debug > 0: print 'scaleDef, xsecFile: ', scaleDef, xsecFile

        print 'scaleDef: ', scaleDef
        
        getXsec(xsecFile)

