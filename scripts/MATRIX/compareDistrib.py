
#
#
# TODO: set range according min-error and max+error
#

import sys
import math
import ROOT as r
import numpy as n

debug = 10

dictOfHists={"total_cross_section_total" : "#sigma(t#bar{t})",
             "leadingTop_pt_total" : "p_{T}^{1} [GeV]",
             "subleadingTop_pt_total" : "p_{T}^{2} [GeV]",
             "ttbar_y_total" : "y(t#bar{t})",
             "ttbar_mass_total" : "m(t#bar{t}) [GeV]"}

listOfColors = [r.kTeal-6, r.kViolet-3, r.kYellow+3, r.kAzure+3, r.kRed-3, 
                r.kPink+3, r.kBlue-3, r.kSpring-6, r.kMagenta+3, r.kCyan-3, 
                r.kGreen+3, r.kOrange+3 ] 


def plotHists(inputs, grname):

    grInfo = inputs[grname]
    
    c = r.TCanvas("c", "canvas",0,0,800,800)
    c.cd()

    #setup pad1 and pad2    
    pad1 = r.TPad("pad1","pad1",0,0.33,1,1,0,0,0)
    pad2 = r.TPad("pad2","pad2",0,0,1,0.33,0,0,0)
    pad2.SetBottomMargin(0.3)
    pad2.SetTopMargin(0)
    pad2.SetRightMargin(0.05)
    pad2.SetGridy(1)
    pad2.SetTicks()
    pad1.SetBottomMargin(0.)
    pad1.SetTopMargin(0.05)
    pad1.SetRightMargin(0.05)
    pad1.SetTicks()
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    pad2.cd()

    #plot hists
    pad1.cd()

    #determine min and max ranges
    gr1 = grInfo[0]['graph']
    rangeXmin = gr1.GetX()[0] - gr1.GetErrorXlow(0)
    rangeXmax = gr1.GetX()[gr1.GetN()-1] + gr1.GetErrorXhigh(gr1.GetN()-1)
    ymin = 100000
    ymax = 0
    for gi in grInfo:
        l  = gi['label']
        gr = gi['graph']
        for ibin in range(gr.GetN()):
            y       = gr.GetY()[ibin]
            if y > ymax :  ymax = y
            if y < ymin :  ymin = y
            
    #set y-range by 10% wider than min:max
    ymin = ymin - 0.1*(ymax-ymin)
    if ymin < 0 : ymin = 0
    ymax = ymax + 0.1*(ymax-ymin)
    h2 = r.TH2F("h2","",100, rangeXmin, rangeXmax, 100, ymin, ymax)
    h2.SetStats(r.kFALSE)
    h2.GetXaxis().SetTitleSize(0.13)
    h2.GetXaxis().SetLabelSize(0.123)
    h2.GetYaxis().SetTitleSize(0.06)
    h2.GetYaxis().SetTitleOffset(0.8)
    h2.GetYaxis().SetLabelSize(0.04)
    h2.GetYaxis().SetTitle("d#sigma / dX [fb]")
    h2.GetXaxis().SetTitle( grname )
    h2.Draw()
    
    id=1
    gr_nom = None
    legend = r.TLegend(0.6,0.6, 0.9,0.9)
    for gi in grInfo:
        l  = gi['label']
        gr = gi['graph']
        gr.SetLineColor(listOfColors[id-1])
        gr.SetLineWidth(2)
        gr.SetTitle("")
        #h.GetXaxis().SetTitle("C_{i}/#Lambda^{2} [1/TeV^{2}]")
        #h.GetXaxis().SetRangeUser(-10.,10.)
        legend.AddEntry(gr, l)
        if id==1:   gr.Draw("P SAME")
        else:       gr.Draw("P SAME")
        if l.find('NNPDF')  >= 0: #nominal 
            gr_nom = gr
        id += 1
        
    #add legend
    legend.Draw()
    pad1.Update()
    pad1.RedrawAxis("g")
    pad1.RedrawAxis()
    
    #plot ratios
    pad2.cd()
    ratioMin = 1
    ratioMax = 0
    id=1
    for gi in grInfo:
        if debug > 3: print 'gi: ' , gi
        l  = gi['label']
        gr = gi['graph']
        if debug > 3: print gr
        grratio = gr.Clone()
        #if not hratio.GetSumw2N() > 0: hratio.Sumw2()
        #grratio.Divide(h_nom)
        grratio.GetXaxis().SetTitleSize(0.13)
        grratio.GetXaxis().SetLabelSize(0.123);
        grratio.GetYaxis().SetTitleSize(0.09)
        grratio.GetYaxis().SetTitleOffset(0.5)
        grratio.GetYaxis().SetLabelSize(0.07);
        grratio.GetYaxis().SetTitle("PDF(i) / NNPDF")
        for ibin in range(grratio.GetN()):
            y       = gr.GetY()[ibin]
            errLow  = gr.GetErrorYlow(ibin)
            errHigh = gr.GetErrorYhigh(ibin)
            nom_y       = gr_nom.GetY()[ibin]
            nom_errLow  = gr_nom.GetErrorYlow(ibin)
            nom_errHigh = gr_nom.GetErrorYhigh(ibin)

            if debug > 5: print y,nom_y, errLow, nom_errLow

            ratio = y/nom_y
            grratio.SetPoint      (ibin, gr.GetX()[ibin], ratio )
            #combined error:
            #grratio.SetPointEYlow (ibin, math.sqrt( math.pow(ratio,2) * ( math.pow(errLow /y,2) + math.pow(nom_errLow /nom_y,2) ) ) )
            #grratio.SetPointEYhigh(ibin, math.sqrt( math.pow(ratio,2) * ( math.pow(errHigh/y,2) + math.pow(nom_errHigh/nom_y,2) ) ) )
            grratio.SetPointEYlow (ibin, errLow /y )
            grratio.SetPointEYhigh(ibin, errHigh/y )

            if ratio > ratioMax :  ratioMax = ratio
            if ratio < ratioMin :  ratioMin = ratio
            
        if debug > 3: print grratio
        gi['ratio'] = grratio
        #if id==1:   
        #    if l.find('NNPDF') >=0 :  grratio.Draw() #nominal (SM hist) 
        #    else:                     grratio.Draw()
        #else:  
        #    if l.find('NNPDF') >=0 :  grratio.Draw("SAME")
        #    else:                     grratio.Draw("SAME")
        #id+=1

    #set y-range by 10% wider than min:max
    rangeMin = ratioMin - 0.1*(ratioMax-ratioMin)
    rangeMax = ratioMax + 0.1*(ratioMax-ratioMin)
    #rangeMin = 0.5
    #rangeMax = 1.5
    
    h2ratio = r.TH2F("h2ratio","",100, rangeXmin, rangeXmax, 100, rangeMin, rangeMax)
    h2ratio.SetStats(r.kFALSE)
    h2ratio.GetXaxis().SetTitleSize(0.13)
    h2ratio.GetXaxis().SetLabelSize(0.123);
    h2ratio.GetYaxis().SetTitleSize(0.09)
    h2ratio.GetYaxis().SetTitleOffset(0.5)
    h2ratio.GetYaxis().SetLabelSize(0.07);
    h2ratio.GetYaxis().SetTitle("PDF(i) / NNPDF31")
    h2ratio.GetXaxis().SetTitle( dictOfHists[grname] )
    h2ratio.Draw()
    
    id=1
    for gi in grInfo:
        grratio = gi['ratio']
        l       = gi['label']
        
        if id==1:   
            if l.find('NNPDF') >=0 :  grratio.Draw("P SAME") #nominal (SM hist) 
            else:                     grratio.Draw("P SAME")
        else:  
            if l.find('NNPDF') >=0 :  grratio.Draw("P SAME")
            else:                     grratio.Draw("P SAME")
        id+=1
        
    pad2.RedrawAxis("g")
    pad2.RedrawAxis()
    c.SaveAs( grname + ".png" ) 
    c.SaveAs( grname + ".pdf" ) 
    
    #if you want to access hist --> need to return also legend, otherwise it's lost
    #return c,legend


    
if __name__ == '__main__':
    
    nFiles=len(sys.argv)-1
    inputs={}
    for id in range(nFiles):
        fileName,label = sys.argv[id+1].split(':')
        inF = r.TFile(fileName)
        for grname in dictOfHists.keys():
            gr = inF.Get(grname)
            if grname in inputs.keys():
                inputs[grname].append({'label':label,'graph':gr})
            else:
                inputs[grname] = []
                inputs[grname].append({'label':label,'graph':gr})

    print "inputs: ", inputs
    
    for grname in inputs.keys():
        plotHists(inputs,grname)
    
    
