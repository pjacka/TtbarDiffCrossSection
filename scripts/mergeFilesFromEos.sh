#!/bin/bash 
function haddFiles () {
	local prefix=$1
	local eosdir=$2
	local foldername=$3
	local inputfoldername=$4
        local outputfilename=$5

	mkdir -p $foldername
	cd $foldername
        eos ls $eosdir | grep $inputfoldername
	directories=(`eos ls $eosdir | grep $inputfoldername`)
	#echo ${directories[*]}
	i=0
	for dir in "${directories[@]}"
	do
#   echo `eos ls $eosdir$dir`
	   x=`eos ls $eosdir$dir`
	   if [ -n "$x" ]; then
	    y=(`eos ls $eosdir$dir`)
	    j=0
	    for z in ${y[@]}
	    do
	    samples[i]=$prefix$eosdir$dir"/"${y[j]}
	    #samples[i]=$prefix$eosdir$dir`eos ls $eosdir$dir`
	    j=$j+1
	    i=$i+1
	    done
	   fi
   
   
	done
	echo ${samples[*]}
	#hadd -f $outputfilename ${samples[*]}
	cd ..
}

function haddFiles2 () {
	local prefix=$1
	local eosdir=$2
	local foldername=$3
	local inputfoldername=$4
        local outputfilename=$5

	mkdir -p $foldername
	cd $foldername
        eos ls $eosdir | grep $inputfoldername
        mkdir files
	directories=(`eos ls $eosdir | grep $inputfoldername`)
        echo ${directories[*]}
	#echo ${directories[*]}
	i=0
	for dir in "${directories[@]}"
	do
#   echo `eos ls $eosdir$dir`
	   x=`eos ls $eosdir$dir`
	#	echo `eos ls $eosdir$dir`
	   if [ -n "$x" ]; then
	    y=(`eos ls $eosdir$dir`)
	    j=0
	    for z in ${y[@]}
	    do
	    samples[i]=$prefix$eosdir$dir"/"${y[j]}
		echo ${samples[i]}
            xrdcp ${samples[i]} files/
	    j=$j+1
	    i=$i+1
	    done
	   fi
   
   
	done
	#echo ${samples[*]}
	#hadd -f $outputfilename files/*
        #rm -r files
	cd ..
}




#maindir=/afs/cern.ch/work/p/pjacka/minitrees
maindir=/afs/cern.ch/work/j/jpalicka/minitrees
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160223_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160300_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160523_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160525_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160607_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160617_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160617_ntuples/add2016_02/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160723_ntuples/
eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160804_ntuples/
prefix=root://eosatlas/
currentdir=`echo $PWD`
cd $maindir

################################################
# data15_16 DAOD_TOPQ4 
################################################
haddFiles2 $prefix $eosdir data15_16.DAOD_TOPQ4_test physics_Main.DAOD_TOPQ4 allhad.boosted.output.root
#mkdir -p data15.DAOD_EXOT3
#cd data15.DAOD_EXOT3
#hadd -f allhad.boosted.output.root ../user.*physics_Main.DAOD_EXOT3*/*
#cd ..


################################################
# mc15_13TeV Pythia8EvtGen JZxW samples
################################################

#i=6
#for (( a=361026 ; $a-361029 ; a=$a+1 ))
#do 
#haddFiles $prefix $eosdir mc15_13TeV.${a}.Pythia8EvtGen.jetjet_JZ${i}W.TOPQ1 $a allhad.boosted.output.root
#i=$(($i + 1))
#mkdir -p mc15_13TeV.${a}.Pythia8EvtGen.jetjet_JZ${i}W.TOPQ1
#targetdir=mc15_13TeV.${a}.*
#cd $targetdir
#hadd -f allhad.boosted.output.root ../user.pjacka.${a}*/*
#cd .. 
#done


################################################
# mc15_13TeV zprime tt samples
################################################

#haddFiles2 $prefix $eosdir mc15_13TeV.301325.Pythia8EvtGen.zprime1000_tt.DAOD_TOPQ1 301325 allhad.boosted.output.root
#haddFiles2 $prefix $eosdir mc15_13TeV.301326.Pythia8EvtGen.zprime1250_tt.DAOD_TOPQ1 301326 allhad.boosted.output.root
#haddFiles2 $prefix $eosdir mc15_13TeV.301327.Pythia8EvtGen.zprime1500_tt.DAOD_TOPQ1 301327 allhad.boosted.output.root




################################################
# mc15_13TeV 410007 ttbar allhad
################################################
#haddFiles2 $prefix $eosdir mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1 410007 allhad.boosted.output.root
#mkdir -p mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1
#cd mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1
#hadd -f allhad.boosted.output.root ../user.*.410007*/*
#cd ..

################################################
# mc15_13TeV 410000 ttbar nonallhad
################################################
#haddFiles2 $prefix $eosdir mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1 410000 allhad.boosted.output.root
#mkdir -p mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1
#cd mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1
#hadd -f allhad.boosted.output.root ../user.*.410000*/*
#cd ..

################################################
# mc15_13TeV 410013 Wt_inclusive_top
################################################
#haddFiles2 $prefix $eosdir mc15_13TeV.410013.PowhegPythiaEvtGen.Wt_inclusive_top.TOPQ1 410013 allhad.boosted.output.root

################################################
# mc15_13TeV 410014 Wt_inclusive_antitop
################################################
#haddFiles2 $prefix $eosdir mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1 410014 allhad.boosted.output.root

#haddFiles2 $prefix $eosdir mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1 410014.PowhegPythiaEvtGen.DAOD_TOPQ1.e3753_s2608_s2183_r7326_r6282_p2516 allhad.boosted.output.root
#mkdir -p mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1
#cd mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1
#hadd -f allhad.boosted.output.root ../user.*.410013*/*
#cd ..

cd $currentdir






