#!/bin/bash
currentDir=$PWD
 baseDir=${TTBAR_PATH}/TtbarDiffCrossSection/filelists
splitDir=${TTBAR_PATH}/TtbarDiffCrossSection/filelists/SPLITTED

cd $baseDir

#defaults 
dir=MC23a.601237.PhPy8EG
nOutputFiles=3

if [ $# -ge 1 ]; then           dir=$1; fi
if [ $# -ge 2 ]; then nOutputFiles=$2; fi

mkdir -p $splitDir 

for d in $dir; do
	#echo $d
  
  isempty=(`ls $d`)
  if [ ${#isempty[@]} -eq 0 ]; then continue; fi
  mkdir -p  $splitDir/$d
  rm -f    $splitDir/$d/*
  textfiles=(`ls $d/*`)
  #echo $textfiles
  files=(`cat ${textfiles[@]}`)
	nfiles=${#files[@]}
	#echo $nfiles
	size=`expr $nfiles - 1 `
	size=`expr $size / $nOutputFiles + 1`
	#echo $nOutputFiles
	#echo $size
	n=${#files1[@]}
	#echo $n
	lowerbound=0
	#echo ${files[*]} > $currentDir/output2.txt
	for((i=0;i<$nOutputFiles;i+=1))
	do
		#echo $i
		smallfiles=("${files[@]:$lowerbound:$size}")
		#echo $lowerbound
		lowerbound=`expr $lowerbound + $size`
		if [ ${#smallfiles[@]} -eq 0 ]; then continue; fi
		suffix='00'
		if [ $i -ge 10 ]; then suffix='0'; fi
		if [ $i -ge 100 ]; then suffix=''; fi 
		printf "%s\n" "${smallfiles[@]}" > $splitDir/$d/allhad.boosted.output.txt_$suffix$i
	done

done
cd $currentDir
