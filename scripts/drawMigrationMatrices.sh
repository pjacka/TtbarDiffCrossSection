#!/bin/bash                                                                                                                                             

dimension="ND"
debugLevel=2

systematicHistosPath=systematics_histosND
outputSubDir=migrationND

config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json

echo "your output will be saved in" ${TtbarDiffCrossSection_output_path}


levels=(
"PartonLevel"
"ParticleLevel"
)


variablesND=(
#"t1_pt_vs_t2_pt"
#"t1_y_vs_t2_y"
#"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
#"ttbar_y_vs_ttbar_mass_vs_t1_pt"
)

for level in ${levels[@]}
do
  for variable in ${variablesND[@]}
  do
    configBinning=${TTBAR_PATH}/TtbarDiffCrossSection/data/optBinningND/${variable}.env
  
    drawMigrationMatrix --mainDir ${TtbarDiffCrossSection_output_path} \
      --config ${config_lumi} \
      --configBinning ${configBinning} \
      --outputSubDir ${outputSubDir} \
      --variable ${variable} \
      --level ${level} \
      --systematicsHistosDir ${systematicHistosPath} \
      --dimension ${dimension} \
      --debugLevel ${debugLevel}

  done
done

dimension="1D"
systematicHistosPath=systematics_histos
outputSubDir=migration

variables=(
"total_cross_section"
"randomTop_pt"
"randomTop_y"
"t1_pt"
"t1_y"
"t2_pt"
"t2_y"
"ttbar_mass"
"ttbar_pt"
"ttbar_y"
"chi_ttbar"
"y_boost"
"pout"
"H_tt_pt"
"ttbar_deltaphi"
"cos_theta_star"
"cos_theta_star"
"z_tt_pt"
"y_star"
"t1_mass"
"t2_mass"
"delta_pt"
"t1_m_over_pt"
"t2_m_over_pt"
)

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
      
    drawMigrationMatrix --mainDir ${TtbarDiffCrossSection_output_path} \
      --config ${config_lumi} \
      --outputSubDir ${outputSubDir} \
      --variable ${variable} \
      --level ${level} \
      --systematicsHistosDir ${systematicHistosPath} \
      --dimension ${dimension} \
      --debugLevel ${debugLevel}

  done
done
