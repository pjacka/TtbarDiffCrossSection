#!/bin/bash

mainDir1=/mnt/nfs19/jacka/TTBarAllhadBoosted/xAnalysis_AT173_211207_DL1r_77_Contained80/
mainDir2=/mnt/nfs19/jacka/TTBarAllhadBoosted/xAnalysis_AT173_211207_DL1r_77_Contained80/
mc_production1="MC16a"
mc_production2="MC16d"

outputDir=${TtbarDiffCrossSection_output_path}/pdf.${mc_production1}/pdf.${mc_production2}/comparisonOfSpectraFromDifferentProductions

debugLevel=1



levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
"total_cross_section"
"randomTop_pt"
"randomTop_y"
"t1_pt"
"t1_y"
"t2_pt"
"t2_y"
"ttbar_mass"
"ttbar_pt"
"ttbar_y"
"chi_ttbar"
"y_boost"
"pout"
"H_tt_pt"
"ttbar_deltaphi"
"cos_theta_star"
"delta_pt"
#"z_tt_pt"
#"y_star"
#"t1_mass"
#"t2_mass"
#"t1_m_over_pt"
#"t2_m_over_pt"
)


for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    compareSpectraFromDifferentProductions --mainDir1=${mainDir1} --mainDir2=${mainDir2} --mcProduction1=${mc_production1} --mcProduction2=${mc_production2} --level ${level} --variable ${variable} --outputDir ${outputDir} --debugLevel ${debugLevel}
  done
done


