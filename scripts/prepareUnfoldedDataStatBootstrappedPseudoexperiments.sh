#!/bin/bash

nReplicas=10000
if [ $USER == pjacka ] || [ $USER == jacka ] ; then
nReplicas=10000                           
fi


config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env

levels=(
"PartonLevel"
"ParticleLevel"
)

config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env

echo "the output will be saved in "${TtbarDiffCrossSection_output_path}/Root

for level in ${levels[@]}
do
  prepareUnfoldedDataStatBootstrappedPseudoexperiments ${TtbarDiffCrossSection_output_path} $config_lumi $level $nReplicas
done

