#!/bin/bash 

function mergeData () {
	local prefix=$1
	local eosdir=$2
	local foldername1=$3
	local foldername2=$4
        local outputfilename=$5

        #eos ls $eosdir | grep $inputfoldername
	directories=(`eos ls $eosdir | grep "physics_Main.DAOD_TOPQ4"`)
        echo ${#directories[@]}
	#echo ${directories[*]}
	i=0
	j=0
        for dir in "${directories[@]}"
	do
	   x=`eos ls $eosdir$dir`
	   if [ -n "$x" ]; then
		sid=${dir#user.*.}
		dsid=`expr substr $sid 1 8`
		echo $dsid
		if [ $dsid -lt 284485 ]; then
			echo data15
			directories15[i]=$dir
			echo $i
			echo ${directories15[i]}
			i=`expr $i + 1`
			
		else
			echo data16
			directories16[j]=$dir
			echo $j
			echo ${directories16[j]}
			j=`expr $j + 1`
		fi
	   fi

	done
#	mkdir -p $foldername1
#	cd $foldername1
#	i=0
#	for dir15 in "${directories15[@]}"
#	do
#   echo `eos ls $eosdir$dir`
#	   x=`eos ls $eosdir$dir15`
#	   if [ -n "$x" ]; then
#	    y=(`eos ls $eosdir$dir15`)
#	    j=0
#	    for z in ${y[@]}
#	    do
#	    samples15[i]=$prefix$eosdir$dir15"/"${y[j]}
#           #xrdcp ${samples[i]} files/
#	    j=`expr $j + 1`
#	    i=`expr $i + 1`
#	    done
#	   fi
#  	done
	

#	echo data15
#	hadd -f $outputfilename ${samples15[*]}
#	cd ..


	mkdir -p $foldername2
	cd $foldername2
	mkdir files
	i=0
	for dir16 in "${directories16[@]}"
	do
   echo `eos ls $eosdir$dir16`
	   x=`eos ls $eosdir$dir16`
	   if [ -n "$x" ]; then
	    y=(`eos ls $eosdir$dir16`)
	    j=0
	    for z in ${y[@]}
	    do
	    samples16[i]=$prefix$eosdir$dir16"/"${y[j]}
	    xrdcp ${samples16[i]} files/
	    j=`expr $j + 1`
	    i=`expr $i + 1`
	    done
	   fi
   	done
	

#	echo data16
#	#hadd -f $outputfilename ${samples16[*]}
#	cd ..


}




maindir=/afs/cern.ch/work/p/pjacka/minitrees
#maindir=/afs/cern.ch/work/j/jpalicka/minitrees
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160223_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160300_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160523_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160525_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160607_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160617_ntuples/
#eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160723_ntuples/
eosdir=/eos/atlas/user/c/cheny/top-phys/2016/20160804_ntuples/
prefix=root://eosatlas/
foldername1=data15.DAOD_TOPQ4
foldername2=data16.DAOD_TOPQ4
outputfilename=allhad.boosted.output.root
currentdir=`echo $PWD`
cd $maindir


mergeData $prefix $eosdir $foldername1 $foldername2 $outputfilename




cd $currentdir






