# prepare setup of atlas software

if [ -z $ATLAS_LOCAL_ASETUP_VERSION ]; 
then
   # do it only if it has not been done yet
   export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
   alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
   source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
else
   #  echo "\$ATLAS_LOCAL_ROOT_BASE and \$TestArea already configured"
   echo "Atlas environment already configured."
fi
