mainDir=${TtbarDiffCrossSection_output_path}
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
outputSubdir=samplesComparison

variables=(
#"pout"
"t1_pt"
"t2_pt"
"t1_y"
"t2_y"
"ttbar_pt"
"ttbar_mass"
#"ttbar_y"
)

for variable in ${variables[@]}
do
   #drawSamplesComparison ${mainDir}/ ${outputSubdir} ${config_files} ${config_lumi} $variable
   drawSamplesComparisonTagWPs ${mainDir}/ ${outputSubdir} ${config_files} ${config_lumi} $variable
done


variables=(
#"fatJet1_DNNHighLevelTopTagger_score"
#"fatJet2_DNNHighLevelTopTagger_score"
#"fatJet1_DNNContainedTopTagger_score"
#"fatJet2_DNNContainedTopTagger_score"
#"fatJet1_DNNInclusiveTopTagger_score"
#"fatJet2_DNNInclusiveTopTagger_score"
#"fatJet1_DNNTopoclusterTopTagger_score"
#"fatJet2_DNNTopoclusterTopTagger_score"
#"fatJet1_pt"
#"fatJet2_pt"
#"nBJets"
#"nFatJets"
#"nJets"
"jet1_pt"
"jet2_pt"
#"fatJet1_pt"
#"fatJet1_mass"
#"fatJet1_y"
#"leadingTop_mass"
#"leadingTop_pt"
#"leadingTop_y"
#"subleadingTop_pt"
#"fatJet2_pt"
#"fatJet2_y"
#"fatJet1_tau32"
#"fatJet2_tau32"
#"fatJet1_split23"
#"fatJet2_split23"
#"H_tt_pt_RecoLevel"
#"ttbar_Ht"
#"total_cross_section_RecoLevel"
#"filter_HT"
#"ttbar_mass_RecoLevel"
#"ttbar_pt_RecoLevel"
#"ttbar_y_RecoLevel"
#"t1_pt_RecoLevel"
#"t1_y_RecoLevel"
#"t2_pt_RecoLevel"
#"t2_y_RecoLevel"
)


#for variable in ${variables[@]}
#do
   #drawSamplesComparison_AllRegions ${mainDir}/ ${outputSubdir} ${config_files} ${config_lumi} $variable
#done

