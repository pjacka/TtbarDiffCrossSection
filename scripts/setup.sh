#!/bin/bash



HOSTNAME=`hostname`

export TTBAR_ANALYSIS_BASE_VERSION=247
export TTBAR_ANALYSIS_BASE='AnalysisBase'
export TTBAR_PATH=`pwd`

if [ $USER == zaplatilek ] ; then
  export TTBAR_EMAIL=zaplaota@fjfi.cvut.cz
  export TtbarDiffCrossSection_output_path=/raid7_atlas/zaplatilek/xAnalysis/xAnalysis_output/pJacka_run_output/     #ended by "/" !!!!
fi



if [ $USER == qitek ] ; then
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
  export TTBAR_EMAIL=jiri.kvita@cern.ch
  export TtbarDiffCrossSection_output_path=/raid7_atlas2/qitek/xAnalysis/
fi



if [ $USER == pberta ] || [ $USER == berta ] ; then
  export TtbarDiffCrossSection_output_path=/afs/cern.ch/user/p/pberta/Workspace/xAnalysis/
  export TTBAR_EMAIL=peter.berta@cern.ch
  if [[ "$HOSTNAME" == *particle.cz* ]] ; then
    #echo "you are in prague farm"
    export TtbarDiffCrossSection_output_path=/raid7_atlas2/berta/xAnalysis/
  fi
fi




if [ $USER == pjacka ] ; then
   export TtbarDiffCrossSection_output_path=/eos/user/p/pjacka/top-phys/dev/xAnalysis/
   export TTBAR_EMAIL=jacka@fzu.cz
fi



if [ $USER == jacka ] ; then
  export TTBAR_EMAIL=jacka@fzu.cz
  export TtbarDiffCrossSection_output_path=/mnt/nfs19/jacka/TTBarAllhadBoosted/topPolarization_AB_25_02_41_Date_250215_GN2v01_85_Contained80_allSys/
  # export TtbarDiffCrossSection_output_path=/mnt/nfs19/jacka/TTBarAllhadBoosted/topPolarization_AB_25_02_41_Date_250219_GN2v01_85_Contained80_allSys/

fi

if [ $USER == jpalicka ] ; then
  export TtbarDiffCrossSection_output_path=/raid7_atlas2/jpalicka/Out_xAnalysis/
  export TTBAR_EMAIL=jpalicka27@gmail.com
fi

if [ $USER == lysak ] ; then
  #export TtbarDiffCrossSection_output_path=/home/lysak/DiffXsecBoosted/xAnalysis/run/
  #export TtbarDiffCrossSection_output_path=/home/lysak/DiffXsecBoosted/xAnalysis/run/TESTS/PASS5/
  export TtbarDiffCrossSection_output_path=/home/lysak/DiffXsecBoosted/xAnalysis/xAnalysis_21.2/run/TESTS/PASS1
  export TTBAR_EMAIL=lysak@fzu.cz
fi

if [ $USER == hejbal ] ; then
  
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070819_CaloJets_MV2c10_77_DNNTaggerTopQuarkContained50_LJets/ 
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070819_TrackJets_MV2c10_77_DNNTaggerTopQuarkContained80_LJets_BugFixed/ 
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets/ 
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_muRF075_15/ 
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_muRF075_15_v2/ 
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_muRF0875_125/ 
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_muRF075_15_Nom_0875/ 
  
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_muRF05_2_Nom_0875/
  #export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_cutoffweights/

#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_new_ME_v1

#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_noHTcut_test
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_newxseccalc
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_noHTcut


#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_morePDF/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_morePDF_ver2/

#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_wider_m_window/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_wider_m_window_ver2/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_more_reco_plots/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_more_reco_plots_ver4/

#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_MV2c10_60_DNNTaggerTopQuarkInclusive80_LJets_new_ntuples/

#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r60_DNNTaggerTopQuarkInclusive80_LJets_new_ntuples/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_TEST_run_ala_MATRIX/

#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r85_DNNTaggerTopQuarkContained80_LJets_new_ntuples_bugfix/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r77_DNNTaggerTopQuarkContained80_LJets_topSFfix/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r77_DNNTaggerTopQuarkContained80_LJets_before_topSFfix/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r77_DNNTaggerTopQuarkInclusive50_LJets_new_ntuples/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_070120_TrackJets_DL1r60_DNNTaggerTopQuarkInclusive80_LJets_new_ntuples_topSFfix/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_010521_TrackJets_DL1r77_DNNTaggerTopQuarkContained80_LJets_wTrigger/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_010521_TrackJets_DL1r77_DNNTaggerTopQuarkContained80_LJets_w_o_Trigger/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_010521_TrackJets_DL1r77_DNNTaggerTopQuarkContained80_LJets_incl_sample_study/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_TEST_weights/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_AT173_210722_DL1r_77_Contained80/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_AT173_210722_DL1r_77_Contained80_newTriggers2017/
#export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/xAnalysis_AT173_210722_DL1r_77_Contained80_TODELETE
export TtbarDiffCrossSection_output_path=/mnt/nfs19/hejbal/DiffXsecBoosted/topPolarization_AB_25_02_09_Date_240613_DL1r_77_Contained80/



  export TTBAR_EMAIL=hejbal@fzu.cz
fi


#echo "hostname: "$HOSTNAME
if [[ "$HOSTNAME" == *particle.cz* ]] ; then
  echo "you are in prague farm" 
  source ${TTBAR_PATH}/TtbarDiffCrossSection/scripts/setup_atlas.sh
fi

#localSetupROOT ${ROOT_VERSION_for_local_setup}-${PLATFORM_for_local_setup} --skipConfirm

#if [ -d RootCore ]
#then
  #echo "sourcing RootCore/scripts/setup.sh"
  #source RootCore/scripts/setup.sh
#else
  #echo "there is no RootCore directory!!!"
#fi

#export LD_LIBRARY_PATH=${TTBAR_PATH}/RooUnfold-1.1.1:${LD_LIBRARY_PATH}
#export LD_LIBRARY_PATH=${TTBAR_PATH}/TtbarDiffCrossSection/RooUnfold_trunk:${LD_LIBRARY_PATH}
#export LD_LIBRARY_PATH=${TTBAR_PATH}/RooUnfold:${LD_LIBRARY_PATH}

echo "TTBAR_PATH: $TTBAR_PATH"
echo "TtbarDiffCrossSection_output_path: $TtbarDiffCrossSection_output_path"


