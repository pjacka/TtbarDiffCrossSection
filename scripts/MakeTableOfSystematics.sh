# Peter 
if [ $USER == pberta ] || [ $USER == berta ] ; then

signal="410007"
#signal="410234"

fi


# Petr
if [ $USER == pjacka ] || [ $USER == jacka ] ; then


#signal="410007"
#signal="410234"
signal="410506"
#signal="410008"
#signal="410045"
#signal="410160"


use_ttbar_SF=true
fi

# Roman
if [ $USER == rlysak ] || [ $USER == lysak ] ; then

#signal="410007"
#signal="410234"
signal="410506"
#signal="410008"
#signal="410045"
#signal="410160"

fi

# Honza
if [ $USER == jpalicka ] ; then

#signal="410007"
#signal="410234"
signal="410506"
#signal="410008"
#signal="410045"
#signal="410160"

fi




mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/macros/Unfolding.env
#nPseudoexperiments=1000


levels=(
"PartonLevel"
#"ParticleLevel"
)

variables=(
"topCandidate_pt_fine"
#"ttbar_mass_fine"
#"ttbar_pt_fine"
#"ttbar_deltaphi_fine"
#"ttbar_y_fine"
#"leadingTop_pt_fine"
#"subleadingTop_pt_fine"
#"leadingTop_y_fine"
#"subleadingTop_y_fine"
#"inclusive_top_pt_fine"
#"inclusive_top_y_fine"
#"randomTop_y_fine"
)

for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
		echo $variable
		MakeTableOfSystematics $mainDir $config_files $config_lumi $signal $variable $level
	done
done
