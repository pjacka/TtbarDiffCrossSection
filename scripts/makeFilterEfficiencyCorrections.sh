mainDir=$TtbarDiffCrossSection_output_path
config=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
debugLevel=2

makeFilterEfficiencyCorrections --mainDir ${mainDir} --config ${config} --debugLevel ${debugLevel}
