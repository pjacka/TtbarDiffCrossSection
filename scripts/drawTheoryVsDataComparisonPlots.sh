#!/bin/bash                                                                                                                                             

signal=410471
dimension="1D"
useABCD16=1
debugLevel=2

covStatName="covDataBasedOnlyDataStat"

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
covAllUncName="covDataBasedStatSysAndSigMod"
outputSubDir="Theory_vs_data_comparison_newModeling"

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
#covAllUncName="covDataBasedAllUnc"
#outputSubDir="Theory_vs_data_comparison_standard"

## This is for alternative method to propagate systematic ucertainties through unfolding. Unfolding corrections and MC background are shifted
#covAllUncName="covDataBasedAllUncAlternative"
#outputSubDir="Theory_vs_data_comparison_alternative"

## For MC based approach
#covStatName="covMCBasedOnlyDataStat"
#covAllUncName="covMCBasedAllUncAlternative"
#outputSubDir="Theory_vs_data_comparison_MCBased_alternative"


echo "your output will be saved in" ${TtbarDiffCrossSection_output_path}

dimension="1D"

drawTheoryVsDataComparisonPlots --mainDir ${TtbarDiffCrossSection_output_path} --signal ${signal} --outputSubDir ${outputSubDir} --dimension ${dimension} --useABCD16 ${useABCD16} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}
#dimension="2D"
#drawTheoryVsDataComparisonPlots --mainDir ${TtbarDiffCrossSection_output_path} --signal ${signal} --outputSubDir ${outputSubDir}_2D --dimension ${dimension} --useABCD16 ${useABCD16} --covAllUncName ${covAllUncName} --covStatName ${covStatName} --debugLevel ${debugLevel}
