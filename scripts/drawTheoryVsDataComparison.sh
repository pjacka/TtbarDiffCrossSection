#!/bin/bash                                                                                                                                             

signal=410471
dimension="1D"
useABCD16=1
debugLevel=2

systematicHistosPath=systematics_histos
outputSubdir=TheoryVsDataComparison

config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
multiDimensionalPlotsConfig=TtbarDiffCrossSection/data/MultiDimensionalPlotsConfig.env

covariancesDir="covariances"

covStatName="covDataBasedOnlyDataStat"

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
covAllUncName="covDataBasedStatSysAndSigMod"
outputSubDir="TheoryVsDataComparison_newModeling"

## This is for standard method to propagate systematic ucertainties through unfolding. Reco and truth (signal modeling) level spectra are shifted.
#covAllUncName="covDataBasedAllUnc"
#outputSubDir="Theory_vs_data_comparison_standard"

## This is for alternative method to propagate systematic ucertainties through unfolding. Unfolding corrections and MC background are shifted
#covAllUncName="covDataBasedAllUncAlternative"
#outputSubDir="Theory_vs_data_comparison_alternative"

## For MC based approach
#covStatName="covMCBasedOnlyDataStat"
#covAllUncName="covMCBasedAllUncAlternative"
#outputSubDir="Theory_vs_data_comparison_MCBased_alternative"


levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
#"total_cross_section"
#"randomTop_pt"
#"randomTop_y"
#"t1_pt"
#"t1_y"
#"t2_pt"
#"t2_y"
#"ttbar_mass"
#"ttbar_pt"
#"ttbar_y"
#"chi_ttbar"
#"y_boost"
#"pout"
#"H_tt_pt"
#"ttbar_deltaphi"
#"cos_theta_star"
##"z_tt_pt"
##"y_star"
##"t1_mass"
##"t2_mass"
##"delta_pt"
###"t1_m_over_pt"
###"t2_m_over_pt"
)

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    echo ${variable}
    drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} \
      --config ${config_lumi} \
      --outputSubDir ${outputSubDir} \
      --variable ${variable} \
      --level ${level} \
      --systematicsHistosDir ${systematicHistosPath} \
      --dimension ${dimension} \
      --covariancesDir ${covariancesDir} \
      --covAllUncName ${covAllUncName} \
      --covStatName ${covStatName} \
      --debugLevel ${debugLevel}

  done
done



outputSubDir="TheoryVsDataComparisonND_newModeling"
systematicHistosPath=systematics_histosND
outputSubdir=TheoryVsDataComparisonND

covariancesDir="covariancesND"

dimension="ND"


variablesND=(
#"t1_pt_vs_t2_pt"
#"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"t1_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
#"ttbar_y_vs_ttbar_pt"
#"t2_pt_vs_ttbar_mass"
#"ttbar_y_vs_t2_pt"
#"t2_y_vs_t2_pt"
#"top_pt_vs_ttbar_mass"
#"ttbar_y_vs_top_pt"
#"ttbar_y_vs_ttbar_mass_vs_t1_pt"
#"ttbar_y_vs_ttbar_pt_vs_ttbar_mass"
#"top_y_vs_top_pt"
#"ttbar_y_vs_top_y"
#"ttbar_y_vs_t1_y"
#"ttbar_y_vs_t2_y"
#"t1_y_vs_ttbar_mass"
#"t2_y_vs_ttbar_mass"
#"top_y_vs_ttbar_mass"
#"ttbar_pt_vs_ttbar_mass"
#"delta_eta_vs_ttbar_mass"
#"delta_y_vs_ttbar_mass"
#"delta_phi_vs_ttbar_mass"
"t1_y_vs_t2_y"
#"delta_y_vs_t1_y"
)

for level in ${levels[@]}
do
  for variable in ${variablesND[@]}
  do
    configBinning=${TTBAR_PATH}/TtbarDiffCrossSection/data/optBinningND/${variable}.env
    
    drawTheoryVsDataComparison --mainDir ${TtbarDiffCrossSection_output_path} \
      --config ${config_lumi} \
      --configBinning ${configBinning} \
      --multiDimensionalPlotsConfig ${multiDimensionalPlotsConfig} \
      --outputSubDir ${outputSubDir} \
      --variable ${variable} \
      --level ${level} \
      --systematicsHistosDir ${systematicHistosPath} \
      --dimension ${dimension} \
      --covariancesDir ${covariancesDir} \
      --covAllUncName ${covAllUncName} \
      --covStatName ${covStatName} \
      --debugLevel ${debugLevel}
  done
done




               
