macroPath="${TTBAR_PATH}/TtbarDiffCrossSection/macros/"
cd $macroPath

#DEFAULTS:
signal="410471"
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
inputDir="systematics_histos"
outputDir="generatorSystematics"
doSysPDF=1
shiftNominal=0
debugLevel=10


levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
"total_cross_section"
#"randomTop_pt"
#"randomTop_y"
#"t1_pt"
#"t1_y"
#"t2_pt"
#"t2_y"
#"ttbar_mass"
#"ttbar_pt"
#"ttbar_y"
#"chi_ttbar"
#"y_boost"
#"pout"
#"H_tt_pt"
#"ttbar_deltaphi"
#"cos_theta_star"
#"z_tt_pt"
#"y_star"
#"t1_mass"
#"t2_mass"
#"delta_pt"
#"t1_m_over_pt"
#"t2_m_over_pt"
#"concatenated"
)






# Ota
if [ $USER == zaplatilek ] || [ $USER == ozaplati ] ; then 
:
fi


# Peter 
if [ $USER == pberta ] || [ $USER == berta ] ; then
:
#signal="410007"
#signal="410234"

fi


# Petr
if [ $USER == pjacka ] || [ $USER == jacka ] ; then
:
fi

# Roman
if [ $USER == rlysak ] || [ $USER == lysak ] ; then
:
fi

# Honza
if [ $USER == jpalicka ] ; then
:

fi




for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
		cmd="runGeneratorSystematics --mainDir ${TtbarDiffCrossSection_output_path}/ --config $config_lumi --signal $signal --variable $variable --level $level --inputDir ${inputDir} --outputDir ${outputDir} --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"
		echo "Running $cmd"
		$cmd
	done
done



