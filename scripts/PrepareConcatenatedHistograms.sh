## Defalut parameters ! Don't change them. You can setup them below
signal="410506"
mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/macros/Unfolding.env
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
useBootstrapHistos=0


levels=(
"PartonLevel"
"ParticleLevel"
)





# Peter 
if [ $USER == pberta ] || [ $USER == berta ] ; then
signal="410506"
fi


# Petr
if [ $USER == pjacka ] || [ $USER == jacka ] ; then
useBootstrapHistos=0
fi

# Roman
if [ $USER == rlysak ] || [ $USER == lysak ] ; then
signal="410506"
fi

# Honza
if [ $USER == jpalicka ] ; then
signal="410506"
fi

echo "useBootstrapHistos: $useBootstrapHistos"

for level in ${levels[@]}
do
  PrepareConcatenatedHistograms $mainDir $config_files $config_lumi $config_binning $config_sysnames $signal $level $useBootstrapHistos
done
