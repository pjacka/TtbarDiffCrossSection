#!/bin/bash

mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env

levels=(
"PartonLevel"
"ParticleLevel"
)

variables=(
"total_cross_section"
"randomTop_pt"
"randomTop_y"
"t1_pt"
"t1_y"
"t2_pt"
"t2_y"
"ttbar_mass"
"ttbar_pt"
"ttbar_y"
"chi_ttbar"
"y_boost"
"pout"
"H_tt_pt"
"ttbar_deltaphi"
"cos_theta_star"
#"z_tt_pt"
#"y_star"
"t1_mass"
"t2_mass"
"delta_pt"
"t1_m_over_pt"
"t2_m_over_pt"
)

covariances=(
"covDataBasedStatAndSys"
"covDataBasedStatAndSysNormalized"
"covDataBasedStatAndSysAlternative"
"covDataBasedStatAndSysAlternativeNormalized"
"covDataBasedOnlyStat"
"covDataBasedOnlyMCStat"
"covDataBasedOnlyMCStatReco"
"covDataBasedOnlyStatNormalized"
"covDataBasedOnlyMCStatNormalized"
"covDataBasedOnlyMCStatRecoNormalized"
"covDataBasedStatSysAndSigMod"
"covDataBasedAllUnc"
"covDataBasedOnlySigMod"
"covDataBasedOnlyMEOriginal"
"covDataBasedOnlyPSOriginal"
"covDataBasedOnlyIFSROriginal"
"covDataBasedOnlyMEAndPSOriginal"
"covDataBasedOnlySigModOriginal"
"covDataBasedStatSysAndSigModNormalized"
"covDataBasedAllUncNormalized"
"covDataBasedOnlySigModNormalized"
"covDataBasedOnlyMEOriginalNormalized"
"covDataBasedOnlyPSOriginalNormalized"
"covDataBasedOnlyIFSROriginalNormalized"
"covDataBasedOnlyMEAndPSOriginalNormalized"
"covDataBasedOnlySigModOriginalNormalized"
"covDataBasedStatSysAndSigModAlternative"
"covDataBasedAllUncAlternative"
"covDataBasedStatSysAndSigModAlternativeNormalized"
"covDataBasedAllUncAlternativeNormalized"
)



for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    for covariance in ${covariances[@]}
    do
      echo $level $variable $covariance
      drawCovariance --variable ${variable} --level ${level} --covarianceName ${covariance}
    done
  done
done


covarianceRatios=(
#"covDataBasedOnlyMCStat;covDataBasedOnlyMCStatReco"
#"covDataBasedOnlyMCStatNormalized;covDataBasedOnlyMCStatRecoNormalized"
#"covDataBasedStatAndSysAlternative;covDataBasedStatAndSys"
#"covDataBasedStatAndSysAlternativeNormalized;covDataBasedStatAndSysNormalized"
#"covDataBasedStatSysAndSigMod;covDataBasedAllUnc"
#"covDataBasedAllUncAlternative;covDataBasedAllUnc"
#"covDataBasedAllUncAlternativeNormalized;covDataBasedAllUncNormalized"
#"covDataBasedStatSysAndSigModNormalized;covDataBasedAllUncNormalized"
#"covDataBasedOnlySigMod;covDataBasedOnlySigModOriginal"
#"covDataBasedOnlySigMod;covDataBasedOnlyIFSROriginal"
#"covDataBasedStatSysAndSigModAlternative;covDataBasedStatSysAndSigMod"
#"covDataBasedOnlySigModNormalized;covDataBasedOnlySigModOriginalNormalized"
#"covDataBasedOnlySigModNormalized;covDataBasedOnlyIFSROriginalNormalized"
)

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    for covariance in ${covarianceRatios[@]}
    do
      IFS=";" read -r -a arr <<< "${covariance}"
      numerator=${arr[0]}
      denominator=${arr[1]}
      echo $level $variable $numerator $denominator
      drawCovarianceRatios --variable ${variable} --level ${level} --covarianceNameNumerator ${numerator} --covarianceNameDenominator ${denominator}
    done
  done
done





