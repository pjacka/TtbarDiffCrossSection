## Default parameters
mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/data/unfolding_histos_optimizedBinning.env
systematicHistosPath=systematics_histos
save_bootstrapHistos=0
save_stressTestHistos=1
save_EFTHistos=0
doRebin=1
debugLevel=2


levels=(
# "PartonLevel"
"ParticleLevel"

)

variables=(
# "total_cross_section"
# "randomTop_pt"
# "randomTop_y"
"t1_pt"
# "t1_y"
# "t2_pt"
# "t2_y"
# "ttbar_mass"
# "ttbar_pt"
# "ttbar_y"
# "chi_ttbar"
# "y_boost"
# "pout"
# "H_tt_pt"
# "ttbar_deltaphi"
# "cos_theta_star"
# "cos_theta_star"
# "z_tt_pt"
# "y_star"
# "t1_mass"
# "t2_mass"
# "delta_pt"
# "t1_m_over_pt"
# "t2_m_over_pt"
)




for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
	  PrepareSystematicHistos --mainDir $mainDir --config $config_lumi --configFiles $config_files --configBinning $config_binning \
				  --variable $variable --level $level --outputDir $systematicHistosPath \
				  --saveBootstrapHistos $save_bootstrapHistos --saveStressTestHistos $save_stressTestHistos --saveEFTHistos ${save_EFTHistos} \
				  --doRebin $doRebin --debugLevel ${debugLevel}
	  #valgrind --tool=massif
	done
done
        
