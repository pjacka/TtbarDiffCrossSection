function haddFiles () {
	local maindir=$1
	local inputdir=$2
	local DSID=$3
	local ntuplename=$4
	local noutputfiles=$5

	echo "$maindir $inputdir $DSID $ntuplename $noutputfiles"

	if [ ! -d `echo $inputdir/*$DSID*|awk '{print $1}'` ]; then 
	    echo "haddFiles: Error: Folder $inputdir/*$DSID.* does not exists"
	    return 
	fi
	
	if [ $noutputfiles -eq 0 ]; then
		echo "mv ntuples into $ntuplename folder"
		rm -rf $maindir/$ntuplename
		mv $inputdir/*$DSID* $maindir/$ntuplename
		return
	fi
	
	files=(`ls $inputdir/*$DSID*/*`)
	echo ${files[*]}

	nfiles=${#files[@]}
	#echo $nfiles

	size=`expr $nfiles - 1 `
	size=`expr $size / $noutputfiles + 1`
	#echo $noutputfiles
	#echo $size
	
	files1=("${files[@]:0:2}")
	
	#echo ${files[0]}
	#echo ${files1[0]} 
	
	#echo ${#filelists[@]}
	n=${#files1[@]}
	#echo $n
	
	lowerbound=0
	> $currentDir/output1.txt
	> $currentDir/output2.txt
	#echo ${files[*]} > $currentDir/output2.txt
	for((i=1;i<=$noutputfiles;i+=1))
	do
		#echo $i
		smallfiles=("${files[@]:$lowerbound:$size}")
		#echo $lowerbound
		#echo ${smallfiles[0]}
		#echo ${files[$lowerbound]}
		#echo ${smallfiles[*]} >> $currentDir/output1.txt
		#echo $maindir/${ntuple}/allhad.boosted.output_${i}.root
		#hadd -f $maindir/${ntuple}/allhad.boosted.output_${i}.root ${files[*]}
		lowerbound=`expr $lowerbound + $size`
		mkdir -p $maindir/$ntuplename
		###use following line when execution was stopped after ith rootfile
		#if [ "$i" -le 7 ]; then continue; fi	
		#echo ${smallfiles[*]}
		
		if [ ${#smallfiles[@]} -eq 0 ]; then continue; fi
		#echo $maindir/$ntuplename/allhad.boosted.output_$i.root
		hadd -f $maindir/$ntuplename/allhad.boosted.output_$i.root ${smallfiles[*]}
		
	done

	
}



currentDir=$PWD

#maindir=/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170223_ntuples
#inputdir=/mnt/nfs05/ATLASDiffXsecAllHadBoosted/ntuples/20170223_ntuples
maindir=$1
inputdir=$maindir

cd $maindir

# 343365
#DSID=343365
#ntuple=mc15_13TeV.343365.aMcAtNloPythia8EvtGen_ttH125_dilep.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles

## 343366
#DSID=343366
#ntuple=mc15_13TeV.343366.aMcAtNloPythia8EvtGen_ttH125_semilep.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles

## 
#DSID=343367
#ntuple=mc15_13TeV.343367.aMcAtNloPythia8EvtGen_ttH125_allhad.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
### 
#DSID=410000
#ntuple=mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1
#noutputfiles=1
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
### 
## 
#DSID=410007
#ntuple=mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
#DSID=410013
#ntuple=mc15_13TeV.410013.PowhegPythiaEvtGen.Wt_inclusive_top.TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
### 
#DSID=410014
#ntuple=mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
### 
#DSID=410155
#ntuple=mc15_13TeV.410155.aMcAtNloPythia8EvtGen_ttW.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
### 
#DSID=410157
#ntuple=mc15_13TeV.410157.aMcAtNloPythia8EvtGen_ttZqq.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
###
DSID=410160
ntuple=mc15_13TeV.410160.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttbar_allhad.merge.DAOD_TOPQ1
noutputfiles=12
haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
## 
#DSID=410227
#ntuple=mc15_13TeV.410227.aMcAtNloPythia8EvtGen_ttbar_allhad.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
# 
#DSID=410234
#ntuple=mc15_13TeV.410234.PowhegPythia8EvtGen.allhad.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
## 
#DSID=410501
#ntuple=mc15_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
# 
#DSID=410502
#ntuple=mc15_13TeV.410502.PowhegPythia8EvtGen_ttbar_hdamp258p75_allhad.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
## 
#DSID=410506
#ntuple=mc15_13TeV.410506.PowhegPythia8_ttbar_hdamp258p75_allhad.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
### 
#DSID=410521
#ntuple=mc15_13TeV.410521.PowhegPythia8EvtGen_A14v3cUp_tt_h517p5_had_boosted.merge.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
## 
#DSID=410522
#ntuple=mc15_13TeV.410522.PowhegPythia8EvtGen_A14v3cDo_tt_h258p75_had_boosted.merge.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
## 
#DSID=410530
#ntuple=mc15_13TeV.410530.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_allhad_boosted.merge.DAOD_TOPQ1
#noutputfiles=0
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
## 
#DSID=period
#ntuple=data15_16
#noutputfiles=6
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles

## 
#DSID=
#ntuple=
#noutputfiles=1
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles

################################################
# mc15_13TeV Powheg+Pythia dijet JZX samples
################################################

#i=1
#a=426001
#for i in {1..9}
#do
#DSID=$a
#ntuple=$mc15_13TeV.${a}.PowhegPythia8EvtGen.jetjet_JZ${i}.TOPQ1
#noutputfiles=1
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
#a=`expr $a + 1`
#done
################################################
# mc15_13TeV Pythia8 36102* dijet JZXW samples
################################################
#i=0
#a=361020
#for i in {0..12}
#do
#DSID=$a
#ntuple=mc15_13TeV.${a}.Pythia8EvtGen.jetjet_JZ${i}W.TOPQ1
#noutputfiles=1
#haddFiles $maindir $inputdir $DSID $ntuple $noutputfiles
#a=`expr $a + 1`
#done

