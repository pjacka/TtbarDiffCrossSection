## Defalut parameters ! Don't change them. You can setup them below
signal="410471"
mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
inputDir="systematics_histosND"
generatorSysDir="generatorSystematicsND/genSystematics_using_signal_410471"
pseudoexperimentsDir="pseudoexperimentsND"
bootstrapDir="bootstrapND"
outputDir="covariancesND"
writePseudoexperiments=0
useBootstrapHistos=0
nPseudoexperiments=10000
#signalModeling="Standard"
#signalModeling="Alternative"
seed=42
debugLevel=0

levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
"t1_y_vs_t2_y"
)

echo "useBootstrapHistos: $useBootstrapHistos"

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    echo $variable
    #cmd="ProducePseudoexperiments2D --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --signal $signal --variable $variable --level $level --writePseudoexperiments $writePseudoexperiments --useBootstrapHistos $useBootstrapHistos --nPseudoexperiments $nPseudoexperiments --signalModelingWithMultijetShifted ${signalModelingWithMultijetShifted} --seed $seed --debugLevel $debugLevel"
    cmd="ProducePseudoexperiments --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --inputDir ${inputDir} --generatorSysDir ${generatorSysDir} --pseudoexperimentsDir ${pseudoexperimentsDir} --bootstrapDir ${bootstrapDir} --outputDir ${outputDir} --writePseudoexperiments $writePseudoexperiments --useBootstrapHistos $useBootstrapHistos --nPseudoexperiments $nPseudoexperiments --seed $seed --debugLevel $debugLevel"
    echo "Running $cmd"
    $cmd
    #valgrind --tool=massif $cmd
  done
done
