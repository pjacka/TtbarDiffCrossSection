macroPath="${TTBAR_PATH}/TtbarDiffCrossSection/macros/"
cd $macroPath

#DEFAULTS:
signal="410471"
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
doSysPDF=1
shiftNominal=0
debugLevel=0
inputDir="systematics_histos2D"
outputDir="generatorSystematics2D"
levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
#"t1_pt_vs_t2_pt"
"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
)






for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
		#cmd="runGeneratorSystematics2D --mainDir ${TtbarDiffCrossSection_output_path}/ --config $config_lumi --signal $signal --variable $variable --level $level --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"
		cmd="runGeneratorSystematics --mainDir ${TtbarDiffCrossSection_output_path}/ --config $config_lumi --signal $signal --variable $variable --level $level --inputDir ${inputDir} --outputDir ${outputDir} --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"
		echo "Running $cmd"
		$cmd
	done
done
      
