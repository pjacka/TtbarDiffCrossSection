#!/bin/bash

#defaults
	configName=TtbarDiffCrossSection/data/config.env
       doSys=0
    doSysPDF=0
    doSysAlphaSFSR=0
    doSysIFSR=0
doBootstrap=0
     nEvents=-1   # run over all events
  isSplitted=1
      inFile="allhad.boosted.output.txt_001"
 directories=( "mc15_13TeV.410234.PowhegPythia8EvtGen.allhad.DAOD_TOPQ1" )


if [ $# -ge 1 ]; then  directories=( "$1" ); fi
if [ $# -ge 2 ]; then       inFile=$2      ; fi
if [ $# -ge 3 ]; then       configName=$3      ; fi
if [ $# -ge 4 ]; then        doSys=$4      ; fi
if [ $# -ge 5 ]; then     doSysPDF=$5      ; fi
if [ $# -ge 6 ]; then     doSysAlphaSFSR=$6      ; fi
if [ $# -ge 7 ]; then     doSysIFSR=$7      ; fi
if [ $# -ge 8 ]; then  doBootstrap=$9      ; fi
if [ $# -ge 9 ]; then   isSplitted=${10}      ; fi
if [ $# -ge 10 ]; then      nEvents=${11}      ; fi

echo "running over these samples: "${directories}

echo "input parameters: inFile=$inFile configName=$configName doSys=$doSys doSysPDF=$doSysPDF doSysAlphaSFSR=$doSysAlphaSFSR doSysIFSR=$doSysIFSR doBootstrap=$doBootstrap isSplitted=$isSplitted nEvents=$nEvents"

filelists=${TTBAR_PATH}/TtbarDiffCrossSection/filelists/
if [ $isSplitted -eq 1 ]; then  filelists=${TTBAR_PATH}/TtbarDiffCrossSection/filelists/SPLITTED/; fi

for dir in "${directories[@]}"; do

    echo $dir
    outpath=${TtbarDiffCrossSection_output_path}${dir}
    if [ $isSplitted -eq 1 ]; then 
	outpath=${TtbarDiffCrossSection_output_path}/SPLITTED/${dir}
    fi

    echo "root file with histograms will be saved in" $outpath

    mkdir -p $outpath

    reweighting=0
    for id in `seq 1 100`; do
      if [[ "${dir}" == *reweighted$id ]] ; then 
	reweighting=$id
      fi
    done
    cmd="produceHistos --output $outpath/"${inFile/.txt/.root}" --filelist ${filelists}/${dir}/${inFile} --config ${configName} --doDetSys ${doSys} --doSysPDF ${doSysPDF} --doSysAlphaSFSR $doSysAlphaSFSR --doSysIFSR $doSysIFSR --doReweighting ${reweighting} --nBootstrapReplicas ${doBootstrap} --nEvents ${nEvents}"
    $cmd
    #produceHistos ${filelists}/${dir}/${inFile} $outpath/"${inFile/.txt/.root}" ${configName} ${doSys} ${doSysPDF} ${doSysAlphaSFSR} ${doSysIFSR} ${reweighting} ${doBootstrap} ${nEvents}
done

