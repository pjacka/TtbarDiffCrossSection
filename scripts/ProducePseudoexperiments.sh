## Defalut parameters ! Don't change them. You can setup them below
mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
inputDir="systematics_histos"
generatorSysDir="generatorSystematics/genSystematics_using_signal_410471"
pseudoexperimentsDir="pseudoexperiments"
bootstrapDir="bootstrap"
outputDir="covariances"
writePseudoexperiments=0
useBootstrapHistos=0
nPseudoexperiments=10000
seed=42
debugLevel=0

levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
"total_cross_section"
#"randomTop_pt"
#"randomTop_y"
#"t1_pt"
#"t1_y"
#"t2_pt"
#"t2_y"
#"ttbar_mass"
#"ttbar_pt"
#"ttbar_y"
#"chi_ttbar"
#"y_boost"
#"pout"
#"H_tt_pt"
#"ttbar_deltaphi"
#"cos_theta_star"
#"z_tt_pt"
#"y_star"
#"t1_mass"
#"t2_mass"
#"delta_pt"
#"t1_m_over_pt"
#"t2_m_over_pt"
#"concatenated"
)

echo "useBootstrapHistos: $useBootstrapHistos"

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    echo $variable
    cmd="ProducePseudoexperiments --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --inputDir ${inputDir} --generatorSysDir ${generatorSysDir} --pseudoexperimentsDir ${pseudoexperimentsDir} --bootstrapDir ${bootstrapDir} --outputDir ${outputDir} --writePseudoexperiments $writePseudoexperiments --useBootstrapHistos $useBootstrapHistos --nPseudoexperiments $nPseudoexperiments --seed $seed --debugLevel $debugLevel"
    echo "Running $cmd"
    $cmd
    #valgrind --tool=massif $cmd
    #valgrind --leak-check=full $cmd
  done
done

