# Ota
if [ $USER == zaplatilek ] || [ $USER == ozaplati ] ; then
    #signal="410007"
    #signal="410234"
    signal="410506"
    #signal="410008"
    #signal="410045"
    #signal="410160"
    use_ttbar_SF=true
fi

# Peter 
if [ $USER == pberta ] || [ $USER == berta ] ; then

signal="410007"
#signal="410234"

fi


# Petr
if [ $USER == pjacka ] || [ $USER == jacka ] ; then


#signal="410007"
#signal="410234"
signal="410506"
#signal="410008"
#signal="410045"
#signal="410160"


use_ttbar_SF=true
fi

# Roman
if [ $USER == rlysak ] || [ $USER == lysak ] ; then

#signal="410007"
#signal="410234"
signal="410506"
#signal="410008"
#signal="410045"
#signal="410160"

fi

# Honza
if [ $USER == jpalicka ] ; then

#signal="410007"
#signal="410234"
signal="410506"
#signal="410008"
#signal="410045"
#signal="410160"

fi




mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/macros/Unfolding.env
nPseudoexperiments=10000


levels=(
"PartonLevel"
"ParticleLevel"
)

variables=(
"total_cross_section_fine"
"topCandidate_pt_fine"
"randomTop_y_fine"
"leadingTop_pt_fine"
"leadingTop_y_fine"
"subleadingTop_pt_fine"
"subleadingTop_y_fine"
"ttbar_mass_fine"
"ttbar_pt_fine"
"ttbar_y_fine"
"chi_ttbar_fine"
"y_boost_fine"
"pout_fine"
"H_tt_pt_fine"
"ttbar_deltaphi_fine"
"cos_theta_star_fine"
"z_tt_pt_fine"
"y_star_fine"
"inclusive_top_pt_fine"
"inclusive_top_y_fine"
)


for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
		echo $variable
		CalculateCovariances $mainDir $config_files $config_lumi $signal $variable $level $nPseudoexperiments
	done
done
