## Default parameters

mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
#config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/data/unfolding_histosND_optimizedBinning.env
systematicHistosPath=systematics_histosND
save_bootstrapHistos=0
save_stressTestHistos=0
doRebin=0
debugLevel=0

levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=( 
#"ttbar_y_vs_ttbar_mass_vs_t1_pt" 
"t1_y_vs_t2_y" 
)






for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
  
    config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/data/optBinningND/${variable}.env
  
    cmd="PrepareSystematicHistosND --mainDir $mainDir --config $config_lumi --configFiles $config_files --configBinning $config_binning --variable $variable --level $level --outputDir $systematicHistosPath --saveBootstrapHistos $save_bootstrapHistos --saveStressTestHistos $save_stressTestHistos --doRebin $doRebin --debugLevel ${debugLevel}"
    #cmd="gdb --args PrepareSystematicHistosND --mainDir $mainDir --config $config_lumi --configFiles $config_files --configBinning $config_binning --variable $variable --level $level --outputDir $systematicHistosPath --saveBootstrapHistos $save_bootstrapHistos --saveStressTestHistos $save_stressTestHistos --doRebin $doRebin --debugLevel ${debugLevel}"
    echo "Running $cmd"
    $cmd
    #valgrind --tool=massif $cmd
  done
done
