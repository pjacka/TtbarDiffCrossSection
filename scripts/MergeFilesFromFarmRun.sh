#!/bin/bash

#defaults
#outDir=${TTBAR_PATH}/run/
outDir=${TtbarDiffCrossSection_output_path}
currentDir=$pwd

if [ $# -ge 1 ]; then   outDir=$1;  fi

splitDir=$outDir/SPLITTED
#outputfilename=allhad.boosted.output.root


for dir in `ls $splitDir`; do

  echo $dir
  mkdir -p $outDir/$dir

  inFiles=(`ls  $splitDir/$dir|grep -v MMefficiencies`)
  inFileBaseName=`echo $inFiles|awk '{print $1}'`
  inFileBaseName=`basename $inFileBaseName _000`
  
  cd $splitDir/$dir
  if [ ${#inFiles[@]} -ge 2 ]; then #hadd -f $outDir/$dir/$inFileBaseName ${inFiles[*]}  
  mergeHistosFromRun $outDir/$dir/$inFileBaseName ${inFiles[*]}
  echo ${#inFiles[@]}
  echo ${inFiles[*]}
  echo $inFileBaseName
  elif [ ${#inFiles[@]} -eq 1 ]; then mv ${inFiles[*]} $outDir/$dir/$inFileBaseName
  fi

done

