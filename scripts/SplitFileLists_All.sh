#!/bin/bash

#=========================================
#
# examples of running:
#   ./TtbarDiffCrossSection/scripts/SplitFileLists_All.sh             #runs over all samples
#   ./TtbarDiffCrossSection/scripts/SplitFileLists_All.sh -s default  #runs over default samples
#   ./TtbarDiffCrossSection/scripts/SplitFileLists_All.sh -i 410234   #runs only over sample ID 410234
#
#=========================================

baseDir=${TTBAR_PATH}/TtbarDiffCrossSection/filelists
cd $baseDir

#constants
signal_reference="601237"
data_reference="data22"

MCsamples=( "MC23a" "MC23d")
#MCsamples=( "MC23a" )


#defaults:
      samples="default" #options: default signal nonallhad Wt tChan data
      sampleIDs="601237"

while getopts :s:i:h OPT; do
    case $OPT in
        s)     samples=$OPTARG               ;;
        i)   sampleIDs=$OPTARG; samples=""   ;;
        h)  echo "usage: SplitFileLists_All.sh [-s <samples>] [-i <datasetID>] "; exit 0 ;;
        *)  echo "SplitFileLists_All.sh: unknown option -$OPTARG, bailing out";
            exit -1;
            ;;
    esac
done



if [ ".$samples" != "."     ]; then echo "running on samples: $samples"
else                                echo "running on dataset IDs: $sampleIDs"; fi

if [ "$samples" == "default" ]; then sampleIDs="${signal_reference} ${data_reference} 601229 601230 601352 601355 601627 601631 601352 601355 601627 601631 522036 522040 602637 602638 700995 700996 700997 data22 data23"; fi
if [ "$samples" == "signal"  ]; then sampleIDs="601237"; fi
if [ "$samples" == "nonallhad"  ]; then sampleIDs="601229 601230"; fi
if [ "$samples" == "Wt"      ]; then sampleIDs="601352 601355 601627 601631"; fi 
if [ "$samples" == "tChan"      ]; then sampleIDs=""; fi 
if [ "$samples" == "ttHWZ"      ]; then sampleIDs="602637 602638 700995 700996 700997 522036 522040"; fi
if [ "$samples" == "data"    ]; then sampleIDs="data22 data23"; fi

echo "this corresponds to dataset IDs: ${sampleIDs}"
  
for MC in ${MCsamples[@]}; do  
  # Temporary if statement; not all MC16e samples ready
  dirs=""
  nJobs=""
  #                                                   #sample directory                            #number of input ntuples in each filelist
  #signal
  if [[ $sampleIDs == *"601237"* ]]; then dirs+=" ${MC}.601237.PhPy8EG "; nJobs+=" 500 "; fi # signal nominal
  
  
  #signal non-allhad
  if [[ $sampleIDs == *"601229"* ]]; then dirs+=" ${MC}.601229.PhPy8EG "; nJobs+=" 200 "; fi # Lepton + jets
  if [[ $sampleIDs == *"601230"* ]]; then dirs+=" ${MC}.601230.PhPy8EG "; nJobs+=" 100 "; fi # Dilepton
  
  #Wt
  if [[ $sampleIDs == *"601352"* ]]; then dirs+=" ${MC}.601352.PhPy8EG  "; nJobs+=" 100 "; fi
  if [[ $sampleIDs == *"601355"* ]]; then dirs+=" ${MC}.601355.PhPy8EG  "; nJobs+=" 100 "; fi
  if [[ $sampleIDs == *"601627"* ]]; then dirs+=" ${MC}.601627.PhPy8EG  "; nJobs+=" 100 "; fi
  if [[ $sampleIDs == *"601631"* ]]; then dirs+=" ${MC}.601631.PhPy8EG  "; nJobs+=" 100 "; fi
  
  #Single-top t-channel
  
  
  #ttHWZ
  if [[ $sampleIDs == *"522036"* ]]; then dirs+=" ${MC}.522036.aMCPy8EG  "; nJobs+=" 100 "; fi  # ttZqq
  if [[ $sampleIDs == *"522040"* ]]; then dirs+=" ${MC}.522040.aMCPy8EG  "; nJobs+=" 100 "; fi  # ttZnunu
  if [[ $sampleIDs == *"602637"* ]]; then dirs+=" ${MC}.602637.PhPy8EG  "; nJobs+=" 100 "; fi  # ttH dilep
  if [[ $sampleIDs == *"602638"* ]]; then dirs+=" ${MC}.602638.PhPy8EG  "; nJobs+=" 100 "; fi  # ttH semilep
  if [[ $sampleIDs == *"700995"* ]]; then dirs+=" ${MC}.700995.Sh_2214_ttW_0Lfilter  "; nJobs+=" 100 "; fi
  if [[ $sampleIDs == *"700996"* ]]; then dirs+=" ${MC}.700996.Sh_2214_ttW_1Lfilter  "; nJobs+=" 100 "; fi
  if [[ $sampleIDs == *"700997"* ]]; then dirs+=" ${MC}.700997.Sh_2214_ttW_2Lfilter  "; nJobs+=" 100 "; fi

  #WZ+jets
  
  #QCD
  
  #QCD_LO

  
  #DATA
  if [[ $sampleIDs == *"data22"*    ]]; then dirs+=" data2022_AllYear.physics_Main    "; nJobs+=" 20 "; fi
  if [[ $sampleIDs == *"data23"*    ]]; then dirs+=" data2023_AllYear.physics_Main    "; nJobs+=" 20 "; fi
  
  
  nDirs=`echo $dirs|wc -w`
  for iDir in `seq $nDirs`; do
	 dir=`echo $dirs         | cut -f $iDir -d" "`
      #nFiles=`echo $nFilesPerJob | cut -f $iDir -d" "`
      njobs=`echo $nJobs | cut -f $iDir -d" "`
      #echo "running over directory: $dir, splitting into $nFiles files per jobs"
      echo "running over directory: $dir, splitting into $njobs jobs"
      #${TTBAR_PATH}/TtbarDiffCrossSection/scripts/SplitFileLists.sh $dir $nFiles
      ${TTBAR_PATH}/TtbarDiffCrossSection/scripts/SplitFileLists.sh $dir $njobs
  done

done
