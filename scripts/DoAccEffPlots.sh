#!/bin/bash
########################################################################################
# Ota 10.11. 2017                                                                      #
# executable script for making plots: Efficiendy and Acceptancy of different MC        #
########################################################################################

macroPath="${TTBAR_PATH}/TtbarDiffCrossSection/macros"


#rootFilePath="${TtbarDiffCrossSection_output_path}/root.MC16a/systematics_histos2D"
subDir="pdf"
AccEffDir="/AccEffPlots"
dimension="1D"
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
systematicHistosPath="systematics_histos"



echo ${TtbarDiffCrossSection_output_path}${subDir}.${mcSamplesProduction}${AccEffDir}

levels=(
"PartonLevel"
"ParticleLevel"
)

variables=(
"total_cross_section"
"randomTop_pt"
"randomTop_y"
"t1_pt"
"t1_y"
"t2_pt"
"t2_y"
"ttbar_mass"
"ttbar_pt"
"ttbar_y"
"chi_ttbar"
"y_boost"
"pout"
"H_tt_pt"
"ttbar_deltaphi"
"cos_theta_star"
"cos_theta_star"
"z_tt_pt"
"y_star"
"t1_mass"
"t2_mass"
"delta_pt"
"t1_m_over_pt"
"t2_m_over_pt"
)


for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    cmd="CompareAccEff ${config_lumi} ${systematicHistosPath} ${AccEffDir} ${variable} ${level} ${dimension}"
    #cmd="gdb --args CompareAccEff ${config_lumi} ${systematicHistosPath} ${AccEffDir} ${variable} ${level} ${dimension}"
    echo "Runnning cmd: $cmd"
    $cmd
  done
done



dimension="ND"
AccEffDir="/AccEffPlotsND"
systematicHistosPath="systematics_histosND"


variables=(
#"ttbar_y_vs_ttbar_mass_vs_t1_pt"
#"t1_pt_vs_t2_pt"
#"t1_pt_vs_delta_pt"
#"t1_y_vs_t2_y"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
)

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    cmd="CompareAccEff ${config_lumi} ${systematicHistosPath} ${AccEffDir} ${variable} ${level} ${dimension}"
    echo "Runnning cmd: $cmd"
    $cmd
  done
done


echo "DONE!"
