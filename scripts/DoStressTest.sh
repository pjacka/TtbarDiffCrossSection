mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json

levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
#"total_cross_section"
#"randomTop_pt"
#"randomTop_y"
#"t1_pt"
#"t1_y"
#"t2_pt"
#"t2_y"
#"ttbar_mass"
#"ttbar_pt"
#"ttbar_y"
#"chi_ttbar"
#"y_boost"
#"pout"
#"H_tt_pt"
#"ttbar_deltaphi"
#"cos_theta_star"
##"z_tt_pt"
##"y_star"
#"t1_mass"
#"t2_mass"
#"delta_pt"
#"t1_m_over_pt"
#"t2_m_over_pt"
)

for level in ${levels[@]}
do
	echo $level
	for variable in ${variables[@]}
	do
		echo $variable
		DoStressTest --variable $variable --level $level --config $config_lumi
	done
done

variables=(
#"ttbar_y_vs_ttbar_mass_vs_t1_pt"
#"t1_pt_vs_t2_pt"
#"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
"t1_y_vs_t2_y"
)

dimension="ND"
systematicsHistosDir="systematics_histosND"
stressTestHistosDir="systematics_histosND"

for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
		configBinning=TtbarDiffCrossSection/data/optBinningND/${variable}.env
		echo $variable
		DoStressTest --variable ${variable} --level ${level} --config $config_lumi --configBinning ${configBinning} --dimension ${dimension} --systematicsHistosDir ${systematicsHistosDir} --stressTestHistosDir ${stressTestHistosDir}
	done
done
