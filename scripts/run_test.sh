#!/bin/sh

if [ $PWD != $HOME/xAnalysis ]
then
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    cd $HOME/xAnalysis
    source  TtbarDiffCrossSection/scripts/setup.sh
fi


# Petr:
if [ $USER == pjacka ] ; then
  output_histos=/afs/cern.ch/user/p/pjacka/Workspace/xAnalysis/
  doSys=0
  #doSys=1
  doSysPDF=0
  #doSysPDF=1

  #sample=all
  #sample=Whad
  #sample=Zhad
  #sample=4tops
  #sample=ttbarWW
  #sample=ttW
  #sample=nonallhad
  #sample=signal
  #sample=QCD
  #sample=QCD_LO
  sample=data
  #sample=reweighted1
  #sample=reweighted2
  #sample=zprime
fi
echo "root file with histograms will be saved in" $output_histos
echo "running over these samples: "${sample}

filelists=${TTBAR_PATH}/TtbarDiffCrossSection/filelists/

if [ $sample == all ] 
then
directories=( "mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1" "mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1" "mc15_13TeV.410013.PowhegPythiaEvtGen.Wt_inclusive_top.TOPQ1" "mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1"
"data15.DAOD_EXOT3"
"mc15_13TeV.301322.Pythia8EvtGen.zprime400_tt.DAOD_TOPQ1"
"mc15_13TeV.301323.Pythia8EvtGen.zprime500_tt.DAOD_TOPQ1"
"mc15_13TeV.301324.Pythia8EvtGen.zprime750_tt.DAOD_TOPQ1"
"mc15_13TeV.301325.Pythia8EvtGen.zprime1000_tt.DAOD_TOPQ1"
"mc15_13TeV.301326.Pythia8EvtGen.zprime1250_tt.DAOD_TOPQ1"
"mc15_13TeV.301327.Pythia8EvtGen.zprime1500_tt.DAOD_TOPQ1"
"mc15_13TeV.301328.Pythia8EvtGen.zprime1750_tt.DAOD_TOPQ1"
"mc15_13TeV.301329.Pythia8EvtGen.zprime2000_tt.DAOD_TOPQ1"
"mc15_13TeV.301330.Pythia8EvtGen.zprime2250_tt.DAOD_TOPQ1"
#"mc15_13TeV.301331.Pythia8EvtGen.zprime2500_tt.DAOD_TOPQ1"
"mc15_13TeV.301332.Pythia8EvtGen.zprime2750_tt.DAOD_TOPQ1"
"mc15_13TeV.426003.PowhegPythia8EvtGen.jetjet_JZ3.TOPQ1"
"mc15_13TeV.426004.PowhegPythia8EvtGen.jetjet_JZ4.TOPQ1" 
"mc15_13TeV.426005.PowhegPythia8EvtGen.jetjet_JZ5.TOPQ1" 
"mc15_13TeV.426006.PowhegPythia8EvtGen.jetjet_JZ6.TOPQ1" 
#"mc15_13TeV.426007.PowhegPythia8EvtGen.jetjet_JZ7.TOPQ1" 
"mc15_13TeV.426008.PowhegPythia8EvtGen.jetjet_JZ8.TOPQ1" 
#"mc15_13TeV.426009.PowhegPythia8EvtGen.jetjet_JZ9.TOPQ1"
)
fi
if [ $sample == signal ] 
then
directories=( "mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1" )
fi
if [ $sample == nonallhad ]
then
directories=( "mc15_13TeV.410000.PowhegPythiaEvtGen.nonallhad.DAOD_TOPQ1" )
fi
if [ $sample == Wt ]
then
directories=( "mc15_13TeV.410013.PowhegPythiaEvtGen.Wt_inclusive_top.TOPQ1.p2454" "mc15_13TeV.410014.PowhegPythiaEvtGen.Wt_inclusive_antitop.TOPQ1.p2454" )
fi
if [ $sample == QCD ]
then
directories=( "mc15_13TeV.426003.PowhegPythia8EvtGen.jetjet_JZ3.TOPQ1.p2454" "mc15_13TeV.426004.PowhegPythia8EvtGen.jetjet_JZ4.TOPQ1.p2454" "mc15_13TeV.426005.PowhegPythia8EvtGen.jetjet_JZ5.TOPQ1.p2454" "mc15_13TeV.426006.PowhegPythia8EvtGen.jetjet_JZ6.TOPQ1.p2454" "mc15_13TeV.426007.PowhegPythia8EvtGen.jetjet_JZ7.TOPQ1.p2454" "mc15_13TeV.426008.PowhegPythia8EvtGen.jetjet_JZ8.TOPQ1.p2454" "mc15_13TeV.426009.PowhegPythia8EvtGen.jetjet_JZ9.TOPQ1.p2454" )
fi
if [ $sample == QCD_LO ]
then
directories=( # "mc15_13TeV.361021.Pythia8EvtGen.jetjet_JZ1W.TOPQ1"
# "mc15_13TeV.361022.Pythia8EvtGen.jetjet_JZ2W.TOPQ1"
 "mc15_13TeV.361023.Pythia8EvtGen.jetjet_JZ3W.TOPQ1"
 "mc15_13TeV.361024.Pythia8EvtGen.jetjet_JZ4W.TOPQ1"
 "mc15_13TeV.361025.Pythia8EvtGen.jetjet_JZ5W.TOPQ1"
 "mc15_13TeV.361026.Pythia8EvtGen.jetjet_JZ6W.TOPQ1"
 "mc15_13TeV.361027.Pythia8EvtGen.jetjet_JZ7W.TOPQ1"
 "mc15_13TeV.361028.Pythia8EvtGen.jetjet_JZ8W.TOPQ1"
 "mc15_13TeV.361029.Pythia8EvtGen.jetjet_JZ9W.TOPQ1"
 "mc15_13TeV.361030.Pythia8EvtGen.jetjet_JZ10W.TOPQ1"
 "mc15_13TeV.361031.Pythia8EvtGen.jetjet_JZ11W.TOPQ1"
 "mc15_13TeV.361032.Pythia8EvtGen.jetjet_JZ12W.TOPQ1"
 )
fi
if [ $sample == QCD1 ]
then
directories=( "mc15_13TeV.426004.PowhegPythia8EvtGen.jetjet_JZ4.TOPQ1.p2454" )
fi
if [ $sample == data ]
then
directories=( "data15.DAOD_EXOT3" )
fi
if [ $sample == Whad ]
then
directories=( "mc15_13TeV.304623.Pythia8EvtGen.WHad_280_500.DAOD_TOPQ1"
"mc15_13TeV.304624.Pythia8EvtGen.WHad_500_700.DAOD_TOPQ1"
"mc15_13TeV.304625.Pythia8EvtGen.WHad_700_1000.DAOD_TOPQ1"
"mc15_13TeV.304626.Pythia8EvtGen.WHad_1000_1400.DAOD_TOPQ1"
 )
fi
if [ $sample == Zhad ]
then
directories=( "mc15_13TeV.304628.Pythia8EvtGen.ZHad_280_500.DAOD_TOPQ1"
"mc15_13TeV.304629.Pythia8EvtGen.ZHad_500_700.DAOD_TOPQ1"
"mc15_13TeV.304630.Pythia8EvtGen.ZHad_700_1000.DAOD_TOPQ1"
"mc15_13TeV.304631.Pythia8EvtGen.ZHad_1000_1400.DAOD_TOPQ1"
"mc15_13TeV.304632.Pythia8EvtGen.ZHad_1400.DAOD_TOPQ1"
 )
fi
if [ $sample == 4tops ]
then
directories=( "mc15_13TeV.410080.MadGraphPythia8EvtGen_A14NNPDF23_4topSM.merge.DAOD_TOPQ1"
 )
fi
if [ $sample == ttbarWW ]
then
directories=( "mc15_13TeV.410081.MadGraphPythia8EvtGen_A14NNPDF23_ttbarWW.merge.DAOD_TOPQ1"
 )
fi
if [ $sample == ttW ]
then
directories=( "mc15_13TeV.410066.MadGraphPythia8EvtGen_A14NNPDF23LO_ttW_Np0.merge.DAOD_TOPQ1"
"mc15_13TeV.410067.MadGraphPythia8EvtGen_A14NNPDF23LO_ttW_Np1.merge.DAOD_TOPQ1"
"mc15_13TeV.410068.MadGraphPythia8EvtGen_A14NNPDF23LO_ttW_Np2.merge.DAOD_TOPQ1"
 )
fi
if [ $sample == zprime ]
then
directories=( "mc15_13TeV.301322.Pythia8EvtGen.zprime400_tt.DAOD_TOPQ1"
"mc15_13TeV.301323.Pythia8EvtGen.zprime500_tt.DAOD_TOPQ1"
"mc15_13TeV.301324.Pythia8EvtGen.zprime750_tt.DAOD_TOPQ1"
"mc15_13TeV.301325.Pythia8EvtGen.zprime1000_tt.DAOD_TOPQ1"
"mc15_13TeV.301326.Pythia8EvtGen.zprime1250_tt.DAOD_TOPQ1"
"mc15_13TeV.301327.Pythia8EvtGen.zprime1500_tt.DAOD_TOPQ1"
"mc15_13TeV.301328.Pythia8EvtGen.zprime1750_tt.DAOD_TOPQ1"
"mc15_13TeV.301329.Pythia8EvtGen.zprime2000_tt.DAOD_TOPQ1"
"mc15_13TeV.301330.Pythia8EvtGen.zprime2250_tt.DAOD_TOPQ1"
"mc15_13TeV.301331.Pythia8EvtGen.zprime2500_tt.DAOD_TOPQ1"
"mc15_13TeV.301332.Pythia8EvtGen.zprime2750_tt.DAOD_TOPQ1"
 )
fi
if [ $sample == reweighted1 ] 
then
cp -r ${filelists}${dir}/"mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1" ${filelists}${dir}/"mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1_reweighted1"
directories=( "mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1_reweighted1" )
fi
if [ $sample == reweighted2 ] 
then
cp -r ${filelists}${dir}/"mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1" ${filelists}${dir}/"mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1_reweighted2"
directories=( "mc15_13TeV.410007.PowhegPythiaEvtGen.allhad.DAOD_TOPQ1_reweighted2" )
fi


#directories=${directories_${sample}}
#directories=${!d}




for dir in "${directories[@]}"
do
echo $dir
mkdir -p ${output_histos}${dir}

reweighting=0
if [[ "${dir}" == *reweighted1 ]]
then
reweighting=1
fi
if [[ "${dir}" == *reweighted2 ]]
then
reweighting=2
fi


for file in `ls ${filelists}${dir}`
do

if [[ "${file}" == *.txt ]]
then
echo ${filelists}${dir}/${file}

produceHistos ${filelists}${dir}/${file} ${output_histos}${dir}/"${file/.txt/.root}" ${doSys} ${doSysPDF} ${reweighting} -1

#produceHistos ${filelists}${dir}/${file} ${output_histos}${dir}/"${file/.txt/.root}" ${doSys} ${doSysPDF} ${reweighting} 1000

fi
done
done
