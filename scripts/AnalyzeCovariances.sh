## Defalut parameters ! Don't change them. You can setup them below
signal="410506"
mainDir=$TtbarDiffCrossSection_output_path
config_files=${TTBAR_PATH}/TtbarDiffCrossSection/data/datasetnames.env
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
config_binning=${TTBAR_PATH}/TtbarDiffCrossSection/macros/Unfolding.env
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env


levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
#"total_cross_section"
#"topCandidate_pt"
#"randomTop_y"
#"leadingTop_pt"
#"leadingTop_y"
#"subleadingTop_pt"
#"subleadingTop_y"
#"ttbar_mass"
#"ttbar_pt"
#"ttbar_y"
#"chi_ttbar"
#"y_boost"
#"pout"
#"H_tt_pt"
#"ttbar_deltaphi"
#"cos_theta_star"
#"z_tt_pt"
#"y_star"
#"inclusive_top_pt"
#"inclusive_top_y"
"concatenated"
)



# Peter 
if [ $USER == pberta ] || [ $USER == berta ] ; then
signal="410506"
fi


# Petr
if [ $USER == pjacka ] || [ $USER == jacka ] ; then
useBootstrapHistos=0
nPseudoexperiments=20000
fi

# Roman
if [ $USER == rlysak ] || [ $USER == lysak ] ; then
signal="410506"
fi

# Honza
if [ $USER == jpalicka ] ; then
signal="410506"
fi

echo "useBootstrapHistos: $useBootstrapHistos"

for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    echo $variable
    AnalyzeCovariances $mainDir $config_files $config_lumi $config_binning $config_sysnames $signal $variable $level
    #valgrind --tool=massif ProducePseudoexperiments $mainDir $config_files $config_lumi $config_binning $config_sysnames $signal $variable $level $writePseudoexperiments $useBootstrapHistos $nPseudoexperiments
  done
done
