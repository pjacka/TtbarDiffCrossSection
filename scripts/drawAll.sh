#defaults
qcd=( "ABCD16_BBB_S9_tight" "ABCD16_BBB_bjets_tight" "ABCD16_BBB_bjet1_tight" "ABCD16_BBB_ljet1_tau32_tight" "ABCD16_BBB_ljet2_tau32_tight" "ABCD16_BBB_regionK" "ABCD16_BBB_regionL" "ABCD16_BBB_regionM" "ABCD16_BBB_regionN" )
#qcd=("ABCD16_BBB_regionLS")
#qcd=( "ABCD16_BBB_S9_tight")
#mc_production=("MC16a" "MC16d" "MC16e" "All")
mc_production=("All")
signal="nominal"
doSys=1
doNorm=1
use_lumi=0
doRebin=0
use_ttbar_SF=0
debugLevel=10

echo using output path $TtbarDiffCrossSection_output_path

for bkg in ${qcd[@]}
do
   for mc_prod in ${mc_production[@]}
   do
      drawAll --mainDir ${TtbarDiffCrossSection_output_path} --qcd ${bkg} --signal ${signal} --mcProd ${mc_prod} --doSys ${doSys} --doNorm ${doNorm} --useLumi ${use_lumi} --doRebin ${doRebin} --useTTBarSF ${use_ttbar_SF} --debugLevel ${debugLevel}
   done
done
      
