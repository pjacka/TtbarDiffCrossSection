#default parameters
mainDir=$TtbarDiffCrossSection_output_path
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.env
config_sysnames=${TTBAR_PATH}/TtbarDiffCrossSection/data/sys_names.env
plotsDir="sys_errors_summary_plots2D"
systematicUncertaintiesDir="SystematicUncertainties2D"
covariancesDir="covariances2D"
systematicHistosDir="systematics_histos2D"
dimension="2D"
debugLevel=0

levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
#"t1_pt_vs_t2_pt"
"t1_pt_vs_delta_pt"
#"t1_pt_vs_ttbar_pt"
#"t1_pt_vs_ttbar_mass"
#"ttbar_y_vs_t1_pt"
#"ttbar_y_vs_ttbar_mass"
)


for level in ${levels[@]}
do
  for variable in ${variables[@]}
  do
    #cmd="CalculateAsymmetricErrors --mainDir $mainDir --outputDir $outputDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --dimension $dimension --debugLevel $debugLevel"
    cmd="CalculateAsymmetricErrors --mainDir $mainDir --config $config_lumi --configSysNames $config_sysnames --variable $variable --level $level --plotsDir ${plotsDir} --systematicUncertaintiesDir ${systematicUncertaintiesDir} --covariancesDir ${covariancesDir} --systematicHistosDir ${systematicHistosDir} --dimension $dimension --debugLevel $debugLevel"
    echo "Running $cmd"
    $cmd    
  done
done
