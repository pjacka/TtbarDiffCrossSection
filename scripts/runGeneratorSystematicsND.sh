#DEFAULTS:
signal="410471"
config_lumi=${TTBAR_PATH}/TtbarDiffCrossSection/data/config.json
inputDir="systematics_histosND"
outputDir="generatorSystematicsND"
doSysPDF=1
shiftNominal=0
debugLevel=10


levels=(
#"PartonLevel"
"ParticleLevel"
)

variables=(
"t1_y_vs_t2_y"
)






# Ota
if [ $USER == zaplatilek ] || [ $USER == ozaplati ] ; then 
:
fi


# Peter 
if [ $USER == pberta ] || [ $USER == berta ] ; then
:
#signal="410007"
#signal="410234"

fi


# Petr
if [ $USER == pjacka ] || [ $USER == jacka ] ; then
:
fi

# Roman
if [ $USER == rlysak ] || [ $USER == lysak ] ; then
:
fi

# Honza
if [ $USER == jpalicka ] ; then
:

fi




for level in ${levels[@]}
do
	for variable in ${variables[@]}
	do
		cmd="runGeneratorSystematics --mainDir ${TtbarDiffCrossSection_output_path}/ --config $config_lumi --signal $signal --variable $variable --level $level --inputDir ${inputDir} --outputDir ${outputDir} --doSysPDF $doSysPDF --shiftNominal $shiftNominal --debugLevel $debugLevel"
		echo "Running $cmd"
		$cmd
	done
done



